jQuery(document).ready(function() { 
 // refresh captcha
	 jQuery('#refresh').click(function() {  
		document.getElementById('captcha').src=""+SURL+"login/get-captcha?rnd=" + Math.random();
	 });

	jQuery('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		
		  clicked_tab = jQuery(this).attr('href');
		  
		  if(clicked_tab == '#card_tab'){
			  
  			jQuery('#payment_method_radio').val('credit_card');  
  			jQuery('#save_btn').val('Make Payment');
			jQuery('#save_btn').html('Make Payment');
			
  			jQuery('#payment_popup_message').text('Please wait while processing your payment.');

  			if(!jQuery('#paypal_form_div').hasClass('hidden')){

  				jQuery('#paypal_form_div').addClass('hidden');

  			} // if(jQuery('#paypal_form_div').hasClass())
	  
		  } else {
			  
  			jQuery('#payment_method_radio').val('paypal');
  	
			jQuery('#save_btn').val('Checkout with PayPal');
			jQuery('#save_btn').html('Checkout with PayPal');
	
	        jQuery('#payment_popup_message').text('Please wait while redirecting to PayPal for payment.');
        
  			jQuery('#card').val('');
  			jQuery('#cvc').val('');
  			jQuery('#expMonth').val('');
  			jQuery('#expYear').val('');
  			
  			if(jQuery('#paypal_form_div').hasClass('hidden')){
  				jQuery('#paypal_form_div').removeClass('hidden');
  			} // if(jQuery('#paypal_form_div').hasClass())
	  
		  }//end if(clicked_tab == '#card_tab')
	});
	jQuery('#payment_method_radio').val('credit_card');  
	 
});

  
  function goto_step_1(){

    location.href = SURL+'register';

  } // function goto_step_1()
  
  function verify_email__(){

      // Verify if the form is Validated
      jQuery('#register_frm').formValidation('validate');
      var isValidForm = jQuery('#register_frm').data('formValidation').isValid();

      var address_id  = jQuery('#address').val();
      var town_id = jQuery('#town').val();
      var county_id   = jQuery('#county').val();
      var postcode_id = jQuery('#postcode').val();  
      
      if(isValidForm == true){

        // Check Email Exists
        var email_address = jQuery('#email_address').val();
        if(email_address !=''){
              
          var path = SURL + 'register/email-exist';
          jQuery.ajax({
            url: path,
            type: "POST",
            data: {'email': email_address},
            success: function(data){
              var obj = JSON.parse(data);
              
              if(obj.exist == 1){
                
                jQuery("#error_msg").html("Email you entered already exist please use another one!");
                jQuery("#error_msg").css({"color":"#a94442"});
                jQuery( "#email_address" ).focus();

                jQuery('#submit_step_1').val('0');

              } else {
                
                jQuery("#error_msg").html("");

                jQuery('#submit_step_1').val('1');

                // Open the popup on Validation success
                jQuery( "form#register_frm" )[0].submit();

              }
              
            } // success

          }); // jQuery.ajax

      } // if(is_valid_form == true)
  	
  	} // end if
		   
  } // function verify_email()

  /* ----------------------------------------- */
  // Start => function validate_credit_card_form()

  function validate_register_frm(){

      // Verify if the form is Validated
      jQuery('#register_frm').formValidation('validate');
      var isValidForm = jQuery('#register_frm').data('formValidation').isValid();

      var address_id  = jQuery('#address').val();
      var town_id = jQuery('#town').val();
      var county_id   = jQuery('#county').val();
      var postcode_id = jQuery('#postcode').val();  
      
      if(isValidForm == true){

        // Check Email Exists
        var email_address = jQuery('#email_address').val();
        if(email_address !=''){
              
          var path = SURL + 'register/email-exist';
          jQuery.ajax({
            url: path,
            type: "POST",
            data: {'email': email_address},
            success: function(data){
              var obj = JSON.parse(data);
              
              if(obj.exist == 1){
                
                jQuery("#error_msg").html("Email you entered already exist please use another one!");
                jQuery("#error_msg").css({"color":"#a94442"});
                jQuery( "#email_address" ).focus();

              } else {
                
                jQuery("#error_msg").html("");
                // Open the popup on Validation success
				  jQuery('#confirm_popup_trigger').click();
				  jQuery("form#register_frm" )[0].submit();

              }
              
            } // success

          }); // jQuery.ajax

      } // if(is_valid_form == true)
  	
  	} // end if
	  
  } // function validate_credit_card_form()
