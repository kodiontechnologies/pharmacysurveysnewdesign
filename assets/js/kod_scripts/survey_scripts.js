
//Draw pie chart options for Current Survey
function current_survey_drawPieChart(chart_title, data_arr, container_id){

	json_decode = JSON.parse(data_arr);	
	var data = google.visualization.arrayToDataTable(json_decode);
	
	var options = {
	  title: chart_title,
	  is3D: true,
	  legend: 'right',
	};

	var chart = new google.visualization.PieChart(document.getElementById(container_id));
	chart.draw(data, options);
	
}//end current_survey_drawPieChart(chart_title, data_arr, container_id)

//Initilized chart options for Current Survey on Click Event
function current_survey_initialize (){

	data_arr = jQuery('#survey_arr_json').val();
	chart_title = jQuery('#current_survey_title').val();

	//Reload Chart on Click Tabs
	jQuery('#current_survey_chart').click(function() {
		current_survey_drawPieChart(chart_title, data_arr , 'survey_piechart_3d');
	});

	//Recall function on page load
	current_survey_drawPieChart(chart_title, data_arr , 'survey_piechart_3d');
	
}//end function current_survey_initialize ()

// On keyup function to recalculate the remaining characters in SMS body
function sms_counter(){
  
  // Get instance of DOM object
  var el = jQuery('#sms_comment');

  // Get the max length allowed
  var ml = parseInt(jQuery(el).attr('maxlength'), 10);
  var length = jQuery(el).val().length;
  
  // Calculate remaining characters
  var msg = ml - length + ' characters of ' + ml + ' characters left';

  // Show the results to sms counter text
  jQuery('#sms_counter_txt').html(msg);

} // function sms_counter()


jQuery(document).ready(function(){
	
    // Get instance of DOM object
    var el = jQuery('#sms_comment');
	
	if(jQuery(document).find('#sms_comment').val() != undefined){
		
		// Get the max length allowed
		var ml = parseInt(jQuery(el).attr('maxlength'), 10);
		var length = jQuery(el).val().length;
		
		// Calculate remaining characters
		var msg = ml - length + ' characters of ' + ml + ' characters left';
	
		// Show the results to sms counter text
		jQuery('#sms_counter_txt').html(msg);
	}

	//This tab function is appplied on survey section
	jQuery('a[data-toggle="tab"]').on('shown.bs.tab', function ( e ) {	
	
		clicked_tab = (jQuery(this).attr('href'));
		
		if(clicked_tab == '#current_survey_tab')
			jQuery('#current_survey_chart').click();
	});

	//Verify if any readio button is selected while startring the survey
	jQuery("#start_survey_btn").click(function(){
	
		is_no_of_survey_checked = jQuery("input:radio[name='radio_no_of_survey']").is(":checked");
		
		if(!is_no_of_survey_checked){
			jQuery('#option_error').html('Error: Please select from one of the options above to proceed!');
			return false;
		}else{
			jQuery('#start_survey_frm').submit();
		}//end if(!$is_rechas_checked)
	});
	
	//Survey opening the charts statitics on selecting the survey from the list.
	jQuery('#survey_body').on('change', '#survey_list', function () {
		
		survey_id = jQuery(this).val();
		
		if(survey_id != ''){
			
			jQuery.ajax({
		
				type: "POST",
				url: SURL + "dashboard/survey-chart-container",
				data: {'survey_id' : survey_id },		
				beforeSend : function(result){
					
					jQuery("#overlay_survey").removeClass("hidden");
					jQuery('#current_survey_actions').addClass('hidden');
					//jQuery('#closed_survey_actions').addClass('hidden');
					jQuery('#survey_statistics_pane').addClass('hidden');
					jQuery('#survey_comment').addClass('hidden');
					
					jQuery('#closed_btn_container').addClass('hidden');
					jQuery('#finalize_report_container').addClass('hidden');
					
				},
				success: function(result){
					
					json_result = JSON.parse(result); 
					
					survey_title = json_result.survey_title;
					survey_year = survey_title.replace('Survey ','');
					
					jQuery("#overlay_survey").addClass("hidden");
					jQuery("#survey_chart_container").html(json_result.response_data);
					
					//To enable/ disable survey action buttons.
					if(json_result.show_survey_features){
						
						jQuery('#current_survey_actions').removeClass('hidden');
						
						if(json_result.survey_status == 0){
							jQuery('#closed_btn_container').removeClass('hidden');
							jQuery('#close_survey_id').val(json_result.survey_id);
						}
						
						if(json_result.survey_status == 1){
							jQuery('#finalize_report_container').removeClass('hidden');
							jQuery('#finish_survey_id').val(json_result.survey_id);
						}//end if(json_result.survey_status == 1)
						
						
					}else{
						jQuery('#current_survey_actions').addClass('hidden');
						jQuery('#closed_btn_container').addClass('hidden');
						jQuery('#finalize_report_container').addClass('hidden');
					}//end if(json_result.show_survey_features)
					
					//If All questions are commented do not shw the message alert box
					if(json_result.total_survey_comments == 28){
						//jQuery('#all_comments_notes').html('');
					}
					

					jQuery('#s_min_no_of_surveys_1').html(json_result.survey_stats.min_no_of_surveys);
					jQuery('#s_total_survey_completed_1').html(json_result.survey_stats.total_survey_completed);
					jQuery('#s_total_submitted_online_surveys_1').html(json_result.survey_stats.total_submitted_online_surveys);
					jQuery('#s_total_submitted_paper_surveys_1').html(json_result.survey_stats.total_submitted_paper_surveys);
					
					//jQuery('#closed_survey_actions').removeClass('hidden');
					jQuery('#survey_statistics_pane').removeClass('hidden');
					

					if(json_result.survey_status == 0){
						
						//jQuery('#current_survey_actions').removeClass('hidden');
						//jQuery('#s_min_no_of_surveys').html(json_result.survey_stats.min_no_of_surveys);
						//jQuery('#s_total_survey_completed').html(json_result.survey_stats.total_survey_completed);
						//jQuery('#s_total_submitted_online_surveys').html(json_result.survey_stats.total_submitted_online_surveys);
						//jQuery('#s_total_submitted_paper_surveys').html(json_result.survey_stats.total_submitted_paper_surveys);
						
						jQuery('#survey_comment').removeClass('hidden');
						
						if(json_result.survey_stats.remaining_surveys > 0){
							jQuery('#survey_comment').html('<p><i class="fa fa-info-circle"></i>  Your current survey for the year '+survey_year+' statistics.</p>');	
							
						}else{
							
							jQuery('#survey_comment').html('<p><i class="fa fa-info-circle"></i>  The minimum number of returned questionnaires required for '+survey_year+' have been successfully completed, now you may close the survey by clicking "Close Survey" button</p>');	
							
						}//end if(json_result.survey_stats.remaining_surveys > 0)
						
						
					}else if(json_result.survey_status == 1){
						
						jQuery('#survey_comment').removeClass('hidden');
						jQuery('#survey_comment').html('<p><i class="fa fa-info-circle"></i> The survey for '+survey_year+' is now closed. In order to finalise your results, you need to review and comment on each question and also select six questions from left menu and comment on each question. This will appear in your final report.</p>');
						
					}else if(json_result.survey_status == 3){
						
						//jQuery('#s_survey_status_1').html('Completed');
						jQuery('#s_min_no_of_surveys_1').html(json_result.survey_stats.min_no_of_surveys);
						jQuery('#s_total_survey_completed_1').html(json_result.survey_stats.total_survey_completed);
						jQuery('#s_total_submitted_online_surveys_1').html(json_result.survey_stats.total_submitted_online_surveys);
						jQuery('#s_total_submitted_paper_surveys_1').html(json_result.survey_stats.total_submitted_paper_surveys);
						
						//jQuery('#closed_survey_actions').removeClass('hidden');

					}//end 
				}
			}); 

		}//end if(survey_id != '')
	});
	
	jQuery('#survey_list option').eq(1).prop('selected', true).trigger('change'); //Select the most recent record from the survey list on load. When no survey in progress
	jQuery('#survey_list').change();

	//Thsi functon saves the Closing Comments of the survey
	jQuery('#survey_body').on('click', '.survey_chart_comments', function () {
		
		question_id = jQuery(this).attr('data-question');
		survey_id = jQuery(this).attr('data-survey');
		comments = jQuery('#comment_'+question_id).val();
		comments = comments.trim();
		
		if(comments == ''){
			
			jQuery("#success_msg_"+question_id).removeClass("hidden");
			jQuery("#success_msg_"+question_id).addClass("text-danger");
			jQuery("#success_msg_"+question_id).html('Please enter comments.');	

		}else{
			
			jQuery.ajax({
	
				type: "POST",
				url: SURL + "dashboard/save-survey-questions-comments",
				data: {'survey_id' : survey_id, 'question_id' : question_id, 'comments' : comments },		
				beforeSend : function(result){
					
					jQuery("#success_msg_"+question_id).addClass("hidden");
					jQuery("#success_msg_"+question_id).removeClass("text-danger");
					jQuery("#success_msg_"+question_id).removeClass("text-success");
				},
				success: function(result){
					
					
					json_result = JSON.parse(result);
					
					jQuery("#success_msg_"+question_id).removeClass("hidden");
					
					if(json_result.success == 1){
						
						jQuery("#success_msg_"+question_id).addClass("text-success");
						jQuery("#success_msg_"+question_id).html('Comments saved successfully.');
						
					}else{
						
						jQuery("#success_msg_"+question_id).addClass("text-danger");
						jQuery("#success_msg_"+question_id).html('Comments cannot save comments. Please try later or contact site admin');	
					}
				}
	
			});

		}
	})
	
	//Survey Finish Report Best and Bad Questions Drop Down Selection
	jQuery('.well_perf_slt').change(function(){
		
		selected_val = jQuery(this).val();
		selected_id = jQuery(this).attr('id');
		
		jQuery('#'+selected_id+'_comment').val('');
		
		success = 1;
		
		//Check if the same question is not already taken or in used. If Yes generate error
		jQuery(".well_perf_slt").each(function(){
			
			others_id = jQuery(this).attr('id'); 

			if(selected_id != others_id){
				
				others_val = jQuery('#'+others_id).val();
				
				if(others_val == selected_val){
					
					//alert('i am already selected');
					jQuery('#'+selected_id).val('');
					jQuery('#'+selected_id+'_comment').addClass('hidden');
					jQuery('#'+selected_id+'_comment').val('');
					
					if(selected_val!= ''){
						jQuery('#already_taken_btn').click();
						success = 0;
					}//end if(selected_val!= '')
					
					
				}//end if(others_val == selected_val)
				
			}//end if(selected_id != others_id)
		
			
		});		//end each
		
		if(selected_val!= '' && success == 1){
			
			jQuery('#'+selected_id+'_comment').removeClass('hidden');
			jQuery('#page_q_'+selected_val).click();
			
		}else{
			jQuery('#'+selected_id+'_comment').addClass('hidden');
			jQuery('#'+selected_id+'_comment').val('');
		}//end if(selected_val!= '')
		
	})

	//This function will make sure all six options in teh survey for well performed and bad performed is selcted. If Yes open confirmation boxes
	jQuery('#verify_finish_report').click(function(){

		success = 1;
		bad = 1;
		good = 1;
		//Checking if well performed and bad performed all 6 are selected
		jQuery(".well_perf_slt").each(function(){
			
			data_type = jQuery(this).attr('data-type'); 
			
			others_id = jQuery(this).attr('id'); 
			others_val = jQuery('#'+others_id).val();
			
			if(others_val == ''){
				
				if(data_type == 'bad')
					bad = 0;
				if(data_type == 'good')
					good = 0;
					
				success = 0;
			}//end if(others_val == '')
			
		});	//end each

		//Checking if well performed and bad performed all 6 have the comments filled
		jQuery(".well_perf_slt").each(function(){
			
			data_type = jQuery(this).attr('data-type'); 
			
			others_id = jQuery(this).attr('id')+'_comment'; 
			others_comments = jQuery('#'+others_id).val();
			others_comments = others_comments.trim();
			
			//console.log(others_comments+'----->'+others_id);
			
			if(others_comments == ''){
				bad = 0; 
				good = 0;
				success = 0;
			}//end if(others_comments == '')
			
		});	//end each
		
		survey_id = jQuery('#finish_survey_id').val();
		
		jQuery.ajax({
		
			type: "POST",
			url: SURL + "dashboard/survey-non-commented",
			data: {'survey_id' : survey_id, 'well_performed' : good, 'bad_performed' : bad  },		
			beforeSend : function(result){
				
			},
			success: function(result){
				
				json_decode = JSON.parse(result);	
				
				if(json_decode.success == 0){
					
					jQuery('#survey_close_err_msg').html(json_decode.message);
					jQuery('#finish_survey_err_btn').click();
				}else{
					jQuery('#finish_survey_btn').click();		
				}
				
				console.log(json_decode);
				
			}
		}); 
		
	})
	
	jQuery('#finish_survey_sbt').click(function(){
		jQuery('#survey_finish_frm').submit();
	});
	
	//Download Survey Poster
	jQuery('.download_poster_btn').click(function(){
		
		survey_id = jQuery(this).attr('data-survey');
		jQuery('#poster_frm_'+survey_id).submit();
		jQuery(this).attr('disabled',true);
		jQuery.fancybox.close();
			
	});
	
	// Patient Satisfaction Survey
	jQuery('.download_patient_satisfaction_survey_poster_btn').click(function(){
		
		survey_id = jQuery(this).attr('data-survey');
		jQuery('#patient_satis_survey_poster_frm_'+survey_id).submit();
		//jQuery(this).attr('disabled',true);
		jQuery.fancybox.close();
			
	});


	//Download Patient Survey Poster2
	jQuery('.download_patient_survey_poster_btn').click(function(){
		
		survey_id = jQuery(this).attr('data-survey');
		
		jQuery.ajax({
		
			type: "POST",
			url: SURL + "dashboard/generate-auto-report-charts",
			data: {'survey_id' : survey_id},		
			beforeSend : function(result){
				$('#autoload_charts').html('');
				
				$('.svg_gender_pie_chart').val('');
				$('.svg_age_pie_chart').val('');
				
			},
			success: function(result){
				
				json_decode = JSON.parse(result);	
				
				if(json_decode.error == 0){

					$('#autoload_charts').html(json_decode.data); //This will generate Auto Charts and used for new reports.
					setTimeout(function(){ 
						$('#patient_survey_poster_frm_'+survey_id)[0].submit(); 
						jQuery.fancybox.close(); 
						}, 1000);
					
				}
			}
		}); 
		
		//jQuery(this).attr('disabled',true);
		
			
	});

	//Download Survey DoH Letter
	jQuery('.download_doh_btn').click(function(){
		
		survey_id = jQuery(this).attr('data-survey');
		jQuery('#doh_frm_'+survey_id).submit();
		jQuery(this).attr('disabled',true);
		jQuery.fancybox.close();
			
	});
	
	//Download Survey Report Letter
	jQuery('.download_report_btn').click(function(){
		
		survey_id = jQuery(this).attr('data-survey');
		
		jQuery('#final_report_frm_'+survey_id).submit();
		jQuery(this).attr('disabled',true);
		jQuery.fancybox.close();
		
			
	});

	//Download Survey Forms Letter
	jQuery('.download_forms_btn').click(function(){
		
		survey_id = jQuery(this).attr('data-survey');
		jQuery('#survey_form_frm_'+survey_id).submit();
		jQuery(this).attr('disabled',true);
		jQuery.fancybox.close();
	});
	
	jQuery('.show_hide_textbox').click(function(){
		
		checked_val = jQuery(this).attr('value');

		if(checked_val == 4 || checked_val == 226){
			jQuery('#question_10_txtbox').removeClass('hidden');
		}else{
			jQuery('#question_10_txtbox').addClass('hidden');
			jQuery('#other_reason_txt').val('');
			
		}
	})
	
	jQuery("#submit_suv_btn").click(function() {
		
		jQuery('#common_err').addClass('hidden');
		
		var radio_groups = {}
		jQuery(":radio").each(function(){
			radio_groups[this.name] = true;
		})
		error = 0;
		first_err_id = '';
		for(group in radio_groups){
			
			if_checked = !!jQuery(":radio[name='"+group+"']:checked").length
			
			question_id = group.replace('[','');
			question_id = question_id.replace(']','');

			console.log(question_id);
			if(question_id !='q2'){
			
				if(!if_checked){
					
					jQuery('#err_'+question_id).removeClass('hidden');
					error = 1;
					
					if(first_err_id == '')
						first_err_id = '#err_'+question_id;
						//console.log(group+ (if_checked?' has checked radios':' does not have checked radios'));
						//console.log('#err_'+group);
				}else
					jQuery('#err_'+question_id).addClass('hidden');
				
			}else{
				console.log(question_id);
			}//end if(question_id !=2)
			
		}//end for(group in radio_groups)	
		
		if(error == 0){
			jQuery('#manage_survey_frm').submit();
		}else{
			jQuery('#common_err').removeClass('hidden');
			jQuery('html, body').animate({ scrollTop: jQuery(first_err_id).offset().top  }, 1000);
			
		}//end if(error == 0)

	})
	
	jQuery("#submit_incomplete_suv_btn").click(function() {
		
		jQuery('#in_common_err').addClass('hidden');
		
		var radio_groups = {}
		jQuery(":radio").each(function(){
			radio_groups[this.name] = true;
		})
		error = 0;
		first_err_id = '';
		for(group in radio_groups){
			
			if_checked = !!jQuery(":radio[name='"+group+"']:checked").length
			
			question_id = group.replace('[','');
			question_id = question_id.replace(']','');

			console.log(question_id);
			if(question_id !='q2'){
			
				if(!if_checked){
					
					jQuery('#in_err_'+question_id).removeClass('hidden');
					error = 1;
					
					if(first_err_id == '')
						first_err_id = '#in_err_'+question_id;
						//console.log(group+ (if_checked?' has checked radios':' does not have checked radios'));
						//console.log('#err_'+group);
				}else
					jQuery('#in_err_'+question_id).addClass('hidden');
				
			}else{
				console.log(question_id);
			}//end if(question_id !=2)
			
		}//end for(group in radio_groups)	
		
		if(error == 0){
			jQuery('#in_manage_survey_frm').submit();
		}else{
			jQuery('#in_common_err').removeClass('hidden');
			jQuery('html, body').animate({ scrollTop: jQuery(first_err_id).offset().top  }, 1000);
			
		}//end if(error == 0)

	})

	jQuery('#incomplete_survey_slt').change(function(){
		
		survey_id = jQuery(this).val();
		
		if(survey_id != ''){
		
			jQuery.ajax({
			
				type: "POST",
				url: SURL + "dashboard/get-incomplete-survey",
				data: {'survey_id' : survey_id},		
				beforeSend : function(result){
					
					jQuery('#btn_survey_stats').addClass('hidden');
					jQuery('#in_survey_txt').addClass('hidden');
				},
				success: function(result){
					
					json_result = JSON.parse(result);	
					
					if(json_result.success == 1){
						
						var min_no_of_surveys = parseInt(json_result.survey_data.min_no_of_surveys);
						var total_submitted_surveys = parseInt(json_result.total_submitted_surveys);
						
						if(total_submitted_surveys >= min_no_of_surveys)
							jQuery('#in_survey_txt').removeClass('hidden');
						
						jQuery('#min_survey_required').val(min_no_of_surveys);
						jQuery('#spn_min_survey_required').html(min_no_of_surveys);
						jQuery('#spn_survey_completed').html(total_submitted_surveys);
						
						jQuery('#incomplete_survey_id').val(survey_id);
						
						jQuery('#btn_survey_stats').removeClass('hidden');
						
					}//end if(json_decode.success == 1)
					
				}
			}); 
			
		}//end if(survey_id != '')
		
	});
	
	jQuery('#incomplete_survey_slt').change();

	jQuery('#min_survey_required').change(function(){
		
		min_survey_required = jQuery(this).val();
		survey_id = jQuery('#incomplete_survey_slt').val();
		
		
		if(survey_id != '' && min_survey_required!= ''){
		
			jQuery.ajax({
			
				type: "POST",
				url: SURL + "dashboard/update-min-survey-required",
				data: {'survey_id' : survey_id, 'min_survey_required': min_survey_required},		
				beforeSend : function(result){
					
					jQuery('#btn_survey_stats').addClass('hidden');
				},
				success: function(result){
					
					json_result = JSON.parse(result);	
					
					if(json_result.success == 1){
						
						//jQuery('#spn_min_survey_required').html(min_survey_required);
						//jQuery('#btn_survey_stats').removeClass('hidden');
						
						jQuery('#incomplete_survey_slt').change();
						
					}//end if(json_decode.success == 1)
					
				}
			}); 
			
		}//end if(survey_id != '')
		
	});


});
