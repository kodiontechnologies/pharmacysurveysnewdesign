jQuery(document).ready(function() { 

	jQuery('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		
		  clicked_tab = jQuery(this).attr('href');
		  
		  if(clicked_tab == '#card_tab'){
			  
			jQuery('#payment_method_radio').val('credit_card');  
			jQuery('#save_btn').val('Make Payment');
			jQuery('#save_btn').html('Make Payment');
			
			//jQuery('#save_btn').addClass('disabled');
			//jQuery('#save_btn').attr('disabled',true);
      
			jQuery('#payment_popup_message').text('Please wait while processing your payment.');
			
			if(!jQuery('#paypal_form_div').hasClass('hidden')){

				jQuery('#paypal_form_div').addClass('hidden');
				

			  } // if(jQuery('#paypal_form_div').hasClass())
	  
		  } else if(clicked_tab == '#paypal_tab') {
			  
			jQuery('#payment_method_radio').val('paypal');
			jQuery('#save_btn').val('Checkout with PayPal');
			jQuery('#save_btn').html('Checkout with PayPal');
			jQuery('#payment_popup_message').text('Please wait while redirecting to PayPal for payment.');
			
			jQuery('#card').val('');
			jQuery('#cvc').val('');
			jQuery('#expMonth').val('');
			jQuery('#expYear').val('');
			
			//jQuery('#save_btn').removeClass('disabled');
			//jQuery('#save_btn').attr('disabled',false);
			
			if(jQuery('#paypal_form_div').hasClass('hidden')){
				jQuery('#paypal_form_div').removeClass('hidden');

				
			  } // if(jQuery('#paypal_form_div').hasClass())
	  
		  }//end if(clicked_tab == '#card_tab')
	});
	jQuery('#payment_method_radio').val('credit_card');  
	jQuery('#save_btn').html('Make Payment');
	
	 
});

  /* ----------------------------------------- */
  // Start => function validate_credit_card_form()

  function validate_credit_card_form(){
    // Verify if the form is Validated
    jQuery('#credit_card_frm').formValidation('validate');
    var is_valid_form = jQuery('#credit_card_frm').data('formValidation').isValid();
    if(is_valid_form == true){
      // Open the popup on Validation success
      jQuery('#confirm_popup_trigger').click();
      jQuery( "form#credit_card_frm" )[0].submit();
    } // if(is_valid_form == true)
  } // function validate_credit_card_form()
  
