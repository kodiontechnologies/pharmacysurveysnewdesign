// Generic-Dynamic table instance by class
$('.dynamic-table').dataTable({
  "aaSorting": [],
  aoColumnDefs: [{  
    bSortable: false,
    aTargets: [ -1 ],
  }],
  
  'iDisplayLength': 50,
		"sPaginationType": "full_numbers",
		//"dom": 'T<"clear">lfrtip',
		"tableTools": {
			"sSwfPath": "",
			"aButtons": []
			
	}
});


$('.dynamic-table-help-videos').dataTable({
  "aaSorting": [],
  aoColumnDefs: [{  
    bSortable: false,
    aTargets: [ 0,1,2,3,4,5],
  }],
  
  'iDisplayLength': 50,
		"sPaginationType": "full_numbers",
		//"dom": 'T<"clear">lfrtip',
		"tableTools": {
			"sSwfPath": "",
			"aButtons": []
			
	}
});


// Generic-Dynamic table instance by class
$('.dynamic-table-pharmacies').dataTable({
  "aaSorting": [],
  aoColumnDefs: [{  
    bSortable: false,
    aTargets: [ 0,2,3,4,6,8 ],
  }],
  
  'iDisplayLength': 50,
		"sPaginationType": "full_numbers",
		//"dom": 'T<"clear">lfrtip',
		"tableTools": {
			"sSwfPath": "",
			"aButtons": []
			
	}
});

// Generic-Dynamic table instance by class
$('.dynamic-table-business').dataTable({
  "aaSorting": [],
  aoColumnDefs: [{  
    bSortable: false,
    aTargets: [ 0,2,4,6,7,8 ],
  }],
  
  'iDisplayLength': 50,
		"sPaginationType": "full_numbers",
		//"dom": 'T<"clear">lfrtip',
		"tableTools": {
			"sSwfPath": "",
			"aButtons": []
			
	}
});
