<!-- datepicker plugin -->

//link
$('.date-picker').datepicker({
	format: 'dd/mm/yyyy',
	autoclose: true,
	todayHighlight: true
})
//show datepicker when clicking on the icon
.next().on(ace.click_event, function(){
	$(this).prev().focus();
});

$('[id^="sch_date"]').datepicker({autoclose: true, orientation: 'right',format: 'dd/mm/yyyy'}).on('changeDate', function (ev) {
	
	c_date = $('[id^="sch_date"]').val();
	
	if(c_date != ''){
	
		new_format = date('D, d M, Y',strtotime(c_date));
		$('#date_link').text(new_format);

		c_date_arr = c_date.split("/"); 
		c_new_date = c_date_arr[2]+'-'+c_date_arr[1]+'-'+c_date_arr[0]
		
		mysql_format = date('Y-m-d',strtotime(c_new_date));
		$('#rxdate').attr('rel',mysql_format);
		
		$('#current_day').click();
		
	}// if(c_date != '')
});

$('#date_link').click(function() {
	$('[id^="sch_date"]').datepicker('show');
});
