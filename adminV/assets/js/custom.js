<!-- inline scripts related to this page --> 

<!-- inline scripts related to this page -->
jQuery(function($) {

//////////////////////////////////////////
			
$('#modeldrugsupplied > #modeldrugreceived > #modelstockcheck > #modelspillage > #modeleditentry > #modeldrugcollected > #modelnewscript > #modelnewuser > #modeladdnewpatient').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var recipient = button.data('whatever') // Extract info from data-* attributes
  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
  var modal = $(this)
  modal.find('.modal-title').text('New message to ' + recipient)
  modal.find('.modal-body input').val(recipient)
});

////////////////////
/// date picker ////
////////////////////
$( "#datepicker" ).datepicker({
	showOtherMonths: true,
	selectOtherMonths: false,
	//isRTL:true,
	changeMonth: true,
	changeYear: true,
	
	showButtonPanel: true,
	beforeShow: function() {
		//change button colors
		var datepicker = $(this).datepicker( "widget" );
		setTimeout(function(){
			var buttons = datepicker.find('.ui-datepicker-buttonpane')
			.find('button');
			buttons.eq(0).addClass('btn btn-xs');
			buttons.eq(1).addClass('btn btn-xs btn-success');
			buttons.wrapInner('<span class="bigger-110" />');
		}, 0);
	}

});
/// end date picker
});		

function showHideDiv(did)
{
	$('.pagedivs').addClass("hide").removeClass('show');
	$('#'+did).removeClass('hide').addClass('show');

}