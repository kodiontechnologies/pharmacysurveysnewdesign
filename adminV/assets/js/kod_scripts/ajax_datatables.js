$(document).ready(function(){
	//or change it into a date range picker
	$('.input-daterange').datepicker({
		format: 'dd/mm/yyyy',
		autoclose:true
		
		});
}); // $(document).ready(function()

///////////////////////////////////////////////
////////// Start show table functions /////////
///////////////////////////////////////////////
// Function to show Ajax based DataTable for : All Entries
function show_all_entries_table(tab){

	// Validate form
	$('#'+tab+'-form').formValidation('validate');

	// Validation for DateTime Picker [ date range ]
	$('.input-daterange').on('changeDate show', function(e) {
        // Revalidate the date when user change it
        $('#'+tab+'-form').bootstrapValidator('revalidateField', 'start_date');
        $('#'+tab+'-form').bootstrapValidator('revalidateField', 'end_date');
	});

	// Verify if any field has error
	var proceed = 1;
	var input_values = [];

	$("#"+tab+"-form :input").each(function(){
	
		var input = $(this); // This is the jquery object of the input
	 
		if(input.parent().hasClass("has-error")){
	 		proceed = 0;
	 	} // if(input.parent().parent().hasClass("has-error"))

	 	// Get all input field values
	 	if(input.attr("type") == 'hidden' ){
	 		if(input.val() != ''){
				input_values.push(input.val());
			} else {
				proceed = 0;

				// Highlight input field [ error ]
				input.parent().addClass("has-error");

				// Show error message
				if($('.help-block')){
	                $.each( $('.help-block'), function(k, error_el){
	                  if($(error_el).attr('data-fv-for') == 'drug_name_6'){
	                    if( $(error_el).attr('data-fv-validator') == 'notEmpty' ){
	                      $(error_el).show();
	                      $('#drug_name_6').val('');
	                    } else {
	                      $(error_el).hide();
	                    }
	                  } // if( $(error_el).attr('data-fv-for') == 'patient_name_1' )
	                });
              	} // if($('.help-block'))
				
			} // if(input.val())
		} // if(input.attr("type") == 'hidden' )

	});

	// Get User [ id OR any ]
	input_values.push($('#all_entreis_select_user').val());

	if( $('#all_entries_start_date').val() != '' && $('#all_entries_end_date').val() != '' ){
		input_values.push($('#all_entries_start_date').val());
		input_values.push($('#all_entries_end_date').val());
	} // if( $('#all_entries_start_date').val() != '' && $('#all_entries_end_date').val() != '' )

	// If form has no errors : proceed = 1
	if(proceed == 1){

		// Remove table hidden class : if table needs to be appeared onClick event
		$('#'+tab+'-table').removeClass("hidden");
		$('#'+tab+'-btn').removeClass("hidden");
		// Avoid re-Initialization
		$('#all-entries-table').DataTable().destroy();

		// DataTable : Initialise & Send Ajax call
		var table = $('#all-entries-table').DataTable({
		  
		  "processing": true,
		  "serverSide": true,
		  "searching" : false,
		  "columnDefs": [

		    // Column definitions : Make columns orderable, searchable => true, false
		    { "orderable": true,  "targets": 0 },
		    { "orderable": false, "targets": 1 },
		    { "orderable": false, "targets": 2 },
		    { "orderable": false, "targets": 3 },
		    { "orderable": false, "targets": 4 },
		    { "orderable": false, "targets": 5 },
		    { "orderable": false, "targets": 6 },
		    { "orderable": false, "targets": 7 },
		    { "orderable": false, "targets": 8 },
		    { "orderable": false, "targets": 9 }

		  ],

		  // Send ajax call to controller
		  "ajax": {

		    "url": SURL + "reports/all-entries-report",
		    "type" : "post",

		    "data": function(d) {

		    	// Extra attributes : custom input field data

		      	//d.patient_id = input_values[1]; // patient_id
		       d.drug_id = input_values[1]; // drug_id
		      	d.user_id = input_values[2]; // user_id [ 123 OR "any" ]
		      
		      	// Verify if the date rage is not null [ validate both fields ]
		      	if(input_values[3] != '' && input_values[4] != ''){
		      	
		      		d.start_date = input_values[3]; // start_date
		      		d.end_date = input_values[4]; // end_date

		      	} // if(input_values[3] != '' && input_values[4] != '')

		    } // data
		    
		  } // ajax

		}); // End -> DataTable instance

	} // if(process == 1)

} // End => function show_all_entries_table(tab)

// Function to show Ajax based DataTable for : Supplied Log
function show_supplied_log_table(tab){

	// Validate form
	$('#'+tab+'-form').formValidation('validate');
	
	
	var patient_id = '';
	var drug_id = '';

	// Validation for DateTime Picker [ date range ]
	$('.input-daterange').on('changeDate show', function(e) {
        // Revalidate the date when user change it
        $('#'+tab+'-form').bootstrapValidator('revalidateField', 'start_date');
        $('#'+tab+'-form').bootstrapValidator('revalidateField', 'end_date');
	});

	// Verify if any field has error
	var proceed = 1;

	$("#"+tab+"-form :input").each(function(){
	
		var input = $(this); // This is the jquery object of the input
		if(input.parent().hasClass("has-error")){
	 		proceed = 0;
	 	} // if(input.parent().parent().hasClass("has-error"))

	 	// Get all input field values
	 	if(input.attr("type") == 'hidden' ){
			
	 		if(input.val() != ''){
				
				if(input.attr("name") == 'patient_id'){
					 patient_id = input.val();
				}
				
				if(input.attr("name") == 'drug_id'){
					 drug_id = input.val();
				}
				
			} else {

				// input.parent().addClass("has-error");

				// this field ( hidden value ) field is null [ in loop ]

				// if(input.name == '')
				if(input.attr("name") == 'patient_id'){
					
					$('#'+tab+'-form').find('#patient_name_2').val('');

					if( $('#'+tab+'-form').find('#drug_id').val() == '' ){

						proceed = 0;
						input.parent().addClass("has-error");
						$('#drug_name_1').parent().addClass("has-error");

						if( $('#supplied_log_drug_error_msg').text() == '' || $('#supplied_log_drug_error_msg').text() == undefined ){
							$('#drug_name_1').after('<small id="supplied_log_drug_error_msg" style="color:#d16e6c;">Please fill out this field</small>');
						}

						if( $('#supplied_log_supplier_error_msg').text() == '' || $('#supplied_log_supplier_error_msg').text() == undefined ){
							input.after('<small id="supplied_log_supplier_error_msg" style="color:#d16e6c;">Please fill out this field</small>');
						}

					} else {
						
						if( $('#supplied_log_drug_error_msg').text() != '' || $('#supplied_log_drug_error_msg').text() != undefined ){
							$('#supplied_log_drug_error_msg').remove();
						}

						if( $('#supplied_log_supplier_error_msg').text() != '' || $('#supplied_log_supplier_error_msg').text() != undefined ){
							$('#supplied_log_supplier_error_msg').remove();
						}

						proceed = 1;
						if( input.parent().hasClass("has-error") ){
							input.parent().removeClass("has-error");
						}
						if( $('#drug_name_1').parent().hasClass("has-error") ){
							$('#drug_name_1').parent().removeClass("has-error");
						}
					}

				} else {
					$('#'+tab+'-form').find('#drug_name_1').val('');
					if( $('#'+tab+'-form').find('#patient_id').val() == '' ){

						proceed = 0;
						input.parent().addClass("has-error");
						$('#patient_id').parent().addClass("has-error");

						if( $('#supplied_log_drug_error_msg').text() == '' || $('#supplied_log_drug_error_msg').text() == undefined ){
							$('#patient_id').after('<small id="supplied_log_drug_error_msg" style="color:#d16e6c;">Please fill out this field</small>');
						}

						if( $('#supplied_log_supplier_error_msg').text() == '' || $('#supplied_log_supplier_error_msg').text() == undefined ){
							input.after('<small id="supplied_log_supplier_error_msg" style="color:#d16e6c;">Please fill out this field</small>');
						}

					} else {
						
						if( $('#supplied_log_drug_error_msg').text() != '' || $('#supplied_log_drug_error_msg').text() != undefined ){
							$('#supplied_log_drug_error_msg').remove();
						}

						if( $('#supplied_log_supplier_error_msg').text() != '' || $('#supplied_log_supplier_error_msg').text() != undefined ){
							$('#supplied_log_supplier_error_msg').remove();
						}

						proceed = 1;
						if( input.parent().hasClass("has-error") ){
							input.parent().removeClass("has-error");
						}

						if( $('#patient_id').parent().hasClass("has-error") ){
							$('#patient_id').parent().removeClass("has-error");
						}

					}

				} // if(input.attr("name") == 'supplier_id')
				
				// proceed = 0;

				// Show error message
				if($('.help-block')){
	                $.each( $('.help-block'), function(k, error_el){
	                  	if($(error_el).attr('data-fv-for') == 'patient_name' || $(error_el).attr('data-fv-for') == 'drug_name_1'){
		                    if( $(error_el).attr('data-fv-validator') == 'notEmpty' ){
		                      if(input.attr("id") == 'patient_id'){
		                      	$('#patient_name_2').val('');
		                      } else if(input.attr("id") == 'drug_id'){
		                      	$('#drug_name_1').val('');
		                      } // if(input.attr("id") == 'patient_id')
		                      
		                      if(input.attr("id") == 'patient_id' && $(error_el).attr('data-fv-for') == 'patient_name'){
								$(error_el).show();
		                      } else if(input.attr("id") == 'drug_id' && $(error_el).attr('data-fv-for') == 'drug_name_1'){
		                      	$(error_el).show();
		                      } // if(input.attr("id") == 'patient_id' && $(error_el).attr('data-fv-for') == 'patient_name')

		                    } else {
		                      $(error_el).hide();
		                    } // if( $(error_el).attr('data-fv-validator') == 'notEmpty' )
	                	} // if( $(error_el).attr('data-fv-for') == 'patient_name_1' )
	                });
              	} // if($('.help-block'))

			} // if(input.val())

		} // if(input.attr("type") == 'hidden' )

	}); // $("#"+tab+"-form :input").each(function()

	if( $('#'+tab+'-form').find('#drug_id').val() != '' && $('#'+tab+'-form').find('#patient_id').val() != '' ){

		if( $('#supplied_log_drug_error_msg').text() != '' || $('#supplied_log_drug_error_msg').text() != undefined ){
			$('#supplied_log_drug_error_msg').remove();
		}

		if( $('#supplied_log_supplier_error_msg').text() != '' || $('#supplied_log_supplier_error_msg').text() != undefined ){
			$('#supplied_log_supplier_error_msg').remove();
		}

	}

	// If form has no errors : proceed = 1
	if(proceed == 1){

		// Remove table hidden class : if table needs to be appeared onClick event
		$('#'+tab+'-table').removeClass("hidden");
		$('#'+tab+'-btn').removeClass("hidden");

		// Avoid re-Initialization
		$('#'+tab+'-table').DataTable().destroy();
	

		// DataTable : Initialise & Send Ajax call
		var table = $('#'+tab+'-table').DataTable({

			"processing": true,
		  	"serverSide": true,
		  	"searching" : false,
		  	"order": [ 0, "desc" ], // Set default order to DESC of colums 0
		  	"columnDefs": [

			// Column definitions : Make columns orderable, searchable => true, false
			{ "orderable": true,  "targets": 0 },
		    { "orderable": false, "targets": 1 },
		    { "orderable": false, "targets": 2 },
		    { "orderable": false, "targets": 3 },
		    { "orderable": false, "targets": 4 },
		    { "orderable": false, "targets": 5 }

		  ],

		  // Send ajax call to controller
		  "ajax": {
		    
		    "url": SURL + "reports/supplied-log-report",
		    "type" : "post",
		    "data": function(d) {

		      // Extra attributes : custom input field data

		      d.patient_id = patient_id; // patient_id
		      d.drug_id = drug_id; // drug_id
		      d.user_id = $('#supplied_select_user').val(); // user_id [ 123 OR "any" ]
		      
		      // Verify if the date rage is not null [ validate both fields ]
		      if($('#supplied_start_date').val() != '' && $('#supplied_end_date').val() != ''){
		      	
		      	d.start_date = $('#supplied_start_date').val(); // start_date
		      	d.end_date   = $('#supplied_end_date').val(); // end_date

		      } // if(input_values[3] != '' && input_values[4] != '')
		      
		    } // data

		  } // ajax

		}); // End -> DataTable instance

	} // if(process == 1)

} // End => function show_supplied_log_table(tab)

// Function to show Ajax based DataTable for : Supplied Log
function show_received_log_table(tab){

		// Validate form
	$('#'+tab+'-form').formValidation('validate');
	
	var supplier_id = '';
	var drug_id = '';

	// Validation for DateTime Picker [ date range ]
	$('.input-daterange').on('changeDate show', function(e) {
        // Revalidate the date when user change it
        $('#'+tab+'-form').bootstrapValidator('revalidateField', 'start_date');
        $('#'+tab+'-form').bootstrapValidator('revalidateField', 'end_date');
	});

	// Verify if any field has error
	var proceed = 1;

	$("#"+tab+"-form :input").each(function(){
	
		var input = $(this); // This is the jquery object of the input
	 
		if(input.parent().hasClass("has-error")){
	 		proceed = 0;
	 	} // if(input.parent().parent().hasClass("has-error"))

	 	// Get all input field values
	 	if(input.attr("type") == 'hidden' ){
	 		if(input.val() != ''){
				
				if(input.attr("name") == 'supplier_id'){
					 supplier_id = input.val();
				}
				
				if(input.attr("name") == 'drug_id'){
					 drug_id = input.val();
				}
			} else {
				// this field ( hidden value ) field is null [ in loop ]

				// if(input.name == '')
				if(input.attr("name") == 'supplier_id'){
					
					$('#'+tab+'-form').find('#supplier_name_1').val('');

					if( $('#'+tab+'-form').find('#drug_id').val() == '' ){
						
						//$('#'+tab+'-form').find('#drug_name_2').val('');
						proceed = 0;
						input.parent().parent().addClass("has-error");
						$('#drug_name_2').parent().addClass("has-error");

						if( $('#received_log_drug_error_msg').text() == '' || $('#received_log_drug_error_msg').text() == undefined ){
							$('#drug_name_2').after('<small id="received_log_drug_error_msg" style="color:#d16e6c;">Please fill out this field</small>');
						}

						if( $('#received_log_supplier_error_msg').text() == '' || $('#received_log_supplier_error_msg').text() == undefined ){
							input.after('<small id="received_log_supplier_error_msg" style="color:#d16e6c;">Please fill out this field</small>');
						}

					} else {
						
						if( $('#received_log_drug_error_msg').text() != '' || $('#received_log_drug_error_msg').text() != undefined ){
							$('#received_log_drug_error_msg').remove();
						}

						if( $('#received_log_supplier_error_msg').text() != '' || $('#received_log_supplier_error_msg').text() != undefined ){
							$('#received_log_supplier_error_msg').remove();
						}

						proceed = 1;
						if( input.parent().parent().hasClass("has-error") ){
							input.parent().parent().removeClass("has-error");
						}
						if( $('#drug_name_2').parent().hasClass("has-error") ){
							$('#drug_name_2').parent().removeClass("has-error");
						}
					}

				} else {

					$('#'+tab+'-form').find('#drug_name_2').val('');
					
					if( $('#'+tab+'-form').find('#supplier_id_1').val() == '' ){

						// $('#'+tab+'-form').find('#supplier_name_1').val('');
						proceed = 0;
						input.parent().addClass("has-error");
						$('#supplier_name_1').parent().parent().addClass("has-error");

					} else {
						
						if( $('#received_log_drug_error_msg').text() != '' || $('#received_log_drug_error_msg').text() != undefined ){
							$('#received_log_drug_error_msg').remove();
						}

						if( $('#received_log_supplier_error_msg').text() != '' || $('#received_log_supplier_error_msg').text() != undefined ){
							$('#received_log_supplier_error_msg').remove();
						}

						proceed = 1;
						if( input.parent().hasClass("has-error") ){
							input.parent().removeClass("has-error");
						}

						if( $('#supplier_name_1').parent().parent().hasClass("has-error") ){
							$('#supplier_name_1').parent().parent().removeClass("has-error");
						}

					}

				} // if(input.attr("name") == 'supplier_id')

				// proceed = 0;

				// Show error message
				if($('.help-block')){
	                $.each( $('.help-block'), function(k, error_el){
	                  	if($(error_el).attr('data-fv-for') == 'supplier_name' || $(error_el).attr('data-fv-for') == 'drug_name_2'){
		                    if( $(error_el).attr('data-fv-validator') == 'notEmpty' ){
		                      if(input.attr("id") == 'supplier_id_1'){
		                      	$('#supplier_name_1').val('');
		                      } else if(input.attr("id") == 'drug_id'){
		                      	$('#drug_name_2').val('');
		                      } // if(input.attr("id") == 'supplier_id_1')
		                      
		                      if(input.attr("id") == 'supplier_id_1' && $(error_el).attr('data-fv-for') == 'supplier_name'){
								$(error_el).show();
		                      } else if(input.attr("id") == 'drug_id' && $(error_el).attr('data-fv-for') == 'drug_name_2'){
		                      	$(error_el).show();
		                      } // if(input.attr("id") == 'supplier_id_1' && $(error_el).attr('data-fv-for') == 'patient_name')

		                    } else {
		                      $(error_el).hide();
		                    } // if( $(error_el).attr('data-fv-validator') == 'notEmpty' )
	                	} // if( $(error_el).attr('data-fv-for') == 'patient_name_1' )
	                });
              	} // if($('.help-block'))

			} // if(input.val())
		} // if(input.attr("type") == 'hidden' )

		// New script for hidden fields for this function only [ tab specific ]
		/*
		if( $('#'+tab+'-form').find('#supplier_id_1').val() != ''|| $('#'+tab+'-form').find('#drug_id').val() != '' ){
			proceed = 1;
		}
		*/

	});

	if( $('#'+tab+'-form').find('#drug_id').val() != '' && $('#'+tab+'-form').find('#supplier_id_1').val() != '' ){

		if( $('#received_log_drug_error_msg').text() != '' || $('#received_log_drug_error_msg').text() != undefined ){
			$('#received_log_drug_error_msg').remove();
		}

		if( $('#received_log_supplier_error_msg').text() != '' || $('#received_log_supplier_error_msg').text() != undefined ){
			$('#received_log_supplier_error_msg').remove();
		}

	}


	// If form has no errors : proceed = 1
	if(proceed == 1){

		// Remove table hidden class : if table needs to be appeared onClick event
		$('#'+tab+'-table').removeClass("hidden");
		$('#'+tab+'-btn').removeClass("hidden");
		// Avoid re-Initialization
		$('#'+tab+'-table').DataTable().destroy();


		// DataTable : Initialise & Send Ajax call
		var table = $('#'+tab+'-table').DataTable({

		  "processing": true,
		  "serverSide": true,
		  "searching" : false,
		  "order": [ 0, "desc" ], // Set default order to DESC of colums 0
		  "columnDefs": [

		    // Column definitions : Make columns orderable, searchable => true, false
		    { "orderable": true,  "targets": 0 },
		    { "orderable": false, "targets": 1 },
		    { "orderable": false, "targets": 2 },
		    { "orderable": false, "targets": 4 }
		    
		  ],

		  // Send ajax call to controller
		  "ajax": {
		    
		    "url": SURL + "reports/received-log-report",
		    "type" : "post",
		    "data": function(d) {

		      // Extra attributes : custom input field data

		      d.supplier_id = supplier_id; // supplier_id
		      d.drug_id = drug_id; // drug_id
		      d.user_id = $('#received_log_select_user').val(); // user_id [ 123 OR "any" ]
		      
		      // Verify if the date rage is not null [ validate both fields ]
		      if($('#received_log_start_date').val() != '' && $('#received_log_end_date').val() != ''){
		      	
		      	d.start_date = $('#received_log_start_date').val(); // start_date
		      	d.end_date = $('#received_log_end_date').val(); // end_date

		      } // if(input_values[3] != '' && input_values[4] != '')

		    } // data
		   
		  } // ajax

		}); // End -> DataTable instance

	} // if(process == 1)

} // End => function show_received_log_table(tab)

// Function to show Ajax based DataTable for : Stock Check Log
function show_stock_check_log_table(tab){

	// Validate form
	$('#'+tab+'-form').formValidation('validate');

	// Validation for DateTime Picker [ date range ]
	$('.input-daterange').on('changeDate show', function(e) {
        // Revalidate the date when user change it
        $('#'+tab+'-form').bootstrapValidator('revalidateField', 'start_date');
        $('#'+tab+'-form').bootstrapValidator('revalidateField', 'end_date');
	});

	// Verify if any field has error
	var proceed = 1;
	var input_values = [];

	$("#"+tab+"-form :input").each(function(){
	
		var input = $(this); // This is the jquery object of the input
	 
		if(input.parent().hasClass("has-error")){
	 		proceed = 0;
	 	} // if(input.parent().parent().hasClass("has-error"))

	 	// Get all input field values
	 	if(input.attr("type") == 'hidden' ){
	 		if(input.val()){
				input_values.push(input.val());
			} else {
				input.parent().addClass("has-error");
				proceed = 0;

				// Show error message
				if($('.help-block')){
	                $.each( $('.help-block'), function(k, error_el){
	                  if($(error_el).attr('data-fv-for') == 'drug_name_3'){
	                    if( $(error_el).attr('data-fv-validator') == 'notEmpty' ){
	                      $(error_el).show();
	                      $('#drug_name_3').val('');
	                    } else {
	                      $(error_el).hide();
	                    }
	                  } // if( $(error_el).attr('data-fv-for') == 'patient_name_1' )
	                });
              	} // if($('.help-block'))

			} // if(input.val())
		} // if(input.attr("type") == 'hidden' )

	});

	// Get User [ id OR any ]
	input_values.push($('#stock_check_log_select_user').val());

	if( $('#stock_check_log_start_date').val() != '' && $('#stock_check_log_end_date').val() != '' ){
		input_values.push($('#stock_check_log_start_date').val());
		input_values.push($('#stock_check_log_end_date').val());
	} // if( $('#stock_check_log_start_date').val() != '' && $('#stock_check_log_end_date').val() != '' )
	
	// If form has no errors : proceed = 1
	if(proceed == 1){

		// Remove table hidden class : if table needs to be appeared onClick event
		$('#'+tab+'-table').removeClass("hidden");
		$('#'+tab+'-btn').removeClass("hidden");
		// Avoid re-Initialization
		$('#'+tab+'-table').DataTable().destroy();

		// DataTable : Initialise & Send Ajax call
		var table = $('#'+tab+'-table').DataTable({

		  "processing": true,
		  "serverSide": true,
		  "searching" : false,
		  "order" : [0, 'desc'],
		  "columnDefs": [

		    // Column definitions : Make columns orderable, searchable => true, false
		    { "orderable": true,  "targets": 0 },
		    { "orderable": false, "targets": 1 },
		    { "orderable": false, "targets": 2 },
		    { "orderable": false, "targets": 3 },
		    { "orderable": false, "targets": 4 }

		  ],

		  // Send ajax call to controller
		  "ajax": {
		    
		    "url": SURL + "reports/stock-check-log-report",
		    "type" : "post",

		    "data": function(d) {

		    	// Extra attributes : custom input field data
				d.drug_id = input_values[1]; // patient_id
				d.user_id = input_values[2]; // user_id [ 123 OR "any" ]

				// Verify if the date rage is not null [ validate both fields ]
				if(input_values[3] != '' && input_values[4] != ''){
					
					d.start_date = input_values[3]; // start_date
					d.end_date = input_values[4]; // end_date

				} // if(input_values[2] != '' && input_values[3] != '')
		      
		    } // data
		    
		  } // ajax

		}); // End -> DataTable instance

	} // if(process == 1)

} // End => function show_stock_check_log_table(tab)

// Function to show Ajax based DataTable for : Stock Check Log
function show_adjust_stock_log_table(tab){

	// Validate form
	$('#'+tab+'-form').formValidation('validate');

	// Validation for DateTime Picker [ date range ]
	$('.input-daterange').on('changeDate show', function(e) {
        // Revalidate the date when user change it
        $('#'+tab+'-form').bootstrapValidator('revalidateField', 'start_date');
        $('#'+tab+'-form').bootstrapValidator('revalidateField', 'end_date');
	});

	// Verify if any field has error
	var proceed = 1;
	var input_values = [];

	$("#"+tab+"-form :input").each(function(){
	
		var input = $(this); // This is the jquery object of the input
	 
		if(input.parent().hasClass("has-error")){
	 		proceed = 0;
	 	} // if(input.parent().parent().hasClass("has-error"))

	 	// Get all input field values
	 	if(input.attr("type") == 'hidden' ){
	 		if(input.val()){
				input_values.push(input.val());
			} else {
				input.parent().addClass("has-error");
				proceed = 0;

				// Show error message
				if($('.help-block')){
	                $.each( $('.help-block'), function(k, error_el){
	                  if($(error_el).attr('data-fv-for') == 'drug_name_4'){
	                    if( $(error_el).attr('data-fv-validator') == 'notEmpty' ){
	                      $(error_el).show();
	                      $('#drug_name_4').val('');
	                    } else {
	                      $(error_el).hide();
	                    }
	                  } // if( $(error_el).attr('data-fv-for') == 'patient_name_1' )
	                });
              	} // if($('.help-block'))

			} // if(input.val())
		} // if(input.attr("type") == 'hidden' )

	}); // $("#"+tab+"-form :input").each(function()

	// Get User [ id OR any ]
	input_values.push($('#adjust_select_user').val());

	if( $('#adjust_start_date').val() != '' && $('#adjust_end_date').val() != '' ){
		input_values.push($('#adjust_start_date').val());
		input_values.push($('#adjust_end_date').val());
	} // if( $('#adjust_start_date').val() != '' && $('#adjust_end_date').val() != '' )
	
	// If form has no errors : proceed = 1
	if(proceed == 1){

		// Remove table hidden class : if table needs to be appeared onClick event
		$('#'+tab+'-table').removeClass("hidden");
		$('#'+tab+'-btn').removeClass("hidden");
		// Avoid re-Initialization
		$('#'+tab+'-table').DataTable().destroy();


		// DataTable : Initialise & Send Ajax call
		var table = $('#'+tab+'-table').DataTable({

		  "processing": true,
		  "serverSide": true,
		  "searching" : false,
		  "columnDefs": [

		    // Column definitions : Make columns orderable, searchable => true, false
		    { "orderable": true, "width": "11%", "targets": 0 },
		    { "orderable": false, "width": "26%", "targets": 1 },
		    { "orderable": false, "width": "16%", "targets": 2 },
		    { "orderable": false, "width": "16%", "targets": 3 },
		    { "orderable": false, "width": "9%", "targets": 4 },
		    { "orderable": false, "width": "9%", "targets": 5 },
		    { "orderable": false, "width": "15%", "targets": 6 }

		  ],

		  // Send ajax call to controller
		  "ajax": {
		    
		    "url": SURL + "reports/adjust-stock-log-report",
		    "type" : "post",

		    "data": function(d) {

			    // Extra attributes : custom input field data
				d.drug_id = input_values[1]; // patient_id
				d.user_id = input_values[2]; // user_id [ 123 OR "any" ]

				// Verify if the date rage is not null [ validate both fields ]
				if(input_values[3] != '' && input_values[4] != ''){
					
					d.start_date = input_values[3]; // start_date
					d.end_date = input_values[4]; // end_date

				} // if(input_values[2] != '' && input_values[3] != '')
		      
		    } // data

		  } // ajax

		}); // End -> DataTable instance

	} // if(process == 1)

} // End => function show_adjust_stock_log_table(tab)

//////////////////////////////////////////////////////

// Function to show Ajax based DataTable for : Destruction Log
function show_destruction_log_table(tab){

	// Validate form
	$('#'+tab+'-form').formValidation('validate');

	// Validation for DateTime Picker [ date range ]
	$('.input-daterange').on('changeDate show', function(e) {
        // Revalidate the date when user change it
        $('#'+tab+'-form').bootstrapValidator('revalidateField', 'start_date');
        $('#'+tab+'-form').bootstrapValidator('revalidateField', 'end_date');
	});

	// Verify if any field has error
	var proceed = 1;
	var input_values = [];

	$("#"+tab+"-form :input").each(function(){
	
		var input = $(this); // This is the jquery object of the input
	 
		if(input.parent().hasClass("has-error")){
	 		proceed = 0;
	 	} // if(input.parent().parent().hasClass("has-error"))

	 	// Get all input field values
	 	if(input.attr("type") == 'hidden' ){
	 		if(input.val()){
				input_values.push(input.val());
			} else {
				input.parent().addClass("has-error");
				proceed = 0;

				// Show error message
				if($('.help-block')){
	                $.each( $('.help-block'), function(k, error_el){
	                  if($(error_el).attr('data-fv-for') == 'drug_name_5'){
	                    if( $(error_el).attr('data-fv-validator') == 'notEmpty' ){
	                      $(error_el).show();
	                      $('#drug_name_5').val('');
	                    } else {
	                      $(error_el).hide();
	                    }
	                  } // if( $(error_el).attr('data-fv-for') == 'patient_name_1' )
	                });
              	} // if($('.help-block'))

			} // if(input.val())
		} // if(input.attr("type") == 'hidden' )

	}); // $("#"+tab+"-form :input").each(function()

	// Get User [ id OR any ]
	input_values.push($('#destruction_log_select_user').val());

	if( $('#destruction_log_start_date').val() != '' && $('#destruction_log_end_date').val() != '' ){
		input_values.push($('#destruction_log_start_date').val());
		input_values.push($('#destruction_log_end_date').val());
	} // if( $('#destruction_log_start_date').val() != '' && $('#destruction_log_end_date').val() != '' )
	
	// If form has no errors : proceed = 1
	
	if(proceed == 1){

		var destruction_type = $("#"+tab+"-form :input[type=radio]:checked").val();
		var table_head = '';
		
		if(destruction_type == 'expired') {
		
			$('#destruction_radio_id').val('expired');
			table_head = '<tr><th>Expiry Date</th><th>Drug</th><th>Quantity</th><th>Destruction Date</th><th>Destroyed By</th><th>Witnessed By</th></tr>';

		} else if(destruction_type == 'obsolete') {

			$('#destruction_radio_id').val('obsolete');
			table_head = '<tr><th>Obsolete Date</th><th>Drug</th><th>Quantity</th><th>Destruction Date</th><th>Destroyed By</th><th>Witnessed By</th></tr>';

		} else if(destruction_type == 'return') {

			$('#destruction_radio_id').val('return');
			table_head = '<tr><th>Date Returned</th><th>Patient Name/Address</th><th>Person Returning</th><th>Drug Class/Brand/Form/Strength</th><th>Qauntity</th><th>Destroyed By/Date</th><th>Witnessed By</th><th>Initials</th></tr>';

		} // if(destruction_type == '')

		if(destruction_type == 'return'){

			if( $('#return-table-div').hasClass("hidden") )
				$('#return-table-div').removeClass("hidden");
			// if( $('#return-table-div').hasClass("hidden") )

			if( !$('#'+tab+'-table').hasClass("hidden") ){
				if( $.fn.DataTable.isDataTable( '#'+tab+'-table' )){
					$('#'+tab+'-table').DataTable().destroy();
				} // if( $.fn.DataTable.isDataTable( '#'+tab+'-table' ))
				$('#'+tab+'-table').addClass("hidden");
			} // if( !$('#'+tab+'-table').hasClass("dynamic-table") )

			if( !$('#'+tab+'-table-return').hasClass("dynamic-table") )
				$('#'+tab+'-table-return').addClass("dynamic-table");
			// if( !$('#'+tab+'-table').hasClass("dynamic-table") )

			$('#'+tab+'-table-thead-return').html(table_head);

			// Remove table hidden class : if table needs to be appeared onClick event
			$('#'+tab+'-table-return').removeClass("hidden");
			$('#'+tab+'-btn').removeClass("hidden");

			// Avoid re-Initialization
			$('#'+tab+'-table-return').DataTable().destroy();
			//$('#'+tab+'-table').dataTable().fnDestroy();

			// DataTable : Initialise & Send Ajax call
			var table = $('#'+tab+'-table-return').DataTable({

			  "processing": true,
			  "serverSide": true,
			  "searching" : false,
			  "order" : [0, 'desc'],
			  "columnDefs": [

			    // Column definitions : Make columns orderable, searchable => true, false
			    { "orderable": true,  "targets": 0 },
			    { "orderable": false, "targets": 1 },
			    { "orderable": false, "targets": 2 },
			    { "orderable": false, "targets": 3 },
			    { "orderable": false, "targets": 4 },
			    { "orderable": false, "targets": 5 },
			    { "orderable": false, "targets": 6 },
			    { "orderable": false, "targets": 7 },

			  ],

			  // Send ajax call to controller
			  "ajax": {
			    
			    "url": SURL + "reports/destruction-log-report",
			    "type" : "post",

			    "data": function(d) {

			    	// Extra attributes : custom input field data

					d.drug_id = input_values[1]; // patient_id

					d.user_id = input_values[2]; // user_id [ 123 OR "any" ]

					// Verify if the date rage is not null [ validate both fields ]
					if(input_values[3] != '' && input_values[4] != ''){
						
						d.start_date = input_values[3]; // start_date

						d.end_date = input_values[4]; // end_date
						
						d.destruction_type = destruction_type; // [ expired | Obsolete | Return ]

					} // if(input_values[2] != '' && input_values[3] != '')
			      
			    } // data
			    
			  } // ajax

			}); // End -> DataTable instance

		} else {

			if( !$('#return-table-div').hasClass("hidden") )
				$('#return-table-div').addClass("hidden");
			// if( $('#return-table-div').hasClass("hidden") )

			if( !$('#'+tab+'-table').hasClass("dynamic-table") ){
				$('#'+tab+'-table').addClass("dynamic-table");
			} // if( !$('#'+tab+'-table').hasClass("dynamic-table") )

			$('#'+tab+'-table-thead').html(table_head);

			// Remove table hidden class : if table needs to be appeared onClick event
			$('#'+tab+'-table').removeClass("hidden");
			$('#'+tab+'-btn').removeClass("hidden");

			// Avoid re-Initialization
			$('#'+tab+'-table').DataTable().destroy();
			//$('#'+tab+'-table').dataTable().fnDestroy();

			// DataTable : Initialise & Send Ajax call
			var table = $('#'+tab+'-table').DataTable({

			  "processing": true,
			  "serverSide": true,
			  "searching" : false,
			  "order" : [0, 'desc'],
			  "columnDefs": [

			    // Column definitions : Make columns orderable, searchable => true, false
			    { "orderable": true,  "targets": 0 },
			    { "orderable": false, "targets": 1 },
			    { "orderable": false, "targets": 2 },
			    { "orderable": false, "targets": 3 },
			    { "orderable": false, "targets": 4 },
			    { "orderable": false, "targets": 5 }

			  ],

			  // Send ajax call to controller
			  "ajax": {
			    
			    "url": SURL + "reports/destruction-log-report",
			    "type" : "post",

			    "data": function(d) {

			    	// Extra attributes : custom input field data

					d.drug_id = input_values[1]; // patient_id

					d.user_id = input_values[2]; // user_id [ 123 OR "any" ]

					// Verify if the date rage is not null [ validate both fields ]
					if(input_values[3] != '' && input_values[4] != ''){
						
						d.start_date = input_values[3]; // start_date

						d.end_date = input_values[4]; // end_date
						
						d.destruction_type = destruction_type; // [ expired | Obsolete | Return ]

					} // if(input_values[2] != '' && input_values[3] != '')
			      
			    } // data
			    
			  } // ajax

			}); // End -> DataTable instance

		} //

	} // if(process == 1)

} // End => function show_destruction_log_table(tab)
///////////// End show table functions //////////////

/////////////////////////////////////////////////////

function show_patient_history_rx(tab){

	// Validate form
	$('#'+tab+'-form').formValidation('validate');

	// Validation for DateTime Picker [ date range ]
	$('.input-daterange').on('changeDate show', function(e) {
        // Revalidate the date when user change it
        $('#'+tab+'-form').bootstrapValidator('revalidateField', 'start_date');
        $('#'+tab+'-form').bootstrapValidator('revalidateField', 'end_date');
	});

	// Verify if any field has error
	var proceed = 1;
	var input_values = [];

	$("#"+tab+"-form :input").each(function(){
	
		var input = $(this); // This is the jquery object of the input

		if(input.parent().parent().hasClass("has-error")){
	 		proceed = 0;
	 	} // if(input.parent().parent().hasClass("has-error"))

	 	// Get all input field values
	 	if(input.attr("type") == 'hidden' ){
	 		if(input.val()){
				input_values.push(input.val());
			} else {
				input.parent().	parent().addClass("has-error");
				proceed = 0;

				// Show error message
				if($('.help-block')){
	                $.each( $('.help-block'), function(k, error_el){
	                  if($(error_el).attr('data-fv-for') == 'patient_name'){
	                    if( $(error_el).attr('data-fv-validator') == 'notEmpty' ){
	                      $(error_el).show();
	                      $('#2').val('');
	                    } else {
	                      $(error_el).hide();
	                    }
	                  } // if( $(error_el).attr('data-fv-for') == 'patient_name_1' )
	                });
              	} // if($('.help-block'))

			} // if(input.val())
		} // if(input.attr("type") == 'hidden' )

	});

	if( $('#patient_history_start_date').val() != '' && $('#patient_history_end_date').val() != '' ){
		input_values.push($('#patient_history_start_date').val());
		input_values.push($('#patient_history_end_date').val());
	} // if( $('#patient_history_start_date').val() != '' && $('#patient_history_end_date').val() != '' )

	// If form has no errors : proceed = 1
	if(proceed == 1){

		// Remove table hidden class : if table needs to be appeared onClick event
		$('#'+tab+'-table').removeClass("hidden");
		$('#'+tab+'-btn').removeClass("hidden");
		
		// Avoid re-Initialization
		$('#'+tab+'-table').DataTable().destroy();

		// DataTable : Initialise & Send Ajax call
		var table = $('#'+tab+'-table').DataTable({

		  "processing": true,
		  "serverSide": true,
		  "searching" : true,
		  "columnDefs": [

		    // Column definitions : Make columns orderable, searchable => true, false
		    { "orderable": true,  "targets": 0 },
		    { "orderable": false, "targets": 1 },
		    { "orderable": false, "targets": 2 },
		    { "orderable": false, "targets": 3 },
		    { "orderable": false, "targets": 4 },
		    { "orderable": false, "targets": 5 },
		    { "orderable": false, "targets": 6 }

		  ],

		  // Send ajax call to controller
		  "ajax": {

		    "url": SURL + "reports/patient-history-installment-rx-report",
		    "type" : "post",

		    "data": function(d) {

		    	// Extra attributes : custom input field data

		      	d.patient_id = input_values[1]; // patient_id
		      	
		      	// Verify if the date rage is not null [ validate both fields ]
		      	if(input_values[2] != '' && input_values[3] != ''){
		      	
		      		d.start_date = input_values[2]; // start_date
		      		d.end_date = input_values[3]; // end_date

		      	} // if(input_values[3] != '' && input_values[4] != '')

		    } // data
		    
		  } // ajax

		}); // End -> DataTable instance

	} // if(process == 1)

} // End => function show_patient_history_rx(tab)
/////////////////////////////////////////////////////

// Print table function : get table thead & tbody by table ID //
function print_table(table_id){

	// Verify if table ID is given and not null
    if(table_id){

      var table = '<table>';
      table += $('#'+table_id).html();
      table += '</table>';

      var table_contents = table;
      var printWindow = window.open('', '', 'height=400,width=800');
      printWindow.document.write('<html><head><title> Print Reports Testing </title>');
      printWindow.document.write('</head><body>');
      printWindow.document.write(table_contents);
      printWindow.document.write('</body></html>');
      printWindow.document.close();
      printWindow.print();
      return;

      var mode = 'iframe';
      var close = mode == "popup";
      var options = {
        mode: mode,
        popClose: close
      };
      table.printArea(options);

    } // if(table_id)
  } // function print_table(table_id)