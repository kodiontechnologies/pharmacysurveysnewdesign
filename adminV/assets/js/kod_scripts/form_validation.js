$(document).ready(function() { 
	
	//edit admin profile
	$('#edit_profile_frm').formValidation(); // end #add_edit_sop_frm
	
	//change admin password
	$('#change_password_frm').formValidation(); // end #add_edit_sop_frm
	
	//Login Admin
	$('#login_frm').formValidation(); // end #add_edit_sop_frm	
	
	//Login Admin
	$('#forgot_password_frm').formValidation(); // end #forgot_password_frm	
	
		//Add Default Drug
	$('#add_edit_drug_frm').formValidation(); // end #add_edit_drug_frm	
	
	//Edit Default Drug
	$('#edit_drug_frm').formValidation(); // end #add_edit_drug_frm	
	
	//Add Email Templates
	$('#add_email_template_frm').formValidation(); // end #add_email_template_frm
	
	//Edit Email Templates
	$('#edit_email_template_frm').formValidation(); // end #edit_email_template_frm	
	
	//Add Privallages
	$('#add_prv_frm').formValidation(); // end #update_prv_frm	
	
	//Update Privallages
	$('#update_prv_frm').formValidation(); // end #update_prv_frm	
	
	//Add cms new page 
	$('#add_new_page_frm').formValidation(); // end #add_new_page_frm	
	
	//Update cms new page 
	$('#edit_new_page_frm').formValidation(); // end #edit_new_page_frm	
	
	//Add drug form
	$('#add_drug_form_frm').formValidation(); // end #add_drug_form_frm	
	
	//Update drug form
	$('#edit_drug_from_frm').formValidation(); // end #edit_drug_from_frm
	
	//Add pharmacy form
	$('#add_pharmacy_frm').formValidation(); // end #add_pharmacy_frm
	
	//edit pharmacy form
	$('#edit_pharmacy_frm').formValidation(); // end #edit_pharmacy_frm
	
	//Add business form
	$('#add_business_frm').formValidation(); // end #add_business_frm
	
	//Edit business form
	$('#edit_business_frm').formValidation(); // end #edit_business_frm
			
});
