			jQuery(function($) {
			
								
							
				
				
			
				//datepicker plugin
				//link
				$('.date-picker').datepicker({
					autoclose: true,
					todayHighlight: true
				})
				//show datepicker when clicking on the icon
				.next().on(ace.click_event, function(){
					$(this).prev().focus();
				});
			
				//or change it into a date range picker
				$('.input-daterange').datepicker({autoclose:true});
			
			
				//to translate the daterange picker, please copy the "examples/daterange-fr.js" contents here before initialization
				$('input[name=date-range-picker]').daterangepicker({
					'applyClass' : 'btn-sm btn-success',
					'cancelClass' : 'btn-sm btn-default',
					locale: {
						applyLabel: 'Apply',
						cancelLabel: 'Cancel',
					}
				})
				.prev().on(ace.click_event, function(){
					$(this).next().focus();
				});
			
			
				$('#timepicker1').timepicker({
					minuteStep: 1,
					showSeconds: true,
					showMeridian: false
				}).next().on(ace.click_event, function(){
					$(this).prev().focus();
				});
				
				$('#date-timepicker1').datetimepicker().next().on(ace.click_event, function(){
					$(this).prev().focus();
				});

			
			
				//override dialog's title function to allow for HTML titles
				$.widget("ui.dialog", $.extend({}, $.ui.dialog.prototype, {
					_title: function(title) {
						var $title = this.options.title || '&nbsp;'
						if( ("title_html" in this.options) && this.options.title_html == true )
							title.html($title);
						else title.text($title);
					}
				}));
			
				$( "#id-btn-dialog1" ).on('click', function(e) {
					e.preventDefault();
			
					var dialog = $( "#dialog-message" ).removeClass('hide').dialog({
						modal: true,
						title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='ace-icon fa fa-check'></i> jQuery UI Dialog</h4></div>",
						title_html: true,
						buttons: [ 
							{
								text: "Cancel",
								"class" : "btn btn-minier",
								click: function() {
									$( this ).dialog( "close" ); 
								} 
							},
							{
								text: "OK",
								"class" : "btn btn-primary btn-minier",
								click: function() {
									$( this ).dialog( "close" ); 
								} 
							}
						]
					});
			
					/**
					dialog.data( "uiDialog" )._title = function(title) {
						title.html( this.options.title );
					};
					**/
				});
			
			
				$( "#id-btn-dialog2" ).on('click', function(e) {
					e.preventDefault();
				
					$( "#dialog-confirm" ).removeClass('hide').dialog({
						resizable: false,
						width: '320',
						modal: true,
						title: "<div class='widget-header'><h4 class='smaller'><i class='ace-icon fa fa-exclamation-triangle red'></i> Empty the recycle bin?</h4></div>",
						title_html: true,
						buttons: [
							{
								html: "<i class='ace-icon fa fa-trash-o bigger-110'></i>&nbsp; Delete all items",
								"class" : "btn btn-danger btn-minier",
								click: function() {
									$( this ).dialog( "close" );
								}
							}
							,
							{
								html: "<i class='ace-icon fa fa-times bigger-110'></i>&nbsp; Cancel",
								"class" : "btn btn-minier",
								click: function() {
									$( this ).dialog( "close" );
								}
							}
						]
					});
				});
			
			
				
				//autocomplete
				 var availableTags = [
					"Morphine",
					"Dexamphetamine",
					"Diamorphine",
					"Pethidine",
					"Methadone",
					"Methylphenidate",
					"Fentanyl",
					"Hydromorphone",
					"Oxycodone",
					"Buprenorphine",
					"Temazepam",
					"Dipipanone",
					"Pentazocine",
					"Phenobarbital",
					"Midazolam",
					"Diazepam"
					
				];
				//autocomplete
				 var availableTags2 = [
					"Micheal Fred",
					"William Smith",
					"Aboar Joson",
					"Bores Jonson",
					"David Cameron",
					"Haller Boker 8 Wensley close, N11 3GU, London",
					"Ahmed Sidiqzia 8 Wensley close, N11 3GU, London"					
					
				];
				$( "#autodrugnname" ).autocomplete({
					source: availableTags
				});
				$( "#stock_supply_patient_name" ).autocomplete({
					source: availableTags2,
					 select: function (a, b) {
						$( "#stock_supply_patient_address" ).val('8 Wensley Close');
						$( "#stock_supply_patient_town" ).val('London');
						$( "#stock_supply_patient_postcode" ).val('N11 3GU');
					}
				});
				
				$( "#autostockrecevied" ).autocomplete({
					source: availableTags
				});
				
				$( "#stock_received_supplier" ).autocomplete({
					source: availableTags2
				});
				
				$( "#autostockcheck" ).autocomplete({
					source: availableTags
				});
				
				$( "#autostockspillage" ).autocomplete({
					source: availableTags
				});
				
				$( "#autoselectpatient" ).autocomplete({
					source: availableTags2
				});
				
				$( "#autoselectprescriber" ).autocomplete({
					source: availableTags2
				});
				
				$( "#autoselectprescribeddrug" ).autocomplete({
					source: availableTags
				});
				
				
				
				
				
				
				
				
				
			
				//custom autocomplete (category selection)
				$.widget( "custom.catcomplete", $.ui.autocomplete, {
					_create: function() {
						this._super();
						this.widget().menu( "option", "items", "> :not(.ui-autocomplete-category)" );
					},
					_renderMenu: function( ul, items ) {
						var that = this,
						currentCategory = "";
						$.each( items, function( index, item ) {
							var li;
							if ( item.category != currentCategory ) {
								ul.append( "<li class='ui-autocomplete-category'>" + item.category + "</li>" );
								currentCategory = item.category;
							}
							li = that._renderItemData( ul, item );
								if ( item.category ) {
								li.attr( "aria-label", item.category + " : " + item.label );
							}
						});
					}
				});
				
				 var data = [
					{ label: "anders", category: "" },
					{ label: "andreas", category: "" },
					{ label: "antal", category: "" },
					{ label: "annhhx10", category: "Products" },
					{ label: "annk K12", category: "Products" },
					{ label: "annttop C13", category: "Products" },
					{ label: "anders andersson", category: "People" },
					{ label: "andreas andersson", category: "People" },
					{ label: "andreas johnson", category: "People" }
				];
				$( "#search" ).catcomplete({
					delay: 0,
					source: data
				});
				
				
				//tooltips
				$( "#show-option" ).tooltip({
					show: {
						effect: "slideDown",
						delay: 250
					}
				});
			
				$( "#hide-option" ).tooltip({
					hide: {
						effect: "explode",
						delay: 250
					}
				});
			
				$( "#open-event" ).tooltip({
					show: null,
					position: {
						my: "left top",
						at: "left bottom"
					},
					open: function( event, ui ) {
						ui.tooltip.animate({ top: ui.tooltip.position().top + 10 }, "fast" );
					}
				});
			
			
				//Menu
				$( "#menu" ).menu();
			
			
				//spinner
				var spinner = $( "#spinner" ).spinner({
					create: function( event, ui ) {
						//add custom classes and icons
						$(this)
						.next().addClass('btn btn-success').html('<i class="ace-icon fa fa-plus"></i>')
						.next().addClass('btn btn-danger').html('<i class="ace-icon fa fa-minus"></i>')
						
						//larger buttons on touch devices
						if('touchstart' in document.documentElement) 
							$(this).closest('.ui-spinner').addClass('ui-spinner-touch');
					}
				});
			
				//slider example
				$( "#slider" ).slider({
					range: true,
					min: 0,
					max: 500,
					values: [ 75, 300 ]
				});
			
			
			
				//jquery accordion
				$( "#accordion" ).accordion({
					collapsible: true ,
					heightStyle: "content",
					animate: 250,
					header: ".accordion-header"
				}).sortable({
					axis: "y",
					handle: ".accordion-header",
					stop: function( event, ui ) {
						// IE doesn't register the blur when sorting
						// so trigger focusout handlers to remove .ui-state-focus
						ui.item.children( ".accordion-header" ).triggerHandler( "focusout" );
					}
				});
				//jquery tabs
				$( "#tabs" ).tabs();
				
				
				//progressbar
				$( "#progressbar" ).progressbar({
					value: 37,
					create: function( event, ui ) {
						$(this).addClass('progress progress-striped active')
							   .children(0).addClass('progress-bar progress-bar-success');
					}
				});
			
				
				//selectmenu
				 $( "#number" ).css('width', '200px')
				.selectmenu({ position: { my : "left bottom", at: "left top" } })
					
			});
			
			// row activity function
			function rowActivity(rid)
			{						
							
				
				var postForm = {
                      'option'     : 'loadeditentry',
					  'rid'     : rid				 				
					  
                 };
				
				$.ajax({
				url: url,
				type: 'POST',
				dataType: "JSON",
				data: postForm,
				//processData: false,
				//contentType: false,
				success: function (data, status)
				{					
				    
					// must show edit box
					//alert(data.htm)
					$('#model_edit_entry').modal('show');
					$('#model_edit_entry .modal-content').html(data.htm);	
						
					$('#model_edit_entry').modal({backdrop: 'static', keyboard: false}) 
					
					//alert(rid);
					//$( data.row ).insertAfter( "#"+rid );
					//$('#'+rid).replaceWith(data.row);
							
				},
				error: function (xhr, desc, err)
				{
					alert(err);

				}
				});
				
			}
			
			// repalce row in register
			function insertNewRow(rid)
			{
				var annotationtext = $('#annotationtext').val();
				var postForm = {
                      'option'     : 'rowactivity',
					  'rid'     : rid,
					  'annotationtext' :annotationtext			
					  
                 };
				
				$.ajax({
				url: url,
				type: 'POST',
				dataType: "JSON",
				data: postForm,
				//processData: false,
				//contentType: false,
				success: function (data, status)
				{					
				  
					$('#'+rid).replaceWith(data.row);
							
				},
				error: function (xhr, desc, err)
				{
					alert(err);

				}
				});
			}
			
			// load form
			var url = "process.php"
			function loadCategories(cid)
			{
								
				
				$('#drugclasstable').hide( "slide", { direction: "left"  }, 1000 );
				$('#drugformtable').css("display", "block");
				// load drug form
				
				/* var postForm = {
                      'option'     : 'loadform',
					  'categoryid'     : cid
                 };

				
				$.ajax({
				url: url,
				type: 'POST',
				dataType: "JSON",
				data: postForm,
				//processData: false,
				//contentType: false,
				success: function (data, status)
				{
					
					var htmlStr = '';
					$.each(data.form, function(k, v){
						htmlStr+= v;
					});
					$('#drugclasstable').html(htmlStr);
					$('#drugclasstable').css({display: "block"});
				},
				error: function (xhr, desc, err)
				{
					alert('no'+desc);

				}
				});     */   
				
			}
			
			// Ajax function for stock supplied
			function stock_supplied()
			{
							
				var cid = $('#stock_supply_category_id').val();
				var bid = $('#stock_supply_brand_id').val();
				var fid = $('#stock_supply_form_id').val();				
				var sid = $('#stock_supply_strength_id').val();
				
				var qty = $('#stock_supply_qty').val();
				var date = $('#stock_supply_date').val();
				var name = $('#stock_supply_patient_name').val();
				var address = $('#stock_supply_patient_address').val();
				var town = $('#stock_supply_patient_town').val();
				var postcode = $('#stock_supply_patient_postcode').val();
				var prescriber = $('#stock_supply_prescriber_name').val();
				var regNo = $('#stock_supply_registration_number').val();
				var personcollecting = $('#person_collecting').val();
				var idprovided = $('#id_provided').val();
				var discretionused = $('#discretion_used').val();
				
				// load drug form
				
			   var postForm = {
                      'option'     : 'stocksupllied',
					  'cid'     : cid,
					  'fid'     : fid,
					  'bid'     : bid,
					  'sid'     : sid,					  
					  'qty'     : qty,
					  'date'     : date,
					  'name'     : name,
					  'address'     : address,
					  'town'     : town,
					  'postcode'     : postcode,
					  'prescriber'     : prescriber,
					  'regNo'     : regNo,
					  'personcollecting'     : personcollecting,
					  'idprovided'     : idprovided,
					  'discretionused'     : discretionused
                 };

				
				$.ajax({
				url: url,
				type: 'POST',
				dataType: "JSON",
				data: postForm,
				//processData: false,
				//contentType: false,
				success: function (data, status)
				{
					//alert(data.msg);
					$('#model_stock_supplied .modal-body').html('Stock Supplied Successfully!');
					$('#model_stock_supplied .modal-title').html('Stock Supplied');
					
					$('#model_stock_supplied').modal('show');								
					
					$('#model_stock_supplied').modal({backdrop: 'static', keyboard: false})  
							
				},
				error: function (xhr, desc, err)
				{
					alert(err);

				}
				});   
				
			
			}
			
			
			// Ajax function for stock received
			function stock_received()
			{							
				var cid = $('#stock_received_category_id').val();
				var bid = $('#stock_received_brand_id').val();
				var fid = $('#stock_received_form_id').val();
				var sid = $('#stock_received_strength_id').val();				
				
				var qty = $('#stock_received_qty').val();
				var date = $('#stock_received_date').val();
				var supplier = $('#stock_received_supplier').val();				
				var address = $('#stock_received_supplier_address').val();				
				var invoicenumber = $('#stock_received_supplier_invoiceNo').val();			
				
			   // load drug form				
			   var postForm = {
                      'option'     : 'stockreceived',
					  'cid'     : cid,
					  'bid'     : bid,
					  'fid'     : fid,					  
					  'sid'     : sid,						  
					  'qty'     : qty,
					  'date'     : date,
					  'supplier'     : supplier,
					  'address'     : address,
					  'invoicenumber'     : invoicenumber
					  };
				
				$.ajax({
				url: url,
				type: 'POST',
				dataType: "JSON",
				data: postForm,
				//processData: false,
				//contentType: false,
				success: function (data, status)
				{
					//alert(data.msg);
					$('#model_stock_supplied .modal-body').html('Stock Received Successfully!');
					$('#model_stock_supplied .modal-title').html('Stock Received');
					
					$('#model_stock_supplied').modal('show');										
					
					$('#model_stock_supplied').modal({backdrop: 'static', keyboard: false})  
							
				},
				error: function (xhr, desc, err)
				{
					alert(err);

				}
				}); 		
			
			}
			
			
			// Ajax function for stock Check
			function stock_check()
			{
							
				var cid = $('#stock_check_category_id').val();				
				var bid = $('#stock_check_brand_id').val();
				var fid = $('#stock_check_form_id').val();
				var sid = $('#stock_check_strength_id').val();
				
				var date = $('#stock_check_date').val();
				var person_making_entry = $('#person_making_entry').val();				
				var stock_check_by = $('#stock_check_checked_by').val();				
				var qty = $('#stock_check_qty_present').val();		
				var difference_with_balance = $('#difference_with_balance').val();					
				
				// load drug form
				
			   var postForm = {
                      'option'     : 'stockcheck',
					  'cid'     : cid,
					  'bid'     : bid,
					  'fid'     : fid,
					  'sid'     : sid,					 
					  'date'     : date,
					  'person_making_entry'     : person_making_entry,
					  'stock_check_by'     : stock_check_by,
					  'qty'     : qty,
					  'difference_with_balance'    : difference_with_balance
					  };

				
				$.ajax({
				url: url,
				type: 'POST',
				dataType: "JSON",
				data: postForm,
				//processData: false,
				//contentType: false,
				success: function (data, status)
				{
					//alert(data.msg);
					$('#model_stock_supplied .modal-body').html('Stock Checked Successfully!');
					$('#model_stock_supplied .modal-title').html('Stock Check');
					
					$('#model_stock_supplied').modal('show');										
					
					$('#model_stock_supplied').modal({backdrop: 'static', keyboard: false})  
							
				},
				error: function (xhr, desc, err)
				{
					alert(err);

				}
				});   
				
			
			}
			
			
			// Ajax function for stock Spillage
			function stock_spillage()
			{
							
				var cid = $('#stock_spill_category_id').val();
				var bid = $('#stock_spill_brand_id').val();
				var fid = $('#stock_spill_form_id').val();
				var sid = $('#stock_spill_strength_id').val();
				
				var date = $('#stock_spillage_date').val();
				var qty = $('#stock_spillage_qty').val();
				var stock_spillage_staff_member = $('#stock_spillage_staff_member').val();				
				var stock_spillage_other_staff_present = $('#stock_spillage_other_staff_present').val();							
								
				
				// load drug form
				
			   var postForm = {
                      'option'     : 'stockspillage',
					  'cid'     : cid,
					  'bid'     : bid,
					  'fid'     : fid,
					  'sid'     : sid,					 
					  'date'     : date,
					  'qty'     : qty,
					  'stock_spillage_staff_member'     : stock_spillage_staff_member,
					  'stock_spillage_other_staff_present'     : stock_spillage_other_staff_present
					  };

				
				$.ajax({
				url: url,
				type: 'POST',
				dataType: "JSON",
				data: postForm,
				//processData: false,
				//contentType: false,
				success: function (data, status)
				{
					//alert(data.msg);
					$('#model_stock_supplied .modal-body').html('Stock Spillage Successfully!');
					$('#model_stock_supplied .modal-title').html('Stock Spillage');
					
					$('#model_stock_supplied').modal('show');										
					
					$('#model_stock_supplied').modal({backdrop: 'static', keyboard: false})  
							
				},
				error: function (xhr, desc, err)
				{
					alert(err);

				}
				});   
				
			
			}
			
			
			// Ajax function for stock expired
			function stock_expired()
			{
							
				var cid = $('#stock_expired_category_id').val();
				var bid = $('#stock_expired_brand_id').val();
				var fid = $('#stock_expired_form_id').val();
				var sid = $('#stock_expired_strength_id').val();
				
				var date = $('#stock_expired_date').val();
				var qty = $('#stock_expired_qty').val();					
								
				
				// load drug form
				
			   var postForm = {
                      'option'     : 'stockexpired',
					  'cid'     : cid,
					  'bid'     : bid,	
					  'fid'     : fid,
					  'sid'     : sid,					 
					  'date'     : date,
					  'qty'     : qty					 
					  };
				
				$.ajax({
				url: url,
				type: 'POST',
				dataType: "JSON",
				data: postForm,
				//processData: false,
				//contentType: false,
				success: function (data, status)
				{
					//alert(data.msg);
					$('#model_stock_supplied .modal-body').html('Stock Expired Successfully!');
					$('#model_stock_supplied .modal-title').html('Stock Expired');
					
					$('#model_stock_supplied').modal('show');										
					
					$('#model_stock_supplied').modal({backdrop: 'static', keyboard: false})  
							
				},
				error: function (xhr, desc, err)
				{
					alert(err);

				}
				});   
				
			
			}
			
			// Ajax function for stock obsolete
			function stock_obsolete()
			{
							
				var cid = $('#stock_obsolete_category_id').val();
				var bid = $('#stock_obsolete_brand_id').val();
				var fid = $('#stock_obsolete_form_id').val();
				var sid = $('#stock_obsolete_strength_id').val();
				
				var date = $('#stock_obsolete_date').val();
				var qty = $('#stock_obsolete_qty').val();					
								
				
				// load drug form
				
			   var postForm = {
                      'option'     : 'stockobsolete',
					  'cid'     : cid,
					  'bid'     : bid,	
					  'fid'     : fid,
					  'sid'     : sid,					 
					  'date'     : date,
					  'qty'     : qty					 
					  };
				
				$.ajax({
				url: url,
				type: 'POST',
				dataType: "JSON",
				data: postForm,
				//processData: false,
				//contentType: false,
				success: function (data, status)
				{
					//alert(data.msg);
					$('#model_stock_obsolete .modal-body').html('Stock Obsolete Successfully!');
					$('#model_stock_obsolete .modal-title').html('Stock Obsolete');
					
					$('#model_stock_obsolete').modal('show');										
					
					$('#model_stock_obsolete').modal({backdrop: 'static', keyboard: false})  
							
				},
				error: function (xhr, desc, err)
				{
					alert(err);

				}
				});   
				
			
			}
			
			

			
			// show strenght
			function showStrength(fid)
			{
				$('#drugbrandstable').css("display", "none");
				$('#drugstrengthtable').css("display", "block");
			}
			
			// show brands
			function showBrands(fid)
			{
				$('#drugformtable').css("display", "none");
				$('#drugbrandstable').css("display", "block");
			}
			
			// show heathcare professioanl name and address
			function showPersonDetails(id)
			{
				$('#hpn').css("display", "block");
				$('#hpa').css("display", "block");
				$('#id_provided').attr("selcted", "");
				
				
			}
			
			// id provided 
			function selectIDProvided(id)
			{
				var sid = $("#id_provided").val();
				if(sid!="no"){
					
				     $("#discretion_used").val('no');	
				}
				else{
					$("#discretion_used").val('knowtostaff');
				}
				 
				
			    
			}
			
			function calculateDiff(v,cid,bid,fid,sid)
			{				
				
				// load drug form
				
			   var postForm = {
                      'option'     : 'calcdiff',
					  'cid'     : cid,
					  'bid'     : bid,
					  'fid'     : fid,
					  'sid'     : sid,					 
					  'bpresent'     : v				 
					  };

				
				$.ajax({
				url: url,
				type: 'POST',
				dataType: "JSON",
				data: postForm,
				//processData: false,
				//contentType: false,
				success: function (data, status)
				{
					
					$('#difference_with_balance').val(data.qp);				
							
				},
				error: function (xhr, desc, err)
				{
					alert(err);
				}
				});   
			}
			
			
			// annotation buttons
			function annotationBtn(divid,rid)
			{
				
				if(divid == 'annotatediv')
				{
					$('#annotatediv').css("display", "block");
					$('#voiddiv').css("display", "none");
					$('#editdiv').css("display", "none");
					
				}
				else if(divid == 'voiddiv')
				{
					
					$('#annotatediv').css("display", "none");
					$('#voiddiv').css("display", "block");
					$('#editdiv').css("display", "none");
					
					// do the void entry
					voidEntry(rid);
					
					
					
					
				}
				else(divid == 'editdiv')
				{
					$('#annotatediv').css("display", "none");
					$('#voiddiv').css("display", "none");
					$('#editdiv').css("display", "block");
				}
			}
			
			// void entry
			function voidEntry(rid)
			{				
				
			   // void entry				
			   var postForm = {
                      'option'     : 'voidentry',
					  'rid'     : rid					  				 
					  };
				
				$.ajax({
				url: url,
				type: 'POST',
				dataType: "JSON",
				data: postForm,				
				success: function (data, status)
				{					
					$('#'+rid).replaceWith(data.row);
					$('#registertable > tbody').append(data.appd);					
				},
				error: function (xhr, desc, err)
				{
					alert(err);

				}
				});   
			}
			
			
			// destruction function
			function rowDestruction()
			{						
							
				
				$('#model_drug_destroy').modal('show');
				
			}
			
			// destruction expired functuon
           function rowExpiredStock()
		   {
			   $('#model_drug_expiry').modal('show');
			   
		   }		   
			
			
			
			
			
		