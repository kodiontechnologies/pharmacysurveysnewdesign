<?php

//Function print_this(&$array): Used for development purpose. Pritning Array or any Object with <pre>.
function print_this(&$arr){
	
	echo '<pre>';
	print_r($arr);
	
}//end print_this(&$array);

//Function filter_string():  will filter the string from unwanted characters while displaying it on teh screen.
function filter_string(&$string){
	
	$ci =& get_instance();
	
	$filter_txt = stripcslashes(trim($string));
	
	return $filter_txt;
	
}//end filter_string()

//Function filter_price():  Wil change the price formats as per the need.
function filter_price(&$price){
	
	$ci =& get_instance();
	
	$filter_price = number_format($price,2);
	
	return $filter_price;
	
}//end filter_price()

//Function random_number_generator($digit): random number generator function
function random_number_generator($digit){
	$randnumber = '';
	$totalChar = $digit;  //length of random number
	$salt = "0123456789abcdefjhijklmnopqrstuvwxyz";  // salt to select chars
	srand((double)microtime()*1000000); // start the random generator
	$password=""; // set the inital variable
	
	for ($i=0;$i<$totalChar;$i++)  // loop and create number
	$randnumber = $randnumber. substr ($salt, rand() % strlen($salt), 1);
	return $randnumber;
	
}// end random_password_generator()


//Function filter_price():  Wil change the price formats as per the need.
function get_global_settings($setting_name){
	
	$ci =& get_instance();
	
	$ci->db->dbprefix('global_settings');
	$ci->db->where('setting_name',$setting_name);
	$get_result = $ci->db->get('global_settings');
	
	return $get_result->row_array();
	
}//end get_global_settings(&$setting_name)

// Start - function kod_date_format($date='', $time=''): Function to return the formated date (first parameter date and second is boolean true or false to get time)
if (!function_exists('kod_date_format')) {
	function kod_date_format($date = '', $time = FALSE){
		
		if(substr($date,0,4) != '0000'){
			$date1 = $date;
			if($date != ''){
				
				if($time == true)
					return date_format(date_create($date),"j M, Y g:i a");
				elseif($time == false)
					return date_format(date_create($date),"j M, Y");
					
			} else
				return $date;
				
		}else{
			return $date;
			
		}//end if(substr($date,0,4) != '0000')
		
	}//end kod_date_format($date='', $time='')

} // End - function kod_date_format($date='', $time='')

//Function get_longitude_latitude(): Fetch longitude, latitude by address
function get_longitude_latitude($address){

	$address = urlencode($address);
	$request_url = "http://maps.googleapis.com/maps/api/geocode/xml?address=".$address."&sensor=true";

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $request_url);
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$xml = curl_exec($ch);
	curl_close($ch);
	
	$xml = simplexml_load_string($xml);

	$status = $xml->status;

	if ($status=="OK") {
		
	  $result_arr['latitude'] = $xml->result->geometry->location->lat;
	  $result_arr['longitude'] = $xml->result->geometry->location->lng;
	  
	}//end if ($status=="OK") 
	
	return $result_arr;

}//end get_longitude_latitude($address)

//GET All ACTIVE USER COUNTRY
function get_active_countries(){
	
	$ci =& get_instance();
	
	$ci->db->dbprefix('countries_worldwide');
	$ci->db->order_by('id ASC');
	$list_arr = $ci->db->get('countries_worldwide');
	//echo $ci->db->last_query();exit;
	
	return $list_arr->result_array();
	
}//end get_active_country()

//Function kod_send_email():  Send email.
function kod_send_email($from, $from_name = '', $to, $subject = '', $email_body ='', $attachments =''){

	$ci =& get_instance();

	$ci->load->helper(array('email'));		
	//Preparing Sending Email
	$config['charset'] = 'utf-8';
	$config['mailtype'] = 'html';
	$config['wordwrap'] = TRUE;			
	$config['protocol'] = 'mail';
		
	$ci->load->library('email',$config);
	$ci->email->from($from, $from_name);
	$ci->email->to(trim($to));
	$ci->email->subject($subject);
	$ci->email->message($email_body);
	
	$ci->email->send();
	$ci->email->clear();
	
	// EMAIL SENDING CODE - STOP
	
}//end kod_send_email()	

	// Start - get_cms_page($url_slug): Return CMS Page by URL
function get_cms_page_footer($url_slug){
	    $ci =& get_instance();
		$ci->db->dbprefix('pages');
		$ci->db->where('url_slug', $url_slug);
		$ci->db->where('status', '1');
		$get_result = $ci->db->get('pages');
	
		return $get_result->row_array();
		
	} // End - get_cms_page($url_slug)
	
function get_user_details_profile($user_id){
		
		$ci =& get_instance();
		// Get Patient Record
		$ci->db->dbprefix('admin');
		$ci->db->select('admin.*');
		$ci->db->where('admin.id', $user_id);
		$get = $ci->db->get('admin');
		//echo $this->db->last_query(); exit;
		return $get->row_array();
		
} // End public function get_user_details($patient_id)

//Function dates_between(): Will give the date ranges between two dates
function dates_between($startDate, $endDate){
	
	// get the number of days between the two given dates.
	$days = (strtotime($endDate) - strtotime($startDate)) / 86400 + 1;
	$startMonth = date("m", strtotime($startDate));
	$startDay = date("d", strtotime($startDate));
	$startYear = date("Y", strtotime($startDate));   
	$dates;//the array of dates to be passed back
	for($i=0; $i<$days; $i++)
		$dates[$i] = date("m/d/Y", mktime(0, 0, 0, $startMonth , ($startDay+$i), $startYear));
	
	return $dates;   
}//end dates_between($startDate, $endDate)

//Function verify_end_date(): Will calculate and verify the delivery end date
function verify_end_date($end_date,$holidays_arr){

	$day_name = date('N', strtotime($end_date));
	$month_day = date('m/d',strtotime($end_date));
	
	if($day_name == 7){

		$final_return_date = date('m/d/Y', strtotime(str_replace('/','-',$end_date). " + 1 days"));
		return verify_end_date($final_return_date,$holidays_arr);
		
	}elseif(in_array($month_day,$holidays_arr)){
		
		$final_return_date = date('m/d/Y', strtotime(str_replace('/','-',$end_date). " + 1 days"));
		return verify_end_date($final_return_date,$holidays_arr);
		
	}else{
		return $end_date;
	}//end if(in_array($end_date,$holidays_arr))
	
}//end verify_end_date($end_date,$holidays_arr)
	
//Function calculate_shipping_dates: Claculate final delivey dates start and end.
function calculate_patient_dose_schedule($start_date,$duration){
	
	$holidays_arr = array(
						'01/01',
						'03/25',
						'03/28',
						'05/02',
						'05/30',
						'08/29',
						'12/26',
						'12/25',
						'12/27',
					);
	
	$duration = $duration - 1;
	$start_date = date('Y-m-d', strtotime(str_replace('/','-',$start_date)));
	$end_date = date('Y-m-d', strtotime(str_replace('/','-',$start_date). " + $duration days"));
	
	$date_range = dates_between($start_date,$end_date);
	
	//print_this($date_range); exit;
	
	$add_more_days = 0;
	
	//Check Sundays and Bank Holidays fo first time
	$eliminate_date = array();
	for($i=0; $i<count($date_range)-1; $i++){
		
		$day_name = date('N', strtotime($date_range[$i]));
		$common_date_for_holidays = date('m/d',strtotime($date_range[$i]));
		
		if($day_name == 7){
			//$add_more_days++;	
		}else{
	
			if(in_array($common_date_for_holidays,$holidays_arr)){
				//$add_more_days++;
			}
		}//end if($day_name == 7)
		
	}//end for($i = 0;$i<count($date_range);$i++)
	
	$intermediate_end_date = date('m/d/Y', strtotime(str_replace('/','-',$end_date). " + $add_more_days days"));
	
	//$final_end_date = verify_end_date($intermediate_end_date,$holidays_arr);
	$final_end_date = $intermediate_end_date;
	$final_range = dates_between($start_date,$final_end_date);
	
	//print_this($final_range); exit;

	$eliminate_date = array();

	for($i=0; $i<count($final_range); $i++){
		
		$day_name = date('N', strtotime($final_range[$i]));
		$common_date_for_holidays = date('m/d',strtotime($final_range[$i]));
		
		if($day_name == 7){

			
			$eliminate_date[] = $final_range[$i];
			/*
			//If first day is sunday add all fridays into the list.
			
			$is_first_day_fri = date('N', strtotime($final_range[0]));
			
			if($is_first_day_fri == 7)
				$eliminate_date[] = $final_range[$i];
			
			*/
		}else{
			//If any of the dates coming are holidays, skip it else add it.
			if(!in_array($common_date_for_holidays,$holidays_arr))
				$eliminate_date[] = $final_range[$i];
				
		}//end if($day_name == 7)
		
	}//end for($i = 0;$i<count($date_range);$i++)

	//print_this($eliminate_date); exit;
	return ($eliminate_date);
		
}//end calculate_patient_dose_schedule($start_date,$duration)

if(!function_exists('cdr_left_nav')){
    
    function cdr_left_nav(){

    	$ci =& get_instance();

    	$ci->db->dbprefix('menues');

    	// $ci->db->select('title');
    	$ci->db->where('parent_id', 0);

    	return $ci->db->get('menues')->result_array();

    } // function cdr_left_nav()

} // if(!function_exists('cdr_left_nav'))

function sub_menu_listing($parent_id){

	$ci =& get_instance();
	$ci->db->dbprefix('menues');
	$ci->db->where('parent_id', $parent_id);

	return $ci->db->get('menues')->result_array();

} // function sub_menu_listing($parent_id)


function menu_listing(){

	$ci =& get_instance();

	$ci->db->dbprefix('menues');
	$ci->db->where('parent_id', 0);

	$parent_menu = $ci->db->get('menues')->result_array();
	
	//print_this($parent_menu); exit;
	
	$final_menu_arr = array();
	
	for($i=0;$i<count($parent_menu);$i++){
		
		$final_menu_arr[$i]['menu_name'] = filter_string($parent_menu[$i]['title']);
		$final_menu_arr[$i]['menu_id'] = filter_string($parent_menu[$i]['id']);
		
		//Get Submenu array.
		$final_menu_arr[$i]['submenu_arr'] = sub_menu_listing(filter_string($parent_menu[$i]['id']));
		
	}//end for($i=0;$i<count($parent_menu);$i++)
	
	return $final_menu_arr;

} // function cdr_left_nav()

function get_usertype_list($usertype_id = '', $responsibility_id=''){

	$ci =& get_instance();

	$ci->db->dbprefix('usertype');
	$ci->db->where('status','1');

	if(trim($responsibility_id)!= '')
		$ci->db->where('responsibility_id',$responsibility_id);

	if(trim($usertype_id)!= ''){
		$ci->db->where('id',$usertype_id);
		return $ci->db->get('usertype')->result_array();
	}else
		return $ci->db->get('usertype')->result_array();

	//echo $ci->db->last_query();
	//exit;
	
} // function

function get_usertype_permissions($usertype_id = '', $responsibility_id=''){

	$ci =& get_instance();

	$ci->db->dbprefix('usertype_permissions,usertype');
	$ci->db->select('usertype_permissions.*, usertype.user_type');
	$ci->db->from('usertype_permissions');
	$ci->db->join('usertype', 'usertype.id = usertype_permissions.user_type_id');
	$ci->db->where('usertype_permissions.status','1');

	if(trim($responsibility_id)!= '')
		$ci->db->where('usertype_permissions.responsibility_id',$responsibility_id);
	else
		$ci->db->where('usertype_permissions.responsibility_id IS NULL');

	if(trim($usertype_id)!= ''){
		$ci->db->where('usertype_permissions.user_type_id',$usertype_id);
		return $ci->db->get()->result_array();
	}else
		return $ci->db->get()->result_array();

	//echo $ci->db->last_query();
	//exit;
	
} // function permissions()

//Function get_drug_forms(): This function returns the list of drug forms in the system
function get_drug_forms(){
	
	$ci =& get_instance();
	
	$ci->db->dbprefix('drugs_form');
	$get = $ci->db->get('drugs_form');

	//echo $this->db->last_query(); exit;
	return $get->result_array();
	
}//end get_drug_forms()

//WIll retuirn date format in MM/dd/YY, if time is true it will return the time as well
function filter_uk_date($date, $time = false){
	
	if($time)
		return date('d/m/Y G:i:s',strtotime($date));
	else
		return date('d/m/Y',strtotime($date));
		
}//end filter_uk_date($date, $time = false)

function get_most_recent_survey($pharmacy_id){
	
	$ci =& get_instance();
	return $ci->survey->get_most_recent_survey($pharmacy_id);
	
}//end get_pharmacy_survey_listget_pharmacy_survey_list($pharmacy_id, $survey_status = '')

function get_group_pharmacy_list($group_id){
	
	$ci =& get_instance();
	return $ci->pharmacies->get_group_pharmacy_list($group_id);
	
}//end get_pharmacy_survey_listget_pharmacy_survey_list($pharmacy_id, $survey_status = '')

//Function get_survey_submitted_count(): Returns the number of Survey submitted so far
function get_survey_submitted_count($pharmacy_id, $survey_id, $submit_type = ''){

	$ci =& get_instance();
	return $ci->survey->get_survey_submitted_count($pharmacy_id, $survey_id, $submit_type);

	
}//end get_survey_submitted_count($this->session->p_id, $pharmacy_current_survey['id'])


?>