<div class="row" style="margin:0">
<div class="col-xs-12 nopadding">
<div class="pm-title">Business: <?php echo ucfirst(filter_string($business_details['business_name']));?></div>
</div>
</div>
<div class="space-10"></div>

<div class="row" style="margin:0">           
<form class="prevent_enter_key" id="edit_business_frm" name="edit_business_frm" method="post" enctype="multipart/form-data" action="<?php echo SURL;?>business/add-new-business-process">

     <div class="row" style="margin:0">
    <label for="business_type" class="col-sm-3 control-label">Business Type</label>
    <div class="form-group col-sm-9">
        <select name="business_type" id="business_type"  required="required" class="form-control">
        <option value="">Select Business Type</option>
        <option value="B" <?php if($business_details['business_type']=='B'){?> selected="selected"<?php }?>>Buying Group</option>
        <option value="C" <?php if($business_details['business_type']=='C'){?> selected="selected"<?php }?>>Chain</option>
        </select>
                      
    </div>
  </div>
  
  
  <div class="row" style="margin:0">
    <label for="add_new_drug_class" class="col-sm-3 control-label"> Business Name </label>
    <div class="form-group col-sm-9">
     <input type="text" class="form-control" name="business_name" id="business_name" required="required" placeholder="Business name" value="<?php echo $business_details['business_name'];?>" data-fv-regexp="true" data-fv-regexp-regexp="^[a-zA-z0-9\s\-,\]+[a-zA-Z0-9\s\-_,.'/]{1,100}$" data-fv-regexp-message="Please use allowed characters (Alphabet, Numbers, Hyphens, Underscore, Comma, Apostrophe, Dot, Forward Slash, Back Slash, Space)" maxlength="100">
    </div>
  </div>

  <div class="row" style="margin:0">
  <label for="contact_name" class="col-sm-3 control-label"> Contact name </label>
    <div class="form-group col-sm-9">
       <input type="text"  class="form-control" name="contact_name" id="contact_name" placeholder="Contact name" value="<?php echo $business_details['contact_name'];?>"   aria-required="true"  required data-fv-regexp="true" data-fv-regexp-regexp="^[a-zA-z0-9\s\-,\]+[a-zA-Z0-9\s\-_,.'/]{1,50}$" data-fv-regexp-message="Please use allowed characters (Alphabet, Numbers, Hyphens, Underscore, Comma, Apostrophe, Dot, Forward Slash, Back Slash, Space)" maxlength="50">
    </div>
  </div>
  <div class="row" style="margin:0">
    <label for="add_new_drug_class" class="col-sm-3 control-label"> Contact number </label>
    <div class="form-group col-sm-9">
     <input type="text" class="form-control" name="contact_no" id="contact_no" placeholder="Contact number" value="<?php echo $business_details['contact_no'];?>" aria-required="true"  required data-fv-regexp="true"  data-fv-regexp-regexp="^(?=.*[0-9])[0-9]{11,}$" data-fv-regexp-message="Please use allowed characters (Numbers) and length should be minimum 11 characters"  maxlength="15">
    </div>
  </div>
  <div class="row" style="margin:0">
    <label for="add_new_drug_class" class="col-sm-3 control-label"> Email address  </label>
    <div class="form-group col-sm-9">
      <input type="email"  class="form-control" name="email_address" id="email_address" value="<?php echo $business_details['email_address'];?>" placeholder="Email address" aria-required="true"  required data-fv-regexp="true" data-fv-regexp-regexp="^[a-zA-z0-9\s\-]+[a-zA-Z0-9\s\-_.@]+$" data-fv-regexp-message="Please use allowed characters (Alphabet, Numbers, Hyphens, Underscore, Dot and @)" autocomplete="off" maxlength="50">
    </div>
  </div>
  
  <div class="row" style="margin:0">
    <label for="add_new_drug_brand" class="col-sm-3 control-label"> Password </label>
    <div class="form-group col-sm-9">
       <input type="password"  class="form-control"  name="password" id="password" placeholder="Password" data-fv-regexp="true" data-fv-regexp-regexp="^^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[A-Za-z0-9]{8,20}$" data-fv-regexp-message="Password must be between 8 to 20 characters with atleast one uppercase, one lowercase and one digit. Allowed characters (Alphabet, Numbers)" maxlength="20">
    </div>
  </div>

  <div class="row" style="margin:0">
    <label for="add_new_drug_brand" class="col-sm-3 control-label"> Status </label>
    <div class="form-group col-sm-9">
    	<select class="form-control" name="status" id="status">
        	<option <?php echo (filter_string($business_details['status']) == 1) ? 'selected="selected"' : ''?> value="1" >Active</option>
            <option <?php echo (filter_string($business_details['status']) == 0) ? 'selected="selected"' : ''?> value="0">Inactive</option>
        </select>

    </div>
  </div>


  <br />
  <div class="row" style="margin:2px 0px;">
    <div class="col-md-12 text-right">
      <button class="btn btn-success btn-sm" name="new_pharmacy_btn" id="new_pharmacy_btn" value="1" type="submit"><i class="ace-icon fa fa-floppy-o bigger-110"></i> Update</button>
      <input type="hidden" name="business_id" id="business_id" value="<?php echo filter_string($business_details['id']);?>" readonly="readonly" />
    </div>
  </div>
</form>

        
</div>
