<?php  $active = $this->input->get('t');?>
<?php
    // Set if submition fiald showing data in fields which is user filled 
	$session_data =  $this->session->flashdata();
	
	if($this->session->flashdata('err_message')){
?>

 <div class="alert alert-danger"><?php echo $this->session->flashdata('err_message'); ?></div>
<?php
	}//end if($this->session->flashdata('err_message'))
	
	if($this->session->flashdata('ok_message')){
?>
<div class="alert alert-success alert-dismissable"><?php echo $this->session->flashdata('ok_message'); ?></div>
<?php 
		}//if($this->session->flashdata('ok_message'))
?>
<div class="row">
  <div class="col-xs-12"> 
    <!-- PAGE CONTENT BEGINS -->
    <div class="tabbable"> 
      <!-- #section:pages/faq -->
      <ul class="nav nav-tabs padding-18" id="myTab">

          <li class="<?php if($active ==1 || $active==''){?>active<?php }?>">
            <a data-toggle="tab" href="#tab-business-listing"> <i class="blue ace-icon fa fa-h-square bigger-130"></i> Business Listing</a>
          </li>
          <li class="<?php if($active ==2){?>active<?php }?>">
	          <a data-toggle="tab" href="#tab-add-new-business"> <i class="blue ace-icon fa fa-h-square bigger-130"></i> Add New Business</a>
          </li>
 
      </ul>
      
      <!-- /section:pages/faq -->
      <div class="tab-content no-border padding-24">
          <div id="tab-business-listing" class="tab-pane fade in <?php if($active ==1 || $active==''){?>active<?php }?>">
            <h4 class="blue"> Business Listing </h4>
            <div class="table-responsive">
             <div style="margin-top:20px;"></div>
              <table class="table <?php echo (count($business_list) > 0) ? 'dynamic-table-business' : '' ?> table-striped table-bordered table-hover dataTable no-footer " role="grid" aria-describedby="dynamic-table_info">

  	          	<thead>
  	          	 <tr>
                    <th> Register Date</th>
                    <th> Business Name </th>
                    <th> Busines Type </th>
                    <th> No. of Pharmacies </th>
                    <th> Contact Name </th>
                    <th> Contact No. </th>
                    <th> Email </th>
                    
                    <th> Status </th>
                    <th> Action </th>
                  </tr>
  	          	</thead>
  	            <tbody>
                <?php if(!empty($business_list)) {
  					  	foreach($business_list as $each): 
  				  ?>
                    <tr>
                      <td><?php echo kod_date_format(filter_string($each['created_date'])); ?></td>
                      <td><?php echo ucfirst(filter_string($each['business_name']));?> (<?php echo $each['id']; ?>)</td>
                       <td> <?php if($each['business_type'] == 'B') { ?>Buying Group<?php } else {?>Chain<?php }?></td>
                      <td>
                      	<?php 
							$group_pharmaciy_list  = get_group_pharmacy_list($each['id']);
							echo count($group_pharmaciy_list);
						?>
                      </td>

                      <td><?php echo filter_string($each['contact_name']);?></td>
                      <td><?php echo filter_string($each['contact_no']);?></td>
                      <td><?php echo filter_string($each['email_address']);?></td>
                      <td> <?php if($each['status'] == '1') { ?>Active<?php } else {?>Deactive<?php }?></td>
                      <td width="10%">
                      <a class="fancybox_view fancybox.ajax" href="<?php echo base_url(); ?>business/edit-business/<?php echo $each['id'];?>"> <i class="ace-icon fa fa-pencil bigger-130" title="Edit Business"></i> </a>
                      
                      <a  href="#" data-href="<?php echo base_url(); ?>business/delete-business/<?php echo $each['id']; ?>" title="Delete" data-toggle="modal" data-target="#confirm-delete-<?php echo $each['id']; ?>"> <i class="ace-icon fa fa-trash-o bigger-130"></i></a>
                      
                      
                      <div class="modal fade" id="confirm-delete-<?php echo $each['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  								<div class="modal-dialog">
  									<div class="modal-content">
  										<div class="modal-header">
  											Warning !
  										</div>
  										<div class="modal-body">
  											Are you sure you want to delete this business ?
  										</div>
  										<div class="modal-footer">
  											<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
  											<a href="<?php echo base_url(); ?>business/delete-business/<?php echo $each['id']; ?>" class="btn btn-danger btn-ok">Delete</a>
  										</div>
  									</div>
  								</div>
  							</div>
                  
                      </td>
                      
                    </tr>
                  <?php 
  						endforeach; // foreach
  					 }  else { ?>
                     
                      <tr>
                               <td colspan="8" class="text-danger">No record found</td>
                     </tr>
                     
  			   <?php } ?>		 
                   </tbody>
  	          </table>
            </div>
          </div>
          <div id="tab-add-new-business" class="tab-pane fade in <?php if($active ==2){?>active<?php }?>">
          <h4 class="blue"> Create New Business</h4>          
          <div class="space-20"></div>
          <form id="add_business_frm" name="add_business_frm" method="post" enctype="multipart/form-data" action="<?php echo SURL;?>business/add-new-business-process"> 
          
              <div class="row">
                  <label for="business_type" class="col-sm-2 control-label">Business Type</label>
                  <div class="form-group col-sm-10">
                      <select name="business_type" id="business_type"  required="required" class="form-control">
                      <option value="">Select Business Type</option>
                      <option value="B" <?php if($session_data['business_type']=='B'){?> selected="selected"<?php }?>>Buying Group</option>
                      <option value="C" <?php if($session_data['business_type']=='C'){?> selected="selected"<?php }?>>Chain</option>
                      </select>
                  </div>
                </div>
          
              <div class="row">
                  <label for="pharmacy_name" class="col-sm-2 control-label">Business name</label>
                  <div class="form-group col-sm-10">
                    <input type="text" class="form-control" name="business_name" id="business_name" required="required" placeholder="Business name" value="<?php echo $session_data['business_name'];?>" data-fv-regexp="true" data-fv-regexp-regexp="^[a-zA-z0-9\s\-,\]+[a-zA-Z0-9\s\-_,.'/]{1,100}$" data-fv-regexp-message="Please use allowed characters (Alphabet, Numbers, Hyphens, Underscore, Comma, Apostrophe, Dot, Forward Slash, Back Slash, Space)" maxlength="100">
                  </div>
              </div>
                
              <div class="row">
                  <label for="contact_name" class="col-sm-2 control-label"> Contact name </label>
                  
                  <div class="form-group col-sm-10">
                   <input type="text"  class="form-control" name="contact_name" id="contact_name" placeholder="Contact name" value="<?php echo $session_data['contact_name'];?>"   aria-required="true"  required data-fv-regexp="true" data-fv-regexp-regexp="^[a-zA-z0-9\s\-,\]+[a-zA-Z0-9\s\-_,.'/]{1,50}$" data-fv-regexp-message="Please use allowed characters (Alphabet, Numbers, Hyphens, Underscore, Comma, Apostrophe, Dot, Forward Slash, Back Slash, Space)" maxlength="50">
                  </div>
              </div> 
              
               <div class="row">
                  <label for="contact_name" class="col-sm-2 control-label"> Contact number </label>
                  
                  <div class="form-group col-sm-10">
                  <input type="text" class="form-control" name="contact_no" id="contact_no" placeholder="Contact number" value="<?php echo $session_data['contact_no'];?>" aria-required="true"  required data-fv-regexp="true"  data-fv-regexp-regexp="^(?=.*[0-9])[0-9]{11,}$" data-fv-regexp-message="Please use allowed characters (Numbers) and length should be minimum 11 characters"  maxlength="15">
                  </div>
              </div>               
              <div class="row">
                  <label for="contact_name" class="col-sm-2 control-label"> Email address </label>
                  
                  <div class="form-group col-sm-10">
                    <input type="email"  class="form-control" name="email_address" id="email_address" value="<?php echo $session_data['email_address'];?>" placeholder="Email address" aria-required="true"  required data-fv-regexp="true" data-fv-regexp-regexp="^[a-zA-z0-9\s\-]+[a-zA-Z0-9\s\-_.@]+$" data-fv-regexp-message="Please use allowed characters (Alphabet, Numbers, Hyphens, Underscore, Dot and @)" autocomplete="off" maxlength="50">
                  </div>
              </div>
              <div class="row">
                  <label for="contact_name" class="col-sm-2 control-label"> Password </label>
                  <div class="form-group col-sm-10">
                    <input type="password"  class="form-control"  name="password" id="password" placeholder="Password"   required data-fv-regexp="true" data-fv-regexp-regexp="^^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[A-Za-z0-9]{8,20}$" data-fv-regexp-message="Password must be between 8 to 20 characters with atleast one uppercase, one lowercase and one digit. Allowed characters (Alphabet, Numbers)" maxlength="20">
                  </div>
              </div>
              
              <div class="row">
                  <label for="contact_name" class="col-sm-2 control-label">Retype Password</label>
                  
                  <div class="form-group col-sm-10">
                   <input type="password"  class="form-control" name="re_password" id="re_password" placeholder="Retype password"  aria-required="true"   data-fv-identical="true" data-fv-identical-field="password" data-fv-identical-message="The password and its confirm password does not match" maxlength="20" >
                  </div>
              </div>
              
               <div class="row">
                  <label for="status" class="col-sm-2 control-label">Status</label>
                  <div class="form-group col-sm-10">
                      <select name="status" id="status"  required="required" class="form-control">
                      <option value="1">Active</option>
                      <option value="0">InActive</option>
                      </select>
                  </div>
                </div>

             <div class="pull-right">
                  <button class="btn btn-success btn-sm" type="submit" id="new_business_btn" name="new_business_btn"> <i class="ace-icon fa fa-floppy-o bigger-110"></i> Save </button>
                  <button class="btn btn-danger btn-sm" type="reset"> <i class="ace-icon fa fa-refresh bigger-110"></i> Clear </button>
             </div>  
          </form>
         </div>
      </div>
    </div>
  </div>
</div>
