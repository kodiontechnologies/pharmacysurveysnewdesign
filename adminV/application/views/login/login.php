				<div class="row">
                    <div class="col-sm-10 col-sm-offset-1">
                        <div class="login-container">
                            <div class="text-center">
                                <img src="<?php echo IMAGES; ?>logo-dark.png" />
                            </div>

                            <div class="space-6"></div>

                            <div class="position-relative">
                                <div id="login-box" class="login-box visible widget-box no-border">
                                    <div class="widget-body">
                                        <div class="widget-main">
                                            <h4 class="header blue lighter bigger">
                                                <i class="ace-icon fa fa-coffee green"></i>
                                                Please Enter Your Information
                                            </h4>

                                            <div class="space-6"></div>

<?php
                                                if($this->session->flashdata('err_message')){
?>
                                                   <div class="alert alert-danger"><?php echo $this->session->flashdata('err_message'); ?></div>
<?php
                                                }//end if($this->session->flashdata('err_message'))
                                                
                                                if($this->session->flashdata('ok_message')){
?>
                                                    <div class="alert alert-success alert-dismissable"><?php echo $this->session->flashdata('ok_message'); ?></div>
<?php 
                                                }//if($this->session->flashdata('ok_message'))
?>

                                            <form action="<?php echo SURL?>login/login_process" method="post" id="login_frm" name="login_frm">
                                                <fieldset>
                                                    <label class="block clearfix">
                                                        <span class="form-group block input-icon input-icon-right">
                                                            <input type="text" name="email_address" class="form-control" placeholder="Email address" required="required" />
                                                            <i class="ace-icon fa fa-user"></i>
                                                        </span>
                                                    </label>

                                                    <label class="block clearfix">
                                                        <span class="form-group block input-icon input-icon-right">
                                                            <input type="password" name="password" class="form-control" placeholder="Password" required="required" />
                                                            <i class="ace-icon fa fa-lock"></i>
                                                        </span>
                                                    </label>

                                                    <div class="space"></div>

                                                    <!-- Captcha -->
                                                    <div class="g-recaptcha" data-sitekey="6LdasR4TAAAAAIIucnPPHLt6TNFQsPz9Kdk6q0hj"></div><br />

                                                    <div class="clearfix">
                                                       
                                                        <!-- Submit button -->
                                                        <button type="submit" class="width-35 pull-right btn btn-sm btn-primary" id="login_btn">
                                                            <i class="ace-icon fa fa-key"></i>
                                                            <span class="bigger-110">Login</span>
                                                        </button>
                                                    </div>

                                                    <div class="space-4"></div>
                                                </fieldset>
                                            </form>

                                            <!--
                                            <div class="social-or-login center">
                                                <span class="bigger-110">Or Login Using</span>
                                            </div>

                                            <div class="space-6"></div>
                                            <div class="social-login center">
                                                <a class="btn btn-primary">
                                                    <i class="ace-icon fa fa-facebook"></i>
                                                </a>

                                                <a class="btn btn-info">
                                                    <i class="ace-icon fa fa-twitter"></i>
                                                </a>

                                                <a class="btn btn-danger">
                                                    <i class="ace-icon fa fa-google-plus"></i>
                                                </a>
                                            </div>
                                            -->

                                        </div><!-- /.widget-main -->

                                        <div class="toolbar clearfix">
                                            <div>
                                                <a href="<?php echo SURL?>login/forgot-password" class="forgot-password-link">
                                                    <i class="ace-icon fa fa-arrow-left"></i>
                                                    I forgot my password
                                                </a>
                                            </div>
                                        </div>
                                    </div><!-- /.widget-body -->
                                </div><!-- /.login-box -->
                           
                            </div><!-- /.position-relative -->

                        </div>
                    </div><!-- /.col -->
                </div><!-- /.row -->