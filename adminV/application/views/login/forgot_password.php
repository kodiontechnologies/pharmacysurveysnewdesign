 <div class="row">
    <div class="col-sm-10 col-sm-offset-1">
                        <div class="login-container">
                             <div class="text-center">
                                <img src="<?php echo IMAGES; ?>logo-dark.png" />
                            </div>

                            <div class="space-6"></div>

                            <div class="position-relative">
                               
                                <div id="forgot-box" class="forgot-box visible widget-box no-border">
                                    <div class="widget-body">
                                        <div class="widget-main">
                                            <h4 class="header red lighter bigger">
                                                <i class="ace-icon fa fa-key"></i>
                                                Retrieve Password
                                            </h4>

                                            <div class="space-6"></div>
                                         	 <?php 
												if($this->session->flashdata('err_message')){
											 ?>
											 <div class="alert alert-danger"><?php echo $this->session->flashdata('err_message'); ?></div>
											 <?php
												}//end if($this->session->flashdata('err_message'))
												if($this->session->flashdata('ok_message')){
											 ?>
											 <div class="alert alert-success alert-dismissable"><?php echo $this->session->flashdata('ok_message'); ?></div>
											 <?php 
												}//if($this->session->flashdata('ok_message'))
											 ?>

                                             <p>
                                                Enter your email and to receive instructions
                                            </p>

                                            <form action="<?php echo SURL?>login/forgot-password-process" method="post" enctype="multipart/form-data" name="forgot_password_frm" id="forgot_password_frm">
                                                <fieldset>
                                                    <label class="block clearfix">
                                                        <span class="form-group block input-icon input-icon-right">
                                                            <input type="email" class="form-control" placeholder="Email" id="email_address" name="email_address" required="required" />
                                                            <i class="ace-icon fa fa-envelope"></i>
                                                        </span>
                                                    </label>
                                                    
                                                    <div class="space"></div>

                                                    <!-- Captcha -->
                                                    <div class="g-recaptcha" data-sitekey="6LdasR4TAAAAAIIucnPPHLt6TNFQsPz9Kdk6q0hj"></div><br />

                                                    <div class="clearfix">
                                                        <button type="submit" id="fgetpass_btn" name="fgetpass_btn" class="width-35 pull-right btn btn-sm btn-danger">
                                                            <i class="ace-icon fa fa-lightbulb-o"></i>
                                                            <span class="bigger-110">Send Me!</span>
                                                        </button>
                                                    </div>

                                                </fieldset>
                                            </form>
                                        </div><!-- /.widget-main -->

                                        <div class="toolbar center">
                                        
                                        <a href="<?php echo SURL?>login" class="back-to-login-link">
                                                Back to login
                                                <i class="ace-icon fa fa-arrow-right"></i>
                                            </a>
                                        </div>

                                    </div><!-- /.widget-body -->

                                </div><!-- /.forgot-box -->

                            </div><!-- /.position-relative -->

                        </div>
                    </div><!-- /.col -->
  </div><!-- /.row -->