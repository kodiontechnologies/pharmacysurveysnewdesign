<?php
	$menu_listing = menu_listing();
	$get_usertype_list = get_usertype_list();
	//print_this($get_usertype_list);
?>
<?php 
  if($this->session->flashdata('err_message')){
?>
  <p class="alert alert-danger"><i class="red icon-only ace-icon fa fa-times"></i> <?php echo $this->session->flashdata('err_message'); ?></p>
<?php
  }//end if($this->session->flashdata('err_message'))
  
  if($this->session->flashdata('ok_message')){
?>
   <p class="alert alert-success"><i class="green icon-only ace-icon fa fa-check"></i> <?php echo $this->session->flashdata('ok_message'); ?></p>
<?php 
  }//if($this->session->flashdata('ok_message'))
?>
<div class="row">
    <div class="col-md-3">
        <form name="permission_frm" id="permission_frm" method="post" action="<?php echo SURL?>users/privilages">
            <h5>Select Uer Role</h5>
          	<select name="user_type_slt" id="user_type_slt" class="form-control">
              <option value="">--- Choose ---</option>

<?php                 for($i=0;$i<count($get_usertype_list);$i++){ ?>
                  
                  <option <?php echo ($user_type_id == $get_usertype_list[$i]['id']) ? 'selected="selected"' : ''?>  value="<?php echo filter_string($get_usertype_list[$i]['id'])?>"><?php echo filter_string($get_usertype_list[$i]['user_type'])?></option>

<?php                 }//end for ?>
            </select>
        </form>
    </div>
    <div class="col-md-3" id="select_responsibility">
        <h5>Select Responsibility</h5>
        <select name="user_role_responsibility" id="user_role_responsibility" class="form-control responsibility-slt" onchange="javascript:;" >
            <option value="">Default</option>
            <?php if($pharmacy_user_responsibilities){ ?>
              <?php foreach($pharmacy_user_responsibilities as $responsibility){ ?>
                <option <?php echo ($responsibility_id && $responsibility_id == $responsibility['id']) ? 'selected="selected"' : '' ; ?> value="<?php echo $responsibility['id']; ?>">
                  <?php echo $responsibility['responsibility_type']; ?>
                </option>
              <?php } // foreach($pharmacy_user_responsibilities as $responsibility) ?>
            <?php } // if($pharmacy_user_responsibilities) ?>
         </select>
    </div>
    <div class="col-md-3">
      <h5>&nbsp;</h5>
      <button onClick="view_results();" class="btn btn-sm btn-success">View Privilliges</button>
    </div>  
    <!--
    <div class="col-md-6 text-left">
        <br /> <br />
        <a href="<?php echo base_url(); ?>users/add-new-user-type" class="btn btn-sm btn-success"> Add New </a>
        
        <?php if($user_type_id != '' && $user_type_id !='9' ){ ?>
           
            <a href="#" data-href="<?php echo base_url(); ?>users/delete-user-type/<?php echo $user_type_id; ?>" title="Delete" data-toggle="modal" data-target="#confirm-delete-<?php echo $user_type_id; ?>"> <button class="btn btn-sm btn-danger"> Delete </button></a>
            
             <div class="modal fade" id="confirm-delete-<?php echo $user_type_id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            Warning !
                        </div>
                        <div class="modal-body">
                            Are you sure you want to delete this user type ?
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <a href="<?php echo base_url(); ?>users/delete-user-type/<?php echo $user_type_id; ?>" class="btn btn-danger btn-ok">Delete</a>
                        </div>
                    </div>
                </div>
            </div>
        <?php } // if($user_type_id != '') ?>
    </div>
    -->

</div>

<?php
	  if($user_type_id){

  		$user_type_prv = get_usertype_permissions($user_type_id, $responsibility_id);

  		$user_type_prv_arr = explode(',',$user_type_prv[0]['permissions']);
?>
      <form id="update_prv_frm" name="update_prv_frm" method="post" action="<?php echo SURL?>users/add-user-type-process">

        <br />
        <!--
        <div class="row">
            <div class="col-md-12">
                    
                <label for=""> Name </label>
                <div class="form-group">
                <input type="text" name="user_type_name" placeholder="enter name" class="form-control" required="required" data-fv-regexp="true" data-fv-regexp-regexp="^[a-zA-z0-9\s\-,\]+[a-zA-Z0-9\s\-_,.'/]{1,50}$" data-fv-regexp-message="Please use allowed characters (Alphabet, Numbers, Hyphens, Underscore, Comma, Apostrophe, Dot, Forward Slash, Back Slash, Space)" maxlength="50" value="< ?php echo ($user_type_prv[0]['user_type']) ? $user_type_prv[0]['user_type'] : '' ; ?>" />
                </div>

                <label for=""> Description </label>
                <div class="form-group">
                <input type="text" name="user_type_desc" placeholder="enter description" class="form-control"  data-fv-regexp="true" data-fv-regexp-regexp="^[a-zA-z0-9\s\-,\]+[a-zA-Z0-9\s\-_,.%'/]{1,150}$" data-fv-regexp-message="Please use allowed characters (Alphabet, Numbers, Hyphens, Underscore, Comma, Apostrophe, Dot, Forward Slash, Back Slash, Space, Percentage)" maxlength="150" value="< ?php echo ($user_type_prv[0]['description']) ? $user_type_prv[0]['description'] : '' ; ?>" />
                </div>

            </div>
        </div>
        -->

        <?php	
            for($i=0;$i<count($menu_listing);$i++){
        ?>
            <div class="row">
                <div class="col-md-12">

                    <h4>
                        <div class="checkbox">
                            <label class="block">
                                <input <?php echo in_array($menu_listing[$i]['menu_id'],$user_type_prv_arr) ? 'checked="checked"' : ''?> name="menu_chk[]" type="checkbox" class="ace input-lg" value="<?php echo $menu_listing[$i]['menu_id']?>"  id="checkAll-<?php echo $menu_listing[$i]['menu_id'];?>"/>
                                <span class="lbl bigger-120"> <?php echo $menu_listing[$i]['menu_name']?></span>
                            </label>
                        </div>
                    </h4>
                    <hr />
                    <?php
                        for($j=0;$j<count($menu_listing[$i]['submenu_arr']);$j++){
                    ?>
                            <div class="checkbox">
                                <label>
                                    <input <?php echo in_array($menu_listing[$i]['submenu_arr'][$j]['id'],$user_type_prv_arr) ? 'checked="checked"' : ''?> name="menu_chk[]" class="ace ace-checkbox-2 sub-menu-<?php echo $menu_listing[$i]['menu_id'];?>" type="checkbox" value="<?php echo $menu_listing[$i]['submenu_arr'][$j]['id']?>" />
                                    <span class="lbl"> <?php echo $menu_listing[$i]['submenu_arr'][$j]['title'];?></span>
                                </label>
                            </div>
                    <?php			
                        }//end for($j=0;$j<count($menu_listing[$i]['submenu_arr']);$j++)
                    ?>
                    
                </div>
            </div>
		 <script>
          
		  // Checked All None verify users
			$("#checkAll-"+<?php echo $menu_listing[$i]['menu_id'];?>).change(function () {
				
				$(".sub-menu-"+<?php echo $menu_listing[$i]['menu_id'];?>).prop('checked', $(this).prop("checked"));
			});
			
        </script>
        <?php
            } //end for($i=0;$i<count($menu_listing);$i++)
        ?>
        <hr />
        <button type="submit" class="btn btn-sm btn-success pull-right" name="upd_priv_btn" id="upd_priv_btn" value="Update" >Update</button>
        <input type="hidden" name="usertype_id" id="usertype_id" value="<?php echo $user_type_id; ?>" />
        <input type="hidden" name="responsibility_id" id="responsibility_id" value="<?php echo $responsibility_id; ?>" readonly="readonly" />

        <?php if(!$user_type_prv){ ?>

          <?php
          $usertype_name = get_usertype_list($user_type_id);
          if($pharmacy_user_responsibilities){
              foreach($pharmacy_user_responsibilities as $responsibility){
                if($responsibility['id'] == $responsibility_id)
                  $responsibility_name = $responsibility['responsibility_type'];
              } // foreach($pharmacy_user_responsibilities as $responsibility)
          } // if($pharmacy_user_responsibilities)
          ?>
          <input type="hidden" name="usertype_name" id="usertype_name" value="<?php echo ($usertype_name[0]['user_type']) ? $usertype_name[0]['user_type'] : '' ; ?>" readonly="readonly" />

          <input type="hidden" name="responsibility_name" id="responsibility_name" value="<?php echo ($responsibility_name) ? $responsibility_name : '' ; ?>" readonly="readonly" />
          
        <?php } //  ?>

    </form>

<?php }?>
<script>

    function view_privillages(el){

      var user_role_id = $('#user_type_slt').val();
      var user_role_responsibility_id = $('#user_role_responsibility').val();

      if($(el).val() != '')
          location.href = SURL+"users/privilages/"+user_role_id+'/'+user_role_responsibility_id;
      else
          location.href = SURL+"users/privilages/";
      // if($(el).val() != '')

    } // function view_privillages(el)

    // Start => view_privillages_responsibility()
    function view_privillages_responsibility(){
      
      var user_role_id = $('#user_type_slt').val();
      var user_role_responsibility_id = $('#user_role_responsibility').val();

      if(user_role_responsibility_id != '' && user_role_id != '')
          location.href = SURL+"users/privilages/"+user_role_id+'/'+user_role_responsibility_id;
      else if(user_role_id != '')
          location.href = SURL+"users/privilages/"+user_role_id;
      else if(user_role_id == '')
        location.href = SURL+"users/privilages/";
      // if($(el).val() != '')

    } // End => view_privillages_responsibility()

    function view_results(){
      var user_role_id = $('#user_type_slt').val();
      var user_role_responsibility_id = $('#user_role_responsibility').val();

      if(user_role_responsibility_id != '' && user_role_id != '')
          location.href = SURL+"users/privilages/"+user_role_id+'/'+user_role_responsibility_id;
      else if(user_role_id != '')
          location.href = SURL+"users/privilages/"+user_role_id;
      else if(user_role_id == '')
        location.href = SURL+"users/privilages";
      // if($(el).val() != '')
    }

    $('#user_type_slt').change(function(e){

        if($(this).val() != ''){
          $('#select_responsibility').show();
        }else{
          $('#select_responsibility').hide();
        } // if($(e).val() != '')

        ////////////////////////////
        ///////// Set Rules ////////
        ////////////////////////////
        
        if($(this).val() == '1'){
          
          // Doctor
          $(".responsibility-slt option[value='1']").remove();

          //$(".responsibility-slt option[value='2']").show();
          //$(".responsibility-slt option[value='3']").show();

          if($(".responsibility-slt option[value='2']").length == 0)
            $('.responsibility-slt').append($('<option></option>').val('2').html('Manager'));
          if($(".responsibility-slt option[value='3']").length == 0)
            $('.responsibility-slt').append($('<option></option>').val('3').html('Locum'));

          if($(".responsibility-slt option[value='4']").length == 0)
            $('.responsibility-slt').append($('<option></option>').val('4').html('Administrator'));

        } else if($(this).val() == '2'){
          
          // Pharmacist
          /*$(".responsibility-slt option[value='1']").show();
          $(".responsibility-slt option[value='2']").show();
          $(".responsibility-slt option[value='3']").show();*/

          if($(".responsibility-slt option[value='1']").length == 0)
            $('.responsibility-slt').append($('<option></option>').val('1').html('Superintendent'));
          if($(".responsibility-slt option[value='2']").length == 0)
            $('.responsibility-slt').append($('<option></option>').val('2').html('Manager'));
          if($(".responsibility-slt option[value='3']").length == 0)
            $('.responsibility-slt').append($('<option></option>').val('3').html('Locum'));

          if($(".responsibility-slt option[value='4']").length == 0)
            $('.responsibility-slt').append($('<option></option>').val('4').html('Administrator'));

        } else if($(this).val() == '3'){

          // Inspector
          $(".responsibility-slt option[value='1']").remove();
          $(".responsibility-slt option[value='2']").remove();
          $(".responsibility-slt option[value='3']").remove();
          $(".responsibility-slt option[value='4']").remove();

        } else if($(this).val() == '4'){

          // Technician
          $(".responsibility-slt option[value='1']").remove();

          //$(".responsibility-slt option[value='2']").show();
          //$(".responsibility-slt option[value='3']").show();

          if($(".responsibility-slt option[value='2']").length == 0)
            $('.responsibility-slt').append($('<option></option>').val('2').html('Manager'));
          if($(".responsibility-slt option[value='3']").length == 0)
            $('.responsibility-slt').append($('<option></option>').val('3').html('Locum'));

          if($(".responsibility-slt option[value='4']").length == 0)
            $('.responsibility-slt').append($('<option></option>').val('4').html('Administrator'));

        } else if($(this).val() == '5'){

          // Pre-reg
          $(".responsibility-slt option[value='1']").remove();
          $(".responsibility-slt option[value='3']").remove();

          //$(".responsibility-slt option[value='2']").show();

          if($(".responsibility-slt option[value='2']").length == 0)
            $('.responsibility-slt').append($('<option></option>').val('2').html('Manager'));

          if($(".responsibility-slt option[value='4']").length == 0)
            $('.responsibility-slt').append($('<option></option>').val('4').html('Administrator'));

        } else if($(this).val() == '6'){

          // Nurse
          $(".responsibility-slt option[value='1']").remove();

          //$(".responsibility-slt option[value='2']").show();
          //$(".responsibility-slt option[value='3']").show();

          if($(".responsibility-slt option[value='2']").length == 0)
            $('.responsibility-slt').append($('<option></option>').val('2').html('Manager'));
          if($(".responsibility-slt option[value='3']").length == 0)
            $('.responsibility-slt').append($('<option></option>').val('3').html('Locum'));

          if($(".responsibility-slt option[value='4']").length == 0)
            $('.responsibility-slt').append($('<option></option>').val('4').html('Administrator'));

        } else if($(this).val() == '7'){
          
          // Dispenser
          $(".responsibility-slt option[value='1']").remove();

          //$(".responsibility-slt option[value='2']").show();
          //$(".responsibility-slt option[value='3']").show();

          if($(".responsibility-slt option[value='2']").length == 0)
            $('.responsibility-slt').append($('<option></option>').val('2').html('Manager'));
          if($(".responsibility-slt option[value='3']").length == 0)
            $('.responsibility-slt').append($('<option></option>').val('3').html('Locum'));

          if($(".responsibility-slt option[value='4']").length == 0)
            $('.responsibility-slt').append($('<option></option>').val('4').html('Administrator'));

        } else if($(this).val() == '8'){
          // Others

          if($(".responsibility-slt option[value='2']").length == 0)
            $('.responsibility-slt').append($('<option></option>').val('2').html('Manager'));

          $(".responsibility-slt option[value='1']").remove();
          $(".responsibility-slt option[value='3']").remove();

          if($(".responsibility-slt option[value='4']").length == 0)
            $('.responsibility-slt').append($('<option></option>').val('4').html('Administrator'));

        } // } if($(this).val() == '1'){

        // $('#user_type_slt').trigger('change');

      }); // function

      $(window).on("load", function(){
        $('#user_type_slt').change();
      });

      $(document).ready(function(e){
        
        if($('#usertype_id').val() == undefined)
          $('#select_responsibility').hide();
        else
          $('#select_responsibility').show();
        
      });

</script>