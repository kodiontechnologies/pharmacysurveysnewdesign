<?php 
	$menu_listing = menu_listing();
	$get_usertype_list = false; //get_usertype_list();
	//print_this($get_usertype_list);
?>
<form id="add_prv_frm" name="add_prv_frm" method="post" action="<?php echo SURL?>users/add-user-type-process">

    <div class="row">
        <div class="col-md-12">
                
            <label for=""> Name </label>
            <div class="form-group">
            <input type="text" name="user_type_name" placeholder="enter name" class="form-control" required="required" data-fv-regexp="true" data-fv-regexp-regexp="^[a-zA-z0-9\s\-,\]+[a-zA-Z0-9\s\-_,.'/]{1,50}$" data-fv-regexp-message="Please use allowed characters (Alphabet, Numbers, Hyphens, Underscore, Comma, Apostrophe, Dot, Forward Slash, Back Slash, Space)" maxlength="50" />
			</div>
            
            <label for=""> Description </label>
            <div class="form-group">
            <input type="text" name="user_type_desc" placeholder="enter description" class="form-control"  data-fv-regexp="true" data-fv-regexp-regexp="^[a-zA-z0-9\s\-,\]+[a-zA-Z0-9\s\-_,.%'/]{1,150}$" data-fv-regexp-message="Please use allowed characters (Alphabet, Numbers, Hyphens, Underscore, Comma, Apostrophe, Dot, Forward Slash, Back Slash, Space, Percentage)" maxlength="150" />
            </div>

        </div>
    </div>

    <h3>Set Privillages</h3>

    <?php 
    	if(!$this->input->post('user_type_slt')){
    		
    		$user_type_prv = get_usertype_list($this->input->post('user_type_slt'));
    		$user_type_prv_arr = explode(',',$user_type_prv['permissions']);		
    ?>
            <?php	
                for($i=0;$i<count($menu_listing);$i++){
            ?>
                <div class="row">
                    <div class="col-md-12">

                        <h4>
                            <div class="checkbox">
                                <label class="block">
                                    <input <?php echo in_array($menu_listing[$i]['menu_id'],$user_type_prv_arr) ? 'checked="checked"' : ''?> name="menu_chk[]" type="checkbox" class="ace input-lg" value="<?php echo $menu_listing[$i]['menu_id']?>"  id="checkAll-<?php echo $menu_listing[$i]['menu_id'];?>"/>
                                    <span class="lbl bigger-120"> <?php echo $menu_listing[$i]['menu_name']?></span>
                                </label>
                            </div>
                        </h4>
                        <hr />
                        <?php
                            for($j=0;$j<count($menu_listing[$i]['submenu_arr']);$j++){
                        ?>
                                <div class="checkbox">
                                    <label>
                                        <input <?php echo in_array($menu_listing[$i]['submenu_arr'][$j]['id'],$user_type_prv_arr) ? 'checked="checked"' : ''?> name="menu_chk[]" class="ace ace-checkbox-2 sub-menu-<?php echo $menu_listing[$i]['menu_id'];?>" type="checkbox" value="<?php echo $menu_listing[$i]['submenu_arr'][$j]['id']?>" />
                                        <span class="lbl"> <?php echo $menu_listing[$i]['submenu_arr'][$j]['title']?></span>
                                    </label>
                                </div>
                        <?php			
                            }//end for($j=0;$j<count($menu_listing[$i]['submenu_arr']);$j++)
                        ?>
                        
                    </div>
                </div>
                
                 <script>
          
			  // Checked All None verify users
				$("#checkAll-"+<?php echo $menu_listing[$i]['menu_id'];?>).change(function () {
					
					$(".sub-menu-"+<?php echo $menu_listing[$i]['menu_id'];?>).prop('checked', $(this).prop("checked"));
				});
				
        </script>
            <?php
                } //end for($i=0;$i<count($menu_listing);$i++)
            ?>
            <hr />
            <button type="submit" class="btn btn-md btn-success pull-right" name="upd_priv_btn" id="upd_priv_btn" value="Save" >Save</button>
            <input type="hidden" name="usertype_id" id="usertype_id" value="<?php echo $this->input->post('user_type_slt') ?>" />
    </form>

<?php }?>