<div class="row" style="margin:0">
<div class="col-xs-12 nopadding">
<div class="pm-title">Pharmacy: <?php echo ucfirst(filter_string($get_pharmacy_details['pharmacy_name']));?></div>
</div>
</div>
<div class="space-10"></div>

<div class="row" style="margin:0">
    <div class="row" style="margin:0">
                                                   
            <!-- #section:pages/profile.info -->
            <div class="profile-user-info profile-user-info-striped">
                <div class="profile-info-row">
                    <div class="profile-info-name" style="width:115px; text-align:left"> Username</div>
    
                    <div class="profile-info-value">
                        <span class="editable" id="username"><?php echo ucfirst(filter_string($get_user_details['username']));?></span>
                    </div>
                </div>
                <div class="profile-info-row">
                    <div class="profile-info-name" style="width:115px; text-align:left"> First Name </div>
    
                    <div class="profile-info-value">
                        <span class="editable" id="username"><?php echo ucfirst(filter_string($get_user_details['first_name']));?></span>
                    </div>
                </div>
                
                  <div class="profile-info-row">
                    <div class="profile-info-name" style="width:115px; text-align:left"> Last Name </div>
    
                    <div class="profile-info-value">
                        <span class="editable" id="username"><?php echo ucfirst(filter_string($get_user_details['last_name']));?></span>
                    </div>
                </div>
                
                <div class="profile-info-row">
                    <div class="profile-info-name" style="width:115px; text-align:left"> Email Address </div>
    
                    <div class="profile-info-value">
                        <span class="editable" id="username"><?php echo filter_string($get_user_details['email_address']);?></span>
                    </div>
                </div>
    
               <div class="profile-info-row">
                    <div class="profile-info-name" style="width:115px; text-align:left"> Contact NO</div>
    
                    <div class="profile-info-value">
                        <span class="editable" id="city"><?php if($get_user_details['mobile_no'] !='') { echo filter_string($get_user_details['mobile_no']); } else { echo 'N/A'; } ?></span>
                    </div>
                </div>
                
                
                <div class="profile-info-row">
                    <div class="profile-info-name" style="width:115px; text-align:left"> Registration NO</div>
    
                    <div class="profile-info-value">
                        <span class="editable" id="city"><?php if($get_user_details['registration_no'] !='') { echo filter_string($get_user_details['registration_no']); } else { echo 'N/A'; } ?></span>
                    </div>
                </div>
                
                
               <div class="profile-info-row">
                    <div class="profile-info-name" style="width:115px; text-align:left"> Status</div>
    
                    <div class="profile-info-value">
                        <span class="editable" id="city"><?php echo ($get_user_details['status'] == 1) ? 'Active' : 'InActive' ?></span>
                    </div>
                </div>
                
            </div>
           <br />
          
        </div>
</div>
