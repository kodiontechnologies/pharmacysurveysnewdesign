<?php
    // Set if submition fiald showing data in fields which is user filled 
	$session_data =  $this->session->flashdata();
	
	if($this->session->flashdata('err_message')){
?>

 <div class="alert alert-danger"><?php echo $this->session->flashdata('err_message'); ?></div>
<?php
	}//end if($this->session->flashdata('err_message'))
	
	if($this->session->flashdata('ok_message')){
?>
<div class="alert alert-success alert-dismissable"><?php echo $this->session->flashdata('ok_message'); ?></div>
<?php 
		}//if($this->session->flashdata('ok_message'))
?>

<div class="row">
  <div class="col-xs-12"> 
    <!-- PAGE CONTENT BEGINS -->
    <div class="tabbable"> 
      <!-- #section:pages/faq -->
      <ul class="nav nav-tabs padding-18" id="myTab">
        <li class="active"> <a data-toggle="tab" href="#tab-users"> <i class="blue ace-icon fa fa-user bigger-130"></i> Users Listing</a> </li>
      </ul>
      
      <!-- /section:pages/faq -->
      <div class="tab-content no-border padding-24">
        <div id="tab-users" class="tab-pane fade in active">
          <h4 class="blue"> <?php echo filter_string($page_heading); ?> Users Listing </h4>
          <div class="table-responsive">
            <div style="margin-top:20px;"></div>
            <table class="table <?php echo (count($users_list) > 0) ? 'dynamic-table' : '' ?> table-striped table-bordered table-hover dataTable no-footer DTTT_selectable" role="grid" aria-describedby="dynamic-table_info">
              <thead>
                <tr>
                  <th>Username</th>
                  <th>First name</th>
                  <th>Last name</th>
                  <th>Email</th>
                  <th>Contact No</th>
                  <th>Registration No</th>
                  <th>Type</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php if(!empty($users_list)) {
				  	foreach($users_list as $each): 
			  ?>
                <tr>
                  <td><?php echo filter_string($each['username']);?></td>
                  <td><?php echo filter_string($each['first_name']);?></td>
                  <td><?php echo filter_string($each['last_name']);?></td>
                  <td><?php echo filter_string($each['email_address']);?></td>
                  <td><?php if($each['mobile_no'] !='') { echo filter_string($each['mobile_no']); } else { echo 'N/A'; } ?></td>
                  <td><?php if($each['registration_no'] !='') { echo filter_string($each['registration_no']); } else { echo 'N/A'; } ?></td>
                  <td><?php echo filter_string($each['usertype']);?></td>
                  <td><?php echo ($each['status'] == 1) ? 'Active' : 'InActive' ?></td>
                  <td>
                  	<div class="action-buttons"> 
                  		<a class="blue fancybox_view fancybox.ajax" href="<?php echo base_url(); ?>users/get-user-details/<?php echo $each['pharmacy_id'];?>/<?php echo $each['id'];?>"> <i class="ace-icon fa fa-search-plus bigger-130" title="View User Details"></i> </a> 
                        
                        <?php if($each['lock_time'] != NULL){?>
                        <a href="javascript:;" data-toggle="modal" data-target="#unlock-user-<?php echo $each['id']; ?>"> <i class="ace-icon fa fa-lock bigger-130" title="Unlock User"></i> </a> 
                        
                        	<div class="modal fade" id="unlock-user-<?php echo $each['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                Warning !
                                            </div>
                                            <div class="modal-body">
                                                Are you sure you want to unlock the user account?
                                            </div>
                                            <div class="modal-footer">
                                                <a type="button" class="btn btn-default" data-dismiss="modal">Cancel</a>
                                                <a href="<?php echo base_url(); ?>users/unlock-user/<?php echo filter_string($each['pharmacy_id']);?>/<?php echo $each['id']; ?>" class="btn btn-danger btn-ok">Confirm</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        
                        <?php }?>
                        
                    </div>
                  </td>
                </tr>
                <?php 
						endforeach; // foreach
					 } else { 
				?>
		                <tr><td colspan="7" class="text-danger">No record found</td></tr>
               <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
