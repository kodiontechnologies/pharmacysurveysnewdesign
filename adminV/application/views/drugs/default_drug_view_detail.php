<div class="row" style="margin:0">
<div class="col-xs-12 nopadding">
<div class="pm-title">Drug Details</div>
</div>
</div>
<div class="space-10"></div>

<div class="row" style="margin:0">  
   <div class="col-xs-12 col-sm-12">
                                                   
            <!-- #section:pages/profile.info -->
            <div class="profile-user-info profile-user-info-striped">
                
                <div class="profile-info-row">
                    <div class="profile-info-name" style="width:115px; text-align:left"> Drug Brand </div>
    
                    <div class="profile-info-value">
                        <span class="editable" id="username"><?php echo ucfirst(filter_string($get_drug_details['drug_brand']));?></span>
                    </div>
                </div>
                
                  <div class="profile-info-row">
                    <div class="profile-info-name" style="width:115px; text-align:left"> Drug Class </div>
    
                    <div class="profile-info-value">
                        <span class="editable" id="username"><?php echo ucfirst(filter_string($get_drug_details['drug_class']));?></span>
                    </div>
                </div>
                
                <div class="profile-info-row">
                    <div class="profile-info-name" style="width:115px; text-align:left"> Drug Strength </div>
    
                    <div class="profile-info-value">
                        <span class="editable" id="username"><?php echo filter_string($get_drug_details['drug_strength']);?></span>
                    </div>
                </div>
    
               <div class="profile-info-row">
                    <div class="profile-info-name" style="width:115px; text-align:left"> Drug Form</div>
    
                    <div class="profile-info-value">
                        <span class="editable" id="city"><?php echo filter_string($get_drug_details['drug_form']);?></span>
                    </div>
                </div>
                
                <div class="profile-info-row">
                    <div class="profile-info-name" style="width:115px; text-align:left"> Drug Units</div>
    
                    <div class="profile-info-value">
                        <span class="editable" id="city"><?php if($get_drug_details['drug_unit'] !='') { echo filter_string($get_drug_details['drug_unit']);} else { echo 'N/A';}?></span>
                    </div>
                </div>
                
            </div>
           <br />
          
        </div>
</div>
