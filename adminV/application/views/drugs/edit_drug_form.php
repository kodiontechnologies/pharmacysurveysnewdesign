<div class="row" style="margin:0">
<div class="col-xs-12 nopadding">
<div class="pm-title">Update Drug Form</div>
</div>
</div>
<div class="space-10"></div>

<div class="row" style="margin:0">

<div class="col-xs-12">

    <form  id="edit_drug_from_frm" name="edit_drug_from_frm" method="post" enctype="multipart/form-data" action="<?php echo SURL?>drugs/drug-form-process"> 
              
              <div class="row">
                <label for="add_new_drug_form" class="col-sm-2 control-label"> Drug Form </label>
                
                <div class=" form-group col-sm-10">
                  <input class="form-control" id="drug_form" name="drug_form" placeholder="drug form" type="text" value="<?php echo filter_string($edit_drug_form['drug_form']); ?>" required="required" 
                  data-fv-regexp="true" data-fv-regexp-regexp="^[a-zA-z0-9\s\-,\]+[a-zA-Z0-9\s\-_,.'/]{1,20}$"
                  data-fv-regexp-message="Please use allowed characters (Alphabet, Numbers, Hyphens, Underscore, Comma, Apostrophe, Dot, Forward Slash, Back Slash, Space)"
                  maxlength="20">
                </div>
              </div>
              
              <div class="row">
                <label for="add_new_drug_units" class="col-sm-2 control-label"> Drug Units </label>
                
                <div class="form-group col-sm-10">
                  <input class="form-control" id="drug_unit" name="drug_unit" placeholder="drug units" type="text" value="<?php echo filter_string($edit_drug_form['drug_unit']); ?>" required="required" 
                  data-fv-regexp="true" data-fv-regexp-regexp="^[a-zA-z0-9\s\-,\]+[a-zA-Z0-9\s\-_,.'/]{1,20}$"
                  data-fv-regexp-message="Please use allowed characters (Alphabet, Numbers, Hyphens, Underscore, Comma, Apostrophe, Dot, Forward Slash, Back Slash, Space)"
                  maxlength="20">
                </div>
              </div>
              <hr />
              
              <div class="pull-right">
                <button class="btn btn-success btn-sm" name="add_new_drug_form_btn" id="add_new_drug_form_btn"  type="submit"><i class="ace-icon fa fa-floppy-o bigger-110"></i> Update</button>				<input type="hidden" id="drug_id" name="drug_id" value="<?php echo $edit_drug_form['id']; ?>">
                <button class="btn btn-danger btn-sm" type="reset"><i class="ace-icon fa fa-refresh bigger-110"></i> Clear </button>
              </div>
              </form>
</div>

</div>

