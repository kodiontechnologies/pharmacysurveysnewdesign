<div class="row">
  <div class="col-xs-12"> 
    <!-- PAGE CONTENT BEGINS -->
    <div class="tabbable"> 
      <!-- #section:pages/faq -->
      <ul class="nav nav-tabs padding-18" id="myTab">

          <li class="active">
            <a data-toggle="tab" href="#tab-users"> <i class="blue ace-icon fa fa-user bigger-130"></i> Drugs Listing</a>
          </li>
 
      </ul>
      
      <!-- /section:pages/faq -->
      <div class="tab-content no-border padding-24">
        
          <div id="tab-users" class="tab-pane fade in active">
            <h4 class="blue">  <?php echo filter_string($page_heading); ?> Drugs Listing </h4>
            <div class="table-responsive">
             <div style="margin-top:20px;"></div>
              <table class="table <?php echo (count($drugs_list) > 0) ? 'dynamic-table' : '' ?> table-striped table-bordered table-hover dataTable no-footer DTTT_selectable" role="grid" aria-describedby="dynamic-table_info">
              <thead>
                <tr>
                  <th>Drug Brand</th>
                  <th>Drug Class</th>
                  <th>Drug Strength</th>
                  <th>Drug Form</th>
                  <th> Units </th>
                  <th> Drug Source </th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
              <?php if(!empty($drugs_list)) {
					  	foreach($drugs_list as $each): 

			  ?>		
                <tr>
                  <td><?php echo filter_string($each['drug_brand']);?></td>
                  <td><?php echo filter_string($each['drug_class']);?></td>
                  <td><?php echo filter_string($each['drug_strength']);?></td>
                  <td><?php echo filter_string($each['drug_form']);?></td>
                  <td><?php if($each['drug_unit'] !='') { echo filter_string($each['drug_unit']);} else { echo 'N/A';}?></td>
                  <td><?php echo ($each['is_default'] == '1') ? 'Default' : 'User'?></td>
                  <td>
                    <div class="hidden-sm hidden-xs action-buttons"> 
                    
                    		<a class="blue fancybox_view fancybox.ajax" href="<?php echo base_url(); ?>drugs/get-drugs-details/<?php echo $each['pharmacy_id'];?>/<?php echo $each['id'];?>"> <i class="ace-icon fa fa-search-plus bigger-130" title="View Drug Details"></i> </a>
                          
                      </div>
                  </td>
                  
                </tr>
				 <?php 
						endforeach; // foreach
					 }  else { ?>
                     
                       <tr>
                               <td colspan="6" class="text-danger">No record found</td>
                     </tr>
                     
  			   <?php } ?>	
              </tbody>
            </table>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>
