<div class="row" style="margin:0">
<div class="col-xs-12 nopadding">
<div class="pm-title">Update Drug</div>
</div>
</div>
<div class="space-10"></div>

<div class="row" style="margin:0">
<div class="col-xs-12">
  
    <form  id="edit_drug_frm" name="edit_drug_frm" method="post" enctype="multipart/form-data" action="<?php echo SURL?>drugs/add-new-pharmacy-default-drug-process"> 
              
              <div class="row">
                <label for="add_new_drug_brand" class="col-sm-2 control-label"> Drug Brand </label>
                
                <div class="form-group col-sm-10">
                  <input class="form-control" id="drug_brand" name="drug_brand" placeholder="drug brand" type="text" value="<?php echo filter_string($edit_drug['drug_brand']); ?>" required="required" 
                  data-fv-regexp="true" data-fv-regexp-regexp="^[a-zA-z0-9\s\-,\]+[a-zA-Z0-9\s\-_,.'/]{1,50}$"
                  data-fv-regexp-message="Please use allowed characters (Alphabet, Numbers, Hyphens, Underscore, Comma, Apostrophe, Dot, Forward Slash, Back Slash, Space)"
                  maxlength="50">
                </div>
              </div>
              
              <div class="row">
                <label for="add_new_drug_class" class="col-sm-2 control-label"> Drug Class </label>
                
                <div class="form-group col-sm-10">
                  <input class="form-control" id="drug_class" name="drug_class" placeholder="drug class" type="text" value="<?php echo filter_string($edit_drug['drug_class']); ?>" required="required" 
                  data-fv-regexp="true" data-fv-regexp-regexp="^[a-zA-z0-9\s\-,\]+[a-zA-Z0-9\s\-_,.'/]{1,50}$"
                  data-fv-regexp-message="Please use allowed characters (Alphabet, Numbers, Hyphens, Underscore, Comma, Apostrophe, Dot, Forward Slash, Back Slash, Space)"
                  maxlength="50">
                </div>
              </div>
              
              <div class="row">
                <label for="add_new_drug_strength" class="col-sm-2 control-label"> Drug Strength </label>
                
                <div class="form-group col-sm-10">
                  <input class="form-control" id="drug_strength" name="drug_strength" placeholder="drug strength" type="text" value="<?php echo filter_string($edit_drug['drug_strength']); ?>" required="required" 
                  data-fv-regexp="true" data-fv-regexp-regexp="^[a-zA-z0-9\s\-,\]+[a-zA-Z0-9\s\-_,.'/]{1,15}$"
                  data-fv-regexp-message="Please use allowed characters (Alphabet, Numbers, Hyphens, Underscore, Comma, Apostrophe, Dot, Forward Slash, Back Slash, Space)"
                  maxlength="15">
                </div>
              </div>
              
              <div class="row">
                <label for="add_new_drug_form" class="col-sm-2 control-label"> Drug Form </label>
                
                <div class="col-sm-10">
                  <select class="form-control" name="drug_form" id="drug_form">
                  	<?php 
						$drug_form_list = get_drug_forms();
						
						for($i=0;$i<count($drug_form_list);$i++){
					?>
							<option value="<?php echo filter_string($drug_form_list[$i]['id'])?>" <?php if($edit_drug['drug_form_id']==$drug_form_list[$i]['id']){ ?> selected="selected"<?php } ?>> <?php echo filter_string($drug_form_list[$i]['drug_form'])?> </option>
                    <?php		
						}//end for($i=0;$i<count($drug_form_list);$i++)
					?>
                  	
                  </select>
                </div>
            </div>
              <hr />
              
              <div class="pull-right">
                <button class="btn btn-success btn-sm" name="add_new_drug_btn" id="add_new_drug_btn"  type="submit"><i class="ace-icon fa fa-floppy-o bigger-110"></i> Update</button>				<input type="hidden" id="drug_id" name="drug_id" value="<?php echo $edit_drug['id']; ?>">
                <button class="btn btn-danger btn-sm" type="reset"><i class="ace-icon fa fa-refresh bigger-110"></i> Clear </button>
              </div>
              </form>
</div>
</div>

