<?php 
  if($this->session->flashdata('err_message')){
?>
  <p class="alert alert-danger"><i class="red icon-only ace-icon fa fa-times"></i> <?php echo $this->session->flashdata('err_message'); ?></p>
<?php
  }//end if($this->session->flashdata('err_message'))
  
  if($this->session->flashdata('ok_message')){
?>
   <p class="alert alert-success"><i class="green icon-only ace-icon fa fa-check"></i> <?php echo $this->session->flashdata('ok_message'); ?></p>
<?php 
  }//if($this->session->flashdata('ok_message'))
?>
<div class="row">
  <div class="col-xs-12"> 
    <!-- PAGE CONTENT BEGINS -->
    <div class="tabbable"> 
      <!-- #section:pages/faq -->
      <ul class="nav nav-tabs padding-18" id="myTab">

          <li class="active">
            <a data-toggle="tab" href="#tab-users"> <i class="blue ace-icon fa fa-user bigger-130"></i> Drugs Form Listing</a>
          </li>
          
           <li class="">
            <a data-toggle="tab" href="#tab-add-new-drug"><i class="green icon-only ace-icon fa fa-plus-circle bigger-130"></i> Add New Drug Form</a>
          </li>
 
      </ul>
      
      <!-- /section:pages/faq -->
      <div class="tab-content no-border padding-24">
        
          <div id="tab-users" class="tab-pane fade in active">
            <h4 class="blue"> Drugs Form Listing </h4>
            <div class="table-responsive">
             <div style="margin-top:20px;"></div>
              <table class="table <?php echo (count($drugs_form_list) > 0) ? 'dynamic-table' : '' ?> table-striped table-bordered table-hover dataTable no-footer DTTT_selectable" role="grid" aria-describedby="dynamic-table_info">
              <thead>
                <tr>
                  <th>Drug Form</th>
                  <th>Drug Units</th>
                  <th>Created Date</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
              <?php if(!empty($drugs_form_list)) {
					  	foreach($drugs_form_list as $each): 
			  ?>		
                <tr>
                  <td><?php echo filter_string($each['drug_form']);?></td>
                  <td><?php echo filter_string($each['drug_unit']);?></td>
                  <td><?php echo kod_date_format(filter_string($each['created_date'])); ?></td>
                  <td class=" "><?php echo ($each['status'] == 1) ? 'Active' : 'InActive' ?></td>
                  <td>
                    <div class="hidden-sm hidden-xs action-buttons"> 
                    
                    		<!--<a class="blue fancybox_view fancybox.ajax" href="<?php echo base_url(); ?>drugs/get-drug-form-details/<?php echo $each['id'];?>"> <i class="ace-icon fa fa-search-plus bigger-130" title="View Drug form Details"></i> </a>-->
                            
                          	<a class="green fancybox_view fancybox.ajax" href="<?php echo base_url(); ?>drugs/add-edit-drug-form/<?php echo $each['id'];?>"> <i class="ace-icon fa fa-pencil bigger-130" title="Edit"></i> </a> 
                          
                         	<a  href="#" data-href="<?php echo base_url(); ?>drugs/delete-drug-form/<?php echo $each['id']; ?>" title="Delete" data-toggle="modal" data-target="#confirm-delete-<?php echo $each['id']; ?>"> <i class="ace-icon fa fa-trash-o bigger-130"></i></a> 
                          
                      </div>
                      
                  </td>
                  <div class="modal fade" id="confirm-delete-<?php echo $each['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  								<div class="modal-dialog">
  									<div class="modal-content">
  										<div class="modal-header">
  											Warning !
  										</div>
  										<div class="modal-body">
  											Are you sure you want to delete this drug form ?
  										</div>
  										<div class="modal-footer">
  											<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
  											<a href="<?php echo base_url(); ?>drugs/delete-drug-form/<?php echo $each['id']; ?>" class="btn btn-danger btn-ok">Delete</a>
  										</div>
  									</div>
  								</div>
  							</div>
                  
                </tr>
				 <?php 
						endforeach; // foreach
					 }  else { ?>
                     
                       
                       <tr>
                               <td colspan="5" class="text-danger">No record found</td>
                     </tr>
                     
  			   <?php } ?>	
              </tbody>
            </table>
            </div>
          </div>

          <div class="tab-pane fade in" id="tab-add-new-drug">
             <h4 class="blue no-padding">Add New Drug</h4>
            
  			<form  id="add_drug_form_frm" name="add_drug_form_frm" method="post" enctype="multipart/form-data" action="<?php echo SURL?>drugs/drug-form-process"> 
              
              <div class="row">
                <label for="add_new_drug_form" class="col-sm-2 control-label"> Drug Form </label>
                
                <div class=" form-group col-sm-10">
                  <input class="form-control" id="drug_form" name="drug_form" placeholder="drug form" type="text" required="required" 
                  data-fv-regexp="true" data-fv-regexp-regexp="^[a-zA-z0-9\s\-,\]+[a-zA-Z0-9\s\-_,.'/]{1,20}$"
                  data-fv-regexp-message="Please use allowed characters (Alphabet, Numbers, Hyphens, Underscore, Comma, Apostrophe, Dot, Forward Slash, Back Slash, Space)"
                  maxlength="20">
                </div>
              </div>
              
              <div class="row">
                <label for="add_new_drug_units" class="col-sm-2 control-label"> Drug Units </label>
                
                <div class="form-group col-sm-10">
                  <input class="form-control" id="drug_unit" name="drug_unit" placeholder="drug units" type="text" required="required" 
                  data-fv-regexp="true" data-fv-regexp-regexp="^[a-zA-z0-9\s\-,\]+[a-zA-Z0-9\s\-_,.'/]{1,20}$"
                  data-fv-regexp-message="Please use allowed characters (Alphabet, Numbers, Hyphens, Underscore, Comma, Apostrophe, Dot, Forward Slash, Back Slash, Space)"
                  maxlength="20">
                </div>
              </div>
              <hr />
              
              <div class="pull-right">
                <button class="btn btn-success btn-sm" name="add_new_drug_form_btn" id="add_new_drug_form_btn"  type="submit"><i class="ace-icon fa fa-floppy-o bigger-110"></i> Save</button>
                <button class="btn btn-danger btn-sm" type="reset"><i class="ace-icon fa fa-refresh bigger-110"></i> Clear </button>
              </div>
              </form>

          </div>
      </div>
    </div>
  </div>
</div>
