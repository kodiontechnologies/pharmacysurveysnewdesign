<?php 
  if($this->session->flashdata('err_message')){
?>
  <p class="alert alert-danger"><i class="red icon-only ace-icon fa fa-times"></i> <?php echo $this->session->flashdata('err_message'); ?></p>
<?php
  }//end if($this->session->flashdata('err_message'))
  
  if($this->session->flashdata('ok_message')){
?>
   <p class="alert alert-success"><i class="green icon-only ace-icon fa fa-check"></i> <?php echo $this->session->flashdata('ok_message'); ?></p>
<?php 
  }//if($this->session->flashdata('ok_message'))
?>
<div class="row">
  <div class="col-xs-12"> 
    <!-- PAGE CONTENT BEGINS -->
    <div class="tabbable"> 
      <!-- #section:pages/faq -->
      <ul class="nav nav-tabs padding-18" id="myTab">

          <li class="active">
            <a data-toggle="tab" href="#tab-users"> <i class="blue ace-icon fa fa-user bigger-130"></i> Drugs Listing</a>
          </li>
          
           <li class="">
            <a data-toggle="tab" href="#tab-add-new-drug"><i class="green icon-only ace-icon fa fa-plus-circle bigger-130"></i> Add New Drug</a>
          </li>
 
      </ul>
      
      <!-- /section:pages/faq -->
      <div class="tab-content no-border padding-24">
        
          <div id="tab-users" class="tab-pane fade in active">
            <h4 class="blue"> Drugs Listing </h4>
            <div class="table-responsive">
             <div style="margin-top:20px;"></div>
              <table class="table <?php echo (count($default_drugs_list) > 0) ? 'dynamic-table' : '' ?> table-striped table-bordered table-hover dataTable no-footer DTTT_selectable" role="grid" aria-describedby="dynamic-table_info">
              <thead>
                <tr>
                  <th>Drug Brand</th>
                  <th>Drug Class</th>
                  <th>Drug Strength</th>
                  <th>Drug Form</th>
                  <th>Units</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
              <?php if(!empty($default_drugs_list)) {
					  	foreach($default_drugs_list as $each): 
			  ?>		
                <tr>
                  <td><?php echo filter_string($each['drug_brand']);?></td>
                  <td><?php echo filter_string($each['drug_class']);?></td>
                  <td><?php echo filter_string($each['drug_strength']);?></td>
                  <td><?php echo filter_string($each['drug_form']);?></td>
                  <td><?php if($each['drug_unit'] !='') { echo filter_string($each['drug_unit']);} else { echo 'N/A';}?></td>
                  <td>
                    <div class="hidden-sm hidden-xs action-buttons"> 
                    
                    		<a class="blue fancybox_view fancybox.ajax" href="<?php echo base_url(); ?>drugs/get-drug-deafult-details/<?php echo $each['id'];?>"> <i class="ace-icon fa fa-search-plus bigger-130" title="View Default Drug Details"></i> </a>
                            
                          	<a class="green fancybox_view fancybox.ajax" href="<?php echo base_url(); ?>drugs/add-edit-default-drug/<?php echo $each['id'];?>"> <i class="ace-icon fa fa-pencil bigger-130" title="Edit"></i> </a> 
                          
                         	<a  href="#" data-href="<?php echo base_url(); ?>drugs/delete-drug-deafult/<?php echo $each['id']; ?>" title="Delete" data-toggle="modal" data-target="#confirm-delete-<?php echo $each['id']; ?>"> <i class="ace-icon fa fa-trash-o bigger-130"></i></a> 
                          
                      </div>
                      
                  </td>
                  <div class="modal fade" id="confirm-delete-<?php echo $each['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  								<div class="modal-dialog">
  									<div class="modal-content">
  										<div class="modal-header">
  											Warning !
  										</div>
  										<div class="modal-body">
  											Are you sure you want to delete this drug ?
  										</div>
  										<div class="modal-footer">
  											<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
  											<a href="<?php echo base_url(); ?>drugs/delete-drug-deafult/<?php echo $each['id']; ?>" class="btn btn-danger btn-ok">Delete</a>
  										</div>
  									</div>
  								</div>
  							</div>
                  
                </tr>
				 <?php 
						endforeach; // foreach
					 }  else { ?>
                     
                      
                       <tr>
                               <td colspan="6" class="text-danger">No record found</td>
                     </tr>
                     
  			   <?php } ?>	
              </tbody>
            </table>
            </div>
          </div>

          <div class="tab-pane fade in" id="tab-add-new-drug">
             <h4 class="blue no-padding">Add New Drug</h4>
            
  			<form class="prevent_enter_key" id="add_edit_drug_frm" name="add_edit_drug_frm" method="post" enctype="multipart/form-data" action="<?php echo SURL?>drugs/add-new-pharmacy-default-drug-process"> 
              
              <div class="row">
                <label for="add_new_drug_brand" class="col-sm-2 control-label"> Drug Brand </label>
                
                <div class="form-group col-sm-10">
                  <input class="form-control" id="drug_brand" name="drug_brand" placeholder="drug brand" type="text" required="required" 
                  data-fv-regexp="true" data-fv-regexp-regexp="^[a-zA-z0-9\s\-,\]+[a-zA-Z0-9\s\-_,.'/]{1,50}$"
                  data-fv-regexp-message="Please use allowed characters (Alphabet, Numbers, Hyphens, Underscore, Comma, Apostrophe, Dot, Forward Slash, Back Slash, Space)"
                  maxlength="50">
                </div>
              </div>
              
              <div class="row">
                <label for="add_new_drug_class" class="col-sm-2 control-label"> Drug Class </label>
                
                <div class="form-group col-sm-10">
                  <input class="form-control" id="drug_class" name="drug_class" placeholder="drug class" type="text" required="required" 
                  data-fv-regexp="true" data-fv-regexp-regexp="^[a-zA-z0-9\s\-,\]+[a-zA-Z0-9\s\-_,.'/]{1,50}$"
                  data-fv-regexp-message="Please use allowed characters (Alphabet, Numbers, Hyphens, Underscore, Comma, Apostrophe, Dot, Forward Slash, Back Slash, Space)"
                  maxlength="50">
                </div>
              </div>
              
              <div class="row">
                <label for="add_new_drug_strength" class="col-sm-2 control-label"> Drug Strength </label>
                
                <div class="form-group col-sm-10">
                  <input class="form-control" id="drug_strength" name="drug_strength" placeholder="drug strength" type="text" required="required" 
                  data-fv-regexp="true" data-fv-regexp-regexp="^[a-zA-z0-9\s\-,\]+[a-zA-Z0-9\s\-_,.'/]{1,15}$"
                  data-fv-regexp-message="Please use allowed characters (Alphabet, Numbers, Hyphens, Underscore, Comma, Apostrophe, Dot, Forward Slash, Back Slash, Space)"
                  maxlength="15">
                </div>
              </div>
              
              <div class="row">
                <label for="add_new_drug_form" class="col-sm-2 control-label"> Drug Form </label>
                
                <div class="col-sm-10">
                  <select class="form-control" name="drug_form" id="drug_form">
                  	<?php 
						$drug_form_list = get_drug_forms();
						
						for($i=0;$i<count($drug_form_list);$i++){
					?>
							<option value="<?php echo filter_string($drug_form_list[$i]['id'])?>"> <?php echo filter_string($drug_form_list[$i]['drug_form'])?> </option>
                    <?php		
						}//end for($i=0;$i<count($drug_form_list);$i++)
					?>
                  	
                  </select>
                </div>
            </div>
              
              <hr />
              
              <div class="pull-right">
                <button class="btn btn-success btn-sm" name="add_new_drug_btn" id="add_new_drug_btn"  type="submit"><i class="ace-icon fa fa-floppy-o bigger-110"></i> Save</button>
                <button class="btn btn-danger btn-sm" type="reset"><i class="ace-icon fa fa-refresh bigger-110"></i> Clear </button>
              </div>
              </form>

          </div>
      </div>
    </div>
  </div>
</div>
