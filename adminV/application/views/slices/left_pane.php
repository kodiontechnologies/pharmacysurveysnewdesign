<ul class="nav nav-list">
       <li class="<?php if($this->uri->segment(1) == 'pharmacies'){echo 'active highlight';} else{ echo ""; } ?>">
        <a href="<?php echo base_url();?>pharmacies/pharmacies-list">
            <i class="menu-icon  fa fa-h-square"></i>
            Pharmacies
        </a>
        <b class="arrow"></b>
    </li>

       <li class="<?php if($this->uri->segment(1) == 'add-a-business'){echo 'active highlight';} else{ echo ""; } ?>">
        <a href="<?php echo base_url();?>business">
            <i class="menu-icon  fa fa-h-square"></i>
            Add a Business
        </a>
        <b class="arrow"></b>
    </li>
    
     <li class="<?php if($this->uri->segment(2) == 'list-all-templates'){echo 'active highlight';} else{ echo ""; } ?>">
        <a href="<?php echo base_url();?>emailtemplates/list-all-templates">
            <i class="menu-icon fa fa-edit"></i>
            Email Templates
        </a>
        <b class="arrow"></b>
    </li>
    
        <li class="<?php if($this->uri->segment(2) == 'help-videos'){echo 'active highlight';} else{ echo ""; } ?>">
            <a href="<?php echo base_url();?>pharmacies/help-videos">
                <i class="menu-icon fa fa-play"></i>
                Help Videos
            </a>
            <b class="arrow"></b>
        </li>
    

        <li class="<?php if($this->uri->segment(2) == 'list-all-settings'){echo 'active highlight';} else{ echo ""; } ?>">
            <a href="<?php echo base_url();?>settings/list-all-settings">
                <i class="menu-icon fa fa-cog"></i>
                Settings
            </a>
            <b class="arrow"></b>
        </li>
    
        <li class="<?php if($this->uri->segment(2) == 'list-all-page'){echo 'active highlight';} else{ echo ""; } ?>">
            <a href="<?php echo base_url();?>page/list-all-page">
                <i class="menu-icon fa fa-edit"></i>
               CMS Pages
            </a>
            <b class="arrow"></b>
        </li>

 </ul>