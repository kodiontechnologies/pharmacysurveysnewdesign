<!-- BASE URL for Javascript Files -->
<script>var SURL = '<?php echo SURL?>';</script>

<?php
	//Including all Required CSS 
	$common_css = array('bootstrap.css','font-awesome.css','ace-fonts.css','ace.css','datepicker.css','daterangepicker.css','custom.css');
	echo add_css($common_css);
	
	$common_js = array('jquery.js','ace-extra.js');
	echo add_js($common_js);
	
?>

<!--[if lte IE 9]>
  <link rel="stylesheet" href="<?php echo CSS; ?>ace-part2.css" class="ace-main-stylesheet" />
<![endif]-->

<!--[if lte IE 9]>
<link rel="stylesheet" href="<?php echo CSS; ?>ace-ie.css" />
<![endif]-->

<!-- inline styles related to this page -->

<!-- ace settings handler -->
<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

<!--[if lte IE 8]>
<script src="<?php echo JS; ?>html5shiv.js"></script>
<script src="<?php echo JS; ?>respond.js"></script>
<![endif]-->
