		
		<!-- the following scripts are used in demo only for onpage help and you don't need them -->
		<link rel="stylesheet" href="<?php echo CSS; ?>ace.onpage-help.css" />
		<!--<link rel="stylesheet" href="<?php echo CSS; ?>themes/sunburst.css" />-->

		<?php
			$common_js = array('bootstrap.js', 'date-time/bootstrap-datepicker.js', 'date-time/daterangepicker.js', 'date-time/custom_datepicker.js', 'ace/ace.js', 'ace/elements.scroller.js', 'ace/ace.submenu-hover.js', 'ace/ace.sidebar.js', 'ace/ace.sidebar-scroll-1.js', 'dataTables/jquery.dataTables.js', 'dataTables/jquery.dataTables.bootstrap.js', 'dataTables/custom_datatable.js', 'common.js');

			echo add_js($common_js);
			echo $js; 
		?>
