<!-- the following scripts are used in demo only for onpage help and you don't need them -->
<link rel="stylesheet" href="<?php echo CSS; ?>ace.onpage-help.css" />
<!--<link rel="stylesheet" href="<?php echo CSS; ?>themes/sunburst.css" />-->

<?php
    $common_js = array('date-time/bootstrap-datepicker.js', 'date-time/daterangepicker.js', 'date-time/custom_datepicker.js', 'common.js');

    echo add_js($common_js);
    echo $js; 
?>
