<div id="navbar" class="navbar navbar-default">
    <script type="text/javascript">
        try{ace.settings.check('navbar' , 'fixed')}catch(e){}
    </script>

    <div class="navbar-container" id="navbar-container">
        <!-- #section:basics/sidebar.mobile.toggle -->
        <button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
    
            <span class="sr-only">Toggle sidebar</span>

            <span class="icon-bar"></span>

            <span class="icon-bar"></span>

            <span class="icon-bar"></span>

        </button>

        <!-- /section:basics/sidebar.mobile.toggle -->
        <div class="navbar-header pull-left">
            
            <!-- #section:basics/navbar.layout.brand -->
            <a href="#" class="navbar-brand">
                    <img src="<?php echo IMAGES; ?>logo-dark2.png" class="img-responsive" width="70%" />
            </a>

        </div>

        <!-- #section:basics/navbar.dropdown -->
        
        <div class="navbar-buttons navbar-header pull-right" role="navigation">
    
            <ul class="nav ace-nav">
                
                <!-- #section:basics/navbar.user_menu -->
                 <li class="light-blue">
                    <a data-toggle="dropdown" href="#" class="dropdown-toggle"> 
                         <?php 
						  
						  $get_user_details = get_user_details_profile($this->session->admin_id);
						  
						  ?>
                 			 <i class="ace-icon fa fa-user bigger-200" style="padding-right:25px;"></i>
           				
                        <span class="user-info"><small>Welcome,</small><?php echo $this->session->admin_last_name; ?></span>

                        <i class="ace-icon fa fa-caret-down"></i>
                    </a>

                    <ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
                        <li><a href="<?php echo SURL; ?>profile-setting/edit-profile"><i class="ace-icon fa fa-user"></i>Profile</a></li>
                         <li class="divider"></li>
                         <li><a href="<?php echo SURL; ?>profile-setting/change-password"><i class="ace-icon fa fa-lock"></i>Change Password</a></li>
                       	 <li class="divider"></li>
                         <li><a href="<?php echo SURL; ?>login/logout"><i class="ace-icon fa fa-power-off"></i>Logout</a></li>
                    </ul>
                </li>

            </ul>

        </div>

        <!-- /section:basics/navbar.dropdown -->

    </div><!-- /.navbar-container -->

</div>
