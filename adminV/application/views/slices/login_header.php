        <!-- bootstrap & fontawesome -->
        <link rel="stylesheet" href="<?php echo CSS; ?>bootstrap.css" />
        <link rel="stylesheet" href="<?php echo CSS; ?>font-awesome.css" />

        <!-- text fonts -->
        <link rel="stylesheet" href="<?php echo CSS; ?>ace-fonts.css" />

        <!-- ace styles -->
        <link rel="stylesheet" href="<?php echo CSS; ?>ace.css" />

        <!--[if lte IE 9]>
            <link rel="stylesheet" href="../assets/css/ace-part2.css" />
        <![endif]-->
        <link rel="stylesheet" href="<?php echo CSS; ?>ace-rtl.css" />

        <!--[if lte IE 9]>
          <link rel="stylesheet" href="../assets/css/ace-ie.css" />
        <![endif]-->

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

        <!--[if lt IE 9]>
        <script src="../assets/js/html5shiv.js"></script>
        <script src="../assets/js/respond.js"></script>
        <![endif]-->