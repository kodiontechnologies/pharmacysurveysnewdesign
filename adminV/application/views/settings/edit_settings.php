<div class="row" style="margin:0">
<div class="col-xs-12 nopadding">
<div class="pm-title">Update Settings</div>
</div>
</div>
<div class="space-10"></div>

<div class="row" style="margin:0">

<div class="col-xs-12"> 
 <form id="edit_new_page_frm" name="edit_new_page_frm" method="post" enctype="multipart/form-data" action="<?php echo SURL;?>settings/add-update-settings"> 
          
                <div class="row">
                  <label for="first_name" class="col-sm-2 control-label"> Setting Name </label>
                  
                  <div class="form-group col-sm-10">
                    <input type="text" class="form-control" placeholder="setting name" id="setting_name" name="setting_name" required="required" 
                    value="<?php echo $settings['setting_name'];?>"
                    data-fv-regexp="true" data-fv-regexp-regexp="^[a-zA-z0-9\s\-,\]+[a-zA-Z0-9\s\-_,.'/]{1,100}$"
                    data-fv-regexp-message="Please use allowed characters (Alphabet, Numbers, Hyphens, Underscore, Comma, Apostrophe, Dot, Forward Slash, Back Slash, Space)"
                    maxlength="100" readonly="readonly">
                  </div>
              </div>
                              	
              <div class="row form-group">
                <label for="page_description" class="col-sm-2 control-label"> Setting Value </label>
                <div class="col-sm-10">
                <textarea class="form-control" rows="3" placeholder="Description" name="setting_value" id="setting_value"><?php echo filter_string($settings['setting_value'])?></textarea>
                </div>
              </div>
              
              <div class="row form-group">
                <label for="page_description" class="col-sm-2 control-label"> Setting Description</label>
                <div class="col-sm-10">
                <textarea class="form-control" rows="3" required="required" placeholder="Short Description" name="setting_description" id="setting_description"><?php echo filter_string($settings['setting_description'])?></textarea>
                </div>
              </div>
               
              
             
               <div class="row" style="margin:2px 0px;">
              	<div class="pull-right">
                  <input type="hidden" name="settings_id" id="settings_id" value="<?php echo filter_string($settings['id'])?>" />
              	  <input type="hidden" name="action" value="update" />
                  <button class="btn btn-success btn-sm" type="submit" id="new_page_btn" name="new_page_btn"> <i class="ace-icon fa fa-floppy-o bigger-110"></i> Update </button>
                  <button class="btn btn-danger btn-sm" type="reset"> <i class="ace-icon fa fa-refresh bigger-110"></i> Clear </button>
               </div>  
             </div>
          </form>
</div>

</div>