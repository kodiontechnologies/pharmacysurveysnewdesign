<?php
	if($this->session->flashdata('err_message')){
?>

 <div class="alert alert-danger"><?php echo $this->session->flashdata('err_message'); ?></div>
<?php
	}//end if($this->session->flashdata('err_message'))
	
	if($this->session->flashdata('ok_message')){
?>
<div class="alert alert-success alert-dismissable"><?php echo $this->session->flashdata('ok_message'); ?></div>
<?php 
		}//if($this->session->flashdata('ok_message'))
?>
<script type="text/javascript" src="<?php echo JS?>tinymce/tinymce.min.js"></script>
<div class="row">
  <div class="col-xs-12"> 
    <!-- PAGE CONTENT BEGINS -->
    <div class="tabbable"> 
      <!-- #section:pages/faq -->
      <ul class="nav nav-tabs padding-18" id="myTab">
      
        	<li class="active"> <a data-toggle="tab" href="#tab-settings-list"> <i class="blue ace-icon fa fa-edit bigger-130"></i> Settings List</a> </li>
        	<li class=""> <a data-toggle="tab" href="#tab-settings-add"> <i class="green ace-icon fa fa-plus bigger-130"></i> Create New Settings </a> </li>
      </ul>
      
      <!-- /section:pages/faq -->
      <div class="tab-content no-border padding-24">
      
        <div id="tab-settings-list" class="tab-pane fade in active">
          <h4 class="blue"> Settings List</h4>          
          
          <div class="table-responsive">
          <div style="margin-top:20px;"></div>
          <table class="table <?php echo (count($list_settings) > 0) ? 'dynamic-table' : '' ?> table-striped table-bordered table-hover dataTable no-footer DTTT_selectable" role="grid" aria-describedby="dynamic-table_info">
				<thead>
					<tr class="headings"> 
						<th>Setting Name</th>
						<th>Setting Value</th>
						<th>Created Date</th>
						<th class=" no-link last"><span class="nobr">Action</span> </th>
					</tr>
				</thead>
				<tbody>
				<?php if(!empty($list_settings)){ ?>
					<?php foreach($list_settings as $each): 
					
					     	$setting_value = substr($each['setting_value'],0,80);
					 ?>
						<tr class="even pointer"> 
							<td><?php echo filter_string($each['setting_name']); ?></td>
							<td title="<?php echo filter_string($setting_value); ?>"><?php echo filter_string($setting_value); ?></td>
							<td><?php echo kod_date_format($each['created_date']); ?></td>
							<td>
							
                            <div class="hidden-sm hidden-xs action-buttons"> 
                            <a class="green fancybox_page_edit fancybox.ajax" href="<?php echo base_url()?>settings/edit-settings/<?php echo $each['id']; ?>"> <i class="ace-icon fa fa-pencil bigger-130" title="Edit"></i> </a>
							 <!--<a  href="#" data-href="<?php echo base_url(); ?>settings/delete-settings/<?php echo $each['id']; ?>" title="Delete" data-toggle="modal" data-target="#confirm-delete-<?php echo $each['id']; ?>" class="btn btn-xs btn-danger" >X</a>-->
                            </div> 
                            </td>
                            
                            <div class="modal fade" id="confirm-delete-<?php echo $each['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											Warning !
										</div>
										<div class="modal-body">
											Are you sure you want to delete this settings ?
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
											<a href="<?php echo base_url(); ?>settings/delete-settings/<?php echo $each['id']; ?>" class="btn btn-danger btn-ok">Delete</a>
										</div>
									</div>
								</div>
							</div>
						</tr>
					<?php endforeach; ?>
				<?php } else { ?>
								  <tr>
                               <td colspan="4" class="text-danger">No record found</td>
                     </tr>
				<?php } ?>
			  </tbody>
			</table>
           
           
          </div>
        </div>
     
        <div id="tab-settings-add" class="tab-pane fade in">
          <h4 class="blue"> Create New Setting </h4>          
          <div class="space-20"></div>
          <form id="add_new_page_frm" name="add_new_page_frm" method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>settings/add-update-settings"> 
          
                <div class="row">
                  <label for="first_name" class="col-sm-2 control-label"> Setting Name</label>
                  
                  <div class="form-group col-sm-10">
                    <input type="text" class="form-control" placeholder="setting name" id="setting_name" name="setting_name" required="required" 
                    value="<?php echo $session_data['setting_name'];?>"
                    data-fv-regexp="true" data-fv-regexp-regexp="^[a-zA-z0-9\s\-,\]+[a-zA-Z0-9\s\-_,.'/]{1,100}$"
                    data-fv-regexp-message="Please use allowed characters (Alphabet, Numbers, Hyphens, Underscore, Comma, Apostrophe, Dot, Forward Slash, Back Slash, Space)"
                    maxlength="100">
                    <div class="error help-block" id="error_msg"></div>
                  </div>
              </div>
                
                <div class="row">
                <label for="email_body" class="col-sm-2 control-label"> Setting Value </label>
                <div class="form-group col-sm-10">
              <textarea class="form-control" rows="3" placeholder="Description" name="setting_value" id="setting_value"><?php echo filter_string($settings['setting_value'])?></textarea>
                </div>
              </div>
              
              <div class="row">
                <label for="email_body" class="col-sm-2 control-label"> Setting Description </label>
                <div class="form-group col-sm-10">
              <textarea class="form-control" rows="3" required="required" placeholder="Short Description" name="setting_description" id="setting_description"><?php echo filter_string($settings['setting_description'])?></textarea>
                </div>
              </div>
             
             <div class="pull-right">
                  <button class="btn btn-success btn-sm" type="submit" id="add_edit_settinngs_btn" name="add_edit_settinngs_btn"> <i class="ace-icon fa fa-floppy-o bigger-110"></i> Save </button>
                  	<input type="hidden" name="action" value="add" />
                  <button class="btn btn-danger btn-sm" type="reset"> <i class="ace-icon fa fa-refresh bigger-110"></i> Clear </button>
             </div>  
          </form>
         </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
	// Check Setting Name exists			
$("#setting_name").blur(function(){
						
	var name = $('#setting_name').val();
	var path = SURL + 'settings/setting-name-exists';
	$.ajax({
		url: path,
		type: "POST",
		data: {'key': name},
		success: function(data){
			var obj = JSON.parse(data);
		 //alert(obj.exist);
			if(obj.exist == 1){
				
				$("#error_msg").html("Name already exist!");
				$("#error_msg").css({"color":"#a94442"});
				$("#setting_name").val("");
				$( "#setting_name" ).focus();
				$( ".validate_msg" ).addClass( "has-error" ).removeClass( "has-success");
				
				$( "#error_msg" ).fadeOut(3000);
				$('#add_new_page_frm').bootstrapValidator('revalidateField', 'setting_name');
			
				
			} else {
				$("#error_msg").html("");
		}
			console.log(obj.exist);
			
		}
	});

});
	// End Exists
</script>