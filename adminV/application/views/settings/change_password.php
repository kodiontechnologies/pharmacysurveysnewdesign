<?php
    // Set if submition fiald showing data in fields which is user filled 
	$session_data =  $this->session->flashdata();
	
	if($this->session->flashdata('err_message')){
?>

 <div class="alert alert-danger"><?php echo $this->session->flashdata('err_message'); ?></div>
<?php
	}//end if($this->session->flashdata('err_message'))
	
	if($this->session->flashdata('ok_message')){
?>
<div class="alert alert-success alert-dismissable"><?php echo $this->session->flashdata('ok_message'); ?></div>
<?php 
		}//if($this->session->flashdata('ok_message'))
?>
<div class="row">
  <div class="col-xs-12"> 
    <!-- PAGE CONTENT BEGINS -->
    <div class="tabbable"> 
      <!-- #section:pages/faq -->
      <ul class="nav nav-tabs padding-18" id="myTab">
      
            <li class="active"> <a data-toggle="tab" href="#tab-editprofile"> <i class="green ace-icon fa fa-lock bigger-120"></i>Change Password</a> </li>
      </ul>
      
      <!-- /section:pages/faq -->
      <div class="tab-content no-border padding-24">
      
        <div id="tab-editprofile" class="tab-pane fade in active">
          <h4 class="blue">Change Password </h4>
          
          <form id="change_password_frm" name="change_password_frm" method="post"  enctype="multipart/form-data" action="<?php echo SURL?>profile-setting/change-password-process"> 
              
             <div class="row">
                  <label for="password" class="col-sm-2 control-label"> New Password* </label>
                  
                  <div class="form-group col-sm-10">
                    <input type="password" class="form-control" placeholder="password" id="new_password" name="new_password" required="required"
                    data-fv-regexp="true" data-fv-regexp-regexp="^^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[A-Za-z0-9]{8,20}$"
                    data-fv-regexp-message="Password must be between 8 to 20 characters with atleast one uppercase, one lowercase and one digit. Allowed characters (Alphabet, Numbers)"
                    maxlength="20">
                  </div>
              </div>     
             
             <div class="row">
                  <label for="password" class="col-sm-2 control-label"> Re-enter New Password* </label>
                  
                  <div class="form-group col-sm-10">
                    <input type="password" class="form-control" placeholder="confirm password" id="confirm_password" name="confirm_password"
                    data-fv-identical="true"
               		data-fv-identical-field="new_password"
                	data-fv-identical-message="The password and its confirm password does not match" 
                    maxlength="20" />
                  </div>
              </div>    
             <hr />
             
             <div class="pull-right">
              	  <button class="btn btn-success btn-sm" type="submit" name="change_pass_btn" id="change_pass_btn"> <i class="ace-icon fa fa-floppy-o bigger-110"></i> Submit </button>
            </div>  
          </form>
         </div>
       
      </div>
    </div>
  </div>
</div>