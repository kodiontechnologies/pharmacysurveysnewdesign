<?php
    // Set if submition fiald showing data in fields which is user filled 
	$session_data =  $this->session->flashdata();
	
	if($this->session->flashdata('err_message')){
?>

 <div class="alert alert-danger"><?php echo $this->session->flashdata('err_message'); ?></div>
<?php
	}//end if($this->session->flashdata('err_message'))
	
	if($this->session->flashdata('ok_message')){
?>
<div class="alert alert-success alert-dismissable"><?php echo $this->session->flashdata('ok_message'); ?></div>
<?php 
		}//if($this->session->flashdata('ok_message'))
?>
<div class="row">
  <div class="col-xs-12"> 
    <!-- PAGE CONTENT BEGINS -->
    <div class="tabbable"> 
      <!-- #section:pages/faq -->
      <ul class="nav nav-tabs padding-18" id="myTab">
      
            <li class="active"> <a data-toggle="tab" href="#tab-editprofile"> <i class="green ace-icon fa fa-user bigger-120"></i>Edit Profile</a> </li>
      </ul>
      
      <!-- /section:pages/faq -->
      <div class="tab-content no-border padding-24">
      
        <div id="tab-editprofile" class="tab-pane fade in active">
          <h4 class="blue">Edit Profile </h4>
          
          <form id="edit_profile_frm" name="edit_profile_frm" method="post"  enctype="multipart/form-data" action="<?php echo SURL?>profile-setting/edit-profile-process"> 
          
              <div class="row">
                  <label for="first_name" class="col-sm-2 control-label"> First Name* </label>
                  
                  <div class="form-group col-sm-10">
                    <input type="text" class="form-control" placeholder="first name" id="first_name" name="first_name" required="required" 
                    value="<?php echo filter_string($admin_edit_profile['admin_first_name']);?>"
                    data-fv-regexp="true" data-fv-regexp-regexp="^[a-zA-z0-9\s\-,\]+[a-zA-Z0-9\s\-_,.'/]{1,50}$"
                    data-fv-regexp-message="Please use allowed characters (Alphabet, Numbers, Hyphens, Underscore, Comma, Apostrophe, Dot, Forward Slash, Back Slash, Space)"
                    maxlength="50">
                  </div>
              </div>
                
              <div class="row">
                  <label for="last_name" class="col-sm-2 control-label"> Last Name* </label>
                  
                  <div class="form-group col-sm-10">
                    <input type="text" class="form-control" placeholder="last name" id="last_name" name="last_name" required="required"
                    value="<?php echo filter_string($admin_edit_profile['admin_last_name']);?>" 
                    data-fv-regexp="true" data-fv-regexp-regexp="^[a-zA-z0-9\s\-,\]+[a-zA-Z0-9\s\-_,.'/]{1,50}$"
                    data-fv-regexp-message="Please use allowed characters (Alphabet, Numbers, Hyphens, Underscore, Comma, Apostrophe, Dot, Forward Slash, Back Slash, Space)"
                    maxlength="50">
                  </div>
              </div> 
              
             <div class="row">
                  <label for="email_address" class="col-sm-2 control-label"> Email* </label>
                  
                  <div class="form-group col-sm-10">
                    <input type="email" class="form-control" placeholder="email" id="email_address" name="email_address" required="required"
                     value="<?php echo filter_string($admin_edit_profile['admin_email_address']);?>"
                    data-fv-regexp="true" data-fv-regexp-regexp="^[a-zA-z0-9\s\-]+[a-zA-Z0-9\s\-_.@]+$"
                    data-fv-regexp-message="Please use allowed characters (Alphabet, Numbers, Hyphens, Underscore, Dot and @)" autocomplete="off" maxlength="30">
                  </div>
              </div>
             <hr />
             
             <div class="pull-right">
                  <input type="hidden" id="admin_user" name="admin_user" value="<?php echo $this->session->userdata('admin_id');?>">
              	  <button class="btn btn-success btn-sm" type="submit" name="edit_profile_btn"> <i class="ace-icon fa fa-floppy-o bigger-110"></i> Update </button>
            </div>  
          </form>
         </div>
       
      </div>
    </div>
  </div>
</div>