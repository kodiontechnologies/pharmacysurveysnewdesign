    
    <!--Hero Unit with Slider-->
    <section class="hero">
      <div class="container">
        
        <!--Hero Slider-->
        <div class="row">
          <div class="hero-slider">
            <div class="slide first-slide">
              <div class="col-lg-5 col-md-5 col-sm-5 animated">
                <h2 style="margin-top:50px;" class="text-light text-left">Pro CD Register</h2>
                <p>As pharmacists ourselves, we understand how difficult it is to keep all your paperwork in order—so we decided to simplify it and make it electronic.</p>
                <p>Wish to extend your landing page? We offer you simple copy and paste solution with new Components &amp; Styles page.</p>
                
                <a class="btn btn-primary" href="<?php echo base_url(); ?>signup">Try ProCDR for free</a>

              </div>
              <div class="col-lg-7 col-md-7 col-sm-7 animated">
                <img class="pull-right" src="<?php echo FRONT_IMG; ?>hero-slider/imac.png" width="689" height="659" alt="Components and Styles"/>
              </div>
            </div>
            <div class="slide">
              <div class="col-lg-5 col-md-5 col-sm-5 animated fadeInUp">
                <h2 style="margin-top:50px;" class="text-light text-left">Coming Soon Page</h2>
                <p>Have a look at Ultima Coming soon page!</p>
                <p>While your landing page is under construction show your visitors beautiful coming soon page with the coundown and subscription form.</p>
                <p>And they always stay connected.</p>
                <a class="btn btn-primary" href="coming-soon.html">Go to Coming soon page</a>
              </div>
              <div class="col-lg-7 col-md-7 col-sm-7 animated fadeInRight">
                <img class="iphone" src="<?php echo FRONT_IMG; ?>hero-slider/iphone.png" width="649" height="400" alt="Coming Soon Page"/>
              </div>
            </div>
            <div class="slide">
              <div class="col-lg-4 col-md-4 col-sm-4 animated fadeInDown">
                <h2 style="margin-top:70px;" class="text-light text-left">Some Heading</h2>
                <p>You shall not pass this theme!</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.</p>
                <a class="btn btn-primary" href="#">Push it!</a>
              </div>
              <div class="col-lg-8 col-md-8 col-sm-8 animated fadeInRight">
                <div class="video">
                  <img src="<?php echo FRONT_IMG; ?>hero-slider/macbook.png" width="782" height="422" alt=""/>
                  <div class="vide-holder">
                    <iframe src="//player.vimeo.com/video/79036490?byline=0&amp;portrait=0&amp;color=ffeeac" width="720" height="405"></iframe>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div><!--Close Hero Slider-->
      </div>      
    </section><!--Close Hero-->
    
    <!--Features-->
    <section class="page-block" id="features">
      <div class="container">
        <div class="row page-header">
          <h2>Features</h2>
          <span>Subtext for header</span>
        </div>
        <div class="row features">
          <div class="col-lg-4 col-md-4 col-sm-4">
            <div class="feature-img"><img src="<?php echo FRONT_IMG; ?>features/feature1.png" width="124" height="120" alt="Editable colors"/></div>
            <h3>Editable colors</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commo.</p>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-4">
            <div class="feature-img"><img src="<?php echo FRONT_IMG; ?>features/feature2.png" width="131" height="131" alt="All you need for start"/></div>
            <h3>All you need for start</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commo.</p>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-4" id="featuesblock">
            <div class="feature-img"><img src="<?php echo FRONT_IMG; ?>features/feature3.png" width="72" height="72" alt="Clean code"/></div>
            <h3>Clean code</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commo.</p>
          </div>
        </div>
      </div>
      <!--Tabs-->
      <div class="container-fluid">
        <div class="row">
          <!-- Nav tabs -->
          <ul class="nav nav-tabs central">
            <li class="active"><a href="#clean-code" data-toggle="tab">All Registers</a></li>
            <li><a href="#font-icons" data-toggle="tab">Current Stock</a></li>
            <li><a href="#more" data-toggle="tab">Installment RX</a></li>
            <li><a href="#patientreturns" data-toggle="tab">Patient Returns</a></li>
            <li><a href="#destructions" data-toggle="tab">Destruction</a></li>
            <li><a href="#inspectornote" data-toggle="tab">Inspector Note</a></li>
            <li><a href="#manageusers" data-toggle="tab">Manage Users</a></li>
            <li><a href="#reports" data-toggle="tab">Reports</a></li>
          </ul>
        </div>
      </div>
      <!-- Tab panes -->
      <div class="container">
        <div class="row tab-content central">
          <div class="tab-pane fade in active col-lg-12" id="clean-code">
            <div class="row">
              <div class="col-lg-6 col-md-6 animated fadeInLeft">
                <img src="<?php echo FRONT_IMG; ?>dashborad.png" class="m-top-custom"  alt="Clean Code"/>
              </div>
              <div class="col-lg-6 col-md-6 animated fadeInRight">
                <div class="row">
                  <div class="col-lg-12">
                    <h2>All Registers</h2>
                    <p class="double-space-bottom">This section lists  all schedule 2 controlled drugs and their available brands, strengths and forms. If you want to add a special then please click on the “Add new drug”.</p>
                  </div>
                </div>
        
                <div class="row">
                  <div class="col-lg-2"><i class="feature-icon icon-checkmark-circle"></i></div>
                  <div class="col-lg-10">
                    <p class="double-space-bottom">The system contains a database of all schedule 2 controlled drugs which can be searched by brand name, class, strength and form.</p>
                  </div>
                </div>
        
         <div class="row">
                  <div class="col-lg-2"><i class="feature-icon icon-checkmark-circle"></i></div>
                  <div class="col-lg-10">
                    <p class="double-space-bottom">Each register icon has a number which represents the current balance, this particularly helpful as it can be viewed before ordering new stock.</p>
                  </div>
                </div>
        
                <div class="row">
                  <div class="col-lg-2"><i class="feature-icon icon-checkmark-circle"></i></div>
                  <div class="col-lg-10">
                    <p class="double-space-bottom">The option is provided to add a new drug. This is specifically to enable addition of specials or drugs which are not present in the database.</p>
                  </div>
                </div>
                
              </div>
            </div>
          </div>
          <div class="tab-pane fade col-lg-12" id="font-icons">
            <div class="row">
              <div class="col-lg-6 col-md-6 animated fadeInLeft">
                <img src="<?php echo FRONT_IMG; ?>currentstock.png" alt="Font Icons"/>
              </div>
              <div class="col-lg-6 col-md-6 animated fadeInRight">
                <div class="row">
                  <div class="col-lg-12">
                    <h2>Current stock</h2>
                    <p class="double-space-bottom">This section shows all the active registers. This means that there is at least one entry made in this register before. You can print a list of all the current stock and carry out a stock check by clicking on the “start check” button.</p>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-2"><i class="feature-icon icon-checkmark-circle"></i></div>
                  <div class="col-lg-10">
                    <p class="double-space-bottom">This section enables you to check what stock you have currently and also to print a list as a pdf file.</p>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-2"><i class="feature-icon icon-checkmark-circle"></i></div>
                  <div class="col-lg-10">
                    <p class="double-space-bottom">Entering stock check is made easy by the “Start Check” button. It takes you straight to the relevant register to make an entry.</p>
                  </div>
                </div>
        <div class="row">
                  <div class="col-lg-2"><i class="feature-icon icon-checkmark-circle"></i></div>
                  <div class="col-lg-10">
                    <p class="double-space-bottom">You can monitor when how often the stock check has being carried out by checking the last stock check date provided in the table.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="tab-pane fade col-lg-12" id="more">
            <div class="row">
              <div class="col-lg-6 col-md-6 animated fadeInLeft">
                <img class="img-center" src="<?php echo FRONT_IMG; ?>installmentrx.png" alt="More Features"/>
              </div>
              <div class="col-lg-6 col-md-6 animated fadeInRight">
                <div class="row">
                  <div class="col-lg-12">
                    <h2>Installment RX</h2>
                    <p class="double-space-bottom">Entering all your installment scripts at the end of the day can take hours. With this modules, you can enter an installment script like you do on your PMR systems and then within a matter of few clicks you can make an entry for the daily collections of any Schedule 2 controlled drug.</p>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-2"><i class="feature-icon icon-checkmark-circle"></i></div>
                  <div class="col-lg-10">
                    <p class="double-space-bottom">In the “Current prescriptions” tab you can see all the scripts for that specified date. You can change the date to see due scripts for that date.</p>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-2"><i class="feature-icon icon-checkmark-circle"></i></div>
                  <div class="col-lg-10">
                    <p class="double-space-bottom">You can record a dose as “Missed” or “Collected”. All the information will be prefilled, just review and save to make an entry.</p>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-2"><i class="feature-icon icon-checkmark-circle"></i></div>
                  <div class="col-lg-10">
                    <p class="double-space-bottom">When adding a new script, you can adjust the schedule according to the prescription. This is done in few simple and quick steps.</p>
                  </div>
                </div>
        
        <div class="row">
                  <div class="col-lg-2"><i class="feature-icon icon-checkmark-circle"></i></div>
                  <div class="col-lg-10">
                    <p class="double-space-bottom">Notes can be added by the pharmacists to each script regarding any important issue, which can be viewed by other users.</p>
                  </div>
                </div>
        
        <div class="row">
                  <div class="col-lg-2"><i class="feature-icon icon-checkmark-circle"></i></div>
                  <div class="col-lg-10">
                    <p class="double-space-bottom">Patient history can be searched with a list of previous and current scripts displayed, further details can be viewed by clicking on the “view” button.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
       <div class="tab-pane fade col-lg-12" id="patientreturns">
            <div class="row">
              <div class="col-lg-6 col-md-6 animated fadeInLeft">
                <img class="img-center" src="<?php echo FRONT_IMG; ?>pateintreturns.png" alt="More Features"/>
              </div>
              <div class="col-lg-6 col-md-6 animated fadeInRight">
                <div class="row">
                  <div class="col-lg-12">
                    <h2>Patient Returns</h2>
                    <p class="double-space-bottom">This is a register for patient returns of controlled drugs. Entries are made easy by having a drug database and patient names can be searched instead of typing all their details every time they make returns.</p>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-2"><i class="feature-icon icon-checkmark-circle"></i></div>
                  <div class="col-lg-10">
                    <p class="double-space-bottom">Upon receiving returns from patients or patient’s representatives, an entry should be made by  clicking on “Add Returns”.</p>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-2"><i class="feature-icon icon-checkmark-circle"></i></div>
                  <div class="col-lg-10">
                    <p class="double-space-bottom">You can check destruction status of all patient returns by going to the register or patient returns section of the destruction module.</p>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-2"><i class="feature-icon icon-checkmark-circle"></i></div>
                  <div class="col-lg-10">
                    <p class="double-space-bottom">All entries can be annotated, edited and voided. This means that if you make an error in the initial process, it can be corrected.</p>
                  </div>
                </div>
        <div class="row">
                  <div class="col-lg-2"><i class="feature-icon icon-checkmark-circle"></i></div>
                  <div class="col-lg-10">
                    <p class="double-space-bottom">Once a patient return is being destroyed, the relevant information is entered automatically in the patient returns register.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
       <div class="tab-pane fade col-lg-12" id="destructions">
            <div class="row">
              <div class="col-lg-6 col-md-6 animated fadeInLeft">
                <img class="img-center" src="<?php echo FRONT_IMG; ?>destruction.png" alt="More Features"/>
              </div>
              <div class="col-lg-6 col-md-6 animated fadeInRight">
                <div class="row">
                  <div class="col-lg-12">
                    <h2>Destruction</h2>
                    <p class="double-space-bottom">This module allows safe, quick and easy destruction of patient returns, expired and obsolete stock. Patient returns can be destroyed by various users but expired and obsolete stock can only be destroyed when an inspector is logged in to the system.</p>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-2"><i class="feature-icon icon-checkmark-circle"></i></div>
                  <div class="col-lg-10">
                    <p class="double-space-bottom">When an entry is made for a particular drug as expired or obsolete in the register, they automatically appear on the destruction list.</p>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-2"><i class="feature-icon icon-checkmark-circle"></i></div>
                  <div class="col-lg-10">
                    <p class="double-space-bottom">As the expired or obsolete stock is destroyed, an entry is made automatically in the relevant register for the quantity destroyed.</p>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-2"><i class="feature-icon icon-checkmark-circle"></i></div>
                  <div class="col-lg-10">
                    <p class="double-space-bottom">Upon the destruction of patient returns, an entry is made in the patient returns register and record removed from destruction list.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
      
      <div class="tab-pane fade col-lg-12" id="inspectornote">
            <div class="row">
              <div class="col-lg-6 col-md-6 animated fadeInLeft">
                <img class="img-center" src="<?php echo FRONT_IMG; ?>inspectornote.png" alt="More Features"/>
              </div>
              <div class="col-lg-6 col-md-6 animated fadeInRight">
                <div class="row">
                  <div class="col-lg-12">
                    <h2>Inspector Note</h2>
                    <p class="double-space-bottom">This module is for the purpose of pharmacy inspections. The inspector once logged in, can make notes about the finding of the inspection and can view view previous inspection notes.</p>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-2"><i class="feature-icon icon-checkmark-circle"></i></div>
                  <div class="col-lg-10">
                    <p class="double-space-bottom">Inspector can write a report in this dedicated section, which will be saved and available to view to others users of the pharmacy.</p>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-2"><i class="feature-icon icon-checkmark-circle"></i></div>
                  <div class="col-lg-10">
                    <p class="double-space-bottom">Inspector can edit or delete notes as required. Other members can only view the notes. All previous inspection notes will be listed.</p>
                  </div>
                </div>
                
              </div>
            </div>
          </div>
      
      <div class="tab-pane fade col-lg-12" id="manageusers">
            <div class="row">
              <div class="col-lg-6 col-md-6 animated fadeInLeft">
                <img class="img-center" src="<?php echo FRONT_IMG; ?>inspectornote.png" alt="More Features"/>
              </div>
              <div class="col-lg-6 col-md-6 animated fadeInRight">
                <div class="row">
                  <div class="col-lg-12">
                    <h2>Manage Users</h2>
                    <p class="double-space-bottom">You can manage all your users database in this section. This feature enables you to select patients, suppliers and prescribers from the drop down search thus saving you time. You can view, edit and delete users in this section.</p>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-2"><i class="feature-icon icon-checkmark-circle"></i></div>
                  <div class="col-lg-10">
                    <p class="double-space-bottom">Manage current patients, suppliers and prescribers as you can view, edit and delete patients as required.</p>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-2"><i class="feature-icon icon-checkmark-circle"></i></div>
                  <div class="col-lg-10">
                    <p class="double-space-bottom">Add new users to the database and the next time you search them on the system, they will appear on the drop down search.</p>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-2"><i class="feature-icon icon-checkmark-circle"></i></div>
                  <div class="col-lg-10">
                    <p class="double-space-bottom">You can add patient photos for ID purposes so they will not be required bring a photographic ID every time.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
      
      <div class="tab-pane fade col-lg-12" id="reports">
            <div class="row">
              <div class="col-lg-6 col-md-6 animated fadeInLeft">
                <img class="img-center" src="<?php echo FRONT_IMG; ?>reports.png" alt="More Features"/>
              </div>
              <div class="col-lg-6 col-md-6 animated fadeInRight">
                <div class="row">
                  <div class="col-lg-12">
                    <h2>Reports</h2>
                    <p class="double-space-bottom">This section is designed to provide you with reports for audits for all the different operations of the register. This is perfect for the purpose of inspections or when you need to investigate a possible error.</p>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-2"><i class="feature-icon icon-checkmark-circle"></i></div>
                  <div class="col-lg-10">
                    <p class="double-space-bottom">You can generate a report on stock supplied, stock received, stock check, adjust stock and stock destruction.</p>
                  </div>
                </div>
               
                <div class="row">
                  <div class="col-lg-2"><i class="feature-icon icon-checkmark-circle"></i></div>
                  <div class="col-lg-10">
                    <p class="double-space-bottom">Most reports allow you to search by multiple criteria so you can find specific information by running a report.</p>
                  </div>
                </div>
         <div class="row">
                  <div class="col-lg-2"><i class="feature-icon icon-checkmark-circle"></i></div>
                  <div class="col-lg-10">
                    <p class="double-space-bottom">Each report can be saved and printed in PDF format.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
      
        </div>
      </div>
    </section>
    
    <!--Pricing Plans--> 
    <section class="page-block" id="pricing">
      <div class="container">
        <div class="row page-header">
          <h2 id="pricingsection">Pricing plans</h2>
          <span>Subtext for header</span>
        </div>
        <div class="row">
          <div class="col-lg-4 col-md-4 col-sm-4">
            <div class="pricing-plan">
              <div class="header">
                <h3>Pricing Plan 01</h3>
                <span class="price">0.00 $</span>
              </div>
              <div class="body">
                <uL>
                  <li>Lots of Components</li>
                  <li>Cool Icons</li>
                  <li>Unlimited colors</li>
                </uL>
                <div class="buy-btn"><a class="btn btn-primary" href="#">Buy</a></div>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-4">
            <div class="pricing-plan highlited">
              <div class="header">
                <h3>Pricing Plan 02</h3>
                <span class="price">00.00 $</span>
              </div>
              <div class="body">
                <uL>
                  <li>Lots of Components</li>
                  <li>Cool Icons</li>
                  <li>Unlimited colors</li>
                </uL>
                <div class="buy-btn"><a class="btn btn-primary" href="#">Buy</a></div>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-4">
            <div class="pricing-plan">
              <div class="header">
                <h3>Pricing Plan 03</h3>
                <span class="price">00.00 $</span>
              </div>
              <div class="body">
                <uL>
                  <li>Lots of Components</li>
                  <li>Cool Icons</li>
                  <li>Unlimited colors</li>
                </uL>
                <div class="buy-btn"><a class="btn btn-primary" href="#">Buy</a></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
