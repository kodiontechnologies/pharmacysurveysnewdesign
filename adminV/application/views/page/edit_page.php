<div class="row" style="margin:0">
<div class="col-xs-12 nopadding">
<div class="pm-title">Update CMS Page</div>
</div>
</div>
<div class="space-10"></div>

<div class="row" style="margin:0">

<div class="col-xs-12"> 
 <form id="edit_new_page_frm" name="edit_new_page_frm" method="post" enctype="multipart/form-data" action="<?php echo SURL;?>page/add-new-page-process"> 
          
                <div class="row">
                  <label for="first_name" class="col-sm-2 control-label"> Page Title</label>
                  
                  <div class="form-group col-sm-10">
                    <input type="text" class="form-control" placeholder="page title" id="page_title" name="page_title" required="required" 
                    value="<?php echo filter_string($get_page_details['page_title']);?>"
                    data-fv-regexp="true" data-fv-regexp-regexp="^[a-zA-z0-9\s\-,\]+[a-zA-Z0-9\s\-_,.'/]{1,100}$"
                    data-fv-regexp-message="Please use allowed characters (Alphabet, Numbers, Hyphens, Underscore, Comma, Apostrophe, Dot, Forward Slash, Back Slash, Space)"
                    maxlength="100">
                  </div>
              </div>
                
                <div class="row">
                  <label for="first_name" class="col-sm-2 control-label"> Meta Title</label>
                  
                  <div class="form-group col-sm-10">
                    <input type="text" class="form-control" placeholder="Meta title" id="meta_title" name="meta_title"   value="<?php echo filter_string($get_page_details['meta_title']);?>">
                  </div>
              </div>
              	<div class="row">
                  <label for="first_name" class="col-sm-2 control-label"> Meta Keywords</label>
                  
                  <div class="form-group col-sm-10">
                    <textarea  id="meta_keywords" name="meta_keywords" cols="119" rows="3" placeholder="meta keywords"><?php echo filter_string($get_page_details['meta_keywords']);?></textarea>
                  </div>
              </div>
              	<div class="row">
                  <label for="first_name" class="col-sm-2 control-label"> Meta Description</label>
                  
                  <div class="form-group col-sm-10">
                     <textarea  id="meta_description" name="meta_description" cols="119"  rows="3" placeholder="meta description"><?php echo filter_string($get_page_details['meta_description']);?></textarea>
                  </div>
              </div>
                <div class="row form-group">
                <label for="page_description" class="col-sm-2 control-label"> Page Description </label>
                <div class="col-sm-10">
                <textarea  id="page_description1" name="page_description" rows="10" placeholder="page description"><?php echo filter_string($get_page_details['page_description']);?></textarea>
                </div>
              </div>
               
              <div class="row form-group">
                  <label for="inspection_notes" class="col-sm-2 control-label"> Status</label>
                  <div class="col-sm-10">
                     <select name="status" id="status"  required="required" class="form-control">
                      <option value="1" <?php echo ($get_page_details['status'] == '1') ? 'selected="selected"' : '' ?>>Active</option>
                      <option value="0" <?php echo ($get_page_details['status'] == '0') ? 'selected="selected"' : '' ?>>InActive</option>
                      </select>
                  </div>
                </div>
             
             <div class="pull-right">
              <input type="hidden" name="page_id" id="page_id" value="<?php echo filter_string($get_page_details['id'])?>" />
                  <button class="btn btn-success btn-sm" type="submit" id="new_page_btn" name="new_page_btn"> <i class="ace-icon fa fa-floppy-o bigger-110"></i> Update </button>
                  <button class="btn btn-danger btn-sm" type="reset"> <i class="ace-icon fa fa-refresh bigger-110"></i> Clear </button>
             </div>  
          </form>
</div>

</div>

<script type="text/javascript">
//Edit Organization Governance SOP 
			
tinymce.init({
	selector: "#page_description1",
	menubar: 'edit insert table tools',
	theme: "modern",
  cleanup : false,
  verify_html: false,
  allow_html_in_named_anchor: true,
	plugins: [
		"advlist autolink lists link image charmap print preview hr anchor pagebreak",
		"searchreplace wordcount visualblocks visualchars code fullscreen",
		"insertdatetime media nonbreaking save table contextmenu directionality",
		"emoticons template paste textcolor colorpicker textpattern imagetools"
	],
	toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
	relative_urls : false,
	remove_script_host : false,
	convert_urls : true,
	

});
	
</script>