<div class="col-xs-12"> 
  <!-- PAGE CONTENT BEGINS -->
    <h4 class="blue">Update Email Template</h4> 
    <hr />
	<form id="edit_email_template_frm" name="edit_email_template_frm" method="post" enctype="multipart/form-data" action="<?php echo SURL;?>emailtemplates/add-new-template-process"> 
          
              <div class="row">
                  <label for="first_name" class="col-sm-2 control-label"> Email Title</label>
                  
                  <div class="form-group col-sm-10">
                    <input type="text" class="form-control" placeholder="email title" id="email_title" name="email_title" required="required" 
                    value="<?php echo filter_string($get_template_details['email_title']);?>"
                    data-fv-regexp="true" data-fv-regexp-regexp="^[a-zA-z0-9\s\-,\]+[a-zA-Z0-9\s\-_,.'/]{1,100}$"
                    data-fv-regexp-message="Please use allowed characters (Alphabet, Numbers, Hyphens, Underscore, Comma, Apostrophe, Dot, Forward Slash, Back Slash, Space)"
                    maxlength="100">
                  </div>
              </div>
                
              <div class="row">
                  <label for="last_name" class="col-sm-2 control-label"> Email Subject </label>
                  
                  <div class="form-group col-sm-10">
                    <input type="text" class="form-control" placeholder="email subject" id="email_subject" name="email_subject" required="required"
                    value="<?php echo filter_string($get_template_details['email_subject']);?>" 
                    data-fv-regexp="true" data-fv-regexp-regexp="^[a-zA-z0-9\s\-,\]+[a-zA-Z0-9\s\-_,.'/]{1,100}$"
                    data-fv-regexp-message="Please use allowed characters (Alphabet, Numbers, Hyphens, Underscore, Comma, Apostrophe, Dot, Forward Slash, Back Slash, Space)"
                    maxlength="100">
                  </div>
              </div> 
              
                <div class="row form-group">
                <label for="email_body1" class="col-sm-2 control-label"> Email Body </label>
                <div class="col-sm-10">
                <textarea  id="email_body1" name="email_body" rows="20"><?php echo filter_string($get_template_details['email_body'])?></textarea>
                </div>
              </div>
               
              <div class="row form-group">
                  <label for="inspection_notes" class="col-sm-2 control-label"> Status</label>
                  <div class="col-sm-10">
                      <select name="status" id="status"  required="required" class="form-control">
                      <option value="1" <?php echo ($get_template_details['status'] == '1') ? 'selected="selected"' : '' ?>>Active</option>
                      <option value="0" <?php echo ($get_template_details['status'] == '0') ? 'selected="selected"' : '' ?>>InActive</option>
                      </select>
                  </div>
                </div>
             
             <div class="pull-right">
                  <button class="btn btn-success btn-sm" type="submit" id="new_template_btn" name="new_template_btn"> <i class="ace-icon fa fa-floppy-o bigger-110"></i> Update </button>
                  <input type="hidden" name="template_id" id="template_id" value="<?php echo filter_string($get_template_details['id'])?>" />
                  <button class="btn btn-danger btn-sm" type="reset"> <i class="ace-icon fa fa-refresh bigger-110"></i> Clear </button>
             </div>  
          </form>
          
</div>
<script type="text/javascript">
//Edit Organization Governance SOP 
			
tinymce.init({
	selector: "#email_body1",
	menubar: 'edit insert table tools',
	theme: "modern",
	plugins: [
		"advlist autolink lists link image charmap print preview hr anchor pagebreak",
		"searchreplace wordcount visualblocks visualchars code fullscreen",
		"insertdatetime media nonbreaking save table contextmenu directionality",
		"emoticons template paste textcolor colorpicker textpattern imagetools"
	],
	toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
	relative_urls : false,
	remove_script_host : false,
	convert_urls : true,

});
	
</script>