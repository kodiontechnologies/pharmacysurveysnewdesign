<?php
	// Set if submition fiald showing data in fields which is user filled 
	if($this->session->flashdata()) {
		$session_data =  $this->session->flashdata();
	}
	
	if($this->session->flashdata('err_message')){
?>

 <div class="alert alert-danger"><?php echo $this->session->flashdata('err_message'); ?></div>
<?php
	}//end if($this->session->flashdata('err_message'))
	
	if($this->session->flashdata('ok_message')){
?>
<div class="alert alert-success alert-dismissable"><?php echo $this->session->flashdata('ok_message'); ?></div>
<?php 
		}//if($this->session->flashdata('ok_message'))
?>
<script type="text/javascript" src="<?php echo JS?>tinymce/tinymce.min.js"></script>
<div class="row">
  <div class="col-xs-12"> 
    <!-- PAGE CONTENT BEGINS -->
    <div class="tabbable"> 
      <!-- #section:pages/faq -->
      <ul class="nav nav-tabs padding-18" id="myTab">
      
        	<li class="active"> <a data-toggle="tab" href="#tab-pages"> <i class="blue ace-icon fa fa-edit bigger-130"></i> CMS Pages</a> </li>
        	<li class=""> <a data-toggle="tab" href="#tab-page-add"> <i class="green ace-icon fa fa-plus bigger-130"></i> Create New Page </a> </li>
      </ul>
      
      <!-- /section:pages/faq -->
      <div class="tab-content no-border padding-24">
      
        <div id="tab-pages" class="tab-pane fade in active">
          <h4 class="blue"> List All CMS Page </h4>          
          
          <div class="table-responsive">
          <div style="margin-top:20px;"></div>
          <table class="table <?php echo (count($list_page) > 0) ? 'dynamic-table' : '' ?> table-striped table-bordered table-hover dataTable no-footer DTTT_selectable" role="grid" aria-describedby="dynamic-table_info">
				<thead>
					<tr class="headings"> 
						<th>Page Title</th>
						<th>Url</th>
						<th>Created Date</th>
						<th>Status </th>
						<th class=" no-link last"><span class="nobr">Action</span> </th>
					</tr>
				</thead>
				<tbody>
				<?php if(!empty($list_page)){ ?>
					<?php foreach($list_page as $each): 
					
					      $subject = filter_string($each['email_subject']);
					 ?>
						<tr class="even pointer"> 
							<td><?php echo filter_string($each['page_title']); ?></td>
							<td><?php echo filter_string($each['url_slug']); ?></td>
							<td><?php echo kod_date_format($each['created_date']); ?></td>
							<td><?php echo ($each['status'] == 1) ? 'Active' : 'InActive' ?></td>
							<td>
							
                            <div class="hidden-sm hidden-xs action-buttons"> 
                            <a class="green fancybox_page_edit fancybox.ajax" href="<?php echo base_url()?>page/add-new-page/<?php echo $each['id']; ?>"> <i class="ace-icon fa fa-pencil bigger-130" title="Edit"></i> </a>
							<a  href="#" data-href="<?php echo base_url(); ?>page/delete-page/<?php echo $each['id']; ?>" title="Delete" data-toggle="modal" data-target="#confirm-delete-<?php echo $each['id']; ?>"> <i class="ace-icon fa fa-trash-o bigger-130"></i></a>
                            </div> 
                            </td>
                            
                            <div class="modal fade" id="confirm-delete-<?php echo $each['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											Warning !
										</div>
										<div class="modal-body">
											Are you sure you want to delete this Page ?
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
											<a href="<?php echo base_url(); ?>page/delete-page/<?php echo $each['id']; ?>" class="btn btn-danger btn-ok">Delete</a>
										</div>
									</div>
								</div>
							</div>
						</tr>
					<?php endforeach; ?>
				<?php } else { ?>
								  <tr>
                               <td colspan="5" class="text-danger">No record found</td>
                     </tr>
				<?php } ?>
			  </tbody>
			</table>
           
           
          </div>
        </div>
     
        <div id="tab-page-add" class="tab-pane fade in">
          <h4 class="blue"> Create New Page </h4>          
          <div class="space-20"></div>
          <form id="add_new_page_frm" name="add_new_page_frm" method="post" enctype="multipart/form-data" action="<?php echo SURL;?>page/add-new-page-process"> 
          
                <div class="row">
                  <label for="first_name" class="col-sm-2 control-label"> Page Title</label>
                  
                  <div class="form-group col-sm-10">
                    <input type="text" class="form-control" placeholder="page title" id="page_title" name="page_title" required="required" 
                    value="<?php echo $session_data['page_title'];?>"
                    data-fv-regexp="true" data-fv-regexp-regexp="^[a-zA-z0-9\s\-,\]+[a-zA-Z0-9\s\-_,.'/]{1,100}$"
                    data-fv-regexp-message="Please use allowed characters (Alphabet, Numbers, Hyphens, Underscore, Comma, Apostrophe, Dot, Forward Slash, Back Slash, Space)"
                    maxlength="100">
                  </div>
              </div>
                
                <div class="row">
                  <label for="first_name" class="col-sm-2 control-label"> Meta Title</label>
                  
                  <div class="form-group col-sm-10">
                    <input type="text" class="form-control" placeholder="Meta title" id="meta_title" name="meta_title">
                  </div>
              </div>
              	<div class="row">
                  <label for="first_name" class="col-sm-2 control-label"> Meta Keywords</label>
                  
                  <div class="form-group col-sm-10">
                    <textarea  id="meta_keywords" name="meta_keywords" cols="140" rows="3" placeholder="meta keywords"><?php echo $session_data['meta_keywords'];?></textarea>
                  </div>
              </div>
              	<div class="row">
                  <label for="first_name" class="col-sm-2 control-label"> Meta Description</label>
                  
                  <div class="form-group col-sm-10">
                     <textarea  id="meta_description" name="meta_description" cols="140"  rows="3" placeholder="meta description"><?php echo $session_data['meta_description'];?></textarea>
                  </div>
              </div>
                <div class="row form-group">
                <label for="email_body" class="col-sm-2 control-label"> Page Description </label>
                <div class="col-sm-10">
                <textarea  id="page_description" name="page_description" rows="10" placeholder="page description"><?php echo $session_data['page_description'];?></textarea>
                </div>
              </div>
               
              <div class="row form-group">
                  <label for="inspection_notes" class="col-sm-2 control-label"> Status</label>
                  <div class="col-sm-10">
                      <select name="status" id="status"  required="required" class="form-control">
                      <option value="1">Active</option>
                      <option value="0">InActive</option>
                      </select>
                  </div>
                </div>
             
             <div class="pull-right">
                  <button class="btn btn-success btn-sm" type="submit" id="new_page_btn" name="new_page_btn"> <i class="ace-icon fa fa-floppy-o bigger-110"></i> Save </button>
                  <button class="btn btn-danger btn-sm" type="reset"> <i class="ace-icon fa fa-refresh bigger-110"></i> Clear </button>
             </div>  
          </form>
         </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
	tinymce.init({
		selector: "#page_description",
		menubar: 'edit insert table tools',
		theme: "modern",
		height : "300",
    cleanup : false,
    verify_html : false,
		plugins: [
			"advlist autolink lists link image charmap print preview hr anchor pagebreak",
			"searchreplace wordcount visualblocks visualchars code fullscreen",
			"insertdatetime media nonbreaking save table contextmenu directionality",
			"emoticons template paste textcolor colorpicker textpattern imagetools"
		],
		toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
		
	});
</script>