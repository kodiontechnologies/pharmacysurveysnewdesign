<?php
    // Set if submition fiald showing data in fields which is user filled 
  $session_data =  $this->session->flashdata();
  
  if($this->session->flashdata('err_message')){
?>

 <div class="alert alert-danger"><?php echo $this->session->flashdata('err_message'); ?></div>
<?php
  }//end if($this->session->flashdata('err_message'))
  
  if($this->session->flashdata('ok_message')){
?>
<div class="alert alert-success alert-dismissable"><?php echo $this->session->flashdata('ok_message'); ?></div>
<?php 
    }//if($this->session->flashdata('ok_message'))
?>
<div class="row">
  <div class="col-xs-12"> 
    <!-- PAGE CONTENT BEGINS -->
    <div class="tabbable"> 
      <!-- #section:pages/faq -->
      <ul class="nav nav-tabs padding-18" id="myTab">

          <li class="active">
            <a data-toggle="tab" href="#tab-survey-invoices"> <i class="blue ace-icon fa fa-h-square bigger-130"></i> Pharmacy Survey Invoices</a>
          </li>
 
      </ul>
      
      <!-- /section:pages/faq -->
      <div class="tab-content no-border padding-24">
        <div id="tab-survey-invoices" class="tab-pane fade in active">
          <h4 class="blue"> Pharmacy Survey Invoices </h4>
          <div class="table-responsive">
            <div style="margin-top:20px;"></div>
            <table class="table dynamic-tabletable-striped table-condensed table-bordered table-hover dataTable no-footer" role="grid" aria-describedby="dynamic-table_info" width="100%">
              <thead>
                <tr>
                  <th>Date</th>
                  <th>Survey Year</th>
                  <th>Payment Method</th>
                  <th>Amount(&pound;)</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                if(count($survey_payment_invoices) > 0){
                  for($i=0;$i<count($survey_payment_invoices);$i++){
                ?>
                    <tr>
                      <td><?php echo filter_uk_date($survey_payment_invoices[$i]['created_date'])?></td>
                      <td><?php echo filter_string($survey_payment_invoices[$i]['survey_session'])?></td>
                      <td><?php echo filter_string($survey_payment_invoices[$i]['payment_method'])?></td>
                      <td><?php echo filter_string($survey_payment_invoices[$i]['grand_total'])?></td>
                      <td><a href="<?php echo SURL?>surveys/download-invoice/<?php echo $pharmacy_id; ?>/<?php echo filter_string($survey_payment_invoices[$i]['id'])?>" target="_blank" class="btn btn-sm btn-info">View</a></td>
                    </tr>
                <?php       
                  } // End for($i=0;$i<count($survey_payment_invoices);$i++)

                } else {
                ?>
                  <tr>
                    <td class="text-danger" colspan="6">No Record Found</td>
                  </tr>
                <?php   
                }//end if(count($survey_payment_invoices) > 0)
                ?>
              </tbody>
            </table>

            <?php 

                if(!$pharmacy_current_survey){
                
					//No Survey is currently started. Showing the statistics of the previous servey.
					
					//Now check if there is anyy most rcent survey, its tenure of survey period is over or not. If Not then show message

					$get_most_recent_survey = get_most_recent_survey($pharmacy_id);
					
					if(filter_string($get_most_recent_survey['survey_start_date']) == NULL ){
						$show_new_survey = 1;

                }else{
					
					$recent_survey_end_date = $get_most_recent_survey['survey_finish_date'];
					
					if($recent_survey_end_date >= date('Y-m-d')){
						//Survey is complete but not yet expired
						$show_new_survey = 0;
				  }else{
                    	//No Survy available, No Expired, No Purchased. Go for buying new Servey
						$show_new_survey = 2;
					}
				}//end if(filter_string($get_most_recent_survey['survey_start_date']) == NULL )

              // echo $show_new_survey=2;
              ?>

              <?php if($show_new_survey == 2){ ?>

                <br />
                
                 

                <div class="alert alert-danger">
	                <h4 class="inline">Survey Status: The survey has expired. Survey required to be renewed.</h4>
                  <!--<h4 class="inline">Buy survey for this pharmacy</h4>
                  
                  <a href="javascript:;" class="btn btn-sm btn-success pull-right" title="Buy Survey" data-toggle="modal" data-target="#survey-buy-confirmation"> Buy Survey <strong><?php echo $survey_session; ?></strong> </a>

                  <div class="modal fade" id="survey-buy-confirmation" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                      <form action="<?php echo SURL; ?>surveys/buy-pharmacy-survey" method="post">
                      
                        <div class="modal-content">
                          <div class="modal-header">
                            Warning !
                          </div>
                          <div class="modal-body">
                            Are you sure you want to purchase <?php echo $survey_session; ?> survey.

                            
                            <input type="hidden" name="pharmacy_id" value="<?php echo $pharmacy_id; ?>" />
                            <input type="hidden" name="survey_session" value="<?php echo $survey_session; ?>" />

                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-success btn-ok">Buy Now</button>
                          </div>
                        </div>

                      </form>
                    </div>
                  </div>-->

                </div>
                
              <?php } else  if($show_new_survey == 1){ ?>

                <br />
                <div class="alert alert-warning">
                  <h4 class="text-warning">Survey Status: Survey is not started yet !</h4>
                </div>

              <?php } else if($show_new_survey == 0){ ?>

                <p><h4 class="text-danger">Survey Status: You cannot start a new survey before <?php echo date('F j, Y', strtotime($recent_survey_end_date)) ?></h4></p>

              <?php } // if($show_new_survey == 2) ?>

            <?php 
				} else { 

					$min_no_of_surveys = filter_string($pharmacy_current_survey['min_no_of_surveys']);
					$total_submitted_surveys = get_survey_submitted_count($pharmacy_id, $pharmacy_current_survey['id']);
					
					$remaining_surveys = $min_no_of_surveys - $total_submitted_surveys;
					$remaining_surveys = ($remaining_surveys < 0) ? 0 : $remaining_surveys;

					$total_submitted_online_surveys = get_survey_submitted_count($pharmacy_id, $pharmacy_current_survey['id'],'ONLINE');
					$total_submitted_paper_surveys = get_survey_submitted_count($pharmacy_id, $pharmacy_current_survey['id'],'PAPER');
			?>
              
              <br />
              <div class="alert alert-success"><h4>Survey Status: Your survery for the year <?php echo ltrim($pharmacy_current_survey['survey_title'],'Survey ')?> has been started</h4></div>

                <div class="col-md-8">
                	<div class="row">
                    	<div class="col-md-12"><h4> Survey Statistics </h4></div>
                    </div>
                	<div class="row alert alert-success">
                    	<div class="col-md-6"><strong>The minimum number of returned surveys required</strong></div>
                        <div class="col-md-4 text-right"><strong><?php echo filter_string($pharmacy_current_survey['min_no_of_surveys'])?></strong></div>
                    </div>
                	<div class="row alert alert-danger">
                    	<div class="col-md-6"><strong>Remaining surveys</strong></div>
                        <div class="col-md-4 text-right">
                        	<strong>
                            <?php 
                                $remaining_surveys = filter_string($pharmacy_current_survey['min_no_of_surveys']) - $total_submitted_surveys;
                                echo $remaining_surveys = ($remaining_surveys < 0) ? 0 : $remaining_surveys;
                            ?>
                            </strong>
                        </div>
                    </div>
                    
                    <div class="row alert alert-info">
                    	<div class="col-md-6"><strong>Total completed surveys online</strong></div>
                        <div class="col-md-4 text-right"><strong><?php echo $total_submitted_online_surveys?></strong></div>
                    </div>

                    <div class="row alert alert-primary">
                    	<div class="col-md-6"><strong>Total completed paper based surveys</strong> </div>
                        <div class="col-md-4 text-right"><strong><?php echo $total_submitted_paper_surveys?></strong></div>
                    </div>
                </div>
            <?php } // if(!$pharmacy_current_survey) ?>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>