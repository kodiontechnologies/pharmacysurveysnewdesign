<div class="row" style="margin:0">
<div class="col-xs-12 nopadding">
<div class="pm-title">Pharmacy: <?php echo ucfirst(filter_string($get_pharmacy_details['pharmacy_name']));?></div>
</div>
</div>
<div class="space-10"></div>

<div class="row" style="margin:0">           
     
<div class="row" style="margin:0">      
                                                   
            <!-- #section:pages/profile.info -->
            <div class="profile-user-info profile-user-info-striped">
                
                <div class="profile-info-row">
                    <div class="profile-info-name" style="width:115px; text-align:left"> Pharmacy Name </div>
    
                    <div class="profile-info-value">
                        <span class="editable" id="username"><?php echo ucfirst(filter_string($get_pharmacy_details['pharmacy_name']));?></span>
                    </div>
                </div>
                
                <div class="profile-info-row">
                    <div class="profile-info-name" style="width:115px; text-align:left"> Email Address </div>
    
                    <div class="profile-info-value">
                        <span class="editable" id="username"><?php echo filter_string($get_pharmacy_details['email_address']);?></span>
                    </div>
                </div>
    
                <div class="profile-info-row">
                    <div class="profile-info-name" style="width:115px; text-align:left"> Address</div>
    
                    <div class="profile-info-value">
                        <span class="editable" id="city"><?php echo filter_string($get_pharmacy_details['address']);?></span>
                    </div>
                </div>
                
                <div class="profile-info-row">
                    <div class="profile-info-name" style="width:115px; text-align:left"> Address 2</div>
    
                    <div class="profile-info-value">
                        <span class="editable" id="city"><?php if($get_pharmacy_details['address_2'] !='') { echo filter_string($get_pharmacy_details['address_2']); } else { echo 'N/A'; } ?></span>
                    </div>
                </div>
                
                <div class="profile-info-row">
                    <div class="profile-info-name" style="width:115px; text-align:left">Town</div>
    
                    <div class="profile-info-value">
                        <span class="editable" id="city"><?php echo filter_string($get_pharmacy_details['town']);  ?></span>
                    </div>
                </div>
                
                  <div class="profile-info-row">
                    <div class="profile-info-name" style="width:115px; text-align:left">Postcode</div>
    
                    <div class="profile-info-value">
                        <span class="editable" id="city"><?php echo filter_string($get_pharmacy_details['postcode']);  ?></span>
                    </div>
                </div>
    
                <div class="profile-info-row">
                    <div class="profile-info-name" style="width:115px; text-align:left"> Contact No </div>
    
                    <div class="profile-info-value">
                        <span class="editable" id="age"><?php echo filter_string($get_pharmacy_details['contact_no']);?></span>
                    </div>
                </div>
                
            </div>
           <br />
          
        </div>
        
</div>
