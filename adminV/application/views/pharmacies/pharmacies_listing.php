<?php 							
	$active = $this->input->get('t');

	//Survey is not yet started means we have to start the survey of the current one.
	   $current_date = date('m/d'); 
	   $survey_start_date = date('Y-m-d');
	   $data['survey_start_date'] = $survey_start_date;
	   
	   $SURVEY_END_MONTH = 'SURVEY_END_MONTH';
	   $survey_end_global_value = get_global_settings($SURVEY_END_MONTH); //Set from the Global Settings
	   $next_survey_end_date = filter_string($survey_end_global_value['setting_value']);
		// Set if submition fiald showing data in fields which is user filled 
		
		
	  if(strtotime($current_date) > strtotime($next_survey_end_date)){
		
		$next_survey_end = strtotime("$next_survey_end_date +1 year"); 
		$survey_session = date('Y').'-'.(date('Y')+1);
		
	   }else{
		
		$next_survey_end = strtotime("$next_survey_end_date +0 year");
		$survey_session = (date('Y', $next_survey_end)-1).' - '.date('Y', $next_survey_end);
		
	   }//end if
?>
<?php
	
	$session_data =  $this->session->flashdata();
	
	if($this->session->flashdata('err_message')){
?>

 <div class="alert alert-danger"><?php echo $this->session->flashdata('err_message'); ?></div>
<?php
	}//end if($this->session->flashdata('err_message'))
	
	if($this->session->flashdata('ok_message')){
?>
<div class="alert alert-success alert-dismissable"><?php echo $this->session->flashdata('ok_message'); ?></div>
<?php 
		}//if($this->session->flashdata('ok_message'))
?>
<div class="row">
  <div class="col-xs-12"> 
    <!-- PAGE CONTENT BEGINS -->
    <div class="tabbable"> 
      <!-- #section:pages/faq -->
      <ul class="nav nav-tabs padding-18" id="myTab">

          <li class="<?php if($active ==1 || $active==''){?>active<?php }?>">
            <a data-toggle="tab" href="#tab-pharmacies"> <i class="blue ace-icon fa fa-h-square bigger-130"></i> Pharmacies Listing</a>
          </li>
           <li class="<?php if($active ==2){?>active<?php }?>"> <a data-toggle="tab" href="#tab-pharmacy-add"> <i class="green ace-icon fa fa-plus bigger-120"></i> Create New Pharmacy</a> </li>
 
      </ul>
      
      <!-- /section:pages/faq -->
      <div class="tab-content no-border padding-24">
          <div id="tab-pharmacies" class="tab-pane fade in <?php if($active ==1 || $active==''){?>active<?php }?>">
            <h4 class="blue"> Pharmacies Listing </h4>
            <div class="table-responsive">
             <div style="margin-top:20px;"></div>
              <table class="table <?php echo (count($pharmacies_list) > 0) ? 'dynamic-table-pharmacies' : '' ?> table-striped table-bordered table-hover dataTable no-footer " role="grid" aria-describedby="dynamic-table_info">

  	          	<thead>
  	          	 <tr>
                    <th> Register Date</th>
                    <th> Pharmacy Name </th>
                    <th> Email Address </th>
                    <th> Address </th>
                    <th> Postcode </th>
                    <th> Contact No </th>
                    <th> Status </th>
                    <th> Chain/ Group ID </th>
                    <th> Action </th>
                  </tr>
  	          	</thead>
  	            <tbody>
                <?php if(!empty($pharmacies_list)) {
  					  	foreach($pharmacies_list as $each): 
  				  ?>
                    <tr>
                      <td>
					  	<?php echo kod_date_format(filter_string($each['created_date'])); ?>
                        <div class="btn-group"><a href='#login' onclick="user_login_function('<?php echo $each["email_address"] ?>','<?php echo $each["id"] ?>','<?php echo $each["password"] ?>')" type='button' title='Click here to login this user.' class='pull-right btn btn-default btn-xs'><i class='fa fa-external-link'></i></a></div>
                      </td>
                      <td>
					  	<?php echo ucfirst(filter_string($each['pharmacy_name']));?><br />
                        <?php echo (filter_string($each['pharmacy_type']) == 'O') ? '(Online)' : '(Community)'?>
                      </td>
                      <td><?php echo filter_string($each['email_address']);?></td>
                      <td><?php echo filter_string($each['address']);?></td>
                      <td><?php echo filter_string($each['postcode']);?></td>
                      <td><?php echo filter_string($each['contact_no']);?></td>
                      <td>
					  
					  <?php if($each['status'] == '1') { ?> 
                     	
                         <a href="#" data-href="<?php echo base_url(); ?>pharmacies/activate-deactivate-pharmacy/<?php echo $each['id']; ?>" title="click to deactivate the <?php echo ucfirst(filter_string($each['pharmacy_name']));?>" data-toggle="modal" data-target="#confirm-deactivate<?php echo $each['id']; ?>"> Active </a> 
                      
                       <?php } else { ?>
                       	 <a  href="#" data-href="<?php echo base_url(); ?>pharmacies/activate-deactivate-pharmacy/<?php echo $each['id']; ?>" title="click to activate the <?php echo ucfirst(filter_string($each['pharmacy_name']));?>" data-toggle="modal" data-target="#confirm-activate<?php echo $each['id']; ?>"> Deactive </a> 
                       
                        <?php }?>
                      </td>
                      <td><?php echo filter_string($each['group_id']);?></td>
                      <td width="10%">
                          <div class="modal fade" id="confirm-activate<?php echo $each['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                Warning !
                                            </div>
                                            <div class="modal-body">
                                                Are you sure you want to activate <?php echo ucfirst(filter_string($each['pharmacy_name']));?> ?
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                <a href="<?php echo base_url(); ?>pharmacies/activate-deactivate-pharmacy/<?php echo $each['id']; ?>/<?php echo $each['status']; ?>" class="btn btn-danger btn-ok">Confirm</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="modal fade" id="confirm-deactivate<?php echo $each['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                Warning !
                                            </div>
                                            <div class="modal-body">
                                                Are you sure you want to deactivate <?php echo ucfirst(filter_string($each['pharmacy_name']));?> ?
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                <a href="<?php echo base_url(); ?>pharmacies/activate-deactivate-pharmacy/<?php echo $each['id']; ?>/<?php echo $each['status']; ?>" class="btn btn-danger btn-ok">Confirm</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                
                          <div class="hidden-sm hidden-xs action-buttons"> 
                          			<a class="fancybox_view fancybox.ajax" href="<?php echo base_url(); ?>pharmacies/edit-pharmacy/<?php echo $each['id'];?>"> <i class="ace-icon fa fa-pencil bigger-130" title="Edit Pharamcy"></i> </a>

                                <a class="green fancybox_view fancybox.ajax" href="<?php echo base_url(); ?>pharmacies/get-pharmacy-details/<?php echo $each['id'];?>"> <i class="ace-icon fa fa-search-plus bigger-130" title="View Pharmacy Details"></i> </a>

                                <a class="text-warning" target="_blank" href="<?php echo base_url(); ?>surveys/pharmacy-survey-status/<?php echo $each['id'];?>"> <i class="ace-icon fa fa-eye bigger-130" title="View Pharmacy Survey Details"></i> </a>
                                
                            <?php   $pharmacy_current_survey = get_most_recent_survey($each['id']);
							
						             if($pharmacy_current_survey['survey_finish_date'] < date('Y-m-d') ){
																								   ?>
                                	 <a href="javascript:;" title="Buy Survey" data-toggle="modal" data-target="#survey-buy-confirmation-<?php echo $each['id'];?>">[Renew]</a>
                           <?php } ?>      
                                 
                    <div class="modal fade" id="survey-buy-confirmation-<?php echo $each['id'];?>" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                      <form action="<?php echo SURL; ?>surveys/buy-pharmacy-survey" method="post" name="Test" id="Test-<?php echo $each['id'];?>">
                      
                        <div class="modal-content">
                          <div class="modal-header">
                            Warning !
                          </div>
                          <div class="modal-body">
                            
                            
                            <div class="row">
                            	<div class="col-md-12">
                                Please enter the amount for <?php echo $survey_session; ?> survey. <br /><br />
								<label for="question" class="col-sm-4" style="padding:0"> Enter survey price (&pound;)</label>
                                <div class="form-group col-sm-6">
                                <input type="text" id="survey_price" name="survey_price" class="form-control" placeholder="Enter survey Price" value=""  required data-fv-regexp="true" data-fv-regexp-regexp="^(0*[1-9][0-9]*(\.[0-9]+)?|0+\.[0-9]*[1-9][0-9]*)$" data-fv-regexp-message="Please use allowed characters (Numbers)"/>
                                </div>

                                </div>
                            </div>

                            <!-- Input hidden fields -->
                            <input type="hidden" name="pharmacy_id" value="<?php echo $each['id'];?>" readonly="readonly" />
                            <input type="hidden" name="survey_session" value="<?php echo $survey_session; ?>" readonly="readonly" />

                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-success btn-ok" onclick="check_validation(<?php echo $each['id'];?>);">Buy Now</button>
                          </div>
                        </div>

                      </form>
                    </div>
                  </div>

                          </div>
                      </td>
                      
                    </tr>
                  <?php 
  						endforeach; // foreach
  					 }  else { ?>
                     
                      <tr>
                               <td colspan="8" class="text-danger">No record found</td>
                     </tr>
                     
  			   <?php } ?>		 
                   </tbody>
  	          </table>
            </div>
          </div>
          
          <div id="tab-pharmacy-add" class="tab-pane fade in <?php if($active ==2){?>active<?php }?>">
          <h4 class="blue"> Create New Pharmacy</h4>          
          <div class="space-20"></div>
          <form id="add_pharmacy_frm" name="add_pharmacy_frm" method="post" enctype="multipart/form-data" action="<?php echo SURL;?>pharmacies/add-new-pharmacy-process"> 
          
              <div class="row">
                  <label for="business_type" class="col-sm-2 control-label">Business Type</label>
                  <div class="form-group col-sm-10">
                      <select name="business_type" id="business_type"  required="required" class="form-control">
                      <option value="">Select Business Type</option>
                      <option value="B"<?php if($session_data['business_type']=='B'){?> selected="selected"<?php }?>>Buying Group</option>
                      <option value="C" <?php if($session_data['business_type']=='C'){?> selected="selected"<?php }?>>Chain</option>
                      </select>
                  </div>
                </div>
                
            <div class="row">
              <label for="pharmacy_type" class="col-sm-2 control-label">Pharmacy Type</label>
              <div class="form-group col-sm-10">
                  <select name="pharmacy_type" id="pharmacy_type"  required="required" class="form-control">
                  <option value="OF" <?php if($session_data['business_type']=='OF'){?> selected="selected"<?php }?>>Community Pharmacy</option>
                  <option value="O" <?php if($session_data['business_type']=='O'){?> selected="selected"<?php }?>>Online Pharmacy</option>
                  </select>
              </div>
            </div>
          
              <div class="row">
                  <label for="group_id" class="col-sm-2 control-label">Group id </label>
                  
                  <div class="form-group col-sm-10">
                   <input type="text" class="form-control" name="group_id" id="group_id" placeholder="Group id" value="<?php echo $session_data['group_id'];?>" required>
                  </div>
              </div>
          
              <div class="row">
                  <label for="pharmacy_name" class="col-sm-2 control-label">Business name</label>
                  <div class="form-group col-sm-10">
                    <input type="text" class="form-control" name="pharmacy_name" id="pharmacy_name" required="required" placeholder="Business name" value="<?php echo $session_data['pharmacy_name'];?>" data-fv-regexp="true" data-fv-regexp-regexp="^[a-zA-z0-9\s\-,\]+[a-zA-Z0-9\s\-_,.'/]{1,100}$" data-fv-regexp-message="Please use allowed characters (Alphabet, Numbers, Hyphens, Underscore, Comma, Apostrophe, Dot, Forward Slash, Back Slash, Space)" maxlength="100">
                  </div>
              </div>
                
              <div class="row">
                  <label for="contact_name" class="col-sm-2 control-label"> Contact name </label>
                  
                  <div class="form-group col-sm-10">
                   <input type="text"  class="form-control" name="owner_name" id="owner_name" placeholder="Contact name" value="<?php echo $session_data['owner_name'];?>"   aria-required="true"  required data-fv-regexp="true" data-fv-regexp-regexp="^[a-zA-z0-9\s\-,\]+[a-zA-Z0-9\s\-_,.'/]{1,50}$" data-fv-regexp-message="Please use allowed characters (Alphabet, Numbers, Hyphens, Underscore, Comma, Apostrophe, Dot, Forward Slash, Back Slash, Space)" maxlength="50">
                  </div>
              </div> 
              
               <div class="row">
                  <label for="contact_name" class="col-sm-2 control-label"> Contact number </label>
                  
                  <div class="form-group col-sm-10">
                  <input type="text" class="form-control" name="contact_no" id="contact_no" placeholder="Contact number" value="<?php echo $session_data['contact_no'];?>" aria-required="true"  required data-fv-regexp="true"  data-fv-regexp-regexp="^(?=.*[0-9])[0-9]{11,}$" data-fv-regexp-message="Please use allowed characters (Numbers) and length should be minimum 11 characters"  maxlength="15">
                  </div>
              </div> 
              
              
              <div class="row">
                  <label for="contact_name" class="col-sm-2 control-label"> Email address </label>
                  
                  <div class="form-group col-sm-10">
                    <input type="email"  class="form-control" name="email_address" id="email_address" value="<?php echo $session_data['email_address'];?>" placeholder="Email address" aria-required="true"  required data-fv-regexp="true" data-fv-regexp-regexp="^[a-zA-z0-9\s\-]+[a-zA-Z0-9\s\-_.@]+$" data-fv-regexp-message="Please use allowed characters (Alphabet, Numbers, Hyphens, Underscore, Dot and @)" autocomplete="off" maxlength="50">
                  </div>
              </div>
              
              <div class="row">
                  <label for="contact_name" class="col-sm-2 control-label"> Find your Postcode </label>
                  
                  <div class="form-group col-sm-10">
                    <div id="postcode_lookup" class=""></div>
                  </div>
              </div>
              
              <div class="row">
                  <label for="contact_name" class="col-sm-2 control-label"> Address 1 </label>
                  
                  <div class="form-group col-sm-10">
                    <input type="text" class="form-control" name="address" id="address" placeholder="Address 1"  value="<?php echo $session_data['address'];?>" required data-fv-regexp="true" data-fv-regexp-regexp="^[a-zA-z0-9\s\-,\]+[a-zA-Z0-9\s\-_,.'/]{1,50}$"  data-fv-regexp-message="Please use allowed characters (Alphabet, Numbers, Hyphens, Underscore, Comma, Apostrophe, Dot, Forward Slash, Back Slash, Space)" maxlength="50">
                  </div>
              </div>
              
              <div class="row">
                  <label for="address_2" class="col-sm-2 control-label"> Address 2 </label>
                  
                  <div class="form-group col-sm-10">
                    <input type="text" class="form-control" name="address_2" id="address_2" placeholder="Address 2" value="<?php echo $session_data['address_2'];?>" data-fv-regexp="true" data-fv-regexp-regexp="^[a-zA-z0-9\s\-,\]+[a-zA-Z0-9\s\-_,.'/]{1,50}$"  data-fv-regexp-message="Please use allowed characters (Alphabet, Numbers, Hyphens, Underscore, Comma, Apostrophe, Dot, Forward Slash, Back Slash, Space)" maxlength="50">
                  </div>
              </div>
              
              <div class="row">
                  <label for="address_2" class="col-sm-2 control-label">Town</label>
                  
                  <div class="form-group col-sm-10">
                   <input type="text" class="form-control" name="town" id="town" placeholder="Town"  value="<?php echo $session_data['town'];?>" required data-fv-regexp="true" data-fv-regexp-regexp="^[a-zA-z0-9\s\-,\]+[a-zA-Z0-9\s\-_,.'/]{1,50}$" data-fv-regexp-message="Please use allowed characters (Alphabet, Numbers, Hyphens, Underscore, Comma, Apostrophe, Dot, Forward Slash, Back Slash, Space)" maxlength="50">
                  </div>
              </div>
              
              <div class="row">
                  <label for="address_2" class="col-sm-2 control-label">County (optional)</label>
                  
                  <div class="form-group col-sm-10">
                    <input type="text" class="form-control" name="county" id="county" placeholder="County" value="<?php echo $session_data['county'];?>" data-fv-regexp="true" data-fv-regexp-regexp="^[a-zA-z0-9\s\-,\]+[a-zA-Z0-9\s\-_,.'/]{1,50}$"  data-fv-regexp-message="Please use allowed characters (Alphabet, Numbers, Hyphens, Underscore, Comma, Apostrophe, Dot, Forward Slash, Back Slash, Space)">
                  </div>
              </div>
              
              <div class="row">
                  <label for="address_2" class="col-sm-2 control-label">Postcode</label>
                  
                  <div class="form-group col-sm-10">
                   <input type="text" class="form-control" name="postcode" id="postcode" placeholder="Postcode" value="<?php echo $session_data['postcode'];?>"  required data-fv-regexp="true"  data-fv-regexp-regexp="^[a-z|A-Z|]+[a-z|A-Z|0-9|\s]{0,10}$" data-fv-regexp-message="Please use allowed characters (Alphabets, Numbers and spaces) and first character must be alphabet." maxlength="10">
                  </div>
              </div>
              
             <div class="row">
                <label for="question" class="col-sm-2 control-label"> Survey Price (&pound;)</label>
                <div class="form-group col-sm-10">
                <input type="text" id="survey_price" name="survey_price" class="form-control" placeholder="Price" value="<?php echo $session_data['survey_price'];?>"  required data-fv-regexp="true" data-fv-regexp-regexp="^(0*[1-9][0-9]*(\.[0-9]+)?|0+\.[0-9]*[1-9][0-9]*)$" data-fv-regexp-message="Please use allowed characters (Numbers)"/>
                </div>
              </div>
              
              <div class="row">
                  <label for="contact_name" class="col-sm-2 control-label"> Password </label>
                  <div class="form-group col-sm-10">
                    <input type="password"  class="form-control"  name="password" id="password" placeholder="Password"   required data-fv-regexp="true" data-fv-regexp-regexp="^^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[A-Za-z0-9]{8,20}$" data-fv-regexp-message="Password must be between 8 to 20 characters with atleast one uppercase, one lowercase and one digit. Allowed characters (Alphabet, Numbers)" maxlength="20">
                  </div>
              </div>
              
              <div class="row">
                  <label for="contact_name" class="col-sm-2 control-label">Retype Password</label>
                  
                  <div class="form-group col-sm-10">
                   <input type="password"  class="form-control" name="re_password" id="re_password" placeholder="Retype password"  aria-required="true"   data-fv-identical="true" data-fv-identical-field="password" data-fv-identical-message="The password and its confirm password does not match" maxlength="20" >
                  </div>
              </div>

             <div class="pull-right">
                  <button class="btn btn-success btn-sm" type="submit" id="new_pharmacy_btn" name="new_pharmacy_btn"> <i class="ace-icon fa fa-floppy-o bigger-110"></i> Save </button>
                  <button class="btn btn-danger btn-sm" type="reset"> <i class="ace-icon fa fa-refresh bigger-110"></i> Clear </button>
             </div>  
          </form>
         </div>
      </div>
    </div>
  </div>
</div>
<form method="post"  action="<?php echo FRONT_SURL;?>/login/login-process" id="auto_login_form" >
       <input type="hidden" id="is_admin" name="is_admin" value="" readonly="readonly" >
       <input type="hidden" id="email_address_autologin" name="email_address" value="" readonly="readonly" >
       <input type="hidden" id="password_autologin" name="password" value=""  readonly="readonly">
       <input type="hidden" id="login_btn" name="login_btn" value="">
    </form>
<script src="https://getaddress.io/js/jquery.getAddress-2.0.5.min.js"></script> 

<!-- Add after your form --> 

<?php  
	   // Get POSTCODE API KEY
	   $POSTCODE_KEY = 'POSTCODE_KEY'; 
	   $key = get_global_settings($POSTCODE_KEY);
	   $api_postcode_key = $key['setting_value']; 
?>


<script>
(function($){
	jQuery('#postcode_lookup').getAddress({
	// api_key: 'BmiwdQp9W0q1l-EucVP_9A6735',  
	api_key: '<?php echo $api_postcode_key;?>',
	
	<!--  Or use your own endpoint - api_endpoint:https://your-web-site.com/getAddress, -->
	output_fields:{
		line_1: '#address',
		line_2: '#address_2',
		line_3: '#line3',
		post_town: '#town',
		county: '#county',
		postcode: '#postcode'
	},
<!--  Optionally register callbacks at specific stages -->                                                                                                               
    onLookupSuccess: function(data){/* Your custom code */},
    onLookupError: function(){/* Your custom code */},
    onAddressSelected: function(elem,index){
				
    // Always true 4 fields
    jQuery('.fv-hidden-submit').removeClass('disabled');
    jQuery('.fv-hidden-submit').attr('disabled', false);

	  if(jQuery('.help-block')){

		var new_html = '';
      var data_fv_for = '';
			
      jQuery.each( jQuery('.help-block'), function(k, error_el){

        data_fv_for = jQuery(error_el).attr('data-fv-for');
			  
        if(data_fv_for == 'address' || data_fv_for == 'town' || data_fv_for == 'county' || data_fv_for == 'postcode'){
          jQuery('#'+data_fv_for).trigger('input');
        } // if(data_fv_for == 'address' || data_fv_for == 'town' || data_fv_for == 'county' || data_fv_for == 'postcode')

			}); // jQuery.each( jQuery('.help-block'), function(k, error_el)

		} // if($('.help-block'))	

	}

});
})(jQuery);


function check_validation(id){
	  //Edit business form
	$('#Test-'+id).formValidation(); // end #edit_business_frm
}

function user_login_function (email, id, password) {
	
	$('#is_admin').val(1);
	$('#email_address_autologin').val(email);
	$('#password_autologin').val(password);
	$('#login_btn').val(1);
	
	$("#auto_login_form").attr('target', '_blank').submit();
	
 }
</script>