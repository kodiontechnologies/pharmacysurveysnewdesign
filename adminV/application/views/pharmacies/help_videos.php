<?php 							
	$active = $this->input->get('t');
?>
<?php
	
	$session_data =  $this->session->flashdata();
	
	if($this->session->flashdata('err_message')){
?>

 <div class="alert alert-danger"><?php echo $this->session->flashdata('err_message'); ?></div>
<?php
	}//end if($this->session->flashdata('err_message'))
	
	if($this->session->flashdata('ok_message')){
?>
<div class="alert alert-success alert-dismissable"><?php echo $this->session->flashdata('ok_message'); ?></div>
<?php 
		}//if($this->session->flashdata('ok_message'))
?>
<div class="row">
  <div class="col-xs-12"> 
    <!-- PAGE CONTENT BEGINS -->
    <div class="tabbable"> 
      <!-- #section:pages/faq -->
      <ul class="nav nav-tabs padding-18" id="myTab">

          <li class="<?php if($active ==1 || $active==''){?>active<?php }?>">
            <a data-toggle="tab" href="#tab-pharmacies"> <i class="blue ace-icon fa fa-h-square bigger-130"></i> Help Videos</a>
          </li>
           <li class="<?php if($active ==2){?>active<?php }?>"> <a data-toggle="tab" href="#tab-pharmacy-add"> <i class="green ace-icon fa fa-plus bigger-120"></i> Add new Video</a> </li>
 
      </ul>
      
      <!-- /section:pages/faq -->
      <div class="tab-content no-border padding-24">
          <div id="tab-pharmacies" class="tab-pane fade in <?php if($active ==1 || $active==''){?>active<?php }?>">
            <h4 class="blue"> Add New Video</h4>
            <div class="table-responsive">
             <div style="margin-top:20px;"></div>
              <table class="table <?php echo (count($video_list) > 0) ? 'dynamic-table-help-videos' : '' ?> table-striped table-bordered table-hover dataTable no-footer " role="grid" aria-describedby="dynamic-table_info">

  	          	<thead>
  	          	 <tr>
                    <th> Video Title</th>
                    <th> Video Description</th>
                    <th> Video</th>
                    <th> Display Order</th>
                    <th> Status </th>
                    <th> Action </th>
                  </tr>
  	          	</thead>
  	            <tbody>
                <?php 
					if(!empty($video_list)) {
						
  					  	foreach($video_list as $each){
  				  ?>
                    <tr>
                      <td><?php echo filter_string($each['video_title']);?></td>
                      <td><?php echo filter_string($each['video_description']);?></td>
                      <td><?php echo filter_string($each['embed_code']);?></td>
                      <td><?php echo filter_string($each['display_order']);?></td>
                      <td>
					  
					  <?php if($each['status'] == '1') { ?> 
                     	
                         <a href="#" data-href="<?php echo base_url(); ?>pharmacies/activate-deactivate-pharmacy/<?php echo $each['id']; ?>" title="click to deactivate the <?php echo ucfirst(filter_string($each['pharmacy_name']));?>" data-toggle="modal" data-target="#confirm-deactivate<?php echo $each['id']; ?>"> Active </a> 
                      
                       <?php } else { ?>
                       	 <a  href="#" data-href="<?php echo base_url(); ?>pharmacies/activate-deactivate-pharmacy/<?php echo $each['id']; ?>" title="click to activate the <?php echo ucfirst(filter_string($each['pharmacy_name']));?>" data-toggle="modal" data-target="#confirm-activate<?php echo $each['id']; ?>"> Deactive </a> 
                       
                        <?php }?>
                      </td>
                      <td width="10%">
                          <div class="modal fade" id="confirm-activate<?php echo $each['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                Warning !
                                            </div>
                                            <div class="modal-body">
                                                Are you sure you want to activate <?php echo ucfirst(filter_string($each['pharmacy_name']));?> ?
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                <a href="<?php echo base_url(); ?>pharmacies/activate-deactivate-pharmacy/<?php echo $each['id']; ?>/<?php echo $each['status']; ?>" class="btn btn-danger btn-ok">Confirm</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="modal fade" id="confirm-deactivate<?php echo $each['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                Warning !
                                            </div>
                                            <div class="modal-body">
                                                Are you sure you want to deactivate <?php echo ucfirst(filter_string($each['pharmacy_name']));?> ?
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                <a href="<?php echo base_url(); ?>pharmacies/activate-deactivate-pharmacy/<?php echo $each['id']; ?>/<?php echo $each['status']; ?>" class="btn btn-danger btn-ok">Confirm</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                
                          <div class="hidden-sm hidden-xs action-buttons"> 
                          			<a class="fancybox_view fancybox.ajax" href="<?php echo base_url(); ?>pharmacies/edit-help-video/<?php echo $each['id'];?>"> <i class="ace-icon fa fa-pencil bigger-130" title="Edit Video"></i> </a>

									<a  href="#" data-href="<?php echo base_url(); ?>emailtemplates/delete-help-video/<?php echo $each['id']; ?>" title="Delete" data-toggle="modal" data-target="#confirm-delete-<?php echo $each['id']; ?>"> <i class="ace-icon fa fa-trash-o bigger-130"></i></a> 
                            </div>
                            </td>
                            
                            <div class="modal fade" id="confirm-delete-<?php echo $each['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  								<div class="modal-dialog">
  									<div class="modal-content">
  										<div class="modal-header">
  											Warning !
  										</div>
  										<div class="modal-body">
  											Are you sure you want to delete this video?
  										</div>
  										<div class="modal-footer">
  											<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
  											<a href="<?php echo base_url(); ?>pharmacies/delete-help-video/<?php echo $each['id']; ?>" class="btn btn-danger btn-ok">Delete</a>
  										</div>
  									</div>
  								</div>
  							</div>                                
                  </div>

                          </div>
                      </td>
                      
                    </tr>
                  <?php 
						} // foreach
  					 }  else { ?>
                     
                      <tr>
                               <td colspan="8" class="text-danger">No record found</td>
                     </tr>
                     
  			   <?php } ?>		 
                   </tbody>
  	          </table>
            </div>
          </div>
          
          <div id="tab-pharmacy-add" class="tab-pane fade in <?php if($active ==2){?>active<?php }?>">
          <div class="space-20"></div>
          <form id="add_pharmacy_frm" name="add_pharmacy_frm" method="post" enctype="multipart/form-data" action="<?php echo SURL;?>pharmacies/add-edit-help-video-process"> 
          
              <div class="row">
                  <label for="group_id" class="col-sm-2 control-label">Video Title</label>
                  <div class="form-group col-sm-10">
                   <input type="text" class="form-control" name="video_title" id="video_title" placeholder="Video Title" value="" required>
                  </div>
              </div>
          
              <div class="row">
                  <label for="pharmacy_name" class="col-sm-2 control-label">Video Description</label>
                  <div class="form-group col-sm-10">
                    <textarea class="form-control" name="video_description" id="video_description" placeholder="Video Description"></textarea>
                  </div>
              </div>

              <div class="row">
                  <label for="pharmacy_name" class="col-sm-2 control-label">Video Embed Code</label>
                  <div class="form-group col-sm-10">
                    <textarea class="form-control" name="embed_code" id="embed_code" placeholder="Video Embed Code"></textarea>
                  </div>
              </div>
              <div class="row">
                  <label for="group_id" class="col-sm-2 control-label">Video URL</label>
                  <div class="form-group col-sm-10">
                   <input type="text" class="form-control" name="video_url" id="video_url" placeholder="Video URL" value="">
                  </div>
              </div>

              <div class="row">
                  <label for="group_id" class="col-sm-2 control-label">Display Order</label>
                  <div class="form-group col-sm-10">
                   <input type="text" class="form-control" name="display_order" id="display_order" placeholder="Display Order" value="">
                  </div>
              </div>

              <div class="row">
                <label for="add_new_drug_brand" class="col-sm-2 control-label"> Status </label>
                <div class="form-group col-sm-10">
                    <select class="form-control" name="status" id="status">
                        <option value="1" >Active</option>
                        <option value="0">Inactive</option>
                    </select>
                </div>
              </div>

             <div class="pull-right">
                  <button class="btn btn-success btn-sm" type="submit" id="new_pharmacy_btn" name="new_pharmacy_btn"> <i class="ace-icon fa fa-floppy-o bigger-110"></i> Save </button>
                  <button class="btn btn-danger btn-sm" type="reset"> <i class="ace-icon fa fa-refresh bigger-110"></i> Clear </button>
             </div>  
          </form>
         </div>
      </div>
    </div>
  </div>
</div>