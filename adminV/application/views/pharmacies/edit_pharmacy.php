<div class="row" style="margin:0">
<div class="col-xs-12 nopadding">
<div class="pm-title">Pharmacy: <?php echo ucfirst(filter_string($pharmacy_details['pharmacy_name']));?></div>
</div>
</div>
<div class="space-10"></div>

<div class="row" style="margin:0">           
     
<form class="prevent_enter_key" id="edit_pharmacy_frm" name="edit_pharmacy_frm" method="post" enctype="multipart/form-data" action="<?php echo SURL?>pharmacies/edit-pharmacy-process">

<div class="row" style="margin:0">
    <label for="add_new_drug_class" class="col-sm-3 control-label"> Business Type </label>
    <div class="form-group col-sm-9">
    <select name="business_type" id="business_type"  required class="form-control">
        <option value="">Select Business Type</option>
        <option value="B"<?php if($pharmacy_details['business_type']=='B'){?> selected="selected"<?php }?>>Buying Group</option>
        <option value="C" <?php if($pharmacy_details['business_type']=='C'){?> selected="selected"<?php }?>>Chain</option>
    </select>
    </div>
  </div>
  
  
  <div class="row" style="margin:0">
    <label for="add_new_drug_class" class="col-sm-3 control-label"> Pharmacy Type</label>
    <div class="form-group col-sm-9">
    <select name="pharmacy_type" id="pharmacy_type"  required class="form-control">
        <option value="OF" <?php if($pharmacy_details['pharmacy_type'] == 'OF'){?> selected="selected"<?php }?>>Community Pharmacy</option>
        <option value="O" <?php if($pharmacy_details['pharmacy_type'] == 'O'){?> selected="selected"<?php }?>>Online Pharmacy</option>
    </select>
    </div>
  </div>

   <div class="row" style="margin:0">
    <label for="add_new_drug_class" class="col-sm-3 control-label"> Group id  </label>
    <div class="form-group col-sm-9">
     <input type="text" class="form-control" name="group_id" id="group_id" placeholder="Group id" value="<?php echo $pharmacy_details['group_id'];?>" required>
    </div>
  </div>


  <div class="row" style="margin:0">
    <label for="add_new_drug_class" class="col-sm-3 control-label"> Business Name </label>
    <div class="form-group col-sm-9">
      <input class="form-control" id="pharmacy_name" name="pharmacy_name" placeholder="pharmacy name" type="text" required value="<?php echo filter_string($pharmacy_details['pharmacy_name']);?>"  data-fv-regexp="true" data-fv-regexp-regexp="^[a-zA-z0-9\s\-,\]+[a-zA-Z0-9\s\-_,.'/]{1,100}$" data-fv-regexp-message="Please use allowed characters (Alphabet, Numbers, Hyphens, Underscore, Comma, Apostrophe, Dot, Forward Slash, Back Slash, Space)" maxlength="100">
    </div>
  </div>

  <div class="row" style="margin:0">
    <label for="add_new_drug_class" class="col-sm-3 control-label"> Business phone number </label>
    <div class="form-group col-sm-9">
      <input class="form-control" id="contact_no" name="contact_no" placeholder="business phone number" type="text" required value="<?php echo filter_string($pharmacy_details['contact_no']);?>" data-fv-regexp="true"  data-fv-regexp-regexp="^(?=.*[0-9])[0-9]{11,}$" data-fv-regexp-message="Please use allowed characters (Numbers) and length should be minimum 11 characters"  maxlength="15">
    </div>
  </div>
  <div class="row" style="margin:0">
    <label for="add_new_drug_class" class="col-sm-3 control-label"> Business address </label>
    <div class="form-group col-sm-9">
      <input class="form-control" id="address" name="address" placeholder="business address" type="text" required value="<?php echo filter_string($pharmacy_details['address']);?>" data-fv-regexp="true" data-fv-regexp-regexp="^[a-zA-z0-9\s\-,\]+[a-zA-Z0-9\s\-_,.'/]{1,50}$"  data-fv-regexp-message="Please use allowed characters (Alphabet, Numbers, Hyphens, Underscore, Comma, Apostrophe, Dot, Forward Slash, Back Slash, Space)" maxlength="50">
    </div>
  </div>
  <div class="row" style="margin:0">
    <label for="add_new_drug_class" class="col-sm-3 control-label"> Town  </label>
    <div class="form-group col-sm-9">
      <input class="form-control" id="town" name="town" placeholder="town" type="text" required value="<?php echo filter_string($pharmacy_details['town']);?>" data-fv-regexp="true" data-fv-regexp-regexp="^[a-zA-z0-9\s\-,\]+[a-zA-Z0-9\s\-_,.'/]{1,50}$" data-fv-regexp-message="Please use allowed characters (Alphabet, Numbers, Hyphens, Underscore, Comma, Apostrophe, Dot, Forward Slash, Back Slash, Space)" maxlength="50">
    </div>
  </div>
  
  <div class="row" style="margin:0">
    <label for="add_new_drug_brand" class="col-sm-3 control-label"> Postcode </label>
    <div class="form-group col-sm-9">
      <input class="form-control" id="postcode" name="postcode" placeholder="postcode" value="<?php echo filter_string($pharmacy_details['postcode']);?>" required data-fv-regexp="true"  data-fv-regexp-regexp="^[a-z|A-Z|]+[a-z|A-Z|0-9|\s]{0,10}$" data-fv-regexp-message="Please use allowed characters (Alphabets, Numbers and spaces) and first character must be alphabet." maxlength="10">
    </div>
  </div>

  <div class="row" style="margin:0">
    <label for="add_new_drug_brand" class="col-sm-3 control-label"> Status </label>
    <div class="form-group col-sm-9">
    	<select class="form-control" name="status" id="status">
        	<option <?php echo (filter_string($pharmacy_details['status'])) ? 'selected="selected"' : ''?> value="1" >Active</option>
            <option <?php echo (filter_string($pharmacy_details['status']) == 0) ? 'selected="selected"' : ''?> value="0">Inactive</option>
        </select>

    </div>
  </div>
  
  <div class="row" style="margin:0">
    <label for="sms_quota" class="col-sm-3 control-label"> Sms Quota </label>
    <div class="form-group col-sm-9">
      <input class="form-control" id="sms_quota" name="sms_quota" placeholder="sms quota" value="<?php echo filter_string($pharmacy_details['pharmacy_sms_quota']);?>">
    </div>
  </div>

  <br />
  <div class="row" style="margin:2px 0px;">
    <div class="col-md-12 text-right">
      <button class="btn btn-success btn-sm" name="edit_pharamcy_btn" id="edit_pharamcy_btn" value="1" type="submit"><i class="ace-icon fa fa-floppy-o bigger-110"></i> Update</button>
      <input type="hidden" name="pharmacy_id" id="pharmacy_id" value="<?php echo filter_string($pharmacy_details['id']);?>" readonly="readonly" />
    </div>
  </div>
</form>

        
</div>
