<style>
	.main_container{
		width:100%; 
		font-family:Arial, Helvetica, sans-serif; 
		font-size:12px;
	}
</style>
<div class="main_container">

    <div style="width:100%">
        <div style="width:100%; padding: 5px" >
            
            <div style="width:100%">
                <div style="width:50%; padding: 5px" >
                    <div style="background-color:#84C060; padding: 10px 5px; color:#fff; font-size:14px;">INVOICE</div>
                </div>
            </div>
            <div style="width:60%">
                <div style="width:50%; padding: 5px; float:left" >Invoice No.</div>
                 <div style="width:40%; padding: 5px;  float:left" ><?php echo filter_string($invoice_data['invoice_no']);?></div>
            </div>
            
            <div style="width:60%">
                <div style="width:50%; padding: 5px; float:left" >Date</div>
                 <div style="width:40%; padding: 5px;  float:left" ><?php echo filter_uk_date($invoice_data['created_date']);?></div>
            </div>
        	<br /><br /><br />
            <div style="width:100%;">
                <div style="width:25%; float:left">&nbsp;</div>
                
                <div style="width:50%; float:left">&nbsp;</div>
                
                <div style="width:25%; float:left">
                   <img src="http://surveyfocus.co.uk/assets/img/logo.png" alt="">
                </div>
           </div>
           <br /><br />
            <div style="width:100%;">
                <div style="width:25%; float:left">
                    <strong>Bill To</strong> <br />
                    <?php echo filter_string($invoice_data['pharmacy_name']);?> <br />
                    <?php echo filter_string($invoice_data['pharmacy_address']).', ';?> 
                    <?php echo (filter_string($invoice_data['pharmacy_address_2'])) ? filter_string($invoice_data['pharmacy_address_2']).', ' : '';?> 
                    <?php echo filter_string($invoice_data['pharmacy_postcode']);?><br /> 
                    <?php echo filter_string($invoice_data['pharmacy_county']);?>
                </div>
                
                <div style="width:50%; float:left">&nbsp;</div>
                
                <div style="width:25%; float:left">
                    <?php echo filter_string($invoice_data['from_name']);?> <br />
                    <?php echo filter_string($invoice_data['from_address']).', ';?>  
                    <?php echo (filter_string($invoice_data['from_address_2'])) ? filter_string($invoice_data['from_address_2']).' ' : '';?> <br />
                    <?php echo filter_string($invoice_data['from_town']).', ';?> <?php echo filter_string($invoice_data['from_postcode']);?> <br />
                    <?php echo filter_string($invoice_data['from_county']);?>
                    
                </div>
           </div>
           <br /><br /><br />
            <div style="width:100%;">
                <div style="width:100%;">
                    <table width="100%" cellpadding="11" cellspacing="2" style="font-family:Arial, Helvetica, sans-serif; font-size:12px">
                      <tr>
                        <td width="70%" bgcolor="#84C060" style="background-color:#84C060; color:#fff; font-size:14px">Item Description</td>
                        <td width="15%" bgcolor="#84C060" style="background-color:#84C060; color:#fff; font-size:14px">Qty</td>
                        <td width="15%" bgcolor="#84C060" style="background-color:#84C060; color:#fff; font-size:14px">Price</td>
                        
                      </tr>
                      <tr style="font-size:14px; background-color:#FAFAFA;">
                        <td>Payment for <?php echo filter_string($invoice_data['survey_session']);?></td>
                        <td>1</td>
                        <td>&pound;<?php echo filter_string($invoice_data['subtotal'])?></td>
                      </tr>
                     
                  </table>
                </div>
            </div>
           <br /><br /> <br /> <br /> 
            <div style="width:100%">
                <table class="table" width="100%" cellpadding="10" style="font-family:font-family:Arial, Helvetica, sans-serif; font-size:12px;">
                  <tr style=" padding:15px;">
                    <td width="70%"></td>
                    <td width="15%" style="background-color:#F2F2F2;">Subtotal</td>
                    <td width="15%" style="background-color:#F2F2F2;"><strong>&pound;<?php echo filter_string($invoice_data['subtotal'])?></strong></td>
                  </tr>
                  <tr style=" padding:15px;">
                    <td width="70%"></td>
                    <td width="15%" style="background-color:#FAFAFA;">VAT (<?php echo number_format($invoice_data['vat_percentage'],1)?>%)</td>
                    <td width="15%" style="background-color:#FAFAFA;"><strong>&pound;<?php echo filter_string($invoice_data['vat'])?></strong></td>
                  </tr>
                  <tr style=" padding:15px;">
                    <td width="70%"></td>
                    <td width="15%" style="background-color:#F2F2F2;">Total</td>
                    <td width="15%" style="background-color:#F2F2F2;"><strong>&pound;<?php echo filter_string($invoice_data['grand_total'])?></strong></td>
                  </tr>
                  </table>
            </div>
            <br /><br /> <br /> <br /> <br /><br /> <br /> <br /> 
            <div style="width:100%; background-color:#84C060;">
            	<table class="table" width="100%" cellpadding="10" style="font-family:font-family:Arial, Helvetica, sans-serif; font-size:12px;">
                  <tr style="padding:15px;">
                  	<td style="color:#FFF"><?php echo filter_string($invoice_data['from_website']);?></td>
                    <td style="color:#FFF"><?php echo filter_string($invoice_data['from_email']);?></td>
                    <td style="color:#FFF">  <?php echo filter_string($invoice_data['from_phone']);?></td>
                  </tr>
                 </table>
            </div>

        </div>
        
    </div>    
        
</div>

