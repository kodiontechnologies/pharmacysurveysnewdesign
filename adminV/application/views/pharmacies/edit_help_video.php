<div class="row" style="margin:0">
<div class="col-xs-12 nopadding">
<div class="pm-title">Edit Help Video</div>
</div>
</div>
<div class="space-10"></div>

<div class="row" style="margin:0">           
     
<form class="prevent_enter_key" id="edit_pharmacy_frm" name="edit_pharmacy_frm" method="post" enctype="multipart/form-data" action="<?php echo SURL?>pharmacies/add-edit-help-video-process">

      <div class="row" style="margin:0">
          <label for="group_id" class="col-sm-3 control-label">Video Title</label>
          <div class="form-group col-sm-9">
           <input type="text" class="form-control" name="video_title" id="video_title" placeholder="Video Title" value="<?php echo filter_string($help_videos['video_title'])?>" required>
          </div>
      </div>
  
      <div class="row" style="margin:0">
          <label for="pharmacy_name" class="col-sm-3 control-label">Video Description</label>
          <div class="form-group col-sm-9">
            <textarea class="form-control" name="video_description" id="video_description" placeholder="Video Description"><?php echo filter_string($help_videos['video_description'])?></textarea>
          </div>
      </div>

      <div class="row" style="margin:0">
          <label for="pharmacy_name" class="col-sm-3 control-label">Video Embed Code</label>
          <div class="form-group col-sm-9">
            <textarea class="form-control" name="embed_code" id="embed_code" placeholder="Video Embed Code"><?php echo filter_string($help_videos['embed_code'])?></textarea>
          </div>
      </div>
      <div class="row" style="margin:0">
          <label for="group_id" class="col-sm-3 control-label">Video URL</label>
          <div class="form-group col-sm-9">
           <input type="text" class="form-control" name="video_url" id="video_url" placeholder="Video URL" value="<?php echo filter_string($help_videos['video_url'])?>">
          </div>
      </div>

      <div class="row" style="margin:0">
          <label for="group_id" class="col-sm-3 control-label">Display Order</label>
          <div class="form-group col-sm-9">
           <input type="text" class="form-control" name="display_order" id="display_order" placeholder="Display Order" value="<?php echo filter_string($help_videos['display_order'])?>">
          </div>
      </div>


  <div class="row" style="margin:0">
    <label for="add_new_drug_brand" class="col-sm-3 control-label"> Status </label>
    <div class="form-group col-sm-9">
    	<select class="form-control" name="status" id="status">
        	<option <?php echo (filter_string($help_videos['status'])) ? 'selected="selected"' : ''?> value="1" >Active</option>
            <option <?php echo (filter_string($help_videos['status']) == 0) ? 'selected="selected"' : ''?> value="0">Inactive</option>
        </select>

    </div>
  </div>


  <br />
  <div class="row" style="margin:2px 0px;">
    <div class="col-md-12 text-right">
      <button class="btn btn-success btn-sm" name="edit_pharamcy_btn" id="edit_pharamcy_btn" value="1" type="submit"><i class="ace-icon fa fa-floppy-o bigger-110"></i> Update</button>
      <input type="hidden" name="video_id" id="" value="<?php echo filter_string($help_videos['id']);?>" readonly="readonly" />
    </div>
  </div>
</form>

        
</div>
