<style>
table td {
    border: none !important;
	
}
</style>
<div class="color-line"></div>
<div class="modal-header text-left">
  <h4 class="modal-title">Invoice Details</h4>
</div>
<div class="modal-body">
  
  <div class="row" style="margin:0">
	<div class="c12 text-right">
    	<a style="font-size:16" href="<?php echo SURL?>dashboard/download-invoice/<?php echo filter_string($invoice_data['id']);?>"><i class="fa fa-print"></i> Print Invoice</a>
    </div>
    <table width="100%" cellpadding="5" cellspacing="5">
      <tbody>
        <tr>
          <td align="left" valign="top"><h1>Invoice</h1>
            <strong>Ref:</strong> <?php echo filter_string($invoice_data['invoice_no']);?><br />
            <strong>Date:</strong> <?php echo filter_uk_date($invoice_data['created_date']);?></td>
          <td align="right"><img style="margin-right: 1px;" src="<?php echo IMAGES?>survey_focus.png" width="250px" /></td>
        </tr>
        <tr>
          <td align="left" valign="top"><strong>To :</strong> <?php echo filter_string($invoice_data['pharmacy_name']);?><br />
            <p><strong><?php echo filter_string($invoice_data['pharmacy_owner_name']);?></strong><br />
              <?php echo filter_string($invoice_data['pharmacy_address']);?><br />
              <?php echo (filter_string($invoice_data['pharmacy_address_2'])) ? filter_string($invoice_data['pharmacy_address_2']).'<br>' : '';?>
              <?php echo filter_string($invoice_data['pharmacy_county']);?> <br />
              <?php echo filter_string($invoice_data['pharmacy_postcode']);?></p></td>
          <td style="text-align: right;" align="center" valign="top"><strong><?php echo filter_string($invoice_data['from_name']);?></strong><br />
            <p><?php echo filter_string($invoice_data['from_address']);?> <br />
            	<?php echo (filter_string($invoice_data['from_address_2'])) ? filter_string($invoice_data['from_address_2']).'<br>' : '';?>
              <?php echo filter_string($invoice_data['from_town']);?> <br />
              <?php echo filter_string($invoice_data['from_county']);?></p>
            <p><strong>Phone:</strong> <?php echo filter_string($invoice_data['from_phone']);?> <br />
              <strong>Email:</strong> <?php echo filter_string($invoice_data['from_email']);?> <br />
              <strong>Website:</strong> <?php echo filter_string($invoice_data['from_website']);?></p></td>
        </tr>
        <tr>
          <td colspan="2"><table cellspacing="5" cellpadding="5" width="100%">
              <tbody>
                <tr class="bg-info">
                  <td><strong>Payment for the <?php echo filter_string($invoice_data['survey_session']);?></strong></td>
                  <td style="text-align: right;" align="right"><?php echo filter_string($invoice_data['subtotal'])?></td>
                </tr>
                <tr>
                  <td style="text-align: right;" align="right"><strong>Sub Total (&pound;)</strong></td>
                  <td style="text-align: right;" align="right"><?php echo filter_string($invoice_data['subtotal'])?></td>
                </tr>
                <tr>
                  <td style="text-align: right;" align="right"><strong>VAT (<?php echo filter_string($invoice_data['vat_percentage'])?>%)</strong></td>
                  <td style="text-align: right;" align="right"><?php echo filter_string($invoice_data['vat'])?></td>
                </tr>
                <tr>
                  <td style="text-align: right;" align="right"><strong>Grand Total (&pound;)</strong></td>
                  <td style="text-align: right;" align="right"><?php echo filter_string($invoice_data['grand_total'])?></td>
                </tr>
              </tbody>
            </table></td>
        </tr>
      </tbody>
    </table>
  	
  </div>
</div>