<!DOCTYPE html>
<html lang="en">
    <head>

        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta charset="utf-8" />
        <title><?php echo $title; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

	    <meta name="author" content="" />

        <!-- Start - Kod-Header Scripts -->
        <?php
			echo $meta;        
			echo $header_scripts;
			echo $css;
		?>
        <!-- End - Header Scripts -->

    </head>
    <body class="no-skin">

        <!-- Kod / header -->
        <?php echo $header_contents; ?>

        <!-- Start - Kod-Main Container -->
        <div class="main-container" id="main-container">
            
            <script type="text/javascript">
                try{ace.settings.check('main-container' , 'fixed')}catch(e){}
            </script>

            <!-- #section:basics/sidebar -->
            <div id="sidebar" class="sidebar responsive">
                <script type="text/javascript">
                    try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
                </script>

                <!-- Start - Kod-LeftPane -->
                <?php echo $left_pane; ?>
                <!-- End - Kod-LeftPane -->

                <!-- #section:basics/sidebar.layout.minimize -->
                <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
                    <i class="ace-icon fa fa-angle-double-left" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
                </div>

                <!-- /section:basics/sidebar.layout.minimize -->
                <script type="text/javascript">
                    try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}
                </script>
            </div>

            <!-- Start Kod - Main-Contents -->
            <div class="main-content">
                <div class="main-content-inner">
                    
                    <!-- Start - Kod-BreadCrumbs -->
                    <div class="breadcrumbs" id="breadcrumbs">
						<?php echo $breadcrum_data; ?>
                    </div>
                    <!-- End - Kod-BreadCrumbs + Searchbar on right -->

                    <!-- Start - Kod-Page -->
                    <div class="page-content">

                        <!-- Kod Page-Header -->
                        <div class="page-header">
                          <h1><?php echo filter_string($page_heading); ?></h1>
                        </div>
                        
                        <!-- Kod Page-Contents -->
                        <?php echo $content; ?>

                    </div>
                    <!-- End - Kod-Page -->

                </div>

            </div>
            <!-- End Kod - Main-Contents -->

            <!-- Start - Kod-Footer -->
            <?php echo $footer_contents; ?>
            <!-- End - Kod-Footer -->

            <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
            </a>

        </div><!-- /.main-container -->

        <?php echo $footer_scripts; ?>
       
    </body>
</html>