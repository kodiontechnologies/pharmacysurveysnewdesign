<!DOCTYPE html>
<html lang="en">
    <head>

        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta charset="utf-8" />
        <title><?php echo $title; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

	    <meta name="author" content="" />

        <!-- Start - Kod-Header Scripts -->
        <?php
			echo $meta;        
			echo $header_scripts;
			echo $css;
		?>
        <!-- End - Header Scripts -->

    </head>
    <body class="login-layout light-login">

        <!-- Kod / header -->
        <?php echo $login_header; ?>

        <!-- Start - Kod-Main Container -->
        <div class="main-container">
            <div class="main-content">

                <?php echo $content; ?>

            </div><!-- /.main-content -->
        </div><!-- /.main-container -->

        <?php echo $login_footer; ?>
		<?php echo $footer_scripts; ?>
        
       
    </body>
</html>