<!-- Start - Kod-Main Container -->
<div class="main-container" id="main-container"> 
  
  <!-- Start Kod - Main-Contents -->
  <div class="main-content">
    <div class="main-content-inner"> 
      
      <!-- End - Kod-BreadCrumbs + Searchbar on right --> 
      
      <!-- Start - Kod-Page -->
      <div class="page-content nopadding"> 
        
        <!-- Kod Page-Contents --> 
        <?php echo $content; ?> </div>
      <!-- End - Kod-Page --> 
      
    </div>
  </div>
  <!-- End Kod - Main-Contents --> 
  
</div>
<!-- /.main-container --> 
<?php echo $footer_scripts_modal; ?> 