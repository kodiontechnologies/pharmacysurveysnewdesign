<?php
class Users_mod extends CI_Model {
	
	function __construct(){
		 parent::__construct();
    }

	//Function: get_user_details(): Get User details
	public function get_user_details($pharmacy_id, $user_id){
		
		// Get Patient Record
		$this->db->dbprefix('users,usertype');
		$this->db->select('users.*, usertype.user_type AS usertype');
		$this->db->join('usertype','users.user_type = usertype.id','INNER');
		$this->db->where('users.id', $user_id);
		$this->db->where('users.pharmacy_id', $pharmacy_id);
		$get = $this->db->get('users');
		
		//echo $this->db->last_query(); exit;
		return $get->row_array();
		
	} // End public function get_user_details($patient_id)
	
	// Get users List
	public function get_user_list($pharmacy_id){
		
		$this->db->dbprefix('users,usertype');
		$this->db->select('users.*, usertype.user_type AS usertype');
		$this->db->join('usertype','users.user_type = usertype.id','INNER');
		$this->db->where('users.pharmacy_id', $pharmacy_id);
		//$this->db->where('users.is_owner', '0');
		$this->db->order_by('users.id', 'DESC');
	    
	    return $this->db->get('users')->result_array();

	} // end get_user_list

	// Start => add_user_type_process($post)
	public function add_user_type_process($post){

		extract($post);

		if($menu_chk){
			$menu_chk_str = implode(',',$menu_chk);
		} else {
			$menu_chk_str = $menu_chk;
		}
		
		$ins_arr = array(

			'user_type' => $user_type_name,
			'permissions' => $this->db->escape_str(trim($menu_chk_str)),
			'description' => $user_type_desc,
			'status' => '1'
		);

		$this->db->dbprefix('usertype');
		$this->db->insert('usertype', $ins_arr);

		$insert_id = $this->db->insert_id();

		$ins_arr = array(

			'user_type_id' => $insert_id,
			'responsibility_id' => NULL,
			'permissions' => $this->db->escape_str(trim($menu_chk_str)),
			'description' => $user_type_desc,
			'status' => '1'
		);

		$this->db->dbprefix('usertype_permissions');
		$this->db->insert('usertype_permissions', $ins_arr);

		return $insert_id;

	} // Start => add_user_type_process($post)

	// Start => function delete_user_type($usertype_id)
	public function delete_user_type($usertype_id){

		$upd_arr = array( 'status' => '0' );

		$this->db->dbprefix('usertype');
		$this->db->where('id', $usertype_id);
		return $this->db->update('usertype', $upd_arr);

	} //  End => function delete_user_type($usertype_id)

	// Start => get_pharmacy_user_responsibilities()
	public function get_pharmacy_user_responsibilities(){

		$this->db->dbprefix('user_role_responsibilities');
		return $this->db->get('user_role_responsibilities')->result_array();

	} // Start => get_pharmacy_user_responsibilities()
	
	public function unlock_user($pharmacy_id,$user_id){
		

		$upd_arr = array( 'lock_time' => NULL );

		$this->db->dbprefix('users');
		$this->db->where('id', $user_id);
		$this->db->where('pharmacy_id', $pharmacy_id);
		
		$upd_status = $this->db->update('users', $upd_arr);
		
		if($upd_status)
			return true;
		else
			return false;
		
	}//end unlock_user($pharmacy_id,$user_id)
	
}//end file
?>