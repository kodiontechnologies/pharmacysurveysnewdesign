<?php
class Pharmacies_mod extends CI_Model {
	
	function __construct(){
		parent::__construct();
    }


	//Function: get_group_pharmacy_list(): Get Pharmacy list 
	public function get_group_pharmacy_list($group_id){
		
		$this->db->dbprefix('pharmacies');
		$this->db->where('pharmacies.group_id', $group_id);
		$get = $this->db->get('pharmacies');
		//echo $this->db->last_query(); exit;
		return $get->result_array();
		
	} // end get_group_pharmacy_list($pharmacy_id)
	
	//Function: get_pharmacy_details(): Get Pharmacy details
	public function get_pharmacy_details($pharmacy_id){
		
		// Get Patient Record
		$this->db->dbprefix('pharmacies');
		$this->db->select('pharmacies.*');
		$this->db->where('pharmacies.id', $pharmacy_id);
		$get = $this->db->get('pharmacies');
		
		return $get->row_array();
		
	} // End public function get_patient_data($patient_id)
	
	// Get Pharmacies List
	public function get_pharmacies_list(){
		
		$this->db->dbprefix('pharmacies');
		$this->db->order_by('id', 'DESC');
	    return $this->db->get('pharmacies')->result_array();
		
	} // end get_pharmacies_list
	
	
	// Get Pharmacies List
	public function activate_deactivate_pharmacy($pharmacy_id, $status){
		
		$upd_status = ($status == '1') ? '0' : '1' ;
		$update_data = array('status' => $upd_status);
		$this->db->dbprefix('pharmacies');
		$this->db->where('id', $pharmacy_id);
		
		return $this->db->update('pharmacies', $update_data);

		/*
		$this->load->model('email_mod','email_template');
		
		$get_pharmacy_details = $this->get_pharmacy_details($pharmacy_id);
		$get_user_details = $this->users->get_user_details($pharmacy_id,$get_pharmacy_details['owner_id']);

		$user_first_last_name = ucwords(strtolower(stripslashes($get_user_details['first_name'].' '.$get_user_details['last_name'])));
		$user_name = $get_user_details['username'];
		
		$email_address = filter_string($get_user_details['email_address']);
		$pin = $get_pharmacy_details['pin'];
		
		$login_link = SURL."login";

		if($status =='1'){
			
			$update_data['status'] = '0';
			
			$search_arr = array('[FIRST_LAST_NAME]','[LOGIN_LINK]','[USERNAME]','[EMAIL_ADDRESS]','[PINCODE]','[SITE_LOGO]','[SITE_URL]');
			$replace_arr = array($user_first_last_name,$login_link,$user_name,$email_address,$pin,SITE_LOGO,SURL); 
			
			$email_body_arr = $this->email_template->get_email_template(6);
			$email_subject = $email_body_arr['email_subject'];
			$email_body = $email_body_arr['email_body'];
			
			$email_body =  str_replace($search_arr,$replace_arr,$email_body);
			$email_body = filter_string($email_body);
			
			
		} else {
			
			$update_data['status'] = '1';

			$search_arr = array('[FIRST_LAST_NAME]','[LOGIN_LINK]','[USERNAME]','[EMAIL_ADDRESS]','[PINCODE]','[SITE_LOGO]','[SITE_URL]');
			$replace_arr = array($user_first_last_name,$login_link,$user_name,$email_address,$pin,SITE_LOGO,SURL); 
			
			$email_body_arr = $this->email_template->get_email_template(5);
			$email_subject = $email_body_arr['email_subject'];
			$email_body = $email_body_arr['email_body'];
			
			$email_body = str_replace($search_arr,$replace_arr,$email_body);
			$email_body = filter_string($email_body);
			
		}//end if($status =='1')
		
		//Update Patient data into the database. 
		$this->db->dbprefix('pharmacies');
		$this->db->where('id', $pharmacy_id);
		$updated_into_db =  $this->db->update('pharmacies', $update_data);
		//echo $this->db->last_query();exit;
		
		if($updated_into_db){

			$noreply_email =  "noreply@procdr.co.uk";
			
			$email_from_txt = "Pro CDR";
					
			//Preparing Sending Email
			$config['charset'] = 'utf-8';
			$config['mailtype'] = 'html';
			$config['wordwrap'] = TRUE;			
			$config['protocol'] = 'mail';
				
			$this->load->library('email',$config);
			$this->email->from($noreply_email, $email_from_txt);
			$this->email->to(trim($email_address));
			$this->email->subject(filter_string($email_subject));
			$this->email->message(filter_string($email_body));
			
			//print_this($email_body); exit;
			
			$this->email->send();
			$this->email->clear();
			
			return true;
		}else
			return false;
		*/

	} // end activate_deactivate_pharmacy
	
	//Function update_pharamcy()
	public function update_pharamcy($data){
		
		extract($data);

		$created_date = date('Y-m-d G:i:s');
		$created_by_ip = $this->input->ip_address();
		
		$upd_data = array(
			'pharmacy_name' => $this->db->escape_str(trim($pharmacy_name)),
			'business_type' => $this->db->escape_str(trim($business_type)),
			'pharmacy_type' => $this->db->escape_str(trim($pharmacy_type)),
			'group_id' => $this->db->escape_str(trim($group_id)),
			'contact_no' => $this->db->escape_str(trim($contact_no)),
			'address' => $this->db->escape_str($address),
			'town' => $this->db->escape_str($town),
			'postcode' => $this->db->escape_str($postcode),
			'status' => $this->db->escape_str($status),
			'pharmacy_sms_quota' => $this->db->escape_str($sms_quota),
			'modified_date' => $this->db->escape_str(trim($created_date)),
			'modified_by_ip' => $this->db->escape_str(trim($created_by_ip)),
		);
		
		$this->db->dbprefix('pharmacies');
		$this->db->where('id', $pharmacy_id);
		$upd_into_db = $this->db->update('pharmacies',$upd_data);		
		
		if($upd_into_db)
			return true;
		else
			return false;
		
	}//end update_pharamcy($data)
	
	//Function: verify_if_pharmacy_already_exist(): verify if pharamcy exist with a given email address
	public function verify_if_pharmacy_already_exist($email_address,$pharmacy_id=''){
		
		$this->db->dbprefix('pharmacies');
		$this->db->where('id !=', $pharmacy_id);
		$this->db->where('email_address', $email_address);
		$get = $this->db->get('pharmacies');
		
		// echo $this->db->last_query();

		if($get->num_rows() > 0)
			return true;
		else
			return false;			
		
	} // end verify_if_pharmacy_already_exist($email_address)
	
	
	// Start Function add bussines
	public function register_pharmacy($data){

	   	extract($data);

		$created_date = date('Y-m-d G:i:s');
		$created_by_ip = $this->input->ip_address();
		
		$postcode = str_replace(' ', '', $postcode); // Saving the postcode without spaces for all consistent Postcode search	

		$SMS_QUOTA = 'SMS_QUOTA';
		$sms_quota = get_global_settings($SMS_QUOTA);
		$sms_quota_value = filter_string($sms_quota['setting_value']);
		
		$ins_data_pharmacy = array(
		
			'pharmacy_name' => $this->db->escape_str(trim($pharmacy_name)),
			'business_type' => $this->db->escape_str(trim($business_type)),
			'group_id' => $this->db->escape_str(trim($group_id)),
			'owner_name' => $this->db->escape_str(trim($owner_name)),
			'contact_no' => $this->db->escape_str(trim($contact_no)),
			'address' => $this->db->escape_str(trim($address)),
			'address_2' => $this->db->escape_str(trim($address_2)),
			'town' => $this->db->escape_str(trim($town)),
			'county' => $this->db->escape_str(trim($county)),
			'postcode' => $this->db->escape_str(trim($postcode)),
			'status' => $this->db->escape_str('1'),
			'pharmacy_sms_quota' => $this->db->escape_str($sms_quota_value),
			'pharmacy_sms_quota_consumed' => $this->db->escape_str('0'),
			'email_address' => $this->db->escape_str(trim($email_address)),
			'password' => $this->db->escape_str(trim(md5($password))),
			'pharmacy_type' => $this->db->escape_str(trim($pharmacy_type)),
			'created_date' => $this->db->escape_str(trim($created_date)),
			'created_by_ip' => $this->db->escape_str(trim($created_by_ip)),
		);
		
		//Inserting users data into the database. 
		$this->db->dbprefix('pharmacies');
		$ins_into_db = $this->db->insert('pharmacies', $ins_data_pharmacy);
		
		$ins_into_db = $this->db->insert_id();
		
		if($ins_into_db){
			return $ins_into_db;
		}else
			return false;
				
	}//end add_new_user
	
	public function get_help_videos_list($video_id = ''){
		
		// Get Patient Record
		$this->db->dbprefix('help_videos');

		if($video_id != '')
			$this->db->where('id', $video_id);
			
		$this->db->order_by('display_order', 'ASC');
		$get = $this->db->get('help_videos');
		
		if($video_id != '')
			return $get->row_array();
		else
			return $get->result_array();
			
	}//end get_help_videos_list($data)
		
	
	public function add_edit_help_videos_list($data){
		
	   	extract($data);

		$created_date = date('Y-m-d G:i:s');
		$created_by_ip = $this->input->ip_address();
		
		$ins_data = array(
		
			'video_title' => $this->db->escape_str(trim($video_title)),
			'video_description' => $this->db->escape_str(trim($video_description)),
			'embed_code' => $this->db->escape_str(trim($embed_code)),
			'video_url' => $this->db->escape_str(trim($video_url)),
			'display_order' => $this->db->escape_str(trim($display_order)),
			'status' => $this->db->escape_str(trim($status)),
		);
		
		
		if($video_id != ''){
			
			$ins_data['modified_date'] = $this->db->escape_str(trim($created_date));
			$ins_data['modified_by_ip'] = $this->db->escape_str(trim($created_by_ip));
			
			//Inserting users data into the database. 
			$this->db->dbprefix('help_videos');
			$this->db->where('id',$video_id);
			$ins_into_db = $this->db->update('help_videos', $ins_data);

		}else{

			$ins_data['created_date'] = $this->db->escape_str(trim($created_date));
			$ins_data['created_by_ip'] = $this->db->escape_str(trim($created_by_ip));

			//Inserting users data into the database. 
			$this->db->dbprefix('help_videos');
			$ins_into_db = $this->db->insert('help_videos', $ins_data);
			
		}
		
		if($ins_into_db){
			return true;
		}else
			return false;
		
	}//end get_help_videos_list($data)
	
	public function delete_help_video($video_id){

		$this->db->dbprefix('help_videos');
		$this->db->where('id', $video_id);
		$del = $this->db->delete('help_videos');
		
		if($del)
			return true;
		else
			return false;

	}//end delete_help_video($video_id)

} //End - file

?>