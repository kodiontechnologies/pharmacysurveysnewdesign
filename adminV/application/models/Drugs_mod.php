<?php
class Drugs_mod extends CI_Model {
	
	function __construct(){
		 parent::__construct();
    }

	//Function: get_drug_details(): Get Drug details
	public function get_drug_details($pharmacy_id, $drug_id){
		
		// Get Patient Record
		$this->db->dbprefix('pharmacy_drugs');
		$this->db->select('pharmacy_drugs.*,drugs_form.drug_form,drugs_form.drug_unit');
		$this->db->join('drugs_form','drugs_form.id = pharmacy_drugs.drug_form_id','LEFT');
		$this->db->where('pharmacy_drugs.id', $drug_id);
		$this->db->where('pharmacy_drugs.pharmacy_id', $pharmacy_id);
		$get = $this->db->get('pharmacy_drugs');
		//echo $this->db->last_query(); exit;
		return $get->row_array();
		
	} // End public function get_drug_details($pharmacy_id)
	
	// Get users List
	public function get_drugs_list($pharmacy_id){
		
		$this->db->dbprefix('pharmacy_drugs');
		$this->db->select('pharmacy_drugs.*,drugs_form.drug_form,drugs_form.drug_unit');
		$this->db->join('drugs_form','drugs_form.id = pharmacy_drugs.drug_form_id','LEFT');
		$this->db->where('pharmacy_drugs.pharmacy_id', $pharmacy_id);
		$this->db->order_by('pharmacy_drugs.id', 'DESC');
	    return $this->db->get('pharmacy_drugs')->result_array();
	} // end get_user_list
	
	/****************************************************************
              DEFAULT DRUGS                                         
	****************************************************************/
	
	
	    // Function drug_already_exist_in_pharmacy(): Will verify if the same drug is already listed into the database against the pharmacy
    public function drug_already_exist_in_pharmacy($data){

		extract($data);
		$this->db->dbprefix('default_drugs');
		$this->db->where('drug_brand',$drug_brand);
		$this->db->where('drug_class',$drug_class);
		$this->db->where('drug_strength',$drug_strength);
		$this->db->where('status','1');
		if($drug_id !='') $this->db->where('default_drugs.id !=',$drug_id);
		
		$get = $this->db->get('default_drugs');
	
		
		if($get->num_rows() > 0)
			return true;
		else
			return false;

	}//end drug_already_exist_in_pharmacy($pharmacy_id,$data)
	
	public function add_new_drug_in_pharmacy($data){
		
		extract($data);

		$created_date = date('Y-m-d G:i:s');
		$created_by_ip = $this->input->ip_address();
		
		if($drug_id ==""){

			$ins_data = array(
					
				'drug_brand' => $this->db->escape_str(trim($drug_brand)),
				'drug_class' => $this->db->escape_str(trim($drug_class)),
				'drug_strength' => $this->db->escape_str(trim($drug_strength)),
				'drug_form_id' => $this->db->escape_str(trim($drug_form)),
				'status' => $this->db->escape_str(trim('1')),
				'created_date' => $this->db->escape_str(trim($created_date)),
				'created_by_ip' => $this->db->escape_str(trim($created_by_ip)),
			);
		
			//Inserting drugs data into the database. 
			$this->db->dbprefix('default_drugs');
			$ins_into_db = $this->db->insert('default_drugs', $ins_data);
			
		} else {
	
			$ins_data = array(
					
				'drug_brand' => $this->db->escape_str(trim($drug_brand)),
				'drug_class' => $this->db->escape_str(trim($drug_class)),
				'drug_strength' => $this->db->escape_str(trim($drug_strength)),
				'drug_form_id' => $this->db->escape_str(trim($drug_form)),
				'modified_date' => $this->db->escape_str(trim($created_date)),
				'modified_by_ip' => $this->db->escape_str(trim($created_by_ip)),
			);
			
			//Inserting drugs data into the database. 
			$this->db->dbprefix('default_drugs');
			$this->db->where('default_drugs.id', $drug_id);
			$ins_into_db = $this->db->update('default_drugs', $ins_data);
			
		}
		
		if($ins_into_db)
			return true;
		else
			return false;
		
	}//end add_new_drug_in_pharmacy($pharmacy_id,$data)
	
	
	//Function: get_default_drug_details(): Get Default Drug details
	public function get_default_drug_details($drug_id){
		
		// Get default drug Record
		$this->db->dbprefix('default_drugs');
		$this->db->select('default_drugs.*,drugs_form.drug_form,drugs_form.drug_unit');
		$this->db->join('drugs_form','drugs_form.id = default_drugs.drug_form_id','LEFT');
		$this->db->where('default_drugs.id', $drug_id);
		$get = $this->db->get('default_drugs');
		//echo $this->db->last_query(); exit;
		return $get->row_array();
		
	} // End public function get_default_drug_details($drug_id)
	
	// Get Default drugs List
	public function get_default_drug_list(){
		
		$this->db->dbprefix('default_drugs');
		$this->db->select('default_drugs.*,drugs_form.drug_form,drugs_form.drug_unit');
		$this->db->join('drugs_form','drugs_form.id = default_drugs.drug_form_id','LEFT');
		$this->db->order_by('default_drugs.id', 'DESC');
	    return $this->db->get('default_drugs')->result_array();
		//echo $this->db->last_query(); exit;
		
	} // end get_default_drug_list()
	
	//Function delete_drug_deafult(): Delete Default Drug from  database
	public function delete_drug_deafult($drug_id){
		
		$this->db->dbprefix('default_drugs');
		$this->db->where('id',$drug_id);
		$get_drug = $this->db->delete('default_drugs');
		//echo $this->db->last_query(); 		exit;
		
		if($get_drug)
			return true;
		else
			return false;
		
	}//end delete_drug_deafult($drug_id)
	
	/***********************************************/
	/*           Drugs Form                        */
	/***********************************************/
	
	// public function add_new_drug_form($data)
	public function add_new_drug_form($data){
		
		extract($data);

		$created_date = date('Y-m-d G:i:s');
		$created_by_ip = $this->input->ip_address();
		
		if($drug_id ==""){

			$ins_data = array(
					
				'drug_form' => $this->db->escape_str(trim($drug_form)),
				'drug_unit' => $this->db->escape_str(trim($drug_unit)),
				'created_date' => $this->db->escape_str(trim($created_date)),
				'created_by_ip' => $this->db->escape_str(trim($created_by_ip)),
			);
		
			//Inserting drugs data into the database. 
			$this->db->dbprefix('drugs_form');
			$ins_into_db = $this->db->insert('drugs_form', $ins_data);
			
		} else {
	
			$ins_data = array(

				'drug_form' => $this->db->escape_str(trim($drug_form)),
				'drug_unit' => $this->db->escape_str(trim($drug_unit)),
				'modified_date' => $this->db->escape_str(trim($created_date)),
				'modified_by_ip' => $this->db->escape_str(trim($created_by_ip)),
			);
			
			//Inserting drugs data into the database. 
			$this->db->dbprefix('drugs_form');
			$this->db->where('drugs_form.id', $drug_id);
			$ins_into_db = $this->db->update('drugs_form', $ins_data);
			
		}
		
		if($ins_into_db)
			return true;
		else
			return false;
		
	}//end add_new_drug_form($data)
	
	// Get  drugs form List
	public function get_drug_form_list(){
		
		$this->db->dbprefix('drugs_form');
		$this->db->select('drugs_form.*');
		$this->db->order_by('drugs_form.id', 'DESC');
	    return $this->db->get('drugs_form')->result_array();
		//echo $this->db->last_query(); exit;
		
	} // end get_drug_form_list()
	
	//Function: get_drug_form_details($drug_id): Get Drug form details
	public function get_drug_form_details($drug_id){
		
		// Get default drug Record
		$this->db->dbprefix('drugs_form');
		$this->db->select('drugs_form.*');
		$this->db->where('drugs_form.id', $drug_id);
		$get = $this->db->get('drugs_form');
		//echo $this->db->last_query(); exit;
		return $get->row_array();
		
	} // End public function get_drug_form_details($drug_id)
	
	//Function delete_drug_form(): Update Drug form set is_deleted = 0 and status = 0 from  database
	public function delete_drug_form($drug_id){
		
		
		$upd_data = array(
			
			'status' => $this->db->escape_str('0'),
			'is_deleted' => $this->db->escape_str('1')
		);
		
		$this->db->dbprefix('drugs_form');
		$this->db->where('drugs_form.id', $drug_id);
		$ins_into_db = $this->db->update('drugs_form', $upd_data);
		//echo $this->db->last_query(); 		exit;
		
		if($ins_into_db)
			return true;
		else
			return false;
		
	}//end delete_drug_form($drug_id)
	
	// Function drug_already_exist_form(): Will verify if the same drug form is already listed into the database 
    public function drug_form_already_exist($data){

		extract($data);
		$this->db->dbprefix('drugs_form');
		$this->db->where('drug_form',$drug_form);
		$this->db->where('status','1');
		if($drug_id !='') $this->db->where('drugs_form.id !=',$drug_id);
		
		$get = $this->db->get('drugs_form');
		
		if($get->num_rows() > 0)
			return true;
		else
			return false;

	}//end drug_already_exist_in_pharmacy($pharmacy_id,$data)
		
}//end file
?>