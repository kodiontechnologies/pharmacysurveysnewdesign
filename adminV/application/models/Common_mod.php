<?php
class Common_mod extends CI_Model {
	
	function __construct(){
		 parent::__construct();
    }

	//Function random_number_generator($digit): random number generator function
	public function random_number_generator($digit){
		
		$randnumber = '';
		$totalChar = $digit;  //length of random number
		$salt = "0123456789abcdefjhijklmnopqrstuvwxyz";  // salt to select chars
		srand((double)microtime()*1000000); // start the random generator
		$password=""; // set the inital variable
		
		for ($i=0;$i<$totalChar;$i++)  // loop and create number
		$randnumber = $randnumber. substr ($salt, rand() % strlen($salt), 1);
		return $randnumber;
		
	}// end random_password_generator()
	
	//Function submit_contactus_form(): Submmiting/ emailing the contact us form to the admin
	public function submit_contactus_form($data){
		
		extract($data);
	
		// EMAIL SENDING CODE - START
		$full_name   = filter_string($full_name);
		$search_arr  = array('[USER_FULL_NAME]','[USER_EMAIL]','[COMMENTS]','[SITE_LOGO]','[SITE_URL]');
		$replace_arr = array($full_name, filter_string($email),filter_string($message),SITE_LOGO,SURL); 
		
		$this->load->helper(array('email'));
		$this->load->model('email_mod','email_template');
		
		$email_body_arr = $this->email_template->get_email_template(3);
		$email_subject = $email_body_arr['email_subject'];
		
		$email_body = $email_body_arr['email_body'];
		$email_body = str_replace($search_arr,$replace_arr,$email_body);
		
		
		$CONTACT_EMAIL = 'CONTACT_EMAIL';
		$send_to_email = get_global_settings($CONTACT_EMAIL); 

		 $EMAIL_FROM_TXT = 'EMAIL_FROM_TXT';
		 $email_from_txt = get_global_settings($EMAIL_FROM_TXT); 
		
		//Preparing Sending Email
		$config['charset'] = 'utf-8';
		$config['mailtype'] = 'html';
		$config['wordwrap'] = TRUE;			
		$config['protocol'] = 'mail';
			
		$this->load->library('email',$config);
		$this->email->from($email, $email_from_txt['setting_value']);
		$this->email->to(trim($send_to_email['setting_value']));
		$this->email->subject($email_subject);
		$this->email->message($email_body);
		
		$this->email->send();
		$this->email->clear();
		
		// EMAIL SENDING CODE - STOP
		
		return true;
		
	}//end submit_contactus_form($data)
	
	public function update_user_permissions($data){

		extract($data);

		if($menu_chk)
			$menu_chk_str = implode(',',$menu_chk);
		else
			$menu_chk_str = '';
		
		$this->db->dbprefix('usertype_permissions');
		$this->db->where('user_type_id', $usertype_id);

		if(trim($responsibility_id)!= '')
			$this->db->where('responsibility_id',$responsibility_id);
		else
			$this->db->where('responsibility_id IS NULL');

		$exist = $this->db->get('usertype_permissions')->row_array();

		if($exist){

			/*$ins_arr = array(

				'user_type' => $user_type_name,
				'permissions' => $this->db->escape_str(trim($menu_chk_str)),
				'description' => $user_type_desc,
				'status' => '1'
			);

			$this->db->dbprefix('usertype');
			$this->db->insert('usertype', $ins_arr);

			return true;*/

			$upd_arr = array(

				'user_type_id' => $usertype_id,
				'responsibility_id' => ($responsibility_id) ? $responsibility_id : NULL,
				'permissions' => $this->db->escape_str(trim($menu_chk_str)),
				'description' => $user_type_desc,
				'status' => '1'
			);

			$this->db->dbprefix('usertype_permissions');
			$this->db->where('user_type_id', $usertype_id);
			
			if(trim($responsibility_id)!= '')
				$this->db->where('responsibility_id',$responsibility_id);
			else
				$this->db->where('responsibility_id IS NULL');
			
			$this->db->update('usertype_permissions', $upd_arr);

			return $usertype_id;

		} else {

			/*$this->db->dbprefix('usertype');
			$this->db->where('id', $usertype_id);
			$exist_usertye = $this->db->get('usertype')->row_array();

			if(!$exist_usertye){

				$ins_arr = array(

					'user_type' => $user_type_name,
					'permissions' => $this->db->escape_str(trim($menu_chk_str)),
					'description' => $user_type_desc,
					'status' => '1'
				);

				$this->db->dbprefix('usertype');
				$this->db->insert('usertype', $ins_arr);

				$usertype_id = $this->db->insert_id();

			} //
			*/

			$ins_arr = array(

				'user_type_id' => $usertype_id,
				'responsibility_id' => ($responsibility_id) ? $responsibility_id : NULL ,
				'permissions' => $this->db->escape_str(trim($menu_chk_str)),
				'description' => $user_type_desc,
				'status' => '1'
			);

			$this->db->dbprefix('usertype_permissions');
			$this->db->insert('usertype_permissions', $ins_arr);

			return $usertype_id;

		} // if
		
	}//end update_user_permissions($data)
	
	
	 //Function verify_seo_url(): If URL String already exist in the database table record generate new.
	public function verify_seo_url($url_string,$table_name,$field_name,$exclude_self,$cms_type = ''){
	
		$this->db->dbprefix($table_name);
		$this->db->select('id');
		$this->db->where($field_name, $url_string); 

		if(trim($cms_type) != '')  
			$this->db->where('cms_type', $cms_type); 

		if($exclude_self != '') $this->db->where('id !=', $exclude_self); 
		$rs_count_rec = $this->db->get($table_name);
		//echo $this->db->last_query(); exit;
		if($rs_count_rec->num_rows() == 0) return $url_string;
		else{
			//Add Postfix and generate concatenate.
			$generate_postfix = $this->common->random_number_generator(3);
			$new_url_string = $url_string.'-'.$generate_postfix;
			return $this->common->verify_seo_url($new_url_string,$table_name,'url_slug',$exclude_self);
		
		}//end if
	
	}//end verify_seo_url
	
	// Function generate_seo_url();
	public function generate_seo_url($url_string){

	  $str_tmp = str_replace(' ', '-', $url_string); // Replaces all spaces with hyphens.
	  return strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', $str_tmp)); 	// Replaces all Special characters	

  }//end generate_seo_url

	
}//end file
?>