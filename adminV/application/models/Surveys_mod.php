<?php
class Surveys_mod extends CI_Model {
	
	function __construct(){
		parent::__construct();
    }

	//Function get_survey_payment_invoices(): Get the invoice list of Pharamcies
	public function get_survey_payment_invoices($pharmacy_id, $invoice_id = ''){
		
		$this->db->dbprefix('invoices');

		$this->db->where('pharmacy_id',$pharmacy_id);
		$this->db->order_by('id','DESC');

		$get = $this->db->get('invoices');
		
		if(trim($invoice_id)!='')
			return $get->row_array();
		else
			return $get->result_array();
			
	}//end get_survey_payment_invoices($pharmacy_id)

	//Function get_pharmacy_current_survey(): Return the survey which is currently in execution.
	public function get_pharmacy_current_survey($pharmacy_id, $survey_id = '', $survey_ref_no = '' ){

		$this->db->dbprefix('pharmacy_survey');
		
		$this->db->where('pharmacy_survey.pharmacy_id', $pharmacy_id);
		if(trim($survey_id) != '') $this->db->where('pharmacy_survey.id', $survey_id);
		if(trim($survey_ref_no) != '') $this->db->where('pharmacy_survey.survey_ref_no', $survey_ref_no);
		
		$this->db->where('pharmacy_survey.survey_status !=', '3'); //Not completed yet
		$this->db->where('pharmacy_survey.survey_finish_date  > "'.date('Y-m-d').'"');
		$this->db->where('pharmacy_survey.survey_start_date IS NOT NULL');
		
		$get = $this->db->get('pharmacy_survey');

		//echo $this->db->last_query(); 		exit;
		
		return $get->row_array();
		
	}//end get_pharmacy_current_survey($pharmacy_id)

	public function get_most_recent_survey($pharmacy_id){
		
		$this->db->dbprefix('pharmacy_survey');

		$this->db->where('pharmacy_id',$pharmacy_id);
		$this->db->order_by('id','DESC');
		$this->db->limit(0,1);

		$get = $this->db->get('pharmacy_survey');
		
		return $get->row_array();

	}//end get_most_recent_survey($pharmacy_id)

	// Start =>public function add_new_survey($pharmacy_id)
	public function add_new_survey($pharmacy_id){

		$current_survey_date = date('m/d');

		$SURVEY_END_MONTH = 'SURVEY_END_MONTH';
		$survey_end_global_value = get_global_settings($SURVEY_END_MONTH); //Set from the Global Settings

		$next_survey_end_date = filter_string($survey_end_global_value['setting_value']);

		if(strtotime($current_survey_date) > strtotime($next_survey_end_date)){
			
			$next_survey_end = strtotime("$next_survey_end_date +1 year"); 
			//$survey_session = date('Y').'-'.(date('Y')+1).'-survey';
			$survey_session = 'Survey '.date('Y').'-'.(date('Y')+1);
    		$survey_end_date = date('Y-m-d', $next_survey_end).'-'.(date('y', $next_survey_end)+1);

		} else {
			
			$next_survey_end = strtotime("$next_survey_end_date +0 year");
			// Survey 2016-2017
			$survey_session = 'Survey '.(date('Y', $next_survey_end)-1).'-'.date('y', $next_survey_end);
    		$survey_end_date = (date('Y-m-d', $next_survey_end)-1).'-'.date('y', $next_survey_end);

		} //end if

		$expiry_date = date('Y-m-d', $next_survey_end);
		$survey_year = date('Y', $next_survey_end);

		$survey_ref_no = $this->generate_survey_reference_no();

		$created_date = date('Y-m-d H:i:s');
		$created_by_ip = $this->input->ip_address();

		$ins_data = array(

			'pharmacy_id' => $this->db->escape_str(trim($pharmacy_id)),
			'survey_ref_no' => $this->db->escape_str(trim($survey_ref_no)),
			'survey_title' => $this->db->escape_str(trim($survey_session)),
			'survey_year' => $this->db->escape_str(trim($survey_year)),
			'survey_finish_date' => $this->db->escape_str(trim($expiry_date)),
			'survey_status' => $this->db->escape_str(trim('0')),
			'created_date' => $this->db->escape_str(trim($created_date)),
			'created_by_ip' => $this->db->escape_str(trim($created_by_ip))
		);
		
		//Inserting rota requested leaves data into the database. 
		$this->db->dbprefix('pharmacy_survey');
		$this->db->insert('pharmacy_survey', $ins_data);
		
		return $this->db->insert_id();

	} // End =>public function add_new_survey($pharmacy_id)

	//Function get_pharmacy_survey_list(): Will retirn list of Survey added by the pharmacies with any given status.
	public function get_pharmacy_survey_list($pharmacy_id, $survey_status = '', $survey_id = ''){
		
		$this->db->dbprefix('pharmacy_survey');
		$this->db->where('pharmacy_id',$pharmacy_id);
		if(trim($survey_status) != '') $this->db->where('survey_status',$survey_status);
		if(trim($survey_id) != '') $this->db->where('id',$survey_id);
		
		$this->db->order_by('id','DESC');
		
		$get = $this->db->get('pharmacy_survey');
		
		//echo $this->db->last_query(); 		exit;
		
		if(trim($survey_id) != '') 
			return $get->row_array();
		else
			return $get->result_array();

	}//end get_pharmacy_survey_list($pharmacy_id, $survey_status = '')

	// Start => public function save_invoice($pharmacy_id, $survey_id, $invoice_data)
	public function save_invoice($pharmacy_id, $survey_id, $invoice_data){

		extract($invoice_data);

		// Generate unique Order Number
		$order_no = $this->generate_order_number();

		$ins_arr = array(
			'pharmacy_id' => $pharmacy_id,
			'survey_id' => $survey_id,
			'paid_by_admin' => '1',
			'invoice_no' => $order_no,
			'subtotal' => $subtotal,
			'vat' => $vat_tax,
			'vat_percentage' => $vat_percentage,
			'survey_session' => $survey_session,
			'grand_total' => $grand_total,
			'payment_method' => 'ADMIN',
			'transaction_id' => NULL,

			'pharmacy_name' => $pharmacy_name,
			'pharmacy_owner_name' => $pharmacy_owner_name,
			'pharmacy_address' => $pharmacy_address,
			'pharmacy_address_2' => $pharmacy_address_2,
			'pharmacy_town' => $pharmacy_town,
			'pharmacy_county' => $pharmacy_county,
			'pharmacy_postcode' => $pharmacy_postcode,
			'from_name' => $from_name,
			'from_address' => $from_address,
			'from_address_2' => $from_address_2,
			'from_town' => $from_town,
			'from_county' => $from_county,
			'from_postcode' => $from_postcode,
			'from_phone' => $from_phone,
			'from_email' => $from_email,
			'from_website' => $from_website,

			'created_date' => date('Y-m-d H:i:s'),
			'created_by_ip' => $this->input->ip_address()
		);

		$this->db->dbprefix('invoices');
		$this->db->insert('invoices', $ins_arr);

		$last_invice_id = $this->db->insert_id();

		$this->db->dbprefix('invoices');
		$this->db->where('id', $last_invice_id);
		
		return $this->db->get('invoices')->row_array();
		
	} // End => public function save_invoice($pharmacy_id, $survey_id, $invoice_data)

	//Function generate_survey_reference_no(): Generate unique SURVEY REF No, by recursive call.
	public function generate_survey_reference_no(){

		$new_ref_no = strtoupper($this->common->random_number_generator(7));
		
		$this->db->dbprefix('pharmacy_survey');
		$this->db->select('id');
		$this->db->where('survey_ref_no',$new_ref_no);
		$get = $this->db->get('pharmacy_survey');
		//echo $this->db->last_query(); 		exit;
		
		if($get->num_rows > 0)
			$this->generate_survey_reference_no();
		else
			return $new_ref_no;
	}//end generate_survey_reference_no()

	//Function generate_order_number(): Generate unique Order number, by recursive call.
	public function generate_order_number(){
		
		$new_order_no = strtoupper($this->common->random_number_generator(4));
		
		$this->db->dbprefix('invoices');
		$this->db->select('id');
		$this->db->where('invoice_no',$new_order_no);
		$get = $this->db->get('invoices');
		// echo $this->db->last_query(); exit;
		
		if($get->num_rows > 0)
			$this->generate_order_number();
		else
			return $new_order_no;
		
	}//end generate_order_number()


	//Function get_survey_submitted_count(): Returns the number of Survey submitted so far
	public function get_survey_submitted_count($pharmacy_id, $survey_id, $submit_type = ''){
		
		$this->db->dbprefix('submitted_surveys');
		$this->db->select('COUNT(id) AS total_no_of_survey_submitted');
		
		$this->db->where('pharmacy_id', $pharmacy_id);
		$this->db->where('survey_id', $survey_id);
		if(trim($submit_type)!= '') $this->db->where('submit_type', $submit_type);
		
		$get = $this->db->get('submitted_surveys');

		//echo $this->db->last_query(); 		exit;
		$total_count =  $get->row_array();
		
		return $total_count['total_no_of_survey_submitted'];
		
		
	}//end get_survey_submitted_count($pharmacy_id, $survey_id)

} //End - file


?>