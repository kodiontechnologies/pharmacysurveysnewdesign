<?php
class Business_mod extends CI_Model {
	
	function __construct(){
		parent::__construct();
    }
	
	// Get Pharmacies List
	public function get_business_list($business_id=''){
		
		$this->db->dbprefix('business');
		$this->db->select('business.*');
		$this->db->from('business');
		
		if(trim($business_id)  !='')$this->db->where('business.id', $business_id);
		$this->db->order_by('id', 'DESC');
		$get_business = $this->db->get();
		

		if($business_id !='')
			$result_arr = $get_business->row_array();
		else 
			$result_arr = $get_business->result_array();
		
		//echo $this->db->last_query(); exit;
		return $result_arr;		
		
	} // end get_pharmacies_list
	
	// Start Function add bussines
	public function register_business($data){

	   	extract($data);
		
		$created_date = date('Y-m-d G:i:s');
		$created_by_ip = $this->input->ip_address();
		
		if($business_id ==''){
		
			$ins_data_business = array(
			
				'business_name' => $this->db->escape_str(trim($business_name)),
				'contact_name' => $this->db->escape_str(trim($contact_name)),
				'contact_no' => $this->db->escape_str(trim($contact_no)),
				'business_type' => $this->db->escape_str(trim($business_type)),
				'status' => $this->db->escape_str($status),
				'email_address' => $this->db->escape_str(trim($email_address)),
				'password' => $this->db->escape_str(trim(md5($password))),
				'created_date' => $this->db->escape_str(trim($created_date)),
				'created_by_ip' => $this->db->escape_str(trim($created_by_ip)),
			);
	
			//Inserting users data into the database. 
			$this->db->dbprefix('business');
			$ins_into_db = $this->db->insert('business', $ins_data_business);
		
		} else {
			
					$upd_data_business = array(
				
					'business_name' => $this->db->escape_str(trim($business_name)),
					'contact_name' => $this->db->escape_str(trim($contact_name)),
					'contact_no' => $this->db->escape_str(trim($contact_no)),
					'business_type' => $this->db->escape_str(trim($business_type)),
					'status' => $this->db->escape_str($status),
					'email_address' => $this->db->escape_str(trim($email_address)),
					'modified_date' => $this->db->escape_str(trim($created_date)),
					'modified_by_ip' => $this->db->escape_str(trim($created_by_ip)),
				);
				
				if($password !=''){
					$upd_data_business['password'] = $this->db->escape_str(trim(md5($password)));
				}
				//Inserting users data into the database. 
				$this->db->dbprefix('business');
				$this->db->where('id', $business_id);
				$ins_into_db = $this->db->update('business', $upd_data_business);
				
		} // else
		
		
		if($ins_into_db)
			return $ins_into_db;
		else
			return false;
				
	}//end add_new_user
	
	
	//Function delete_business(): Delete Business from  database
	public function delete_business($business_id){
		
		$this->db->dbprefix('business');
		$this->db->where('id',$business_id);
		$get_business = $this->db->delete('business');
		
		if($get_business)
			return true;
		else
			return false;
		
	}//end delete_business($business_id)

} //End - file

?>