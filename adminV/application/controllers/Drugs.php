<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Drugs extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		// Verify if the user is loggedin
		if(!$this->session->admin_id)
			redirect(SURL.'login');
		// if(!$this->session->admin_id)
		
		$this->load->model('Users_mod','users');
		$this->load->model('Pharmacies_mod','pharmacies');
		$this->load->model('Drugs_mod','drugs');
		
		$this->load->library('BreadcrumbComponent');
		
		//Sets the variable $header_contents to use the slice head (/views/slices/header_contents.php)
		$this->stencil->slice('header_contents');

		//Sets the variable $header_scripts to use the slice head (/views/slices/header_scripts.php)
		$this->stencil->slice('header_scripts');
		
		//Sets the variable $footer_contents to use the slice head (/views/slices/footer_contents.php)
		$this->stencil->slice('footer_contents');
		
		//Sets the variable $footer_scripts_modal to use the slice head (/views/slices/footer_scripts_modal.php)
		$this->stencil->slice('footer_scripts_modal');
		
		//Sets the variable $footer_scripts to use the slice head (/views/slices/footer_scripts.php)
		$this->stencil->slice('footer_scripts');

		// left_pane
		$this->stencil->slice('left_pane');
		
		// Layout
		$this->stencil->layout('page');
		
		// Form Validation	
		$this->stencil->js('kod_scripts/form_validation.js');
		$this->stencil->js('kod_scripts/form_validation/bootstrap_validator/dist/formValidation.min.js');
		$this->stencil->js('kod_scripts/form_validation/bootstrap_validator/dist/bootstrap.min.js');
		// end CMS file Validation
		

	} // public function __construct()

	public function index($pharmacy_id = ''){
		
			// Verify pharmacy id
		if(!$pharmacy_id)
			redirect(SURL.'pharmacies/pharmacies-list');
		// if(!$pharmacy_id)
		

		//set title
		$page_title = DEFAULT_TITLE;
		$this->stencil->title($page_title);	
		
		//Sets the Meta data
		$this->stencil->meta(array(
			'description' => DEFAULT_META_DESCRIPTION,
			'keywords' => DEFAULT_META_KEYWORDS,
			'meta_title' => DEFAULT_TITLE
		));
		
		// Page Heading
		$page_heading = 'Pharmacies';
		$data['page_heading'] = $page_heading;
		
		// Bread crumb
		$this->breadcrumbcomponent->add('<i class="ace-icon fa fa-home home-icon"></i> Home ', base_url().'pharmacies/pharmacies-list');	
		$this->breadcrumbcomponent->add(filter_string($page_heading), base_url().'pharmacies/pharmacies-list');					
		$data['breadcrum_data'] = $this->breadcrumbcomponent->output();
		
		$pharmacies_list = $this->pharmacies->get_pharmacies_list();
		$data['pharmacies_list'] = $pharmacies_list;
		
		$this->stencil->css('jquery.fancybox.css');
        $this->stencil->js('jquery.fancybox.js');
				
		$this->stencil->paint('drugs/drugs_listing',$data);
		
	}//end index()
	
	// Drugs listing
	public function drugs_list($pharmacy_id = ''){
		
			// Verify pharmacy id
		if(!$pharmacy_id)
			redirect(SURL.'pharmacies/pharmacies-list');
		// if(!$pharmacy_id)
		
		//set title
		$page_title = DEFAULT_TITLE;
		$this->stencil->title($page_title);	
		
		//Sets the Meta data
		$this->stencil->meta(array(
			'description' => DEFAULT_META_DESCRIPTION,
			'keywords' => DEFAULT_META_KEYWORDS,
			'meta_title' => DEFAULT_TITLE
		));
		
		
		$get_pharmacy_details = $this->pharmacies->get_pharmacy_details($pharmacy_id);
		
		$data['get_pharmacy_details'] = $get_pharmacy_details;
		
		// Page Heading
		$page_heading = ucfirst($get_pharmacy_details['pharmacy_name']);
		$data['page_heading'] = $page_heading;
		
		// Bread crumb
		$this->breadcrumbcomponent->add('<i class="ace-icon fa fa-home home-icon"></i> Home ', base_url().'pharmacies/pharmacies-list');	
		$this->breadcrumbcomponent->add(filter_string($page_heading), base_url().'drugs/drugs-list');					
		$data['breadcrum_data'] = $this->breadcrumbcomponent->output();		
		
		$drugs_list = $this->drugs->get_drugs_list($pharmacy_id);
		$data['drugs_list'] = $drugs_list;
		
		$this->stencil->css('jquery.fancybox.css');
        $this->stencil->js('jquery.fancybox.js');
				
		$this->stencil->paint('drugs/drugs_listing',$data);
		
	}//end index()
	
	// Start - public function get_drugs_details()
	public function get_drugs_details($pharmacy_id ='', $drug_id = ''){
	
		if($drug_id){
			
			$data['get_pharmacy_details'] = $this->pharmacies->get_pharmacy_details($pharmacy_id);
			
			$data['get_drug_details'] = $this->drugs->get_drug_details($pharmacy_id,$drug_id);
			$this->load->view('drugs/drug_view_detail', $data);
		}

	} // End - public function get_drug_details()
	
	// Start - public function add_edit()
	public function add_edit_default_drug($drug_id=''){
		
		// Update
		$data['edit_drug'] = $this->drugs->get_default_drug_details($drug_id);
	
		// Add New
		$this->stencil->layout('modal_layout');
		$this->stencil->paint('drugs/add_edit', $data);

	} // End - public function add_edit()
	
	/* DRUGS PROCESS */
	public function add_new_pharmacy_default_drug_process(){

		//If Post is not SET
		if(!$this->input->post() && !$this->input->post('add_new_drug_btn')) redirect(base_url());
	  
		extract($this->input->post());
	
		//Check if same drug exist in the database against the selected pharmacy
		$check_if_drug_already_exist = $this->drugs->drug_already_exist_in_pharmacy($this->input->post());

	  
		if($check_if_drug_already_exist){
		
			$this->session->set_flashdata('err_message', 'The drug you are trying to add already exist in your drugs listing.');
			redirect(base_url().'drugs/default-drugs-list');
		  
		}else{
			
			
			$add_new_drug = $this->drugs->add_new_drug_in_pharmacy($this->input->post());
			
			if($add_new_drug && $drug_id == ''){

				$this->session->set_flashdata('ok_message', 'New drug added successfully into pharmacy drug list');
				redirect(base_url().'drugs/default-drugs-list');
				
			} else if($add_new_drug && $drug_id != ''){

				$this->session->set_flashdata('ok_message', 'Drug updated successfully');
				redirect(base_url().'drugs/default-drugs-list');
				
			}else{

				$this->session->set_flashdata('err_message', 'Something went wrong while adding new drug, please try again later or contact system administrator');
				redirect(base_url().'drugs/default-drugs-list');
				
			}//end if($add_new_drug)
		  
		}//end if($check_if_drug_already_exist)
		
	}//end add_new_pharmacy_default_drug_process()
		
	// Default Drug listing
	public function default_drugs_list(){
		
		//set title
		$page_title = DEFAULT_TITLE;
		$this->stencil->title($page_title);	
		
		//Sets the Meta data
		$this->stencil->meta(array(
			'description' => DEFAULT_META_DESCRIPTION,
			'keywords' => DEFAULT_META_KEYWORDS,
			'meta_title' => DEFAULT_TITLE
		));
		
		// Bread crumb
		$this->breadcrumbcomponent->add('<i class="ace-icon fa fa-medkit"></i> Default Drugs Listing ', base_url().'drugs/default-drugs-list');	
		$data['breadcrum_data'] = $this->breadcrumbcomponent->output();		
		
		$default_drugs_list = $this->drugs->get_default_drug_list();
		$data['default_drugs_list'] = $default_drugs_list;
		
		$this->stencil->css('jquery.fancybox.css');
        $this->stencil->js('jquery.fancybox.js');
				
		$this->stencil->paint('drugs/default_drugs_listing',$data);
		
	}//end index()
	
	
	// Start - public function get_drug_deafult_details($drug_id = '')
	public function get_drug_deafult_details($drug_id = ''){
	
		if($drug_id){
			
			$data['get_drug_details'] = $this->drugs->get_default_drug_details($drug_id);
			$this->load->view('drugs/default_drug_view_detail', $data);
		}

	} // End - public function get_drug_deafult_details($drug_id = '')
	
	// Delete Drug Deafult
	public function delete_drug_deafult($drug_id){
		
		if($drug_id!="")
		{
			$get_drug_deafult_delete = $this->drugs->delete_drug_deafult($drug_id);
			
			if($get_drug_deafult_delete == '1')
			{
				$this->session->set_flashdata('ok_message', 'Drug deleted successfully.');
				redirect(SURL.'drugs/default-drugs-list');
				
			} else {
				
				$this->session->set_flashdata('err_message', 'Something went wrong, please try again later.');
				redirect(SURL.'drugs/default-drugs-list');
				
			}//end if if($drug_id != '')
			
		}//end if($drug_id!="")
			
	}//end function delete_drug_deafult($drug_id)
	
	/***********************************************/
	/*                 DRUG FORMS                  */
	/***********************************************/
	
	/* DRUGS FORM PROCESS */
	public function drug_form_process(){

		//If Post is not SET
		if(!$this->input->post() && !$this->input->post('add_new_drug_form_btn')) redirect(base_url());
	  
		extract($this->input->post());
	
		//Check if same drug exist in the database against the selected pharmacy
		$check_if_drug_already_exist = $this->drugs->drug_form_already_exist($this->input->post());

	  
		if($check_if_drug_already_exist){
		
			$this->session->set_flashdata('err_message', 'The drug you are trying to add already exist in your drugs form listing.');
			redirect(base_url().'drugs/drugs-form-list');
		  
		}else{
			
			
			$add_new_drug = $this->drugs->add_new_drug_form($this->input->post());
			
			if($add_new_drug && $drug_id == ''){

				$this->session->set_flashdata('ok_message', 'New drug form added successfully into pharmacy drug list');
				redirect(base_url().'drugs/drugs-form-list');
				
			} else if($add_new_drug && $drug_id != ''){

				$this->session->set_flashdata('ok_message', 'Drug form updated successfully');
				redirect(base_url().'drugs/drugs-form-list');
				
			}else{

				$this->session->set_flashdata('err_message', 'Something went wrong while adding new drug form, please try again later or contact system administrator');
				redirect(base_url().'drugs/drugs-form-list');
				
			}//end if($add_new_drug)
		  
		}//end if($check_if_drug_already_exist)
		
	}//end drug_form_process()
			
	// Default Drug form listing
	public function drugs_form_list(){
		
		//set title
		$page_title = DEFAULT_TITLE;
		$this->stencil->title($page_title);	
		
		//Sets the Meta data
		$this->stencil->meta(array(
			'description' => DEFAULT_META_DESCRIPTION,
			'keywords' => DEFAULT_META_KEYWORDS,
			'meta_title' => DEFAULT_TITLE
		));
		
		// Bread crumb
		$this->breadcrumbcomponent->add('<i class="ace-icon fa fa-medkit"></i> Drugs Form Listing ', base_url().'drugs/drugs-form-list');	
		$data['breadcrum_data'] = $this->breadcrumbcomponent->output();		
		
		$drugs_form_list = $this->drugs->get_drug_form_list();
		$data['drugs_form_list'] = $drugs_form_list;
		
		$this->stencil->css('jquery.fancybox.css');
        $this->stencil->js('jquery.fancybox.js');
				
		$this->stencil->paint('drugs/drugs_form_listing',$data);
		
	}//end index()
	
	// Start - public function get_drug_form_details($drug_id = '')
	public function get_drug_form_details($drug_id = ''){
	
		if($drug_id){
			
			$data['get_drug_form_details'] = $this->drugs->get_drug_form_details($drug_id);
			$this->load->view('drugs/drug_form_view_detail', $data);
		}

	} // End - public function get_drug_form_details($drug_id = '')
	
	
	// Start - public function add_edit_drug_form()
	public function add_edit_drug_form($drug_id=''){
		
		// Update
		$data['edit_drug_form'] = $this->drugs->get_drug_form_details($drug_id);
	
		// Add New
		$this->stencil->layout('modal_layout');
		$this->stencil->paint('drugs/edit_drug_form', $data);

	} // End - public function add_edit_drug_form()
	
	
	// Delete Drug form
	public function delete_drug_form($drug_id){
		
		if($drug_id!="")
		{
			$get_delete_drug_form = $this->drugs->delete_drug_form($drug_id);
			
			if($get_delete_drug_form == '1')
			{
				$this->session->set_flashdata('ok_message', 'Drug form deleted successfully.');
				redirect(SURL.'drugs/drugs-form-list');
				
			} else {
				
				$this->session->set_flashdata('err_message', 'Something went wrong, please try again later.');
				redirect(SURL.'drugs/drugs-form-list');
				
			}//end if if($drug_id != '')
			
		}//end if($drug_id!="")
			
	}//end function delete_drug_deafult($drug_id)
	
	
	
		
		
} // End => Ci => Class Users
