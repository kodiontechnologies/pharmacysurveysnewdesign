<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{

		parent::__construct();

		$this->load->model('Common_mod', 'common');
		$this->load->model('Login_mod', 'login');

		// Load BreadcrumbComponent Library
		//$this->load->library('BreadcrumbComponent');
		
		// Sets the variable $header_contents to use the slice head (/views/slices/header_contents.php)
		$this->stencil->slice('login_header');
		
		//Sets the variable $header_scripts to use the slice head (/views/slices/header_scripts.php)
		$this->stencil->slice('header_scripts');
		
		//Sets the variable $footer_contents to use the slice head (/views/slices/footer_contents.php)
		$this->stencil->slice('login_footer');
		
		$this->stencil->slice('footer_scripts');
		
		// Form Validation	
	    $this->stencil->js('kod_scripts/form_validation.js');
		$this->stencil->js('kod_scripts/form_validation/bootstrap_validator/dist/formValidation.min.js');
		$this->stencil->js('kod_scripts/form_validation/bootstrap_validator/dist/bootstrap.min.js');
		// end CMS file Validation

	}

	public function index(){
		
		//set title
		$page_title = DEFAULT_TITLE;
		$this->stencil->title($page_title);	
		
		//Sets the Meta data
		$this->stencil->meta(array(
			'description' => DEFAULT_DESCRIPTION,
			'meta_title' => DEFAULT_TITLE
		));
		
		$this->stencil->js('https://www.google.com/recaptcha/api.js');
		//load login template
		$this->stencil->layout('login'); //frontend template
		
		$this->stencil->paint('login/login'); 
		
	} //end index()
	
	//Function login_process(): Process and authenticate the login form	
	public function login_process(){

		//If Post is not SET
		if(!$this->input->post() && !$this->input->post('login_btn')) redirect(SURL.'page-not-found');

		// Captcha Validation
		if($this->input->post('g-recaptcha-response') == ''){
			 
			$this->session->set_flashdata('err_message', 'Please verify Captcha');
			redirect(base_url().'login');

		}//end if($this->input->post('g-recaptcha-response') == '')
		
		// PHP Validation 
		$this->form_validation->set_rules('email_address', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');
		
		$chk_isvalid_user = $this->login->validate_credentials($this->input->post('email_address'),$this->input->post('password'));
		
		if($this->form_validation->run() == FALSE){
			 
			 // PHP Error 
			$this->session->set_flashdata('err_message', validation_errors());
			redirect(base_url().'login');
			
		} else {
		
				if($chk_isvalid_user){
					
					
					$this->session->set_userdata($chk_isvalid_user);
					
					if($this->session->userdata('login_user_type')=='admin'){
						
						redirect(base_url().'pharmacies/pharmacies-list');
					
					} 
					
				}else{
		
					$this->session->set_flashdata('err_message', 'Invalid Username or Password. Please try again!');
					redirect(base_url().'login');
					
				}//end if($chk_isvalid_user) 
	        } // end else condition
		
	}//end public function login_process()
	
	//Function activation() Email activation process
	public function activate_account(){

		$verify_account = $this->login->verify_email_account($this->input->get());
		
		if($verify_account){
			
			$get_user_details = $this->users->get_user_details($this->input->get('uid'));
			
			if($get_user_details['admin_verify_status'] == 0)
				$this->session->set_flashdata('ok_message', 'Your account is successfully verified and sent for admin approval. Please try to login after sometime.');
			else
				$this->session->set_flashdata('ok_message', 'Your account is successfully verified, please login with your details.');	
			redirect(base_url().'login');

		}else{
			$this->session->set_flashdata('err_message', 'We were not able to verify your account. Please contact site administrator for further details.');
			redirect(base_url().'login');
			
		}//end if($verify_account)
		
	} // End - activation()
	
	public function forgot_password(){
	
		//set title
		$page_title = DEFAULT_TITLE;
		$this->stencil->title($page_title);	
		
		//Sets the Meta data
		$this->stencil->meta(array(
			'description' => DEFAULT_DESCRIPTION,
			'meta_title' => DEFAULT_TITLE
		));
		
		$this->stencil->js('https://www.google.com/recaptcha/api.js');

		
		$this->stencil->layout('login'); //frontend template
		$this->stencil->paint('login/forgot_password',$data);
		
	} //end forgot_password()

	//Function forgot_password_process(): Process and authenticate the forgot password
	public function forgot_password_process(){
		
		//If Post is not SET
		if(!$this->input->post() && !$this->input->post('fgetpass_btn')) redirect(base_url());

		// Captcha Validation
		if($this->input->post('g-recaptcha-response') == ''){
			 
			$this->session->set_flashdata('err_message', 'Please verify Captcha');
			redirect(base_url().'login/forgot-password');

		}//end if($this->input->post('g-recaptcha-response') == '')
		
		// PHP Validation
		$this->form_validation->set_rules('email_address', 'Email', 'trim|required|valid_email');
				
		$user_exist_arr = $this->login->get_admin_by_email($this->input->post('email_address'));
			

		if($this->form_validation->run() == FALSE){
			 
			 // PHP Error 
			$this->session->set_flashdata('err_message', validation_errors());
			redirect(base_url().'login/forgot-password');
			
		} else {
				
			if($user_exist_arr){
			
				//Sending Email to the User
				$send_new_password = $this->login->send_new_password($user_exist_arr);
			
				if($send_new_password){
					$this->session->set_flashdata('ok_message', 'New Password is sent to your Email Address. Please check your email address for your New Password');
					redirect(base_url().'login/forgot-password');
				
				}else{
				
					$this->session->set_flashdata('err_message', 'Something went wrong, please try again later.');
					redirect(base_url().'login/forgot-password');
			
			} // End - if($send_new_password)
			
		  }else{

			$this->session->set_flashdata('err_message', 'The Email Address do not exist in your database. Please try again, with the correct email address');
			redirect(base_url().'login/forgot-password');
			
		}//end if($chk_isvalid_user) 
		
	 } // Else  
		
	}//end forgot_password_process
	
	//Function logout(): Process the logout operation
	public function logout(){

		//Distroy All Sessions
		// $this->session->unset_userdata('uid');

		$this->session->sess_destroy();

		$this->session->set_flashdata('ok_message', 'You have successfully logged out.');
		redirect(base_url().'login');

	}//end logout
	
} /* End of file */
