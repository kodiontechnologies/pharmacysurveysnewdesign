<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends CI_Controller {

	public function __construct()
	{
		parent::__construct();	

		// Load models
		$this->load->model('common_mod', 'common');
		$this->load->model('cms_mod', 'cms');
		$this->load->model('faq_mod', 'faq');

		//Sets the variable $header_contents to use the slice head (/views/slices/header_contents.php)
		$this->stencil->slice('header_contents');
		
		//Sets the variable $header_scripts to use the slice head (/views/slices/header_scripts.php)
		$this->stencil->slice('header_scripts');
		
		//Sets the variable $footer_contents to use the slice head (/views/slices/footer_contents.php)
		$this->stencil->slice('footer_contents');
		
		//Sets the variable $footer_scripts to use the slice head (/views/slices/footer_scripts.php)
		$this->stencil->slice('footer_scripts');
		
		//load main template
		$this->stencil->layout('home'); //frontend template

	}

	// Start - function index() : Function to view CMS pages by page slug URL
 	public function index($seo_url = ''){

 		// Verify valid request
		if($seo_url == '')
			redirect(base_url().'page-not-found');
		
		if($seo_url =='faqs'){
			// Get faq questions answers from DB
			$list_faq = $this->faq->get_faq();
			$data['list_faq'] = $list_faq;
		}
		// Get Page CMS data from DB
		$cms_data_arr = $this->cms->get_cms_page($seo_url);

		// Verify if page exist in the DB
		if($cms_data_arr['cms_page_count'] == 0){
			redirect(base_url().'pages/page-not-found');
		} else {
			
			// Set title
			$page_title = $cms_data_arr['cms_page_arr']['page_title'];
			$this->stencil->title($page_title);	
			
			// Sets the Meta data
			$this->stencil->meta(array(
				'description' => $cms_data_arr['cms_page_arr']['meta_description'],
				'keywords' => $cms_data_arr['cms_page_arr']['meta_keywords']
			));

			// Set data "page_data"
			$data['page_data'] = $cms_data_arr['cms_page_arr'];

			// Set data "page_request"
			$data['page_request'] = $seo_url;
			
		} // End if($cms_data_arr['cms_page_count'] == 0)
		
		// Js file using for  Form validation
		$this->stencil->js('kod_scripts/form_validation/bootstrap_validator/dist/validator.js');
		$this->stencil->js('kod_scripts/form_validation/bootstrap_validator/dist/validator.min.js');
		
		$this->stencil->paint('pages/cms',$data);
		
	} // End - function index()
	
	// Contact us process Haad Exam
	public function contactus_process(){
		
		
		// If Post is not SET
		if(!$this->input->post() && !$this->input->post('contact_btn')) redirect(base_url().'page-not-found');
		
		
		$submit_contact_frm = $this->common->submit_contactus_form($this->input->post());
		
		if($submit_contact_frm){
			
			$this->session->set_flashdata('ok_message', 'Your comments sent successfully for admin review.');
			redirect(base_url().'contact-us');

		}//end if($submit_contact_frm)
		
	}//end contactus_process()
	
	// Start - page_not_found_404()
	public function page_not_found_404(){
		echo "Page 404, stop";
		//$this->load->view('errors/page_not_found',$data);
	} // End - page_not_found_404()

}
