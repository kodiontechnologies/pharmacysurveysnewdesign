<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Pharmacies extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		// Verify if the user is loggedin
		if(!$this->session->admin_id)
			redirect(SURL.'login');
		// if(!$this->session->uid)
		
		$this->load->model('Pharmacies_mod','pharmacies');
		$this->load->model('Business_mod','group');
		$this->load->model('Users_mod','users');
		$this->load->model('Surveys_mod','survey');
		$this->load->model('Common_mod','common');

		$this->load->library('BreadcrumbComponent');
		
		//Sets the variable $header_contents to use the slice head (/views/slices/header_contents.php)
		$this->stencil->slice('header_contents');

		//Sets the variable $header_scripts to use the slice head (/views/slices/header_scripts.php)
		$this->stencil->slice('header_scripts');
		
		//Sets the variable $footer_contents to use the slice head (/views/slices/footer_contents.php)
		$this->stencil->slice('footer_contents');
		
		//Sets the variable $footer_scripts_modal to use the slice head (/views/slices/footer_scripts_modal.php)
		$this->stencil->slice('footer_scripts_modal');
		
		//Sets the variable $footer_scripts to use the slice head (/views/slices/footer_scripts.php)
		$this->stencil->slice('footer_scripts');

		// left_pane
		$this->stencil->slice('left_pane');
	
		// Layout
		$this->stencil->layout('page');
		
			// Form Validation	
		$this->stencil->js('kod_scripts/form_validation.js');
		$this->stencil->js('kod_scripts/form_validation/bootstrap_validator/dist/formValidation.min.js');
		$this->stencil->js('kod_scripts/form_validation/bootstrap_validator/dist/bootstrap.min.js');
		// end CMS file Validation

	} // public function __construct()

	public function index(){

		//set title
		$page_title = DEFAULT_TITLE;
		$this->stencil->title($page_title);	
		
		//Sets the Meta data
		$this->stencil->meta(array(
			'description' => DEFAULT_META_DESCRIPTION,
			'keywords' => DEFAULT_META_KEYWORDS,
			'meta_title' => DEFAULT_TITLE
		));
		
		// Page Heading
		$page_heading = 'Pharmacies';
		$data['page_heading'] = $page_heading;
		
		// Bread crumb
		$this->breadcrumbcomponent->add('<i class="ace-icon fa fa-home home-icon"></i> Home ', base_url().'pharmacies/pharmacies-list');	
		$this->breadcrumbcomponent->add(filter_string($page_heading), base_url().'pharmacies/pharmacies-list');					
		$data['breadcrum_data'] = $this->breadcrumbcomponent->output();
		
		$pharmacies_list = $this->pharmacies->get_pharmacies_list();
		$data['pharmacies_list'] = $pharmacies_list;
		
		$this->stencil->css('jquery.fancybox.css');
        $this->stencil->js('jquery.fancybox.js');
				
		$this->stencil->paint('pharmacies/pharmacies_listing',$data);
		
	}//end index()
	
	
	// Pharmacies listing
	public function pharmacies_list(){
		
		//set title
		$page_title = DEFAULT_TITLE;
		$this->stencil->title($page_title);	
		
		//Sets the Meta data
		$this->stencil->meta(array(
			'description' => DEFAULT_META_DESCRIPTION,
			'keywords' => DEFAULT_META_KEYWORDS,
			'meta_title' => DEFAULT_TITLE
		));
		
		// Page Heading
		$page_heading = 'Pharmacies';
		$data['page_heading'] = $page_heading;
		
		// Bread crumb
		$this->breadcrumbcomponent->add('<i class="ace-icon fa fa-home home-icon"></i> Home ', base_url().'pharmacies/pharmacies-list');	
		$this->breadcrumbcomponent->add(filter_string($page_heading), base_url().'pharmacies/pharmacies-list');					
		$data['breadcrum_data'] = $this->breadcrumbcomponent->output();
		
		$pharmacies_list = $this->pharmacies->get_pharmacies_list();
		$data['pharmacies_list'] = $pharmacies_list;
		
		//$survey_session = date('Y').'-'.(date('Y')+1);
		//$data['survey_session'] = $survey_session;
		
		$this->stencil->css('jquery.fancybox.css');
        $this->stencil->js('jquery.fancybox.js');
				
		$this->stencil->paint('pharmacies/pharmacies_listing',$data);
		
	}//end index()

	// Edit Pharmacy
	public function edit_pharmacy($pharmacy_id){
		
		$pharamcy_details = $this->pharmacies->get_pharmacy_details($pharmacy_id);
		
		//print_this($pharmacies_details); exit;
		$data['pharmacy_details'] = $pharamcy_details;
		$this->stencil->layout('modal_layout');
		
		$this->stencil->paint('pharmacies/edit_pharmacy',$data);
		
	}//end function edit_pharmacy($pharmacy_id)

	public function edit_pharmacy_process(){
		
		extract($this->input->post());
		
		if(!$this->input->post('edit_pharamcy_btn')) redirect(SURL);
		
		
		$this->form_validation->set_rules('business_type', 'Business Type', 'trim|required');
		$this->form_validation->set_rules('group_id', 'Group id', 'trim|required');
		$this->form_validation->set_rules('pharmacy_name', 'Business Name', 'trim|required');
		$this->form_validation->set_rules('contact_no', 'Business Phone Number', 'trim|required');
		$this->form_validation->set_rules('address', 'Address', 'trim|required');
		$this->form_validation->set_rules('town', 'Business Town', 'trim|required');
		$this->form_validation->set_rules('postcode', 'Postcode', 'trim|required');	
		$this->form_validation->set_rules('sms_quota', 'SMS Quota', 'trim|required');

		if($this->form_validation->run() == FALSE){

			// session set form data in fields
		    $this->session->set_flashdata($this->input->post());
			// PHP Error 
			$this->session->set_flashdata('err_message', validation_errors());
			redirect(SURL.'pharmacies/pharmacies-list');

		} else {
		
		   $verify_if_valid_group = $this->group->get_business_list($group_id);
			
			if(!$verify_if_valid_group){
				
			    // session set form data in fields
		        $this->session->set_flashdata($this->input->post());
				$this->session->set_flashdata('err_message', 'You have entered an invalid Group ID. Please try again.');
				redirect(SURL.'pharmacies/pharmacies-list');

			} else {
		
				$update_pharamcy = $this->pharmacies->update_pharamcy($this->input->post());
				
				if($update_pharamcy){
		
					$this->session->set_flashdata('ok_message', 'Pharmacy record updated successfully');
					redirect(SURL.'pharmacies/pharmacies-list');
					
				}else{
					
					$this->session->set_flashdata('err_message', 'Something went wrong please try again.');
					redirect(SURL.'pharmacies/pharmacies-list');
				}
				
			}
		}
				
	}//end edit_pharmacy_process()
	// Start - public function get_pharmacy_details()
	public function get_pharmacy_details($pharmacy_id = ''){
				
		if($pharmacy_id){
			
			$data['get_pharmacy_details'] = $this->pharmacies->get_pharmacy_details($pharmacy_id);
			$this->load->view('pharmacies/pharmacy_view_detail', $data);
		}

	} // End - public function get_pharmacy_details()
	
	
	// Activate deactivate pharmacy
	public function activate_deactivate_pharmacy($pharmacy_id ='', $status =''){
		
		$get_pharmacy_details = $this->pharmacies->get_pharmacy_details($pharmacy_id);
		
		$data['get_pharmacy_details'] = $get_pharmacy_details;
		
			if($pharmacy_id!="")
			{
				$activate_deactivate_pharmacy = $this->pharmacies->activate_deactivate_pharmacy($pharmacy_id,$status);
				
				
				if($activate_deactivate_pharmacy =='1' && $status =='1')
				{
					
					$this->session->set_flashdata('ok_message', ucfirst(filter_string($get_pharmacy_details['pharmacy_name'])).' successfully deactivated.');
					redirect(SURL.'pharmacies/pharmacies-list');
					
				} else if($activate_deactivate_pharmacy =='1' && $status =='0')
				{
					
					$this->session->set_flashdata('ok_message', ucfirst(filter_string($get_pharmacy_details['pharmacy_name'])).' successfully activated.');
					redirect(SURL.'pharmacies/pharmacies-list');
					
				} else {
					
					$this->session->set_flashdata('err_message', 'Something went wrong, please try again later.');
					redirect(SURL.'pharmacies/pharmacies-list');
					
				}//end if if($delete_page != '')
				
			}//end if($page_id!="")
			
	}//end function delete_prescriber($prescriber_id)
	
	// pharmacy add 
	public function add_new_pharmacy_process(){
		
	//If Post is not SET
	if(!$this->input->post() && !$this->input->post('new_pharmacy_btn')) redirect(base_url());
		
	    extract($this->input->post());

		$this->form_validation->set_rules('business_type', 'Business Type', 'trim|required');
		$this->form_validation->set_rules('group_id', 'Group id', 'trim|required');
		$this->form_validation->set_rules('pharmacy_name', 'Business Name', 'trim|required');
		$this->form_validation->set_rules('owner_name', 'Owner Name', 'trim|required');
		$this->form_validation->set_rules('contact_no', 'Business Phone Number', 'trim|required');
		$this->form_validation->set_rules('email_address', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('address', 'Address', 'trim|required');
		$this->form_validation->set_rules('town', 'Business Town', 'trim|required');
		$this->form_validation->set_rules('postcode', 'Postcode', 'trim|required');	
		$this->form_validation->set_rules('survey_price', 'Survey Price', 'trim|required');	
		$this->form_validation->set_rules('password', 'Password', 'trim|required');

		if($this->form_validation->run() == FALSE){

			// session set form data in fields
		    $this->session->set_flashdata($this->input->post());
			// PHP Error 
			$this->session->set_flashdata('err_message', validation_errors());
			redirect(SURL.'pharmacies/pharmacies-list');

		} else {
			
			$verify_if_valid_group = $this->group->get_business_list($group_id);
			
			if(!$verify_if_valid_group){
				
			    // session set form data in fields
		        $this->session->set_flashdata($this->input->post());
				$this->session->set_flashdata('err_message', 'You have entered an invalid Group ID. Please try again.');
				redirect(SURL.'pharmacies/pharmacies-list?t=2');

			}//end if(!$verify_if_valid_group)
			
			$verify_pharmacy_exist = $this->pharmacies->verify_if_pharmacy_already_exist($email_address);

			if($verify_pharmacy_exist){
				
			    // session set form data in fields
		        $this->session->set_flashdata($this->input->post());
				$this->session->set_flashdata('err_message', 'The email you have entered already exist, please try another one.');
				redirect(SURL.'pharmacies/pharmacies-list?t=2');
				
			} else {
							
				 // add update pharmacies
				 $pharmacy_id = $this->pharmacies->register_pharmacy($this->input->post()); 
				// if add or update succuess
				
				if($pharmacy_id){
					
					// Insert Survey
					$survey_id = $this->survey->add_new_survey($pharmacy_id);
					
					if($survey_id){

						$VAT_PERCENTAGE = 'VAT_PERCENTAGE';
						$vat_percentage = get_global_settings($VAT_PERCENTAGE); //Set from the Global Settings
			
						$vat_amount = (trim($vat_percentage['setting_value']) / 100) * $survey_price;
						$vat_amount = filter_price($vat_amount);
						
						$grand_total = $survey_price + $vat_amount;
			
						$survey_session = $this->survey->get_pharmacy_survey_list($pharmacy_id, '', $survey_id);
			
						// Insert Invoice
						$invoice_data['subtotal'] = $survey_price;
						$invoice_data['vat_tax'] = $vat_amount;
						$invoice_data['vat_percentage'] = trim($vat_percentage['setting_value']);
						$invoice_data['survey_session'] = $survey_session['survey_title'];
						$invoice_data['grand_total'] = $grand_total;
						$invoice_data['payment_method'] = 'ADMIN';
						$invoice_data['transaction_id'] = $grand_total;
						
						// Send Pharmacy Registration Email
						
						// Get price from GLOBAL SETTINGS
						$FROM_BUSINESS_NAME = 'FROM_BUSINESS_NAME';
						$from_business_name = get_global_settings($FROM_BUSINESS_NAME); //Set from the Global Settings
						$from_business_name = filter_string($from_business_name['setting_value']);
						
						// Get price from GLOBAL SETTINGS
						$FROM_ADDRESS = 'FROM_ADDRESS';
						$from_address = get_global_settings($FROM_ADDRESS); //Set from the Global Settings
						$from_address = filter_string($from_address['setting_value']);
						
						// Get price from GLOBAL SETTINGS
						$FROM_ADDRESS_2 = 'FROM_ADDRESS_2';
						$from_address_2 = get_global_settings($FROM_ADDRESS_2); //Set from the Global Settings
						$from_address_2 = filter_string($from_address_2['setting_value']);
						
						// Get price from GLOBAL SETTINGS
						$FROM_COUNTY = 'FROM_COUNTY';
						$from_county = get_global_settings($FROM_COUNTY); //Set from the Global Settings
						$from_county = filter_string($from_county['setting_value']);
						
						// Get price from GLOBAL SETTINGS
						$FROM_POSTCODE = 'FROM_POSTCODE';
						$from_postcode = get_global_settings($FROM_POSTCODE); //Set from the Global Settings
						$from_postcode = filter_string($from_postcode['setting_value']);
						
						// Get price from GLOBAL SETTINGS
						$FROM_TOWN = 'FROM_TOWN';
						$from_town = get_global_settings($FROM_TOWN); //Set from the Global Settings
						$from_town = filter_string($from_town['setting_value']);
						
						// Get price from GLOBAL SETTINGS
						$FROM_EMAIL = 'FROM_EMAIL';
						$from_email = get_global_settings($FROM_EMAIL); //Set from the Global Settings
						$from_email = filter_string($from_email['setting_value']);
						
						// Get price from GLOBAL SETTINGS
						$FROM_WEBSITE = 'FROM_WEBSITE';
						$from_website = get_global_settings($FROM_WEBSITE); //Set from the Global Settings
						$from_website = filter_string($from_website['setting_value']);
						
						// Get price from GLOBAL SETTINGS
						$FROM_PHONE = 'FROM_PHONE';
						$from_phone = get_global_settings($FROM_PHONE); //Set from the Global Settings
						$from_phone = filter_string($from_phone['setting_value']);
						
						// Insert invoice : along with new fields //
						$invoice_data['pharmacy_name'] = $pharmacy_name;
						$invoice_data['pharmacy_owner_name'] = $owner_name;
						$invoice_data['pharmacy_address'] = $address;
						$invoice_data['pharmacy_address_2'] = $address_2;
						$invoice_data['pharmacy_town'] = $town;
						$invoice_data['pharmacy_county'] = $county;
						$invoice_data['pharmacy_postcode'] = $postcode;
						$invoice_data['from_name'] = $from_business_name;
						$invoice_data['from_address'] = $from_address;
						$invoice_data['from_address_2'] = $from_address_2;
						$invoice_data['from_town'] = $from_town;
						$invoice_data['from_county'] = $from_county;
						$invoice_data['from_postcode'] = $from_postcode;
						$invoice_data['from_phone'] = $from_phone;
						$invoice_data['from_email'] = $from_email;
						$invoice_data['from_website'] = $from_website;
		
						$invoice_generated = $this->survey->save_invoice($pharmacy_id, $survey_id, $invoice_data, $survey_price);
						
					}//end if($survey_id)
								
					$this->session->set_flashdata('ok_message', 'Pharmacy added successfully.');
					redirect(SURL.'pharmacies/pharmacies-list');
					
				}else{
					
					$this->session->set_flashdata('err_message', 'Something went wrong, please try again later.');
					redirect(SURL.'pharmacies/pharmacies-list');
				
				}//end pharmacies_id
				
		  } // end else 
		
		}// end else
	} // end public function add_new_pharmacy_process(){
		
	public function help_videos(){
		
		//set title
		$page_title = DEFAULT_TITLE;
		$this->stencil->title($page_title);	
		
		//Sets the Meta data
		$this->stencil->meta(array(
			'description' => DEFAULT_META_DESCRIPTION,
			'keywords' => DEFAULT_META_KEYWORDS,
			'meta_title' => DEFAULT_TITLE
		));
		
		// Page Heading
		$page_heading = 'Help Videos';
		$data['page_heading'] = $page_heading;
		
		// Bread crumb
		$this->breadcrumbcomponent->add('<i class="ace-icon fa fa-home home-icon"></i> Home ', base_url().'pharmacies/pharmacies-list');	
		$this->breadcrumbcomponent->add(filter_string($page_heading), base_url().'pharmacies/pharmacies-list');					
		$data['breadcrum_data'] = $this->breadcrumbcomponent->output();
		
		$video_list = $this->pharmacies->get_help_videos_list();
		$data['video_list'] = $video_list;
		
		//$survey_session = date('Y').'-'.(date('Y')+1);
		//$data['survey_session'] = $survey_session;
		
		$this->stencil->css('jquery.fancybox.css');
        $this->stencil->js('jquery.fancybox.js');
				
		$this->stencil->paint('pharmacies/help_videos',$data);
		
	}//end help_videos()		
	
	public function add_edit_help_video_process(){

		extract($this->input->post());
		
		if(!$this->input->post('video_title')) redirect(SURL);
		
	   $add_new_video = $this->pharmacies->add_edit_help_videos_list($this->input->post());
	   
		if($add_new_video){

			if($video_id != '')
				$this->session->set_flashdata('ok_message', 'Video updated successfully');		
			else
				$this->session->set_flashdata('ok_message', 'New Video added successfully');
			redirect(SURL.'pharmacies/help-videos');
			
		}else{
			
			$this->session->set_flashdata('err_message', 'Something went wrong please try again.');
			redirect(SURL.'pharmacies/help-videos');
		}
		
	}//end add_edit_help_video_process()
	
	public function edit_help_video($video_id){

		if($video_id != ''){

			$get_help_videos_list = $this->pharmacies->get_help_videos_list($video_id);
			$data['help_videos'] = $get_help_videos_list;
			
			$this->stencil->layout('modal_layout');
			$this->stencil->paint('pharmacies/edit_help_video',$data);
				
		}else{
			$this->session->set_flashdata('err_message', 'Something went wrong please try again.');
			redirect(SURL.'pharmacies/help-videos');
		}//end if($video_id != '')

		
	}//end edit_help_video($video_id)
	
	public function delete_help_video($video_id){
		
		if($video_id != ''){

			$delete_help_video = $this->pharmacies->delete_help_video($video_id);
			
			if($delete_help_video){

				$this->session->set_flashdata('ok_message', 'Video successfully deleted');
				redirect(SURL.'pharmacies/help-videos');
				
			}else{

				$this->session->set_flashdata('err_message', 'Something went wrong please try again.');
				redirect(SURL.'pharmacies/help-videos');
			}//end if($delete_help_video)
			
		}else{
			$this->session->set_flashdata('err_message', 'Something went wrong please try again.');
			redirect(SURL.'pharmacies/help-videos');
		}//end if($video_id != '')
		
	}//end delete_help_video($video_id)
	
} // End => Ci => Class Users
