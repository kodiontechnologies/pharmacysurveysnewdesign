<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		// Verify if the user is loggedin
		if(!$this->session->admin_id)
			redirect(SURL.'login');
		// if(!$this->session->admin_id)
		
		$this->load->model('Users_mod','users');
		$this->load->model('Pharmacies_mod','pharmacies');
		
		$this->load->library('BreadcrumbComponent');
		
		//Sets the variable $header_contents to use the slice head (/views/slices/header_contents.php)
		$this->stencil->slice('header_contents');

		//Sets the variable $header_scripts to use the slice head (/views/slices/header_scripts.php)
		$this->stencil->slice('header_scripts');
		
		//Sets the variable $footer_contents to use the slice head (/views/slices/footer_contents.php)
		$this->stencil->slice('footer_contents');
		
		//Sets the variable $footer_scripts_modal to use the slice head (/views/slices/footer_scripts_modal.php)
		$this->stencil->slice('footer_scripts_modal');
		
		//Sets the variable $footer_scripts to use the slice head (/views/slices/footer_scripts.php)
		$this->stencil->slice('footer_scripts');

		// left_pane
		$this->stencil->slice('left_pane');
		
		// Layout
		$this->stencil->layout('page');
		
		// Form Validation	
		$this->stencil->js('kod_scripts/form_validation.js');
		$this->stencil->js('kod_scripts/form_validation/bootstrap_validator/dist/formValidation.min.js');
		$this->stencil->js('kod_scripts/form_validation/bootstrap_validator/dist/bootstrap.min.js');
		// end CMS file Validation

	} // public function __construct()

	public function index($pharmacy_id = ''){
		
		// Verify pharmacy id
		if(!$pharmacy_id)
			redirect(SURL.'pharmacies/pharmacies-list');
		// if(!$pharmacy_id)

		//set title
		$page_title = DEFAULT_TITLE;
		$this->stencil->title($page_title);	
		
		//Sets the Meta data
		$this->stencil->meta(array(
			'description' => DEFAULT_META_DESCRIPTION,
			'keywords' => DEFAULT_META_KEYWORDS,
			'meta_title' => DEFAULT_TITLE
		));
		
		$get_pharmacy_details = $this->pharmacies->get_pharmacy_details($pharmacy_id);
		
		$data['get_pharmacy_details'] = $get_pharmacy_details;
		
		// Page Heading
		$page_heading = ucfirst($get_pharmacy_details['pharmacy_name']);
		$data['page_heading'] = $page_heading;
		
		// Bread crumb
		$this->breadcrumbcomponent->add('<i class="ace-icon fa fa-home home-icon"></i> Home ', base_url().'pharmacies/pharmacies-list');	
		$this->breadcrumbcomponent->add(filter_string($page_heading), base_url().'pharmacies/pharmacies-list');					
		$data['breadcrum_data'] = $this->breadcrumbcomponent->output();
		
		$pharmacies_list = $this->pharmacies->get_pharmacies_list();
		$data['pharmacies_list'] = $pharmacies_list;
		
		$this->stencil->css('jquery.fancybox.css');
        $this->stencil->js('jquery.fancybox.js');
				
		$this->stencil->paint('pharmacies/pharmacies_listing',$data);
		
	}//end index()
	
	
	// Users listing
	public function users_list($pharmacy_id = ''){
		
		// Verify pharmacy id
		if(!$pharmacy_id)
			redirect(SURL.'pharmacies/pharmacies-list');
		// if(!$pharmacy_id)
		
		//set title
		$page_title = DEFAULT_TITLE;
		$this->stencil->title($page_title);	
		
		//Sets the Meta data
		$this->stencil->meta(array(
			'description' => DEFAULT_META_DESCRIPTION,
			'keywords' => DEFAULT_META_KEYWORDS,
			'meta_title' => DEFAULT_TITLE
		));
		
		
		$get_pharmacy_details = $this->pharmacies->get_pharmacy_details($pharmacy_id);
		
		$data['get_pharmacy_details'] = $get_pharmacy_details;
		
		// Page Heading
		$page_heading = ucfirst($get_pharmacy_details['pharmacy_name']);
		$data['page_heading'] = $page_heading;
		
		// Bread crumb
		$this->breadcrumbcomponent->add('<i class="ace-icon fa fa-home home-icon"></i> Home ', base_url().'pharmacies/pharmacies-list');	
		$this->breadcrumbcomponent->add(filter_string($page_heading), base_url().'users/users-list');					
		$data['breadcrum_data'] = $this->breadcrumbcomponent->output();
				
		$users_list = $this->users->get_user_list($pharmacy_id);
		$data['users_list'] = $users_list;
		
		$this->stencil->css('jquery.fancybox.css');
        $this->stencil->js('jquery.fancybox.js');
				
		$this->stencil->paint('users/users_listing',$data);
		
	}//end index()
	

	// Start - public function get_user_details()
	public function get_user_details($pharmacy_id ='', $user_id = ''){
				
		if($user_id){
			
			$data['get_pharmacy_details'] = $this->pharmacies->get_pharmacy_details($pharmacy_id);
			
			$data['get_user_details'] = $this->users->get_user_details($pharmacy_id,$user_id);
			$this->load->view('users/user_view_detail', $data);
		}

	} // End - public function get_user_details()

	public function user_types(){
		
		//set title
		$page_title = DEFAULT_TITLE;
		$this->stencil->title($page_title);	
		
		//Sets the Meta data
		$this->stencil->meta(array(
			'description' => DEFAULT_META_DESCRIPTION,
			'keywords' => DEFAULT_META_KEYWORDS,
			'meta_title' => DEFAULT_TITLE
		));
		
		// Page Heading
		$data['page_heading'] = 'Update User Types';
		
		// Bread crumb
		$this->breadcrumbcomponent->add('<i class="ace-icon fa fa-home home-icon"></i> Home ', base_url().'pharmacies/pharmacies-list');
		$this->breadcrumbcomponent->add(filter_string($page_heading), base_url().'users/user-types');	
		$data['breadcrum_data'] = $this->breadcrumbcomponent->output();
		
		$this->stencil->css('jquery.fancybox.css');
        $this->stencil->js('jquery.fancybox.js');
		
		// Get pharmacy responsibilities
		$data['pharmacy_user_responsibilities'] = $this->users->get_pharmacy_user_responsibilities($this->session->my_pharmacy_id);

		$this->stencil->paint('users/user_types',$data);
	}

	public function privilages($user_type_id='', $responsibility_id=''){

		//if($user_type_id == '') redirect(base_url());

		//set title
		$page_title = DEFAULT_TITLE;
		$this->stencil->title($page_title);	
		
		//Sets the Meta data
		$this->stencil->meta(array(
			'description' => DEFAULT_META_DESCRIPTION,
			'keywords' => DEFAULT_META_KEYWORDS,
			'meta_title' => DEFAULT_TITLE
		));
		
		// Page Heading
		$data['page_heading'] = 'Update User Type';
		
		// Bread crumb
		$this->breadcrumbcomponent->add('<i class="ace-icon fa fa-home home-icon"></i> Home ', base_url().'pharmacies/pharmacies-list');
		$this->breadcrumbcomponent->add(filter_string($page_heading), base_url().'users/user-types');	
		$data['breadcrum_data'] = $this->breadcrumbcomponent->output();
		
		$this->stencil->css('jquery.fancybox.css');
        $this->stencil->js('jquery.fancybox.js');

        // Get pharmacy responsibilities
		$data['pharmacy_user_responsibilities'] = $this->users->get_pharmacy_user_responsibilities($this->session->my_pharmacy_id);

        $data['user_type_id'] = $user_type_id;
        $data['responsibility_id'] = $responsibility_id;

		$this->stencil->paint('users/user_types',$data);

	}//end privilages()

	/*public function update_privilage(){
		
		$this->load->model('Common_mod','common');
		
		extract($this->input->post());
		
		$update_menu = $this->common->update_user_permissions($this->input->post());
		
		if($update_menu){

			//set title
			$page_title = DEFAULT_TITLE;
			$this->stencil->title($page_title);	
			
			//Sets the Meta data
			$this->stencil->meta(array(
				'description' => DEFAULT_META_DESCRIPTION,
				'keywords' => DEFAULT_META_KEYWORDS,
				'meta_title' => DEFAULT_TITLE
			));
			
			// Page Heading
			$data['page_heading'] = 'Update User Type';
			
			// Bread crumb
			$this->breadcrumbcomponent->add('<i class="ace-icon fa fa-home home-icon"></i> Home ', base_url().'pharmacies/pharmacies-list');
			$this->breadcrumbcomponent->add(filter_string($page_heading), base_url().'users/update_privilage');	
			$data['breadcrum_data'] = $this->breadcrumbcomponent->output();

			$data['user_type_id'] = $usertype_id;
			
		} // if($update_menu)
		
		if($usertype_id){

				$this->session->set_flashdata('ok_message', 'User type added successfully');
				redirect(base_url().'users/privilages/'.$usertype_id);
				
			} else if($add_new_drug && $drug_id != ''){

				$this->session->set_flashdata('ok_message', 'User type updated successfully');
				redirect(base_url().'users/update-privilage/'.$update);
				
			}else{

				$this->session->set_flashdata('err_message', 'Something went wrong, please try again later or contact system administrator');
				redirect(base_url().'users/privilages/'.$saved);
				
			}//end if($saved)
		
		//$this->stencil->paint('users/user_types',$data);

	} // End => privilages()*/

	// Start => Function 
	public function delete_user_type($usertype_id=''){
		
		if($usertype_id == '') redirect(base_url());

		$this->load->model('Common_mod','common');
		
		extract($this->input->post());
		
		$deleted = $this->users->delete_user_type($usertype_id);
		
		
		if($deleted){

				$this->session->set_flashdata('ok_message', 'Record deleted successfully');
				redirect(base_url().'users/user-types');
				
			} else{

				$this->session->set_flashdata('err_message', 'Something went wrong, please try again later or contact system administrator');
				redirect(base_url().'users/user-types');
				
		}//end if($deleted)
		
		
		//$this->stencil->paint('users/user_types',$data);

	} // End => privilages()

	// Start => function add_new_user_type()
	public function add_new_user_type(){

		//set title
		$page_title = DEFAULT_TITLE;
		$this->stencil->title($page_title);	
		
		//Sets the Meta data
		$this->stencil->meta(array(
			'description' => DEFAULT_META_DESCRIPTION,
			'keywords' => DEFAULT_META_KEYWORDS,
			'meta_title' => DEFAULT_TITLE
		));
		
		// Page Heading
		$data['page_heading'] = 'Add User Type';
		
		// Bread crumb
		$this->breadcrumbcomponent->add('<i class="ace-icon fa fa-home home-icon"></i> Home ', base_url().'pharmacies/pharmacies-list');
		$this->breadcrumbcomponent->add(filter_string($page_heading), base_url().'users/add-new-user-type');	
		$data['breadcrum_data'] = $this->breadcrumbcomponent->output();
		
		$this->stencil->css('jquery.fancybox.css');
        $this->stencil->js('jquery.fancybox.js');

        // Get pharmacy responsibilities
		// $data['pharmacy_user_responsibilities'] = $this->users->get_pharmacy_user_responsibilities($this->session->my_pharmacy_id);

		$this->stencil->paint('users/add_new_user_type',$data);

	} // End => function add_new_user_type()

	// Start => function add_user_type_process()
	public function add_user_type_process(){

		if(!$this->input->post()) redirect(base_url());

		extract($this->input->post());
		
		if($usertype_id !=''){
		
			$this->load->model('Common_mod','common');
			
			$update_menu = $this->common->update_user_permissions($this->input->post());
		
			$data['user_type_id'] = $usertype_id;
		
		} else {
		
			$saved = $this->users->add_user_type_process($this->input->post());
		}
	 
		if($saved){

			//set title
			$page_title = DEFAULT_TITLE;
			$this->stencil->title($page_title);	
			
			//Sets the Meta data
			$this->stencil->meta(array(
				'description' => DEFAULT_META_DESCRIPTION,
				'keywords' => DEFAULT_META_KEYWORDS,
				'meta_title' => DEFAULT_TITLE
			));
			
			// Page Heading
			$data['page_heading'] = 'Update User Type';
			
			// Bread crumb
			$this->breadcrumbcomponent->add('<i class="ace-icon fa fa-home home-icon"></i> Home ', base_url().'pharmacies/pharmacies-list');
			$this->breadcrumbcomponent->add(filter_string($page_heading), base_url().'users/add-new-user-type');	
			$data['breadcrum_data'] = $this->breadcrumbcomponent->output();

			$data['user_type_id'] = $saved;
			
		} // if($saved)
		
		if($saved){

			$this->session->set_flashdata('ok_message', 'Record added successfully');
			redirect(base_url().'users/privilages/'.$saved);
			
		} else if($usertype_id != ''){

			$responsibility_id = '/'.$responsibility_id;

			$this->session->set_flashdata('ok_message', 'Record updated successfully');
			redirect(base_url().'users/privilages/'.$usertype_id.$responsibility_id);
			
		}else{

			$this->session->set_flashdata('err_message', 'Something went wrong, please try again later or contact system administrator');
			redirect(base_url().'users/privilages/'.$saved);
			
		}//end if($saved)
		
		//$this->stencil->paint('users/user_types',$data);

	} // End => function add_user_type_process()
	
	public function unlock_user($pharmacy_id, $user_id){
		
		if(!$user_id && !$pharmacy_id) redirect(SURL);
		
		$unlock_user = $this->users->unlock_user($pharmacy_id, $user_id);
		
		if($unlock_user){

			$this->session->set_flashdata('ok_message', 'User is unlocked successfully.');
			redirect(base_url().'users/users-list/'.$pharmacy_id);
			
		}else{

			$this->session->set_flashdata('err_message', 'Something went wrong, please try again later or contact system administrator');
			redirect(base_url().'users/users-list/'.$pharmacy_id);
			
		}//end if($unlock_user)
		
	}//end unlock_user($user_id)
	
} // End => Ci => Class Users
