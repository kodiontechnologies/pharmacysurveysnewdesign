<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Settings extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		
		// Verify if the user is loggedin
		if(!$this->session->admin_id)
			redirect(SURL.'login');
		// if(!$this->session->admin_id)
		
		$this->load->model('login_mod','login');
		$this->load->model('common_mod','common');
		$this->load->model('Settings_mod','settings');

		
		$this->load->library('BreadcrumbComponent');
		
		//Sets the variable $header_contents to use the slice head (/views/slices/header_contents.php)
		$this->stencil->slice('header_contents');

		//Sets the variable $header_scripts to use the slice head (/views/slices/header_scripts.php)
		$this->stencil->slice('header_scripts');
		
		//Sets the variable $footer_contents to use the slice head (/views/slices/footer_contents.php)
		$this->stencil->slice('footer_contents');
		
		//Sets the variable $footer_scripts_modal to use the slice head (/views/slices/footer_scripts_modal.php)
		$this->stencil->slice('footer_scripts_modal');
		
		//Sets the variable $footer_scripts to use the slice head (/views/slices/footer_scripts.php)
		$this->stencil->slice('footer_scripts');

		// left_pane
		$this->stencil->slice('left_pane');
		
		// Layout
		$this->stencil->layout('page');
		
		// Form Validation	
		$this->stencil->js('kod_scripts/form_validation.js');
		$this->stencil->js('kod_scripts/form_validation/bootstrap_validator/dist/formValidation.min.js');
		$this->stencil->js('kod_scripts/form_validation/bootstrap_validator/dist/bootstrap.min.js');
		// end CMS file Validation

	}

	public function index(){
		
		//Page not on used at the moment
		redirect(SURL.'login');		
		
	} //end index()


	// Start - list_all_settings():
	public function edit_settings($settings_id = ''){
		
		//set title
		$page_title = DEFAULT_TITLE;
		$this->stencil->title($page_title);	
		
		//Sets the Meta data
		$this->stencil->meta(array(
			'description' => DEFAULT_DESCRIPTION,
			'meta_title' => DEFAULT_TITLE
		));
			
		// Set Bread Crumb		
		// Pull all categories from db
		
		if($settings_id != ''){ // If update action requested
			$data['settings'] = $this->settings->get_settings($settings_id);
			$data['form_action'] = 'update';
			
			// Bread crumb Update Settings
			
		} else {
			$data['form_action'] = 'add';
		}
		
		// Load view: settings
		$this->load->view('settings/edit_settings', $data);		
	} // End - list_all_settings():
	
	// Start - list_all_settings():
	public function list_all_settings($settings_id = ''){
		
		//set title
		$page_title = DEFAULT_TITLE;
		$this->stencil->title($page_title);	
		
		//Sets the Meta data
		$this->stencil->meta(array(
			'description' => DEFAULT_DESCRIPTION,
			'meta_title' => DEFAULT_TITLE
		));
			
		// Set Bread Crumb
		$this->breadcrumbcomponent->add('Add new Settings', base_url().'settings/list-all-settings');
		
		// Pull all categories from db
		$data['list_settings'] = $this->settings->get_all_settings();
		
		if($settings_id != ''){ // If update action requested
			$data['settings'] = $this->settings->get_settings($settings_id);
			$data['form_action'] = 'update';
			
			// Bread crumb Update Settings
			$this->breadcrumbcomponent->add('Update Settings', base_url().'settings/list-all-settings');
			
		} else {
			$data['form_action'] = 'add';
		}
		
		// Bread crumb output
		$data['breadcrum_data'] = $this->breadcrumbcomponent->output();
		
		
		$this->stencil->css('jquery.fancybox.css');
        $this->stencil->js('jquery.fancybox.js');
		
		// Load view: settings
		$this->stencil->paint('settings/list_all_settings', $data);
		
	} // End - list_all_settings():
	
	// Start - add_update_settings():
	public function add_update_settings(){
		
		
		$success = $this->settings->add_update_settings($this->input->post());
		$action = $this->input->post('action');
		if($success){
			if($action == 'add')
				$this->session->set_flashdata('ok_message', 'Settings has been successfully added.');
			else
				$this->session->set_flashdata('ok_message', 'Settings has been successfully updated.');
			redirect(SURL.'settings/list-all-settings');
		} else {
			$this->session->set_flashdata('err_message', 'Oops! Something went wrong.');
			redirect(SURL.'settings/list-all-settings');
		}
		
	} // End - add_update_settings():
	
	// Start setting_name_exists
	public function setting_name_exists(){
		
		$result = $this->settings->get_setting_name($this->input->post('key'));
		$response = array('exist' => $result);
		echo json_encode($response);
		
	} // End - setting_name_exists():
	
}/* End of file */
