<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Page extends CI_Controller {
	
	private $nav_tree;
	
	public function __construct()
	{
		parent::__construct();
		
		
		// Verify if the user is loggedin
		if(!$this->session->admin_id)
			redirect(SURL.'login');
		// if(!$this->session->admin_id)
		
		$this->load->model('login_mod','login');
		$this->load->model('common_mod','common');
		$this->load->model('page_mod','page');
		
		$this->load->library('BreadcrumbComponent');
		
		//Sets the variable $header_contents to use the slice head (/views/slices/header_contents.php)
		$this->stencil->slice('header_contents');

		//Sets the variable $header_scripts to use the slice head (/views/slices/header_scripts.php)
		$this->stencil->slice('header_scripts');
		
		//Sets the variable $footer_contents to use the slice head (/views/slices/footer_contents.php)
		$this->stencil->slice('footer_contents');
		
		//Sets the variable $footer_scripts_modal to use the slice head (/views/slices/footer_scripts_modal.php)
		$this->stencil->slice('footer_scripts_modal');
		
		//Sets the variable $footer_scripts to use the slice head (/views/slices/footer_scripts.php)
		$this->stencil->slice('footer_scripts');

		// left_pane
		$this->stencil->slice('left_pane');
		
		// Layout
		$this->stencil->layout('page');
		
		// Form Validation	
		$this->stencil->js('kod_scripts/form_validation.js');
		$this->stencil->js('kod_scripts/form_validation/bootstrap_validator/dist/formValidation.min.js');
		$this->stencil->js('kod_scripts/form_validation/bootstrap_validator/dist/bootstrap.min.js');
		// end CMS file Validation

  }

	public function index(){
		//Page not on used at the moment
		redirect(SURL.'login');		
		
	} //end index()
	
	// Start - public function add_new_page()
	public function add_new_page($page_id=''){
		
		// Update
		$get_page_details = $this->page->get_page_details($page_id);
			
		$data['get_page_details'] = $get_page_details;
		
		// Add New
		$this->stencil->layout('modal_layout');
		$this->stencil->paint('page/edit_page', $data);

	} // End - public function add_new_page()
	
	// Function add_new_page_process()
	public function add_new_page_process(){
		
		//If Post is not SET
		if(!$this->input->post() && !$this->input->post('new_page_btn')) redirect(base_url());
		
		 $add_new_page = $this->page->add_new_page($this->input->post()); 
		 
		 if($add_new_page){
			
			$page_id = $this->input->post('page_id');
			
			if($page_id == ''){
				
				$this->session->set_flashdata('ok_message', 'New Page added successfully.');
				redirect(SURL.'page/list-all-page');
				
			}else{

				$referrer_link  = $page_id.'?'.$this->input->post('tab_id').'=1';
				$this->session->set_flashdata('ok_message', 'Page updated successfully.');
				redirect(SURL.'page/list-all-page');
				
			}//end if($this->input->post('page_id') == '')
			
		}else{
			
			$referrer_link  = $page_id.'?'.$this->input->post('tab_id').'=1';
			$this->session->set_flashdata('err_message', 'Something went wrong, please try again later.');
			redirect(SURL.'page/list-all-page');

		}//end if($add_new_page)

	}//end add_new_page_process()
	
	// Function delete_page
	public function delete_page($page_id){
		
			if($page_id!="")
			{
				$get_page_delete = $this->page->delete_page_db($page_id);
				
				if($get_page_delete == '1')
				{
					
					$this->session->set_flashdata('ok_message', 'Page deleted successfully.');
					redirect(SURL.'page/list-all-page');
					
				} else {
					
					$this->session->set_flashdata('err_message', 'Something went wrong, please try again later.');
					redirect(SURL.'page/list-all-page');
					
				}//end if if($delete_page != '')
				
			}//end if($page_id!="")
			
	}//end function delete_page($page_id)
	
	//Function list_all cms pages
	public function list_all_page(){		
		
		//set title
		$page_title = DEFAULT_TITLE;
		$this->stencil->title($page_title);	
		
		//Sets the Meta data
		$this->stencil->meta(array(
			'description' => DEFAULT_DESCRIPTION,
			'meta_title' => DEFAULT_TITLE
		));
			
		// Set Bread Crumb
		$this->breadcrumbcomponent->add('CMS Page Listing', base_url().'page/list-all-page');
		// Bread crumb output
		$data['breadcrum_data'] = $this->breadcrumbcomponent->output();
		
		// Get all Pages
		$data['list_page'] = $this->page->get_all_pages();
		
		$this->stencil->css('jquery.fancybox.css');
        $this->stencil->js('jquery.fancybox.js');
		
		// Load view: list_all_pages
		$this->stencil->paint('page/list_all_page',$data);
		
	} // End - list_all_page():

}/* End of file */
