<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Surveys extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		// Verify if the user is loggedin
		if(!$this->session->admin_id)
			redirect(SURL.'login');
		// if(!$this->session->uid)
		
		$this->load->model('Pharmacies_mod','pharmacies');
		$this->load->model('Users_mod','users');
		$this->load->model('Surveys_mod','survey');
		$this->load->model('Common_mod','common');

		$this->load->library('BreadcrumbComponent');
		
		//Sets the variable $header_contents to use the slice head (/views/slices/header_contents.php)
		$this->stencil->slice('header_contents');

		//Sets the variable $header_scripts to use the slice head (/views/slices/header_scripts.php)
		$this->stencil->slice('header_scripts');
		
		//Sets the variable $footer_contents to use the slice head (/views/slices/footer_contents.php)
		$this->stencil->slice('footer_contents');
		
		//Sets the variable $footer_scripts_modal to use the slice head (/views/slices/footer_scripts_modal.php)
		$this->stencil->slice('footer_scripts_modal');
		
		//Sets the variable $footer_scripts to use the slice head (/views/slices/footer_scripts.php)
		$this->stencil->slice('footer_scripts');

		// left_pane
		$this->stencil->slice('left_pane');
	
		// Layout
		$this->stencil->layout('page');

	} // public function __construct()

	// Start => public function pharmacy_survey_status($pharmacy_id='')
	public function pharmacy_survey_status($pharmacy_id=''){

		if($pharmacy_id == '') exit;

		//set title
		$page_title = DEFAULT_TITLE;
		$this->stencil->title($page_title);	
		
		//Sets the Meta data
		$this->stencil->meta(array(
			'description' => DEFAULT_META_DESCRIPTION,
			'keywords' => DEFAULT_META_KEYWORDS,
			'meta_title' => DEFAULT_TITLE
		));
		
		$pharmacy_details = $this->pharmacies->get_pharmacy_details($pharmacy_id);

		// Page Heading
		$page_heading = 'Pharmacy "'.$pharmacy_details['pharmacy_name'].'" Invoices';
		$data['page_heading'] = $page_heading;
		
		// Bread crumb
		$this->breadcrumbcomponent->add('<i class="ace-icon fa fa-home home-icon"></i> Home ', base_url().'pharmacies/pharmacies-list');	
		$this->breadcrumbcomponent->add(filter_string($page_heading), base_url().'pharmacies/pharmacies-list');					
		$data['breadcrum_data'] = $this->breadcrumbcomponent->output();
		
		//Get Survey Payment Invoices
		$get_survey_payment_invoices = $this->survey->get_survey_payment_invoices($pharmacy_id);
		$data['survey_payment_invoices'] = $get_survey_payment_invoices;

		$data['pharmacy_id'] = $pharmacy_id;

		//Active Survey Detials
		$pharmacy_current_survey = $this->survey->get_pharmacy_current_survey($pharmacy_id);
		$data['pharmacy_current_survey'] = $pharmacy_current_survey;

		if(!$pharmacy_current_survey){

			//No Current Survey in Process	

			//Survey is not yet started means we have to start the survey of the current one.
			$current_date = date('m/d');	
			$survey_start_date = date('Y-m-d');
			$data['survey_start_date'] = $survey_start_date;
			
			$SURVEY_END_MONTH = 'SURVEY_END_MONTH';
			$survey_end_global_value = get_global_settings($SURVEY_END_MONTH); //Set from the Global Settings
			$next_survey_end_date = filter_string($survey_end_global_value['setting_value']);
			
			if(strtotime($current_date) > strtotime($next_survey_end_date)){
				
				$next_survey_end = strtotime("$next_survey_end_date +1 year");	
				$survey_session = date('Y').'-'.(date('Y')+1);
				$survey_end_date = date('Y-m-d', $next_survey_end).'-'.(date('Y', $next_survey_end)+1); 
				
			}else{
				
				$next_survey_end = strtotime("$next_survey_end_date +0 year");
				$survey_session = (date('Y', $next_survey_end)-1).' - '.date('Y', $next_survey_end);
				$survey_end_date = (date('Y-m-d', $next_survey_end)-1).'-'.date('Y', $next_survey_end);
			}//end if
			
			$data['next_survey_end'] = $next_survey_end;
			$data['survey_session'] = $survey_session;
			$data['survey_end_date'] = $survey_end_date;
			
		} // if(!$pharmacy_current_survey)

		$this->stencil->css('jquery.fancybox.css');
        $this->stencil->js('jquery.fancybox.js');

		$this->stencil->paint('pharmacies/pharmacy_surveys',$data);

	} // End => public function pharmacy_survey_status($pharmacy_id='')

	public function view_invoice($pharmacy_id, $invoice_id){

		$verify_if_valid_invoice = $this->survey->get_survey_payment_invoices($pharmacy_id,$invoice_id);
		
		if($verify_if_valid_invoice){

			$data['invoice_data'] = $verify_if_valid_invoice;
			
			$data['pharmacy_id'] = $pharmacy_id;

			$this->stencil->layout('modal_layout');
			$this->stencil->paint('pharmacies/view_invoice_details',$data);
			
		} else {
			echo 'Invalid Invoice'; exit;
		}
		
	}//end view_invoice($invoice_id)
	

	// Start =>public function add_new_survey($pharmacy_id)
	/*
	public function add_new_survey($pharmacy_id){

		$current_survey_date = date('m/d');

		$SURVEY_END_MONTH = 'SURVEY_END_MONTH';
		$survey_end_global_value = get_global_settings($SURVEY_END_MONTH); //Set from the Global Settings

		$next_survey_end_date = filter_string($survey_end_global_value['setting_value']);

		if(strtotime($current_survey_date) > strtotime($next_survey_end_date)){
			
			$next_survey_end = strtotime("$next_survey_end_date +1 year"); 
			$survey_session = date('Y').'-'.(date('y')+1).'-survey';
    		$survey_end_date = date('Y-m-d', $next_survey_end).'-'.(date('y', $next_survey_end)+1);

		} else {
			
			$next_survey_end = strtotime("$next_survey_end_date +0 year");
			// Survey 2016-2017
			$survey_session = 'Survey '.(date('Y', $next_survey_end)-1).'-'.date('y', $next_survey_end);
    		$survey_end_date = (date('Y-m-d', $next_survey_end)-1).'-'.date('y', $next_survey_end);

		} //end if

		$expiry_date = date('Y-m-d', $next_survey_end);
		$survey_year = date('Y', $next_survey_end);

		$survey_ref_no = $this->generate_survey_reference_no();

		$created_date = date('Y-m-d H:i:s');
		$created_by_ip = $this->input->ip_address();

		$ins_data = array(

			'pharmacy_id' => $this->db->escape_str(trim($pharmacy_id)),
			'survey_ref_no' => $this->db->escape_str(trim($survey_ref_no)),
			'survey_title' => $this->db->escape_str(trim($survey_session)),
			'survey_year' => $this->db->escape_str(trim($survey_year)),
			'survey_finish_date' => $this->db->escape_str(trim($expiry_date)),
			'survey_status' => $this->db->escape_str(trim('0')),
			'created_date' => $this->db->escape_str(trim($created_date)),
			'created_by_ip' => $this->db->escape_str(trim($created_by_ip))
		);

		//Inserting rota requested leaves data into the database. 
		$this->db->dbprefix('pharmacy_survey');
		$this->db->insert('pharmacy_survey', $ins_data);

		return $this->db->insert_id();

	} // End =>public function add_new_survey($pharmacy_id)
	*/
	// Start => public function buy_pharmacy_survey()
	public function buy_pharmacy_survey(){

		if(!$this->input->post()) exit;

		extract($this->input->post());

		// Insert Survey
		$survey_id = $this->survey->add_new_survey($pharmacy_id);


		if($survey_id){
		
			$VAT_PERCENTAGE = 'VAT_PERCENTAGE';
			$vat_percentage = get_global_settings($VAT_PERCENTAGE); //Set from the Global Settings
		
			$vat_amount = (trim($vat_percentage['setting_value']) / 100) * $survey_price;
			$vat_amount = filter_price($vat_amount);
			
			$grand_total = $survey_price + $vat_amount;
		
			$survey_session = $this->survey->get_pharmacy_survey_list($pharmacy_id, '', $survey_id);
		
			// Insert Invoice
			$invoice_data['subtotal'] = $survey_price;
			$invoice_data['vat_tax'] = $vat_amount;
			$invoice_data['vat_percentage'] = trim($vat_percentage['setting_value']);
			$invoice_data['survey_session'] = $survey_session['survey_title'];
			$invoice_data['grand_total'] = $grand_total;
			$invoice_data['payment_method'] = 'ADMIN';
			$invoice_data['transaction_id'] = $grand_total;
			
			// Send Pharmacy Registration Email
			
			// Get price from GLOBAL SETTINGS
			$FROM_BUSINESS_NAME = 'FROM_BUSINESS_NAME';
			$from_business_name = get_global_settings($FROM_BUSINESS_NAME); //Set from the Global Settings
			$from_business_name = filter_string($from_business_name['setting_value']);
			
			// Get price from GLOBAL SETTINGS
			$FROM_ADDRESS = 'FROM_ADDRESS';
			$from_address = get_global_settings($FROM_ADDRESS); //Set from the Global Settings
			$from_address = filter_string($from_address['setting_value']);
			
			// Get price from GLOBAL SETTINGS
			$FROM_ADDRESS_2 = 'FROM_ADDRESS_2';
			$from_address_2 = get_global_settings($FROM_ADDRESS_2); //Set from the Global Settings
			$from_address_2 = filter_string($from_address_2['setting_value']);
			
			// Get price from GLOBAL SETTINGS
			$FROM_COUNTY = 'FROM_COUNTY';
			$from_county = get_global_settings($FROM_COUNTY); //Set from the Global Settings
			$from_county = filter_string($from_county['setting_value']);
			
			// Get price from GLOBAL SETTINGS
			$FROM_POSTCODE = 'FROM_POSTCODE';
			$from_postcode = get_global_settings($FROM_POSTCODE); //Set from the Global Settings
			$from_postcode = filter_string($from_postcode['setting_value']);
			
			// Get price from GLOBAL SETTINGS
			$FROM_TOWN = 'FROM_TOWN';
			$from_town = get_global_settings($FROM_TOWN); //Set from the Global Settings
			$from_town = filter_string($from_town['setting_value']);
			
			// Get price from GLOBAL SETTINGS
			$FROM_EMAIL = 'FROM_EMAIL';
			$from_email = get_global_settings($FROM_EMAIL); //Set from the Global Settings
			$from_email = filter_string($from_email['setting_value']);
			
			// Get price from GLOBAL SETTINGS
			$FROM_WEBSITE = 'FROM_WEBSITE';
			$from_website = get_global_settings($FROM_WEBSITE); //Set from the Global Settings
			$from_website = filter_string($from_website['setting_value']);
			
			// Get price from GLOBAL SETTINGS
			$FROM_PHONE = 'FROM_PHONE';
			$from_phone = get_global_settings($FROM_PHONE); //Set from the Global Settings
			$from_phone = filter_string($from_phone['setting_value']);
			
			$get_pharmacy_details = $this->pharmacies->get_pharmacy_details($pharmacy_id);
			
			// Insert invoice : along with new fields //
			$invoice_data['pharmacy_name'] = filter_string($get_pharmacy_details['pharmacy_name']);
			$invoice_data['pharmacy_owner_name'] = filter_string($get_pharmacy_details['pharmacy_name']);
			$invoice_data['pharmacy_address'] = filter_string($get_pharmacy_details['address']);
			$invoice_data['pharmacy_address_2'] = filter_string($get_pharmacy_details['address_2']);
			$invoice_data['pharmacy_town'] = filter_string($get_pharmacy_details['town']);
			$invoice_data['pharmacy_county'] = filter_string($get_pharmacy_details['county']);
			$invoice_data['pharmacy_postcode'] = filter_string($get_pharmacy_details['postcode']);
			$invoice_data['from_name'] = $from_business_name;
			$invoice_data['from_address'] = $from_address;
			$invoice_data['from_address_2'] = $from_address_2;
			$invoice_data['from_town'] = $from_town;
			$invoice_data['from_county'] = $from_county;
			$invoice_data['from_postcode'] = $from_postcode;
			$invoice_data['from_phone'] = $from_phone;
			$invoice_data['from_email'] = $from_email;
			$invoice_data['from_website'] = $from_website;
			
			$invoice_generated = $this->survey->save_invoice($pharmacy_id, $survey_id, $invoice_data, $survey_price);
			
		}//end if($survey_id)
					
		redirect(SURL.'pharmacies/pharmacies-list');

	} // End => public function buy_pharmacy_survey()
	
	public function download_invoice($pharmacy_id, $invoice_id){
			
			$verify_if_valid_invoice = $this->survey->get_survey_payment_invoices($pharmacy_id,$invoice_id);
			
			if($verify_if_valid_invoice){
	
				$data['invoice_data'] = $verify_if_valid_invoice;
				
				$this->stencil->layout('modal_layout');
				$html = $this->load->view('pharmacies/pdf/download_invoice', $data, true);
				
				//echo $html; exit;
				
				$this->load->library('pdf');
				$pdf = $this->pdf->load();
				
				$file_name = 'invoice-'.$verify_if_valid_invoice['invoice_no'].'.pdf';
	
				$pdf->SetHTMLFooter('<table width="100%" style="vertical-align: bottom; font-family: Arial, Helvetica, sans-serif; font-size: 9pt; color: #000000;"><tr>
	<td width="33%"><span>Copyright '.$this->session->pharmacy_name.'</span></td>
	<td width="33%" align="center"></td>
	<td width="33%" style="text-align: right; ">Page {PAGENO} of {nbpg}</td>
	</tr></table>'); // Add a footer for good measure
	
				$pdf->AddPageByArray(array(
							'orientation' => 'P',
							'mgl' => '20',
							'mgr' => '20',
							'mgt' => '10',
							'mgb' => '10',
							'mgh' => '0',
							'mgf' => '0', 
						)
					);
				//header('Content-Type: application/pdf');
				//$pdf->AddPage('P'); // L - P
				$pdf->WriteHTML($html); // write the HTML into the PDF
				$pdf->Output($file_name,'I'); // save to file because we can
				
			}else{
				echo 'Invalid Invoice'; exit;
			}
			
		}//end view_invoice($invoice_id)	
} // End => Ci => Class Users
