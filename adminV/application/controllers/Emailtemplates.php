<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Emailtemplates extends CI_Controller {
	
	private $nav_tree;
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('login_mod','login');
		$this->load->model('common_mod','common');
		$this->load->model('template_mod','template');
		
		// Verify if the user is loggedin
		if(!$this->session->admin_id)
			redirect(SURL.'login');
		// if(!$this->session->admin_id)
		
		$this->load->model('Users_mod','users');
		$this->load->model('Pharmacies_mod','pharmacies');
		$this->load->model('Drugs_mod','drugs');
		
		$this->load->library('BreadcrumbComponent');
		
		//Sets the variable $header_contents to use the slice head (/views/slices/header_contents.php)
		$this->stencil->slice('header_contents');

		//Sets the variable $header_scripts to use the slice head (/views/slices/header_scripts.php)
		$this->stencil->slice('header_scripts');
		
		//Sets the variable $footer_contents to use the slice head (/views/slices/footer_contents.php)
		$this->stencil->slice('footer_contents');
		
		//Sets the variable $footer_scripts_modal to use the slice head (/views/slices/footer_scripts_modal.php)
		$this->stencil->slice('footer_scripts_modal');
		
		//Sets the variable $footer_scripts to use the slice head (/views/slices/footer_scripts.php)
		$this->stencil->slice('footer_scripts');

		// left_pane
		$this->stencil->slice('left_pane');
		
		// Layout
		$this->stencil->layout('page');
		
		// Form Validation	
		$this->stencil->js('kod_scripts/form_validation.js');
		$this->stencil->js('kod_scripts/form_validation/bootstrap_validator/dist/formValidation.min.js');
		$this->stencil->js('kod_scripts/form_validation/bootstrap_validator/dist/bootstrap.min.js');
		// end CMS file Validation
	
	} // End  public function __construct()

	public function index(){
		//Page not on used at the moment
		redirect(SURL.'login');		
		
	} //end index()
	
	
	// Start - public function add_new_template()
	public function add_new_template($template_id=''){
		
		// Update
		$get_template_details = $this->template->get_template_details($template_id);
			
		$data['get_template_details'] = $get_template_details;
		
		// Add New
		$this->stencil->layout('modal_layout');
		$this->stencil->paint('emailtemplates/edit_template', $data);

	} // End - public function add_new_template()
	
	public function add_new_template_process(){
		
		//If Post is not SET
		if(!$this->input->post() && !$this->input->post('new_template_btn')) redirect(base_url());
		
		// Add New Template Call add_new_template function from template_mod model
		 $add_new_template = $this->template->add_new_template($this->input->post()); 
		
		if($add_new_template){
			
			$template_id = $this->input->post('template_id');
			
			if($template_id == ''){
				
				$this->session->set_flashdata('ok_message', 'New Email Template added successfully.');
				redirect(SURL.'emailtemplates/list-all-templates');
				
			}else{

				$referrer_link  = $template_id.'?'.$this->input->post('tab_id').'=1';
				$this->session->set_flashdata('ok_message', 'Email Template updated successfully.');
				redirect(SURL.'emailtemplates/list-all-templates');
				
			}//end if($this->input->post('template_id') == '')
			
		}else{
			
			$referrer_link  = $template_id.'?'.$this->input->post('tab_id').'=1';
			$this->session->set_flashdata('err_message', 'Something went wrong, please try again later.');
			redirect(SURL.'emailtemplates/add-new-template/'.$referrer_link);

		}//end if($add_new_template)
	
	}//end add_new_template_process()
	
	public function delete_template($template_id){
		
			if($template_id!="")
			{
				// Delete Template from database call template_mod
				$get_template_delete = $this->template->delete_template($template_id);
				
				if($get_template_delete == '1')
				{
					$this->session->set_flashdata('ok_message', 'Email Template deleted successfully.');
					redirect(SURL.'emailtemplates/list-all-templates');
					
				} else {
					
					$this->session->set_flashdata('err_message', 'Something went wrong, please try again later.');
					redirect(SURL.'emailtemplates/list-all-templates');
					
				}//end if if($get_template_delete != '')
				
			}//end if($template_id!="")
			
	}//end function delete_template($template_id)
	
	// list_all Email Templates
	public function list_all_templates(){		
		
		//set title
		$page_title = DEFAULT_TITLE;
		$this->stencil->title($page_title);	
		
		//Sets the Meta data
		$this->stencil->meta(array(
			'description' => DEFAULT_DESCRIPTION,
			'meta_title' => DEFAULT_TITLE
		));
		
	  
     	// Set Bread Crumb
		$this->breadcrumbcomponent->add('Templates Listing', base_url().'emailtemplates/list-all-templates');
		
		// Get all Pages
		$data['list_templates'] = $this->template->get_all_templates($template_id);
		
		// Bread crumb output
		$data['breadcrum_data'] = $this->breadcrumbcomponent->output();
		
		$this->stencil->css('jquery.fancybox.css');
        $this->stencil->js('jquery.fancybox.js');
		
		// Load view: list_all_templates
		$this->stencil->paint('emailtemplates/list_all_templates',$data);
		
	} // End - list_all_template():
	
} /* End of file */
