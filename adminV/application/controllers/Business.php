<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Business extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		// Verify if the user is loggedin
		if(!$this->session->admin_id)
			redirect(SURL.'login');
		// if(!$this->session->uid)
		
		$this->load->model('Business_mod','business');
		$this->load->model('Users_mod','users');
		$this->load->model('Pharmacies_mod','pharmacies');


		$this->load->library('BreadcrumbComponent');
		
		//Sets the variable $header_contents to use the slice head (/views/slices/header_contents.php)
		$this->stencil->slice('header_contents');

		//Sets the variable $header_scripts to use the slice head (/views/slices/header_scripts.php)
		$this->stencil->slice('header_scripts');
		
		//Sets the variable $footer_contents to use the slice head (/views/slices/footer_contents.php)
		$this->stencil->slice('footer_contents');
		
		//Sets the variable $footer_scripts_modal to use the slice head (/views/slices/footer_scripts_modal.php)
		$this->stencil->slice('footer_scripts_modal');
		
		//Sets the variable $footer_scripts to use the slice head (/views/slices/footer_scripts.php)
		$this->stencil->slice('footer_scripts');

		// left_pane
		$this->stencil->slice('left_pane');
	
		// Layout
		$this->stencil->layout('page');
		
		// Form Validation	
		$this->stencil->js('kod_scripts/form_validation.js');
		$this->stencil->js('kod_scripts/form_validation/bootstrap_validator/dist/formValidation.min.js');
		$this->stencil->js('kod_scripts/form_validation/bootstrap_validator/dist/bootstrap.min.js');
		// end CMS file Validation

	} // public function __construct()

	public function index(){

		//set title
		$page_title = DEFAULT_TITLE;
		$this->stencil->title($page_title);	
		
		//Sets the Meta data
		$this->stencil->meta(array(
			'description' => DEFAULT_META_DESCRIPTION,
			'keywords' => DEFAULT_META_KEYWORDS,
			'meta_title' => DEFAULT_TITLE
		));
		
		// Page Heading
		$page_heading = 'Business Listing';
		$data['page_heading'] = $page_heading;
		
		// Bread crumb
		$this->breadcrumbcomponent->add('<i class="ace-icon fa fa-home home-icon"></i> Home ', base_url().'pharmacies/pharmacies-list');	
		$this->breadcrumbcomponent->add(filter_string($page_heading), base_url().'pharmacies/pharmacies-list');					
		$data['breadcrum_data'] = $this->breadcrumbcomponent->output();
		
		$business_list = $this->business->get_business_list();
		$data['business_list'] = $business_list;
		
		$this->stencil->css('jquery.fancybox.css');
        $this->stencil->js('jquery.fancybox.js');
				
		$this->stencil->paint('business/business_listing',$data);
		
	}//end index()
		
	// Business add 
	public function add_new_business_process(){
		
	//If Post is not SET
	if(!$this->input->post() && !$this->input->post('new_pharmacy_btn')) redirect(base_url());
		
	    extract($this->input->post());

		$this->form_validation->set_rules('business_type', 'Business Type', 'trim|required');
		$this->form_validation->set_rules('business_name', 'Business Name', 'trim|required');
		$this->form_validation->set_rules('contact_name', 'Contact Name', 'trim|required');
		$this->form_validation->set_rules('contact_no', 'Business Phone Number', 'trim|required');
		$this->form_validation->set_rules('email_address', 'Email', 'trim|required|valid_email');
		if($business_id ==''){
			$this->form_validation->set_rules('password', 'Password', 'trim|required');
		}

		if($this->form_validation->run() == FALSE){

			// session set form data in fields
		    $this->session->set_flashdata($this->input->post());
			// PHP Error 
			$this->session->set_flashdata('err_message', validation_errors());
			redirect(SURL.'business');

		} else {			
							
				 // add update pharmacies
				 $add_update_business = $this->business->register_business($this->input->post()); 
				// if add or update succuess
				if($add_update_business){
					
					if($business_id==''){
						
					$this->session->set_flashdata('ok_message', 'Business added successfully.');
					redirect(SURL.'business');
					
				  } else {
					
					$this->session->set_flashdata('ok_message', 'Business updated successfully.');
					redirect(SURL.'business');
				  }
					
				}else{
					
					$this->session->set_flashdata('err_message', 'Something went wrong, please try again later.');
					redirect(SURL.'business');
				}
		
		}// end else
	} // end public function add_new_business_process()
	
	
	// Edit Business
	public function edit_business($business_id){
		
		$business_details = $this->business->get_business_list($business_id);
		
		//print_this($pharmacies_details); exit;
		$data['business_details'] = $business_details;
		$this->stencil->layout('modal_layout');
		
		$this->stencil->paint('business/edit_business',$data);
		
	}//end function edit_pharmacy($pharmacy_id)

	
	
	// Function delete_business
	public function delete_business($business_id=''){
		
			if($business_id!="")
			{
				// Delete Business
				$business_success = $this->business->delete_business($business_id);
				
				if($business_success == '1')
				{
					
					$this->session->set_flashdata('ok_message', 'Business deleted successfully.');
					redirect(SURL.'business');
					
				} else {
					
					$this->session->set_flashdata('err_message', 'Something went wrong, please try again later.');
					redirect(SURL.'business');
					
				}//end if if($business_success != '')
				
			}//end if($business_id!="")
			
	}//end function delete_business($business_id='')
	
} // End => Ci => Class Users
