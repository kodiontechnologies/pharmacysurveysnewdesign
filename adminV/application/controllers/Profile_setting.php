<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile_setting extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		
			
		// Verify if the user is loggedin
		if(!$this->session->admin_id)
			redirect(SURL.'login');
		// if(!$this->session->admin_id)
		
		$this->load->model('login_mod','login');
		$this->load->model('Settings_mod','settings');	
		
		$this->load->library('BreadcrumbComponent');
		
		//Sets the variable $header_contents to use the slice head (/views/slices/header_contents.php)
		$this->stencil->slice('header_contents');

		//Sets the variable $header_scripts to use the slice head (/views/slices/header_scripts.php)
		$this->stencil->slice('header_scripts');
		
		//Sets the variable $footer_contents to use the slice head (/views/slices/footer_contents.php)
		$this->stencil->slice('footer_contents');
		
		//Sets the variable $footer_scripts_modal to use the slice head (/views/slices/footer_scripts_modal.php)
		$this->stencil->slice('footer_scripts_modal');
		
		//Sets the variable $footer_scripts to use the slice head (/views/slices/footer_scripts.php)
		$this->stencil->slice('footer_scripts');

		// left_pane
		$this->stencil->slice('left_pane');
		
		// Layout
		$this->stencil->layout('page');
		
		// Form Validation	
	    $this->stencil->js('kod_scripts/form_validation.js');
		$this->stencil->js('kod_scripts/form_validation/bootstrap_validator/dist/formValidation.min.js');
		$this->stencil->js('kod_scripts/form_validation/bootstrap_validator/dist/bootstrap.min.js');
		// end CMS file Validation
	}

	public function index(){
		
		//Page not on used at the moment
		redirect(SURL.'login');		
		
	} //end index()
	
	//Function change_password(): Change password view mode
	public function change_password(){
		
		//Login Check
		if($this->session->admin_id == 1)
		 	$this->login->verify_is_user_login();
			
		// Set Bread Crumb
		$this->breadcrumbcomponent->add('Admin Change Password', base_url().'profile-setting/change-password');

		//set title
		$page_title = DEFAULT_TITLE;
		$this->stencil->title($page_title);	
		
		//Sets the Meta data
		$this->stencil->meta(array(
			'description' => DEFAULT_DESCRIPTION,
			'meta_title' => DEFAULT_TITLE
		));
		
				
		// Bread crumb output
		$data['breadcrum_data'] = $this->breadcrumbcomponent->output();
		
		$this->stencil->paint('settings/change_password',$data);
		
	} //end forgot_password()


	//Function change_password_process(): Change Pasword Process
	public function change_password_process(){

		//Login Check
		if($this->session->admin_id == 1)
		 	$this->login->verify_is_user_login();

		//If Post is not SET
		if(!$this->input->post() && !$this->input->post('change_pass_btn')) redirect(base_url());		
		
		
		if($this->input->post('new_password')!=$this->input->post('confirm_password')){
			
			$this->session->set_flashdata('err_message', 'Password does not match!');
			redirect(SURL.'profile-setting/change-password');
			exit;
			
		}
		
		// Chnage Password function login mode call to change password
		$is_password_changed = $this->settings->change_password($this->input->post(),$this->session->userdata('admin_id'));

		if($is_password_changed){
			
			$this->session->set_flashdata('ok_message', 'Password successfully updated. Thank you!');
			redirect(SURL.'profile-setting/change-password');
			
		}else{

			$this->session->set_flashdata('err_message', 'Change Password Failed, Please Try Again!');
			redirect(SURL.'profile-setting/change-password');
			
		}//end if($chk_isvalid_user) 
		
	}//end public function change_password_process()
		
	// Start  edit_profile
	public function edit_profile () {
		//Login Check
		if($this->session->admin_id == 1)
		 	$this->login->verify_is_user_login();

		
		$data['admin_edit_profile'] = $this->settings->edit_admin_profile($this->session->userdata('admin_id'));
		
		// Set Bread Crumb
		$this->breadcrumbcomponent->add('Admin Edit Profile', base_url().'profile-setting/edit-profile');
	
		if(!empty($data['admin_edit_profile']))
		{
		  $this->session->set_userdata($data['admin_edit_profile']);
		}
		//set title
		$page_title = DEFAULT_TITLE;
		$this->stencil->title($page_title);	
		//$chk_isvalid_user = $this->login->validate_credentials($this->input->post('email_address'),$this->input->post('password'));
		//Sets the Meta data
		$this->stencil->meta(array(
			'description' => DEFAULT_DESCRIPTION,
			'meta_title' => DEFAULT_TITLE
		));
		
	
		// Bread crumb output
		$data['breadcrum_data'] = $this->breadcrumbcomponent->output();
		
		$this->stencil->paint('settings/edit_profile',$data);
		
	}// end edit-profile
	
	// Function edit_profile_process
	public function edit_profile_process() {
		
		//Login Check
		if($this->session->admin_id == 1)
		 	$this->login->verify_is_user_login();
	
	    $is_profile_changed = $this->settings->update_profile_admin($this->session->userdata('admin_id'),$this->input->post());
		
		if($is_profile_changed){
				$this->session->set_flashdata('ok_message', 'Profile has been successfully updated.');
			    redirect(SURL.'profile-setting/edit-profile');
		} else {
			    $this->session->set_flashdata('err_message', 'Oops! Something went wrong.');
			    redirect(SURL.'profile-setting/edit-profile');
		}
	}// end edit_profile_process

}/* End of file */
