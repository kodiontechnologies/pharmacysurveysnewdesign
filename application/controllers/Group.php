<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Group extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('Group_mod', 'group');
		$this->load->model('Pharmacy_mod', 'pharmacy');
		$this->load->model('Cms_mod', 'cms');
		$this->load->model('Survey_mod', 'survey');

		//Sets the variable $header_contents to use the slice head (/views/slices/header_contents.php)
		$this->stencil->slice('header_contents');

		//Sets the variable $header_scripts to use the slice head (/views/slices/header_scripts.php)
		$this->stencil->slice('header_scripts');
		
		//Sets the variable $footer_contents to use the slice head (/views/slices/footer_contents.php)
		$this->stencil->slice('footer_contents');
		
		//Sets the variable $footer_scripts to use the slice head (/views/slices/footer_scripts.php)
		$this->stencil->slice('footer_scripts');

		$this->stencil->js('kod_scripts/form_validation.js');
		$this->stencil->js('kod_scripts/form_validation/bootstrap_validator/dist/formValidation.min.js');
		$this->stencil->js('kod_scripts/form_validation/bootstrap_validator/dist/bootstrap.min.js');

	}

	public function index(){
		
		redirect(SURL);
		exit;
		
	}
	public function login(){

		if($this->session->g_id){ 
			redirect(base_url().'group/dashboard');
		}

		$cms_page = $this->cms->get_cms_page('login');
		$data['cms_page'] = $cms_page;

		if(filter_string($cms_page['page_title'])){
			
			//set title
			$page_title = $cms_page['meta_title'];
			$this->stencil->title($page_title);	
			
			//Sets the Meta data
			$this->stencil->meta(array(
				'description' => $cms_page['meta_description'],
				'keywords' => $cms_page['meta_keywords']
			));

		}else{
			
			//set title
			$page_title = DEFAULT_TITLE;
			$this->stencil->title($page_title);	
			
			//Sets the Meta data
			$this->stencil->meta(array(
				'description' => DEFAULT_META_DESCRIPTION,
				'keywords' => DEFAULT_META_KEYWORDS
			));

		}//end if(filter_string($homepage_cms['page_title']))


		// Page Heading
		$page_heading = 'Login ::Group Login';
		$data['page_heading'] = $page_heading;
		
		$this->stencil->css('captcha.css');
		
		// Get CMS contents from DB for Login Page
		$data['cms_page'] = $this->cms->get_cms_page('login');

		$this->stencil->layout('page');
		$this->stencil->paint('group/login', $data);
		
	} //end index()
	
	//Function login_process(): Process and authenticate the login form
	public function login_process(){
		
		extract($this->input->post());
		
		// If is a valid request
		if(!$this->input->post('email_address')) redirect(SURL);
		
		// Captcha Validation
		if($this->input->post('g-recaptcha-response') == ''){
			
			$this->session->set_flashdata($this->input->post());
			$this->session->set_flashdata('err_message', 'Please verify Captcha');
			redirect(base_url().'group/login');

		} //end if($this->input->post('g-recaptcha-response') == '')
			
				// Captcha Validation
		if(strtolower($this->input->post('g-recaptcha-response')) != $this->session->random_number){
			
			$this->session->set_flashdata($this->input->post());
			$this->session->set_flashdata('err_message', 'Invalid captcha. Please try again.');
			redirect(base_url().'group/login');

		} //end if($this->input->post('g-recaptcha-response') == '')

		// PHP Validation
		$this->form_validation->set_rules('email_address', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');

		if($this->form_validation->run() == FALSE){
			
			$this->session->set_flashdata($this->input->post());
			// PHP Error
			$this->session->set_flashdata('err_message', validation_errors());
			redirect(base_url().'group/login');

		} else {
			
			$verify_business_credentials = $this->group->verify_business_credentials($email_address,$password);
			
			if($verify_business_credentials){
				// Set Session
				$this->session->set_userdata($verify_business_credentials);
				redirect(base_url().'group/dashboard');
					
			}else{
				$this->session->set_flashdata('err_message', 'Invalid email or password. Please try again.');
				redirect(base_url().'group/login');
				
			}//end if($verify_account_activation)
			
		}//end if($this->form_validation->run() == FALSE)
		
	}//end public function login_process()
	
	public function forgot_password(){
	
		//CMS DATA
		$cms_page = $this->cms->get_cms_page('forgot-password');
		$data['cms_page'] = $cms_page;

		if(filter_string($cms_page['page_title'])){
			
			//set title
			$page_title = $cms_page['meta_title'];
			$this->stencil->title($page_title);	
			
			//Sets the Meta data
			$this->stencil->meta(array(
				'description' => $cms_page['meta_description'],
				'keywords' => $cms_page['meta_keywords']
			));

		}else{
			
			//set title
			$page_title = DEFAULT_TITLE;
			$this->stencil->title($page_title);	
			
			//Sets the Meta data
			$this->stencil->meta(array(
				'description' => DEFAULT_META_DESCRIPTION,
				'keywords' => DEFAULT_META_KEYWORDS
			));

		}//end if(filter_string($homepage_cms['page_title']))


		// Page Heading
		$page_heading = 'Login ::Pro CDR';
		$data['page_heading'] = $page_heading;
		
		$this->stencil->css('captcha.css');

		$this->stencil->layout('page');
		$this->stencil->paint('group/forgot_password',$data);
		
	} //end forgot_password()

	//Function forgot_password_process(): Process and authenticate the forgot password
	public function forgot_password_process(){
		
		//If Post is not SET
		if(!$this->input->post() && !$this->input->post('fgetpass_btn')) redirect(base_url());

		// Captcha Validation
		if($this->input->post('g-recaptcha-response') == ''){
			 
			$this->session->set_flashdata('err_message', 'Please verify Captcha');
			redirect(base_url().'group/forgot-password');

		}//end if($this->input->post('g-recaptcha-response') == '')
		
		
		// Captcha Validation
		if(strtolower($this->input->post('g-recaptcha-response')) != $this->session->random_number){
			
			$this->session->set_flashdata($this->input->post());
			$this->session->set_flashdata('err_message', 'Invalid captcha. Please try again.');
			redirect(base_url().'group/forgot-password');

		} //end if($this->input->post('g-recaptcha-response') == '')

		
		// PHP Validation
		$this->form_validation->set_rules('username', 'Username', 'trim|required');
		
		if($this->form_validation->run() == FALSE){
			 
			// PHP Error 
			$this->session->set_flashdata('err_message', validation_errors());
			redirect(base_url().'group/forgot-password');
			
		} else {

			$user_exist_arr = $this->group->get_user_by_name_pincode($this->input->post('username'));
			
			if($user_exist_arr){
			
				//Sending Email to the User
				$send_new_password = $this->group->send_new_password($user_exist_arr);
			
				if($send_new_password){
					$this->session->set_flashdata('ok_message', 'A link has been sent to your email address. Please open the link to set a new password.');
					redirect(base_url().'login/forgot-password');
				
				}else{
				
					$this->session->set_flashdata('err_message', 'Something went wrong, please try again later.');
					redirect(base_url().'login/forgot-password');
			
			} // End - if($send_new_password)
			
		  }else{

			$this->session->set_flashdata('err_message', 'The Email Address or Password do not exist in your database. Please try again, with the correct Email and Password');
			redirect(base_url().'login/forgot-password');
			
		}//end if($chk_isvalid_user) 
		
	 } // Else  
		
	}//end forgot_password_process
	
	//Function reset_password(): Reset Password Form
	public function reset_password($reset_code){
		
		$verify_reset_code = $this->group->verify_reset_code($reset_code);
		
		if(!$verify_reset_code){

			$this->session->set_flashdata('err_message', 'The reset link is either invalid or have expired. Please try again.');
			redirect(base_url().'group/forgot-password');
			
		}//end if(!$verify_reset_code)

		//CMS DATA
		//$cms_data_arr = $this->cms->get_cms_page('forgot-password');
		
		//set title
		$page_title = DEFAULT_TITLE;
		$this->stencil->title($page_title);	
		
		//Sets the Meta data
		$this->stencil->meta(array(
			'description' => DEFAULT_META_DESCRIPTION,
			'keywords' => DEFAULT_META_KEYWORDS,
			'meta_title' => DEFAULT_TITLE
		));

		// Page Heading
		$page_heading = 'Set new Password ';
		$data['page_heading'] = $page_heading;
		
		$this->stencil->css('captcha.css');

		$data['reset_code'] = $reset_code;

		// Get CMS contents from DB for Forgot Password
		$data['cms_page'] = $this->cms->get_cms_page('reset-password');

		$this->stencil->layout('page');
		$this->stencil->paint('group/reset_password',$data);

	}//end reset_password($reset_code)
	
	//Function reset_password_process()
	public function reset_password_process(){

		// Extract POST
		extract($this->input->post());
		
		//If Post is not SET
		if(!$this->input->post() && !$this->input->post('group_reset_pass_btn') && !$verify_code) redirect(base_url());

		// Captcha Validation
		if($this->input->post('g-recaptcha-response') == ''){
			 
			$this->session->set_flashdata('err_message', 'Please verify Captcha');
			redirect(base_url().'group/reset-password/'.$verify_code);

		}//end if($this->input->post('g-recaptcha-response') == '')
		
		
		// Captcha Validation
		if(strtolower($this->input->post('g-recaptcha-response')) != $this->session->random_number){
			
			$this->session->set_flashdata($this->input->post());
			$this->session->set_flashdata('err_message', 'Invalid captcha. Please try again.');
			redirect(base_url().'group/reset-password/'.$verify_code);

		} //end if($this->input->post('g-recaptcha-response') == '')

		// PHP Validation
		$this->form_validation->set_rules('new_password', 'New Password', 'trim|required');
		$this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required');
		
		if($this->form_validation->run() == FALSE){
			 
			// PHP Error 
			$this->session->set_flashdata('err_message', validation_errors());
			redirect(base_url().'group/reset-password/'.$verify_code);
			
		} else {
			
			$decode = base64_decode(urldecode($verify_code));
			$split_decode = explode('|',$decode);
			
			$reset_password = $this->group->reset_password($split_decode[1], $this->input->post());

			if($reset_password){
			
				$this->session->set_flashdata('ok_message', 'Your password is successfully updated, You may login to your account with the updated credentials');
				redirect(base_url().'group/login');

			} else {
					
				$this->session->set_flashdata('err_message', 'Something went wrong, please try again later.');
				redirect(base_url().'group/reset-password/'.$verify_code);
			
			} // End - if($send_new_password)

		} // end if($this->form_validation->run() == FALSE)
		
	} // End reset_password_process()
	
	public function dashboard(){

		if(!$this->session->g_id) 
			redirect(base_url());

		//set title
		$page_title = DEFAULT_TITLE;
		$this->stencil->title($page_title);	
		
		//Sets the Meta data
		$this->stencil->meta(array(
			'description' => DEFAULT_META_DESCRIPTION,
			'keywords' => DEFAULT_META_KEYWORDS,
			'meta_title' => DEFAULT_TITLE
		));
		
		// Page Heading
		$data['page_heading'] = 'Dashboard';

		//Fancybox files
		$this->stencil->css('jquery.fancybox.css');
        $this->stencil->js('jquery.fancybox.js');

		// Get pharmacy profile
		$group_pharmacy_list = $this->pharmacy->get_group_pharmacy_list($this->session->g_id);
		$data['group_pharmacy_list'] = $group_pharmacy_list;
		
		$business_profile = $this->group->get_group_details($this->session->g_id);
		$data['business_profile'] = $business_profile;

		// Layout
		$this->stencil->layout('page');
		$this->stencil->paint('group/dashboard',$data);
		
	}//end dashboard()
	
	// Start => public function update_group_profile()
	public function update_group_profile(){

		if(!$this->session->g_id) 
			redirect(base_url());

		// Verify if the POST is set
		if(!$this->input->post()) exit;
		
		extract($this->input->post());
		
		$verify_pharmacy_exist = $this->group->verify_if_group_already_exist($email_address,$this->session->g_id);

			if($verify_pharmacy_exist){

				// session set form data in fields
				$this->session->set_flashdata($this->input->post());
				$this->session->set_flashdata('err_message', 'The email you have entered already exist, please try another one.');
				redirect(base_url().'group/dashboard?t=2');
				
			} else{

				$updated = $this->group->update_group_profile($this->input->post(), $this->session->g_id);
				if($updated){

					$this->session->set_flashdata('ok_message', "Profile is updated successfully.");	
					
				} else {
					$this->session->set_flashdata('err_message', 'Oops! Something went wrong, please try again later.');
				} // if($updated)
		
			}

		// Redirect user to Dashboard
		redirect(base_url().'group/dashboard?t=2');

	} // End => public function update_group_profile()

	// Start => public function create_new_password()
	public function create_new_password(){


		if(!$this->session->g_id) 
			redirect(base_url());

		// Verify if the POST is set
		if(!$this->input->post()) exit;

		$updated = $this->group->create_new_password($this->input->post(), $this->session->g_id);
		if($updated){
			$this->session->set_flashdata('ok_message', "New password is successfully updated.");
		} else {
			$this->session->set_flashdata('err_message', 'Invalid Current Password.');
		} // if($updated)

		// Redirect user to Dashboard
		redirect(base_url().'group/dashboard?t=3');

	} // End => public function create_new_password()
	
	public function logout(){

		//Distroy All Sessions
		$this->session->sess_destroy();

		$this->session->set_flashdata('ok_message', 'You have successfully logged out.');
		redirect(base_url());

	} //end index()
	
	public function get_captcha(){

		$string = '';
		
		for($i = 0; $i < 5; $i++){
			$string .= chr(rand(97, 122));
		} // for($i = 0; $i < 5; $i++)
		
		$this->session->random_number = $string;
		
		$dir = './assets/captcha/fonts/';
		
		$image = imagecreatetruecolor(165, 50);
		
		// random number 1 or 2
		$num = rand(1,2);
		if($num==1)
		{
			$font = "Capture it 2.ttf"; // font style
		}
		else
		{
			$font = "Molot.otf";// font style
		}
		
		// random number 1 or 2
		$num2 = rand(1,2);
		if($num2==1)
		{
			$color = imagecolorallocate($image, 113, 193, 217);// color
		}
		else
		{
			$color = imagecolorallocate($image, 163, 197, 82);// color
		}
		
		$white = imagecolorallocate($image, 255, 255, 255); // background color white
		imagefilledrectangle($image,0,0,399,99,$white);
		
		imagettftext ($image, 30, 0, 10, 40, $color,$dir.$font, $this->session->random_number);
		
		//header("Content-type: image/png");

		//$im = imagecreatefrompng($_POST['image']); 
		imagepng($image);
				
	} //end index()
	
} /* End of file */
