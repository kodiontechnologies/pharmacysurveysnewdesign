<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		
		// Verify if the user is loggedin
		if(!$this->session->p_id)
			redirect(SURL.'login');
		// if(!$this->session->uid)

		$this->load->model('Survey_mod', 'survey');
		$this->load->model('Common_mod', 'common');
		$this->load->model('Register_mod', 'register');
		$this->load->model('Email_mod', 'email_template');
		$this->load->model('Pharmacy_mod', 'pharmacy');

		$this->load->model('Cms_mod', 'cms');

		// paypal username
		$PAYPAL_API_USERNAME = 'PAYPAL_API_USERNAME';
		$paypal_api_username = get_global_settings($PAYPAL_API_USERNAME); //Set from the Global Settings
		$paypal_api_username = filter_string($paypal_api_username['setting_value']);

		// paypal ai signatures
		$PAYPAL_API_SIGNATURE = 'PAYPAL_API_SIGNATURE';
		$paypal_api_signature = get_global_settings($PAYPAL_API_SIGNATURE); //Set from the Global Settings
		$paypal_api_signature = filter_string($paypal_api_signature['setting_value']);

		// paypal password
		$PAYPAL_API_PASSWORD = 'PAYPAL_API_PASSWORD';
		$paypal_api_password = get_global_settings($PAYPAL_API_PASSWORD); //Set from the Global Settings
		$paypal_api_password = filter_string($paypal_api_password['setting_value']);

		// paypal sandbox status
		$PAYPAL_SANDBOX_STATUS = 'PAYPAL_SANDBOX_STATUS';
		$paypal_sandbox_status = get_global_settings($PAYPAL_SANDBOX_STATUS); //Set from the Global Settings
		$paypal_sandbox_status = filter_string($paypal_sandbox_status['setting_value']);

		// Google API Short URL Key
		$GOOGLE_API_SHORT_URL_KEY = 'GOOGLE_API_SHORT_URL_KEY';
		$google_short_url_key = get_global_settings($GOOGLE_API_SHORT_URL_KEY); //Set from the Global Settings
		$google_short_url_key = filter_string($google_short_url_key['setting_value']);
		
		$this->google_short_url_key = $google_short_url_key;

		// Applying Paypal Settings
		$this->paypal_details = array(
			// you can get this from your Paypal account, or from your
			// test accounts in Sandbox
			'API_username' => $paypal_api_username, 
			'API_signature' => $paypal_api_signature, 
			'API_password' => $paypal_api_password,
			'sandbox_status' => ($paypal_sandbox_status == 0) ? false : true
			// Paypal_ec defaults sandbox status to true
			// Change to false if you want to go live and
			// update the API credentials above
			// 'sandbox_status' => false,
		);
		
		$this->load->library('paypal_ec', $this->paypal_details);
		
		$this->load->library('BreadcrumbComponent');
		
		//Sets the variable $header_contents to use the slice head (/views/slices/header_contents.php)
		$this->stencil->slice('header_contents');

		//Sets the variable $header_scripts to use the slice head (/views/slices/header_scripts.php)
		$this->stencil->slice('header_scripts');
		
		//Sets the variable $footer_contents to use the slice head (/views/slices/footer_contents.php)
		$this->stencil->slice('footer_contents');
		
		//Sets the variable $footer_scripts to use the slice head (/views/slices/footer_scripts.php)
		$this->stencil->slice('footer_scripts');

		$this->stencil->js('kod_scripts/form_validation.js');
		$this->stencil->js('kod_scripts/form_validation/bootstrap_validator/dist/formValidation.min.js');
		$this->stencil->js('kod_scripts/form_validation/bootstrap_validator/dist/bootstrap.min.js');

	} // public function __construct()

	public function index(){
		
		$get_help_videos_list = $this->common->get_help_videos_list();
		$data['video_list'] = $get_help_videos_list;

		//Active Survey Detials
		$pharmacy_current_survey = $this->survey->get_pharmacy_current_survey($this->session->p_id);
		//print_this($pharmacy_current_survey); 		exit;
		$data['pharmacy_current_survey'] = $pharmacy_current_survey;
		
		if(!$pharmacy_current_survey){
			
			//No Current Survey in Process	
			
			//06Apr2017: Not checking if the most recent survey is properly started and closed? If not then immediately close it and ask for payment

			 $get_most_recent_survey = get_most_recent_survey($this->session->p_id);
			 $data['most_recent_survey'] = $get_most_recent_survey;
			 
			 if($get_most_recent_survey){
				 //We have most recent survey
				 
				 if($get_most_recent_survey['survey_start_date'] == NULL && ($get_most_recent_survey['survey_finish_date'] < date('Y-m-d')) && $get_most_recent_survey['survey_status'] == '0'){

					 //The most recent survey is not yet started and is expired and status is 0
					 $force_survey_to_close = $this->survey->force_survey_to_close($this->session->p_id, $get_most_recent_survey['id']);
					 
					 if($force_survey_to_close)
					 	redirect(SURL.'dashboard');
					 
				 }//end if
				 
			 }//end if($get_most_recent_survey)
			 
			//Survey is not yet started means we have to start the survey of the current one.
			$current_date = date('m/d');	
			$survey_start_date = date('Y-m-d');
			$data['survey_start_date'] = $survey_start_date;
			
			$SURVEY_END_MONTH = 'SURVEY_END_MONTH';
			$survey_end_global_value = get_global_settings($SURVEY_END_MONTH); //Set from the Global Settings
			$next_survey_end_date = filter_string($survey_end_global_value['setting_value']);
			
			if(strtotime($current_date) > strtotime($next_survey_end_date)){
				
				$next_survey_end = strtotime("$next_survey_end_date +1 year");	
				$survey_session = date('Y').'-'.(date('y')+1);
				$survey_end_date = date('Y-m-d', $next_survey_end).'-'.(date('Y', $next_survey_end)+1); 
				
			}else{
				
				$next_survey_end = strtotime("$next_survey_end_date +0 year");
				$survey_session = (date('Y', $next_survey_end)-1).'-'.date('y', $next_survey_end);
				$survey_end_date = (date('Y-m-d', $next_survey_end)-1).'-'.date('y', $next_survey_end);
			}//end if
			
			$data['next_survey_end'] = $next_survey_end;
			$data['survey_session'] = $survey_session;
			$data['survey_end_date'] = $survey_end_date;
			
		} else {
		
			$total_submitted_surveys = $this->survey->get_survey_submitted_count($this->session->p_id, $pharmacy_current_survey['id']);
			$data['total_submitted_surveys'] = $total_submitted_surveys;
			
			$total_submitted_online_surveys = $this->survey->get_survey_submitted_count($this->session->p_id, $pharmacy_current_survey['id'],'ONLINE');
			$data['total_submitted_online_surveys'] = $total_submitted_online_surveys;
			
			$total_submitted_paper_surveys = $this->survey->get_survey_submitted_count($this->session->p_id, $pharmacy_current_survey['id'],'PAPER');
			$data['total_submitted_paper_surveys'] = $total_submitted_paper_surveys;

			$email_to_friend_email_body_arr = $this->email_template->get_email_template(3);
			$email_body = strip_tags($email_to_friend_email_body_arr['email_body']);
			
			$search_arr = array('[PHARMACY_NAME]','[SURVEY_LINK]');
			$replace_arr = array($this->session->pharmacy_name,SURL.'online-survey/'.$this->session->p_id.'/'.$pharmacy_current_survey['survey_ref_no']);
			$email_body = str_replace($search_arr,$replace_arr,$email_body);
			$data['email_body'] = $email_body;
			
			if(!$pharmacy_current_survey['embed_url']){
				
				$embed_link = SURL.'online-survey/'.$this->session->p_id.'/'.$pharmacy_current_survey['survey_ref_no'].'/embed';

				$this->load->library('GoogleUrlApi');
				$googer = new GoogleURLAPI($this->google_short_url_key);
				$short_url = $googer->shorten($embed_link);
				
				if($short_url != ''){
					
					//Update into th e Survey Table
					$update_embed_url = $this->survey->update_embed_url($pharmacy_current_survey['id'],$short_url);
					
					if($update_embed_url)
						$p_embed_link  = $short_url;
						
				}//end if($short_url != '')
				
			}else
				$p_embed_link = filter_string($pharmacy_current_survey['embed_url']);	
			
			$data['embed_link'] = $p_embed_link;
			
		}//end if(!$pharmacy_current_survey)

		//Get Survey Payment Invoices
		$get_survey_payment_invoices = $this->survey->get_survey_payment_invoices($this->session->p_id);
		$data['survey_payment_invoices'] = $get_survey_payment_invoices;
		
		//Get Array of Questions
		$get_questionnnair_arr = $this->survey->get_questionnaire_list($this->session->pharmacy_type);
		
		//print_this($get_questionnnair_arr); exit;
		
		//print_this($get_questionnnair_arr); exit;
		$data['questionnnair_arr'] = $get_questionnnair_arr;

		//CMS DATA
		$cms_page = $this->cms->get_cms_page('dashboard');
		$data['cms_page'] = $cms_page;

		if(filter_string($cms_page['page_title'])){
			
			//set title
			$page_title = $cms_page['meta_title'];
			$this->stencil->title($page_title);	
			
			//Sets the Meta data
			$this->stencil->meta(array(
				'description' => $cms_page['meta_description'],
				'keywords' => $cms_page['meta_keywords']
			));

		}else{
			
			//set title
			$page_title = DEFAULT_TITLE;
			$this->stencil->title($page_title);	
			
			//Sets the Meta data
			$this->stencil->meta(array(
				'description' => DEFAULT_META_DESCRIPTION,
				'keywords' => DEFAULT_META_KEYWORDS
			));

		}//end if(filter_string($homepage_cms['page_title']))
		
		// Page Heading
		$data['page_heading'] = 'Dashboard';

		// Get pharmacy profile
		$pharmacy_profile = $this->pharmacy->get_pharmacy_details($this->session->p_id);
		$data['pharmacy_profile'] = $pharmacy_profile;

		//Fancybox files
		$this->stencil->css('jquery.fancybox.css');
        $this->stencil->js('jquery.fancybox.js');
		
		$this->stencil->css('built.css');
		
		$this->stencil->js('kod_scripts/survey_scripts.js');
		$this->stencil->js('kod_scripts/dashboard.js');

		$SMS_BODY = 'SMS_BODY';
		$sms_body = get_global_settings($SMS_BODY);
		$sms_body_global_settings = filter_string($sms_body['setting_value']);

		// Replace veriables with data
		// [PHARMACY_NAME] ([POSTCODE])
		// [URL_LINK]
		// 7799923165

		// http://demotd.com/surveys/online-survey/2/CPMBW52

		$sms_body_global_settings = str_replace('[PHARMACY_NAME]', $this->session->pharmacy_name, $sms_body_global_settings);
		$sms_body_global_settings = str_replace('[POSTCODE]', $this->session->postcode, $sms_body_global_settings);

		//$this->load->library('GoogleUrlApi');
		
		//$googer = new GoogleURLAPI($this->google_short_url_key);
		$long_url = SURL.'online-survey/'.$this->session->p_id.'/'.$pharmacy_current_survey['survey_ref_no'];
		//$short_url = $googer->shorten($long_url);
		
		$sms_body_global_settings = str_replace('[URL_LINK]', $long_url, $sms_body_global_settings);

		$data['sms_body_global_settings'] = $sms_body_global_settings;
		
		// Get price from GLOBAL SETTINGS
		$PHARMACY_SURVEY_PRICE = 'PHARMACY_SURVEY_PRICE';
		$pharmacy_survey_price = get_global_settings($PHARMACY_SURVEY_PRICE); //Set from the Global Settings
		$pharmacy_survey_price = filter_string($pharmacy_survey_price['setting_value']);
		
		$price = $pharmacy_survey_price;
		$sub_total = $price;

		// Get VAT % Settings
		$VAT_PERCENTAGE = 'VAT_PERCENTAGE';
		$vat_percentage = get_global_settings($VAT_PERCENTAGE); //Set from the Global Settings

		$vat_amount = (trim($vat_percentage['setting_value']) / 100) * $sub_total;
		$vat_amount = filter_price($vat_amount);

		$data['pharmacy_survey_price'] = $pharmacy_survey_price;
		$data['sub_total'] = $sub_total;
		$data['vat'] = trim($vat_percentage['setting_value']);
		$data['vat_amount'] = $vat_amount;
		$data['grand_total'] = $sub_total + $vat_amount;

		// Layout
		$this->stencil->layout('page');
		$this->stencil->paint('dashboard/dashboard',$data);
		
	}//end index()

	public function start_survey(){
		
		if(!$this->input->post('radio_no_of_survey')) redirect(SURL);
		
		extract($this->input->post());
		
		//Active Survey Detials
		$pharmacy_survey_valid =  $this->survey->get_pharmacy_survey_list($this->session->p_id, '', $survey_id);
		
		if($pharmacy_survey_valid && $pharmacy_survey_valid['survey_start_date'] == NULL){
			//Its a valid Survey
			$start_survey = $this->survey->start_pharmacy_survey($this->session->p_id,$this->input->post());
			
			if($start_survey)
				$this->session->set_flashdata('ok_message', $pharmacy_survey_valid['survey_title']. " is successfully started");	
			else
				$this->session->set_flashdata('err_message', 'Oops! Something went wrong, please try again later.');

		}else{
			
			$this->session->set_flashdata('err_message', 'Oops! Something went wrong, please try again later.');
		}//end if(!$pharmacy_current_survey)

		redirect(SURL.'dashboard');
		
	}//end start_survey()
	
	public function submit_paper_survey(){

		if(!$this->input->post('survey_id')) redirect(SURL);
		
		extract($this->input->post());
		
		if($survey_id == '' || $pharmacy_id == '') redirect(SURL);
		
		$pharmacy_current_survey = $this->survey->get_pharmacy_current_survey($pharmacy_id, $survey_id);

		if($pharmacy_current_survey){

			//Survey is Active and is Started!
			$submit_survey = $this->survey->submit_survey($this->session->p_id, $this->input->post());
			
			if($submit_survey)
				$this->session->set_flashdata('ok_message', "You have successfully transfered the responses, mark the paper survey form as transfered");
			else
				$this->session->set_flashdata('err_message', 'Oops! Something went wrong, please try again later.');
			//end if($submit_survey)	
			
			if($is_incomplete)	
				redirect(SURL.'dashboard?t=7');
			
			else
				redirect(SURL.'dashboard?t=2');				
			
		}else{
			redirect(SURL.'dashboard');
		}//end if($pharmacy_current_survey)
	
	}//end submit_paper_survey()
	
	public function submit_paper_incomplete_survey(){

		if(!$this->input->post('survey_id')) redirect(SURL);
		
		extract($this->input->post());
		
		if($survey_id == '' || $pharmacy_id == '') redirect(SURL);
		
		$pharmacy_survey_valid =  $this->survey->get_pharmacy_survey_list($this->session->p_id, '', $survey_id);

		if($pharmacy_survey_valid && $pharmacy_survey_valid['survey_status'] == '0' && $pharmacy_survey_valid['survey_start_date'] != NULL){
			
			//Survey is Valid, not closed and have started.
			$submit_survey = $this->survey->submit_survey($this->session->p_id, $this->input->post());
			
			if($submit_survey)
				$this->session->set_flashdata('ok_message', "You have successfully transfered the responses, mark the paper survey form as transfered");
			else
				$this->session->set_flashdata('err_message', 'Oops! Something went wrong, please try again later.');
			//end if($submit_survey)	
			
			redirect(SURL.'dashboard?t=7');
			
		}else{
			redirect(SURL.'dashboard');
		}//end if($pharmacy_current_survey)
	
	}//end submit_paper_incomplete_survey()
	
	public function send_survey_link_process(){
		
		if(!$this->input->post('survey_email_btn')) redirect(SURL);
		
		extract($this->input->post());

		$send_email_to_friend = $this->survey->email_survey_link($this->session->p_id, $this->input->post());
		
		if($send_email_to_friend){

			$this->session->set_flashdata('ok_message', "Survey link sent successfully to the email address.");
			redirect(SURL.'dashboard?t=1');
			
		}//end if($send_email_to_friend)

	}//end send_survey_link_process()
	
	public function survey_chart_container(){
		
		extract($this->input->post());
		
		$survey_split = explode('|',$survey_id);
		
		$get_pharmacy_survey_details = $this->survey->get_pharmacy_survey_list($this->session->p_id,'',$survey_split[0]);
		$data['pharmacy_survey_details'] = $get_pharmacy_survey_details;
		
		if($get_pharmacy_survey_details){
			
			$response_arr['survey_title'] = $get_pharmacy_survey_details['survey_title'];
			
			//Get Array of Questions
			$get_questionnnair_arr = $this->survey->get_questionnaire_list($this->session->pharmacy_type);
			$data['questionnnair_arr'] = $get_questionnnair_arr;
			
			$get_survey_question_stats = $this->survey->get_survey_question_stats($this->session->p_id, $survey_split[0], $survey_split[1],'',$this->session->pharmacy_type);
			//print_this($get_survey_question_stats); exit;
			
			$data['survey_question_stats'] = $get_survey_question_stats;
			
			$closing_comments_arr = array_column($get_survey_question_stats, 'closing_question_comments');
			$data['closing_comments_arr'] = $closing_comments_arr;

			$this->stencil->layout('ajax_layout');
			$response_data = $this->load->view('dashboard/survey_chart_container', $data,true);
			
			$response_arr['response_data'] = $response_data;
			
			$response_arr['survey_status'] = $get_pharmacy_survey_details['survey_status'];
			
			$response_arr['total_survey_comments'] = count($closing_comments_arr);
			
			$pharmacy_current_survey = $this->survey->get_pharmacy_current_survey($this->session->p_id);

			//Total Survey Submitted
			$total_submitted_surveys = $this->survey->get_survey_submitted_count($this->session->p_id, $survey_split[0]);

			$survey_statistics['min_no_of_surveys'] = $get_pharmacy_survey_details['min_no_of_surveys'];

			$remaining_surveys = filter_string($get_pharmacy_survey_details['min_no_of_surveys']) - $total_submitted_surveys;
			$survey_statistics['remaining_surveys'] = ($remaining_surveys < 0) ? 0 : $remaining_surveys;

			$total_submitted_online_surveys = $this->survey->get_survey_submitted_count($this->session->p_id, $survey_split[0],'ONLINE');
			$survey_statistics['total_submitted_online_surveys'] = $total_submitted_online_surveys;
			
			$total_submitted_paper_surveys = $this->survey->get_survey_submitted_count($this->session->p_id, $survey_split[0],'PAPER');
			$survey_statistics['total_submitted_paper_surveys'] = $total_submitted_paper_surveys;
			
			$survey_statistics['total_survey_completed'] = $total_submitted_online_surveys + $total_submitted_paper_surveys;
			
			//echo $pharmacy_current_survey['id'] .'=='. $get_pharmacy_survey_details['id'];
			
			//$response_arr['show_survey_features'] = ($pharmacy_current_survey['id'] == $get_pharmacy_survey_details['id']) ? 1 : 0;
			
			$response_arr['show_survey_features'] = (($get_pharmacy_survey_details['survey_status'] == '0' && $total_submitted_surveys >= $get_pharmacy_survey_details['min_no_of_surveys']) ||  $get_pharmacy_survey_details['survey_status'] > 0) ? 1 : 0;
			
			//print_this($get_pharmacy_survey_details);
			//echo $response_arr['show_survey_features'];
			//exit;
			
			$response_arr['survey_stats'] = $survey_statistics;
			
			$response_arr['survey_id'] = $get_pharmacy_survey_details['id'];
			
			echo json_encode($response_arr);

		}//end if($get_pharmacy_survey_details)
		
		
	}//end survey_chart_container()
	
	public function close_survey(){

		extract($this->input->post());
		
		$get_pharmacy_survey_details = $this->survey->get_pharmacy_survey_list($this->session->p_id,'',$close_survey_id);
		
		if($get_pharmacy_survey_details){
			
			$survey_session = $get_pharmacy_survey_details['survey_title'];
			
			$change_survey_status = $this->survey->change_survey_status($this->session->p_id, $close_survey_id, '1');
			
			if($change_survey_status){
				$this->session->set_flashdata('ok_message', "$survey_session is successfully closed.");		
			}else{
				$this->session->set_flashdata('err_message', 'Oops! Something went wrong, please try again later.');
			}//if($change_survey_status)
			
			redirect(SURL.'dashboard?t=3');
			
		}else{
			redirect(SURL);
		}//end if($get_pharmacy_survey_details)
		
	}//end close_survey()
	
	/*
	public function finalize_survey(){

		extract($this->input->post());
		
		$get_pharmacy_survey_details = $this->survey->get_pharmacy_survey_list($this->session->p_id,'',$finalize_survey_id);
		
		if($get_pharmacy_survey_details){
			
			$survey_session = $get_pharmacy_survey_details['survey_title'];
			
			//Verify if the survey to be finalize have completed all survey comments

			$get_survey_question_stats = $this->survey->get_survey_question_stats($this->session->p_id, $finalize_survey_id, $get_pharmacy_survey_details['survey_ref_no']);
			$closing_comments_arr = array_column($get_survey_question_stats, 'closing_question_comments'); //Content array of closing comments

			if(count($closing_comments_arr) == 28){
				
				$change_survey_status = $this->survey->change_survey_status($this->session->p_id, $finalize_survey_id, '2');
				
				if($change_survey_status)
					$this->session->set_flashdata('ok_message', "$survey_session is successfully finalized.");		
				else
					$this->session->set_flashdata('err_message', 'Oops! Something went wrong, please try again later.');
				
			}else
				$this->session->set_flashdata('err_message', 'Survey cannot be finalized. You still do not have comments for all the questions.');
			
			redirect(SURL.'survey');
			
		}else{
			redirect(SURL);
		}//end if($get_pharmacy_survey_details)
		
	}//end finalize_survey()
	*/
	public function finish_survey(){

		extract($this->input->post());
		
		$get_pharmacy_survey_details = $this->survey->get_pharmacy_survey_list($this->session->p_id,'',$finish_survey_id);
		
		if($get_pharmacy_survey_details){
			
			$survey_session = $get_pharmacy_survey_details['survey_title'];
			
			$change_survey_status = $this->survey->change_survey_status($this->session->p_id, $finish_survey_id, '3', $this->input->post() );
			
			if($change_survey_status)
				$this->session->set_flashdata('ok_message', "$survey_session is successfully completed.");		
			else
				$this->session->set_flashdata('err_message', 'Oops! Something went wrong, please try again later.');
			
			redirect(SURL.'dashboard?t=3');
			
		}else{
			redirect(SURL);
		}//end if($get_pharmacy_survey_details)
		
	}//end close_survey()
	
	//Called while ajax call
	public function get_incomplete_survey(){
		
		extract($this->input->post());
		
		$pharmacy_survey_valid =  $this->survey->get_pharmacy_survey_list($this->session->p_id, '', $survey_id);
		
		$data['success'] = 1;
		if($pharmacy_survey_valid){
			
			$data['survey_data'] = $pharmacy_survey_valid;

			//Total Survey Submitted
			$total_submitted_surveys = $this->survey->get_survey_submitted_count($this->session->p_id, $survey_id);
			$data['total_submitted_surveys'] = $total_submitted_surveys;
			
		}else{
			$data['success'] = '0';
		}//end if($pharmacy_survey_valid)
		
		echo json_encode($data);
		
	}//end get_incomplete_survey()
	
	public function update_min_survey_required(){
		
		extract($this->input->post());

		$pharmacy_survey_valid =  $this->survey->get_pharmacy_survey_list($this->session->p_id, '', $survey_id);
		
		$data['success'] = 0;
		
		if($pharmacy_survey_valid){
			
			$update_min_survey_required = $this->survey->update_min_survey_required($this->session->p_id, $survey_id, $min_survey_required);
			
			if($update_min_survey_required)
				$data['success'] = 1;	
			
		}//end if($pharmacy_survey_valid)
		
		echo json_encode($data);
		
	}//end update_min_survey_required()
	
	
	public function survey_non_commented(){

		extract($this->input->post());
		
		$get_pharmacy_survey_details = $this->survey->get_pharmacy_survey_list($this->session->p_id,'',$survey_id);
		
		if($get_pharmacy_survey_details){
			
			$get_survey_comments_arr = $this->survey->get_survey_question_stats($this->session->p_id, $survey_id,$get_pharmacy_survey_details['survey_ref_no'],'',$this->session->pharmacy_type);
			
			//print_this($get_survey_comments_arr); 
			
			$missing_reviewed = array();
			
			foreach($get_survey_comments_arr as $parent_question_id => $get_survey_comments){
				
				//echo $get_survey_comments['parent_id']; exit;
				if($get_survey_comments['parent_id'] == 0){
					
					if(!$get_survey_comments['children']){
						
						//Those which do not have any children
						if(!$get_survey_comments['closing_question_comments'])
							$missing_reviewed[] = $get_survey_comments['question_code'];
						
					}else{

						//have children.
						foreach($get_survey_comments['children'] as $sub_question_id => $sub_question_arr){
							
							if(!$sub_question_arr['closing_question_comments'])
								$missing_reviewed[] = $sub_question_arr['question_code'];
								
						}//end foreach($get_survey_comments['children'] as $question_id => $sub_question_arr)
						
					}//end if(!$get_survey_comments['children'])
					
				}//end if($get_survey_comments['parent_id'] == 0)
				
			}//end foreach($get_survey_comments_arr as $parent_question_id => $get_survey_comments)
			
			//print_this($missing_reviewed); exit;
			
			$this->stencil->layout('ajax_layout');

			$data['missing_reviewed'] = $missing_reviewed;
			$data['well_performed'] = $well_performed;
			$data['bad_performed'] = $bad_performed;
			
			if($bad_performed == 1 && $well_performed == 1 && count($missing_reviewed) == 0){
				$response_data['success'] = 1;
				$response_data['message'] = '';
				
			}else{
				
				$response_data['success'] = 0;
				$response_data['message'] = $this->load->view('dashboard/survey_close_message', $data,true);
				
			}//end if($bad_performed == 1 && $well_performed == 1 && count($missing_comment) == 0)
			

			echo json_encode($response_data);
			exit;
			
		}//end if($get_pharmacy_survey_details)
		
	}//end survey_non_commented()
	
	public function save_survey_questions_comments(){
		
		extract($this->input->post());
		
		$get_pharmacy_survey_details = $this->survey->get_pharmacy_survey_list($this->session->p_id,'',$survey_id);
		
		if($get_pharmacy_survey_details){
			
			$save_survey_question_comments = $this->survey->save_survey_question_comments($this->session->p_id, $survey_id, $question_id, $comments);
			
			if($save_survey_question_comments){

				$get_survey_question_stats = $this->survey->get_survey_question_stats($this->session->p_id, $survey_id, $get_pharmacy_survey_details['survey_ref_no'],'',$this->session->pharmacy_type);
				$closing_comments_arr = array_column($get_survey_question_stats, 'closing_question_comments'); //Content array of closing comments
				
				$result = array('success' => 1,'total_comments' => count($closing_comments_arr));
			}else
				$result = array('success' => 0,'total_comments' => 0);
			
		}else
			$result = array('success' => 0,'total_comments' => 0);
		//end if($get_pharmacy_survey_details)
		
		echo json_encode($result);
		
	}//end save_survey_questions_comments()
	
	public function download_survey_form($survey_id){
		
		set_time_limit(0);
		
		if($survey_id != ''){
			
			$pharmacy_survey_details = $this->survey->get_pharmacy_survey_list($this->session->p_id,'',$survey_id);
			
			if($pharmacy_survey_details){
				//Suirvey is valid 
				
				$data['pharmacy_survey_details'] = $pharmacy_survey_details;
				
				if($pharmacy_survey_details['survey_status'] == '3'){
					
					//Survey is completed go ahead.
					$get_submitted_surveys_list = $this->survey->get_submitted_surveys_list($this->session->p_id, $survey_id);
					$data['submitted_surveys_list'] = $get_submitted_surveys_list;

					$get_questionnnair_arr = $this->survey->get_questionnaire_list($this->session->pharmacy_type);
					$data['questionnnair_arr'] = $get_questionnnair_arr;
					//print_this($get_questionnnair_arr); exit;
		
					$html = $this->load->view('dashboard/pdf/download_survey_forms', $data, true);
					
					$this->load->library('pdf');
					$pdf = $this->pdf->load();
					
					$file_name = 'survey_forms.pdf';
					
					$address_2 = ($this->session->address_2) ? $this->session->address_2.', ': '';
					$phramcy_address = $this->session->address.', '.$address_2.$this->session->town.', '.$this->session->county.', '.$this->session->postcode;
		
					$pdf->SetHTMLFooter('<table width="100%" style="vertical-align: bottom; font-family: Arial, Helvetica, sans-serif; font-size: 9pt; color: #000000;"><tr>
<td width="66%">'.$this->session->pharmacy_name.', '.$phramcy_address.'<span></span></td>
<td width="33%" style="text-align: right; ">Page {PAGENO} of {nbpg}</td>
</tr></table>'); // Add a footer for good measure

					$pdf->AddPage('P'); // L - P
		
					$pdf->WriteHTML($html); // write the HTML into the PDF
		
					$pdf->Output($file_name,'D'); // save to file because we can
					
				}else{
					
					//Survey is nto yet completed, error
					$this->session->set_flashdata('err_message', 'It seems like survey is not yet completed.');
					redirect(SURL.'dashboard');
			
				}//end if($pharmacy_survey_details['survey_status'] == '3')

			}else
				redirect(SURL.'dashboard');

		}else
			redirect(SURL.'dashboard');
		//edd if($survey_id != '')
		
	}//end download_survey_form($survey_id)
	
	public function download_final_report($survey_id){
		
		set_time_limit(0);
		
		if($survey_id != ''){
			
			$pharmacy_survey_details = $this->survey->get_pharmacy_survey_list($this->session->p_id,'',$survey_id);
			
			if($pharmacy_survey_details){
				//Suirvey is valid 
				
				$pharmacy_details = $this->pharmacy->get_pharmacy_details($this->session->p_id);
				$data['pharmacy_details'] = $pharmacy_details;
				
				$data['pharmacy_survey_details'] = $pharmacy_survey_details;
				
				if($pharmacy_survey_details['survey_status'] == '3'){
					
					//Survey is completed go ahead.
					$get_questionnnair_arr = $this->survey->get_questionnaire_list($this->session->pharmacy_type);
					$data['questionnnair_arr'] = $get_questionnnair_arr;
					//print_this($get_questionnnair_arr); exit;
		
					$get_survey_question_stats = $this->survey->get_survey_question_stats($this->session->p_id, $survey_id, $pharmacy_survey_details['survey_ref_no'], '', $this->session->pharmacy_type);
					$data['survey_question_stats'] = $get_survey_question_stats;
					
					$total_submitted_surveys = $this->survey->get_survey_submitted_count($pharmacy_id, $survey_id);
					$data['total_submitted_surveys'] = $this->survey->get_survey_submitted_count($this->session->p_id, $survey_id);
					
					$data['survey_id'] = $survey_id;
					
					//Get Well Performed Questions
					$well_performed_arr = $this->survey->get_survey_question_closing_comments($this->session->p_id, $survey_id,'','1','');
					$data['well_performed_arr'] = $well_performed_arr;
					
					//print_this($well_performed_arr); exit;

					//Get Bad Performed Questions
					$bad_performed_arr = $this->survey->get_survey_question_closing_comments($this->session->p_id, $survey_id,'','','1');
					$data['bad_performed_arr'] = $bad_performed_arr;

					$html = $this->load->view('dashboard/pdf/download_final_report', $data, true);
					
					//echo $html; exit;
					
					$this->load->library('pdf');
					$pdf = $this->pdf->load();
					
					$file_name = 'survey_final_report.pdf';
		
					$pdf->SetHTMLFooter('<table width="100%" style="vertical-align: bottom; font-family: Arial, Helvetica, sans-serif; font-size: 9pt; color: #000000;"><tr>
<td width="33%"><span>Copyright '.$this->session->pharmacy_name.'</span></td>
<td width="33%" align="center">Result '.$pharmacy_survey_details['survey_title'].'</td>
<td width="33%" style="text-align: right; ">Page {PAGENO} of {nbpg}</td>
</tr></table>'); 

					$pdf->AddPageByArray(
							array(
								'orientation' => 'P',
								'mgl' => '20',
								'mgr' => '20',
								'mgt' => '10',
								'mgb' => '10',
								'mgh' => '0',
								'mgf' => '0', 
							)
						);
					
					//$pdf->AddPage('P'); // L - P
					$pdf->WriteHTML($html); // write the HTML into the PDF
					$pdf->Output($file_name,'D'); // save to file because we can
					
				}else{
					
					//Survey is nto yet completed, error
					$this->session->set_flashdata('err_message', 'It seems like survey is not yet completed.');
					redirect(SURL.'dashboard');
			
				}//end if($pharmacy_survey_details['survey_status'] == '3')

			}else
				redirect(SURL.'dashboard');

		}else
			redirect(SURL.'dashboard');
		//edd if($survey_id != '')
		
	}//end download_final_report($survey_id)
	
	public function download_survey_poster($survey_id){

		set_time_limit(0);
		if($survey_id != ''){
			
			$pharmacy_survey_details = $this->survey->get_pharmacy_survey_list($this->session->p_id,'',$survey_id);
			
			if($pharmacy_survey_details){
				//Suirvey is valid 
				
				$data['pharmacy_survey_details'] = $pharmacy_survey_details;
				
				if($pharmacy_survey_details['survey_status'] == '3'){
					
					//Survey is completed go ahead.
					$get_questionnnair_arr = $this->survey->get_questionnaire_list($this->session->pharmacy_type);
					$data['questionnnair_arr'] = $get_questionnnair_arr;
					//print_this($get_questionnnair_arr); exit;
					
					if($this->session->pharmacy_type == 'O')
						$question_id = 44;
					else if($this->session->pharmacy_type == 'OF')
						$question_id = 9;
					
					$get_survey_question_stats = $this->survey->get_survey_question_stats($this->session->p_id, $survey_id, $pharmacy_survey_details['survey_ref_no'],$question_id, $this->session->pharmacy_type);
					
					$data['survey_question_stats'] = $get_survey_question_stats;
					$data['survey_id'] = $survey_id;

					$html = $this->load->view('dashboard/pdf/download_survey_poster', $data, true);
					
					//echo $html; exit;
					
					$this->load->library('pdf');
					$pdf = $this->pdf->load();
					
					$file_name = 'survey_poster.pdf';
					
					$POSTER_FOOTER_TXT = 'POSTER_FOOTER_TXT';
					$survey_poster_footer_txt = get_global_settings($POSTER_FOOTER_TXT); //Set from the Global Settings
					$survey_poster_footer_txt = filter_string($survey_poster_footer_txt['setting_value']);
					
					$search_arr = array('[SURVEY_SESSION]',' [MIN_NO_OF_SURVEY]');
					$replace_arr = array(ltrim(filter_string($pharmacy_survey_details['survey_title']),'Survey '),filter_string($pharmacy_survey_details['min_no_of_surveys']) );
					
					$survey_poster_footer_txt = str_replace($search_arr, $replace_arr, $survey_poster_footer_txt);
					
					$footer_txt = '

						<div class="poster_footer">
							<div style="font-size:30px; color:#fff; font-family:Arial, Helvetica, sans-serif; text-align:left;">We are always striving to improve our services to you <br /><br /></div>
							<table cellpadding="0" cellspacing="0" width="100%" style="font-family:Arial, Helvetica, sans-serif">
								<tr>
									<td width="400px" style="color:#fff; font-size:12px" >
										'.$survey_poster_footer_txt.'
									</td>
									<td align="right" style="color:#fff" width="120px">
										<img src="'.IMAGES.'dhg.png" width="100px" />
									</td>
									<td align="right" style="color:#fff" width="120px">
										<img src="'.IMAGES.'nhs.png" width="100px" />
									</td>
								</tr>
							</table>
						</div>';
					
					//echo $footer_txt; exit;	
					
					$pdf->SetHTMLFooter($footer_txt); // Add a footer for good measure
					
					$pdf->AddPageByArray(array(
								'orientation' => 'P',
								'mgl' => '0',
								'mgr' => '0',
								'mgt' => '0',
								'mgb' => '0',
								'mgh' => '0',
								'mgf' => '0', 
							)
						);
					
					//echo $html; exit;
					
					$pdf->WriteHTML($html); // write the HTML into the PDF
					$pdf->Output($file_name,'D'); // save to file because we can
					
				}else{
					
					//Survey is nto yet completed, error
					$this->session->set_flashdata('err_message', 'It seems like survey is not yet completed.');
					redirect(SURL.'dashboard');
			
				}//end if($pharmacy_survey_details['survey_status'] == '3')

			}else
				redirect(SURL.'dashboard');

		}else
			redirect(SURL.'dashboard');
		//edd if($survey_id != '')

		
	}//end download_survey_poster($survey_id)
	
	public function generate_auto_report_charts(){
		
		extract($this->input->post());

		set_time_limit(0);
		
		if($survey_id != ''){
			
			$pharmacy_survey_details = $this->survey->get_pharmacy_survey_list($this->session->p_id,'',$survey_id);
			
			if($pharmacy_survey_details){
				//Suirvey is valid 
				
				$data['pharmacy_survey_details'] = $pharmacy_survey_details;
				
				if($pharmacy_survey_details['survey_status'] == '3'){
					
					//Survey is completed go ahead.
					$get_questionnnair_arr = $this->survey->get_questionnaire_list($this->session->pharmacy_type);
					$data['questionnnair_arr'] = $get_questionnnair_arr;
					//print_this($get_questionnnair_arr); exit;
					
					if($this->session->pharmacy_type == 'O'){
						$age_question_id = 49;
						$gender_question_id = 50;
						
					}else if($this->session->pharmacy_type == 'OF'){
						$age_question_id = 11;
						$gender_question_id = 12;
					}
					
					$get_survey_age_question_stats = $this->survey->get_survey_question_stats($this->session->p_id, $survey_id, $pharmacy_survey_details['survey_ref_no'],$age_question_id, $this->session->pharmacy_type);
					
					$data['survey_age_question_stats'] = $get_survey_age_question_stats;


					$get_survey_gender_question_stats = $this->survey->get_survey_question_stats($this->session->p_id, $survey_id, $pharmacy_survey_details['survey_ref_no'],$gender_question_id, $this->session->pharmacy_type);
					
					$data['survey_gender_question_stats'] = $get_survey_gender_question_stats;
					
					$data['survey_id'] = $survey_id;

					$svg_html = $this->load->view('dashboard/generate_auto_charts', $data, true);
					
					$response['error'] = '0';
					$response['data'] = $svg_html;
					
				
				}//end if($pharmacy_survey_details['survey_status'] == '3')
				
			}else{
				$response['error']= '1';	
			}//end if($pharmacy_survey_details)
		}else
			$response['error']= '1';

		echo json_encode($response);
		
	}//end generate_survey_report_charts($survey_id)
	
	public function download_patient_survey_poster($survey_id){
		
		set_time_limit(0);
		
		extract($this->input->post());
		
		if($survey_id != ''){
			
			$pharmacy_survey_details = $this->survey->get_pharmacy_survey_list($this->session->p_id,'',$survey_id);
			
			if($pharmacy_survey_details){
				
				//Suirvey is valid 
				$data['pharmacy_survey_details'] = $pharmacy_survey_details;
				
				$pharmacy_details = $this->pharmacy->get_pharmacy_details($pharmacy_survey_details['pharmacy_id']);
				
				$data['pharmacy_details'] = $pharmacy_details;
				
				if($pharmacy_survey_details['survey_status'] == '3'){
					
					//Survey is completed go ahead.
					$get_questionnnair_arr = $this->survey->get_questionnaire_list($this->session->pharmacy_type);
					$data['questionnnair_arr'] = $get_questionnnair_arr;
					//print_this($get_questionnnair_arr); exit;
					
					if($this->session->pharmacy_type == 'O'){
						$question_id = 44;
						$age_question_id = 49;
						$gender_question_id = 50;
						$patient_accessing_question_id = 36;

					}else if($this->session->pharmacy_type == 'OF'){
						$question_id = 9;
						$age_question_id = 11;
						$gender_question_id = 12;
						$patient_accessing_question_id = 1;
					}
					
					$get_survey_question_stats = $this->survey->get_survey_question_stats($this->session->p_id, $survey_id, $pharmacy_survey_details['survey_ref_no'],$question_id, $this->session->pharmacy_type);
					$data['survey_question_stats'] = $get_survey_question_stats;

					$get_survey_age_question_stats = $this->survey->get_survey_question_stats($this->session->p_id, $survey_id, $pharmacy_survey_details['survey_ref_no'],$age_question_id, $this->session->pharmacy_type);
					$data['survey_age_question_stats'] = $get_survey_age_question_stats;
					

					$get_survey_gender_question_stats = $this->survey->get_survey_question_stats($this->session->p_id, $survey_id, $pharmacy_survey_details['survey_ref_no'],$gender_question_id, $this->session->pharmacy_type);
					$data['survey_gender_question_stats'] = $get_survey_gender_question_stats;
					

					$get_accessing_pharmacy_stats = $this->survey->get_survey_question_stats($this->session->p_id, $survey_id, $pharmacy_survey_details['survey_ref_no'],$patient_accessing_question_id, $this->session->pharmacy_type);
					$data['accessing_pharmacy_stats'] = $get_accessing_pharmacy_stats;
					
					$data['svg_gender_pie_chart'] = $svg_gender_pie_chart;
					$data['svg_age_pie_chart'] = $svg_age_pie_chart;

					$total_submitted_online_surveys = $this->survey->get_survey_submitted_count($this->session->p_id, $survey_id,'ONLINE');
					
					$total_submitted_paper_surveys = $this->survey->get_survey_submitted_count($this->session->p_id, $survey_id,'PAPER');

					$total_submitted_surveys = $total_submitted_online_surveys + $total_submitted_paper_surveys;
					$data['total_submitted_surveys'] = $total_submitted_surveys;

					//Get Well Performed Questions
					$well_performed_arr = $this->survey->get_survey_question_closing_comments($this->session->p_id, $survey_id,'','1','');
					$data['well_performed_arr'] = $well_performed_arr;
					
					//print_this($well_performed_arr); exit;

					//Get Bad Performed Questions
					$bad_performed_arr = $this->survey->get_survey_question_closing_comments($this->session->p_id, $survey_id,'','','1');
					$data['bad_performed_arr'] = $bad_performed_arr;
					
					$data['survey_id'] = $survey_id;

					$html = $this->load->view('dashboard/pdf/download_patient_survey_poster', $data, true);
					
					//echo $html; exit;
					
					$this->load->library('pdf');
					$pdf = $this->pdf->load();
					
					$file_name = 'patient_survey_poster.pdf';
					
					$POSTER_FOOTER_TXT = 'POSTER_FOOTER_TXT';
					$survey_poster_footer_txt = get_global_settings($POSTER_FOOTER_TXT); //Set from the Global Settings
					$survey_poster_footer_txt = filter_string($survey_poster_footer_txt['setting_value']);
					
					$search_arr = array('[SURVEY_SESSION]',' [MIN_NO_OF_SURVEY]');
					$replace_arr = array(ltrim(filter_string($pharmacy_survey_details['survey_title']),'Survey '),filter_string($pharmacy_survey_details['min_no_of_surveys']) );
					
					$survey_poster_footer_txt = str_replace($search_arr, $replace_arr, $survey_poster_footer_txt);
					
					$pharma_address_2 = (filter_string($pharmacy_details['address_2'])) ? filter_string($pharmacy_details['address_2']).'<br>' : '';
					$pharma_town = (filter_string($pharmacy_details['town'])) ? filter_string($pharmacy_details['town']).'<br>' : '';
					$pharma_contact = (filter_string($pharmacy_details['contact_no'])) ? 'Tel: '.filter_string($pharmacy_details['contact_no']) : '';
					$pharma_postcode = (filter_string($pharmacy_details['postcode'])) ? strtoupper(filter_string($pharmacy_details['postcode'])) : '';
					
					$footer_txt = '

						<div class="poster_footer">
							<div style="width:70%; float:left; color:#fff; font-family:Arial, Helvetica, sans-serif; text-align:left;">
								<div style="padding-bottom:10px; font-size:30px;">We are always striving to improve our services to you </div>
								<table cellpadding="0" cellspacing="0" width="100%" style="font-family:Arial, Helvetica, sans-serif">
									<tr>
										<td width="400px" style="color:#fff; font-size:12px" >
											Full report for the Patient Survey is available to customers upon request <br> <br>
											Results for '.$pharmacy_survey_details['survey_title'].' Community Pharmacy Patient Questionnaire with '.$total_submitted_surveys.' patients surveyed. The survey was
								conduction and processed through SurveyFocus a product of Digital Health Group Ltd. © '.date('Y').' <br>
											
										</td>
										<td align="left" valign="top" style="color:#fff" width="120px">
											
										</td>
										
									</tr>
								</table>							
							</div>
							<div style="width:30%; float:left; color:#fff; font-family:Arial, Helvetica, sans-serif; text-align:left; ">
								<div style="padding-left:50px; font-size:18px;">
									'.filter_string($pharmacy_details['pharmacy_name']).' <br>
									'.filter_string($pharmacy_details['address']).'<br>
									'.$pharma_address_2.'
									'.$pharma_town.'
									'.$pharma_postcode.'
								</div>
							</div>
							
						</div>';
					
					//echo $footer_txt; exit;	
					
					$pdf->mirrorMargins = 1;
					//$pdf->SetHTMLFooterByName('MyCustomFooter');
					
					$pdf->SetHTMLFooter('');
						
					$pdf->SetHTMLFooter($footer_txt);
						
					$pdf->AddPageByArray(array(
								'orientation' => 'L',
								'mgl' => '0',
								'mgr' => '0',
								'mgt' => '0',
								'mgb' => '0',
								'mgh' => '0',
								'mgf' => '0', 
							)
						);
					
					//echo $html; exit;
					
					$pdf->WriteHTML($html); // write the HTML into the PDF
					$pdf->Output($file_name,'D'); // save to file because we can
					
				}else{
					
					//Survey is nto yet completed, error
					$this->session->set_flashdata('err_message', 'It seems like survey is not yet completed.');
					redirect(SURL.'dashboard');
			
				}//end if($pharmacy_survey_details['survey_status'] == '3')

			}else
				redirect(SURL.'dashboard');

		}else
			redirect(SURL.'dashboard');
		//edd if($survey_id != '')

	}//end download_patient_survey_poster($survey_id)
	
	public function download_patient_satisfaction_survey_poster($survey_id){
		
		set_time_limit(0);
		
		extract($this->input->post());
		
		if($survey_id != ''){
			
			$pharmacy_survey_details = $this->survey->get_pharmacy_survey_list($this->session->p_id,'',$survey_id);
			
			if($pharmacy_survey_details){
				//Suirvey is valid 
				
				$data['pharmacy_survey_details'] = $pharmacy_survey_details;
				
				if($pharmacy_survey_details['survey_status'] == '3'){
					
					//Survey is completed go ahead.
					$get_questionnnair_arr = $this->survey->get_questionnaire_list($this->session->pharmacy_type);
					$data['questionnnair_arr'] = $get_questionnnair_arr;
					//print_this($get_questionnnair_arr); exit;
					
					if($this->session->pharmacy_type == 'O'){
						$question_id = 44;
						$age_question_id = 49;
					}else if($this->session->pharmacy_type == 'OF'){
						$question_id = 9;
						$age_question_id = 11;
					}
					
					$get_survey_question_stats = $this->survey->get_survey_question_stats($this->session->p_id, $survey_id, $pharmacy_survey_details['survey_ref_no'],$question_id, $this->session->pharmacy_type);
					
					$data['survey_question_stats'] = $get_survey_question_stats;

					$total_submitted_online_surveys = $this->survey->get_survey_submitted_count($this->session->p_id, $survey_id,'ONLINE');
					
					$total_submitted_paper_surveys = $this->survey->get_survey_submitted_count($this->session->p_id, $survey_id,'PAPER');

					$total_submitted_surveys = $total_submitted_online_surveys + $total_submitted_paper_surveys;
					$data['total_submitted_surveys'] = $total_submitted_surveys;

					//Get Well Performed Questions
					$well_performed_arr = $this->survey->get_survey_question_closing_comments($this->session->p_id, $survey_id,'','1','');
					$data['well_performed_arr'] = $well_performed_arr;
					
					//print_this($well_performed_arr); exit;

					//Get Bad Performed Questions
					$bad_performed_arr = $this->survey->get_survey_question_closing_comments($this->session->p_id, $survey_id,'','','1');
					$data['bad_performed_arr'] = $bad_performed_arr;
					
					$data['survey_id'] = $survey_id;

					$html = $this->load->view('dashboard/pdf/download_patient_satisfaction_survey_poster', $data, true);
					
					//echo $html; exit;
					
					$this->load->library('pdf');
					$pdf = $this->pdf->load();
					
					$file_name = 'patient_satisfaction_survey_poster.pdf';
					
					$POSTER_FOOTER_TXT = 'POSTER_FOOTER_TXT';
					$survey_poster_footer_txt = get_global_settings($POSTER_FOOTER_TXT); //Set from the Global Settings
					$survey_poster_footer_txt = filter_string($survey_poster_footer_txt['setting_value']);
					
					$search_arr = array('[SURVEY_SESSION]',' [MIN_NO_OF_SURVEY]');
					$replace_arr = array(ltrim(filter_string($pharmacy_survey_details['survey_title']),'Survey '),filter_string($pharmacy_survey_details['min_no_of_surveys']) );
					
					$survey_poster_footer_txt = str_replace($search_arr, $replace_arr, $survey_poster_footer_txt);
					
					$footer_txt = '

						<div class="poster_footer">
							<div style="font-size:30px; color:#fff; font-family:Arial, Helvetica, sans-serif; text-align:left;">We are always striving to improve our services to you <br /><br /></div>
							<table cellpadding="0" cellspacing="0" width="100%" style="font-family:Arial, Helvetica, sans-serif">
								<tr>
									<td width="400px" style="color:#fff; font-size:12px" >
										'.$survey_poster_footer_txt.'
									</td>
									<td align="right" style="color:#fff" width="120px">
										<img src="'.IMAGES.'dhg.png" width="100px" />
									</td>
									<td align="right" style="color:#fff" width="120px">
										<img src="'.IMAGES.'nhs.png" width="100px" />
									</td>
								</tr>
							</table>
						</div>';
					
					//echo $footer_txt; exit;	
					$pdf->mirrorMargins = 1;
					//$pdf->SetHTMLFooterByName('MyCustomFooter');
					
					$pdf->SetHTMLFooter('');
						
					//$pdf->SetHTMLFooter($footer_txt);
						
					$pdf->AddPageByArray(array(
								'orientation' => 'P',
								'mgl' => '10',
								'mgr' => '10',
								'mgt' => '10',
								'mgb' => '10',
								'mgh' => '0',
								'mgf' => '0', 
							)
						);
					
					//echo $html; exit;
					
					$pdf->WriteHTML($html); // write the HTML into the PDF
					$pdf->Output($file_name,'D'); // save to file because we can
					
				}else{
					
					//Survey is nto yet completed, error
					$this->session->set_flashdata('err_message', 'It seems like survey is not yet completed.');
					redirect(SURL.'dashboard');
			
				}//end if($pharmacy_survey_details['survey_status'] == '3')

			}else
				redirect(SURL.'dashboard');

		}else
			redirect(SURL.'dashboard');
		//edd if($survey_id != '')

	}//end download_patient_survey_poster($survey_id)
	
	public function download_survey_doh_letter($survey_id){
		
		set_time_limit(0);
		
		if($survey_id != ''){
			
			$pharmacy_survey_details = $this->survey->get_pharmacy_survey_list($this->session->p_id,'',$survey_id);
			
			if($pharmacy_survey_details){
				//Suirvey is valid 
				
				$data['pharmacy_survey_details'] = $pharmacy_survey_details;
				
				if($pharmacy_survey_details['survey_status'] == '3'){
					
					$data['survey_id'] = $survey_id;

					$this->load->model('email_mod','email_template');
					$doh_letter_body_arr = $this->email_template->get_email_template(4);
					$data['doh_letter_body'] = $doh_letter_body_arr;
					
					//Total Survey Submitted
					$total_submitted_surveys = $this->survey->get_survey_submitted_count($this->session->p_id, $survey_id);
					$data['total_submitted_surveys'] = $total_submitted_surveys;
					
					//Get Well Performed Questions
					$well_performed_arr = $this->survey->get_survey_question_closing_comments($this->session->p_id, $survey_id,'','1','');
					$data['well_performed_arr'] = $well_performed_arr;
					
					//print_this($well_performed_arr); exit;

					//Get Bad Performed Questions
					$bad_performed_arr = $this->survey->get_survey_question_closing_comments($this->session->p_id, $survey_id,'','','1');
					$data['bad_performed_arr'] = $bad_performed_arr;
					
					$html = $this->load->view('dashboard/pdf/download_doh_letter', $data, true);
					
					//echo $html; exit;
					
					$this->load->library('pdf');
					$pdf = $this->pdf->load();
					
					$file_name = 'survey_doh_letter.pdf';
		
					$pdf->SetHTMLFooter('<table width="100%" style="vertical-align: bottom; font-family: Arial, Helvetica, sans-serif; font-size: 8pt; color: #000000;"><tr>
<td width="66%"><span>Patient Satisfaction Surveys Processed by '.$this->session->pharmacy_name.' &copy; '.date('Y').'</span></td>
<td width="33%" style="text-align: right; ">Page {PAGENO} of {nbpg}</td>
</tr></table>'); // Add a footer for good measure
					
					$pdf->AddPageByArray(array(
							'orientation' => 'P',
							'mgt' => '5',
							'mgb' => '10',
						)
					);
					
					$pdf->WriteHTML($html); // write the HTML into the PDF
					$pdf->Output($file_name,'D'); // save to file because we can
					
				}else{
					
					//Survey is nto yet completed, error
					$this->session->set_flashdata('err_message', 'It seems like survey is not yet completed.');
					redirect(SURL.'dashboard');
			
				}//end if($pharmacy_survey_details['survey_status'] == '3')

			}else
				redirect(SURL.'dashboard');

		}else
			redirect(SURL.'dashboard');
		//edd if($survey_id != '')
		
	}//end download_doh_letter($survey_id)
	
	public function view_question_all_comments($survey_id, $question_id){

		$pharmacy_survey_details = $this->survey->get_pharmacy_survey_list($this->session->p_id,'',$survey_id);
		
		if($pharmacy_survey_details){
			
			$get_survey_question_stats = $this->survey->get_survey_question_stats($this->session->p_id, $survey_id, $pharmacy_survey_details['survey_ref_no'], $question_id, $this->session->pharmacy_type);
			//print_this($get_survey_question_stats); exit;
			
			$data['question_id'] = $question_id;
			$data['survey_question_stats'] = $get_survey_question_stats;

			$this->stencil->layout('modal_layout');
			$this->stencil->paint('dashboard/view_survey_question_comments',$data);
			
		}else{
			echo 'Invalid request';	
			exit;
		}//end if($pharmacy_survey_details)
		
	}//end  view_question_all_comments($survey_id, $question_id)
	
	// Start => public function update_pharmacy_profile()
	public function update_pharmacy_profile(){

		// Verify if the POST is set
		if(!$this->input->post()) exit;
		
		extract($this->input->post());
		
		$verify_pharmacy_exist = $this->register->verify_if_pharmacy_already_exist($email_address,$this->session->p_id);

			if($verify_pharmacy_exist){

				// session set form data in fields
				$this->session->set_flashdata($this->input->post());
				$this->session->set_flashdata('err_message', 'The email you have entered already exist, please try another one.');
				redirect(base_url().'dashboard?t=5');
				
			} else{

				$updated = $this->pharmacy->update_pharmacy_profile($this->input->post(), $this->session->p_id);
				if($updated){

					$this->session->pharmacy_name = $pharmacy_name;
					$this->session->owner_name = $owner_name;
					$this->session->address = $address;
					$this->session->address_2 = $address_2;
					$this->session->postcode = $postcode;
					$this->session->town = $town;
					$this->session->county = $county;
					$this->session->contact_no = $contact_no;
					
					$this->session->set_flashdata('ok_message', "Profile is updated successfully.");	
					
				} else {
					$this->session->set_flashdata('err_message', 'Oops! Something went wrong, please try again later.');
				} // if($updated)
		
			}//end if($verify_pharmacy_exist)

		// Redirect user to Dashboard
		redirect(base_url().'dashboard?t=5');

	} // End => public function update_pharmacy_profile()
	
	// Start => public function create_new_password()
	public function create_new_password(){

		// Verify if the POST is set
		if(!$this->input->post()) exit;

		$updated = $this->pharmacy->create_new_password($this->input->post(), $this->session->p_id);
		if($updated){
			$this->session->set_flashdata('ok_message', "New password is successfully updated.");
		} else {
			$this->session->set_flashdata('err_message', 'Oops! Something went wrong, please try again later.');
		} // if($updated)

		// Redirect user to Dashboard
		redirect(base_url().'dashboard?t=5');

	} // End => public function create_new_password()

	// public function send_sms()
	public function send_sms(){

		if(!$this->input->post()) exit;

		// Extract POST
		extract($this->input->post());
		
		$send_sms = $this->survey->send_sms($this->session->p_id, $survey_id, $this->input->post());
		
		// Verify if the SMS is successfully sent
		if($send_sms){

			// On success
			// Update record : The message is sent
			$this->pharmacy->send_sms($this->session->p_id, $this->input->post());
			$this->session->set_flashdata('sms_ok_message', "SMS is successfully sent to the recepient.");

		} else {

			// On Failure
			$this->session->set_flashdata('sms_err_message', 'Oops! Something went wrong, please try again later.');

		} // if($sent)

		// Redirect to the DASHBOARD
		redirect(SURL.'dashboard');

	} // public function send_sms()
	
	public function mark_question_reviewed(){
		
		if(!$this->input->post()) exit;

		// Extract POST
		extract($this->input->post());
		
		$pharmacy_survey_details = $this->survey->get_pharmacy_survey_list($this->session->p_id,'',$survey_id);
		
		if($pharmacy_survey_details){
			
			$mark_survey_question_as_reviewed = $this->survey->save_survey_question_comments($this->session->p_id, $survey_id, $question_id, '');
			if($mark_survey_question_as_reviewed)
				return true;
			else
				return false;
			
		}else{
			exit;	
		}//end if($pharmacy_survey_details)

	}//end mark_question_reviewed()
	
	public function view_invoice($invoice_id){
		
		$verify_if_valid_invoice = $this->survey->get_survey_payment_invoices($this->session->p_id,$invoice_id);
		
		if($verify_if_valid_invoice){

			$data['invoice_data'] = $verify_if_valid_invoice;
			
			$this->stencil->layout('modal_layout');
			$this->stencil->paint('dashboard/view_invoice_details',$data);
			
		}else{
			echo 'Invalid Invoice'; exit;
		}
		
	}//end view_invoice($invoice_id)

	public function download_invoice($invoice_id){
		
		$verify_if_valid_invoice = $this->survey->get_survey_payment_invoices($this->session->p_id,$invoice_id);
		
		if($verify_if_valid_invoice){

			$data['invoice_data'] = $verify_if_valid_invoice;
			
			$this->stencil->layout('modal_layout');
			$html = $this->load->view('dashboard/pdf/download_invoice', $data, true);
			
			//echo $html; exit;
			
			$this->load->library('pdf');
			$pdf = $this->pdf->load();
			
			$file_name = 'invoice-'.$verify_if_valid_invoice['invoice_no'].'.pdf';
			/*
			$pdf->SetHTMLFooter('<table width="100%" style="vertical-align: bottom; font-family: Arial, Helvetica, sans-serif; font-size: 9pt; color: #000000;"><tr>
<td width="33%"><span>'.$verify_if_valid_invoice['from_website'].'</span></td>
<td width="33%" align="center"><span>'.$verify_if_valid_invoice['from_email'].'</span></td>
<td width="33%" style="text-align: right; "><span>'.$verify_if_valid_invoice['from_phone'].'</span></td>
</tr></table>'); // Add a footer for good measure
*/

			$pdf->AddPageByArray(array(
						'orientation' => 'P',
						'mgl' => '20',
						'mgr' => '20',
						'mgt' => '10',
						'mgb' => '10',
						'mgh' => '0',
						'mgf' => '0', 
					)
				);
			//header('Content-Type: application/pdf');
			//$pdf->AddPage('P'); // L - P
			$pdf->WriteHTML($html); // write the HTML into the PDF
			$pdf->Output($file_name,'I'); // save to file because we can
			
		}else{
			echo 'Invalid Invoice'; exit;
		}
		
	}//end view_invoice($invoice_id)

	// Start => public function buy_survey()
	public function buy_survey(){

		// verify if the POST is set
		if(!$this->input->post()) exit;
		
		extract($this->input->post());

		// Verify which payment method is selected [ 'Paypal | Stripe' ]
		if($payment_method_radio && $payment_method_radio == 'credit_card'){

			// ------------------ //
			// ----- Stripe ----- //
			// ------------------ //

			require(APPPATH.'libraries/stripe/init.php');

			// Get stripe APi credentials from GLOBAL SETTINGS

			$STRIPE_SECRET_KEY = 'STRIPE_SECRET_KEY';
			$stripe_secret_key = get_global_settings($STRIPE_SECRET_KEY); //Set from the Global Settings
			$stripe_secret_key = filter_string($stripe_secret_key['setting_value']);

			$STRIPE_PUBLISHABLE_KEY = 'STRIPE_PUBLISHABLE_KEY';
			$stripe_publishable_key = get_global_settings($STRIPE_PUBLISHABLE_KEY); //Set from the Global Settings
			$stripe_publishable_key = filter_string($stripe_publishable_key['setting_value']);

			$stripe = array(
				"secret_key"      => $stripe_secret_key,
				"publishable_key" => $stripe_publishable_key
			);

			\Stripe\Stripe::setApiKey($stripe['secret_key']);

			$err_msg = '';

			// Get price from GLOBAL SETTINGS
			$PHARMACY_SURVEY_PRICE = 'PHARMACY_SURVEY_PRICE';
			$pharmacy_survey_price = get_global_settings($PHARMACY_SURVEY_PRICE); //Set from the Global Settings
			$pharmacy_survey_price = filter_string($pharmacy_survey_price['setting_value']);
			
			$price = $pharmacy_survey_price;
			$sub_total = $price;

			try {
			  
				// Use Stripe's bindings...
				$charge = \Stripe\Charge::create(array(
					'amount'        => ($price*100),
					'currency'      => CURRENCY,
					'card'          => array(
						'number'    => $card,
						'exp_month' => $expMonth,
						'exp_year'  => $expYear,
						'cvc'       => $cvc
					), 'description'   => '')
				);

			} catch(Stripe_CardError $e) {

				// Since it's a decline, Stripe_CardError will be caught
				$body = $e->getJsonBody();
				$err  = $body['error'];

				print('Status is:' . $e->getHttpStatus() . "\n");
				print('Type is:' . $err['type'] . "\n");
				print('Code is:' . $err['code'] . "\n");
				// param is '' in this case
				print('Param is:' . $err['param'] . "\n");
				print('Message is:' . $err['message'] . "\n");

				$err_msg .= $err['message'];

			} catch (Stripe_InvalidRequestError $e) {
				
				// Invalid parameters were supplied to Stripe's API
				$err_msg .= $e->getMessage();

			} catch (Stripe_AuthenticationError $e) {
			  	
			  	// Authentication with Stripe's API failed
			  	// (maybe you changed API keys recently)
				$err_msg .= $e->getMessage();

			} catch (Stripe_ApiConnectionError $e) {
			  	
			  	// Network communication with Stripe failed
				$err_msg .= $e->getMessage();

			} catch (Stripe_Error $e) {
			  	
			  	// Display a very generic error to the user, and maybe send
			  	// yourself an email
				$err_msg .= $e->getMessage();

			} catch (Exception $e) {
			  	
			  	// Something else happened, completely unrelated to Stripe
				$err_msg .= $e->getMessage();

			} // try => catch

		} else if($payment_method_radio && $payment_method_radio == 'paypal'){
			
			$data = array('registration_form' => $this->input->post());
			$this->session->set_userdata($data);

			// PAYPAL //
			$this->checkout($this->input->post());
			$this->session->set_flashdata('err_message', 'Something went wrong during the payment process, please try again later or contact site administrator.');
			redirect(SURL.'dashboard');
			//exit('PayPal');

		} // if($payment_method_radio && $payment_method_radio == 'credit_card')

		// If the method is CREDIT CARD stripe charge
		if($charge && !$err_msg){

			////////////////////////////////////
			// Make entries into the database //
			// 1- Make entry into surveys
			// 2- Make entry into purchase history

			$pharmacy_id = $this->session->p_id;
			
			// Insert Survey
			$survey_id = $this->survey->add_new_survey($pharmacy_id,$next_year);
			
			$VAT_PERCENTAGE = 'VAT_PERCENTAGE';
			$vat_percentage = get_global_settings($VAT_PERCENTAGE); //Set from the Global Settings

			$vat_amount = (trim($vat_percentage['setting_value']) / 100) * $sub_total;
			$vat_amount = filter_price($vat_amount);
			
			$grand_total = $sub_total + $vat_amount;

			$survey_session = $this->survey->get_pharmacy_survey_list($pharmacy_id, '', $survey_id);

			// Insert Invoice
			$invoice_data['subtotal'] = $sub_total;
			$invoice_data['vat_tax'] = $vat_amount;
			$invoice_data['vat_percentage'] = trim($vat_percentage['setting_value']);
			$invoice_data['survey_session'] = $survey_session['survey_title'];
			$invoice_data['grand_total'] = $grand_total;
			$invoice_data['payment_method'] = 'CARD';
			$invoice_data['transaction_id'] = $charge->id;
			
			// Get pharmacy Details
			$pharmacy_details = $this->pharmacy->get_pharmacy_details($pharmacy_id);
			
			if($survey_id){
				
				//Verify of the Affiliate code exist is still valid
	
				if(filter_string($pharmacy_details['affiliate_code']) != ''){
					
					$affiliate_code = filter_string($pharmacy_details['affiliate_code']);
					
					$verify_affiliate = $this->common->verify_affiliate($affiliate_code);
					
					if($verify_affiliate == "OK"){
						
						//Verify if affiliate is in recurring or not.						

						$affiliate_by_code_encode = $this->common->get_affiliate_by_code($affiliate_code);
						$affiliate_by_code = json_decode($affiliate_by_code_encode);
						
						$affiliate_product_encode = $this->common->get_affiliate_product($affiliate_by_code->id, 1);
						$affiliate_product = json_decode($affiliate_product_encode);
						
						if($affiliate_product->is_recurring == '1'){
						
							//Add Lead to record
							$generate_lead = $this->common->generate_lead('SURVEY',$pharmacy_details['pharmacy_name'], $grand_total, date('Y-m-d G:i:s'), $affiliate_code, 1, $this->input->ip_address(), $pharmacy_id);					

						}//end if($affiliate_product->is_recurring == '1')
						
					}//end if($verify_affiliate == "OK")
							
				}//end if($affiliate_code)

				// Payment success

				// Send Pharmacy Registration Email
				
				// Get price from GLOBAL SETTINGS
				$FROM_BUSINESS_NAME = 'FROM_BUSINESS_NAME';
				$from_business_name = get_global_settings($FROM_BUSINESS_NAME); //Set from the Global Settings
				$from_business_name = filter_string($from_business_name['setting_value']);
				
				// Get price from GLOBAL SETTINGS
				$FROM_ADDRESS = 'FROM_ADDRESS';
				$from_address = get_global_settings($FROM_ADDRESS); //Set from the Global Settings
				$from_address = filter_string($from_address['setting_value']);
				
				// Get price from GLOBAL SETTINGS
				$FROM_ADDRESS_2 = 'FROM_ADDRESS_2';
				$from_address_2 = get_global_settings($FROM_ADDRESS_2); //Set from the Global Settings
				$from_address_2 = filter_string($from_address_2['setting_value']);
				
				// Get price from GLOBAL SETTINGS
				$FROM_COUNTY = 'FROM_COUNTY';
				$from_county = get_global_settings($FROM_COUNTY); //Set from the Global Settings
				$from_county = filter_string($from_county['setting_value']);
				
				// Get price from GLOBAL SETTINGS
				$FROM_POSTCODE = 'FROM_POSTCODE';
				$from_postcode = get_global_settings($FROM_POSTCODE); //Set from the Global Settings
				$from_postcode = filter_string($from_postcode['setting_value']);
				
				// Get price from GLOBAL SETTINGS
				$FROM_TOWN = 'FROM_TOWN';
				$from_town = get_global_settings($FROM_TOWN); //Set from the Global Settings
				$from_town = filter_string($from_town['setting_value']);
				
				// Get price from GLOBAL SETTINGS
				$FROM_EMAIL = 'FROM_EMAIL';
				$from_email = get_global_settings($FROM_EMAIL); //Set from the Global Settings
				$from_email = filter_string($from_email['setting_value']);
				
				// Get price from GLOBAL SETTINGS
				$FROM_WEBSITE = 'FROM_WEBSITE';
				$from_website = get_global_settings($FROM_WEBSITE); //Set from the Global Settings
				$from_website = filter_string($from_website['setting_value']);
				
				// Get price from GLOBAL SETTINGS
				$FROM_PHONE = 'FROM_PHONE';
				$from_phone = get_global_settings($FROM_PHONE); //Set from the Global Settings
				$from_phone = filter_string($from_phone['setting_value']);
				
				// Get price from GLOBAL SETTINGS
				$SITE_LOGO = 'SITE_LOGO';
				$site_logo = get_global_settings($SITE_LOGO); //Set from the Global Settings
				$site_logo_src = filter_string($site_logo['setting_value']);

				// Insert invoice : along with new fields //
				$invoice_data['pharmacy_name'] = $pharmacy_details['pharmacy_name'];
				$invoice_data['pharmacy_owner_name'] = $pharmacy_details['owner_name'];
				$invoice_data['pharmacy_address'] = $pharmacy_details['address'];
				$invoice_data['pharmacy_address_2'] = $pharmacy_details['address_2'];
				$invoice_data['pharmacy_town'] = $pharmacy_details['town'];
				$invoice_data['pharmacy_county'] = $pharmacy_details['county'];
				$invoice_data['pharmacy_postcode'] = $pharmacy_details['postcode'];
				$invoice_data['from_name'] = $from_business_name;
				$invoice_data['from_address'] = $from_address;
				$invoice_data['from_address_2'] = $from_address_2;
				$invoice_data['from_town'] = $from_town;
				$invoice_data['from_county'] = $from_county;
				$invoice_data['from_postcode'] = $from_postcode;
				$invoice_data['from_phone'] = $from_phone;
				$invoice_data['from_email'] = $from_email;
				$invoice_data['from_website'] = $from_website;

				$invoice_generated = $this->survey->save_invoice($pharmacy_id, $survey_id, $invoice_data);

				$search_arr = array(
					'[REF_NO]', '[DATE]', '[TO]', '[FULL_NAME]',
					'[ADDRESS]', '[ADDRESS_2]', '[COUNTY]',
					'[POSTCODE]', '[TOWN]', '[SITE_LOGO]',
					'FROM_BUSINESS_NAME', 'FROM_ADDRESS',
					'FROM_ADDRESS_2', 'FROM_COUNTY',
					'FROM_POSTCODE', 'FROM_TOWN',
					'FROM_PHONE',
					'FROM_EMAIL', 'FROM_WEBSITE',
					'[SURVEY_PRICE]', '[SUB_TOTAL]',
					'[VAT]', '[VAT_AMOUNT]', '[GRAND_TOTAL]', '[SURVEY_SESSION]', '[PHARMACY_NAME]'
				);

				$replace_arr = array(
					$invoice_generated['invoice_no'], date('d/m/Y'), $pharmacy_details['pharmacy_name'], $pharmacy_details['owner_name'], 
					$pharmacy_details['address'], $pharmacy_details['address_2'], $pharmacy_details['county'], 
					$pharmacy_details['postcode'], $pharmacy_details['town'], $site_logo_src,
					$from_business_name, $from_address,
					$from_address_2, $from_county, 
					$from_postcode, $from_town,
					$from_phone,
					$from_email, $from_website, 
					'£'.$price, '£'.$sub_total, 
					$vat_percentage['setting_value'].'%', '£'.$vat_amount, '£'.$grand_total, $survey_session['survey_title'],
					$pharmacy_name
					
					);
				
				$this->load->model('email_mod','email_template');
				
				$email_body_arr = $this->email_template->get_email_template(7);
				
				$email_subject = $email_body_arr['email_subject'];
				$email_body = $email_body_arr['email_body'];
				
				$email_body = str_replace($search_arr,$replace_arr,$email_body);
				
				// Get FROM_EMAIL_TXT from GLOBAL SETTINGS
				$FROM_EMAIL_TXT = 'FROM_EMAIL_TXT';
				$from_email_txt = get_global_settings($FROM_EMAIL_TXT); //Set from the Global Settings
				$email_from_txt = filter_string($from_email_txt['setting_value']);

				$SES_SENDER = 'SES_SENDER';
				$ses_sender = get_global_settings($SES_SENDER);
				$ses_sender = trim($ses_sender['setting_value']);

				$to_email_address = trim($pharmacy_details['email_address']); //'twister787@gmail.com';
				                                                      
				$email_subject = filter_string($email_subject); //'Amazon SES test (SMTP interface accessed using PHP)';
				$email_body = filter_string($email_body); //'This email was sent through the Amazon SES SMTP interface by using PHP.';

				## 
				## START: Preparing Attachment for Invoice
				##

				$data['invoice_data'] = $invoice_generated;
				
				$this->stencil->layout('modal_layout');
				$html = $this->load->view('dashboard/pdf/download_invoice', $data, true);
				
				//echo $html; exit;
				
				$this->load->library('pdf');
				$pdf = $this->pdf->load();
				
				$file_name = 'invoice-'.$invoice_generated['invoice_no'].'.pdf';
	
				$pdf->AddPageByArray(array(
							'orientation' => 'P',
							'mgl' => '20',
							'mgr' => '20',
							'mgt' => '10',
							'mgb' => '10',
							'mgh' => '0',
							'mgf' => '0', 
						)
					);
				//header('Content-Type: application/pdf');
				//$pdf->AddPage('P'); // L - P
				$pdf->WriteHTML($html); // write the HTML into the PDF
				//$pdf->Output($file_name,'I'); // save to file because we can
				$pdf->Output('./assets/invoice_tmp/'.$file_name,'F'); 
				
				## 
				## END: Preparing Attachment for Invoice
				##
				
				$attachment_arr[] = 
					array('name' => $file_name,
							'mime' => 'application/pdf',
							'filepath' => './assets/invoice_tmp/'.$file_name
				);

				$send_email = send_email($to_email_address, $ses_sender, $email_from_txt, $email_subject, $email_body, $attachment_arr);
				
				////////////////////////////////////

					$search_arr = array('[PHARMACY_NAME]',  '[SURVEY_TITLE]', '[PURCHASED_DATE]');
					$replace_arr = array($pharmacy_details['pharmacy_name'], $survey_session['survey_title'], date('d/m/Y'));                
								
					$email_body_arr = $this->email_template->get_email_template(9);
					
					$email_subject = filter_string($email_body_arr['email_subject']);
					$email_body = filter_string($email_body_arr['email_body']);
					
					$email_body = str_replace($search_arr,$replace_arr,$email_body);
					
					$admin_send_email = send_email($ses_sender, $ses_sender, $email_from_txt, $email_subject, $email_body, $attachment_arr);

					if(file_exists('./assets/invoice_tmp/'.$file_name))
						unlink('./assets/invoice_tmp/'.$file_name);
					
				// Store Stripe errors in session
				$this->session->unset_userdata('buy_survey_form');

				$this->session->set_flashdata('ok_message', 'You have successfully purchased a new survey. Please start your survey to continue.');
				redirect(SURL.'dashboard');

			} else {
			
				// Store Stripe errors in session
				$data = array('buy_survey_form' => $this->input->post());
				$this->session->set_userdata($data);

				$this->session->set_flashdata('err_message', 'Something went wrong, please try again later.');
				redirect(SURL.'dashboard');
				
			} // End else - if($survey_id)

		} else {
			
			// Store Stripe errors in session
			$data = array('buy_survey_form' => $this->input->post());
			$this->session->set_userdata($data);

			$this->session->set_flashdata('err_message', $err_msg);
			redirect(SURL.'dashboard');

		} // if($charge)

	} // End => public function buy_survey()

	////////////////////
	/* --- PayPal --- */
	// Function checkout(): Checkout to the PayPal
	public function checkout($post=''){
		
		// Verify if the POST data is being passed from register_process //
		if($post == '') return false;

		// Extract POST
		extract($post);
		
		$to_buy = array(
			'desc' => 'Charge for: Pharmacy Surveys', 
			'currency' => CURRENCY,  // CURRENCY
			'type' => PAYMENT_METHOD,  // PAYMENT_METHOD
			'return_URL' => SURL.'dashboard/checkout-success/'.$next_year, 
			// see below have a function for this -- function back()
			// whatever you use, make sure the URL is live and can process
			// the next steps
			'cancel_URL' => SURL.'dashboard', // this goes to this controllers index()
			'shipping_amount' => 0.00,
			'get_shipping' => false
		);
		
		// Get price from GLOBAL SETTINGS
		$PHARMACY_SURVEY_PRICE = 'PHARMACY_SURVEY_PRICE';
		$pharmacy_survey_price = get_global_settings($PHARMACY_SURVEY_PRICE); //Set from the Global Settings
		$pharmacy_survey_price = filter_string($pharmacy_survey_price['setting_value']);

		$price = $pharmacy_survey_price;
		$sub_total = $price;

		//Survey is not yet started means we have to start the survey of the current one.
		$current_date = date('m/d');	
		$survey_start_date = date('Y-m-d');
		$data['survey_start_date'] = $survey_start_date;
		
		$SURVEY_END_MONTH = 'SURVEY_END_MONTH';
		$survey_end_global_value = get_global_settings($SURVEY_END_MONTH); //Set from the Global Settings
		$next_survey_end_date = filter_string($survey_end_global_value['setting_value']);
		
		if($next_year == '1'){

			$next_survey_end = strtotime("$next_survey_end_date +1 year"); 
			$survey_session = 'Survey '.(date('Y', $next_survey_end)-1).'-'.date('y', $next_survey_end);
			$survey_end_date = (date('Y-m-d', $next_survey_end)-1).'-'.date('y', $next_survey_end);
			
		}else{

			if(strtotime($current_date) > strtotime($next_survey_end_date)){
				
				$next_survey_end = strtotime("$next_survey_end_date +1 year");	
				$survey_session = date('Y').'-'.(date('y')+1);
				$survey_end_date = date('Y-m-d', $next_survey_end).'-'.(date('y', $next_survey_end)+1); 
				
			}else{
				
				$next_survey_end = strtotime("$next_survey_end_date +0 year");
				$survey_session = (date('Y', $next_survey_end)-1).'-'.date('y', $next_survey_end);
				$survey_end_date = (date('Y-m-d', $next_survey_end)-1).'-'.date('y', $next_survey_end);
				
			}//end if
			
		}//end if($next_year == '1')
		
		$survey_session = 'Survey '. ltrim($survey_session,'Survey ');
		
		$temp_product = array(
			'name' => $survey_session,
			'desc' => '',
			'number' => '1000',
			'quantity' => 1, 
			'amount' => $price
		);

		$to_buy['products'][0] = $temp_product;

		$VAT_PERCENTAGE = 'VAT_PERCENTAGE';
		$vat_percentage = get_global_settings($VAT_PERCENTAGE); //Set from the Global Settings

		$vat_amount = (trim($vat_percentage['setting_value']) / 100) * $sub_total;
		$vat_amount = filter_price($vat_amount);
		
		$to_buy['tax_amount'] = $vat_amount;
		
		// enquire Paypal API for token
		$set_ec_return = $this->paypal_ec->set_ec($to_buy);
		
		if (isset($set_ec_return['ec_status']) && ($set_ec_return['ec_status'] === true)) {
			
			// redirect to Paypal
			$this->paypal_ec->redirect_to_paypal($set_ec_return['TOKEN']);
			// You could detect your visitor's browser and redirect to Paypal's mobile checkout
			// if they are on a mobile device. Just add a true as the last parameter. It defaults
			// to false
			// $this->paypal_ec->redirect_to_paypal( $set_ec_return['TOKEN'], true);
			
		} else {
			$this->_error($set_ec_return);
			
		}//end if (isset($set_ec_return['ec_status']) && ($set_ec_return['ec_status'] === true)) 
		
	}//end checkout()
	
	public function checkout_success($next_year = ''){
		
		$token = $_GET['token'];
		$payer_id = $_GET['PayerID'];
		
		$get_ec_return = $this->paypal_ec->get_ec($token);

		if(isset($get_ec_return['ec_status']) && ($get_ec_return['ec_status'] === true)) {
			// at this point, you have all of the data for the transaction.
			// you may want to save the data for future action. what's left to
			// do is to collect the money -- you do that by call DoExpressCheckoutPayment
			// via $this->paypal_ec->do_ec();
			//
			// I suggest to save all of the details of the transaction. You get all that
			// in $get_ec_return array
			$ec_details = array(
				'token' => $token, 
				'payer_id' => $payer_id, 
				'currency' => CURRENCY, 
				'amount' => $get_ec_return['PAYMENTREQUEST_0_AMT'], 
				'IPN_URL' => site_url('dashboard/ipn'), 
				// in case you want to log the IPN, and you
				// may have to in case of Pending transaction
				'type' => PAYMENT_METHOD);
				
			// DoExpressCheckoutPayment
			$do_ec_return = $this->paypal_ec->do_ec($ec_details);
			
			if (isset($do_ec_return['ec_status']) && ($do_ec_return['ec_status'] === true)) {
				
				// at this point, you have collected payment from your customer
				// you may want to process the order now.

				if($get_ec_return['ACK'] == 'Success' && $do_ec_return['ACK'] == 'Success'){
					
					$data['get_ec_return'] = $get_ec_return;
					$data['do_ec_return'] = $do_ec_return;
					$data['user_id'] = $this->session->id;
					$data['purchased_by_id'] = $this->session->id;
					$data['num_of_products'] = 1;
					
					////////////////////////////////////
					// Make entries into the database //
					// 1- Make entry into surveys
					// 2- Make entry into purchase history

					$pharmacy_id = $this->session->p_id;

					// Insert Survey
					$survey_id = $this->survey->add_new_survey($pharmacy_id,$next_year);

					$survey_session = $this->survey->get_pharmacy_survey_list($pharmacy_id, '', $survey_id);
					
					$VAT_PERCENTAGE = 'VAT_PERCENTAGE';
					$vat_percentage = get_global_settings($VAT_PERCENTAGE); //Set from the Global Settings

					// Insert Invoice
					$invoice_data['subtotal'] = $get_ec_return['ITEMAMT'];
					$invoice_data['vat_tax'] = $get_ec_return['TAXAMT'];
					
					$invoice_data['vat_percentage'] = trim($vat_percentage['setting_value']);
					$invoice_data['survey_session'] = $survey_session['survey_title'];

					$invoice_data['grand_total'] = $get_ec_return['AMT'];
					$invoice_data['transaction_id'] = $do_ec_return['PAYMENTINFO_0_TRANSACTIONID'];
					$invoice_data['payment_method'] = 'PAYPAL';
					
					// Get pharmacy Details
					$pharmacy_details = $this->pharmacy->get_pharmacy_details($pharmacy_id);

					if($survey_id){

						//Verify of the Affiliate code exist is still valid
			
						if(filter_string($pharmacy_details['affiliate_code']) != ''){
							
							$affiliate_code = $pharmacy_details['affiliate_code']; //This pharmacy was created by affiliate
							
							$verify_affiliate = $this->common->verify_affiliate($affiliate_code);
							
							if($verify_affiliate == "OK"){

								//Verify if affiliate is in recurring or not.						
		
								$affiliate_by_code_encode = $this->common->get_affiliate_by_code($affiliate_code);
								$affiliate_by_code = json_decode($affiliate_by_code_encode);
								
								$affiliate_product_encode = $this->common->get_affiliate_product($affiliate_by_code->id, 1);
								$affiliate_product = json_decode($affiliate_product_encode);
								
								if($affiliate_product->is_recurring == '1'){
										
										//Add Lead to record	
										$generate_lead = $this->common->generate_lead('SURVEY',$pharmacy_details['pharmacy_name'], $get_ec_return['AMT'], date('Y-m-d G:i:s'), $affiliate_code, 1, $this->input->ip_address(), $pharmacy_id);
										
								}//end if($affiliate_product->is_recurring == '1')
								
							}//end if($verify_affiliate == "OK")
								
						}//end if($this->session->sf_affiliate_code)

						// Send Pharmacy Registration Email
						
						$VAT_PERCENTAGE = 'VAT_PERCENTAGE';
						$vat_percentage = get_global_settings($VAT_PERCENTAGE); //Set from the Global Settings

						$vat_percentage = trim($vat_percentage['setting_value']);

						// Get price from GLOBAL SETTINGS
						$FROM_BUSINESS_NAME = 'FROM_BUSINESS_NAME';
						$from_business_name = get_global_settings($FROM_BUSINESS_NAME); //Set from the Global Settings
						$from_business_name = filter_string($from_business_name['setting_value']);
						
						// Get price from GLOBAL SETTINGS
						$FROM_ADDRESS = 'FROM_ADDRESS';
						$from_address = get_global_settings($FROM_ADDRESS); //Set from the Global Settings
						$from_address = filter_string($from_address['setting_value']);
						
						// Get price from GLOBAL SETTINGS
						$FROM_ADDRESS_2 = 'FROM_ADDRESS_2';
						$from_address_2 = get_global_settings($FROM_ADDRESS_2); //Set from the Global Settings
						$from_address_2 = filter_string($from_address_2['setting_value']);
						
						// Get price from GLOBAL SETTINGS
						$FROM_COUNTY = 'FROM_COUNTY';
						$from_county = get_global_settings($FROM_COUNTY); //Set from the Global Settings
						$from_county = filter_string($from_county['setting_value']);
						
						// Get price from GLOBAL SETTINGS
						$FROM_POSTCODE = 'FROM_POSTCODE';
						$from_postcode = get_global_settings($FROM_POSTCODE); //Set from the Global Settings
						$from_postcode = filter_string($from_postcode['setting_value']);
						
						// Get price from GLOBAL SETTINGS
						$FROM_TOWN = 'FROM_TOWN';
						$from_town = get_global_settings($FROM_TOWN); //Set from the Global Settings
						$from_town = filter_string($from_town['setting_value']);
						
						// Get price from GLOBAL SETTINGS
						$FROM_EMAIL = 'FROM_EMAIL';
						$from_email = get_global_settings($FROM_EMAIL); //Set from the Global Settings
						$from_email = filter_string($from_email['setting_value']);
						
						// Get price from GLOBAL SETTINGS
						$FROM_WEBSITE = 'FROM_WEBSITE';
						$from_website = get_global_settings($FROM_WEBSITE); //Set from the Global Settings
						$from_website = filter_string($from_website['setting_value']);
						
						// Get price from GLOBAL SETTINGS
						$FROM_PHONE = 'FROM_PHONE';
						$from_phone = get_global_settings($FROM_PHONE); //Set from the Global Settings
						$from_phone = filter_string($from_phone['setting_value']);
						
						// Get price from GLOBAL SETTINGS
						$SITE_LOGO = 'SITE_LOGO';
						$site_logo = get_global_settings($SITE_LOGO); //Set from the Global Settings
						$site_logo_src = filter_string($site_logo['setting_value']);

						// Insert invoice : along with new fields //
						$invoice_data['pharmacy_name'] = $pharmacy_details['pharmacy_name'];
						$invoice_data['pharmacy_owner_name'] = $pharmacy_details['owner_name'];
						$invoice_data['pharmacy_address'] = $pharmacy_details['address'];
						$invoice_data['pharmacy_address_2'] = $pharmacy_details['address_2'];
						$invoice_data['pharmacy_town'] = $pharmacy_details['town'];
						$invoice_data['pharmacy_county'] = $pharmacy_details['county'];
						$invoice_data['pharmacy_postcode'] = $pharmacy_details['postcode'];
						$invoice_data['from_name'] = $from_business_name;
						$invoice_data['from_address'] = $from_address;
						$invoice_data['from_address_2'] = $from_address_2;
						$invoice_data['from_town'] = $from_town;
						$invoice_data['from_county'] = $from_county;
						$invoice_data['from_postcode'] = $from_postcode;
						$invoice_data['from_phone'] = $from_phone;
						$invoice_data['from_email'] = $from_email;
						$invoice_data['from_website'] = $from_website;

						$invoice_generated = $this->survey->save_invoice($pharmacy_id, $survey_id, $invoice_data);

						$search_arr = array(
							'[REF_NO]', '[DATE]', '[TO]', '[FULL_NAME]',
							'[ADDRESS]', '[ADDRESS_2]', '[COUNTY]',
							'[POSTCODE]', '[TOWN]', '[SITE_LOGO]',
							'FROM_BUSINESS_NAME', 'FROM_ADDRESS',
							'FROM_ADDRESS_2', 'FROM_COUNTY',
							'FROM_POSTCODE', 'FROM_TOWN',
							'FROM_PHONE',
							'FROM_EMAIL', 'FROM_WEBSITE',
							'[SURVEY_PRICE]', '[SUB_TOTAL]',
							'[VAT]', '[VAT_AMOUNT]', '[GRAND_TOTAL]', '[SURVEY_SESSION]', '[PHARMACY_NAME]'
						);

						$survey_session = $this->survey->get_pharmacy_survey_list($pharmacy_id, '', $survey_id);

						$replace_arr = array(

							$invoice_generated['invoice_no'], date('d/m/Y'), $pharmacy_details['pharmacy_name'], $pharmacy_details['owner_name'], 
							$pharmacy_details['address'], $pharmacy_details['address_2'], $pharmacy_details['county'], 
							$pharmacy_details['postcode'], $pharmacy_details['town'], $site_logo_src,
							$from_business_name, $from_address,
							$from_address_2, $from_county, 
							$from_postcode, $from_town,
							$from_phone,
							$from_email, $from_website, 
							'£'.$invoice_data['subtotal'], '£'.$invoice_data['subtotal'],
							$vat_percentage.'%', '£'.$invoice_data['vat_tax'], '£'.$invoice_data['grand_total'], $survey_session['survey_title'],
							$pharmacy_details['pharmacy_name']
							
						);
						
						$this->load->model('email_mod','email_template');
						
						$email_body_arr = $this->email_template->get_email_template(7);
						
						$email_subject = $email_body_arr['email_subject'];
						$email_body = $email_body_arr['email_body'];
						
						$email_body = str_replace($search_arr,$replace_arr,$email_body);
						
						// Get FROM_EMAIL_TXT from GLOBAL SETTINGS
						$FROM_EMAIL_TXT = 'FROM_EMAIL_TXT';
						$from_email_txt = get_global_settings($FROM_EMAIL_TXT); //Set from the Global Settings
						$email_from_txt = filter_string($from_email_txt['setting_value']);
						
						//Preparing Sending Email
						$SES_SENDER = 'SES_SENDER';
						$ses_sender = get_global_settings($SES_SENDER);
						$ses_sender = trim($ses_sender['setting_value']);

						$to_email_address = trim($pharmacy_details['email_address']); //'twister787@gmail.com';
						                                                      
						$email_subject = filter_string($email_subject); //'Amazon SES test (SMTP interface accessed using PHP)';
						$email_body = filter_string($email_body); //'This email was sent through the Amazon SES SMTP interface by using PHP.';

						## 
						## START: Preparing Attachment for Invoice
						##
						$data['invoice_data'] = $invoice_generated;
						
						$this->stencil->layout('modal_layout');
						$html = $this->load->view('dashboard/pdf/download_invoice', $data, true);
						
						//echo $html; exit;
						
						$this->load->library('pdf');
						$pdf = $this->pdf->load();
						
						$file_name = 'invoice-'.$invoice_generated['invoice_no'].'.pdf';
			
						$pdf->AddPageByArray(array(
									'orientation' => 'P',
									'mgl' => '20',
									'mgr' => '20',
									'mgt' => '10',
									'mgb' => '10',
									'mgh' => '0',
									'mgf' => '0', 
								)
							);
						//header('Content-Type: application/pdf');
						//$pdf->AddPage('P'); // L - P
						$pdf->WriteHTML($html); // write the HTML into the PDF
						$pdf->Output('./assets/invoice_tmp/'.$file_name,'F'); 
						
						## 
						## END: Preparing Attachment for Invoice
						##
		
						$attachment_arr[] = 
							array('name' => $file_name,
									'mime' => 'application/pdf',
									'filepath' => './assets/invoice_tmp/'.$file_name
						);
						
						$send_email = send_email($to_email_address, $ses_sender, $email_from_txt, $email_subject, $email_body, $attachment_arr);
						
						////////////////////////////////////

						// Unset session buy_survey_form
						$this->session->unset_userdata('buy_survey_form');

						if(file_exists('./assets/invoice_tmp/'.$file_name))
							unlink('./assets/invoice_tmp/'.$file_name);
														
					   // survey purchased email send to admin
						$search_arr = array('[PHARMACY_NAME]',  '[SURVEY_TITLE]', '[PURCHASED_DATE]');
		
						$replace_arr = array($pharmacy_details['pharmacy_name'], $survey_session['survey_title'], date('d/m/Y'));                
									
						$email_body_arr = $this->email_template->get_email_template(8);
						
						$email_subject = "RENEW: ".filter_string($email_body_arr['email_subject']);
						$email_body = filter_string($email_body_arr['email_body']);
						
						$email_body = str_replace($search_arr,$replace_arr,$email_body);
						$email_body = filter_string($email_body); //'This email was sent through the Amazon SES SMTP interface by using PHP.';

						$admin_send_email = send_email($ses_sender, $ses_sender, $email_from_txt, $email_subject, $email_body);
						
						// end survey purchased email 

						$this->session->set_flashdata('ok_message', 'You have successfully purchased a new survey. Please start your survey to continue.');
						redirect(SURL.'dashboard');

					} else {
						
						//echo "in error"; exit;
						$data = array('buy_survey_form' => $this->input->post());
						$this->session->set_userdata($data);

						$this->session->set_flashdata('err_message', 'Something went wrong, please try again later.');
						// redirect(SURL.'register');
						redirect(SURL.'dashboard');
						
					} // End else - if($survey_id)
					
				} else {
					echo "OOPS";exit;	
				}//end if($get_ec_return['ACK'] == 'Success' && $do_ec_return['ACK'] = 'Success')
				
			} else {
				
				$this->_error($do_ec_return);
				
			}
		} else {
			$this->_error($get_ec_return);
		}
	
	}//end checkout_success()
	public function ipn(){
		$logfile = 'ipnlog/' . uniqid() . '.html';
		$logdata = "<pre>\r\n" . print_r($_POST, true) . '</pre>';
		file_put_contents($logfile, $logdata);
		
	}//end ipn()

	public function _error($ecd){

		/*$erro_txt .= "<h3>error at Express Checkout<h3>";
		$erro_txt .= "<pre>" . print_r($ecd, true) . "</pre>";
		
		$data['err_msg'] = $ecd;*/
		
	    $err_msg  = $ecd['L_ERRORCODE0'].': '.$ecd['L_SHORTMESSAGE0'].': '.$ecd['L_LONGMESSAGE0'];
		// Paypal Error
		$this->session->set_flashdata('err_message', $err_msg);
		redirect(SURL.'dashboard');

		//$this->stencil->layout('frontend_template_subpage'); // frontend template
		//$this->stencil->paint('errors/paypal_failed',$data);

	}//end _error($ecd)

	// Temp function to test AMAZON SES service
	public function ses(){

		//echo phpinfo();
		//exit;

		$this->load->library('email');

		// Replace sender@example.com with your "From" address. 
		// This address must be verified with Amazon SES.
		$SENDER = 'info@surveyfocus.co.uk';

		// Replace recipient@example.com with a "To" address. If your account 
		// is still in the sandbox, this address must be verified.
		$RECIPIENT = 'twister787@gmail.com';
		                                                      
		// Replace smtp_username with your Amazon SES SMTP user name.
		$USERNAME = 'AKIAJJDSTFURI6LQWNPQ';

		// Replace smtp_password with your Amazon SES SMTP password.
		$PASSWORD = 'As/vj5Eh8mLt7dYHnJWnK/6Fk5UwUGhD9qtA300O9rPL';

		// If you're using Amazon SES in a region other than US West (Oregon), 
		// replace email-smtp.us-west-2.amazonaws.com with the Amazon SES SMTP  
		// endpoint in the appropriate region.
		
		$HOST = 'email-smtp.eu-west-1.amazonaws.com'; 
		// $HOST = 'ses-smtp-user.20170116-163508';
		// ssl://email-smtp.us-east-1.amazonaws.com
		
		// The port you will connect to on the Amazon SES SMTP endpoint.
		$PORT = '587';

		// Other message information                                               
		$SUBJECT ='Amazon SES test (SMTP interface accessed using PHP)';
		$BODY = 'This email was sent through the Amazon SES SMTP interface by using PHP.';

		////////////////////////////////////////
		////// CI SMTP Mail configuration //////

		// recipient email address
		// $to = 'iwebpros@gmail.com';
		$to = 'twister787@gmail.com';

		$config = array(
		    'protocol' => 'smtp',
		    'smtp_host' => $HOST,
		    'smtp_user' => $USERNAME,
        	'starttls' => TRUE,
		    'smtp_pass' => $PASSWORD,
		    'smtp_port' => $PORT,
		    'smtp_crypto' => 'tls', //can be 'ssl' or 'tls' for example
		    'mailtype' => 'html'
		);

		$this->email->initialize($config);
		// $this->email->print_debugger();

		$this->email->from('info@demotd.com', 'Survey Focus');
		$this->email->to($to, 'Usman Anwer');

		$this->email->subject($SUBJECT);
		$this->email->message($BODY);        
		$this->email->set_newline("\r\n");
		$sent_status = $this->email->send();

		print_this($sent_status);
		exit;

	} // End => public function ses()
	
	public function download_survey(){
		
		$get_questionnnair_arr = $this->survey->get_questionnaire_list($this->session->pharmacy_type);
		$data['questionnnair_arr'] = $get_questionnnair_arr;
		//print_this($get_questionnnair_arr); exit;

		$html = $this->load->view('dashboard/pdf/download_empty_survey', $data, true);
		
		$this->load->library('pdf');

		$pdf = $this->pdf->load();

		
		$file_name = 'survey_single_form.pdf';
		
		$address_2 = ($this->session->address_2) ? $this->session->address_2.', ': '';
		$phramcy_address = $this->session->address.', '.$address_2.$this->session->town.', '.$this->session->county.', '.$this->session->postcode;

		$pdf->SetHTMLFooter('<table width="100%" style="vertical-align: bottom; font-family: Arial, Helvetica, sans-serif; font-size: 9pt; color: #000000;"><tr>
<td width="66%">'.$this->session->pharmacy_name.', '.$phramcy_address.'<span></span></td>
<td width="33%" style="text-align: right; ">Page {PAGENO} of {nbpg}</td>
</tr></table>'); // Add a footer for good measure

		$pdf->AddPageByArray(array(
					'orientation' => 'P',
					'mgl' => '20',
					'mgr' => '20',
					'mgt' => '5',
					'mgb' => '20',
					'mgh' => '0',
					'mgf' => '0', 
				)
			);

		$pdf->WriteHTML($html); // write the HTML into the PDF

		$pdf->Output($file_name,'D'); // save to file because we can

	}//end download_survey()
	
	public function buy_survey_p(){

		//Survey is not yet started means we have to start the survey of the current one.
		$current_date_2 = date('m/d');	
		$survey_start_date_2 = date('Y-m-d');
		
		$SURVEY_END_MONTH = 'SURVEY_END_MONTH';
		$survey_end_global_value = get_global_settings($SURVEY_END_MONTH); //Set from the Global Settings
		$next_survey_end_date_2 = filter_string($survey_end_global_value['setting_value']);
		
		$next_survey_end_2 = strtotime("$next_survey_end_date +1 year");	
		$survey_session_2 = (date('Y', $next_survey_end_2)-1).'-'.date('y', $next_survey_end_2);
		
		$data['survey_session_2'] = $survey_session_2;

		// Get price from GLOBAL SETTINGS
		$PHARMACY_SURVEY_PRICE = 'PHARMACY_SURVEY_PRICE';
		$pharmacy_survey_price = get_global_settings($PHARMACY_SURVEY_PRICE); //Set from the Global Settings
		$pharmacy_survey_price = filter_string($pharmacy_survey_price['setting_value']);
		
		$price = $pharmacy_survey_price;
		$sub_total = $price;

		// Get VAT % Settings
		$VAT_PERCENTAGE = 'VAT_PERCENTAGE';
		$vat_percentage = get_global_settings($VAT_PERCENTAGE); //Set from the Global Settings

		$vat_amount = (trim($vat_percentage['setting_value']) / 100) * $sub_total;
		$vat_amount = filter_price($vat_amount);

		$data['pharmacy_survey_price'] = $pharmacy_survey_price;
		$data['sub_total'] = $sub_total;
		$data['vat'] = trim($vat_percentage['setting_value']);
		$data['vat_amount'] = $vat_amount;
		$data['grand_total'] = $sub_total + $vat_amount;
		

		$this->stencil->layout('modal_layout');
		$this->stencil->paint('dashboard/buy_new_survey_p',$data);
		
	}//end buy_survey_p()
	
	public function  test(){

		$text_to_replace = SURL;

		$this->load->library('GoogleUrlApi');
		$googer = new GoogleURLAPI($this->google_short_url_key);
		echo $short_url = $googer->shorten($text_to_replace);
		
	}//end test()

} // End => Ci => Class Dashboard