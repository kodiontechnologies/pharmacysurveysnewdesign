<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('Cms_mod', 'cms');

		//Sets the variable $header_contents to use the slice head (/views/slices/header_contents.php)
		$this->stencil->slice('header_contents');

		//Sets the variable $header_scripts to use the slice head (/views/slices/header_scripts.php)
		$this->stencil->slice('header_scripts');
		
		//Sets the variable $footer_contents to use the slice head (/views/slices/footer_contents.php)
		$this->stencil->slice('footer_contents');
		
		//Sets the variable $footer_scripts to use the slice head (/views/slices/footer_scripts.php)
		$this->stencil->slice('footer_scripts');

		$this->stencil->js('kod_scripts/form_validation.js');
		$this->stencil->js('kod_scripts/form_validation/bootstrap_validator/dist/formValidation.min.js');
		$this->stencil->js('kod_scripts/form_validation/bootstrap_validator/dist/bootstrap.min.js');
	}

	// Start - function index() : Main Homepage view
 	public function index(){
		
		exit('303');

	} // End - public function index()

	// Start => public function about_us()
	public function about_us(){

		$cms_page = $this->cms->get_cms_page('about-us');
		$data['cms_page'] = $cms_page;

		if(filter_string($cms_page['page_title'])){
			
			//set title
			$page_title = $cms_page['meta_title'];
			$this->stencil->title($page_title);	
			
			//Sets the Meta data
			$this->stencil->meta(array(
				'description' => $cms_page['meta_description'],
				'keywords' => $cms_page['meta_keywords']
			));

		}else{
			
			//set title
			$page_title = DEFAULT_TITLE;
			$this->stencil->title($page_title);	
			
			//Sets the Meta data
			$this->stencil->meta(array(
				'description' => DEFAULT_META_DESCRIPTION,
				'keywords' => DEFAULT_META_KEYWORDS
			));

		}//end if(filter_string($homepage_cms['page_title']))


		// Page Heading
		$page_heading = 'Pharamcy Survey';
		$data['page_heading'] = $page_heading;
		
		$this->stencil->layout('page');
		$this->stencil->paint('pages/about_us', $data);

	} // End => public function about_us()
	
	public function terms_and_conditions(){

		//set title
		$page_title = DEFAULT_TITLE;
		$this->stencil->title($page_title);	
		
		//Sets the Meta data
		$this->stencil->meta(array(
			'description' => DEFAULT_META_DESCRIPTION,
			'keywords' => DEFAULT_META_KEYWORDS,
			'meta_title' => DEFAULT_TITLE
		));

		// Page Heading
		$page_heading = 'Pharamcy Survey';
		$data['page_heading'] = $page_heading;
		
		// Get page CMS page contents
		$data['cms_page'] = $this->cms->get_cms_page('terms-and-conditions');
		
		
		$this->stencil->layout('page');
		$this->stencil->paint('pages/cms_page', $data);

	} // End => public function terms_and_conditions()
	
	public function privacy_policy(){

		//set title
		$page_title = DEFAULT_TITLE;
		$this->stencil->title($page_title);	
		
		//Sets the Meta data
		$this->stencil->meta(array(
			'description' => DEFAULT_META_DESCRIPTION,
			'keywords' => DEFAULT_META_KEYWORDS,
			'meta_title' => DEFAULT_TITLE
		));

		// Page Heading
		$page_heading = 'Pharamcy Survey';
		$data['page_heading'] = $page_heading;
		
		// Get page CMS page contents
		$data['cms_page'] = $this->cms->get_cms_page('privacy-policy');
		
		$this->stencil->layout('page');
		$this->stencil->paint('pages/cms_page', $data);

	} // End => public function privacy_policy()
	
	// Start => public function contact_us()
	public function contact_us(){
		
		$cms_page = $this->cms->get_cms_page('contact-us');
		$data['cms_page'] = $cms_page;

		if(filter_string($cms_page['page_title'])){
			
			//set title
			$page_title = $cms_page['meta_title'];
			$this->stencil->title($page_title);	
			
			//Sets the Meta data
			$this->stencil->meta(array(
				'description' => $cms_page['meta_description'],
				'keywords' => $cms_page['meta_keywords']
			));

		}else{
			
			//set title
			$page_title = DEFAULT_TITLE;
			$this->stencil->title($page_title);	
			
			//Sets the Meta data
			$this->stencil->meta(array(
				'description' => DEFAULT_META_DESCRIPTION,
				'keywords' => DEFAULT_META_KEYWORDS
			));

		}//end if(filter_string($homepage_cms['page_title']))

		// Page Heading
		$page_heading = 'Pharamcy Survey';
		$data['page_heading'] = $page_heading;
		
		$this->stencil->layout('page');
		$this->stencil->paint('pages/contact_us', $data);

	} // End => public function contact_us()

	// Start => public function submit_contact_us_form()
	public function submit_contact_us_form(){

		// Verify if the POST is set
		if(!$this->input->post()) exit;

		// Extract POST
		extract($this->input->post());
		
		
	    $this->form_validation->set_rules('first_name', 'First Name', 'trim|required');
		$this->form_validation->set_rules('last_name', 'Last Name', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('message', 'Message', 'trim|required');

		if($this->form_validation->run() == FALSE && $this->input->post('contact_us_btn')){

			// PHP Error 
			$this->session->set_flashdata('err_message', validation_errors());
			redirect(SURL.'pages/contact-us');

		} else {

		// Set email template and send email
		$search_arr = array('[USER_EMAIL]','[COMMENTS]','[SITE_URL]','[PHONE_NO]');
		$replace_arr = array(filter_string($email),filter_string($message),SURL, filter_string($phone)); 

		// Load Email_mod
		$this->load->model('Email_mod', 'email_template');

		$email_body_arr = $this->email_template->get_email_template(5);
		$email_subject = $email_body_arr['email_subject'];
		
		$email_body = $email_body_arr['email_body'];
		$email_body = str_replace($search_arr,$replace_arr,$email_body);

		$CONTACTUS_FORM_EMAIL = 'CONTACTUS_FORM_EMAIL';
		$send_to_email = get_global_settings($CONTACTUS_FORM_EMAIL);
		$send_to_email = trim($send_to_email['setting_value']);
		
		$NO_REPLY_EMAIL = 'NO_REPLY_EMAIL';
		$no_reply_email = get_global_settings($NO_REPLY_EMAIL);
		$no_reply_email = trim($no_reply_email['setting_value']);


		// sender
		$SES_SENDER = 'SES_SENDER';
		$ses_sender = get_global_settings($SES_SENDER);
		$ses_sender = trim($ses_sender['setting_value']);

		// Other message information                                               
		$email_subject = filter_string($email_subject); //'Amazon SES test (SMTP interface accessed using PHP)';
		$email_body = filter_string($email_body); //'This email was sent through the Amazon SES SMTP interface by using PHP.';

		// Get FROM_EMAIL_TXT from GLOBAL SETTINGS
		$FROM_EMAIL_TXT = 'FROM_EMAIL_TXT';
		$from_email_txt = get_global_settings($FROM_EMAIL_TXT); //Set from the Global Settings
		$email_from_txt = filter_string($from_email_txt['setting_value']);

		$email_address = filter_string($email_address); //'twister787@gmail.com';
		$email_subject = filter_string($email_subject); //'Amazon SES test (SMTP interface accessed using PHP)';
		$email_body = filter_string($email_body); //'This email was sent through the Amazon SES SMTP interface by using PHP.';
		
		$search_arr = array('[BUSINESS_NAME]');
		$replace_arr = array(filter_name($business_name));
		
		$email_body = str_replace($search_arr, $replace_arr, $email_body);
		
		$send_email = send_email($send_to_email, $no_reply_email, $email_from_txt, $email_subject, $email_body);

		// Setup the success message
		$this->session->set_flashdata('ok_message', 'Your comments sent successfully for admin review.');

		// Redirect to Contact Us Page
		redirect(base_url().'pages/contact-us');
		
	 }// end else

	} // End => public function submit_contact_us_form()

	// Start => public function faqs()
	public function faqs(){
		
		$cms_page = $this->cms->get_cms_page('faqs');
		$data['cms_page'] = $cms_page;

		if(filter_string($cms_page['page_title'])){
			
			//set title
			$page_title = $cms_page['meta_title'];
			$this->stencil->title($page_title);	
			
			//Sets the Meta data
			$this->stencil->meta(array(
				'description' => $cms_page['meta_description'],
				'keywords' => $cms_page['meta_keywords']
			));

		}else{
			
			//set title
			$page_title = DEFAULT_TITLE;
			$this->stencil->title($page_title);	
			
			//Sets the Meta data
			$this->stencil->meta(array(
				'description' => DEFAULT_META_DESCRIPTION,
				'keywords' => DEFAULT_META_KEYWORDS
			));

		}//end if(filter_string($homepage_cms['page_title']))

		// Page Heading
		$page_heading = 'Pharamcy Survey';
		$data['page_heading'] = $page_heading;
		
		$this->stencil->layout('page');
		$this->stencil->paint('pages/faqs', $data);

	} // End => public function faqs()

	public function cookies(){

		//set title
		$page_title = DEFAULT_TITLE;
		$this->stencil->title($page_title);	
		
		//Sets the Meta data
		$this->stencil->meta(array(
			'description' => DEFAULT_META_DESCRIPTION,
			'keywords' => DEFAULT_META_KEYWORDS,
			'meta_title' => DEFAULT_TITLE
		));

		// Page Heading
		$page_heading = 'Cookies';
		$data['page_heading'] = $page_heading;
		
		// Get page CMS page contents
		$data['cms_page'] = $this->cms->get_cms_page('cookies');
		
		
		$this->stencil->layout('page');
		$this->stencil->paint('pages/cms_page', $data);

	} // End => public function terms_and_conditions()
	

	// Start => public function faqs()
	public function survey_completed(){
		

		// Get page CMS page contents
		$data['cms_page'] = $this->cms->get_cms_page('survey-completed');

		//set title
		$page_title = DEFAULT_TITLE;
		$this->stencil->title($page_title);	
		
		//Sets the Meta data
		$this->stencil->meta(array(
			'description' => DEFAULT_META_DESCRIPTION,
			'keywords' => DEFAULT_META_KEYWORDS,
			'meta_title' => DEFAULT_TITLE
		));

		// Page Heading
		$page_heading = 'Pharamcy Survey';
		$data['page_heading'] = $page_heading;
		
		$this->stencil->layout('page_no_header_footer');
		$this->stencil->paint('pages/survey_completed',$data);

	} // End => public function faqs()

	// Start => public function thank_you()
	public function thank_you(){

		// Verify if the request is coming directly from the Register sucess
		// 
		if($this->session->flashdata('ok_message')){

			//set title
			$page_title = DEFAULT_TITLE;
			$this->stencil->title($page_title);	
			
			//Sets the Meta data
			$this->stencil->meta(array(
				'description' => DEFAULT_META_DESCRIPTION,
				'keywords' => DEFAULT_META_KEYWORDS,
				'meta_title' => DEFAULT_TITLE
			));

			// Page Heading
			$page_heading = 'Pro CD Register';
			$data['page_heading'] = $page_heading;
			
			// Get page CMS page contents
			$data['cms_page'] = $this->cms->get_cms_page('thank-you');

			$this->stencil->layout('page');
			$this->stencil->paint('pages/cms_page', $data);

		} else {

			// Redirect to home : If user is trying to reload the thank-you page
			redirect(base_url());

		} // if($this->session->flashdata('ok_message'))

	} // End => public function thank_you()

	// Start => public function pricing()
	public function pricing(){

		//set title
		$page_title = DEFAULT_TITLE;
		$this->stencil->title($page_title);	
		
		//Sets the Meta data
		$this->stencil->meta(array(
			'description' => DEFAULT_META_DESCRIPTION,
			'keywords' => DEFAULT_META_KEYWORDS,
			'meta_title' => DEFAULT_TITLE
		));

		// Page Heading
		$page_heading = 'Pharamcy Survey';
		$data['page_heading'] = $page_heading;
		
		// Get page CMS page contents
		$data['cms_page'] = $this->cms->get_cms_page('pricing');
		
		$this->stencil->layout('page');
		$this->stencil->paint('pages/pricing', $data);

	} // End => public function pricing()


   // Start - page_not_found_404()
	public function page_not_found_404(){
		//set title
		$page_title = DEFAULT_TITLE;
		$this->stencil->title($page_title);	
		
		//Sets the Meta data
		$this->stencil->meta(array(
			'description' => DEFAULT_META_DESCRIPTION,
			'keywords' => DEFAULT_META_KEYWORDS,
			'meta_title' => DEFAULT_TITLE
		));

		// Page Heading
		$page_heading = 'Pharamcy Survey';
		$data['page_heading'] = $page_heading;
		
		// Get page CMS page contents
		$data['cms_page'] = $this->cms->get_cms_page('page-not-found-404');
		
		$this->stencil->layout('page');
		$this->stencil->paint('pages/cms_page', $data);
	} // End - page_not_found_404()
	
	public function download_ccpq_form($online_pharmacy_survey){
		
		$this->load->model('Survey_mod', 'survey');
		
		$pharmacy_type = ($online_pharmacy_survey == '1') ? 'O' : 'OF';
		$data['pharmacy_type'] = $pharmacy_type;
		
		$get_questionnnair_arr = $this->survey->get_questionnaire_list($pharmacy_type);
		$data['questionnnair_arr'] = $get_questionnnair_arr;
		//print_this($get_questionnnair_arr); exit;

		$html = $this->load->view('dashboard/pdf/download_empty_survey_public', $data, true);
		
		$this->load->library('pdf');

		$pdf = $this->pdf->load();

		
		$file_name = 'survey_single_form.pdf';
		
		//$address_2 = ($this->session->address_2) ? $this->session->address_2.', ': '';
		//$phramcy_address = $this->session->address.', '.$address_2.$this->session->town.', '.$this->session->county.', '.$this->session->postcode;

		$pdf->SetHTMLFooter('<table width="100%" style="vertical-align: bottom; font-family: Arial, Helvetica, sans-serif; font-size: 9pt; color: #000000;"><tr>
<td width="66%"><span></span></td>
<td width="33%" style="text-align: right; ">Page {PAGENO} of {nbpg}</td>
</tr></table>'); // Add a footer for good measure

		$pdf->AddPageByArray(array(
					'orientation' => 'P',
					'mgl' => '20',
					'mgr' => '20',
					'mgt' => '5',
					'mgb' => '20',
					'mgh' => '0',
					'mgf' => '0', 
				)
			);

		$pdf->WriteHTML($html); // write the HTML into the PDF

		$pdf->Output($file_name,'D'); // save to file because we can

	
	}//end download_ccpq_form($online_pharmacy_survey)
} // End - CI Class
