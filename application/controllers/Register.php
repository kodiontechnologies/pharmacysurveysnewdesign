<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('Pharmacy_mod', 'pharmacy');
		$this->load->model('Register_mod', 'register');
		$this->load->model('Common_mod', 'common');
		$this->load->model('Cms_mod', 'cms');
		$this->load->model('Survey_mod', 'survey');
		
		if($this->session->p_id)
		 	redirect(base_url().'dashboard');

		// Get GLOBAL SETTINGS for PAYPAL_API_USERNAME, PAYPAL_API_SIGNATURE, PAYPAL_API_PASSWORD, PAYPAL_SANDBOX_STATUS

		// paypal username
		$PAYPAL_API_USERNAME = 'PAYPAL_API_USERNAME';
		$paypal_api_username = get_global_settings($PAYPAL_API_USERNAME); //Set from the Global Settings
		$paypal_api_username = filter_string($paypal_api_username['setting_value']);

		// paypal ai signatures
		$PAYPAL_API_SIGNATURE = 'PAYPAL_API_SIGNATURE';
		$paypal_api_signature = get_global_settings($PAYPAL_API_SIGNATURE); //Set from the Global Settings
		$paypal_api_signature = filter_string($paypal_api_signature['setting_value']);

		// paypal password
		$PAYPAL_API_PASSWORD = 'PAYPAL_API_PASSWORD';
		$paypal_api_password = get_global_settings($PAYPAL_API_PASSWORD); //Set from the Global Settings
		$paypal_api_password = filter_string($paypal_api_password['setting_value']);

		// paypal sandbox status
		$PAYPAL_SANDBOX_STATUS = 'PAYPAL_SANDBOX_STATUS';
		$paypal_sandbox_status = get_global_settings($PAYPAL_SANDBOX_STATUS); //Set from the Global Settings
		$paypal_sandbox_status = filter_string($paypal_sandbox_status['setting_value']);

		// Applying Paypal Settings
		$this->paypal_details = array(
			// you can get this from your Paypal account, or from your
			// test accounts in Sandbox
			'API_username' => $paypal_api_username, 
			'API_signature' => $paypal_api_signature, 
			'API_password' => $paypal_api_password,
			'sandbox_status' => ($paypal_sandbox_status == 0) ? false : true
			// Paypal_ec defaults sandbox status to true
			// Change to false if you want to go live and
			// update the API credentials above
			// 'sandbox_status' => false,
		);
		
		$this->load->library('paypal_ec', $this->paypal_details);
		
		//Sets the variable $header_contents to use the slice head (/views/slices/header_contents.php)
		$this->stencil->slice('header_contents');

		//Sets the variable $header_scripts to use the slice head (/views/slices/header_scripts.php)
		$this->stencil->slice('header_scripts');
		
		//Sets the variable $footer_contents to use the slice head (/views/slices/footer_contents.php)
		$this->stencil->slice('footer_contents');
		
		//Sets the variable $footer_scripts to use the slice head (/views/slices/footer_scripts.php)
		$this->stencil->slice('footer_scripts');

		$this->stencil->js('kod_scripts/form_validation.js');
		$this->stencil->js('kod_scripts/form_validation/bootstrap_validator/dist/formValidation.min.js');
		$this->stencil->js('kod_scripts/form_validation/bootstrap_validator/dist/bootstrap.min.js');

	}

	// Start - function index() : Main Homepage view
 	public function index(){

		$cms_page = $this->cms->get_cms_page('register');
		$data['cms_page'] = $cms_page;

		if(filter_string($cms_page['page_title'])){
			
			//set title
			$page_title = $cms_page['meta_title'];
			$this->stencil->title($page_title);	
			
			//Sets the Meta data
			$this->stencil->meta(array(
				'description' => $cms_page['meta_description'],
				'keywords' => $cms_page['meta_keywords']
			));

		}else{
			
			//set title
			$page_title = DEFAULT_TITLE;
			$this->stencil->title($page_title);	
			
			//Sets the Meta data
			$this->stencil->meta(array(
				'description' => DEFAULT_META_DESCRIPTION,
				'keywords' => DEFAULT_META_KEYWORDS
			));

		}//end if(filter_string($homepage_cms['page_title']))

		// Page Heading
		$page_heading = 'Pharamcy Survey Register';
		$data['page_heading'] = $page_heading;

		// Get prices for checkout page
		// 

		//Survey is not yet started means we have to start the survey of the current one.
		$current_date = date('m/d');	
		$survey_start_date = date('Y-m-d');
		$data['survey_start_date'] = $survey_start_date;
		
		$SURVEY_END_MONTH = 'SURVEY_END_MONTH';
		$survey_end_global_value = get_global_settings($SURVEY_END_MONTH); //Set from the Global Settings
		$next_survey_end_date = filter_string($survey_end_global_value['setting_value']);
		
		if(strtotime($current_date) > strtotime($next_survey_end_date)){
			
			$next_survey_end = strtotime("$next_survey_end_date +1 year");	
			$survey_session = date('Y').'-'.(date('y')+1);
			$survey_end_date = date('Y-m-d', $next_survey_end).'-'.(date('y', $next_survey_end)+1); 
			
		}else{
			
			$next_survey_end = strtotime("$next_survey_end_date +0 year");
			$survey_session = (date('Y', $next_survey_end)-1).'-'.date('y', $next_survey_end);
			$survey_end_date = (date('Y-m-d', $next_survey_end)-1).'-'.date('y', $next_survey_end);
			
		}//end if
		
		$survey_session = 'Survey '.$survey_session;

		// Get price from GLOBAL SETTINGS
		$PHARMACY_SURVEY_PRICE = 'PHARMACY_SURVEY_PRICE';
		$pharmacy_survey_price = get_global_settings($PHARMACY_SURVEY_PRICE); //Set from the Global Settings
		$pharmacy_survey_price = filter_string($pharmacy_survey_price['setting_value']);
		
		$price = $pharmacy_survey_price;
		$sub_total = $price;

		// Get VAT % Settings
		$VAT_PERCENTAGE = 'VAT_PERCENTAGE';
		$vat_percentage = get_global_settings($VAT_PERCENTAGE); //Set from the Global Settings

		$vat_amount = (trim($vat_percentage['setting_value']) / 100) * $sub_total;
		$vat_amount = filter_price($vat_amount);

		$data['survey_session'] = $survey_session;
		$data['pharmacy_survey_price'] = $pharmacy_survey_price;
		$data['sub_total'] = $sub_total;
		$data['vat'] = trim($vat_percentage['setting_value']);
		$data['vat_amount'] = $vat_amount;
		$data['grand_total'] = $sub_total + $vat_amount;

		//$this->stencil->css('captcha.css');
		$this->stencil->js('kod_scripts/register.js');

		$this->stencil->css('jquery.fancybox.css');
		$this->stencil->js('jquery.fancybox.js');
		
		$this->stencil->layout('page');
		$this->stencil->paint('register/register',$data);

	} // End - public function index()
	
	// Add Business signup process
	public function register_process(){
		
	
		if(!$this->input->post()) redirect(base_url());
		
		//extract($this->session->registration_form);

		extract($this->input->post());

		$this->form_validation->set_rules('pharmacy_name', 'Business Name', 'trim|required');
		$this->form_validation->set_rules('contact_no', 'Business Phone Number', 'trim|required');
		$this->form_validation->set_rules('address', 'Address', 'trim|required');
		$this->form_validation->set_rules('town', 'Business Town', 'trim|required');
		$this->form_validation->set_rules('postcode', 'Postcode', 'trim|required');
		
		$this->form_validation->set_rules('owner_name', 'Owner Name', 'trim|required');
		$this->form_validation->set_rules('email_address', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');
		
		if($payment_method_radio == 'credit_card'){
		
			$this->form_validation->set_rules('card', 'Card Number', 'trim|required');
			$this->form_validation->set_rules('expMonth', 'Expiry Month', 'trim|required');
			$this->form_validation->set_rules('expYear', 'Expiry Year', 'trim|required');
			$this->form_validation->set_rules('cvc', 'CVC Code', 'trim|required');
		}//end if($payment_method_radio == 'credit_card')

		if($this->form_validation->run() == FALSE){

			// PHP Error 
			$this->session->set_flashdata('err_message', validation_errors());
			redirect(base_url().'register');

		} else {

			$data = array('registration_form' => $this->input->post());
			$this->session->set_userdata($data);

			$verify_pharmacy_exist = $this->register->verify_if_pharmacy_already_exist($email_address);

			if($verify_pharmacy_exist){

				$this->session->set_flashdata('err_message', 'The email you have entered already exist, please try another one.');
				redirect(SURL.'register');
				
			} else {
				
				// Verify which payment method is selected [ 'Paypal | Stripe' ]
				if($payment_method_radio && $payment_method_radio == 'credit_card'){
					
					// ------------------ //
					// ----- Stripe ----- //
					// ------------------ //
		
					require(APPPATH.'libraries/stripe/init.php');
		
					// Get stripe APi credentials from GLOBAL SETTINGS
		
					$STRIPE_SECRET_KEY = 'STRIPE_SECRET_KEY';
					$stripe_secret_key = get_global_settings($STRIPE_SECRET_KEY); //Set from the Global Settings
					$stripe_secret_key = filter_string($stripe_secret_key['setting_value']);
		
					$STRIPE_PUBLISHABLE_KEY = 'STRIPE_PUBLISHABLE_KEY';
					$stripe_publishable_key = get_global_settings($STRIPE_PUBLISHABLE_KEY); //Set from the Global Settings
					$stripe_publishable_key = filter_string($stripe_publishable_key['setting_value']);
		
					$stripe = array(
						"secret_key"      => $stripe_secret_key,
						"publishable_key" => $stripe_publishable_key
					);
		
					\Stripe\Stripe::setApiKey($stripe['secret_key']);
		
					$err_msg = '';
		
					// Get price from GLOBAL SETTINGS
					$PHARMACY_SURVEY_PRICE = 'PHARMACY_SURVEY_PRICE';
					$pharmacy_survey_price = get_global_settings($PHARMACY_SURVEY_PRICE); //Set from the Global Settings
					$pharmacy_survey_price = filter_string($pharmacy_survey_price['setting_value']);
					
					$price = $pharmacy_survey_price;
					$sub_total = $price;
		
					try {
					  
						// Use Stripe's bindings...
						$charge = \Stripe\Charge::create(array(
							'amount'        => ($price*100),
							'currency'      => CURRENCY,
							'card'          => array(
								'number'    => $_POST['card'],
								'exp_month' => $_POST['expMonth'],
								'exp_year'  => $_POST['expYear'],
								'cvc'       => $_POST['cvc']
							), 'description'   => '')
						);
		
					} catch(Stripe_CardError $e) {
		
						// Since it's a decline, Stripe_CardError will be caught
						$body = $e->getJsonBody();
						$err  = $body['error'];
		
						print('Status is:' . $e->getHttpStatus() . "\n");
						print('Type is:' . $err['type'] . "\n");
						print('Code is:' . $err['code'] . "\n");
						// param is '' in this case
						print('Param is:' . $err['param'] . "\n");
						print('Message is:' . $err['message'] . "\n");
		
						$err_msg .= $err['message'];
		
					} catch (Stripe_InvalidRequestError $e) {
						
						// Invalid parameters were supplied to Stripe's API
						$err_msg .= $e->getMessage();
		
					} catch (Stripe_AuthenticationError $e) {
						
						// Authentication with Stripe's API failed
						// (maybe you changed API keys recently)
						$err_msg .= $e->getMessage();
		
					} catch (Stripe_ApiConnectionError $e) {
						
						// Network communication with Stripe failed
						$err_msg .= $e->getMessage();
		
					} catch (Stripe_Error $e) {
						
						// Display a very generic error to the user, and maybe send
						// yourself an email
						$err_msg .= $e->getMessage();
		
					} catch (Exception $e) {
						
						// Something else happened, completely unrelated to Stripe
						$err_msg .= $e->getMessage();
		
					} // try => catch
		
				} else if($payment_method_radio && $payment_method_radio == 'paypal'){
					
					$this->checkout($this->input->post());
					$this->session->set_flashdata('err_message', 'Something went wrong during the payment process, please try again later or contact site administrator.');
					redirect(SURL.'register');
					//exit('PayPal');
		
				} // if($payment_method_radio && $payment_method_radio == 'credit_card')
		
				if($charge && !$err_msg){
		
					////////////////////////////////////
					// Make entries into the database //
					// 1- Make entry into pharmacies
					// 2- Make entry into surveys
					// 3- Make entry into purchase history
		
					// Insert pharmacy
					$pharmacy_id = $this->register->register_pharmacy($this->session->registration_form, $this->session->sf_affiliate_code);
		
					// Insert Survey
					$survey_id = $this->survey->add_new_survey($pharmacy_id);
					
					$VAT_PERCENTAGE = 'VAT_PERCENTAGE';
					$vat_percentage = get_global_settings($VAT_PERCENTAGE); //Set from the Global Settings
		
					$vat_amount = (trim($vat_percentage['setting_value']) / 100) * $sub_total;
					$vat_amount = filter_price($vat_amount);
					
					$grand_total = $sub_total + $vat_amount;
		
					$survey_session = $this->survey->get_pharmacy_survey_list($pharmacy_id, '', $survey_id);
		
					// Insert Invoice
					$invoice_data['subtotal'] = $sub_total;
					$invoice_data['vat_tax'] = $vat_amount;
					$invoice_data['vat_percentage'] = trim($vat_percentage['setting_value']);
					$invoice_data['survey_session'] = $survey_session['survey_title'];
					$invoice_data['grand_total'] = $grand_total;
					$invoice_data['payment_method'] = 'CARD';
					$invoice_data['transaction_id'] = $charge->id;
					
					if($survey_id){
		
						if($this->session->sf_affiliate_code){
							
							$generate_lead = $this->common->generate_lead('SURVEY',$pharmacy_name, $grand_total, date('Y-m-d G:i:s'), $this->session->sf_affiliate_code, 1, $this->input->ip_address(), $pharmacy_id);
							
						}//end if($affiliate_code)
						
						
						// Send Pharmacy Registration Email
						
						// Get price from GLOBAL SETTINGS
						$FROM_BUSINESS_NAME = 'FROM_BUSINESS_NAME';
						$from_business_name = get_global_settings($FROM_BUSINESS_NAME); //Set from the Global Settings
						$from_business_name = filter_string($from_business_name['setting_value']);
						
						// Get price from GLOBAL SETTINGS
						$FROM_ADDRESS = 'FROM_ADDRESS';
						$from_address = get_global_settings($FROM_ADDRESS); //Set from the Global Settings
						$from_address = filter_string($from_address['setting_value']);
						
						// Get price from GLOBAL SETTINGS
						$FROM_ADDRESS_2 = 'FROM_ADDRESS_2';
						$from_address_2 = get_global_settings($FROM_ADDRESS_2); //Set from the Global Settings
						$from_address_2 = filter_string($from_address_2['setting_value']);
						
						// Get price from GLOBAL SETTINGS
						$FROM_COUNTY = 'FROM_COUNTY';
						$from_county = get_global_settings($FROM_COUNTY); //Set from the Global Settings
						$from_county = filter_string($from_county['setting_value']);
						
						// Get price from GLOBAL SETTINGS
						$FROM_POSTCODE = 'FROM_POSTCODE';
						$from_postcode = get_global_settings($FROM_POSTCODE); //Set from the Global Settings
						$from_postcode = filter_string($from_postcode['setting_value']);
						
						// Get price from GLOBAL SETTINGS
						$FROM_TOWN = 'FROM_TOWN';
						$from_town = get_global_settings($FROM_TOWN); //Set from the Global Settings
						$from_town = filter_string($from_town['setting_value']);
						
						// Get price from GLOBAL SETTINGS
						$FROM_EMAIL = 'FROM_EMAIL';
						$from_email = get_global_settings($FROM_EMAIL); //Set from the Global Settings
						$from_email = filter_string($from_email['setting_value']);
						
						// Get price from GLOBAL SETTINGS
						$FROM_WEBSITE = 'FROM_WEBSITE';
						$from_website = get_global_settings($FROM_WEBSITE); //Set from the Global Settings
						$from_website = filter_string($from_website['setting_value']);
						
						// Get price from GLOBAL SETTINGS
						$FROM_PHONE = 'FROM_PHONE';
						$from_phone = get_global_settings($FROM_PHONE); //Set from the Global Settings
						$from_phone = filter_string($from_phone['setting_value']);
						
						// Get price from GLOBAL SETTINGS
						$SITE_LOGO = 'SITE_LOGO';
						$site_logo = get_global_settings($SITE_LOGO); //Set from the Global Settings
						$site_logo_src = SURL.filter_string($site_logo['setting_value']);
						
						// Insert invoice : along with new fields //
						$invoice_data['pharmacy_name'] = $pharmacy_name;
						$invoice_data['pharmacy_owner_name'] = $owner_name;
						$invoice_data['pharmacy_address'] = $address;
						$invoice_data['pharmacy_address_2'] = $address_2;
						$invoice_data['pharmacy_town'] = $town;
						$invoice_data['pharmacy_county'] = $county;
						$invoice_data['pharmacy_postcode'] = $postcode;
						$invoice_data['from_name'] = $from_business_name;
						$invoice_data['from_address'] = $from_address;
						$invoice_data['from_address_2'] = $from_address_2;
						$invoice_data['from_town'] = $from_town;
						$invoice_data['from_county'] = $from_county;
						$invoice_data['from_postcode'] = $from_postcode;
						$invoice_data['from_phone'] = $from_phone;
						$invoice_data['from_email'] = $from_email;
						$invoice_data['from_website'] = $from_website;
		
						$invoice_generated = $this->survey->save_invoice($pharmacy_id, $survey_id, $invoice_data);
		
						$search_arr = array(
							'[REF_NO]', '[DATE]', '[TO]', '[FULL_NAME]',
							'[ADDRESS]', '[ADDRESS_2]', '[COUNTY]',
							'[POSTCODE]', '[TOWN]', '[SITE_LOGO]',
							'FROM_BUSINESS_NAME', 'FROM_ADDRESS',
							'FROM_ADDRESS_2', 'FROM_COUNTY',
							'FROM_POSTCODE', 'FROM_TOWN',
							'FROM_PHONE',
							'FROM_EMAIL', 'FROM_WEBSITE',
							'[SURVEY_PRICE]', '[SUB_TOTAL]',
							'[VAT]', '[VAT_AMOUNT]', '[GRAND_TOTAL]', '[SURVEY_SESSION]', '[PHARMACY_NAME]','[LOGIN_URL]'
						);
		
						$replace_arr = array(
							$invoice_generated['invoice_no'], date('d/m/Y'), $pharmacy_name, $owner_name, 
							$address, $address_2, $county, 
							$postcode, $town, $site_logo_src,
							$from_business_name, $from_address,
							$from_address_2, $from_county, 
							$from_postcode, $from_town,
							$from_phone,
							$from_email, $from_website, 
							'£'.$price, '£'.$sub_total, 
							$vat_percentage['setting_value'].'%', '£'.$vat_amount, '£'.$grand_total, $survey_session['survey_title'],
							$pharmacy_name, SURL.'login'
						);
		
						$this->load->model('email_mod','email_template');
						
						$email_body_arr = $this->email_template->get_email_template(1);
						
						$email_subject = $email_body_arr['email_subject'];
						$email_body = $email_body_arr['email_body'];
						
						$email_body = str_replace($search_arr,$replace_arr,$email_body);
						
						// Get FROM_EMAIL_TXT from GLOBAL SETTINGS
						$FROM_EMAIL_TXT = 'FROM_EMAIL_TXT';
						$from_email_txt = get_global_settings($FROM_EMAIL_TXT); //Set from the Global Settings
						$email_from_txt = filter_string($from_email_txt['setting_value']);
							
						$SES_SENDER = 'SES_SENDER';
						$ses_sender = get_global_settings($SES_SENDER);
						$ses_sender = trim($ses_sender['setting_value']);
		
						$to_email_address = trim($email_address); //'twister787@gmail.com';
		
						// Other message information                                               
						$email_subject = filter_string($email_subject); //'Amazon SES test (SMTP interface accessed using PHP)';
						$email_body = filter_string($email_body); //'This email was sent through the Amazon SES SMTP interface by using PHP.';
		
						## 
						## START: Preparing Attachment for Invoice
						##
						$data['invoice_data'] = $invoice_generated;
						
						$this->stencil->layout('modal_layout');
						$html = $this->load->view('dashboard/pdf/download_invoice', $data, true);
						
						//echo $html; exit;
						
						$this->load->library('pdf');
						$pdf = $this->pdf->load();
						
						$file_name = 'invoice-'.$invoice_generated['invoice_no'].'.pdf';
			
						$pdf->AddPageByArray(array(
									'orientation' => 'P',
									'mgl' => '20',
									'mgr' => '20',
									'mgt' => '10',
									'mgb' => '10',
									'mgh' => '0',
									'mgf' => '0', 
								)
							);
						//header('Content-Type: application/pdf');
						//$pdf->AddPage('P'); // L - P
						$pdf->WriteHTML($html); // write the HTML into the PDF
						//$pdf->Output($file_name,'I'); // save to file because we can
						$pdf->Output('./assets/invoice_tmp/'.$file_name,'F'); 
						
						## 
						## END: Preparing Attachment for Invoice
						##
						
						$attachment_arr[] = 
							array('name' => $file_name,
									'mime' => 'application/pdf',
									'filepath' => './assets/invoice_tmp/'.$file_name
						);
						
						$send_email = send_email($to_email_address, $ses_sender, $email_from_txt, $email_subject, $email_body, $attachment_arr);
						
						// Unset session registration_form
						$this->session->unset_userdata('registration_form');
						
						if(file_exists('./assets/invoice_tmp/'.$file_name))
							unlink('./assets/invoice_tmp/'.$file_name);
						
						// survey purchased email send to admin
						$email_body_arr = $this->email_template->get_email_template(8);
						
						$email_subject = filter_string($email_body_arr['email_subject']);
		
						$search_arr = array('[PHARMACY_NAME]',  '[SURVEY_TITLE]', '[PURCHASED_DATE]');
						$replace_arr = array($pharmacy_name, $survey_session['survey_title'], date('d/m/Y'));                
		
						$email_body = filter_string($email_body_arr['email_body']);
						$email_body = str_replace($search_arr,$replace_arr,$email_body);
						
						$admin_send_email = send_email($ses_sender, $ses_sender, $email_from_txt, $email_subject, $email_body);

						$get_pharmacy_details = $this->pharmacy->get_pharmacy_details($pharmacy_id);
						$get_pharmacy_details['p_id'] = $get_pharmacy_details['id'];
						$get_pharmacy_details['sf_affiliate_code'] = $get_pharmacy_details['affiliate_code'];
						
						unset($get_pharmacy_details['id']);
						unset($get_pharmacy_details['affiliate_code']);
						
						$this->session->set_userdata($get_pharmacy_details);
						
						
						$this->session->set_flashdata('ok_message', 'thank-you');
						redirect(SURL.'pages/thank-you');
		
					} else {
						
						$this->session->set_flashdata('err_message', 'Something went wrong, please try again later.');
						redirect(SURL.'register');
						
					} // End else - if($survey_id)
		
				} else {
		
					$this->session->set_flashdata('err_message', $err_msg);
					redirect(SURL.'register/payment');
		
				} // if($charge)

			}//end if($verify_pharmacy_exist)
			
		}//end if($this->form_validation->run() == FALSE)
		
	} // End - public function register_process()
   
   // Start function: email_exists(): Check if the email already exist or not.
	public function username_exists(){
		$result = $this->users->verify_if_username_already_exist($this->input->post('username'));
		$response = array('exist' => $result);
		echo json_encode($response);
		
	} // End - email_exists():

	////////////////////
	/* --- PayPal --- */
	// Function checkout(): Checkout to the PayPal
	public function checkout($post=''){
		
		// Verify if the POST data is being passed from register_process //
		if($post == '') return false;

		// Extract POST
		extract($post);
		extract($this->session->registration_form);
		
		$to_buy = array(
			'desc' => 'Charge for: Pharmacy Surveys', 
			'currency' => CURRENCY,  // CURRENCY
			'type' => PAYMENT_METHOD,  // PAYMENT_METHOD
			'return_URL' => SURL.'register/checkout-success', 
			// see below have a function for this -- function back()
			// whatever you use, make sure the URL is live and can process
			// the next steps
			'cancel_URL' => SURL.'register', // this goes to this controllers index()
			'shipping_amount' => 0.00,
			'get_shipping' => false
		);

		// Get price from GLOBAL SETTINGS
		$PHARMACY_SURVEY_PRICE = 'PHARMACY_SURVEY_PRICE';
		$pharmacy_survey_price = get_global_settings($PHARMACY_SURVEY_PRICE); //Set from the Global Settings
		$pharmacy_survey_price = filter_string($pharmacy_survey_price['setting_value']);

		$price = $pharmacy_survey_price;
		$sub_total = $price;

		//Survey is not yet started means we have to start the survey of the current one.
		$current_date = date('m/d');	
		$survey_start_date = date('Y-m-d');
		$data['survey_start_date'] = $survey_start_date;
		
		$SURVEY_END_MONTH = 'SURVEY_END_MONTH';
		$survey_end_global_value = get_global_settings($SURVEY_END_MONTH); //Set from the Global Settings
		$next_survey_end_date = filter_string($survey_end_global_value['setting_value']);
		
		if(strtotime($current_date) > strtotime($next_survey_end_date)){
			
			$next_survey_end = strtotime("$next_survey_end_date +1 year");	
			$survey_session = date('Y').'-'.(date('y')+1);
			$survey_end_date = date('Y-m-d', $next_survey_end).'-'.(date('y', $next_survey_end)+1); 
			
		}else{
			
			$next_survey_end = strtotime("$next_survey_end_date +0 year");
			$survey_session = (date('Y', $next_survey_end)-1).'-'.date('y', $next_survey_end);
			$survey_end_date = (date('Y-m-d', $next_survey_end)-1).'-'.date('y', $next_survey_end);
			
		}//end if
		
		$survey_session = 'Survey '.$survey_session;
		
		$temp_product = array(
			'name' => $survey_session,
			'desc' => '',
			'number' => '1000',
			'quantity' => 1, 
			'amount' => $price
		);

		$to_buy['products'][0] = $temp_product;

		$VAT_PERCENTAGE = 'VAT_PERCENTAGE';
		$vat_percentage = get_global_settings($VAT_PERCENTAGE); //Set from the Global Settings

		$vat_amount = (trim($vat_percentage['setting_value']) / 100) * $sub_total;
		$vat_amount = filter_price($vat_amount);
		
		$to_buy['tax_amount'] = $vat_amount;
		
		// enquire Paypal API for token
		$set_ec_return = $this->paypal_ec->set_ec($to_buy);
		
		if (isset($set_ec_return['ec_status']) && ($set_ec_return['ec_status'] === true)) {
			
			// redirect to Paypal
			$this->paypal_ec->redirect_to_paypal($set_ec_return['TOKEN']);
			// You could detect your visitor's browser and redirect to Paypal's mobile checkout
			// if they are on a mobile device. Just add a true as the last parameter. It defaults
			// to false
			// $this->paypal_ec->redirect_to_paypal( $set_ec_return['TOKEN'], true);
			
		} else {
			$this->_error($set_ec_return);
			
		}//end if (isset($set_ec_return['ec_status']) && ($set_ec_return['ec_status'] === true)) 
		
	}//end checkout()
	
	public function checkout_success(){

		$token = $_GET['token'];
		$payer_id = $_GET['PayerID'];
		
		$get_ec_return = $this->paypal_ec->get_ec($token);

		if(isset($get_ec_return['ec_status']) && ($get_ec_return['ec_status'] === true)) {
			// at this point, you have all of the data for the transaction.
			// you may want to save the data for future action. what's left to
			// do is to collect the money -- you do that by call DoExpressCheckoutPayment
			// via $this->paypal_ec->do_ec();
			//
			// I suggest to save all of the details of the transaction. You get all that
			// in $get_ec_return array
			$ec_details = array(
				'token' => $token, 
				'payer_id' => $payer_id, 
				'currency' => CURRENCY, 
				'amount' => $get_ec_return['PAYMENTREQUEST_0_AMT'], 
				'IPN_URL' => site_url('register/ipn'), 
				// in case you want to log the IPN, and you
				// may have to in case of Pending transaction
				'type' => PAYMENT_METHOD);
				
			// DoExpressCheckoutPayment
			$do_ec_return = $this->paypal_ec->do_ec($ec_details);
			
			if (isset($do_ec_return['ec_status']) && ($do_ec_return['ec_status'] === true)) {
				
				// at this point, you have collected payment from your customer
				// you may want to process the order now.

				if($get_ec_return['ACK'] == 'Success' && $do_ec_return['ACK'] == 'Success'){
					
					$data['get_ec_return'] = $get_ec_return;
					$data['do_ec_return'] = $do_ec_return;
					$data['user_id'] = $this->session->id;
					$data['purchased_by_id'] = $this->session->id;
					$data['num_of_products'] = 1;
					
					////////////////////////////////////
					// Make entries into the database //
					// 1- Make entry into pharmacies
					// 2- Make entry into surveys
					// 3- Make entry into purchase history

					// Insert pharmacy
					// $pharmacy_id = $this->register->register_pharmacy($this->input->post());
					$pharmacy_id = $this->register->register_pharmacy($this->session->registration_form, $this->session->sf_affiliate_code);

					// Insert Survey
					$survey_id = $this->survey->add_new_survey($pharmacy_id);

					$survey_session = $this->survey->get_pharmacy_survey_list($pharmacy_id, '', $survey_id);
					
					$VAT_PERCENTAGE = 'VAT_PERCENTAGE';
					$vat_percentage = get_global_settings($VAT_PERCENTAGE); //Set from the Global Settings

					// Insert Invoice
					$invoice_data['subtotal'] = $get_ec_return['ITEMAMT'];
					$invoice_data['vat_tax'] = $get_ec_return['TAXAMT'];
					
					$invoice_data['vat_percentage'] = trim($vat_percentage['setting_value']);
					$invoice_data['survey_session'] = $survey_session['survey_title'];

					$invoice_data['grand_total'] = $get_ec_return['AMT'];
					$invoice_data['transaction_id'] = $do_ec_return['PAYMENTINFO_0_TRANSACTIONID'];
					$invoice_data['payment_method'] = 'PAYPAL';

					if($survey_id){
						
						if($this->session->sf_affiliate_code){
							
							$generate_lead = $this->common->generate_lead('SURVEY',$this->session->registration_form['pharmacy_name'], $get_ec_return['AMT'], date('Y-m-d G:i:s'), $this->session->sf_affiliate_code, 1, $this->input->ip_address(), $pharmacy_id);
						
						}//end if($affiliate_code)
						
						// Send Pharmacy Registration Email
						
						$VAT_PERCENTAGE = 'VAT_PERCENTAGE';
						$vat_percentage = get_global_settings($VAT_PERCENTAGE); //Set from the Global Settings

						$vat_percentage = trim($vat_percentage['setting_value']);

						// Get price from GLOBAL SETTINGS
						$FROM_BUSINESS_NAME = 'FROM_BUSINESS_NAME';
						$from_business_name = get_global_settings($FROM_BUSINESS_NAME); //Set from the Global Settings
						$from_business_name = filter_string($from_business_name['setting_value']);
						
						// Get price from GLOBAL SETTINGS
						$FROM_ADDRESS = 'FROM_ADDRESS';
						$from_address = get_global_settings($FROM_ADDRESS); //Set from the Global Settings
						$from_address = filter_string($from_address['setting_value']);
						
						// Get price from GLOBAL SETTINGS
						$FROM_ADDRESS_2 = 'FROM_ADDRESS_2';
						$from_address_2 = get_global_settings($FROM_ADDRESS_2); //Set from the Global Settings
						$from_address_2 = filter_string($from_address_2['setting_value']);
						
						// Get price from GLOBAL SETTINGS
						$FROM_COUNTY = 'FROM_COUNTY';
						$from_county = get_global_settings($FROM_COUNTY); //Set from the Global Settings
						$from_county = filter_string($from_county['setting_value']);
						
						// Get price from GLOBAL SETTINGS
						$FROM_POSTCODE = 'FROM_POSTCODE';
						$from_postcode = get_global_settings($FROM_POSTCODE); //Set from the Global Settings
						$from_postcode = filter_string($from_postcode['setting_value']);
						
						// Get price from GLOBAL SETTINGS
						$FROM_TOWN = 'FROM_TOWN';
						$from_town = get_global_settings($FROM_TOWN); //Set from the Global Settings
						$from_town = filter_string($from_town['setting_value']);
						
						// Get price from GLOBAL SETTINGS
						$FROM_EMAIL = 'FROM_EMAIL';
						$from_email = get_global_settings($FROM_EMAIL); //Set from the Global Settings
						$from_email = filter_string($from_email['setting_value']);
						
						// Get price from GLOBAL SETTINGS
						$FROM_WEBSITE = 'FROM_WEBSITE';
						$from_website = get_global_settings($FROM_WEBSITE); //Set from the Global Settings
						$from_website = filter_string($from_website['setting_value']);
						
						// Get price from GLOBAL SETTINGS
						$FROM_PHONE = 'FROM_PHONE';
						$from_phone = get_global_settings($FROM_PHONE); //Set from the Global Settings
						$from_phone = filter_string($from_phone['setting_value']);
						
						// Get price from GLOBAL SETTINGS
						$SITE_LOGO = 'SITE_LOGO';
						$site_logo = get_global_settings($SITE_LOGO); //Set from the Global Settings
						$site_logo_src = SURL.filter_string($site_logo['setting_value']);

						//echo 'here=> ';
						//print_this($this->session->registration_form);
						//exit;

						// Insert invoice : along with new fields //
						$invoice_data['pharmacy_name'] = $this->session->registration_form['pharmacy_name'];
						$invoice_data['pharmacy_owner_name'] = $this->session->registration_form['owner_name'];
						$invoice_data['pharmacy_address'] = $this->session->registration_form['address'];
						$invoice_data['pharmacy_address_2'] = $this->session->registration_form['address_2'];
						$invoice_data['pharmacy_town'] = $this->session->registration_form['town'];
						$invoice_data['pharmacy_county'] = $this->session->registration_form['county'];
						$invoice_data['pharmacy_postcode'] = $this->session->registration_form['postcode'];
						$invoice_data['from_name'] = $from_business_name;
						$invoice_data['from_address'] = $from_address;
						$invoice_data['from_address_2'] = $from_address_2;
						$invoice_data['from_town'] = $from_town;
						$invoice_data['from_county'] = $from_county;
						$invoice_data['from_postcode'] = $from_postcode;
						$invoice_data['from_phone'] = $from_phone;
						$invoice_data['from_email'] = $from_email;
						$invoice_data['from_website'] = $from_website;

						$invoice_generated = $this->survey->save_invoice($pharmacy_id, $survey_id, $invoice_data);

						$search_arr = array(
							'[REF_NO]', '[DATE]', '[TO]', '[FULL_NAME]',
							'[ADDRESS]', '[ADDRESS_2]', '[COUNTY]',
							'[POSTCODE]', '[TOWN]', '[SITE_LOGO]',
							'FROM_BUSINESS_NAME', 'FROM_ADDRESS',
							'FROM_ADDRESS_2', 'FROM_COUNTY',
							'FROM_POSTCODE', 'FROM_TOWN',
							'FROM_PHONE',
							'FROM_EMAIL', 'FROM_WEBSITE',
							'[SURVEY_PRICE]', '[SUB_TOTAL]',
							'[VAT]', '[VAT_AMOUNT]', '[GRAND_TOTAL]', '[SURVEY_SESSION]', '[PHARMACY_NAME]','[LOGIN_URL]'
						);

						$survey_session = $this->survey->get_pharmacy_survey_list($pharmacy_id, '', $survey_id);

						$replace_arr = array(

							$invoice_generated['invoice_no'], date('d/m/Y'), $this->session->registration_form['pharmacy_name'], $this->session->registration_form['owner_name'], 
							$this->session->registration_form['address'], $this->session->registration_form['address_2'], $this->session->registration_form['county'], 
							$this->session->registration_form['postcode'], $this->session->registration_form['town'], $site_logo_src,
							$from_business_name, $from_address,
							$from_address_2, $from_county, 
							$from_postcode, $from_town,
							$from_phone,
							$from_email, $from_website, 
							'£'.$invoice_data['subtotal'], '£'.$invoice_data['subtotal'],
							$vat_percentage.'%', '£'.$invoice_data['vat_tax'], '£'.$invoice_data['grand_total'], $survey_session['survey_title'],
							$this->session->registration_form['pharmacy_name'], SURL.'login'
						);
						
						$this->load->model('email_mod','email_template');
						
						$email_body_arr = $this->email_template->get_email_template(1);
						
						$email_subject = $email_body_arr['email_subject'];
						$email_body = $email_body_arr['email_body'];
						
						$email_body = str_replace($search_arr,$replace_arr,$email_body);
						
						// Get FROM_EMAIL_TXT from GLOBAL SETTINGS
						$FROM_EMAIL_TXT = 'FROM_EMAIL_TXT';
						$from_email_txt = get_global_settings($FROM_EMAIL_TXT); //Set from the Global Settings
						$email_from_txt = filter_string($from_email_txt['setting_value']);
								
						$SES_SENDER = 'SES_SENDER';
						$ses_sender = get_global_settings($SES_SENDER);
						$ses_sender = trim($ses_sender['setting_value']);

						$to_email_address = $this->session->registration_form['email_address']; //'twister787@gmail.com';

						$email_subject = filter_string($email_subject); 
						$email_body = filter_string($email_body); 

						## 
						## START: Preparing Attachment for Invoice
						##
						$data['invoice_data'] = $invoice_generated;
						
						$this->stencil->layout('modal_layout');
						$html = $this->load->view('dashboard/pdf/download_invoice', $data, true);
						
						//echo $html; exit;
						
						$this->load->library('pdf');
						$pdf = $this->pdf->load();
						
						$file_name = 'invoice-'.$invoice_generated['invoice_no'].'.pdf';
			
						$pdf->AddPageByArray(array(
									'orientation' => 'P',
									'mgl' => '20',
									'mgr' => '20',
									'mgt' => '10',
									'mgb' => '10',
									'mgh' => '0',
									'mgf' => '0', 
								)
							);
						//header('Content-Type: application/pdf');
						//$pdf->AddPage('P'); // L - P
						$pdf->WriteHTML($html); // write the HTML into the PDF
						//$pdf->Output($file_name,'I'); // save to file because we can
						$pdf->Output('./assets/invoice_tmp/'.$file_name,'F'); 
						
						## 
						## END: Preparing Attachment for Invoice
						##
						
						$attachment_arr[] = 
							array('name' => $file_name,
									'mime' => 'application/pdf',
									'filepath' => './assets/invoice_tmp/'.$file_name
						);
						
						
						$send_email = send_email($to_email_address, $ses_sender, $email_from_txt, $email_subject, $email_body, $attachment_arr);
						
						////////////////////////////////////

						if(file_exists('./assets/invoice_tmp/'.$file_name))
							unlink('./assets/invoice_tmp/'.$file_name);
		
						// survey purchased email send to admin
					
						$email_body_arr = $this->email_template->get_email_template(8);
						
						$email_subject = filter_string($email_body_arr['email_subject']);

						$search_arr = array('[PHARMACY_NAME]','[SURVEY_TITLE]','[PURCHASED_DATE]');
						$replace_arr = array($this->session->registration_form['pharmacy_name'], $survey_session['survey_title'], date('d/m/Y'));                
									
						$email_body = filter_string($email_body_arr['email_body']);
						$email_body = str_replace($search_arr,$replace_arr,$email_body);
						
						$admin_send_email = send_email($ses_sender, $ses_sender, $email_from_txt, $email_subject, $email_body);
						
						// Unset session registration_form
						$this->session->unset_userdata('registration_form');
						
						$get_pharmacy_details = $this->pharmacy->get_pharmacy_details($pharmacy_id);
						$get_pharmacy_details['p_id'] = $get_pharmacy_details['id'];
						$get_pharmacy_details['sf_affiliate_code'] = $get_pharmacy_details['affiliate_code'];
						
						unset($get_pharmacy_details['id']);
						unset($get_pharmacy_details['affiliate_code']);
						
						$this->session->set_userdata($get_pharmacy_details);
						
						// Make an entry for Survey
						$this->session->set_flashdata('ok_message', 'thank-you');
						redirect(SURL.'pages/thank-you');

					} else {
						
						$this->session->set_flashdata('err_message', 'Something went wrong, please try again later.');
						redirect(SURL.'register');
						
					} // End else - if($survey_id)
					
				} else {
					echo "OOPS";exit;	
				}//end if($get_ec_return['ACK'] == 'Success' && $do_ec_return['ACK'] = 'Success')
				
			} else {
				
				$this->_error($do_ec_return);
				
			}
		} else {
			$this->_error($get_ec_return);
		}
		
	}//end checkout_success()

	public function ipn(){
		$logfile = 'ipnlog/' . uniqid() . '.html';
		$logdata = "<pre>\r\n" . print_r($_POST, true) . '</pre>';
		file_put_contents($logfile, $logdata);
		
	}//end ipn()

	public function _error($ecd){

		//$erro_txt .= "<h3>error at Express Checkout<h3>";
		//$erro_txt .= "<pre>" . print_r($ecd, true) . "</pre>";

		$err_msg  = $ecd['L_ERRORCODE0'].': '.$ecd['L_SHORTMESSAGE0'].': '.$ecd['L_LONGMESSAGE0'];
		
		// Paypal Error
		$this->session->set_flashdata('err_message', $err_msg);
		redirect(SURL.'register');

	}//end _error($ecd)

	// Check email exist 
	public function email_exist(){
		
		extract($this->input->post());
		
		$result = $this->register->verify_if_pharmacy_already_exist($email,$pharmacy_id);
		$response = array('exist' => $result);
		echo json_encode($response);
		
	} // End - email_exists():

	// Start => public function next_step()
	public function payment(){

		// if(!$this->input->post()) exit;

		if($this->input->post('submit_step_1')){

			$data = array('registration_form' => $this->input->post());
			$this->session->set_userdata($data);

		} // if($this->input->post('submit_step_1'))

		extract($this->input->post());

		$this->form_validation->set_rules('pharmacy_name', 'Business Name', 'trim|required');
		$this->form_validation->set_rules('contact_no', 'Business Phone Number', 'trim|required');
		$this->form_validation->set_rules('address', 'Address', 'trim|required');
		$this->form_validation->set_rules('town', 'Business Town', 'trim|required');
		$this->form_validation->set_rules('postcode', 'Postcode', 'trim|required');
		
		$this->form_validation->set_rules('owner_name', 'Owner Name', 'trim|required');
		$this->form_validation->set_rules('email_address', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');

		if($this->form_validation->run() == FALSE && $this->input->post('add_pharamcy_btn')){

			// PHP Error 
			$this->session->set_flashdata('err_message', validation_errors());
			redirect(base_url().'register');

		} else {

			$verify_pharmacy_exist = $this->register->verify_if_pharmacy_already_exist($email_address);

			if($verify_pharmacy_exist){

				$this->session->set_flashdata('err_message', 'The email you have entered already exist, please try another one.');
				redirect(SURL.'register');
				
			} else {

				//set title
				$page_title = DEFAULT_TITLE;
				$this->stencil->title($page_title);	
				
				//Sets the Meta data
				$this->stencil->meta(array(
					'description' => DEFAULT_META_DESCRIPTION,
					'keywords' => DEFAULT_META_KEYWORDS,
					'meta_title' => DEFAULT_TITLE
				));

				// Page Heading
				$page_heading = 'Pharamcy Survey Register';
				$data['page_heading'] = $page_heading;

				// Get Page CMS data from DB
				//$cms_data_arr = $this->cms->get_cms_page('terms-conditions'); 
				//print_this($cms_data_arr); exit;
				//$data['terms_data'] = $cms_data_arr;
				
				$this->stencil->css('captcha.css');

				// Get prices for checkout page
				// 

				//Fancybox files
				$this->stencil->css('jquery.fancybox.css');
		        $this->stencil->js('jquery.fancybox.js');

				//Survey is not yet started means we have to start the survey of the current one.
				$current_date = date('m/d');	
				$survey_start_date = date('Y-m-d');
				$data['survey_start_date'] = $survey_start_date;
				
				$SURVEY_END_MONTH = 'SURVEY_END_MONTH';
				$survey_end_global_value = get_global_settings($SURVEY_END_MONTH); //Set from the Global Settings
				$next_survey_end_date = filter_string($survey_end_global_value['setting_value']);
				
				if(strtotime($current_date) > strtotime($next_survey_end_date)){
					
					$next_survey_end = strtotime("$next_survey_end_date +1 year");	
					$survey_session = date('Y').'-'.(date('y')+1);
					$survey_end_date = date('Y-m-d', $next_survey_end).'-'.(date('y', $next_survey_end)+1); 
					
				}else{
					
					$next_survey_end = strtotime("$next_survey_end_date +0 year");
					$survey_session = (date('Y', $next_survey_end)-1).'-'.date('y', $next_survey_end);
					$survey_end_date = (date('Y-m-d', $next_survey_end)-1).'-'.date('y', $next_survey_end);
					
				}//end if
				
				$survey_session = 'Survey '.$survey_session;

				// Get price from GLOBAL SETTINGS
				$PHARMACY_SURVEY_PRICE = 'PHARMACY_SURVEY_PRICE';
				$pharmacy_survey_price = get_global_settings($PHARMACY_SURVEY_PRICE); //Set from the Global Settings
				$pharmacy_survey_price = filter_string($pharmacy_survey_price['setting_value']);
				
				$price = $pharmacy_survey_price;
				$sub_total = $price;

				// Get VAT % Settings
				$VAT_PERCENTAGE = 'VAT_PERCENTAGE';
				$vat_percentage = get_global_settings($VAT_PERCENTAGE); //Set from the Global Settings

				$vat_amount = (trim($vat_percentage['setting_value']) / 100) * $sub_total;
				$vat_amount = filter_price($vat_amount);

				$data['survey_session'] = $survey_session;
				$data['pharmacy_survey_price'] = $pharmacy_survey_price;
				$data['sub_total'] = $sub_total;
				$data['vat'] = trim($vat_percentage['setting_value']);
				$data['vat_amount'] = $vat_amount;
				$data['grand_total'] = $sub_total + $vat_amount;
				
				$data['is_step_1'] = false;
				$data['is_step_2'] = true;
				
				$this->stencil->js('kod_scripts/register.js');

				$this->stencil->layout('page');
				$this->stencil->paint('register/register',$data);

			} // if($verify_pharmacy_exist)

		} // if($this->form_validation->run() == FALSE)

	} // End => public function next_step()
	
} // End - CI Class
