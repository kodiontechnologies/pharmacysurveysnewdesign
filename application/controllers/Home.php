<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('Cms_mod', 'cms');

		//Sets the variable $header_contents to use the slice head (/views/slices/header_contents.php)
		$this->stencil->slice('header_contents');

		//Sets the variable $header_scripts to use the slice head (/views/slices/header_scripts.php)
		$this->stencil->slice('header_scripts');
		
		//Sets the variable $footer_contents to use the slice head (/views/slices/footer_contents.php)
		$this->stencil->slice('footer_contents');
		
		//Sets the variable $footer_scripts to use the slice head (/views/slices/footer_scripts.php)
		$this->stencil->slice('footer_scripts');
		
		// left_pane
		// $this->stencil->slice('left_pane');
	}

	// Start - function index() : Main Homepage view
 	public function index(){

		$cms_page = $this->cms->get_cms_page('homepage');
		$data['homepage_cms'] = $cms_page;

		if(filter_string($cms_page['page_title'])){
			
			//set title
			$page_title = $cms_page['meta_title'];
			$this->stencil->title($page_title);	
			
			//Sets the Meta data
			$this->stencil->meta(array(
				'description' => $cms_page['meta_description'],
				'keywords' => $cms_page['meta_keywords']
			));

		}else{
			
			//set title
			$page_title = DEFAULT_TITLE;
			$this->stencil->title($page_title);	
			
			//Sets the Meta data
			$this->stencil->meta(array(
				'description' => DEFAULT_META_DESCRIPTION,
				'keywords' => DEFAULT_META_KEYWORDS
			));

		}//end if(filter_string($homepage_cms['page_title']))

		// Page Heading
		$page_heading = 'Pharamcy Survey';
		$data['page_heading'] = $page_heading;
		

		$this->stencil->layout('page');
		$this->stencil->paint('home/home', $data);

	} // End - public function index()
	
} // End - CI Class
