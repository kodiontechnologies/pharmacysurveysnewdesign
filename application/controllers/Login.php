<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('Login_mod', 'login');
		$this->load->model('Cms_mod', 'cms');

		//Sets the variable $header_contents to use the slice head (/views/slices/header_contents.php)
		$this->stencil->slice('header_contents');

		//Sets the variable $header_scripts to use the slice head (/views/slices/header_scripts.php)
		$this->stencil->slice('header_scripts');
		
		//Sets the variable $footer_contents to use the slice head (/views/slices/footer_contents.php)
		$this->stencil->slice('footer_contents');
		
		//Sets the variable $footer_scripts to use the slice head (/views/slices/footer_scripts.php)
		$this->stencil->slice('footer_scripts');

		$this->stencil->js('kod_scripts/form_validation.js');
		$this->stencil->js('kod_scripts/form_validation/bootstrap_validator/dist/formValidation.min.js');
		$this->stencil->js('kod_scripts/form_validation/bootstrap_validator/dist/bootstrap.min.js');

	}

	public function index(){

		if($this->session->p_id && !$this->input->post('is_admin')){ 
			redirect(base_url().'dashboard');
		}

		$cms_page = $this->cms->get_cms_page('login');
		$data['cms_page'] = $cms_page;

		if(filter_string($cms_page['page_title'])){
			
			//set title
			$page_title = $cms_page['meta_title'];
			$this->stencil->title($page_title);	
			
			//Sets the Meta data
			$this->stencil->meta(array(
				'description' => $cms_page['meta_description'],
				'keywords' => $cms_page['meta_keywords']
			));

		}else{
			
			//set title
			$page_title = DEFAULT_TITLE;
			$this->stencil->title($page_title);	
			
			//Sets the Meta data
			$this->stencil->meta(array(
				'description' => DEFAULT_META_DESCRIPTION,
				'keywords' => DEFAULT_META_KEYWORDS
			));

		}//end if(filter_string($homepage_cms['page_title']))

		// Page Heading
		$page_heading = 'Login ::Pharamcy Survey';
		$data['page_heading'] = $page_heading;
		
		$this->stencil->css('captcha.css');
		
		// Get CMS contents from DB for Login Page
		$data['cms_page'] = $this->cms->get_cms_page('login');

		$this->stencil->layout('page');
		$this->stencil->paint('login/login', $data);
		
	} //end index()
	
	//Function login_process(): Process and authenticate the login form
	public function login_process(){
		
		extract($this->input->post());
		
		// If is a valid request
		if(!$this->input->post('email_address')) redirect(SURL);
		
		if(!$is_group && !$is_admin){
			
			// Captcha Validation
			if($this->input->post('g-recaptcha-response') == ''){
				
				$this->session->set_flashdata($this->input->post());
				$this->session->set_flashdata('err_message', 'Please verify Captcha');
				redirect(base_url().'login');
	
			} //end if($this->input->post('g-recaptcha-response') == '')
				
					// Captcha Validation
			if(strtolower($this->input->post('g-recaptcha-response')) != $this->session->random_number){
				
				$this->session->set_flashdata($this->input->post());
				$this->session->set_flashdata('err_message', 'Invalid captcha. Please try again.');
				redirect(base_url().'login');
	
			} //end if($this->input->post('g-recaptcha-response') == '')

		}//end if($is_group)
		
		// PHP Validation
		$this->form_validation->set_rules('email_address', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');

		if($this->form_validation->run() == FALSE){
			
			$this->session->set_flashdata($this->input->post());
			// PHP Error
			$this->session->set_flashdata('err_message', validation_errors());
			redirect(base_url().'login');

		} else {

			//This part is used for auto-login by admin side.
			if($this->input->post('is_admin') && $this->session->admin_id)
				$admin_login = 1;
			else
				$admin_login = 0;
			
			$verify_pharmacy_credentials = $this->login->verify_pharmacy_credentials($email_address,$password, $is_group, $admin_login);
			
			if($verify_pharmacy_credentials){
				// Set Session
				$this->session->set_userdata($verify_pharmacy_credentials);
				redirect(base_url().'dashboard');
					
			}else{
				$this->session->set_flashdata('err_message', 'Invalid email or password. Please try again.');
				redirect(base_url().'login');
				
			}//end if($verify_account_activation)
			
		}//end if($this->form_validation->run() == FALSE)
		
	}//end public function login_process()
	
	//Function activation() Email activation process
	public function activate_account(){

		$verify_account = $this->login->verify_email_account($this->input->get());
		
		if($verify_account){
			
			$get_user_details = $this->users->get_user_details_email($this->input->get('uid'));
			
			
			if($get_user_details['status'] == 1)
			
 				
		    $this->session->set_flashdata('ok_message', 'Your account is successfully verified, please login with your details.');
			redirect(base_url().'login');

		}else{
			$this->session->set_flashdata('err_message', 'We were not able to verify your account. Please contact site administrator for further details.');
			redirect(base_url().'login');
			
		}//end if($verify_account)
		
	} // End - activation()
	
	public function forgot_password(){

		if($this->session->p_id && !$this->input->post('is_admin')){ 
			redirect(base_url().'dashboard');
		}
		
	
		//CMS DATA
		$cms_page = $this->cms->get_cms_page('forgot-password');
		$data['cms_page'] = $cms_page;

		if(filter_string($cms_page['page_title'])){
			
			//set title
			$page_title = $cms_page['meta_title'];
			$this->stencil->title($page_title);	
			
			//Sets the Meta data
			$this->stencil->meta(array(
				'description' => $cms_page['meta_description'],
				'keywords' => $cms_page['meta_keywords']
			));

		}else{
			
			//set title
			$page_title = DEFAULT_TITLE;
			$this->stencil->title($page_title);	
			
			//Sets the Meta data
			$this->stencil->meta(array(
				'description' => DEFAULT_META_DESCRIPTION,
				'keywords' => DEFAULT_META_KEYWORDS
			));

		}//end if(filter_string($homepage_cms['page_title']))

		// Page Heading
		$page_heading = 'Login ::Pro CDR';
		$data['page_heading'] = $page_heading;
		
		$this->stencil->css('captcha.css');

		// Get CMS contents from DB for Forgot Password
		$data['cms_page'] = $this->cms->get_cms_page('forgot-password');

		$this->stencil->layout('page');
		$this->stencil->paint('login/forgot_password',$data);
		
	} //end forgot_password()

	//Function forgot_password_process(): Process and authenticate the forgot password
	public function forgot_password_process(){

		if($this->session->p_id && !$this->input->post('is_admin')){ 
			redirect(base_url().'dashboard');
		}
		
		//If Post is not SET
		if(!$this->input->post() && !$this->input->post('fgetpass_btn')) redirect(base_url());

		// Captcha Validation
		if($this->input->post('g-recaptcha-response') == ''){
			 
			$this->session->set_flashdata('err_message', 'Please verify Captcha');
			redirect(base_url().'login/forgot-password');

		}//end if($this->input->post('g-recaptcha-response') == '')
		
		
		// Captcha Validation
		if(strtolower($this->input->post('g-recaptcha-response')) != $this->session->random_number){
			
			$this->session->set_flashdata($this->input->post());
			$this->session->set_flashdata('err_message', 'Invalid captcha. Please try again.');
			redirect(base_url().'login/forgot-password');

		} //end if($this->input->post('g-recaptcha-response') == '')

		
		// PHP Validation
		$this->form_validation->set_rules('username', 'Username', 'trim|required');
		
		if($this->form_validation->run() == FALSE){
			 
			// PHP Error 
			$this->session->set_flashdata('err_message', validation_errors());
			redirect(base_url().'login/forgot-password');
			
		} else {

			$user_exist_arr = $this->login->get_user_by_name_pincode($this->input->post('username'));
			
			if($user_exist_arr){
			
				//Sending Email to the User
				$send_new_password = $this->login->send_new_password($user_exist_arr);
			
				if($send_new_password){
					$this->session->set_flashdata('ok_message', 'A link has been sent to your email address. Please open the link to set a new password.');
					redirect(base_url().'login/forgot-password');
				
				}else{
				
					$this->session->set_flashdata('err_message', 'Something went wrong, please try again later.');
					redirect(base_url().'login/forgot-password');
			
			} // End - if($send_new_password)
			
		  }else{

			$this->session->set_flashdata('err_message', 'The Username or Pincode do not exist in your database. Please try again, with the correct username and pincode');
			redirect(base_url().'login/forgot-password');
			
		}//end if($chk_isvalid_user) 
		
	 } // Else  
		
	}//end forgot_password_process
	
	//Function reset_password(): Reset Password Form
	public function reset_password($reset_code){

		if($this->session->p_id && !$this->input->post('is_admin')){ 
			redirect(base_url().'dashboard');
		}
		
		$verify_reset_code = $this->login->verify_reset_code($reset_code);
		
		if(!$verify_reset_code){

			$this->session->set_flashdata('err_message', 'The reset link is either invalid or have expired. Please try again.');
			redirect(base_url().'login/forgot-password');
			
		}//end if(!$verify_reset_code)

		//CMS DATA
		$cms_page = $this->cms->get_cms_page('reset-password');
		$data['cms_page'] = $cms_page;

		if(filter_string($cms_page['page_title'])){
			
			//set title
			$page_title = $cms_page['meta_title'];
			$this->stencil->title($page_title);	
			
			//Sets the Meta data
			$this->stencil->meta(array(
				'description' => $cms_page['meta_description'],
				'keywords' => $cms_page['meta_keywords']
			));

		}else{
			
			//set title
			$page_title = DEFAULT_TITLE;
			$this->stencil->title($page_title);	
			
			//Sets the Meta data
			$this->stencil->meta(array(
				'description' => DEFAULT_META_DESCRIPTION,
				'keywords' => DEFAULT_META_KEYWORDS
			));

		}//end if(filter_string($homepage_cms['page_title']))

		// Page Heading
		$page_heading = 'Set new Password ::Pro CDR';
		$data['page_heading'] = $page_heading;
		
		$this->stencil->css('captcha.css');

		$data['reset_code'] = $reset_code;

		// Get CMS contents from DB for Forgot Password
		$data['cms_page'] = $this->cms->get_cms_page('reset-password');

		$this->stencil->layout('page');
		$this->stencil->paint('login/reset_password',$data);

	}//end reset_password($reset_code)
	
	//Function reset_password_process()
	public function reset_password_process(){

		if($this->session->p_id && !$this->input->post('is_admin')){ 
			redirect(base_url().'dashboard');
		}

		// Extract POST
		extract($this->input->post());
		
		//If Post is not SET
		if(!$this->input->post() && !$this->input->post('reset_pass_btn') && !$verify_code) redirect(base_url());

		// Captcha Validation
		if($this->input->post('g-recaptcha-response') == ''){
			 
			$this->session->set_flashdata('err_message', 'Please verify Captcha');
			redirect(base_url().'login/reset-password/'.$verify_code);

		}//end if($this->input->post('g-recaptcha-response') == '')
		
		
		// Captcha Validation
		if(strtolower($this->input->post('g-recaptcha-response')) != $this->session->random_number){
			
			$this->session->set_flashdata($this->input->post());
			$this->session->set_flashdata('err_message', 'Invalid captcha. Please try again.');
			redirect(base_url().'login/reset-password/'.$verify_code);

		} //end if($this->input->post('g-recaptcha-response') == '')

		// PHP Validation
		$this->form_validation->set_rules('new_password', 'New Password', 'trim|required');
		$this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required');
		
		if($this->form_validation->run() == FALSE){
			 
			// PHP Error 
			$this->session->set_flashdata('err_message', validation_errors());
			redirect(base_url().'login/reset-password/'.$verify_code);
			
		} else {
			
			$decode = base64_decode(urldecode($verify_code));
			$split_decode = explode('|',$decode);
			
			$reset_password = $this->login->reset_password($split_decode[1], $this->input->post());

			if($reset_password){
			
				$this->session->set_flashdata('ok_message', 'Your password is successfully updated, You may login to your Pharmacy Surveys account with the updated credentials');
				redirect(base_url().'login');

			} else {
					
				$this->session->set_flashdata('err_message', 'Something went wrong, please try again later.');
				redirect(base_url().'login/reset-password/'.$verify_code);
			
			} // End - if($send_new_password)

		} // end if($this->form_validation->run() == FALSE)
		
	} // End reset_password_process()
	
	public function get_captcha(){

		$string = '';
		
		for($i = 0; $i < 2; $i++){
			$string .= rand(97, 122);
		} // for($i = 0; $i < 5; $i++)
		
		$this->session->random_number = $string;
		
		$dir = './assets/captcha/fonts/';
		
		$image = imagecreatetruecolor(165, 50);
		
		// random number 1 or 2
		$num = rand(1,2);
		if($num==1)
		{
			$font = "Capture it 2.ttf"; // font style
		}
		else
		{
			$font = "Molot.otf";// font style
		}
		
		// random number 1 or 2
		$num2 = rand(1,2);
		if($num2==1)
		{
			$color = imagecolorallocate($image, 113, 193, 217);// color
		}
		else
		{
			$color = imagecolorallocate($image, 163, 197, 82);// color
		}
		
		$white = imagecolorallocate($image, 255, 255, 255); // background color white
		imagefilledrectangle($image,0,0,399,99,$white);
		
		imagettftext ($image, 30, 0, 10, 40, $color,$dir.$font, $this->session->random_number);
		
		//header("Content-type: image/png");

		//$im = imagecreatefrompng($_POST['image']); 
		imagepng($image);
				
	} //end index()
	
} /* End of file */
