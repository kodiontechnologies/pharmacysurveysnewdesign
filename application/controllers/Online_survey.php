<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Online_survey extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('Survey_mod', 'survey');
		$this->load->model('Common_mod', 'common');
		$this->load->model('Pharmacy_mod', 'pharmacy');
		$this->load->model('Cms_mod', 'cms');
		
		//Sets the variable $header_contents to use the slice head (/views/slices/header_contents.php)
		$this->stencil->slice('header_contents');

		//Sets the variable $header_scripts to use the slice head (/views/slices/header_scripts.php)
		$this->stencil->slice('header_scripts');
		
		//Sets the variable $footer_contents to use the slice head (/views/slices/footer_contents.php)
		$this->stencil->slice('footer_contents');
		
		//Sets the variable $footer_scripts to use the slice head (/views/slices/footer_scripts.php)
		$this->stencil->slice('footer_scripts');

		// Layout
		$this->stencil->layout('page_no_header_footer');
		
	} // public function __construct()

	public function index($pharmacy_id, $survey_ref_no, $invitation_id = ''){
		
		//Active Survey Detials
		$pharmacy_current_survey = $this->survey->get_pharmacy_current_survey($pharmacy_id,'',$survey_ref_no);
		$data['pharmacy_current_survey'] = $pharmacy_current_survey;
		
		if(!$pharmacy_current_survey){
			redirect(SURL);
		}else{

			$get_phramacy_details = $this->pharmacy->get_pharmacy_details($pharmacy_id);
			$data['phramacy_details'] = $get_phramacy_details;
			
			if($invitation_id != 'embed'){
				
				$verify_invitation = $this->survey->get_survey_invitation($pharmacy_id, $pharmacy_current_survey['id'], $invitation_id);
				$data['verify_invitation'] = $verify_invitation;
				
			}else{
				$data['embed_code'] = '1';	
			}//end if($invitation_id != 'embed')
			
			//Get Array of Questions
			$get_questionnnair_arr = $this->survey->get_questionnaire_list($get_phramacy_details['pharmacy_type']);
			$data['questionnnair_arr'] = $get_questionnnair_arr;
			
		}//end if(!$pharmacy_current_survey)
		
		//set title
		$page_title = $get_phramacy_details['pharmacy_name'].' Online '.$pharmacy_current_survey['survey_title'].' Form';
		$this->stencil->title($page_title);	
		
		//Sets the Meta data
		$this->stencil->meta(array(
			'description' => DEFAULT_META_DESCRIPTION,
			'keywords' => DEFAULT_META_KEYWORDS,
			'meta_title' => DEFAULT_TITLE
		));
		
		// Page Heading
		$page_heading = $page_title;
		$data['page_heading'] = $page_heading;
		
		$this->load->library('user_agent');

        $is_mobile = $this->agent->mobile();
		
		if($is_mobile)
			$data['is_mobile'] = 1;
		else
			$data['is_mobile'] = 0;
			
		
		$this->stencil->js('kod_scripts/survey_scripts.js');
		$this->stencil->css('built.css');

		$this->stencil->paint('dashboard/online_survey_form',$data);
		
	}//end index()
	
	public function submit_online_survey(){

		if(!$this->input->post('pharmacy_id')) redirect(SURL);
		
		extract($this->input->post());
		
		$pharmacy_current_survey = $this->survey->get_pharmacy_current_survey($pharmacy_id, $survey_id);
		
		if($inv_id)
			$verify_invitation = $this->survey->get_survey_invitation($pharmacy_id, $survey_id, $inv_id);
		
		if($pharmacy_current_survey && ($verify_invitation || $embed_code)){
			
			//Survey is Active and is Started!
			$submit_survey = $this->survey->submit_survey($pharmacy_id, $this->input->post(), $inv_id);
			
			if($submit_survey){
				 $this->session->set_flashdata('thankyou_message', '1');
				 redirect(SURL.'pages/survey-completed');	
				
			}else{
				$this->session->set_flashdata('err_message', 'Oops! Something went wrong, please try again later.');
				redirect(SURL);
			}//end if($submit_survey)	
		}else{
			
			redirect(SURL);
		}//end if($pharmacy_current_survey)
	
	}//end submit_online_survey-online-survey()
	
} // End => Ci => Class Inspector
