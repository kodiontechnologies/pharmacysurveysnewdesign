<?php
class Survey_mod extends CI_Model {
	
	function __construct(){
		 parent::__construct();
    }
	
	public function get_most_recent_survey($pharmacy_id){
		
		$this->db->dbprefix('pharmacy_survey');

		$this->db->where('pharmacy_id',$pharmacy_id);
		$this->db->order_by('id','DESC');
		$this->db->limit(0,1);

		$get = $this->db->get('pharmacy_survey');
		
		return $get->row_array();

	}//end get_most_recent_survey($pharmacy_id)
	
	//Function get_pharmacy_survey_list(): Will retirn list of Survey added by the pharmacies with any given status.
	public function get_pharmacy_survey_list($pharmacy_id, $survey_status = '', $survey_id = '', $is_started = ''){
		
		$this->db->dbprefix('pharmacy_survey');
		$this->db->where('pharmacy_id',$pharmacy_id);
		if(trim($survey_status) != '') $this->db->where('survey_status',$survey_status);
		if(trim($survey_id) != '') $this->db->where('id',$survey_id);
		
		if(trim($is_started) != '') $this->db->where('survey_start_date IS NOT NULL');
		
		$this->db->order_by('id','DESC');
		
		$get = $this->db->get('pharmacy_survey');
		
		//echo $this->db->last_query(); 		exit;
		
		if(trim($survey_id) != '') 
			return $get->row_array();
		else
			return $get->result_array();

	}//end get_pharmacy_survey_list($pharmacy_id, $survey_status = '')

	public function get_pharmacy_incomplete_survey_list($pharmacy_id){
		
		$this->db->dbprefix('pharmacy_survey');
		$this->db->where('pharmacy_id',$pharmacy_id);
		$this->db->where('survey_status','0');
		$this->db->where('survey_start_date IS NOT NULL');
		$this->db->where('survey_finish_date <', date('Y-m-d'));
		
		$this->db->order_by('id','DESC');
		
		$get = $this->db->get('pharmacy_survey');
		
		//echo $this->db->last_query(); 		exit;
		
		return $get->result_array();

	}//end get_pharmacy_incomplete_survey_list($pharmacy_id, $survey_status = '')


	//Function generate_survey_reference_no(): Generate unique SURVEY REF No, by recursive call.
	public function generate_survey_reference_no(){

		$new_ref_no = strtoupper($this->common->random_number_generator(7));
		
		$this->db->dbprefix('pharmacy_survey');
		$this->db->select('id');
		$this->db->where('survey_ref_no',$new_ref_no);
		$get = $this->db->get('pharmacy_survey');
		//echo $this->db->last_query(); 		exit;
		
		if($get->num_rows > 0)
			$this->generate_survey_reference_no();
		else
			return $new_ref_no;
	}//end generate_survey_reference_no()
	
	public function get_question_detail($question_id){
		
		$this->db->dbprefix('survey_question');
		$this->db->where('id', $question_id);
		$get = $this->db->get('survey_question');
		//echo $this->db->last_query(); 		exit;
		$question_arr = $get->row_array();
		
		return $question_arr;
		
	}//end get_questionnaire_list()

	public function get_option_detail($option_id){
		
		$this->db->dbprefix('survey_question_options');
		$this->db->where('id', $option_id);
		$get = $this->db->get('survey_question_options');
		//echo $this->db->last_query(); 		exit;
		$question_arr = $get->row_array();
		
		return $question_arr;
		
	}//end get_questionnaire_list()
	
	//get_questionnaire_list() : This function will create and return the questionnaire array. The questions having sub_questions will not have the global option section
	public function get_questionnaire_list($pharmacy_type){
		
		$online_pharmacy_survey = ($pharmacy_type == 'O') ? '1' : '0';
		
		$this->db->dbprefix('survey_question');
		$this->db->where('online_pharmacy_survey',$online_pharmacy_survey);
		$this->db->where('status',1);
		$this->db->order_by('display_order','ASC');
		$get = $this->db->get('survey_question');
		//echo $this->db->last_query(); 		exit;
		$question_arr = $get->result_array();
		
		//print_this($question_arr);
		
		$questionnaire_arr = array();
		for($i=0;$i<count($question_arr);$i++){
			
			//Get Questions Options 
			$this->db->dbprefix('survey_question_options');
			$this->db->where('question_id',filter_string($question_arr[$i]['id']));
			$get = $this->db->get('survey_question_options');
			//echo $this->db->last_query(); 		exit;
			$sub_options_arr = $get->result_array();
			

			if(filter_string($question_arr[$i]['parent_id']) == 0){
				//Prepar the parent array when Parent = 0
				$questionnaire[filter_string($question_arr[$i]['id'])]['question_id'] = filter_string($question_arr[$i]['id']);
				$questionnaire[filter_string($question_arr[$i]['id'])]['question'] = filter_string($question_arr[$i]['questions']);
				$questionnaire[filter_string($question_arr[$i]['id'])]['question_code'] = filter_string($question_arr[$i]['question_code']);
				$questionnaire[filter_string($question_arr[$i]['id'])]['sub_notes'] = filter_string($question_arr[$i]['sub_notes']);
				
				$questionnaire[filter_string($question_arr[$i]['id'])]['sub_question'] = array();
				
				for($j=0;$j<count($sub_options_arr);$j++){
					$questionnaire[filter_string($question_arr[$i]['id'])]['options'][filter_string($sub_options_arr[$j]['id'])] = filter_string($sub_options_arr[$j]['options']);
				}//end for($j=0;$j<count($sub_options_arr);$j++)
				
			}else{
				
				//Question is not a Parent, but a child
				$questionnaire[filter_string($question_arr[$i]['parent_id'])]['sub_question'][filter_string($question_arr[$i]['id'])]['question'] = filter_string($question_arr[$i]['questions']);
				
				$questionnaire[filter_string($question_arr[$i]['parent_id'])]['sub_question'][filter_string($question_arr[$i]['id'])]['question_code'] = filter_string($question_arr[$i]['question_code']);
				
				for($j=0;$j<count($sub_options_arr);$j++){
					
					$questionnaire[filter_string($question_arr[$i]['parent_id'])]['sub_question'][filter_string($question_arr[$i]['id'])]['options'][filter_string($sub_options_arr[$j]['id'])] = filter_string($sub_options_arr[$j]['options']);
					
					$questionnaire[filter_string($question_arr[$i]['parent_id'])]['sub_question_options'][$j] = filter_string($sub_options_arr[$j]['options']);
				}//end for($j=0;$j<count($sub_options_arr);$j++)
				
			}//end if(filter_string($question_arr[$i]['parent_id']) == 0)
			
		}//end for($i=0;$i<count($question_arr);$i++)
		
		return $questionnaire;
		
	}//end get_questionnaire_list()
	
	//Function get_pharmacy_current_survey(): Return the survey which is currently in execution.
	public function get_pharmacy_current_survey($pharmacy_id, $survey_id = '', $survey_ref_no = '' ){

		$this->db->dbprefix('pharmacy_survey');
		
		$this->db->where('pharmacy_survey.pharmacy_id', $pharmacy_id);
		if(trim($survey_id) != '') $this->db->where('pharmacy_survey.id', $survey_id);
		if(trim($survey_ref_no) != '') $this->db->where('pharmacy_survey.survey_ref_no', $survey_ref_no);
		
		$this->db->where('pharmacy_survey.survey_status !=', '3'); //Not completed yet
		$this->db->where('pharmacy_survey.survey_finish_date  > "'.date('Y-m-d').'"');
		$this->db->where('pharmacy_survey.survey_start_date IS NOT NULL');
		
		$get = $this->db->get('pharmacy_survey');

		//echo $this->db->last_query(); 		exit;
		
		return $get->row_array();
		
	}//end get_pharmacy_current_survey($pharmacy_id)

	//Function start_pharmacy_survey(): Starting the Survey by updating the new survey into the datavase
	public function start_pharmacy_survey($pharmacy_id, $data){
		
		extract($data);

		$survey_start_date = date('Y-m-d');
		$radio_no_of_survey_arr = explode('|',$radio_no_of_survey);

		$upd_data = array(
			
			'survey_start_date' => $this->db->escape_str(trim($survey_start_date)),
			'min_no_of_surveys' => $this->db->escape_str(trim($radio_no_of_survey_arr[1])),
			'survey_volume' => $this->db->escape_str(trim($radio_no_of_survey_arr[0])),
		);
		
		//Inserting rota requested leaves data into the database. 
		$this->db->dbprefix('pharmacy_survey');
		$this->db->where('id', $survey_id);
		$this->db->where('pharmacy_id', $pharmacy_id);
		$upd_into_db = $this->db->update('pharmacy_survey', $upd_data);
		
		//echo $this->db->last_query(); 		exit;
		
		if($upd_into_db)
			return true;
		else
			return false;

	}//end start_pharmacy_survey($pharmacy_id, $data)
	
	//Function submit_survey($data): Submit the survey into the database.
	public function submit_survey($pharmacy_id, $data, $invitation_id = ''){

		extract($data);
		
		$created_date = date('Y-m-d G:i:s');
		$created_by_ip = $this->input->ip_address();
		
		if($invitation_id != ''){
			
			$delete_inv = $this->survey->delete_survey_invitation($invitation_id);
			
		}//end delete invitation from database
		
		//$pharmacy_current_survey = $this->survey->get_pharmacy_current_survey($pharmacy_id, $survey_id);
		$pharmacy_survey_data = $this->survey->get_pharmacy_survey_list($pharmacy_id, '', $survey_id);
		
		$submit_type = ($submit_type) ? $submit_type : 'ONLINE';
		
		$ins_data = array(
		
			'pharmacy_id' => $this->db->escape_str(trim($pharmacy_id)),
			'survey_id' => $this->db->escape_str(trim($pharmacy_survey_data['id'])),
			'survey_ref_no' => $this->db->escape_str(trim($pharmacy_survey_data['survey_ref_no'])),
			'survey_year' => $this->db->escape_str(trim($pharmacy_survey_data['survey_year'])),
			'submit_type' => $this->db->escape_str(trim($submit_type)),
			'created_date' => $this->db->escape_str(trim($created_date)),
			'created_by_ip' => $this->db->escape_str(trim($created_by_ip))
		);
		//print_this($ins_data); exit;
		//echo "i am here"; exit;
		$this->db->dbprefix('submitted_surveys');
		$ins_into_db = $this->db->insert('submitted_surveys', $ins_data);
		$new_survey_id = $this->db->insert_id();
		
		
		if(count($q) > 0){
			
			foreach($q as $question_id => $option_selected){
				
				$get_question_detail = $this->get_question_detail($question_id);
				$get_option_detail = $this->get_option_detail($option_selected);
				
				if($question_id == 1 || $question_id == 36){
					
					if($option_selected != 4)
						$other_reason_txt_1 = '';
					else
						$other_reason_txt_1 = substr(strip_tags($other_reason_txt),0,250);
					
					if($option_selected != 226)
						$other_reason_txt_1 = '';
					else
						$other_reason_txt_1 = substr(strip_tags($other_reason_txt),0,250);
					
					$ins_array[] = 
					   array(
							'survey_submitted_id' => $this->db->escape_str(trim($new_survey_id)),
							'survey_id' => $this->db->escape_str(trim($pharmacy_survey_data['id'])),
							'survey_ref_no' => $this->db->escape_str(trim($pharmacy_survey_data['survey_ref_no'])),
							'question_id' => $this->db->escape_str(trim($question_id)),
							'question_txt' => $this->db->escape_str(trim($get_question_detail['questions'])),
							'question_sub_notes' => $this->db->escape_str(trim($get_question_detail['sub_notes'])),
							'option_id' => $this->db->escape_str(trim($option_selected)) ,
							'option_val' => $this->db->escape_str(trim($get_option_detail['options'])),
							'option_txt' => $this->db->escape_str(trim($other_reason_txt_1)) ,
					   );
					
				}elseif($question_id == 10 || $question_id == 45){
					
					$ins_array[] = 
					   array(
							'survey_submitted_id' => $this->db->escape_str(trim($new_survey_id)),
							'survey_id' => $this->db->escape_str(trim($pharmacy_survey_data['id'])),
							'survey_ref_no' => $this->db->escape_str(trim($pharmacy_survey_data['survey_ref_no'])),
							'question_id' => $this->db->escape_str(trim($question_id)),
							'question_txt' => $this->db->escape_str(trim($get_question_detail['questions'])),
							'question_sub_notes' => $this->db->escape_str(trim($get_question_detail['sub_notes'])),
							'option_id' => NULL,
							'option_val' => NULL,
							'option_txt' => $this->db->escape_str(trim($option_selected)),
					   );
					   
				}else{
					
					$ins_array[] = array(
					   
							'survey_submitted_id' => $this->db->escape_str(trim($new_survey_id)),
							'survey_id' => $this->db->escape_str(trim($pharmacy_survey_data['id'])),
							'survey_ref_no' => $this->db->escape_str(trim($pharmacy_survey_data['survey_ref_no'])),
							'question_id' => $this->db->escape_str(trim($question_id)),
							'question_txt' => $this->db->escape_str(trim($get_question_detail['questions'])),
							'question_sub_notes' => $this->db->escape_str(trim($get_question_detail['sub_notes'])),
							'option_id' => $this->db->escape_str(trim($option_selected)),
							'option_val' => $this->db->escape_str(trim($get_option_detail['options'])),
							'option_txt' => '',
					   );
				}
			}//end foreach($q as $question_id => $option_selected)
			
		} //end if(count($q) > 0)

		$this->db->dbprefix('surveys_details');
		$ins_data = $this->db->insert_batch('surveys_details', $ins_array); 
		//echo $this->db->last_query(); 		exit;
		
		
		if($ins_data)
			return true;
		else
			return false;
		
	}//end submit_survey($data)
	
	//Function get_survey_submitted_count(): Returns the number of Survey submitted so far
	public function get_survey_submitted_count($pharmacy_id, $survey_id, $submit_type = ''){
		

		$this->db->dbprefix('submitted_surveys');
		$this->db->select('COUNT(id) AS total_no_of_survey_submitted');
		
		$this->db->where('pharmacy_id', $pharmacy_id);
		$this->db->where('survey_id', $survey_id);
		if(trim($submit_type)!= '') $this->db->where('submit_type', $submit_type);
		
		$get = $this->db->get('submitted_surveys');

		//echo $this->db->last_query(); 		exit;
		$total_count =  $get->row_array();
		
		return $total_count['total_no_of_survey_submitted'];
		
		
	}//end get_survey_submitted_count($pharmacy_id, $survey_id)

	//Function: email_survey_link($data): This function send survet link to the friend email address.
	public function email_survey_link($pharmacy_id, $data){
		
		extract($data);

		$created_date = date('Y-m-d G:i:s');
		$created_by_ip = $this->input->ip_address();
		
		//print_this($data); exit;
		
		$email_address_arr = explode(',',$email_addresses);
		$email_address_arr = array_map('trim',$email_address_arr);
		
		for($i=0;$i<count($email_address_arr);$i++){

			if(!filter_var($email_address_arr[$i], FILTER_VALIDATE_EMAIL) === false){
				
				//Insert the invitation entry into the database.
				$ins_data = array(
					
					'survey_id' => $this->db->escape_str(trim($survey_id)),
					'pharmacy_id' => $this->db->escape_str(trim($pharmacy_id)),
					'email_address' => $this->db->escape_str(trim($email_address_arr[$i])),
					'created_date' => $this->db->escape_str(trim($created_date)),
					'created_by_ip' => $this->db->escape_str(trim($created_by_ip)),
				);
				
				//Update prescribers data into the database. 
				$this->db->dbprefix('survey_invitations');
				$into_db = $this->db->insert('survey_invitations', $ins_data);
				
				$inv_id = $this->db->insert_id();
				
				$this->load->model('email_mod','email_template');
				
				$email_body_arr = $this->email_template->get_email_template(3);
				
				$email_subject = $email_body_arr['email_subject'];
				$email_subject = str_replace('[PHARMACY_NAME]',$pharmacy_name,$email_subject);
				
				$FROM_EMAIL_TXT = 'FROM_EMAIL_TXT';
				$email_from_txt = get_global_settings($FROM_EMAIL_TXT);
				$email_from_txt = filter_string($email_from_txt['setting_value']);
				
				$subject = filter_string($email_subject);
				
				$text_to_find = SURL.'online-survey/'.$pharmacy_id.'/'.$survey_ref_code;
				$text_to_replace = SURL.'online-survey/'.$pharmacy_id.'/'.$survey_ref_code.'/'.$inv_id;
				
				$email_body = nl2br(filter_string($friend_message));
				$email_body = str_replace($text_to_find, $text_to_replace, $email_body);
				$email_body = filter_string($email_body);
				
				$SES_SENDER = 'SES_SENDER';
				$ses_sender = get_global_settings($SES_SENDER);
				$ses_sender = trim($ses_sender['setting_value']);

				$to_email_address = trim($email_address_arr[$i]); //'twister787@gmail.com';
				
				$send_email = send_email($to_email_address, $ses_sender, $email_from_txt, $email_subject, $email_body);
				////////////////////////////////////
						
			}else{
				
			}//end if(!filter_var($email_address_arr[$i], FILTER_VALIDATE_EMAIL) === false)
						
		}//end for($i=0;$i<count($email_address_arr);$i++)
		
		return true;
		
	}//end email_survey_link($data)


	//Function get_survey_questionnaire_details(): Get the Record from the Questinnair Table to get teh question record
	public function get_survey_questionnaire_details($question_id){

		$this->db->dbprefix('survey_question');
		$this->db->where('id',$question_id);
		
		$get = $this->db->get('survey_question');
		
		//echo $this->db->last_query(); 		exit;
		return $get->row_array();
		
	}//end get_survey_package($survey_id)
	
	//Function get_survey_options_stats(): Returns the stats of the given question number with the specified option
	public function get_survey_options_stats($survey_id, $question_id, $option_id){
		
		$this->db->dbprefix('surveys_details');
		$this->db->select("COUNT(id) AS total_option_attempt");
		$this->db->where('survey_id',  $survey_id);
		$this->db->where('question_id',  $question_id);
		$this->db->where('option_id',  $option_id);

		$get = $this->db->get('surveys_details');
		
		//echo $this->db->last_query(); 		exit;
		$row = $get->row_array();
		return $row['total_option_attempt'];
		
	}//end get_survey_options_stats($pharmacy_id, $survey_id, $question_id, $option_id)
	
	//Function get_survey_question_stats(): This wil return question complete statitics against the Survey ref no. How many attempts were conducted and what were the options selected. If question id is empty will return the stats of all questions!
	public function get_survey_question_stats($pharmacy_id, $survey_id, $survey_ref_no, $question_id = '', $pharmacy_type){
		
		$online_pharmacy_survey = ($pharmacy_type == 'O') ? '1' : '0';
		
		$this->db->dbprefix('survey_question');
		$this->db->select("survey_question.*,(SELECT count(id) from kod_submitted_surveys WHERE kod_submitted_surveys.survey_id = '".$survey_id."') AS total_survey_attempt");
		
		if(trim($question_id) != '') $this->db->where('survey_question.id',$question_id);
		
		$this->db->where('survey_question.online_pharmacy_survey',$online_pharmacy_survey);

		$get = $this->db->order_by('display_order','ASC');
		$get = $this->db->get('survey_question');
		//echo $this->db->last_query(); 		exit;
		
		$questionnaire_arr = array();
		
			if($question_id !=''){
				
				$question_arr = $get->row_array();
	
				//Get Questions Options 
				$this->db->dbprefix('survey_question_options');
				$this->db->select("survey_question_options.*,(SELECT count(id) from kod_surveys_details WHERE kod_surveys_details.option_id = kod_survey_question_options.id AND kod_surveys_details.survey_ref_no = '".$survey_ref_no."') AS total_options_attempt");
				
				$this->db->where('survey_question_options.question_id',filter_string($question_id));
				
				$get = $this->db->get('survey_question_options');
				//echo $this->db->last_query(); 		exit;

				$closing_comments_arr = $this->survey->survey_question_comments_available($pharmacy_id, $survey_id, $question_id);
				$sub_options_arr = $get->result_array();
				
				$questionnaire['parent_id'] = filter_string($question_arr['parent_id']);
				$questionnaire['total_survey_attempt'] = filter_string($question_arr['total_survey_attempt']);
				$questionnaire['question_id'] = filter_string($question_arr['id']);
				$questionnaire['closing_comments_arr'] = $closing_comments_arr;
				
				if(filter_string($question_arr['parent_id']) == 0){
					//Prepar the parent array when Parent = 0
					
					$questionnaire['question_id'] = filter_string($question_arr['id']);
					$questionnaire['question'] = filter_string($question_arr['questions']);
					$questionnaire['sub_notes'] = filter_string($question_arr['sub_notes']);
		
					if(filter_string($question_arr['id']) != 10 && filter_string($question_arr['id']) != 45){
		
						$total_options_attempted = 0;
						for($j=0;$j<count($sub_options_arr);$j++){
							$total_options_attempted += filter_string($sub_options_arr[$j]['total_options_attempt']);
							$questionnaire['options_attempts'][filter_string($sub_options_arr[$j]['options'])] = filter_string($sub_options_arr[$j]['total_options_attempt']);
							
							
						}//end for($j=0;$j<count($sub_options_arr);$j++)
						$questionnaire['total_options_attempted'] = $total_options_attempted;
						
						//Get Reason of question 1, if 4th option is selected.
						if(filter_string($question_arr['id']) == 1){

							//Get Question 10, comments
							$this->db->dbprefix('surveys_details');
							$this->db->select("option_txt");
							$this->db->where('survey_ref_no',$survey_ref_no);
							$this->db->where('question_id',filter_string($question_arr['id']));
							$this->db->where('option_id = 4');
							$this->db->where('trim(option_txt) != "" ');

							$get = $this->db->get('surveys_details');
							//echo $this->db->last_query(); 		exit;
							$other_reason_result = $get->result_array();
	
							$questionnaire['other_reasons_txt_arr'] = $other_reason_result;
								
						}//end if(filter_string($question_arr['id']) == 1)
						
					}else{
						
						$comments_qry = "
						SELECT  
						(select count(id) AS commented from kod_surveys_details WHERE survey_ref_no = '".$survey_ref_no."' AND question_id = '".$question_arr['id']."' AND option_txt <> '' ) AS commented,
						(select count(id) AS non_commented from kod_surveys_details WHERE survey_ref_no = '".$survey_ref_no."' AND question_id = '".$question_arr['id']."'  AND option_txt = '' ) AS non_commented";
		
						$rs  = $this->db->query($comments_qry);
						$result = $rs->row_array();	
						
						$questionnaire['total_commented'] = filter_string($result['commented']);			
						$questionnaire['total_non_commented'] = filter_string($result['non_commented']);			

						//Get Question 10, comments
						$this->db->dbprefix('surveys_details');
						$this->db->select("option_txt");
						$this->db->where('trim(option_txt) != "" ');
						$this->db->where('survey_ref_no',$survey_ref_no);
						$this->db->where('question_id',filter_string($question_arr['id']));
						
						$get = $this->db->get('surveys_details');
						//echo $this->db->last_query(); 		exit;
						$comments_result = $get->result_array();

						$questionnaire['comments'] = $comments_result;
						
					}//end if(filter_string($question_arr['id']) != 10)
					
				}else{
					
					//Question is not a Parent, but a child
					$questionnaire['question'] = filter_string($question_arr['questions']);
					
					//Get teh parent question
					$get_parent_question = $this->survey->get_survey_questionnaire_details(filter_string($question_arr['parent_id']));
					$questionnaire['parent_question'] = filter_string($get_parent_question['questions']);
					$total_options_attempted = 0;
					
					for($j=0;$j<count($sub_options_arr);$j++){
						
						$total_options_attempted += filter_string($sub_options_arr[$j]['total_options_attempt']);
						$questionnaire['options_attempts'][filter_string($sub_options_arr[$j]['options'])] = filter_string($sub_options_arr[$j]['total_options_attempt']);
						
					}//end for($j=0;$j<count($sub_options_arr);$j++)
					
					$questionnaire['total_options_attempted'] = $total_options_attempted;
					
				}//end if(filter_string($question_arr['parent_id']) == 0)
				
		}else{

				$question_arr = $get->result_array();	
				//$skip_parent_questions_arr = array(4,5,6,7);
				$skip_parent_questions_arr = array();

				for($k=0;$k<count($question_arr);$k++){
					
					$question_id = $question_arr[$k]['id'];
					
					//Get Questions Options 
					$this->db->dbprefix('survey_question_options');
					$this->db->select("survey_question_options.*,(SELECT count(id) from kod_surveys_details WHERE kod_surveys_details.option_id = kod_survey_question_options.id AND kod_surveys_details.survey_ref_no = '".$survey_ref_no."') AS total_options_attempt");
					
					$this->db->where('survey_question_options.question_id',filter_string($question_id));
					
					$get = $this->db->get('survey_question_options');
					//echo $this->db->last_query(); echo '<br><br>';
					$sub_options_arr = $get->result_array();
					
					$question_id = filter_string($question_arr[$k]['id']);
					
					if(!in_array($question_id,$skip_parent_questions_arr)){
	
						$questionnaire[$question_id]['parent_id'] = filter_string($question_arr[$k]['parent_id']);
						$questionnaire[$question_id]['question_code'] = filter_string($question_arr[$k]['questin_code']);
						$questionnaire[$question_id]['total_survey_attempt'] = filter_string($question_arr[$k]['total_survey_attempt']);
						$questionnaire[$question_id]['question_id'] = filter_string($question_arr[$k]['id']);
	
						$closing_comments_arr = $this->survey->survey_question_comments_available($pharmacy_id, $survey_id, $question_id);
						
						if($closing_comments_arr){
							$questionnaire[$question_id]['closing_question_comments'] = $closing_comments_arr;
						}
						if(filter_string($question_arr[$k]['parent_id']) == 0){
		
							//Prepar the parent array when Parent = 0
							$questionnaire[$question_id]['question_id'] = filter_string($question_arr[$k]['id']);
							$questionnaire[$question_id]['question'] = filter_string($question_arr[$k]['questions']);
							$questionnaire[$question_id]['sub_notes'] = filter_string($question_arr[$k]['sub_notes']);
							
							$questionnaire[$question_id]['question_code'] = $question_arr[$k]['question_code'];
				
							if(filter_string($question_id) != 10 && filter_string($question_id) != 45){
				
								$total_options_attempted = 0;
								
								for($j=0;$j<count($sub_options_arr);$j++){
									
									$total_options_attempted += filter_string($sub_options_arr[$j]['total_options_attempt']);
									$questionnaire[$question_id]['options_attempts'][filter_string($sub_options_arr[$j]['options'])] = filter_string($sub_options_arr[$j]['total_options_attempt']);
								}//end for($j=0;$j<count($sub_options_arr);$j++)
								
								$questionnaire[$question_id]['total_options_attempted'] = $total_options_attempted;
								
								//Get Reason of question 1, if 4th option is selected.
								if(filter_string($question_id) == 1){
		
									//Get Question 10, comments
									$this->db->dbprefix('surveys_details');
									$this->db->select("option_txt");
									$this->db->where('survey_ref_no',$survey_ref_no);
									$this->db->where('question_id',filter_string($question_arr[$k]['id']));
									$this->db->where('option_id = 4');
									$this->db->where('trim(option_txt) != "" ');
		
									$get = $this->db->get('surveys_details');
									//echo $this->db->last_query(); 		exit;
									$other_reason_result = $get->result_array();
			
									$questionnaire[$question_id]['other_reasons_txt_arr'] = $other_reason_result;
										
								}//end if(filter_string($question_arr['id']) == 1)
							
							}else{
								
								$comments_qry = "
								SELECT  
								(select count(id) AS commented from kod_surveys_details WHERE survey_ref_no = '".$survey_ref_no."' AND question_id = ".$question_id." AND option_txt <> '' ) AS commented,
								(select count(id) AS non_commented from kod_surveys_details WHERE survey_ref_no = '".$survey_ref_no."' AND question_id = ".$question_id." AND option_txt = '' ) AS non_commented";
				
								$rs  = $this->db->query($comments_qry);
								$result = $rs->row_array();	
								
								$questionnaire[$question_id]['total_commented'] = filter_string($result['commented']);			
								$questionnaire[$question_id]['total_non_commented'] = filter_string($result['non_commented']);			
	
								//Get Question 10, comments
								$this->db->dbprefix('surveys_details');
								$this->db->select("option_txt");
								$this->db->where('trim(option_txt) != "" ');
								$this->db->where('survey_ref_no',$survey_ref_no);
								$this->db->where('question_id',$question_id);
								
								$get = $this->db->get('surveys_details');
								//echo $this->db->last_query(); 		exit;
								$comments_result = $get->result_array();
								$questionnaire[$question_id]['comments'] = $comments_result;
								
							}//end if(filter_string($question_arr[$k]['id']) != 10)
							
						}else{
							
							//Question is not a Parent, but a child
							
							$questionnaire[$question_arr[$k]['parent_id']]['children'][$question_id]['question'] = filter_string($question_arr[$k]['questions']);
							//$questionnaire[$question_id]['question'] = filter_string($question_arr[$k]['questions']);
							
							//Get teh parent question
							$get_parent_question = $this->survey->get_survey_questionnaire_details(filter_string($question_arr[$k]['parent_id']));
							$questionnaire[$question_arr[$k]['parent_id']]['children'][$question_id]['parent_question'] = filter_string($get_parent_question['questions']);
							//$questionnaire[$question_id]['parent_question'] = filter_string($get_parent_question['questions']);
							$total_options_attempted = 0;
							
							for($j=0;$j<count($sub_options_arr);$j++){
								$total_options_attempted += filter_string($sub_options_arr[$j]['total_options_attempt']);
								//$questionnaire[$question_id]['options_attempts'][filter_string($sub_options_arr[$j]['options'])] = filter_string($sub_options_arr[$j]['total_options_attempt']);
								$questionnaire[$question_arr[$k]['parent_id']]['children'][$question_id]['options_attempts'][filter_string($sub_options_arr[$j]['options'])] = filter_string($sub_options_arr[$j]['total_options_attempt']);
								
							}//end for($j=0;$j<count($sub_options_arr);$j++)
							
							//$questionnaire[$question_id]['total_options_attempted'] = $total_options_attempted;
							$questionnaire[$question_arr[$k]['parent_id']]['children'][$question_id]['total_options_attempted'] = $total_options_attempted;
							$questionnaire[$question_arr[$k]['parent_id']]['children'][$question_id]['question_code'] = $question_arr[$k]['question_code'];
							
							$closing_comments_arr = $this->survey->survey_question_comments_available($pharmacy_id, $survey_id, $question_id);
							if($closing_comments_arr)
								$questionnaire[$question_arr[$k]['parent_id']]['children'][$question_id]['closing_question_comments'] = $closing_comments_arr;
							
						}//end if(filter_string($question_arr[$k]['parent_id']) == 0)	
						
					}//end if(!in_array($question_arr[$k]['id'],$skip_parent_questions_arr)
					
				}//end for($k=0$k<count($question_arr);$k++)
			
		}//end if($question_id !='')
		
		return $questionnaire;
		
	}//end get_survey_question_stats($survey_id,$question_id)
	
	//Function get_survey_question_closing_comments(): This function returns the SVG code and closing comments of a survey question
	public function get_survey_question_closing_comments($pharmacy_id, $survey_id,$question_id = '', $well_performed = '', $bad_performed = ''){

		$this->db->dbprefix('survey_closing_comments');

		$this->db->where('pharmacy_id',$pharmacy_id);
		$this->db->where('survey_id',$survey_id);
		if(trim($question_id) != '') $this->db->where('question_id',$question_id);
		if(trim($well_performed) != '') $this->db->where('well_performed','1');
		if(trim($bad_performed) != '') $this->db->where('bad_performed','1');

		$get = $this->db->get('survey_closing_comments');
		
		if(trim($well_performed) != '' || trim($bad_performed) != '')
			return $get->result_array();
		else
			return $get->row_array();
		
	}//end get_survey_question_closing_comments($pharmacy_id, $survey_id,$question_id = '')
	
	//change_survey_status(); Change status of survey to new one as defined
	public function change_survey_status($pharmacy_id, $survey_id, $new_status, $data = ''){

		$created_date = date('Y-m-d G:i:s');
		$created_by_ip = $this->input->ip_address();

		$upd_data = array(
			'survey_status' => $this->db->escape_str(trim($new_status))
		);
		
		//Update prescribers data into the database. 
		$this->db->dbprefix('pharmacy_survey');
		$this->db->where('id', $survey_id);
		$this->db->where('pharmacy_id', $pharmacy_id);
		$this->db->update('pharmacy_survey', $upd_data);
		
		if($new_status == 3){

			//Its a survey complete process. Add the comments for best and bad question into the databse.
			extract($data);
			
			//Saving the SVG Sources into the Database
			foreach($svg as $question_id => $svg_db_code){
				
				//Update code	
				$upd_data = array(
					'svg_code' => $this->db->escape_str(trim($svg_db_code)),
					'modified_date' => $this->db->escape_str(trim($created_date)),
					'modified_by_ip' => $this->db->escape_str(trim($created_by_ip))
				);
				
				//Update prescribers data into the database.
				$this->db->where('question_id', $question_id); 
				$this->db->where('survey_id', $survey_id); 
				$this->db->dbprefix('survey_closing_comments');
				$this->db->update('survey_closing_comments', $upd_data);
				
				if($question_id == 9 || $question_id == 44){

					//Update code	
					$upd_data = array(
						'svg_poster_code' => $this->db->escape_str(trim($svg_poster_9)),
						'modified_date' => $this->db->escape_str(trim($created_date)),
						'modified_by_ip' => $this->db->escape_str(trim($created_by_ip))
					);
					
					//Update prescribers data into the database.
					$this->db->where('question_id', $question_id); 
					$this->db->where('survey_id', $survey_id); 
					$this->db->dbprefix('survey_closing_comments');
					$this->db->update('survey_closing_comments', $upd_data);
					
					//echo $this->db->last_query(); exit;
					
				}//end if($question_id == 9 || $question_id == 44)
				
			}//end foreach($svg => $question_id => $svg_data)
			
			//Well Performed
			if($well_perf_q_1){

				$upd_data = array(
					'well_performed' => $this->db->escape_str(trim('1')),
					'well_performed_comments' => $this->db->escape_str(trim($well_perf_q_1_comment)),
				);
				
				//Update prescribers data into the database. 
				$this->db->dbprefix('survey_closing_comments');
				$this->db->where('survey_id', $survey_id);
				$this->db->where('pharmacy_id', $pharmacy_id);
				$this->db->where('question_id', $well_perf_q_1);
				$this->db->update('survey_closing_comments', $upd_data);
				
			}//end if($well_perf_q_1)
			
			if($well_perf_q_2){

				$upd_data = array(
					'well_performed' => $this->db->escape_str(trim('1')),
					'well_performed_comments' => $this->db->escape_str(trim($well_perf_q_2_comment)),
				);
				
				//Update prescribers data into the database. 
				$this->db->dbprefix('survey_closing_comments');
				$this->db->where('survey_id', $survey_id);
				$this->db->where('pharmacy_id', $pharmacy_id);
				$this->db->where('question_id', $well_perf_q_2);
				$this->db->update('survey_closing_comments', $upd_data);
				
			}//end if($well_perf_q_2)
			
			if($well_perf_q_3){

				$upd_data = array(
					'well_performed' => $this->db->escape_str(trim('1')),
					'well_performed_comments' => $this->db->escape_str(trim($well_perf_q_3_comment)),
				);
				
				//Update prescribers data into the database. 
				$this->db->dbprefix('survey_closing_comments');
				$this->db->where('survey_id', $survey_id);
				$this->db->where('pharmacy_id', $pharmacy_id);
				$this->db->where('question_id', $well_perf_q_3);
				$this->db->update('survey_closing_comments', $upd_data);
				
			}//end if($well_perf_q_3)
			
			//Bad Performed
			if($well_bad_q_1){

				$upd_data = array(
					'bad_performed' => $this->db->escape_str(trim('1')),
					'bad_performed_comments' => $this->db->escape_str(trim($well_bad_q_1_comment)),
				);
				
				//Update prescribers data into the database. 
				$this->db->dbprefix('survey_closing_comments');
				$this->db->where('survey_id', $survey_id);
				$this->db->where('pharmacy_id', $pharmacy_id);
				$this->db->where('question_id', $well_bad_q_1);
				$this->db->update('survey_closing_comments', $upd_data);
				
			}//end if($well_bad_q_1)
			
			if($well_bad_q_2){

				$upd_data = array(
					'bad_performed' => $this->db->escape_str(trim('1')),
					'bad_performed_comments' => $this->db->escape_str(trim($well_bad_q_2_comment)),
				);
				
				//Update prescribers data into the database. 
				$this->db->dbprefix('survey_closing_comments');
				$this->db->where('survey_id', $survey_id);
				$this->db->where('pharmacy_id', $pharmacy_id);
				$this->db->where('question_id', $well_bad_q_2);
				$this->db->update('survey_closing_comments', $upd_data);
				
			}//end if($well_bad_q_2)
			
			if($well_bad_q_3){

				$upd_data = array(
					'bad_performed' => $this->db->escape_str(trim('1')),
					'bad_performed_comments' => $this->db->escape_str(trim($well_bad_q_3_comment)),
				);
				
				//Update prescribers data into the database. 
				$this->db->dbprefix('survey_closing_comments');
				$this->db->where('survey_id', $survey_id);
				$this->db->where('pharmacy_id', $pharmacy_id);
				$this->db->where('question_id', $well_bad_q_3);
				$this->db->update('survey_closing_comments', $upd_data);
				
			}//end if($well_bad_q_3)
			
		}//end if($new_status == 3)

		return true;
		
	}//end change_survey_status($pharmacy_id, $survey_id, $new_status)
	
	//Function survey_question_comments_available(): Return the survey questin comemail_survey_linkments if found.
	public function survey_question_comments_available($pharmacy_id, $survey_id, $question_id){

		$this->db->dbprefix('survey_closing_comments');
		$this->db->where('pharmacy_id',$pharmacy_id);
		$this->db->where('survey_id',$survey_id);
		$this->db->where('question_id',$question_id);
		
		$get = $this->db->get('survey_closing_comments');
		
		//echo $this->db->last_query().'<br>'; 
		
		return $get->row_array();
		
	}//end survey_question_comments_available($pharmacy_id, $survey_id, $question_id)
	
	//Function save_survey_question_comments(): Save the survey comments into the database.
	public function save_survey_question_comments($pharmacy_id, $survey_id, $question_id, $comments){

		$created_date = date('Y-m-d G:i:s');
		$created_by_ip = $this->input->ip_address();
		
		//Check if already comments is saved against the survey question. If Yes then will update, otherwise will insert into the table
		$comment_already_available = $this->survey->survey_question_comments_available($pharmacy_id, $survey_id, $question_id);
		
		if($comment_already_available){

			//Update
			$upd_data = array(
				'comments' => $this->db->escape_str(trim($comments)),
				'modified_date' => $this->db->escape_str(trim($created_date)),
				'modified_by_ip' => $this->db->escape_str(trim($modified_by_ip)),
			);
			
			//Update prescribers data into the database. 
			$this->db->dbprefix('survey_closing_comments');
			$this->db->where('survey_id', $survey_id);
			$this->db->where('question_id', $question_id);
			$this->db->where('pharmacy_id', $pharmacy_id);
			$into_db = $this->db->update('survey_closing_comments', $upd_data);
			
		}else{

			//Insert
			$ins_data = array(
				'comments' => $this->db->escape_str(trim($comments)),
				'survey_id' => $this->db->escape_str(trim($survey_id)),
				'question_id' => $this->db->escape_str(trim($question_id)),
				'pharmacy_id' => $this->db->escape_str(trim($pharmacy_id)),
				'created_date' => $this->db->escape_str(trim($created_date)),
				'created_by_ip' => $this->db->escape_str(trim($created_by_ip)),
			);
			
			//Update prescribers data into the database. 
			$this->db->dbprefix('survey_closing_comments');
			$into_db = $this->db->insert('survey_closing_comments', $ins_data);
			
		}//end if($comment_already_available)
		
		if($into_db)
			return true;
		else
			return false;
	
	}//end save_survey_question_comments($pharmacy_id, $survey_id, $question_id, $comments)
	
	//Function get_submitted_surveys_list(): Get Survey Submitted List. It does not include the options
	public function get_submitted_surveys_list($pharmacy_id, $survey_id){

		$this->db->dbprefix('submitted_surveys');
		$this->db->where('pharmacy_id',$pharmacy_id);
		$this->db->where('survey_id',$survey_id);
		
		$get = $this->db->get('submitted_surveys');
		
		//echo $this->db->last_query(); 		exit;
		
		return $get->result_array();
		
	}//end get_submitted_surveys_list($pharmacy_id, $survey_id)
	
	public function get_survey_submitted_answers($survey_submitted_id, $question_id = ''){
		
		$this->db->dbprefix('surveys_details');
		$this->db->where('survey_submitted_id',$survey_submitted_id);
		$this->db->where('question_id',$question_id);
		
		$get = $this->db->get('surveys_details');
		
		//echo $this->db->last_query(); 		exit;
		
		if(trim($question_id)!= '')
			return $get->row_array();
		else
			return $get->result_array();

	}//end get_survey_submitted_answers($survey_submitted_id, $question_id = '')

	// Start =>public function add_new_survey($pharmacy_id)
	public function add_new_survey($pharmacy_id, $next_year = ''){

		$current_survey_date = date('m/d');

		$SURVEY_END_MONTH = 'SURVEY_END_MONTH';
		$survey_end_global_value = get_global_settings($SURVEY_END_MONTH); //Set from the Global Settings

		$next_survey_end_date = filter_string($survey_end_global_value['setting_value']);
		
		if($next_year == '1'){

			$next_survey_end = strtotime("$next_survey_end_date +1 year"); 
			$survey_session = 'Survey '.(date('Y', $next_survey_end)-1).'-'.date('y', $next_survey_end);
			$survey_end_date = (date('Y-m-d', $next_survey_end)-1).'-'.date('y', $next_survey_end);
			
		}else{
			
			if(strtotime($current_survey_date) > strtotime($next_survey_end_date)){
				
				$next_survey_end = strtotime("$next_survey_end_date +1 year"); 
				$survey_session = 'Survey '.date('Y').'-'.(date('y')+1);
				$survey_end_date = date('Y-m-d', $next_survey_end).'-'.(date('y', $next_survey_end)+1);
	
			} else {
				
				$next_survey_end = strtotime("$next_survey_end_date +0 year");
				// Survey 2016-2017
				$survey_session = 'Survey '.(date('Y', $next_survey_end)-1).'-'.date('y', $next_survey_end);
				$survey_end_date = (date('Y-m-d', $next_survey_end)-1).'-'.date('y', $next_survey_end);
	
			} //end if

		}//end if($next_year == '1')

		$expiry_date = date('Y-m-d', $next_survey_end);
		$survey_year = date('Y', $next_survey_end);

		$survey_ref_no = $this->generate_survey_reference_no();

		$created_date = date('Y-m-d H:i:s');
		$created_by_ip = $this->input->ip_address();

		$ins_data = array(

			'pharmacy_id' => $this->db->escape_str(trim($pharmacy_id)),
			'survey_ref_no' => $this->db->escape_str(trim($survey_ref_no)),
			'survey_title' => $this->db->escape_str(trim($survey_session)),
			'survey_year' => $this->db->escape_str(trim($survey_year)),
			'survey_finish_date' => $this->db->escape_str(trim($expiry_date)),
			'survey_status' => $this->db->escape_str(trim('0')),
			'created_date' => $this->db->escape_str(trim($created_date)),
			'created_by_ip' => $this->db->escape_str(trim($created_by_ip))
		);
		
		//Inserting rota requested leaves data into the database. 
		$this->db->dbprefix('pharmacy_survey');
		$this->db->insert('pharmacy_survey', $ins_data);

		return $this->db->insert_id();

	} // End =>public function add_new_survey($pharmacy_id)

	// Start => public function save_invoice($pharmacy_id, $survey_id, $invoice_data)
	public function save_invoice($pharmacy_id, $survey_id, $invoice_data){

		extract($invoice_data);

		// Generate unique Order Number
		$order_no = $this->generate_order_number();

		$ins_arr = array(
			'pharmacy_id' => $pharmacy_id,
			'survey_id' => $survey_id,
			'invoice_no' => $order_no,
			'subtotal' => $subtotal,
			'vat' => $vat_tax,
			'vat_percentage' => $vat_percentage,
			'survey_session' => $survey_session,
			'grand_total' => $grand_total,
			'payment_method' => $payment_method,
			'transaction_id' => $transaction_id,

			'pharmacy_name' => $pharmacy_name,
			'pharmacy_owner_name' => $pharmacy_owner_name,
			'pharmacy_address' => $pharmacy_address,
			'pharmacy_address_2' => $pharmacy_address_2,
			'pharmacy_town' => $pharmacy_town,
			'pharmacy_county' => $pharmacy_county,
			'pharmacy_postcode' => $pharmacy_postcode,
			'from_name' => $from_name,
			'from_address' => $from_address,
			'from_address_2' => $from_address_2,
			'from_town' => $from_town,
			'from_county' => $from_county,
			'from_postcode' => $from_postcode,
			'from_phone' => $from_phone,
			'from_email' => $from_email,
			'from_website' => $from_website,

			'created_date' => date('Y-m-d H:i:s'),
			'created_by_ip' => $this->input->ip_address()
		);

		$this->db->dbprefix('invoices');
		$this->db->insert('invoices', $ins_arr);

		$last_invice_id = $this->db->insert_id();

		$this->db->dbprefix('invoices');
		$this->db->where('id', $last_invice_id);
		
		return $this->db->get('invoices')->row_array();
		
	} // End => public function save_invoice($pharmacy_id, $survey_id, $invoice_data)

	//Function generate_order_number(): Generate unique Order number, by recursive call.
	public function generate_order_number(){
		
		$new_order_no = strtoupper($this->common->random_number_generator(4));
		
		$this->db->dbprefix('invoices');
		$this->db->select('id');
		$this->db->where('invoice_no',$new_order_no);
		$get = $this->db->get('invoices');
		// echo $this->db->last_query(); exit;
		
		if($get->num_rows > 0)
			$this->generate_order_number();
		else
			return $new_order_no;
		
	}//end generate_order_number()
	
	//Function get_survey_invitation(): Returns the survety invitation if exist
	public function get_survey_invitation($pharmacy_id,$survey_id, $inv_id){
		
		$this->db->dbprefix('survey_invitations');
		$this->db->where('id',$inv_id);
		$get = $this->db->get('survey_invitations');
		return $get->row_array();

	}//end get_survey_invitation($pharmacy_id,$survey_id, $inv_id)

	//Function delete_survey_invitation($inv_id)(): Delete suirvey invitation if exist
	public function delete_survey_invitation($inv_id){
		
		$this->db->dbprefix('survey_invitations');
		$this->db->where('id',$inv_id);
		$delete = $this->db->delete('survey_invitations');
		
		if($delete)
			return true;
		else
			return false;

	}//end delete_survey_invitation($inv_id)
	
	public function send_sms($pharmacy_id, $survey_id, $data){
		
		extract($data);

		$survey_details = $this->survey->get_pharmacy_survey_list($pharmacy_id,'',$survey_id);
		
		//////////////////////////////////////////////////////////////
		// Get Global settings for SMS send [ Username & Password ] //
		
		// Get LIVE Key //
		$SMS_AUTHENTICATION_KEY = 'SMS_AUTHENTICATION_KEY';
		$sms_authentication_key = get_global_settings($SMS_AUTHENTICATION_KEY);
		$sms_authentication_key = filter_string($sms_authentication_key['setting_value']);
		
		// Concat CountryCode with Mobile Number
		$mobilenumber = $country_code.$mobile_number;
		$message = filter_string($sms_comment);

		//Insert the invitation entry into the database.
		$ins_data = array(
			
			'survey_id' => $this->db->escape_str(trim($survey_id)),
			'pharmacy_id' => $this->db->escape_str(trim($pharmacy_id)),
			'email_address' => $this->db->escape_str(trim('')),
			'created_date' => $this->db->escape_str(trim($created_date)),
			'created_by_ip' => $this->db->escape_str(trim($created_by_ip)),
		);
		
		//Update prescribers data into the database. 
		$this->db->dbprefix('survey_invitations');
		$into_db = $this->db->insert('survey_invitations', $ins_data);
		
		$inv_id = $this->db->insert_id();

		$text_to_find = SURL.'online-survey/'.$pharmacy_id.'/'.$survey_details['survey_ref_no'];
		
		$text_to_replace = SURL.'online-survey/'.$pharmacy_id.'/'.$survey_details['survey_ref_no'].'/'.$inv_id;

		$this->load->library('GoogleUrlApi');
		$googer = new GoogleURLAPI($this->google_short_url_key);
		$short_url = $googer->shorten($text_to_replace);
		// $text_to_replace = new

		$message = filter_string($message); // nl2br();
		$message = str_replace($text_to_find, $short_url, $message);

		//echo $mobilenumber.'<br><br>';
		//echo $message;
		//exit;

		$senderid = $this->session->pharmacy_name.' ('.$this->session->postcode.')'; // Pharmacy Name & POSTCODE

		// echo 'To: '.$mobile_number.' <br> Message: '.$message.'<br><br>'.$senderid;

		// $sms_authentication_key
		// $senderid

		// Verify if the key found
		if($sms_authentication_key){

			// For testing only
			// Get TEST Key //
			/*
			$SMS_AUTHENTICATION_KEY_TEST = 'SMS_AUTHENTICATION_KEY_TEST';
			$sms_authentication_key_test = get_global_settings($SMS_AUTHENTICATION_KEY_TEST);
			$sms_authentication_key_test = filter_string($sms_authentication_key_test['setting_value']);
			*/
			
			require_once APPPATH.'libraries/message_bird/autoload.php';
			$MessageBird = new \MessageBird\Client($sms_authentication_key); // Live Key

			$Message = new \MessageBird\Objects\Message();
			$Message->originator = "Pharmacy";
			$Message->recipients = array($mobilenumber);
			$Message->body = $message;

			$response = $MessageBird->messages->create($Message);

			// print_this($response);
			// exit;

		} // if($sms_authentication_key)
		
		return true;
		
	}//end send_sms($pharmacy_id, $survey_id, $data)
	
	//Function get_survey_payment_invoices(): Get the invoice list of Pharamcies
	public function get_survey_payment_invoices($pharmacy_id, $invoice_id = '', $survey_id = ''){
		
		$this->db->dbprefix('invoices');
		
		if(trim($invoice_id) != '') $this->db->where('id',$invoice_id);
		if(trim($survey_id) != '') $this->db->where('survey_id',$survey_id);

		$this->db->where('pharmacy_id',$pharmacy_id);
		$this->db->where('payment_method != ','ADMIN');
		$this->db->order_by('id','DESC');

		$get = $this->db->get('invoices');
		
		//echo $this->db->last_query(); 	exit;
		if(trim($invoice_id)!= '' || trim($survey_id)!= '')
			return $get->row_array();
		else
			return $get->result_array();
			
	}//end get_survey_payment_invoices($pharmacy_id)
	
	public function force_survey_to_close($pharmacy_id, $survey_id){
		
		$get_survey_payment_invoice = $this->survey->get_survey_payment_invoices($pharmacy_id,'',$survey_id);
		$survey_start_date = filter_string($get_survey_payment_invoice['created_date']);
		
		$upd_data = array(
			
			'survey_start_date' => $this->db->escape_str(trim($survey_start_date)),
			'min_no_of_surveys' => $this->db->escape_str(trim(50)),
			'survey_volume' => $this->db->escape_str(trim('0 - 2000')),
		);
		
		//Inserting rota requested leaves data into the database. 
		$this->db->dbprefix('pharmacy_survey');
		$this->db->where('id', $survey_id);
		$this->db->where('pharmacy_id', $pharmacy_id);
		$upd_into_db = $this->db->update('pharmacy_survey', $upd_data);
		
		//echo $this->db->last_query(); 		exit;
		
		if($upd_into_db)
			return true;
		else
			return false;
		
	}//end force_survey_to_close($pharmacy_id, $survey_id)
	
	public function update_min_survey_required($pharmacy_id, $survey_id, $min_survey_required){
		
		$arr_survey_volume = array(
								'50' => '0 - 2000',
								'75' => '2001 - 4000',
								'100' => '4001 - 6000',
								'125' => '6001 - 8000',
								'150' => '8001 - onwards'
							);

		$upd_data = array(
			
			'min_no_of_surveys' => $this->db->escape_str(trim($min_survey_required)),
			'survey_volume' => $this->db->escape_str(trim($arr_survey_volume[$min_survey_required])),
		);
		
		//Inserting rota requested leaves data into the database. 
		$this->db->dbprefix('pharmacy_survey');
		$this->db->where('id', $survey_id);
		$this->db->where('pharmacy_id', $pharmacy_id);
		$upd_into_db = $this->db->update('pharmacy_survey', $upd_data);
		
		//echo $this->db->last_query(); 		exit;
		
		if($upd_into_db)
			return true;
		else
			return false;
		
	}//end update_min_survey_required($pharmacy_id, $survey_id)
	
	public function update_embed_url($survey_d, $short_url){
		
		$upd_data = array(
			'embed_url' => $this->db->escape_str(trim($embed_url)),
		);
		
		//Inserting rota requested leaves data into the database. 
		$this->db->dbprefix('pharmacy_survey');
		$this->db->where('id', $survey_id);
		$upd_into_db = $this->db->update('pharmacy_survey', $upd_data);
		
		//echo $this->db->last_query(); 		exit;
		
		if($upd_into_db)
			return true;
		else
			return false;

	}//end update_embed_url($survey_d, $short_url)
	
}//end file
?>