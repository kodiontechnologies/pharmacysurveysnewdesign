<?php
class Group_mod extends CI_Model {
	
	function __construct(){
		parent::__construct();
    }

	// Validate user credrntials if match in the DB to login
	
	public function get_group_details($business_id){
		
		$this->db->dbprefix('business');
		$this->db->where('business.id', $business_id);
		$get = $this->db->get('business');
		//echo $this->db->last_query(); exit;
		return $get->row_array();
		
	} // end get_group_details($pharmacy_id)

	// Get User by Name email
	public function get_user_by_name_pincode($username){
		
		$this->db->dbprefix('business');
		$this->db->where('email_address', trim($username));
		// Verify user and pharmacy status
		$this->db->where('status', '1');
		//$this->db->where('is_deleted', '0');
		$get = $this->db->get('business');
		
		$row_arr = $get->row_array();
		//echo $this->db->last_query(); 		exit;
		return $row_arr;		
		
	}//end get_user_by_name_pincode()
	
	
	public function verify_business_credentials($email_address,$password){
		
		$this->db->dbprefix('business');
		
		$this->db->select('id AS g_id, business_type, email_address AS group_email_address, contact_name AS group_contact_name, contact_no AS group_contact_no');
		
		$this->db->where('business.email_address', strip_quotes($email_address));
		$this->db->where('business.password', $this->db->escape_str(trim(md5($password))));
		
		// Verify pharmacy status
		$this->db->where('business.status', '1');

		$get = $this->db->get('business');
		
		//echo $this->db->last_query(); 		exit;
		$data_arr = $get->row_array();
		
		if($get->num_rows() > 0){
		}//end if($get->num_rows() > 0)
		
		return $data_arr;
		
	}//end verify_business_credentials($email_address,$password)
	
	//Function send_new_password(): Send New password to the user on forgot password
	public function send_new_password($user_arr){
		
		$user_name = ucwords(strtolower(stripslashes($user_arr['contact_name']))); 
		
		$this->load->model('Common_mod','common');
		$new_password_txt = $this->common->random_number_generator(6);

		$random_number = $this->common->random_number_generator(6);
		$reset_code = urlencode(base64_encode($random_number.'|'.$user_arr['id'].'|'.$user_arr['id']));
		$email_address = stripslashes(trim($user_arr['email_address']));
		
		//Updating New Password into the database

		$last_modified_date = date('Y-m-d G:i:s');
		$last_modified_ip = $this->input->ip_address();

		$upd_data = array(
		   'password_reset_request' => $this->db->escape_str(trim(1)),
		   'password_request_date' => $this->db->escape_str(trim($last_modified_date)),
		   'password_reset_code' => $this->db->escape_str(trim($reset_code)),
		   'modified_date' => $this->db->escape_str(trim($last_modified_date)),
		   'modified_by_ip' => $this->db->escape_str(trim($last_modified_ip))
		);

		//Update the record into the database.
		$this->db->dbprefix('business');
		$this->db->where('id',$user_arr['id']);
		$upd_into_db = $this->db->update('business', $upd_data);

		// EMAIL SENDING CODE - START
		
		$new_password_link = SURL.'group/reset-password/'.$reset_code;
		
		$search_arr = array('[USERNAME]','[USER_NEW_PASSWORD]','[NEW_PASSWORD_LINK]','[SITE_LOGO]','[SITE_URL]');
		$replace_arr = array($user_name, $new_password_txt, $new_password_link, SITE_LOGO, SURL); 
		
		//echo $user_name; exit;
		
		$this->load->model('email_mod','email_template');
		
		$email_body_arr = $this->email_template->get_email_template(2);
		
		$email_subject = $email_body_arr['email_subject'];
		
		$email_body = filter_string($email_body_arr['email_body']);
		$email_body = str_replace($search_arr,$replace_arr,$email_body);
		
		 //echo $email_body; exit;
		
		$NO_REPLY_EMAIL = 'NO_REPLY_EMAIL';
		$noreply_email = get_global_settings($NO_REPLY_EMAIL);
		$noreply_email = trim($noreply_email['setting_value']);

		$FROM_EMAIL_TXT = 'FROM_EMAIL_TXT';
		$email_from_txt = get_global_settings($FROM_EMAIL_TXT);
		$email_from_txt = trim($email_from_txt['setting_value']);
		
		//Preparing Sending Email
		/*
		$config['charset'] = 'utf-8';
		$config['mailtype'] = 'html';
		$config['wordwrap'] = TRUE;			
		$config['protocol'] = 'mail';
			
		$this->load->library('email',$config);
		$this->email->from($noreply_email, $email_from_txt);
		$this->email->to(trim($email_address));
		$this->email->subject(filter_string($email_subject));
		$this->email->message(filter_string($email_body));
		
		$this->email->send();
		$this->email->clear();
		*/
		
		//////////////////////////////////////////////////////////////////////
		/////// Global Settings for Amazon SES SMTP Email credentials ////////
		
		// Username
		$SES_USERNAME = 'SES_USERNAME';
		$ses_username = get_global_settings($SES_USERNAME);
		$ses_username = trim($ses_username['setting_value']);

		// Password
		$SES_PASSWORD = 'SES_PASSWORD';
		$ses_password = get_global_settings($SES_PASSWORD);
		$ses_password = trim($ses_password['setting_value']);

		// Host
		$SES_HOST = 'SES_HOST';
		$ses_host = get_global_settings($SES_HOST);
		$ses_host = trim($ses_host['setting_value']);

		// Port
		$SES_PORT = 'SES_PORT';
		$ses_port = get_global_settings($SES_PORT);
		$ses_port = trim($ses_port['setting_value']);

		// Port
		$SES_SENDER = 'SES_SENDER';
		$ses_sender = get_global_settings($SES_SENDER);
		$ses_sender = trim($ses_sender['setting_value']);

		//////////////////////////////////////////////////////////////////////

		// Replace sender@example.com with your "From" address. 
		// This address must be verified with Amazon SES.
		$SENDER = $noreply_email; // $email; //'info@surveyfocus.co.uk';

		// Replace recipient@example.com with a "To" address. If your account 
		// is still in the sandbox, this address must be verified.
		$RECIPIENT = trim($email_address); //'twister787@gmail.com';
															  
		// Replace smtp_username with your Amazon SES SMTP user name.
		$USERNAME = $ses_username; // 'AKIAJJDSTFURI6LQWNPQ';

		// Replace smtp_password with your Amazon SES SMTP password.
		$PASSWORD = $ses_password; // 'As/vj5Eh8mLt7dYHnJWnK/6Fk5UwUGhD9qtA300O9rPL';

		// If you're using Amazon SES in a region other than US West (Oregon), 
		// replace email-smtp.us-west-2.amazonaws.com with the Amazon SES SMTP  
		// endpoint in the appropriate region.
		
		$HOST = $ses_host; // 'email-smtp.eu-west-1.amazonaws.com'; 
		// $HOST = 'ses-smtp-user.20170116-163508';
		// ssl://email-smtp.us-east-1.amazonaws.com
		
		// The port you will connect to on the Amazon SES SMTP endpoint.
		$PORT = $ses_port; //'587';

		// Other message information                                               
		$SUBJECT = filter_string($email_subject); //'Amazon SES test (SMTP interface accessed using PHP)';
		$BODY = filter_string($email_body); //'This email was sent through the Amazon SES SMTP interface by using PHP.';

		////////////////////////////////////////
		////// CI SMTP Mail configuration //////

		$config = array(
			'protocol' => 'smtp',
			'smtp_host' => $HOST,
			'smtp_user' => $USERNAME,
			'starttls' => TRUE,
			'smtp_pass' => $PASSWORD,
			'smtp_port' => $PORT,
			'smtp_crypto' => 'tls', //can be 'ssl' or 'tls' for example
			'mailtype' => 'html'
		);

		$this->load->library('email');

		$this->email->initialize($config);
		// $this->email->print_debugger();

		$this->email->from($SENDER, $email_from_txt);
		$this->email->to($RECIPIENT);

		$this->email->subject($SUBJECT);
		$this->email->message($BODY);
		$this->email->set_newline("\r\n");
		$sent_status = $this->email->send();
		$this->email->clear();
		////////////////////////////////////
		
		return true;

	} // End - function send_new_password($user_arr)

	//Function verify_is_user_login(): Verify If User is Login on the authorized Pages.
	public function verify_is_user_login(){
		
		if(!$this->session->pid){
			
			$this->session->set_flashdata('err_message', 'You have to login to access this page.');
			redirect(base_url().'login');
			exit;
			
		}//if(!$this->session->pid)
		
	}//end verify_is_user_login()
	
	
	public function reset_password($business_id, $data){
		
		extract($data);

		$last_modified_date = date('Y-m-d G:i:s');
		$last_modified_ip = $this->input->ip_address();

		$upd_data = array(
		   'password_reset_request' => $this->db->escape_str(trim(0)),
		   'password_request_date' => NULL,
		   'password_reset_code' => NULL,
		   'password' => md5($new_password),
		   'modified_date' => $this->db->escape_str(trim($last_modified_date)),
		   'modified_by_ip' => $this->db->escape_str(trim($last_modified_ip))
		);
		
		//Update the record into the database.
		$this->db->dbprefix('business');
		$this->db->where('id',$business_id);
		$upd_into_db = $this->db->update('business', $upd_data);
		
		//echo $this->db->last_query(); exit;
		
		if($upd_into_db)
			return true;
		else
			return false;
		
	}//end reset_password($pharmacy_pin, $pharmacy_id)
	
	public function verify_reset_code($verification_code){
		
		$this->db->dbprefix('business');
		$this->db->where('password_reset_code',$verification_code);
		$this->db->where('password_reset_request','1');
		$get_user= $this->db->get('business');
		$row_arr = $get_user->row_array();
		//echo $this->db->last_query(); 		exit;
		return $row_arr;		
		
	}//end verify_reset_code($verification_code)

	//Function: verify_if_group_already_exist(): verify if group exist with a given email address
	public function verify_if_group_already_exist($email_address,$group_id=''){
		
		$this->db->dbprefix('business');
		$this->db->where('email_address', $email_address);
		if(trim($group_id) !='')$this->db->where('id !=', $group_id);
		$get = $this->db->get('business');
		
		// echo $this->db->last_query();

		if($get->num_rows() > 0)
			return true;
		else
			return false;			
		
	} // end verify_if_group_already_exist($email_address)

	// Start => public function update_group_profile($post, $pharmacy_id)
	public function update_group_profile($post, $group_id){

		// Extract POST
		// extract($post);

		// Add modified date and IP to the update array
		$post['modified_date'] = date('Y-m-d H:i:s');
		$post['modified_by_ip'] = $this->input->ip_address();

		$this->db->dbprefix('business');
		$this->db->where('id', $group_id);
		return $this->db->update('business', $post);

	} // End => public function update_group_profile($post, $pharmacy_id)

	// Start => public function create_new_password($post, $group_id)
	public function create_new_password($post, $group_id){

		// Extract POST
		extract($post);

		$this->db->dbprefix('business');
		$this->db->where('id', $group_id);
		$this->db->where('password', md5($password));
		$is_valid_password = $this->db->get('business')->row_array();

		if($is_valid_password && ($new_password == $re_new_password)){

			$upd_arr = array('password' => md5($new_password));

			$this->db->dbprefix('business');
			$this->db->where('id', $group_id);
			$this->db->where('password', md5($password));
			return $this->db->update('business', $upd_arr);

		} else {
			return false;
		}

	} // End => public function create_new_password($post, $group_id)

}//end file
?>