<?php
class Cms_mod extends CI_Model {
	
	function __construct(){
		 parent::__construct();
    }

	// Start - get_cms_page($url_slug): Return CMS Page by URL
	public function get_cms_page($url_slug){
	
		$this->db->dbprefix('pages');
		$this->db->where('url_slug', $url_slug);
		$this->db->where('status', '1');
		$query = $this->db->get('pages');
	
		return $query->row_array();
		
	} // End - get_cms_page($url_slug)
	
	// Start - function get_noscript_text()
	public function get_noscript_text(){
		
		$this->db->dbprefix('global_settings');
		$this->db->where('setting_name', "NOSCRIPT_TEXT");
		
		return $this->db->get('global_settings')->row_array();
		
	} // End - get_noscript_text()
	
}//end file
?>