<?php
class Register_mod extends CI_Model {
	
	function __construct(){
		parent::__construct();
    }

	// Start Function add bussines
	public function register_pharmacy($data, $affiliate_code){

	   	extract($data);

		$created_date = date('Y-m-d G:i:s');
		$created_by_ip = $this->input->ip_address();

		//Generate Random code	
		$email_activation_code = $this->common->random_number_generator(10);
		
		$postcode = str_replace(' ', '', $postcode); // Saving the postcode without spaces for all consistent Postcode search	

		$SMS_QUOTA = 'SMS_QUOTA';
		$sms_quota = get_global_settings($SMS_QUOTA);
		$sms_quota_value = filter_string($sms_quota['setting_value']);
		
		$ins_data_pharmacy = array(
		
			'pharmacy_name' => $this->db->escape_str(trim($pharmacy_name)),
			'owner_name' => $this->db->escape_str(trim($owner_name)),
			'contact_no' => $this->db->escape_str(trim($contact_no)),
			'address' => $this->db->escape_str(trim($address)),
			'address_2' => $this->db->escape_str(trim($address_2)),
			'town' => $this->db->escape_str(trim($town)),
			'county' => $this->db->escape_str(trim($county)),
			'postcode' => $this->db->escape_str(trim($postcode)),
			'status' => $this->db->escape_str('1'),
			'pharmacy_sms_quota' => $this->db->escape_str($sms_quota_value),
			'pharmacy_sms_quota_consumed' => $this->db->escape_str('0'),
			'email_address' => $this->db->escape_str(trim($email_address)),
			'password' => $this->db->escape_str(trim(md5($password))),
			'pharmacy_type' => $this->db->escape_str(trim($pharmacy_type)),
			'created_date' => $this->db->escape_str(trim($created_date)),
			'created_by_ip' => $this->db->escape_str(trim($created_by_ip)),
		);
		

		//Verifying Affiliate 
		if($affiliate_code){
			
			$verify_affiliate = $this->common->verify_affiliate($affiliate_code);
			
			if($verify_affiliate == "OK")
				$ins_data_pharmacy['affiliate_code'] = $this->db->escape_str(trim($affiliate_code));
			
		}//end if($affiliate_code)
		
		
		//Inserting users data into the database. 
		$this->db->dbprefix('pharmacies');
		$ins_into_db = $this->db->insert('pharmacies', $ins_data_pharmacy);
		
		$ins_into_db = $this->db->insert_id();

		/*	
		$new_pharmacy_id = $this->db->insert_id();
		$activation_link = SURL."login/activate-account?uid=".($new_user_id)."&code=".md5($email_activation_code);
		$user_first_last_name = ucwords(strtolower(stripslashes($first_name.' '.$last_name)));
		$user_name = ucwords($username);

		$search_arr = array('[FIRST_LAST_NAME]','[ACTIVATION_LINK]','[USERNAME]','[EMAIL_ADDRESS]','[PINCODE]','[SITE_LOGO]','[SITE_URL]');
		$replace_arr = array($user_first_last_name,$activation_link,$user_name,$email_address,$pin,SITE_LOGO,SURL); 
		
		$this->load->model('email_mod','email_template');
		
		$email_body_arr = $this->email_template->get_email_template(1);
		
		$email_subject = $email_body_arr['email_subject'];
		
		$email_body = $email_body_arr['email_body'];
		
		$email_body = str_replace($search_arr,$replace_arr,$email_body);
		
		$noreply_email =  "noreply@procdr.co.uk";
		
		$email_from_txt = "Pro CDR";
				
		//Preparing Sending Email
		$config['charset'] = 'utf-8';
		$config['mailtype'] = 'html';
		$config['wordwrap'] = TRUE;			
		$config['protocol'] = 'mail';
			
		$this->load->library('email',$config);
		$this->email->from($noreply_email, $email_from_txt);
		$this->email->to(trim($email_address));
		$this->email->subject(filter_string($email_subject));
		$this->email->message(filter_string($email_body));
		
		$this->email->send();
		$this->email->clear();
			*/
		if($ins_into_db)
			return $ins_into_db;
		else
			return false;
				
	}//end add_new_user
	
	//Function: verify_if_pharmacy_already_exist(): verify if pharamcy exist with a given email address
	public function verify_if_pharmacy_already_exist($email_address,$pharmacy_id=''){
		
		$this->db->dbprefix('pharmacies');
		$this->db->where('email_address', $email_address);
		if(trim($pharmacy_id) !='')$this->db->where('id !=', $pharmacy_id);
		$get = $this->db->get('pharmacies');
		
		// echo $this->db->last_query();

		if($get->num_rows() > 0)
			return true;
		else
			return false;			
		
	} // end verify_if_pharmacy_already_exist($email_address)
	
}//end file
?>