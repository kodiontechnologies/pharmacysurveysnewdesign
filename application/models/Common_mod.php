<?php
class Common_mod extends CI_Model {
	
	function __construct(){
		 parent::__construct();
    }

	//Function random_number_generator($digit): random number generator function
	public function random_number_generator($digit){
		$randnumber = '';
		$totalChar = $digit;  //length of random number
		$salt = "0123456789abcdefjhijklmnopqrstuvwxyz";  // salt to select chars
		srand((double)microtime()*1000000); // start the random generator
		$password=""; // set the inital variable
		
		for ($i=0;$i<$totalChar;$i++)  // loop and create number
		$randnumber = $randnumber. substr ($salt, rand() % strlen($salt), 1);
		return $randnumber;
		
	}// end random_password_generator()
	
	//Function submit_contactus_form(): Submmiting/ emailing the contact us form to the admin
	public function submit_contactus_form($data){
		
		extract($data);
	
		// EMAIL SENDING CODE - START
		$full_name   = filter_string($name);
		$search_arr  = array('[USERNAME]','[EMAIL_ADDRESS]','[COMMENTS]','[SITE_LOGO]','[SITE_URL]');
		$replace_arr = array($full_name, filter_string($email),filter_string($comment),SITE_LOGO,SURL); 
		
		$this->load->helper(array('email'));
		$this->load->model('email_mod','email_template');
		
		$email_body_arr = $this->email_template->get_email_template(3);
		$email_subject = $email_body_arr['email_subject'];
		
		$email_body = stripcslashes($email_body_arr['email_body']);
		$email_body = str_replace($search_arr,$replace_arr,$email_body);
		
		$CONTACT_EMAIL = 'CONTACTUS_FORM_EMAIL';
		$send_to_email = get_global_settings($CONTACT_EMAIL); 

		$EMAIL_FROM_TXT = $name;
		$email_from_txt = $name;
	
		//Preparing Sending Email
		$config['charset'] = 'utf-8';
		$config['mailtype'] = 'html';
		$config['wordwrap'] = TRUE;			
		$config['protocol'] = 'mail';
		$this->load->library('email',$config);
		$this->email->from($email, $email_from_txt);
		$this->email->to(trim($send_to_email['setting_value']));
		$this->email->subject($email_subject);

		$this->email->message(filter_string($email_body));
		
		$this->email->send();
		$this->email->clear();
		
		// EMAIL SENDING CODE - STOP
		
		return true;
		
	}//end submit_contactus_form($data)
	
	public function send_feedback($data,$pharmacy_name, $user_name, $email_address){
		
		extract($data);
	
		// EMAIL SENDING CODE - START
		$full_name   = filter_string($name);
		$search_arr  = array('[USERNAME]','[EMAIL_ADDRESS]','[COMMENTS]','[PHARMACY_NAME]','[SITE_LOGO]','[SITE_URL]');
		$replace_arr = array($user_name, filter_string($email_address),filter_string($feedback),$pharmacy_name,SITE_LOGO,SURL); 
		
		$this->load->helper(array('email'));
		$this->load->model('email_mod','email_template');
		
		$email_body_arr = $this->email_template->get_email_template(4);
		$email_subject = $email_body_arr['email_subject'];
		
		$email_body = stripcslashes($email_body_arr['email_body']);
		$email_body = str_replace($search_arr,$replace_arr,$email_body);
		
		
		$CONTACT_EMAIL = 'CONTACTUS_FORM_EMAIL';
		$send_to_email = get_global_settings($CONTACT_EMAIL); 

		 $EMAIL_FROM_TXT = $user_name;
		 $email_from_txt = $user_name;
	
		//Preparing Sending Email
		$config['charset'] = 'utf-8';
		$config['mailtype'] = 'html';
		$config['wordwrap'] = TRUE;			
		$config['protocol'] = 'mail';
		$this->load->library('email',$config);
		$this->email->from($email_address, $email_from_txt);
		$this->email->to(trim($send_to_email['setting_value']));
		$this->email->subject($subject);

		$this->email->message(filter_string($email_body));
		
		$this->email->send();
		$this->email->clear();
		
		// EMAIL SENDING CODE - STOP
		
		return true;
		
	}//end  send_feedback($data,$pharmacy_name, $user_name, $email_address)
	
	 //Function random_pincode_generator($digit): random pincode generator function
	 public function random_pincode_generator($digit){
	  $randnumber = '';
	  $totalChar = $digit;  //length of random number
	  $salt = "0123456789";  // salt to select chars
	  srand((double)microtime()*1000000); // start the random generator
	  $password=""; // set the inital variable
	  
	  for ($i=0;$i<$totalChar;$i++)  {
		  
	 	 $randnumber = $randnumber. substr ($salt, rand() % strlen($salt), 1);
	  }
	  
	  $this->db->dbprefix('pharmacies');
	  $this->db->where('pin', $randnumber);
	  $exist =$this->db->get('pharmacies')->row_array();
	  
	  if($exist){
		  random_pincode_generator($digit);
	  } else {

		 	return $randnumber;
	  }
	  
	 }// end random_pincode_generator()
	 
	 public function update_user_permissions($data,$pharmacy_id){
		
		extract($data);
		
		if($menu_chk)
			$menu_chk_str = implode(',',$menu_chk);
		else 
			$menu_chk_str = '';
		
		if($update_request == '0'){

			$ins_data = array(
				
				'usertype_id' => $usertype_id,
				'responsibility_id' => ($responsibility_id) ? $responsibility_id : NULL,
				'pharmacy_id' => $pharmacy_id,
				'permissions' => $this->db->escape_str(trim($menu_chk_str)),
				'description' => "Added by pharmacy super admin.",
				'status' => '1'
			);
			
			$this->db->dbprefix('pharmacy_usertype');
			$this->db->insert('pharmacy_usertype',$ins_data);
			
		} else {

			$upd_data = array(
				'permissions' => $this->db->escape_str(trim($menu_chk_str))
			);
			
			$this->db->dbprefix('pharmacy_usertype');
			$this->db->where('usertype_id', $usertype_id);
			$this->db->where('pharmacy_id', $pharmacy_id);

			if($responsibility_id) $this->db->where('responsibility_id', $responsibility_id);

			$this->db->update('pharmacy_usertype',$upd_data);

			//echo $this->db->last_query();
			//exit;

		} // if

		return true;

	}//end update_user_permissions($data)
	
	public function generate_lead($product_type, $business_name, $grand_total, $lead_date, $affiliate_code, $project_id, $lead_ip, $lead_ref_id){
		
		//Saving Lead if Exist
		$post_arr['business_name'] = filter_string($business_name);
		$post_arr['product_type'] = filter_string($product_type);
		$post_arr['amount'] = filter_string($grand_total);
		$post_arr['lead_date'] = filter_string($lead_date);
		$post_arr['affiliate_code'] = filter_string($affiliate_code);
		$post_arr['project_id'] = $project_id;
		$post_arr['lead_ref_id'] = $lead_ref_id;
		$post_arr['lead_ip'] = $lead_ip;
		
		$ch = curl_init();
		
		curl_setopt($ch, CURLOPT_URL, SURL_AFFILIATE_ADD_LEAD);
		curl_setopt($ch, CURLOPT_POST, 1);
		
		$post_str = '';
		foreach($post_arr as $key => $val)
			$post_str .= $key.'='.$val.'&';
		
		$post_str = rtrim($post_str,'&');
		
		curl_setopt($ch, CURLOPT_POSTFIELDS,$post_str);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		
		$server_output = curl_exec ($ch);
		
		curl_close ($ch);
		
		return $server_output;
			
	}	
	
	public function verify_affiliate($affiliate_code){
		
		$ch = curl_init();
		
		curl_setopt($ch, CURLOPT_URL, SURL_AFFILIATE_VERIFY);
		curl_setopt($ch, CURLOPT_POST, 1);
		
		$post_str = 'affiliate_code='.$affiliate_code;
		
		curl_setopt($ch, CURLOPT_POSTFIELDS,$post_str);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		
		$server_output = curl_exec ($ch);
		
		curl_close ($ch);
		
		return $server_output;
	}
	
	public function get_affiliate_by_code($affiliate_code){
		
		$ch = curl_init();
		
		curl_setopt($ch, CURLOPT_URL, SURL_AFFILIATE.'lead-record/get-affiliate-by-code');
		curl_setopt($ch, CURLOPT_POST, 1);
		
		$post_str = 'affiliate_code='.$affiliate_code;
		
		curl_setopt($ch, CURLOPT_POSTFIELDS,$post_str);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		
		$server_output = curl_exec ($ch);
		
		curl_close ($ch);
		
		return $server_output;
	}

	public function get_affiliate_product($affiliate_id, $product_id = ''){
		
		$ch = curl_init();
		
		curl_setopt($ch, CURLOPT_URL, SURL_AFFILIATE.'lead-record/get-affiliate-product');
		curl_setopt($ch, CURLOPT_POST, 1);
		
		$post_str = 'affiliate_id='.$affiliate_id;
		
		if($product_id != '')
			$post_str .= '&product_id='.$product_id;
		
		curl_setopt($ch, CURLOPT_POSTFIELDS,$post_str);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		
		$server_output = curl_exec ($ch);
		
		curl_close ($ch);
		
		return $server_output;
	}

	public function get_help_videos_list($video_id = ''){
		
		// Get Patient Record
		$this->db->dbprefix('help_videos');
		
		$this->db->where('status', '1');
		
		if($video_id != '')
			$this->db->where('id', $video_id);
			
		$this->db->order_by('display_order', 'ASC');
		$get = $this->db->get('help_videos');
		
		if($video_id != '')
			return $get->row_array();
		else
			return $get->result_array();
			
	}//end get_help_videos_list($data)	

}//end file

?>