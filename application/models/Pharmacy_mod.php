<?php
class Pharmacy_mod extends CI_Model {
	
	function __construct(){
		 parent::__construct();
    }

	public function get_pharmacy_details($pharmacy_id){
		
		$this->db->dbprefix('pharmacies');
		$this->db->where('pharmacies.id', $pharmacy_id);
		$get = $this->db->get('pharmacies');
		//echo $this->db->last_query(); exit;
		return $get->row_array();
		
	} // end get_pharmacy_details($pharmacy_id)

	//Function: get_group_pharmacy_list(): Get Pharmacy list 
	public function get_group_pharmacy_list($group_id){
		
		$this->db->dbprefix('pharmacies');
		$this->db->where('pharmacies.group_id', $group_id);
		$get = $this->db->get('pharmacies');
		//echo $this->db->last_query(); exit;
		return $get->result_array();
		
	} // end get_group_pharmacy_list($pharmacy_id)

	// Start => public function update_pharmacy_profile($post, $pharmacy_id)
	public function update_pharmacy_profile($post, $pharmacy_id){

		// Extract POST
		// extract($post);

		// Add modified date and IP to the update array
		$post['modified_date'] = date('Y-m-d H:i:s');
		$post['modified_by_ip'] = $this->input->ip_address();

		$this->db->dbprefix('pharmacies');
		$this->db->where('id', $pharmacy_id);
		return $this->db->update('pharmacies', $post);

	} // End => public function update_pharmacy_profile($post, $pharmacy_id)

	// Start => public function create_new_password($post, $pharmacy_id)
	public function create_new_password($post, $pharmacy_id){

		// Extract POST
		extract($post);

		$this->db->dbprefix('pharmacies');
		$this->db->where('id', $pharmacy_id);
		$this->db->where('password', md5($password));
		$is_valid_password = $this->db->get('pharmacies')->row_array();

		if($new_password == $re_new_password){

			$upd_arr = array('password' => md5($new_password));

			$this->db->dbprefix('pharmacies');
			$this->db->where('id', $pharmacy_id);
			$this->db->update('pharmacies', $upd_arr);
			
			//echo $this->db->last_query(); exit;
			return true;

		} else {
			return false;
		}

	} // End => public function create_new_password($post, $pharmacy_id)

	// public function send_sms($pharmacy_id, $post)
	public function send_sms($pharmacy_id, $post){

		$this->db->dbprefix('pharmacies');
		$this->db->where('id', $pharmacy_id);
		$p_row = $this->db->get('pharmacies')->row_array();

		$upd_arr = array('pharmacy_sms_quota_consumed' => $p_row['pharmacy_sms_quota_consumed'] + 1);

		$this->db->dbprefix('pharmacies');
		$this->db->where('id', $pharmacy_id);
		
		return $this->db->update('pharmacies', $upd_arr);

	} // public function send_sms($pharmacy_id, $post)

}//end file
?>