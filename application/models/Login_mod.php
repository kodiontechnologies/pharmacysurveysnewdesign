<?php
class Login_mod extends CI_Model {
	
	function __construct(){
		parent::__construct();
    }

	// Validate user credrntials if match in the DB to login
	
	public function verify_pharmacy_credentials($email_address,$password, $is_group = '', $is_admin = ''){
		
		$this->db->dbprefix('pharmacies');
		
		$this->db->select('id AS p_id, owner_name, pharmacy_type, affiliate_code AS sf_affiliate_code, pharmacy_name, email_address, address, address_2, postcode, town, county, contact_no, status');
		
		$this->db->where('email_address', strip_quotes($email_address));
		if(trim($is_group) == '1' || $is_admin == '1')
			$this->db->where('password', $this->db->escape_str(trim($password)));
		else
			$this->db->where('password', $this->db->escape_str(trim(md5($password))));
		
		// Verify pharmacy status
		$this->db->where('pharmacies.status', '1');
		$this->db->where('pharmacies.is_deleted', '0');

		$get = $this->db->get('pharmacies');
		
		//echo $this->db->last_query(); 		exit;
		$data_arr = $get->row_array();
		
		if($get->num_rows() > 0){
		}//end if($get->num_rows() > 0)
		
		return $data_arr;
		
	}//end verify_pharmacy_credentials($email_address,$password)
	
	//Function send_new_password(): Send New password to the user on forgot password
	public function send_new_password($user_arr){
		
		$user_name = ucwords(strtolower(stripslashes($user_arr['owner_name']))); 
		
		$this->load->model('Common_mod','common');
		$new_password_txt = $this->common->random_number_generator(6);

		$random_number = $this->common->random_number_generator(6);
		$reset_code = urlencode(base64_encode($random_number.'|'.$user_arr['id'].'|'.$user_arr['id']));
		$email_address = stripslashes(trim($user_arr['email_address']));
		
		//Updating New Password into the database

		$last_modified_date = date('Y-m-d G:i:s');
		$last_modified_ip = $this->input->ip_address();

		$upd_data = array(
		   'password_reset_request' => $this->db->escape_str(trim(1)),
		   'password_request_date' => $this->db->escape_str(trim($last_modified_date)),
		   'password_reset_code' => $this->db->escape_str(trim($reset_code)),
		   'modified_date' => $this->db->escape_str(trim($last_modified_date)),
		   'modified_by_ip' => $this->db->escape_str(trim($last_modified_ip))
		);

		//Update the record into the database.
		$this->db->dbprefix('pharmacies');
		$this->db->where('id',$user_arr['id']);
		$upd_into_db = $this->db->update('pharmacies', $upd_data);

		// EMAIL SENDING CODE - START
		
		$new_password_link = SURL.'login/reset-password/'.$reset_code;
		
		$search_arr = array('[USERNAME]','[USER_NEW_PASSWORD]','[NEW_PASSWORD_LINK]','[SITE_LOGO]','[SITE_URL]');
		$replace_arr = array($user_name, $new_password_txt, $new_password_link, SITE_LOGO, SURL); 
		
		$this->load->model('email_mod','email_template');
		
		$email_body_arr = $this->email_template->get_email_template(2);
		
		$email_subject = $email_body_arr['email_subject'];
		
		$email_body = filter_string($email_body_arr['email_body']);
		$email_body = str_replace($search_arr,$replace_arr,$email_body);
		
		// echo $email_body; exit;

		$FROM_EMAIL_TXT = 'FROM_EMAIL_TXT';
		$email_from_txt = get_global_settings($FROM_EMAIL_TXT);
		$email_from_txt = trim($email_from_txt['setting_value']);
		
		// Port
		$SES_SENDER = 'SES_SENDER';
		$ses_sender = get_global_settings($SES_SENDER);
		$ses_sender = trim($ses_sender['setting_value']);
		
		$send_email = send_email($email_address, $ses_sender, $email_from_txt, $email_subject, $email_body);
		
		return true;

	} // End - function send_new_password($user_arr)

	//Function verify_is_user_login(): Verify If User is Login on the authorized Pages.
	public function verify_is_user_login(){
		
		if(!$this->session->pid){
			
			$this->session->set_flashdata('err_message', 'You have to login to access this page.');
			redirect(base_url().'login');
			exit;
			
		}//if(!$this->session->pid)
		
	}//end verify_is_user_login()
	
	
		//Get User by Email
	public function get_user_by_email($email_address){
		
		$this->db->dbprefix('users');
		$this->db->select('users.*');
		$this->db->where('users.email_address',$email_address);
		$get_user= $this->db->get('users');
		$row_arr = $get_user->row_array();
		//echo $this->db->last_query(); 		exit;
		return $row_arr;		
		
	} // End get_user_by_email()
	
	// Get User by Name email
	public function get_user_by_name_pincode($username){
		
		$this->db->dbprefix('pharmacies');
		$this->db->where('email_address', trim($username));
		// Verify user and pharmacy status
		$this->db->where('status', '1');
		$this->db->where('is_deleted', '0');
		$get = $this->db->get('pharmacies');
		
		$row_arr = $get->row_array();
		//echo $this->db->last_query(); 		exit;
		return $row_arr;		
		
	}//end get_user_by_email()

	public function reset_password($pharmacy_id, $data){
		
		extract($data);

		$last_modified_date = date('Y-m-d G:i:s');
		$last_modified_ip = $this->input->ip_address();

		$upd_data = array(
		   'password_reset_request' => $this->db->escape_str(trim(0)),
		   'password_request_date' => NULL,
		   'password_reset_code' => NULL,
		   'password' => md5($new_password),
		   'modified_date' => $this->db->escape_str(trim($last_modified_date)),
		   'modified_by_ip' => $this->db->escape_str(trim($last_modified_ip))
		);
		
		//Update the record into the database.
		$this->db->dbprefix('pharmacies');
		$this->db->where('id',$pharmacy_id);
		$upd_into_db = $this->db->update('pharmacies', $upd_data);
		
		//echo $this->db->last_query(); exit;
		
		if($upd_into_db)
			return true;
		else
			return false;
		
	}//end reset_password($pharmacy_pin, $pharmacy_id)
	
	public function verify_reset_code($verification_code){
		
		$this->db->dbprefix('pharmacies');
		$this->db->where('password_reset_code',$verification_code);
		$this->db->where('password_reset_request','1');
		$get_user= $this->db->get('pharmacies');
		$row_arr = $get_user->row_array();
		//echo $this->db->last_query(); 		exit;
		return $row_arr;		
		
	}//end verify_reset_code($verification_code)

}//end file
?>