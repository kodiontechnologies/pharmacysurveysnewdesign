<div id="navigation" class="navbar navbar-default navbar-fixed-top">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="<?php echo SURL?>">
			<img src="<?php echo IMG?>logo.png" alt="">
			</a>
		</div>
		<div class="navbar-collapse collapse pull-right" id="mainMenu">
			<ul id="menu-main-menu" class="nav navbar-nav">
				<li><a href="<?php echo SURL?>"><i class="fa fa-home" aria-hidden="true"></i>Home</a></li>
				<li><a href="<?php echo SURL?>pages/about-us"><i class="fa fa-group" aria-hidden="true"></i>About us</a></li>
				<li><a href="<?php echo SURL?>pages/faqs"><i class="fa fa-comments-o" aria-hidden="true"></i>FAQs</a></li>
				<li><a href="<?php echo SURL?>pages/contact-us"><i class="fa fa-phone" aria-hidden="true"></i>Contact us</a></li>
                
                <li class="reister">
                	<?php 
						if($this->session->p_id){
					?>
                            <a href="<?php echo SURL?>dashboard"><i class="fa fa-user" aria-hidden="true"></i>Dashboard</a>
                    
                    <?php		
						}else{
					?>
                    
                        <a href="<?php echo SURL?>register"><i class="fa fa-user" aria-hidden="true"></i>Buy Survey</a>

                    <?php		
						}//end if($this->session->p_id)
					?>
                </li>
                <?php 
				
					if(!$this->session->p_id){
						if(!$this->session->g_id){
				?>
                			<li class="reister"><a href="<?php echo SURL?>login"><i class="fa fa-sign-in" aria-hidden="true"></i>Login</a></li>
                
                <?php
						}
						if(!$this->session->g_id){
				?>

							<li class="login"><a href="<?php echo SURL?>group/login"><i class="fa fa-sign-in" aria-hidden="true"></i>Group Login</a></li>
                <?php			
						}else{
				?>
                            <li class="reister"><a href="<?php echo SURL?>group/dashboard"><i class="fa fa-sign-in" aria-hidden="true"></i>Group Dashboard</a></li>
                            <li class="login"><a href="<?php echo SURL?>group/logout"><i class="fa fa-sign-in" aria-hidden="true"></i>Logout</a></li>
                <?php			
						}//end if(!$this->session->g_id)

					}else{
				?>
                		<li class="reister"><a href="<?php echo SURL?>logout"><i class="fa fa-sign-in" aria-hidden="true"></i>Logout</a></li>
                
                <?php		
					}//end if(!$this->session->p_id)
					
				?>					
					
					
			</ul>
		</div>
	</div>
</div>