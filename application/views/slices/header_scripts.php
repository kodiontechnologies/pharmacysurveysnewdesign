<!-- BASE URL for Javascript Files -->
<script>var SURL = '<?php echo SURL?>';</script>

<!-- Google Fonts -->
<link href='http://fonts.googleapis.com/css?family=Open+Sans:100,200,300,400,400italic,500,600,700,700,800,900italic&amp;subset=latin,greek-ext,cyrillic,latin-ext,greek,cyrillic-ext,vietnamese' rel='stylesheet' type='text/css'/>
<link href='http://fonts.googleapis.com/css?family=ABeeZee:100,200,300,400,400italic,500,600,700,700,800,900italic&amp;subset=latin,greek-ext,cyrillic,latin-ext,greek,cyrillic-ext,vietnamese' rel='stylesheet' type='text/css'/>
<link href="<?php echo ASSETS?>fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="<?php echo JS?>html5shiv.min.js"></script>
      <script src="<?php echo JS?>respond.min.js"></script>
    <![endif]-->

<!-- Favicons -->
<link rel="apple-touch-icon-precomposed" href="<?php echo IMG?>apple-touch-icon-precomposedd.png">
<link rel="icon" href="<?php echo IMG?>faviconn.ico">

<?php
	//Including all Required CSS 
	$common_css = array('bootstrap.min.css','shortcodes.css','font-awesome.css','style.css','animate.css','responsive.css','built.css');
	echo add_css($common_css);
	
	$common_js = array('jquery.js');
	echo add_js($common_js);
	
?>
