<?php 
	if(!$_COOKIE['sf_cookie_info']){
?>
        <div class="fixed-bottom" id="cookie_info" style="position: fixed; bottom:0; left:0; z-index: 1030; width:100%">
            <div class="pull-left btn-block cookie_info_bar">
                <div class="pull-left" style="width:80%">
                    This website uses cookies to ensure you get the best experience on our website. <a href="<?php echo SURL?>pages/cookies"> More info</a> 
                </div>
                <div class="pull-left" style="width:20%">
                    <button class="pull-right btn btn-success" onclick="set_cookie_info(); ">Got it</button>
                </div>
            </div>
        </div>
<?php 
	}//end if($_COOKIE['getjab_cookie_info'])
?>

<?php
	$footer_cms = get_cms_page('footer');
	echo ($footer_cms['page_description']) ? filter_string($footer_cms['page_description']) : '' ;
?>


<div class="footerbottom">
  <div class="grid">
    <div class="row"><!-- left -->
      <div class="col-md-12">
        <div class="col-md-3"></div>
        <div class="col-md-6 text-align-center">
          <p class="copyn">&copy; All rights reserved. 2017 Digital Health Group Ltd</p>
        </div>
        <div class="col-md-3">
          <p class="copyn">
            <?php 
		  	if($this->session->p_id){
		  ?>
            <a href="http://surveyfocus.co.uk/assets/pdf/UserManual.pdf" target="_blank">User Manual</a> |
            <?php 
		  	}//end if($this->session->p_id)
		  ?>
            <a href="http://surveyfocus.co.uk/assets/pdf/SurveyFocus-PrivacyPolicy.pdf" target="_blank">Privacy Policy</a> </p>
        </div>
      </div>
      <!-- right --> <!-- end right --></div>
  </div>
</div>
<div id="back-top"> <a href="#top"><span><i class="fa fa-chevron-up"></i></span></a> </div>
<script>
    jQuery(window).scroll(function() {
        if (jQuery(this).scrollTop() > 200){  
            jQuery('#back-top').addClass("sticky");
        }
        else{
            jQuery('#back-top').removeClass("sticky");
        }
    });
</script>