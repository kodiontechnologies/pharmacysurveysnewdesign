<?php
	$common_js = array('bootstrap.js','easing.js','smoothscroll.js','wow.js','appear.js','fitvids.js','common.js','flexslider.js','countto.js','query.cycle.all.min.js','carousel.js','carousel-anything.js','carousel-testimonials.js','isotope.js','carousel-blog.js','parallax.js','validate.js');
	
	echo add_js($common_js);
	echo $js; 
?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-90341199-2', 'auto');
  ga('send', 'pageview');

</script>