<?php 
	$tab = $this->input->get('t');

?>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<div class="module">
  <div class="stripe-4">
    <div class="container">
      <div class="row">
        <h2>Dashboard</h2>
        <ul class="breadcrumbs">
            <li><a href="medical-home.html">Home</a></li>
            <li> > </li>
            <li class="active">Dashboard</li>
        </ul>
      </div>
     </div>
    </div>
</div>


<div class="dashboard_main">
  <div class="container">
  
    <div class="row for_row_escap">
		  <?php
            if($this->session->flashdata('err_message')){
        ?>
                <div class="alert alert-danger hide_alert"><?php echo $this->session->flashdata('err_message'); ?></div>
          <?php
            }//end if($this->session->flashdata('err_message'))
            
            if($this->session->flashdata('ok_message')){
        ?>
                <div class="alert alert-success alert-dismissable hide_alert"><?php echo $this->session->flashdata('ok_message'); ?></div>
          <?php 
            }//if($this->session->flashdata('ok_message'))
        ?>
      <ul class="nav nav-tabs">
      
        <li class="<?php echo ($tab == 1 || !$tab)? 'active' : ''?>"><a href="#my_pharmacy_tab" data-toggle="tab">My Pharmacies</a></li>
        <li class="<?php echo ($tab == 2)? 'active' : ''?>"><a href="#settings_tab" data-toggle="tab">Edit Profile</a></li>
        <li class="<?php echo ($tab == 3)? 'active' : ''?>"><a href="#password_tab" data-toggle="tab">Change Password</a></li>

      </ul>
      <div class="tab-content">
        <div id="my_pharmacy_tab" class="tab-pane <?php echo ($tab == 1 || !$tab)? 'active' : ''?>"> 

			<div class="table-responsive">
                <table class="table table-condensed table-striped table-bordered table-hover no-margins">
                  <thead>
                    <tr>
                      <th>Pharmacy Name</th>
                      <th>Address</th>
                      <th>Contact No.</th>
                      <th>Email Address</th>
                      <th>Current Survey</th>
                      <th>Status</th>
                      <?php 
					  	if($this->session->business_type == 'C'){
					  ?>
	                      <th>Login</th>
                      <?php }?>
                      
                    </tr>
                  </thead>
                  <tbody>
					<?php 
                        if(count($group_pharmacy_list) > 0){
                            for($i=0;$i<count($group_pharmacy_list);$i++){
								
								$pharmacy_current_survey = $this->survey->get_pharmacy_current_survey($group_pharmacy_list[$i]['id']);
								
								if(!$pharmacy_current_survey){
									
									$get_most_recent_survey = get_most_recent_survey($group_pharmacy_list[$i]['id']);	
									$survey_title = $get_most_recent_survey['survey_title'];
									$survey_id = $get_most_recent_survey['id'];

									if($get_most_recent_survey['survey_start_date'] == NULL ){
									
										$survey_status = 'Not Started';
										
									}else{

										$recent_survey_end_date = $get_most_recent_survey['survey_finish_date'];
										
										if($recent_survey_end_date >= date('Y-m-d')){
											//Survey is complete but not yet expired
											$survey_status = 'Survey Completed';
										}else{
											//No Survy available, No Expired, No Purchased. Go for buying new Servey
											$survey_status = 'Survey Expired';
										}
										
									}//end if($get_most_recent_survey['survey_start_date'] == NULL )
									
								}else{
									$survey_title = filter_string($pharmacy_current_survey['survey_title']);
									$survey_status = 'Survey in Progress';
									$survey_id = $pharmacy_current_survey['id'];
								}//end if(!$pharmacy_current_survey)

								$total_submitted_surveys = get_survey_submitted_count($group_pharmacy_list[$i]['id'], $survey_id);
								$total_submitted_online_surveys = $this->survey->get_survey_submitted_count($group_pharmacy_list[$i]['id'], $survey_id,'ONLINE');
								$total_submitted_paper_surveys = $this->survey->get_survey_submitted_count($group_pharmacy_list[$i]['id'], $survey_id,'PAPER');
                    ?>
                                <tr>
                                  <td>
								  	
                                    <a href="#" data-html="true" data-toggle="tooltip" data-original-title="<p class='text-left'><strong>Minimum Required:</strong> <?php echo $pharmacy_current_survey['min_no_of_surveys']?> </p>
                                    <p class='text-left'><strong>Total Completed:</strong> <?php echo $total_submitted_online_surveys + $total_submitted_paper_surveys?> </p> <p class='text-left'><strong>Online Surveys:</strong> <?php echo $total_submitted_online_surveys?> </p><p class='text-left'><strong>Paper based Surveys:</strong> <?php echo $total_submitted_paper_surveys?> </p>
                                    " data-placement="right" ><?php echo filter_string($group_pharmacy_list[$i]['pharmacy_name'])?></a>
                                  </td>
                                  <td>
                                    <?php echo filter_string($group_pharmacy_list[$i]['address']).', ';?> 
                                    <?php echo (filter_string($group_pharmacy_list[$i]['address_2'])) ? filter_string($group_pharmacy_list[$i]['address_2']).', ' : '';?> 
                                    <?php echo filter_string($group_pharmacy_list[$i]['postcode']);?><br /> 
                                    <?php echo filter_string($group_pharmacy_list[$i]['county']);?>
                                  </td>
                                  <td><?php echo filter_string($group_pharmacy_list[$i]['contact_no'])?></td>
                                  <td><?php echo filter_string($group_pharmacy_list[$i]['email_address'])?></td>
                                  <td><?php echo $survey_title;?></td>
                                  <td><?php echo $survey_status;?></td>
								  <?php 
                                    if($this->session->business_type == 'C'){
                                  ?>
                                        <td>
                                            <form name="login_frm" action="<?php echo SURL?>login/login-process" method="post" target="_blank">
                                                <button class="btn btn-sm btn-success" type="submit" name="">Login</button>
                                                <input type="hidden" name="email_address" value="<?php echo filter_string($group_pharmacy_list[$i]['email_address']);?>" readonly="readonly">
                                                <input type="hidden" name="password" value="<?php echo filter_string($group_pharmacy_list[$i]['password']);?>" readonly="readonly">
                                                <input type="hidden" name="is_group" value="1" readonly="readonly">
                                            </form>
                                            
                                          </td>                                  
                                  <?php }?>
                                  
                                  
                                </tr>                                
                    <?php				
                            }//end for($i=0;$i<count($group_pharmacy_list);$i++)
                        }else{
                    ?>
                            <tr>
                                <td class="text-danger" colspan="6">No Record Found</td>
                            </tr>
                    <?php		
                        }//end if(count($group_pharmacy_list) > 0)
                    ?>
                    
                    
                  </tbody>
                </table>
              </div>

        </div>
        
        <div id="settings_tab" class="tab-pane <?php echo ($tab == 2)? 'active' : ''?>">
          <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
              <div class="contact_content_border">
                <div class="module2">
                  <div class="stripe-5">
                    <h4> Update Settings</h4>
                  </div>
                </div>
                <form id="group_edit_profile" name="group_edit_profile" action="<?php echo base_url(); ?>group/update-group-profile" method="post" class="wowcontactform">
                    <div class="contact_content_border2">
                      <div class="single-field half-field ful_field form-group">
                        <label>Business name </label>
						<input type="text" name="business_name" id="business_name" placeholder="Business name"  value="<?php echo ($business_profile['business_name']) ? filter_string($business_profile['business_name']) : '' ; ?>" required="required" class="input-name" data-fv-regexp="true" data-fv-regexp-regexp="^[a-zA-z0-9\s\-,\]+[a-zA-Z0-9\s\-_,.'/]{1,100}$" data-fv-regexp-message="Please use allowed characters (Alphabet, Numbers, Hyphens, Underscore, Comma, Apostrophe, Dot, Forward Slash, Back Slash, Space)" maxlength="100">
                      </div>
                      <div class="single-field half-field ful_field form-group">
                        <label> Contact name </label>
                        <input type="text" name="contact_name" id="contact_name" placeholder="Contact name"  value="<?php echo ($business_profile['contact_name']) ? filter_string($business_profile['contact_name']) : '' ; ?>" required="required" class="input-name">
                      </div>
                      <div class="single-field half-field ful_field form-group">
                        <label> Contact number </label>
                        <input type="text" name="contact_no" id="contact_no" placeholder="Contact number"  value="<?php echo ($business_profile['contact_no']) ? filter_string($business_profile['contact_no']) : '' ; ?>" required="required" class="input-name" data-fv-regexp="true"  data-fv-regexp-regexp="^(?=.*[0-9])[0-9]{11,}$" data-fv-regexp-message="Please use allowed characters (Numbers) and length should be minimum 11 characters"  maxlength="15">
                      </div>
                      
                      <div class="single-field half-field ful_field form-group">
                        <label> Email address </label>
                        <input type="email" name="email_address" id="email_address" placeholder="Email address"  value="<?php echo ($business_profile['email_address']) ? filter_string($business_profile['email_address']) : '' ; ?>" required="required" class="input-name" data-fv-regexp="true" data-fv-regexp-regexp="^[a-zA-z0-9\s\-]+[a-zA-Z0-9\s\-_.@]+$" data-fv-regexp-message="Please use allowed characters (Alphabet, Numbers, Hyphens, Underscore, Dot and @)" autocomplete="off" maxlength="50">
                        <div id="error_msg" class="error help-block" style="font-size: 85%;"></div>
                      </div>
                      
                      <div class="single-field half-field ful_field form-group">
                        <div class="text-right"><button type="submit" id="submit_21" value="Update Profile" class="btn btn-success">Update Profile</button>
                      </div>
                    </div>
                  </div>
                </form>
                <div class="clearfix"></div>
              </div>
            </div>
            <div class="col-md-3"></div>
            
            
          </div>
        </div>
        
        <div id="password_tab" class="tab-pane <?php echo ($tab == 3)? 'active' : ''?>">
          <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6 ">
              <div class="contact_content_border">
                <div class="module2">
                  <div class="stripe-5">
                    <h4> Change Password </h4>
                  </div>
                </div>
                <form id="group_change_pass_frm" name="group_change_pass_frm" action="<?php echo base_url(); ?>group/create-new-password" method="post" >
                    <div class="contact_content_border2">
                      <div class="single-field half-field ful_field form-group">
                        <label> Current password </label>
                        <input type="password" name="password" id="password" placeholder="Current Password"  value="" required="required" class="input-name" data-fv-regexp="true" data-fv-regexp-regexp="^^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[A-Za-z0-9]{8,20}$" data-fv-regexp-message="Password must be between 8 to 20 characters with atleast one uppercase, one lowercase and one digit. Allowed characters (Alphabet, Numbers)" maxlength="20">
                      </div>
                      <div class="single-field half-field ful_field form-group">
                        <label> New password </label>
                        <input type="password" name="new_password" id="password" placeholder="New Password"  value="" required="required" class="input-name" data-fv-regexp="true" data-fv-regexp-regexp="^^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[A-Za-z0-9]{8,20}$" data-fv-regexp-message="Password must be between 8 to 20 characters with atleast one uppercase, one lowercase and one digit. Allowed characters (Alphabet, Numbers)" maxlength="20">
                      </div>
                      <div class="single-field half-field ful_field form-group">
                        <label> Retype password </label>
                        <input type="password" name="re_new_password" id="re_password" placeholder="Retype password"  value="" required="required" class="input-name" data-fv-identical="true" data-fv-identical-field="new_password" data-fv-identical-message="The password and its confirm password does not match" maxlength="20" >
                      </div>
                      <div class="text-right"> 
                        <button type="submit" id="submit_21" value="Change Password" class="btn btn-success">Change Password</button>
                      </div>
                    </div>
                </form>
                <div class="clearfix"></div>
              </div>
            </div>
            <div class="col-md-3"></div>
            
            
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- This chart is used  to save the SVG for question 9 for survey poster-->
<div id="piechart_9" class="hidden" style="width: 1000px; height:100%;"></div> 
<script src="https://getaddress.io/js/jquery.getAddress-2.0.5.min.js"></script> 

<!-- Add after your form --> 
<script>
(function($){
$('#postcode_lookup').getAddress({
   api_key: '<?php echo POSTCODE_KEY?>',
    <!--  Or use your own endpoint - api_endpoint:https://your-web-site.com/getAddress, -->
    output_fields:{
        line_1: '#address',
        line_2: '#address_2',
        line_3: '#line3',
        post_town: '#town',
        county: '#county',
        postcode: '#postcode'
    },
<!--  Optionally register callbacks at specific stages -->                                                                                                               
    onLookupSuccess: function(data){/* Your custom code */},
    onLookupError: function(){/* Your custom code */},
    onAddressSelected: function(elem,index){/* Your custom code */}
});
})(jQuery);

jQuery(document).ready(function(){

	jQuery('.show_hide_textbox').click(function(){
		
		checked_val = jQuery(this).attr('value');

		if(checked_val == 4){
			jQuery('#question_10_txtbox').removeClass('hidden');
		}else{
			jQuery('#question_10_txtbox').addClass('hidden');
			jQuery('#other_reason_txt').val('');
			
		}
	})
	
});
</script> 
<script>
jQuery(document).ready(function(){
    jQuery('[data-toggle="tooltip"]').tooltip();
});
</script>