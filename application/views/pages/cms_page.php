<div class="module">
  <div class="stripe-4">
    <div class="container">
      <div class="row">
        <h2><?php echo ($cms_page['page_title']) ? ucfirst(filter_string($cms_page['page_title'])) : '' ; ?></h2>
        <ul class="breadcrumbs">
          <li><a href="<?php echo SURL?>">Home</a></li>
          <li> > </li>
          <li class="active"><?php echo ($cms_page['page_title']) ? ucfirst(filter_string($cms_page['page_title'])) : '' ; ?></li>
        </ul>
      </div>
    </div>
  </div>
</div>
<div class="site-content pt-80"> 
  <!-- Start Fullwide Map -->
  <div class="fullwide-map"> 
    
    <!-- End Fullwide Map -->
    <div class="container">
      <div class="row">
		<?php
        if($this->session->flashdata('err_message')){
        ?>
            <div class="alert alert-danger"><?php echo $this->session->flashdata('err_message'); ?></div>
        <?php
        }//end if($this->session->flashdata('err_message'))
        
        if($this->session->flashdata('ok_message') && $this->session->flashdata('ok_message') != 'thank-you'){
        ?>
            <div class="alert alert-success alert-dismissable"><?php echo $this->session->flashdata('ok_message'); ?></div>
        <?php 
        }//if($this->session->flashdata('ok_message'))
        ?>
      </div>
  
        <div class="col-md-12">
          <div class="content-area pt-100">
          <div class="row">
              <div class="col-md-12">
              <div class="module2">
                    <div class="stripe-5">
                        <h4 class="no-margins"> <?php echo ($cms_page['page_title']) ? ucfirst(filter_string($cms_page['page_title'])) : '' ; ?></h4>
                    </div>
                </div>
              </div>
          </div>
            <div class="row">
              <div class="col-md-12 m-t"> <?php echo ($cms_page['page_description']) ? filter_string($cms_page['page_description']) : '' ; ?> </div> </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>