<div class="module">
  <div class="stripe-4">
    <div class="container">
      <div class="row">
        <h2>FAQ's</h2>
        <ul class="breadcrumbs">
          <li><a href="<?php echo SURL?>">Home</a></li>
          <li> > </li>
          <li class="active">FAQ's</li>
        </ul>
      </div>
    </div>
  </div>
</div>
<div class="site-content pt-80"> 
  <!-- Start Fullwide Map -->
  <div class="fullwide-map"> 
    
    <!-- End Fullwide Map -->
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="content-area pt-100">
          <div class="row">
              <div class="col-md-12">
              <div class="module2">
                    <div class="stripe-5">
                        <h4 class="no-margins">FAQ's</h4>
                    </div>
                </div>
              </div>
          </div>
            <div class="row">
              <div class="col-md-12 m-t">
                
                <?php echo ($cms_page['page_description']) ? filter_string($cms_page['page_description']) : '' ; ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
