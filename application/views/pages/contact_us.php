<div class="module">
  <div class="stripe-4">
    <div class="container">
      <div class="row">
        <h2>Contact Us</h2>
        <ul class="breadcrumbs">
          <li><a href="<?php echo SURL?>">Home</a></li>
          <li> > </li>
          <li class="active">Contact Us</li>
        </ul>
      </div>
    </div>
  </div>
</div>
<div class="site-content pt-80"> 
  <!-- Start Fullwide Map -->
  <div class="fullwide-map"> 
    
    <!-- End Fullwide Map -->
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="content-area pt-100">
            <div class="row">
                <?php
                    if($this->session->flashdata('err_message')){
                ?>
                <div class="alert alert-danger"><?php echo $this->session->flashdata('err_message'); ?></div>
                <?php
                    }//end if($this->session->flashdata('err_message'))
        
                    if($this->session->flashdata('ok_message')){
                ?>
                <div class="alert alert-success alert-dismissable"><?php echo $this->session->flashdata('ok_message'); ?></div>
                <?php 
                    }//if($this->session->flashdata('ok_message'))
                ?>
              <div class="contact-us">
                <div class="row">
                  <div class="col-xs-12 col-sm-8 col-md-7 pb-100">
                    <div class="contact_content_border">
                      <div class="module2">
                        <div class="stripe-5">
                          <h4>GET IN TOUCH</h4>
                        </div>
                      </div>
                      <div class="contact_content_border2">
                        <form action="<?php echo base_url(); ?>pages/submit-contact-us-form" id="form-qTqrK85MqM" method="post" name="form-qTqrK85MqM">
                          <!-- End Single Field -->
                          <div class="single-field half-field form-group">
                          	<label>First Name</label>
                            <input class="input-name" id="author" name="first_name" placeholder="First Name" tabindex="1" type="text" value="" required="required" data-fv-regexp="true"  data-fv-regexp-regexp="^[a-zA-z0-9\s\-,\]+[a-zA-Z0-9\s\-_,.'/]{1,50}$" data-fv-regexp-message="Please use allowed characters (Alphabet, Numbers, Hyphens, Underscore, Comma, Apostrophe, Dot, Forward Slash, Back Slash, Space)" maxlength="50">
                          </div>
                          <!-- End Single Field -->
                          <div class="single-field half-field-last form-group">
                          		<label>Last Name</label>
                                <input required="required" class="input-name" id="last_name" name="last_name" placeholder="Last Name" tabindex="2" type="text" value="" data-fv-regexp="true"  data-fv-regexp-regexp="^[a-zA-z0-9\s\-,\]+[a-zA-Z0-9\s\-_,.'/]{1,50}$" data-fv-regexp-message="Please use allowed characters (Alphabet, Numbers, Hyphens, Underscore, Comma, Apostrophe, Dot, Forward Slash, Back Slash, Space)" maxlength="50">
                          </div>
                          <div class="single-field half-field form-group">
                          	<label>Email Address</label>
                            <input required="required" class="input-name" placeholder="Email Address" id="email" name="email" tabindex="2" type="email" value="" data-fv-regexp="true" data-fv-regexp-regexp="^[a-zA-z0-9\s\-]+[a-zA-Z0-9\s\-_.@]+$"
                    data-fv-regexp-message="Please use allowed characters (Alphabet, Numbers, Hyphens, Underscore, Dot and @)" autocomplete="off" maxlength="30">
                          </div>
                          <!-- End Single Field -->
                          <div class="single-field half-field-last form-group">
                          	<label>Phone Number (optional)</label>
                            <input class="input-name" id="phone" name="phone" placeholder="Phone Number (optional)" tabindex="3" type="tel" value=""  data-fv-regexp="true"  data-fv-regexp-regexp="^(?=.*[0-9])[0-9]{11,}$" data-fv-regexp-message="Please use allowed characters (Numbers) and length should be minimum 11 characters"  maxlength="15">
                          </div>
                          
                          <!-- End Single Field --> 
                          <!-- End Single Field -->
                          <div class="clearfix"></div>
                          <div class="single-field form-group">
                          	<label>Your Message</label>
                            <textarea class="textarea-comment" placeholder="Your Message" id="comment" name="message" tabindex="4" style="height:150px" required="required" ></textarea>
                          </div>
                          <!-- End Single Field -->
                          <div class="text-right"> 

                            <button type="submit" name="contact_us_btn" id="contact_us_btn" value="Send Your Message" class="btn btn-success">Send Your Message</button>
                           </div>
                          <div class="clearfix"></div>
                        </form>
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-12 col-sm-4 col-md-5 pb-100">
                    <div class="contact_content_border">
                      <div class="module2">
                        <div class="stripe-5">
                          <h4>Contact Us</h4>
                        </div>
                      </div>
                      
                      <div class="contact_content_border2">
                      	 <?php echo ($cms_page['page_description']) ? filter_string($cms_page['page_description']) : '' ; ?>
                      </div>
                    </div>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

