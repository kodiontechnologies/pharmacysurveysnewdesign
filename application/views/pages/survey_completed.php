<?php 
if(!$this->session->flashdata('thankyou_message'))
	//redirect(SURL);	
?>

<div class="site-content pt-80"> 
  <!-- Start Fullwide Map -->
  <div class="fullwide-map"> 
    
    <!-- End Fullwide Map -->
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="content-area pt-100">
            <div class="row">
              <div class="col-md-12 text-center"><?php echo ($cms_page['page_description']) ? filter_string($cms_page['page_description']) : '' ; ?> </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

