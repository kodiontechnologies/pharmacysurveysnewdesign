<div class="module">
  <div class="stripe-4">
    <div class="container">
      <div class="row">
        <h2>Register Now</h2>
        <ul class="breadcrumbs">
          <li><a href="<?php echo SURL?>">Home</a></li>
          <li> > </li>
          <li class="active">Register</li>
        </ul>
      </div>
    </div>
  </div>
</div>
<div class="site-content pt-80"> 
  <!-- Start Fullwide Map -->
  <div class="fullwide-map"> 
    
    <!-- End Fullwide Map -->
    <div class="container">
      <div class="col-md-12">
        <div class="content-area pt-100">
          <div class="row">
            <div class="col-md-12">
				<?php
                if($this->session->flashdata('err_message')){
                ?>
                    <div class="alert alert-danger hide_alert"><?php echo $this->session->flashdata('err_message'); ?></div>
                <?php
                }//end if($this->session->flashdata('err_message'))
                
                if($this->session->flashdata('ok_message') && $this->session->flashdata('ok_message') != 'thank-you'){
                ?>
                    <div class="alert alert-success alert-dismissable hide_alert"><?php echo $this->session->flashdata('ok_message'); ?></div>
                <?php 
                }//if($this->session->flashdata('ok_message'))
                ?>
              <div class="contact_content_border">
                <div class="module2">
                  <div class="stripe-5">
                    <h4>Sign Up</h4>
                  </div>
                </div>
                <form id="register_frm" name="register_frm" method="post"  enctype="multipart/form-data" action="<?php echo SURL;?>register/register-process">
                
                    <div class="contact_content_border2" style="overflow:auto">
                      
                        <p>Please enter your personal and business details</p>
                          <!--<form id="register_frm" name="register_frm" method="post" enctype="multipart/form-data" action="<?php echo SURL;?>register/payment">-->
                            <span id="step_1_fields">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="single-field col-md-12 no-paddings form-group">
                                            <label>Business name</label>
                                            <input type="text" name="pharmacy_name" id="pharmacy_name" required="required" placeholder="Business name" value="<?php echo ($this->session->registration_form['pharmacy_name']) ? $this->session->registration_form['pharmacy_name'] : '' ; ?>" data-fv-regexp="true" data-fv-regexp-regexp="^[a-zA-z0-9\s\-,\]+[a-zA-Z0-9\s\-_,.'/]{1,100}$" data-fv-regexp-message="Please use allowed characters (Alphabet, Numbers, Hyphens, Underscore, Comma, Apostrophe, Dot, Forward Slash, Back Slash, Space)" maxlength="100">
                                            
                                        </div>
                                        
                                        <div class="single-field col-md-12 no-paddings form-group">
                                            <label>Contact name</label>
                                             <input type="text" name="owner_name" id="owner_name" placeholder="Contact name" value="<?php echo ($this->session->registration_form['owner_name']) ? $this->session->registration_form['owner_name'] : '' ; ?>" aria-required="true" class="input-name" required data-fv-regexp="true" data-fv-regexp-regexp="^[a-zA-z0-9\s\-,\]+[a-zA-Z0-9\s\-_,.'/]{1,50}$" data-fv-regexp-message="Please use allowed characters (Alphabet, Numbers, Hyphens, Underscore, Comma, Apostrophe, Dot, Forward Slash, Back Slash, Space)" maxlength="50">
                                        
                                        </div>
                                        
                                        <div class="single-field col-md-12 no-paddings form-group">
                                            <label>Contact number</label>
                                            <input type="text" name="contact_no" id="contact_no" placeholder="Contact number" value="<?php echo ($this->session->registration_form['contact_no']) ? $this->session->registration_form['contact_no'] : '' ; ?>" aria-required="true" class="input-name" required data-fv-regexp="true"  data-fv-regexp-regexp="^(?=.*[0-9])[0-9]{11,}$" data-fv-regexp-message="Please use allowed characters (Numbers) and length should be minimum 11 characters"  maxlength="15">
                                        
                                        </div>
                                        
                                        <div class="single-field col-md-12 no-paddings form-group">
                                            <label>Email address</label>
                                            <input type="email" name="email_address" id="email_address" placeholder="Email address" value="<?php echo ($this->session->registration_form['email_address']) ? $this->session->registration_form['email_address'] : '' ; ?>" aria-required="true" class="input-name" required data-fv-regexp="true" data-fv-regexp-regexp="^[a-zA-z0-9\s\-]+[a-zA-Z0-9\s\-_.@]+$" data-fv-regexp-message="Please use allowed characters (Alphabet, Numbers, Hyphens, Underscore, Dot and @)" autocomplete="off" maxlength="50">
                                            
                                            <div id="error_msg" class="error help-block" style="font-size: 85%;"></div>
                                        
                                        </div>
                                        
                                        <div class="single-field col-md-12 no-paddings form-group">
                                            <label>Password</label>
                                           <input type="password" name="password" id="password" placeholder="Password" value="<?php echo ($this->session->registration_form['password']) ? $this->session->registration_form['password'] : '' ; ?>" class="input-name" required data-fv-regexp="true" data-fv-regexp-regexp="^^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[A-Za-z0-9]{8,20}$" data-fv-regexp-message="Password must be between 8 to 20 characters with atleast one uppercase, one lowercase and one digit. Allowed characters (Alphabet, Numbers)" maxlength="20">
                                        
                                        </div>
                                        
                                        <div class="single-field col-md-12 no-paddings form-group">
                                            <label>Retype Password</label>
                                           <input type="password" name="re_password" id="re_password" placeholder="Retype password"  aria-required="true" class="input-name"  data-fv-identical="true" data-fv-identical-field="password" data-fv-identical-message="The password and its confirm password does not match" maxlength="20" >
                                        
                                        </div>
    
                                        <div class="single-field col-md-12 no-paddings form-group">
                                            <label>Pharmacy Type</label>
                                            <select name="pharmacy_type" id="pharmacy_type">
                                                <option value="OF" <?php echo ($this->session->registration_form['pharmacy_type'] == 'OF') ? 'selected="selected"' : 'selected="selected"' ?> >Community Pharmacy</option>
                                                <option value="O" <?php echo ($this->session->registration_form['pharmacy_type'] == 'O') ? 'selected="selected"' : '' ?>>Online Pharmacy</option>
                                            </select>
                                        </div>                                        
                                        
                                    </div>
                                    <div class="col-md-6">
                                        <div class="single-field col-md-12 no-paddings form-group">
                                            <label>Find your Postcode</label>
                                            <div id="postcode_lookup" class=""></div>
                                        </div>
                                        
                                        <div class="single-field col-md-12 no-paddings form-group">
                                            <label>Address 1</label>
                                            <input type="text" name="address" id="address" placeholder="Address 1" value="<?php echo ($this->session->registration_form['address']) ? $this->session->registration_form['address'] : '' ; ?>" class="input-name" required data-fv-regexp="true" data-fv-regexp-regexp="^[a-zA-z0-9\s\-,\]+[a-zA-Z0-9\s\-_,.'/]{1,50}$"  data-fv-regexp-message="Please use allowed characters (Alphabet, Numbers, Hyphens, Underscore, Comma, Apostrophe, Dot, Forward Slash, Back Slash, Space)" maxlength="50">
                                        </div>
                                        
                                        <div class="single-field col-md-12 no-paddings form-group">
                                            <label>Address 2 (optional)</label>
                                            <input type="text" name="address_2" id="address_2" placeholder="Address 2" value="<?php echo ($this->session->registration_form['address_2']) ? $this->session->registration_form['address_2'] : '' ; ?>" class="input-name" data-fv-regexp="true" data-fv-regexp-regexp="^[a-zA-z0-9\s\-,\]+[a-zA-Z0-9\s\-_,.'/]{1,50}$"  data-fv-regexp-message="Please use allowed characters (Alphabet, Numbers, Hyphens, Underscore, Comma, Apostrophe, Dot, Forward Slash, Back Slash, Space)" maxlength="50">
                                        </div>
                                        
                                        <div class="single-field col-md-12 no-paddings form-group">
                                            <label>Town</label>
                                            <input type="text" name="town" id="town" placeholder="Town" value="<?php echo ($this->session->registration_form['town']) ? $this->session->registration_form['town'] : '' ; ?>" class="input-name" required data-fv-regexp="true" data-fv-regexp-regexp="^[a-zA-z0-9\s\-,\]+[a-zA-Z0-9\s\-_,.'/]{1,50}$" data-fv-regexp-message="Please use allowed characters (Alphabet, Numbers, Hyphens, Underscore, Comma, Apostrophe, Dot, Forward Slash, Back Slash, Space)" maxlength="50">
                                        </div>
                                        
                                        <div class="single-field col-md-12 no-paddings form-group">
                                            <label>County (optional)</label>
                                            <input type="text" name="county" id="county" placeholder="County" value="<?php echo ($this->session->registration_form['county']) ? $this->session->registration_form['county'] : '' ; ?>" class="input-name" data-fv-regexp="true" data-fv-regexp-regexp="^[a-zA-z0-9\s\-,\]+[a-zA-Z0-9\s\-_,.'/]{1,50}$"  data-fv-regexp-message="Please use allowed characters (Alphabet, Numbers, Hyphens, Underscore, Comma, Apostrophe, Dot, Forward Slash, Back Slash, Space)">
                                        </div>
                                        
                                        <div class="single-field col-md-12 no-paddings form-group">
                                            <label>Postcode</label>
                                            <input type="text" name="postcode" id="postcode" placeholder="Postcode" value="<?php echo ($this->session->registration_form['postcode']) ? $this->session->registration_form['postcode'] : '' ; ?>" class="input-name" required data-fv-regexp="true"  data-fv-regexp-regexp="^[a-z|A-Z|]+[a-z|A-Z|0-9|\s]{0,10}$"
                                  data-fv-regexp-message="Please use allowed characters (Alphabets, Numbers and spaces) and first character must be alphabet." maxlength="10">
                                        </div>
                                        
                                    </div>
    
                                </div> 
    
                                <!--<div class="section_cols_left no_padd custm_contact_button" style="text-align:right"> 
                                    <input name="add_pharamcy_btn" type="button" onCLick="verify_email();" value="Continue" class="btn btn-success">
                                </div>-->
                            </span>
                            <div class="clearfix"></div>
                            <!--<input type="hidden" id="submit_step_1" name="submit_step_1" value="0" />-->
                    </div>
                    <div class="module2">
                      <div class="stripe-5">
                        <h4>Payment Options</h4>
                      </div>
                    </div>
                    <div class="contact_content_border2" style="overflow:auto">
                        <p>Please select your payment method</p>
                        <div class="col-md-8">
                          <div class="payments_tabs">
                            <div class="row for_row_escap">
                              <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#card_tab"><img src="<?php echo IMAGES?>visa_mastercard_logo.png" alt=""></a></li>
                                <li><a data-toggle="tab" href="#paypal_tab"><img src="<?php echo IMAGES?>payal-logo.png" alt=""></a></li>
                              </ul>
                              <div class="tab-content">
                                <div id="card_tab" class="tab-pane fade in active">
                                  <p>Please enter your credit card details below.</p>
                                  <div class="single-field half-field ful_field form-group">
                                      <label for="card number" >Card Number</label>
                                      <input name="card" id="card" type="text" placeholder="Card Number" required="required">
                                    
                                  </div>
                                  <div class="row">
                                    <div class="col-md-4">
                                      <div class="single-field half-field ful_field form-group">
                                        <label for="expMonth" >Expiry Month</label>

                                        <select class="form-control" name="expMonth" id="expMonth" required="required">
                                        <option value="">Month</option>
                                            <?php 
                                                for($i=1;$i<=12;$i++){
                                            ?>
                                                    <option value="<?php echo ($i<10) ? '0'.$i : $i?>"><?php echo ($i<10) ? '0'.$i : $i?></option>
                                            <?php
                                            
                                               }//end for($i=date('Y');$i<=date('Y')+10;$i++)
                                            ?>
                                        </select>
                                      </div>
                                    </div>
                                    <div class="col-md-4">
                                      <div class="single-field half-field ful_field form-group">
                                        <label>&nbsp;</label>
                                        <select class="form-control" name="expYear" id="expYear" required="required">
                                          <option value="">Year</option>
                                              <?php 
                                                  for($i=date('Y');$i<=date('Y')+10;$i++){
                                              ?>
                                                <option value="<?php echo $i?>"><?php echo $i?></option>
                                              <?php
                                                                              
                                                  }//end for($i=date('Y');$i<=date('Y')+10;$i++)
                                              ?>
                                        </select>
                                      </div>
                                    </div>
                                    <div class="col-md-4">
                                      <div class="single-field half-field ful_field form-group">
                                        <label>CVV</label>
                                        <input name="cvc" id="cvc" type="text" placeholder="CVV" required="required">
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div id="paypal_tab" class="tab-pane fade">
                                  <p>You can make payment using your PayPal account. </p>
                                  <div class="row">
                                    <div class="col-md-5">
                                      <h5 class="paypal_accepts">Paypal accepts: </h5>
                                    </div>
                                    <div class="col-md-5"> <img src="<?php echo IMAGES?>paypal_cards.jpg" alt=""> </div>
                                  </div>
                                  <div class="clearfix"></div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-2 text-left">
                            </div>
                            <div class="col-md-7 text-right m-t form-group">
                                <label><input type="checkbox" name="terms_conditions" required /> I accept the <a href="<?php echo SURL?>pages/terms-and-conditions" target="_blank"><strong><u>Terms and Conditions</u></strong></a>                
                                </label>                 </div>
                            
                            <div class="col-md-3 text-right no-paddings">
                            <input name="add_pharamcy_btn" id="save_btn" onClick="validate_register_frm();" type="button"  value="Make Payment" class="btn btn-success no-margins">
                            
                            
                             <a href="#credit_card_confirm_popup" id="confirm_popup_trigger" style="text-decoration:none" class="hidden fancy_dialogue_payment">Make Payment</a>
                            
                            <div id="credit_card_confirm_popup" style="display:none; min-width: 480px;">
                              <div class="color-line"></div>
                              <div class="modal-body">
                                <div class="modal-header text-left">
                                  <h4 class="modal-title" id="payment_popup_message">Please wait while processing your payment.</h4>
                                </div>
                              </div>
                            </div>

                            </div>
                            <div class="text-right"> 
                            </div>
                          </div>
                        </div>
                        <div class="col-md-4 padd_right_0">
                          <div class="contact_content_border order_suumry">
                            <div class="module2">
                              <div class="stripe-5">
                                <h4> Order Summary</h4>
                              </div>
                            </div>
                            <div class="contact_content_border2">
                              <div class="box1">
                                <div style="height: 5px"></div>
                                <div class="a">
                                  <div class="col-md-8"><strong><?php echo ($survey_session) ? $survey_session : '' ; ?></strong></div>
                                  <div class="col-md-4"></div>
                                </div>
                                <div class="clearfix"></div>
                                <hr style="margin:10px">
                                <div class="a">
                                  <div class="col-md-8">Sub Total</div>
                                  <div class="col-md-4">&pound;<?php echo ($sub_total) ? filter_price($sub_total) : '' ; ?></div>
                                </div>
                                <div class="clearfix"></div>
                                <hr style="margin:10px">
                                <div class="a">
                                  <div class="col-md-8">VAT (<?php echo ($vat) ? $vat : '' ; ?>%)</div>
                                  <div class="col-md-4">&pound;<?php echo ($vat_amount) ? filter_price($vat_amount) : '' ; ?></div>
                                </div>
                                <div class="clearfix"></div>
                                <hr style="margin:10px">
                                <div class="a">
                                  <div class="col-md-8"><strong>GRAND TOTAL</strong></div>
                                  <div class="col-md-4"><strong>&pound;<?php echo ($grand_total) ? filter_price($grand_total) : '' ; ?></strong></div>
                                </div>
                                <div class="clearfix"></div>
                              </div>
                            </div>
                          </div>
                        </div>
                    </div>
                    <input type="hidden" name="payment_method_radio" id="payment_method_radio" value="credit_card" readonly="readonly" />
                    <input type="hidden"  id="validate_id" value="0" readonly="readonly" />
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script src="https://getaddress.io/js/jquery.getAddress-2.0.5.min.js"></script> 

<!-- Add after your form --> 

<?php  
   // Get POSTCODE API KEY
   $POSTCODE_KEY = 'POSTCODE_KEY'; 
   $key = get_global_settings($POSTCODE_KEY);
   $api_postcode_key = $key['setting_value']; 
?>

<script>
(function($){
	jQuery('#postcode_lookup').getAddress({
	// api_key: 'BmiwdQp9W0q1l-EucVP_9A6735',  
	api_key: '<?php echo $api_postcode_key;?>',
	
	<!--  Or use your own endpoint - api_endpoint:https://your-web-site.com/getAddress, -->
	output_fields:{
		line_1: '#address',
		line_2: '#address_2',
		line_3: '#line3',
		post_town: '#town',
		county: '#county',
		postcode: '#postcode'
	},
<!--  Optionally register callbacks at specific stages -->                                                                                                               
    onLookupSuccess: function(data){/* Your custom code */},
    onLookupError: function(){/* Your custom code */},
    onAddressSelected: function(elem,index){
				
		// Always true 4 fields
		jQuery('.fv-hidden-submit').removeClass('disabled');
		jQuery('.fv-hidden-submit').attr('disabled', false);
	
		if(jQuery('.help-block')){
	
			var new_html = '';
			var data_fv_for = '';
				
			jQuery.each( jQuery('.help-block'), function(k, error_el){
	
				data_fv_for = jQuery(error_el).attr('data-fv-for');
					  
				if(data_fv_for == 'address' || data_fv_for == 'town' || data_fv_for == 'county' || data_fv_for == 'postcode'){
				  jQuery('#'+data_fv_for).trigger('input');
				} // if(data_fv_for == 'address' || data_fv_for == 'town' || data_fv_for == 'county' || data_fv_for == 'postcode')
		
			}); // jQuery.each( jQuery('.help-block'), function(k, error_el)
	
		} // if($('.help-block'))	
	}

});
})(jQuery);

  
</script>