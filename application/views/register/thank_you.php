<div class="site-content pt-80"> 
  <!-- Start Fullwide Map -->
  <div class="fullwide-map"> 
    
    <!-- End Fullwide Map -->
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="content-area pt-100">
            <div class="row">
              <div class="col-md-12">
                <h3>Registration Completed</h3><br />
               	 <b>Thank you</b> for choosing us, please check your email for your invoice. You can <a href="<?php echo SURL?>login">Login Now</a> to start your survey.
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>