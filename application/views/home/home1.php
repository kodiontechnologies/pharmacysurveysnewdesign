<section class="static_banner"> <img src="http://demotd.com/surveysnew/assets/img/banner-2.png" alt="">
  <div class="banner_text">
    <div class="container">
      <h2><span>Complete solution for</span> <br>
        Community Pharmacy <br>
        Patient Questionnaire (CPPQ)</h2>
      <div class="col-md-7 col-xs-12">
        <ul>
          <li><i class="fa fa-check-square-o"></i>Multi-platform survey (SMS, online and paper based surveys)</li>
          <li><i class="fa fa-check-square-o"></i>100 free SMS surveys</li>
          <li><i class="fa fa-check-square-o"></i>Unlimited surveys for one fixed price</li>
          <li><i class="fa fa-check-square-o"></i>Comply with your NHS contractual requirements</li>
          <li><i class="fa fa-check-square-o"></i>Real-time results and analytics with graphical representation</li>
          <li><i class="fa fa-check-square-o"></i>Survey report, results poster & NHS local team letter </li>
        </ul>
        <a href="#" class="buy_now"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Buy Now</a> <a href="#" class="learn_more"><i class="fa fa-info-circle" aria-hidden="true"></i> Learn more</a> </div>
      <div class="col-md-5 col-xs-12 one_price">
        <div class="one_price_outer">
          <h5><span class="price_yellow">One Price</span> <br>
            £39.99 <br>
            <span class="vat">+VAT</span></h5>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="page_main_text">
  <div class="container">
    <h2><span class="quotes"><i class="fa fa-quote-left" aria-hidden="true"></i></span><span>Survey focus</span> is designed to help you achieve your survey targets in short time. If you already have some returned questionnaire or starting now, we can help.<span class="quotes_right"><i class="fa fa-quote-right" aria-hidden="true"></i></span></h2>
  </div>
</section>
<section class="big_bg_section">
  <div class="big_bg_section_layer">
    <div class="container">
      <div class="section_big_inner">
        <p class="main_block_text">The SMS survey is a great feature as patients are familiar with receiving SMS messages from the pharmacy and it has a considerably high response rate.</p>
        <div class="section_cols_left">
          <div class="col-md-8 col-sm-6 col-xs-12"> <img src="http://demotd.com/surveysnew/assets/images/survey_1.png" alt=""> </div>
          <div class="col-md-4 col-sm-6 col-xs-12">
            <h3>Survey requirements</h3>
            <p>You would need to select the number of returned surveys required for your pharmacy based on average monthly script volume (items). Select an option most appropriate for your pharmacy. </p>
            <a href="#" class="btn btn-default">Buy now</a> </div>
        </div>
        <div class="section_cols_left">
          <div class="col-md-4 col-sm-6 col-xs-12">
            <h3>Survey Progress</h3>
            <p>By clicking on the current survey tab, you can view the progress of your survey. The break down of online and paper based survey, number of surveys completed and the remaining number of surveys. </p>
            <a href="#" class="btn btn-default">Buy now</a> </div>
          <div class="col-md-8 col-sm-6 col-xs-12"> <img src="http://demotd.com/surveysnew/assets/images/survey_2.png" alt=""> </div>
        </div>
        
        <!--col3-->
        <div class="section_cols_left">
          <div class="col-md-8 col-sm-6 col-xs-12"> <img src="http://demotd.com/surveysnew/assets/images/survey_3.png" alt=""> </div>
          <div class="col-md-4 col-sm-6 col-xs-12">
            <h3>Multi-platform Survey forms </h3>
            <p>You can send a survey form online through email, SMS and also you can transfer the responses from a returned paper survey form. Your patient can choose the most convenient method of completing the survey. </p>
            <a href="#" class="btn btn-default">Buy now</a> </div>
        </div>
        <!--col3-->
        
        <div class="section_cols_left">
          <div class="col-md-4 col-sm-6 col-xs-12">
            <h3>Closing the survey</h3>
            <p>Once you have the minimum number of returned questionnaires required for your pharmacy, you may close the survey by going to the results tablet. This will open the review and analysis section of the system.</p>
            <a href="#" class="btn btn-default">Buy now</a> </div>
          <div class="col-md-8 col-sm-6 col-xs-12"> <img src="http://demotd.com/surveysnew/assets/images/survey_4.png" alt=""> </div>
        </div>
        
        <!--col5-->
        <div class="section_cols_left">
          <div class="col-md-8 col-sm-6 col-xs-12"> <img src="http://demotd.com/surveysnew/assets/images/survey_5.png" alt=""> </div>
          <div class="col-md-4 col-sm-6 col-xs-12">
            <h3>Finalising results </h3>
            <p>After closing the survey, you will be asked to review results for all questions and comment on each question. Also you will be required to select three areas for improvement and three strong performing areas and comment. This will appear in the final report.</p>
            <a href="#" class="btn btn-default">Buy now</a> </div>
        </div>
        <!--col5-->
        
        <div class="section_cols_left">
          <div class="col-md-4 col-sm-6 col-xs-12">
            <h3>Survey Archives</h3>
            <p>In the archives tab, you can view all the previous survey documents that you have completed through the SurveyFocus system. You can view, download and print survey documents from here. </p>
            <a href="#" class="btn btn-default">Buy now</a> </div>
          <div class="col-md-8 col-sm-6 col-xs-12"> <img src="http://demotd.com/surveysnew/assets/images/survey_6.png" alt=""> </div>
        </div>
        <div class="mobile_represnt_outer">
          <div class="mobilr_section_top">
            <h3>Why Pharmacy Survey!</h3>
            <p>Pharmacy Survey is a product of Digital Health Group Ltd, an IT company that specialises in solutions for the pharmacy sector. Below are just some of the reasons why Pharmacy Survey is the product of choice for your CPPQ needs.</p>
          </div>
          <div class="section_cols_left">
            <div class="col-md-8 col-sm-6 col-xs-12"> <img src="http://demotd.com/surveysnew/assets/images/show_responsive.png" alt=""> </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
              <h3>Access it from Anywhere!</h3>
              <p>Survey focus is absolutely accessible everywhere you go on your mobile phones, tablets, iPhone and iPads. This will give ease to all of your users to access and monitor their surveys on their devices.</p>
              <a href="#" class="btn btn-default">Buy now</a> </div>
          </div>
          <div class="mobilr_section_top">
            <h3>Act now to get your 2016/17 survey done and plan ahead for 2017/18</h3>
            <p>Surveyfocus is the most flexible and versatile survey system available on the market. Buy with confidence as we will not be beaten on price or quality. </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
