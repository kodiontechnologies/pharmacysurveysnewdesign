<style>
table td {
	border: none !important;
	background:none;
}
</style>
<div class="color-line"></div>
<div class="modal-header text-left">
  <h4 class="modal-title">Invoice Details</h4>
</div>
<div class="modal-body">
  <div class="row" style="margin:0">
    <div class="c12">
      <div class="c12 text-right"> <a style="font-size:16" href="<?php echo SURL?>dashboard/download-invoice/<?php echo filter_string($invoice_data['id']);?>"><i class="fa fa-print"></i> Print Invoice</a> </div>
		
        <div class="row">
        	<div class="col-md-4">
            	<div style="background-color:#84C060; padding: 10px 5px; color:#fff; font-size:14px;">INVOICE</div>
            </div>
        </div>

        <div class="row">
        	<div class="col-md-2">
            	<div style="padding: 0px 5px;">Invoice No.</div>
            </div>
        	<div class="col-md-2 text-right">
            	<div style="padding: 0px 5px;"><?php echo filter_string($invoice_data['invoice_no']);?></div>
            </div>
        </div>
        <div class="row">
        	<div class="col-md-2">
            	<div style="padding: 0px 5px;">Date</div>
            </div>
        	<div class="col-md-2 text-right">
            	<div style="padding: 0px 5px;"><?php echo filter_uk_date($invoice_data['created_date']);?></div>
            </div>
        </div>

        <div class="row">
        	<div class="col-md-12 text-right">
            	<img src="http://surveyfocus.co.uk/assets/img/logo.png" alt="">
            </div>
       </div>
       
       <div class="row">
        	<div class="col-md-4">
            	<strong>Bill To</strong> <br />
                <?php echo filter_string($invoice_data['pharmacy_name']);?> <br />
                <?php echo filter_string($invoice_data['pharmacy_address']).', ';?> 
                <?php echo (filter_string($invoice_data['pharmacy_address_2'])) ? filter_string($invoice_data['pharmacy_address_2']).', ' : '';?> 
                <?php echo filter_string($invoice_data['pharmacy_postcode']);?><br /> 
                <?php echo filter_string($invoice_data['pharmacy_county']);?>
            </div>
            
            <div class="col-md-4"></div>
            
            <div class="col-md-4">
            	<?php echo filter_string($invoice_data['from_name']);?> <br />
                <?php echo filter_string($invoice_data['from_address']).', ';?>  
				<?php echo (filter_string($invoice_data['from_address_2'])) ? filter_string($invoice_data['from_address_2']).' ' : '';?> <br />
                <?php echo filter_string($invoice_data['from_town']).', ';?> <?php echo filter_string($invoice_data['from_postcode']);?> <br />
				<?php echo filter_string($invoice_data['from_county']);?>
                
            </div>
       </div>
       
       <div class="row">
            <div class="col-md-12 m-t">
            	<table width="100%" cellpadding="11" cellspacing="2">
                  <tr>
                    <td width="70%" bgcolor="#84C060" style="background-color:#84C060; color:#fff; font-size:14px">Item Description</td>
                    <td width="15%" bgcolor="#84C060" style="background-color:#84C060; color:#fff; font-size:14px">Qty</td>
                    <td width="15%" bgcolor="#84C060" style="background-color:#84C060; color:#fff; font-size:14px">Price</td>
                    
                  </tr>
                  <tr style="font-size:14px; background-color:#FAFAFA;">
                    <td>Payment for <?php echo filter_string($invoice_data['survey_session']);?></td>
                    <td>1</td>
                    <td>&pound;<?php echo filter_string($invoice_data['subtotal'])?></td>
                  </tr>
                 
			  </table>
            </div>
        </div>
        
        <div class="row m-t">
            <div class="col-md-8"> &nbsp;</div>
            <div class="col-md-4">
            <table width="100%" cellpadding="10" style="font-size:14px;">
              <tr>
                <td width="15%" style="background-color:#F2F2F2;">Subtotal</td>
                <td width="15%" style="background-color:#F2F2F2;"><strong>&pound;<?php echo filter_string($invoice_data['subtotal'])?></strong></td>
              </tr>
              <tr>
                <td width="15%" style="background-color:#FAFAFA;">VAT (<?php echo number_format($invoice_data['vat_percentage'],1)?>%)</td>
                <td width="15%" style="background-color:#FAFAFA;"><strong>&pound;<?php echo filter_string($invoice_data['vat'])?></strong></td>
              </tr>
              <tr>
                <td width="15%" style="background-color:#F2F2F2;">Total</td>
                <td width="15%" style="background-color:#F2F2F2;"><strong>&pound;<?php echo filter_string($invoice_data['grand_total'])?></strong></td>
              </tr>
             </table>
            </div>
        </div>
    </div>
  </div>
</div>
