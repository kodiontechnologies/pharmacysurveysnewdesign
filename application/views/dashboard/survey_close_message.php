<ul class="list-unstyled" style="padding-left:10px;">
	<?php 
        if(count($missing_reviewed) > 0){
    ?>
        <li class="text-danger">- In order to proceed, you need to review the question(s) below:
            <ul class="list-unstyled">
                <li>&nbsp; <?php echo '<strong>Question No: </strong>'.implode(', ',$missing_reviewed);?></li>
            </ul>
        </li>
    
    <?php		
        }//end if(count($missing_comment) > 0)
        
        if($well_performed == 0 || $bad_performed == 0){
    ?>
            <li class="text-danger">- Complete selection of <strong>6 questions</strong> from the left menu dropdown and comment on each question.</li>
      <?php		
        }//end  if($well_performed == 0 || $bas_performed == 0)
    ?>
</ul>