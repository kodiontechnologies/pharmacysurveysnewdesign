<div class="container-fluid" style="<?php echo ($is_mobile) ? 'width:100%' : 'width:80%'; ?>">
<style>
.panel-primary > .panel-heading, .panel-primary > .panel-body  {
	font-size:16px;	
}
.panel-primary > .panel-body textarea {
	color:#474747;	
	font-size:16px;
}
</style>
<div class="row no-margins">
	<div class="col-lg-12">
    	<p class=" text-right"><small><strong>Powered by </strong><a title="CPPQ" href="<?php echo SURL?>">surveyfocus.co.uk</a>  </small></p>
    </div>
  <div class="col-lg-12">
    <div class="panel3 panel-primary">
      <div class="panel-heading"> <p class="text-center" style="font-size:26px; margin:15px; line-height:40px">Community Pharmacy Satisfaction Surveys <br /> 
	  <?php echo ltrim($pharmacy_current_survey['survey_title'], 'Survey ')?></p></div>
      <div class="panel-body">
      <strong><?php echo filter_string($phramacy_details['pharmacy_name']);?></strong><br>
      <?php 
		$address_2 = ($phramacy_details['address_2']) ? $phramacy_details['address_2'].', ': '';
		$phramcy_address = $phramacy_details['address'].', '.$address_2.$phramacy_details['town'].', '.$phramacy_details['county'].', '.$phramacy_details['postcode'];
		echo $phramcy_address;
	  ?>
      </div>
    </div>
  </div>
</div>


<!-- End row -->
        <form <?php if($pharmacy_current_survey['id'] && $pharmacy_current_survey['survey_status'] == '0' && ($verify_invitation || $embed_code)){?> action="<?php echo SURL?>online-survey/submit-online-survey" id="manage_survey_frm" name="manage_survey_frm" method="POST" enctype="multipart/form-data" class="prevent_enter_key" <?php }?>>
        
            <div class="col-md-12 m-t">
              
                    <div class="module2">
                      <div class="stripe-5 m-b-sm">
                        <h4 class="no-margins">This section is about why you visited the pharmacy today</h4>
                      </div>
                    </div>                    
                <?php 
                    foreach($questionnnair_arr as $question_id => $question_list){
                        
                        if($question_id == 4){
                ?>
        
                    <div class="module2">
                      <div class="stripe-5 m-b-sm">
                        <h4 class="no-margins">This section is about the pharmacy and the staff who work there more generally, not just for today's visit</h4>
                      </div>
                    </div>                    
        
        
                <?php			
                        }elseif($question_id == 11){
                ?>
        
                            <div class="module2">
                              <div class="stripe-5 m-b-sm">
                                <h4 class="no-margins">These last few questions are just to help us categorise your answers</h4>
                              </div>
                            </div>   
        
                <?php			
                        }//end if($question_id == 4)
                ?>
                        <div class="row">
                            <div class="col-lg-12">
                              <div class="panel3 panel-primary">
                                <div class="panel-heading"> <span class="badge"><strong><?php echo filter_string($question_list['question_code'])?></strong></span> <?php echo filter_string($question_list['question']);?> <i><?php echo filter_string($question_list['sub_notes']);?></i> </div>
                                <div class="panel-body">
                                <?php 
                                    if($question_id != 10 && $question_id != 45 ){
                                        
                                        if(count($question_list['sub_question']) == 0){
                                            
                                            $total_options = count($question_list['options']);
                                            $per_cell = floor(12/$total_options);
                        
                                            foreach($question_list['options'] as $option_id => $option_list){
                                                
                                                $per_cell = ($total_options == 2) ? '3' : $per_cell;
                                ?>
                                                <div class="col-lg-<?php echo $per_cell;?>" <?php echo ($question_id == 11 || $question_id == 49) ? 'style="width: 120px;"' : '' ?>>
                                                    <div class="radio radio-success radio-circle">
                                                        <input class="<?php echo ($question_id == 1 || $question_id == 36) ? 'show_hide_textbox' : ''?>" type="radio" required="required" value="<?php echo filter_string($option_id)?>" id="opt_<?php echo $question_id.'_'.$option_id?>" name="q[<?php echo $question_id?>]"  >
                                                        <label for="opt_<?php echo $question_id.'_'.$option_id?>"><?php echo filter_string($option_list);?></label>
                                                    </div>
                                                </div>
                                <?php				
                                            }//end foreach($question_list['options'] as $option_id => $option_list)
                                ?>
                                            <div class="row hidden" id="err_q<?php echo filter_string($question_id)?>"><div class="col-md-12 alert alert-sm alert-danger">Please select one of the options</div></div>
                                <?php			
                                            if($question_id == 1 || $question_id == 36){
                                ?>
                                                <div class="col-lg-12 hidden no-paddings" id="question_10_txtbox">
                                                    <textarea rows="5" style="width:100%; background-color:#fff" id="other_reason_txt" name="other_reason_txt" placeholder="For some other reason (please write in the reason for your visit):" maxlength="250"></textarea>
                                                    <p><i>Max 250 characters allowed </i></p>
                                                </div>
                                <?php				
                                            }//end if($question_id == 1 || $question_id == 36)
                                        }else{
                                            
                                             foreach($question_list['sub_question'] as $subquestion_id =>$subquestion_list){
                                ?>
                                                <div class="col-lg-12"><p class="subtitles">
                                                	<strong><?php echo substr(filter_string($subquestion_list['question_code']),1,2);?>) <?php echo filter_string($subquestion_list['question']);?></strong></p></div>
                                <?php				 
                                                $total_options = count($subquestion_list['options']);
                                                $per_cell = floor(12/$total_options);
                                        
                                                foreach($subquestion_list['options'] as $suboption_id => $suboption_list){
                                                    
                                                    $per_cell = ($total_options == 2) ? '1' : $per_cell;
                                ?>
                                                    <div class="col-lg-<?php echo $per_cell?>">
                                                        <div class="radio radio-success radio-circle">
                                                            <input required="required" type="radio" value="<?php echo filter_string($suboption_id)?>" id="<?php echo $subquestion_id.'_'.$suboption_id?>" name="q[<?php echo filter_string($subquestion_id)?>]">
                                                            <label for="<?php echo $subquestion_id.'_'.$suboption_id?>">
                                                               <?php echo $suboption_list?>
                                                            </label>
                                                        </div>                                        
                                                    </div>
                                <?php					
                                                }//end foreach($subquestion_list['options'] as $suboption_id => $suboption_list)
                                ?>
                                                <div class="row hidden" id="err_q<?php echo filter_string($subquestion_id)?>"><div class="col-md-12 alert alert-sm alert-danger">Please select one of the options</div></div>
                                <?php				
                                             }//end foreach($question_list['sub_question'] as $subquestion_id =>$subquestion_list)
                                            
                                        }//end if(count($question_list['sub_question']) == 0)
                                        
                                    }else{
                                ?>
                                        <div class="col-lg-12 no-paddings">
                                            <textarea rows="5" style="width:100%; background-color:#fff" id="comment" name="q[<?php echo $question_id?>]" placeholder="[Insert here, if required, additional questions relating to healthcare service provision]" maxlength="250" ></textarea>
                                            <p><i>Max 250 characters allowed </i></p>	                            
                                        </div>                    
                                <?php		
                                    }//end if($question_id !=10)
                                ?>
                                </div>
                              </div>
                            </div>
                          </div>
                <?php		
                    }//end foreach($questionnnair_arr as $question_id => $question_list)
                ?>		
                
            </div>
            
            <?php 
			
				if($pharmacy_current_survey['id'] && $pharmacy_current_survey['survey_status'] == '0' && ($verify_invitation || $embed_code)){
				//Show submi t only if there is any active survey in progress
            ?>
                    <div class="row no-margins">
                        <div class="col-md-10 text-center">
                            <p class="hidden alert alert-danger" id="common_err"><strong>One of the option above is not yet selected. Please select the options from the highlighed sections above.</strong></p>
                        </div>
                        <div class="col-md-2 text-right no-margins">
                            <button type="button" name="submit_suv_btn" id="submit_suv_btn" value="submit_suv_btn" class="btn btn-success">Submit Survey</button>
                            <input type="hidden" name="pharmacy_id" value="<?php echo filter_string($phramacy_details['id']); ?>" readonly="readonly" />
                            <input type="hidden" name="submit_type" value="ONLINE" readonly="readonly" />
                            <input type="hidden" name="embed_code" value="<?php echo ($embed_code) ? $embed_code : 0?>" readonly="readonly" />
                            <input type="hidden" name="survey_id" id="survey_id" value="<?php echo filter_string($pharmacy_current_survey['id']) ?>" readonly="readonly" />	
                            <input type="hidden" name="inv_id" id="inv_id" value="<?php echo filter_string($verify_invitation['id']) ?>" readonly="readonly" />	
                        </div>
                    </div>
                    <div class="row no-margins">
                        <div class="col-md-12">
                            <div style="border: dashed 1px #ccc" class="m-t p-xs">
                            
                                <small style="color:#999" class="text-justify">
                                <p>
                                    <?php 
                                        $SURVEY_FOOTER_TEXT = 'SURVEY_FOOTER_TEXT';
                                        $survey_footer_text = get_global_settings($SURVEY_FOOTER_TEXT); //Set from the Global Settings
                                        $survey_footer_text = nl2br(filter_string($survey_footer_text['setting_value']));
                                        echo str_replace('[PHARMACY_NAME]',filter_string($phramacy_details['pharmacy_name']),$survey_footer_text);
                                    ?>
                                </p>
                                </small>
                            </div>
                        </div>
                    </div>
        
            <?php		
                }else{
			?>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="alert alert-danger">You cannot submit the survey, either the survey is already submitted, request you have received is invalid or the survey has ended.</div>
                    </div>
                </div>            
            <?php		
				}//end if(!$pharmacy_current_survey['id'])
            ?>
        </form>
</div>
<script>
// Prevent form from enter key
jQuery('.prevent_enter_key').on('keyup keypress', function(e){
	
	var keyCode = e.keyCode || e.which;
	if (keyCode === 13){
		e.preventDefault();
		return false;
	} // if (keyCode === 13)

}); // $('.prevent_enter_key').on('keyup keypress', function(e)

</script>