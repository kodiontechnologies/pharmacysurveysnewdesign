<script>
	
	/*Age Charts */
	
	google.charts.load("current", {packages:["corechart"]});
	google.charts.setOnLoadCallback(drawChart_age_pie);

	function drawChart_age_pie(){
	  
		var data = google.visualization.arrayToDataTable([
		  ['Option', 'Percentage', { role: 'style' }],
		  <?php 
			$j=0;
			foreach($survey_age_question_stats['options_attempts'] as $option_title => $option_value){
				
				$total_survey_attempt = ($survey_age_question_stats['total_survey_attempt'] == 0) ? 1 : $survey_age_question_stats['total_survey_attempt'];
				$eval_percentage = number_format( ($option_value / $total_survey_attempt) * 100, 2);
		?>
				["<?php echo substr(filter_string($option_title),0,25).' ('.number_format($eval_percentage,1).'%)'; ?>", <?php echo filter_percentage($eval_percentage)?>, '<?php echo $bar_color_arr[$j] ?>'],
		<?php	
				$j++;	
			}//end foreach($survey_age_question_stats['options_attempts'] as $option_title => $option_value)
		  ?>
		]);
		
		var options = {
		  width:'1000',
		  height: '800',
		  is3D: true,
		  pieSliceText: 'none',
		  chartArea:{top:20},
		  vAxis: {minValue: 0},
		  colors: ['#109618','#FF9900', '#D9534F', '#D2D1CF', '#00BBA7', '#990099','#FFD700'],
		 
		};
		
		var chart = new google.visualization.PieChart(document.getElementById('age_pie_chart'));
		chart.draw(data, options);
		
		chart_svg = jQuery('#age_pie_chart').find('svg').html();
		chart_svg = '<svg viewBox="10 0 750 500" width="100%" height="75%" style="overflow: hidden;" aria-label="A chart.">'+chart_svg+'</svg>';
		jQuery('.svg_age_pie_chart').val(chart_svg);
	
	}//end drawChart_pie9
	
	/*Gender Charts */

	google.charts.load("current", {packages:["corechart"]});
	google.charts.setOnLoadCallback(drawChart_gender_pie);

	function drawChart_gender_pie(){
	  
		var data = google.visualization.arrayToDataTable([
		  ['Option', 'Percentage', { role: 'style' }],
		  <?php 
			$j=0;
			foreach($survey_gender_question_stats['options_attempts'] as $option_title => $option_value){
				
				$total_survey_attempt = ($survey_gender_question_stats['total_survey_attempt'] == 0) ? 1 : $survey_gender_question_stats['total_survey_attempt'];
				$eval_percentage = number_format( ($option_value / $total_survey_attempt) * 100, 2);
		?>
				["<?php echo substr(filter_string($option_title),0,25).' ('.number_format($eval_percentage,1).'%)'; ?>", <?php echo filter_percentage($eval_percentage)?>, '<?php echo $bar_color_arr[$j] ?>'],
		<?php	
				$j++;	
			}//end foreach($survey_gender_question_stats['options_attempts'] as $option_title => $option_value)
		  ?>
		]);
		
		var options = {
		  width:'1000',
		  height: '800',
		  is3D: true,
		  pieSliceText: 'none',
		  chartArea:{top:20},
		  vAxis: {minValue: 0},
		  colors: ['#109618','#FF9900'],
		 
		};
		
		var chart = new google.visualization.PieChart(document.getElementById('gender_pie_chart'));
		chart.draw(data, options);
		
		chart_svg = jQuery('#gender_pie_chart').find('svg').html();
		chart_svg = '<svg viewBox="10 0 750 500" width="100%" height="75%" style="overflow: hidden;" aria-label="A chart.">'+chart_svg+'</svg>';
		jQuery('.svg_gender_pie_chart').val(chart_svg);
	
	}//end drawChart_gender_pie

</script>
