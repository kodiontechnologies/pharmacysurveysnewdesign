<?php 
//print_this($survey_question_stats);
foreach($survey_question_stats['options_attempts'] as $options => $stats){

	if($options == 'Excellent')
		$eval_percentage_exc = $stats;
	//end if($options == 'Excellent')
	
	if($options == 'Very Good')
		$eval_percentage_vgood = $stats;
	//end if($options == 'Very Good')

	if($options == 'Good')
		$eval_percentage_good = $stats;
	//end if($options == 'Good')

}//end foreach($survey_question_stats['options_attempts'] as $options => $stats)

$avg_exc_good = (($eval_percentage_exc + $eval_percentage_vgood)/$survey_question_stats['total_survey_attempt']) * 100;
$avg_exc_good_txt = 'very good or excellent';

if($avg_exc_good < 80){
	$avg_exc_good = (($eval_percentage_exc + $eval_percentage_vgood + $eval_percentage_good)/$survey_question_stats['total_survey_attempt'])* 100;		
	$avg_exc_good_txt = 'good, very good or excellent';
}//end if($avg_exc_good < 80)
?>
<style>
	.poster_heading {
		width:100%; 
		text-align:center; 
		background-color:#007BC3; 
		color:#fff; 
		font-size:45px; 
		padding:20px 20px; 
		height:50px; 
		font-weight:normal; 
		font-family:Arial, Helvetica, sans-serif;	
		float:left;
	}
	
	.poster_footer{
		width:100%;
		background-color:#007BC3; 
		color:#fff; 
		font-size:20px; 
		padding:40px 20px;
		height:60px; 
		font-weight:normal; 
		font-family:Arial, Helvetica, sans-serif;	

	}
</style>
<center>
<div class="poster_heading"  align="center">Community Pharmacy Patient Questionnaire <?php echo ltrim(filter_string($pharmacy_survey_details['survey_title']),'Survey ')?> <br /><?php echo $this->session->pharmacy_name ?></div>

<div style="width:100%; text-align:center; color:#007BC3; font-size:40px; font-weight:normal; padding:15px 15px; font-family:Arial, Helvetica, sans-serif; float:left;" align="center">
    <span style="font-size:60px"><?php echo number_format($avg_exc_good,0)?>%</span> 
    of our customers rate our service as <?php echo $avg_exc_good_txt?>.
</div>
<div style="width:100%; text-align:center; border: solid 0px #0C6" align="center">
<?php 
	$svg_image =  filter_string($survey_question_stats['closing_comments_arr']['svg_poster_code']);
	$svg_image = str_replace('<rect x="0" y="0" width="1000" height="800" stroke="none" stroke-width="0" fill="#ffffff"></rect>','',$svg_image);
	$svg_image = str_replace('<rect x="0" y="0" width="1138" height="900" stroke="none" stroke-width="0" fill="#ffffff"></rect>','',$svg_image);
	echo $svg_image = str_replace('<rect x="678" y="20" width="243" height="16" stroke="none" stroke-width="0" fill-opacity="0" fill="#ffffff"></rect>','',$svg_image);
?>
</div>

</center>