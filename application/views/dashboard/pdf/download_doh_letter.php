<style>
	p{
		font-family:Arial, Helvetica, sans-serif; 
		font-size:12px;
	}
	.stats_table{
		font-family:Arial, Helvetica, sans-serif; 
		font-size:12px;
	}
	.stats_cell{
		padding:5px 0;
	}
	
</style>

<?php 

	$sequence_arr = array('First area', 'Second area', 'Third area');
	
	$well_performed_str = '<table width="100%">';
	//print_this($well_performed_arr); exit;
	for($i=0;$i<count($well_performed_arr);$i++){
		
		$survey_question_stats = get_survey_question_stats($this->session->p_id, $survey_id, $pharmacy_survey_details['survey_ref_no'], $well_performed_arr[$i]['question_id'],$this->session->pharmacy_type);

		$question_txt = ($survey_question_stats['parent_question']) ? filter_string($survey_question_stats['parent_question']).' <br><b>('.filter_string($survey_question_stats['question']) .') </b>' : filter_string($survey_question_stats['question']);
		
		$well_performed_str .= "<tr><td><h4>".$sequence_arr[$i]." in which the pharmacy performed well:</h4></td></tr>";
		
		if($well_performed_arr[$i]['question_id'] != 10){
			
			$well_performed_str .= '<tr><td><p>'.$question_txt.'</p><br><p><table cellpadding="0" cellspacing="0" width="100%" class="stats_table"  align="center" style="border: solid 1px #CCC"><tr>';
				
			foreach($survey_question_stats['options_attempts'] as $options => $stats){
				
				$eval_percentage = number_format( ($stats / $survey_question_stats['total_survey_attempt']) * 100, 2);
				
				$well_performed_str .= '<td class="stats_cell" align="center" style="border-right: solid 1px #ccc;">
										'.$options.' <br> <strong style="font-size:14px;">'.filter_percentage($eval_percentage).'%</strong>
										</td>';
			}//end foreach($survey_question_stats['options_attempts'] as $options => $stats)
			
			$well_performed_str .= '</tr></table></p></td></tr>';
			$well_performed_str .= '<tr><td><p><strong>Comments:</strong> '.filter_string($well_performed_arr[$i]['well_performed_comments']).'</p></td></tr>';
			
		}else{
			
			$well_performed_str .= '<tr><td><p>'.filter_string($survey_question_stats['question']).'</p><br><p><table cellpadding="0" cellspacing="0" width="100%" class="stats_table"  align="center" style="border: solid 1px #CCC"><tr>';


			$eval_commented_percentage = number_format( ($survey_question_stats['total_commented'] / $survey_question_stats['total_survey_attempt']) * 100, 2);
			$eval_non_commented_percentage = number_format( ($survey_question_stats['total_non_commented'] / $survey_question_stats['total_survey_attempt']) * 100, 2);									
					
			
			$well_performed_str .= '<td class="stats_cell" align="center" style="border-right: solid 1px #ccc;">
										Total Commented  <br> <strong style="font-size:14px;">'.$eval_commented_percentage.'%</strong>
										</td><td class="stats_cell" align="center" style="border-right: solid 1px #ccc;">
										Total Non Commented <br> <strong style="font-size:14px;">'.$eval_non_commented_percentage.'%</strong>
										</td>';
										
			$well_performed_str .= '</tr></table></p></td></tr>';
			$well_performed_str .= '<tr><td><p><strong>Comments:</strong> '.filter_string($well_performed_arr[$i]['well_performed_comments']).'</p></td></tr>';
			
		}//end if($well_performed_arr[$i]['question_id'] != 10)
		
	}//end for($i=0;$i<count($well_performed_arr);$i++)
	
	$well_performed_str .= '</table>';
	
	$bad_performed_str = '<table width="100%">';
	for($i=0;$i<count($bad_performed_arr);$i++){
		
		$survey_question_stats = get_survey_question_stats($this->session->p_id, $survey_id, $pharmacy_survey_details['survey_ref_no'], $bad_performed_arr[$i]['question_id'], $this->session->pharmacy_type);
		
		//print_this($survey_question_stats);
		
		$question_txt = ($survey_question_stats['parent_question']) ? filter_string($survey_question_stats['parent_question']).' <br><b>('.filter_string($survey_question_stats['question']) .') </b>' : filter_string($survey_question_stats['question']);
		
		$bad_performed_str .= "<tr><td><strong>".$sequence_arr[$i]." in which the pharmacy needs improvement:</strong></td></tr>";
		
		if($bad_performed_arr[$i]['question_id'] != 10){
			
			$bad_performed_str .= '<tr><td><p>'.$question_txt.'</p><br><p><table cellpadding="0" cellspacing="0" width="100%" class="stats_table"  align="center" style="border: solid 1px #CCC"><tr>';
				
			foreach($survey_question_stats['options_attempts'] as $options => $stats){
				
				$eval_percentage = number_format( ($stats / $survey_question_stats['total_survey_attempt']) * 100, 2);
				
				$bad_performed_str .= '<td class="stats_cell" align="center" style="border-right: solid 1px #ccc;">
										'.$options.' <br> <strong style="font-size:14px;">'.filter_percentage($eval_percentage).'%</strong>
										</td>';
			}//end foreach($survey_question_stats['options_attempts'] as $options => $stats)
			
			$bad_performed_str .= '</tr></table></p></td></tr>';
			$bad_performed_str .= '<tr><td><p><strong>Comments:</strong> '.filter_string($bad_performed_arr[$i]['bad_performed_comments']).'</p></td></tr>';
			
		}else{
			
			$bad_performed_str = '<tr><td><p>'.filter_string($survey_question_stats['question']).'</p><br><p><table cellpadding="0" cellspacing="0" width="100%" class="stats_table"  align="center" style="border: solid 1px #CCC"><tr>';

			$eval_commented_percentage = number_format( ($survey_question_stats['total_commented'] / $survey_question_stats['total_survey_attempt']) * 100, 2);
			$eval_non_commented_percentage = number_format( ($survey_question_stats['total_non_commented'] / $survey_question_stats['total_survey_attempt']) * 100, 2);									
					
			
			$bad_performed_str .= '<td class="stats_cell" align="center" style="border-right: solid 1px #ccc;">
										Total Commented  <br> <strong style="font-size:14px;">'.$eval_commented_percentage.'%</strong>
										</td><td class="stats_cell" align="center" style="border-right: solid 1px #ccc;">
										Total Non Commented <br> <strong style="font-size:14px;">'.$eval_non_commented_percentage.'%</strong>
										</td>';
										
			$bad_performed_str .= '</tr></table></p></td></tr>';
			$bad_performed_str .= '<tr><td><p><strong>Comments:</strong> '.filter_string($bad_performed_arr[$i]['bad_performed_comments']).'</p></td></tr>';
			
		}//end if($bad_performed_str[$i]['question_id'] != 10)
		
	}//end for($i=0;$i<count($bad_performed_str);$i++)
	$bad_performed_str .= '</table>';
	

	$find_arr = array(
						'[PHARMACY_NAME]',
						'[PHARMACY_ADDRESS]',
						'[PHARMACY_TOWN]',
						'[PHARMACY_POSTCODE]',
						'[DATED]',
						'[SURVEY_YEAR]',
						'[TOTAL_NO_SURVEY_CONDUCTED]',
						'[SURVEY_PERFORMING_WELL]',
						'[SURVEY_PERFORMING_BAD]',
						'[SURVEY_VOLUME]'
					);

	$replace_arr = array(
						$this->session->pharmacy_name,
						$this->session->address,
						$this->session->postcode,
						ucfirst($this->session->town),
						date('F j, Y'),
						ltrim($pharmacy_survey_details['survey_title'],'Survey '),
						$total_submitted_surveys,
						$well_performed_str,
						$bad_performed_str,
						$pharmacy_survey_details['survey_volume']
					);

	echo $doh_letter = str_replace($find_arr,$replace_arr,filter_string($doh_letter_body['email_body']));
?>