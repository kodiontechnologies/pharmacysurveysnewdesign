<?php 
foreach($survey_question_stats['options_attempts'] as $options => $stats){

	if($options == 'Excellent')
		$eval_percentage_exc = $stats;
	//end if($options == 'Excellent')
	
	if($options == 'Very Good')
		$eval_percentage_vgood = $stats;
	//end if($options == 'Very Good')

	if($options == 'Good')
		$eval_percentage_good = $stats;
	//end if($options == 'Good')

}//end foreach($survey_question_stats['options_attempts'] as $options => $stats)

$avg_exc_good = (($eval_percentage_exc + $eval_percentage_vgood)/$survey_question_stats['total_survey_attempt']) * 100;
$avg_exc_good_txt = 'very good or excellent';

if($avg_exc_good < 80){
	$avg_exc_good = (($eval_percentage_exc + $eval_percentage_vgood + $eval_percentage_good)/$survey_question_stats['total_survey_attempt'])* 100;		
	$avg_exc_good_txt = 'good, very good or excellent';
}//end if($avg_exc_good < 80)


	$question_dissatisfied = array();
	$question_satisfied = array();
	
	if($this->session->pharmacy_type == 'OF'){

		//Question 3a Dissatisfied
		$question_3_opt1 = $this->survey->get_survey_options_stats($survey_id, 3, 9); //Not at all satisfied
		$question_3_opt1_per = ($question_3_opt1/$total_submitted_surveys) * 100;
	
		$question_3_opt2 = $this->survey->get_survey_options_stats($survey_id, 3, 10); //Not very satisfied
		$question_3_opt2_per = ($question_3_opt2/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['3'] = ($question_3_opt1_per + $question_3_opt2_per);
		
		//Question 3a Satisfied
		$question_3_opt4 = $this->survey->get_survey_options_stats($survey_id, 3, 12); //Very Satisfied
		$question_3_opt4_per = ($question_3_opt4/$total_submitted_surveys) * 100;
		
		$question_satisfied['3'] = $question_3_opt4_per;
	
		// ---------------------------------------
		//Question 3b Dissatisfied
		$question_3b_opt2 = $this->survey->get_survey_options_stats($survey_id, 33, 126); //YES
		$question_3b_opt2_per = ($question_3b_opt2/$total_submitted_surveys) * 100;
	
		$question_dissatisfied['33'] = $question_3b_opt2_per;
		
		//Question 3b Satisfied
		$question_3b_opt1 = $this->survey->get_survey_options_stats($survey_id, 33, 127); //NO
		$question_3b_opt1_per = ($question_3b_opt1/$total_submitted_surveys) * 100;
		
		$question_satisfied['33'] = $question_3b_opt1_per;
	
		// ---------------------------------------
		//Question 3c Dissatisfied
		$question_3c_opt1 = $this->survey->get_survey_options_stats($survey_id, 34, 129); //NO
		$question_3c_opt1_per = ($question_3c_opt1/$total_submitted_surveys) * 100;
	
		$question_dissatisfied['34'] = $question_3c_opt1_per;
		
		//Question 3c Satisfied
		$question_3c_opt2 = $this->survey->get_survey_options_stats($survey_id, 34, 128); //YES
		$question_3c_opt2_per = ($question_3c_opt2/$total_submitted_surveys) * 100;
		
		$question_satisfied['34'] = $question_3c_opt2_per;
	
		// ---------------------------------------
		//Question 3d Dissatisfied
		$question_3d_opt1 = $this->survey->get_survey_options_stats($survey_id, 35, 131); //NO
		$question_3d_opt1_per = ($question_3d_opt1/$total_submitted_surveys) * 100;
	
		$question_dissatisfied['35'] = $question_3d_opt1_per;
		
		//Question 3d Satisfied
		$question_3d_opt2 = $this->survey->get_survey_options_stats($survey_id, 35, 130); //YES
		$question_3d_opt2_per = ($question_3d_opt2/$total_submitted_surveys) * 100;
		
		$question_satisfied['35'] = $question_3d_opt2_per;
	
		// ---------------------------------------
		//Question 4a Dissatisfied
		$question_4_opt1 = $this->survey->get_survey_options_stats($survey_id, 14, 13); //Very Poor
		$question_4_opt1_per = ($question_4_opt1/$total_submitted_surveys) * 100;
	
		$question_4_opt2 = $this->survey->get_survey_options_stats($survey_id, 14, 14); //Fairly Poor
		$question_4_opt2_per = ($question_4_opt2/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['14'] = ($question_4_opt1_per + $question_4_opt2_per);
		
		//Question 4a Satisfied
		$question_4_opt3 = $this->survey->get_survey_options_stats($survey_id, 14, 15); //Fairly Good
		$question_4_opt3_per = ($question_4_opt3/$total_submitted_surveys) * 100;
	
		$question_4_opt4 = $this->survey->get_survey_options_stats($survey_id, 14, 16); //Very Good
		$question_4_opt4_per = ($question_4_opt4/$total_submitted_surveys) * 100;
		
		$question_satisfied['14'] = ($question_4_opt3_per + $question_4_opt4_per); 
	
		// ---------------------------------------
		//Question 4b Dissatisfied
		$question_4b_opt1 = $this->survey->get_survey_options_stats($survey_id, 15, 19); //Very Poor
		$question_4b_opt1_per = ($question_4b_opt1/$total_submitted_surveys) * 100;
	
		$question_4b_opt2 = $this->survey->get_survey_options_stats($survey_id, 15, 20); //Fairly Poor
		$question_4b_opt2_per = ($question_4b_opt2/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['15'] = ($question_4b_opt1_per + $question_4b_opt2_per);
		
		//Question 4b Satisfied
		$question_4b_opt3 = $this->survey->get_survey_options_stats($survey_id, 15, 21); //Fairly Good
		$question_4b_opt3_per = ($question_4b_opt3/$total_submitted_surveys) * 100;
	
		$question_4b_opt4 = $this->survey->get_survey_options_stats($survey_id, 15, 22); //Very Good
		$question_4b_opt4_per = ($question_4b_opt4/$total_submitted_surveys) * 100;
		
		$question_satisfied['15'] = ($question_4b_opt3_per + $question_4b_opt4_per); 
	
		// ---------------------------------------
		//Question 4c Dissatisfied
		$question_4c_opt1 = $this->survey->get_survey_options_stats($survey_id, 16, 26); //Very Poor
		$question_4c_opt1_per = ($question_4c_opt1/$total_submitted_surveys) * 100;
	
		$question_4c_opt2 = $this->survey->get_survey_options_stats($survey_id, 16, 27); //Fairly Poor
		$question_4c_opt2_per = ($question_4c_opt2/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['16'] = ($question_4c_opt1_per + $question_4c_opt2_per);
		
		//Question 4c Satisfied
		$question_4c_opt3 = $this->survey->get_survey_options_stats($survey_id, 16, 29); //Fairly Good
		$question_4c_opt3_per = ($question_4c_opt3/$total_submitted_surveys) * 100;
	
		$question_4c_opt4 = $this->survey->get_survey_options_stats($survey_id, 16, 31); //Very Good
		$question_4c_opt4_per = ($question_4c_opt4/$total_submitted_surveys) * 100;
		
		$question_satisfied['16'] = ($question_4c_opt3_per + $question_4c_opt4_per); 
	
		// ---------------------------------------
		//Question 4d Dissatisfied
		$question_4d_opt1 = $this->survey->get_survey_options_stats($survey_id, 17, 34); //Very Poor
		$question_4d_opt1_per = ($question_4d_opt1/$total_submitted_surveys) * 100;
	
		$question_4d_opt2 = $this->survey->get_survey_options_stats($survey_id, 17, 35); //Fairly Poor
		$question_4d_opt2_per = ($question_4d_opt2/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['17'] = ($question_4d_opt1_per + $question_4d_opt2_per);
		
		//Question 4d Satisfied
		$question_4d_opt3 = $this->survey->get_survey_options_stats($survey_id, 17, 36); //Fairly Good
		$question_4d_opt3_per = ($question_4d_opt3/$total_submitted_surveys) * 100;
	
		$question_4d_opt4 = $this->survey->get_survey_options_stats($survey_id, 17, 37); //Very Good
		$question_4d_opt4_per = ($question_4d_opt4/$total_submitted_surveys) * 100;
		
		$question_satisfied['17'] = ($question_4d_opt3_per + $question_4d_opt4_per); 
	
		// ---------------------------------------
		//Question 4e Dissatisfied
		$question_4e_opt1 = $this->survey->get_survey_options_stats($survey_id, 18, 39); //Very Poor
		$question_4e_opt1_per = ($question_4e_opt1/$total_submitted_surveys) * 100;
	
		$question_4e_opt2 = $this->survey->get_survey_options_stats($survey_id, 18, 40); //Fairly Poor
		$question_4e_opt2_per = ($question_4e_opt2/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['18'] = ($question_4e_opt1_per + $question_4e_opt2_per);
		
		//Question 4e Satisfied
		$question_4e_opt3 = $this->survey->get_survey_options_stats($survey_id, 18, 41); //Fairly Good
		$question_4e_opt3_per = ($question_4e_opt3/$total_submitted_surveys) * 100;
	
		$question_4e_opt4 = $this->survey->get_survey_options_stats($survey_id, 18, 42); //Very Good
		$question_4e_opt4_per = ($question_4e_opt4/$total_submitted_surveys) * 100;
		
		$question_satisfied['18'] = ($question_4e_opt3_per + $question_4e_opt4_per); 
		
		// ---------------------------------------
		//Question 4f Dissatisfied
		$question_4f_opt1 = $this->survey->get_survey_options_stats($survey_id, 19, 44); //Very Poor
		$question_4f_opt1_per = ($question_4f_opt1/$total_submitted_surveys) * 100;
	
		$question_4f_opt2 = $this->survey->get_survey_options_stats($survey_id, 19, 45); //Fairly Poor
		$question_4f_opt2_per = ($question_4f_opt2/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['19'] = ($question_4f_opt1_per + $question_4f_opt2_per);
		
		//Question 4f Satisfied
		$question_4f_opt3 = $this->survey->get_survey_options_stats($survey_id, 19, 46); //Fairly Good
		$question_4f_opt3_per = ($question_4f_opt3/$total_submitted_surveys) * 100;
	
		$question_4f_opt4 = $this->survey->get_survey_options_stats($survey_id, 19, 47); //Very Good
		$question_4f_opt4_per = ($question_4f_opt4/$total_submitted_surveys) * 100;
		
		$question_satisfied['19'] = ($question_4f_opt3_per + $question_4f_opt4_per); 
		
		// ---------------------------------------
		//Question 5a Dissatisfied
		$question_5a_opt1 = $this->survey->get_survey_options_stats($survey_id, 20, 49); //Very Poor
		$question_5a_opt1_per = ($question_5a_opt1/$total_submitted_surveys) * 100;
	
		$question_5a_opt2 = $this->survey->get_survey_options_stats($survey_id, 20, 50); //Fairly Poor
		$question_5a_opt2_per = ($question_5a_opt2/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['20'] = ($question_5a_opt1_per + $question_5a_opt2_per);
		
		//Question 5a Satisfied
		$question_5a_opt3 = $this->survey->get_survey_options_stats($survey_id, 20, 51); //Fairly Good
		$question_5a_opt3_per = ($question_5a_opt3/$total_submitted_surveys) * 100;
	
		$question_5a_opt4 = $this->survey->get_survey_options_stats($survey_id, 20, 52); //Very Good
		$question_5a_opt4_per = ($question_5a_opt4/$total_submitted_surveys) * 100;
		
		$question_satisfied['20'] = ($question_5a_opt3_per + $question_5a_opt4_per); 
	
		// ---------------------------------------
		//Question 5b Dissatisfied
		$question_5b_opt1 = $this->survey->get_survey_options_stats($survey_id, 21, 54); //Very Poor
		$question_5b_opt1_per = ($question_5b_opt1/$total_submitted_surveys) * 100;
	
		$question_5b_opt2 = $this->survey->get_survey_options_stats($survey_id, 21, 55); //Fairly Poor
		$question_5b_opt2_per = ($question_5b_opt2/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['21'] = ($question_5b_opt1_per + $question_5b_opt2_per);
		
		//Question 5b Satisfied
		$question_5b_opt3 = $this->survey->get_survey_options_stats($survey_id, 21, 56); //Fairly Good
		$question_5b_opt3_per = ($question_5b_opt3/$total_submitted_surveys) * 100;
	
		$question_5b_opt4 = $this->survey->get_survey_options_stats($survey_id, 21, 57); //Very Good
		$question_5b_opt4_per = ($question_5b_opt4/$total_submitted_surveys) * 100;
		
		$question_satisfied['21'] = ($question_5b_opt3_per + $question_5b_opt4_per); 
	
		// ---------------------------------------
		//Question 5c Dissatisfied
		$question_5c_opt1 = $this->survey->get_survey_options_stats($survey_id, 22, 59); //Very Poor
		$question_5c_opt1_per = ($question_5c_opt1/$total_submitted_surveys) * 100;
	
		$question_5c_opt2 = $this->survey->get_survey_options_stats($survey_id, 22, 60); //Fairly Poor
		$question_5c_opt2_per = ($question_5c_opt2/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['22'] = ($question_5c_opt1_per + $question_5c_opt2_per);
		
		//Question 5c Satisfied
		$question_5c_opt3 = $this->survey->get_survey_options_stats($survey_id, 22, 61); //Fairly Good
		$question_5c_opt3_per = ($question_5c_opt3/$total_submitted_surveys) * 100;
	
		$question_5c_opt4 = $this->survey->get_survey_options_stats($survey_id, 22, 62); //Very Good
		$question_5c_opt4_per = ($question_5c_opt4/$total_submitted_surveys) * 100;
		
		$question_satisfied['22'] = ($question_5c_opt3_per + $question_5c_opt4_per); 
	
		// ---------------------------------------
		//Question 5d Dissatisfied
		$question_5d_opt1 = $this->survey->get_survey_options_stats($survey_id, 23, 64); //Very Poor
		$question_5d_opt1_per = ($question_5d_opt1/$total_submitted_surveys) * 100;
	
		$question_5d_opt2 = $this->survey->get_survey_options_stats($survey_id, 23, 65); //Fairly Poor
		$question_5d_opt2_per = ($question_5d_opt2/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['23'] = ($question_5d_opt1_per + $question_5d_opt2_per);
		
		//Question 5d Satisfied
		$question_5d_opt3 = $this->survey->get_survey_options_stats($survey_id, 23, 66); //Fairly Good
		$question_5d_opt3_per = ($question_5d_opt3/$total_submitted_surveys) * 100;
	
		$question_5d_opt4 = $this->survey->get_survey_options_stats($survey_id, 23, 67); //Very Good
		$question_5d_opt4_per = ($question_5d_opt4/$total_submitted_surveys) * 100;
		
		$question_satisfied['23'] = ($question_5d_opt3_per + $question_5d_opt4_per); 
	
		// ---------------------------------------
		//Question 5e Dissatisfied
		$question_5e_opt1 = $this->survey->get_survey_options_stats($survey_id, 24, 69); //Very Poor
		$question_5e_opt1_per = ($question_5e_opt1/$total_submitted_surveys) * 100;
	
		$question_5e_opt2 = $this->survey->get_survey_options_stats($survey_id, 24, 70); //Fairly Poor
		$question_5e_opt2_per = ($question_5e_opt2/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['24'] = ($question_5e_opt1_per + $question_5e_opt2_per);
		
		//Question 5e Satisfied
		$question_5e_opt3 = $this->survey->get_survey_options_stats($survey_id, 24, 71); //Fairly Good
		$question_5e_opt3_per = ($question_5e_opt3/$total_submitted_surveys) * 100;
	
		$question_5e_opt4 = $this->survey->get_survey_options_stats($survey_id, 24, 72); //Very Good
		$question_5e_opt4_per = ($question_5e_opt4/$total_submitted_surveys) * 100;
		
		$question_satisfied['24'] = ($question_5e_opt3_per + $question_5e_opt4_per); 
	
		// ---------------------------------------
		//Question 5f Dissatisfied
		$question_5f_opt1 = $this->survey->get_survey_options_stats($survey_id, 25, 74); //Very Poor
		$question_5f_opt1_per = ($question_5f_opt1/$total_submitted_surveys) * 100;
	
		$question_5f_opt2 = $this->survey->get_survey_options_stats($survey_id, 25, 75); //Fairly Poor
		$question_5f_opt2_per = ($question_5f_opt2/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['25'] = ($question_5f_opt1_per + $question_5f_opt2_per);
		
		//Question 5f Satisfied
		$question_5f_opt3 = $this->survey->get_survey_options_stats($survey_id, 25, 76); //Fairly Good
		$question_5f_opt3_per = ($question_5f_opt3/$total_submitted_surveys) * 100;
	
		$question_5f_opt4 = $this->survey->get_survey_options_stats($survey_id, 25, 77); //Very Good
		$question_5f_opt4_per = ($question_5f_opt4/$total_submitted_surveys) * 100;
		
		$question_satisfied['25'] = ($question_5f_opt3_per + $question_5f_opt4_per); 
	
		// ---------------------------------------
		//Question 6a Dissatisfied
		$question_6a_opt1 = $this->survey->get_survey_options_stats($survey_id, 26, 79); //Not At All Well
		$question_6a_opt1_per = ($question_6a_opt1/$total_submitted_surveys) * 100;
	
		$question_6a_opt2 = $this->survey->get_survey_options_stats($survey_id, 26, 80); //Fairly Well
		$question_6a_opt2_per = ($question_6a_opt2/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['26'] = ($question_6a_opt1_per + $question_6a_opt2_per);
		
		//Question 6a Satisfied
		$question_6a_opt3 = $this->survey->get_survey_options_stats($survey_id, 26, 81); //Fairly Well
		$question_6a_opt3_per = ($question_6a_opt3/$total_submitted_surveys) * 100;
	
		$question_6a_opt4 = $this->survey->get_survey_options_stats($survey_id, 26, 82); //Very Well
		$question_6a_opt4_per = ($question_6a_opt4/$total_submitted_surveys) * 100;
		
		$question_satisfied['26'] = ($question_6a_opt3_per + $question_6a_opt4_per); 
	
	
		// ---------------------------------------
		//Question 6b Dissatisfied
		$question_6b_opt1 = $this->survey->get_survey_options_stats($survey_id, 27, 84); //Not At All Well
		$question_6b_opt1_per = ($question_6b_opt1/$total_submitted_surveys) * 100;
	
		$question_6b_opt2 = $this->survey->get_survey_options_stats($survey_id, 27, 85); //Fairly Well
		$question_6b_opt2_per = ($question_6b_opt2/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['27'] = ($question_6b_opt1_per + $question_6b_opt2_per) ;
		
		//Question 6b Satisfied
		$question_6b_opt3 = $this->survey->get_survey_options_stats($survey_id, 27, 86); //Fairly Well
		$question_6b_opt3_per = ($question_6b_opt3/$total_submitted_surveys) * 100;
	
		$question_6b_opt4 = $this->survey->get_survey_options_stats($survey_id, 27, 87); //Very Well
		$question_6b_opt4_per = ($question_6b_opt4/$total_submitted_surveys) * 100;
		
		$question_satisfied['27'] = ($question_6b_opt3_per + $question_6b_opt4_per) ; 
	
		// ---------------------------------------
		//Question 6c Dissatisfied
		$question_6c_opt1 = $this->survey->get_survey_options_stats($survey_id, 28, 89); //Not At All Well
		$question_6c_opt1_per = ($question_6c_opt1/$total_submitted_surveys) * 100;
	
		$question_6c_opt2 = $this->survey->get_survey_options_stats($survey_id, 28, 90); //Fairly Well
		$question_6c_opt2_per = ($question_6c_opt2/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['28'] = ($question_6c_opt1_per + $question_6c_opt2_per);
		
		//Question 6c Satisfied
		$question_6c_opt3 = $this->survey->get_survey_options_stats($survey_id, 28, 91); //Fairly Well
		$question_6c_opt3_per = ($question_6c_opt3/$total_submitted_surveys) * 100;
	
		$question_6c_opt4 = $this->survey->get_survey_options_stats($survey_id, 28, 92); //Very Well
		$question_6c_opt4_per = ($question_6c_opt4/$total_submitted_surveys) * 100;
		
		$question_satisfied['28'] = ($question_6c_opt3_per + $question_6c_opt4_per); 
		
		// ---------------------------------------
		//Question 6d Dissatisfied
		$question_6d_opt1 = $this->survey->get_survey_options_stats($survey_id, 29, 94); //Not At All Well
		$question_6d_opt1_per = ($question_6d_opt1/$total_submitted_surveys) * 100;
	
		$question_6d_opt2 = $this->survey->get_survey_options_stats($survey_id, 29, 95); //Fairly Well
		$question_6d_opt2_per = ($question_6d_opt2/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['29'] = ($question_6d_opt1_per + $question_6d_opt2_per);
		
		//Question 6d Satisfied
		$question_6d_opt3 = $this->survey->get_survey_options_stats($survey_id, 29, 96); //Fairly Well
		$question_6d_opt3_per = ($question_6d_opt3/$total_submitted_surveys) * 100;
	
		$question_6d_opt4 = $this->survey->get_survey_options_stats($survey_id, 29, 97); //Very Well
		$question_6d_opt4_per = ($question_6d_opt4/$total_submitted_surveys) * 100;
		
		$question_satisfied['29'] = ($question_6d_opt3_per + $question_6d_opt4_per); 
	
		// ---------------------------------------
		//Question 7a Dissatified
		$question_7a_opt1 = $this->survey->get_survey_options_stats($survey_id, 30, 100); //No
		$question_7a_opt1_per = ($question_7a_opt1/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['30'] = $question_7a_opt1_per;
		
		//Question 7a Satisfied
		$question_7a_opt2 = $this->survey->get_survey_options_stats($survey_id, 30, 99); //Yes
		$question_7a_opt2_per = ($question_7a_opt2/$total_submitted_surveys) * 100;
		
		$question_satisfied['30'] = $question_7a_opt2_per;

		//Question 7b Dissatified
		$question_7b_opt1 = $this->survey->get_survey_options_stats($survey_id, 31, 102); //No
		$question_7b_opt1_per = ($question_7b_opt1/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['31'] = $question_7b_opt1_per;
		
		//Question 7b Satisfied
		$question_7b_opt2 = $this->survey->get_survey_options_stats($survey_id, 31, 101); //Yes
		$question_7b_opt2_per = ($question_7b_opt2/$total_submitted_surveys) * 100;
		
		$question_satisfied['31'] = $question_7b_opt2_per;
		
		//Question 7c Dissatified
		$question_7c_opt1 = $this->survey->get_survey_options_stats($survey_id, 32, 104); //No
		$question_7c_opt1_per = ($question_7c_opt1/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['32'] = $question_7b_opt1_per;
		
		//Question 7c Satisfied
		$question_7c_opt2 = $this->survey->get_survey_options_stats($survey_id, 32, 103); //Yes
		$question_7c_opt2_per = ($question_7c_opt2/$total_submitted_surveys) * 100;
		
		$question_satisfied['32'] = $question_7c_opt2_per;
	
		// ---------------------------------------
		//Question 9 Dissatisfied
		$question_9_opt1 = $this->survey->get_survey_options_stats($survey_id, 9, 108); //Poor
		$question_9_opt1_per = ($question_9_opt1/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['9'] = $question_9_opt1_per;
	
		//Question 9 Satisfied
		$question_9_opt3 = $this->survey->get_survey_options_stats($survey_id, 9, 110); //Good
		$question_9_opt3_per = ($question_9_opt3/$total_submitted_surveys) * 100;
	
		$question_9_opt4 = $this->survey->get_survey_options_stats($survey_id, 9, 111); //Very Good
		$question_9_opt4_per = ($question_9_opt4/$total_submitted_surveys) * 100;
	
		$question_9_opt5 = $this->survey->get_survey_options_stats($survey_id, 9, 112); //Excellent
		$question_9_opt5_per = ($question_9_opt5/$total_submitted_surveys) * 100;
		
		$question_satisfied['9'] = ($question_9_opt3_per + $question_9_opt4_per + $question_9_opt5_per); 
		
	}else{

		//Question 2 Dissatisfied
		$question_2_opt3 = $this->survey->get_survey_options_stats($survey_id, 37, 136); //Not Very
		$question_2_opt3_per = ($question_3_opt1/$total_submitted_surveys) * 100;
	
		$question_2_opt4 = $this->survey->get_survey_options_stats($survey_id, 37, 137); //Not at all
		$question_2_opt4_per = ($question_2_opt4/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['37'] = ($question_2_opt3_per + $question_2_opt4_per);
		
		//Question 2 Satisfied
		$question_2_opt1 = $this->survey->get_survey_options_stats($survey_id, 37, 134); //Very 
		$question_2_opt1_per = ($question_2_opt1/$total_submitted_surveys) * 100;

		$question_2_opt2 = $this->survey->get_survey_options_stats($survey_id, 37, 135); //fairly
		$question_2_opt2_per = ($question_2_opt2/$total_submitted_surveys) * 100;
		
		$question_satisfied['37'] = ($question_2_opt1_per + $question_2_opt2_per);

		//Question 3 Dissatisfied
		$question_3_opt3 = $this->survey->get_survey_options_stats($survey_id, 38, 140); //Not Very
		$question_3_opt3_per = ($question_3_opt3/$total_submitted_surveys) * 100;
	
		$question_3_opt4 = $this->survey->get_survey_options_stats($survey_id, 38, 141); //Not at all
		$question_3_opt4_per = ($question_3_opt4/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['38'] = ($question_3_opt3_per + $question_3_opt4_per);
		
		//Question 3 Satisfied
		$question_3_opt1 = $this->survey->get_survey_options_stats($survey_id, 38, 138); //Very 
		$question_3_opt1_per = ($question_3_opt1/$total_submitted_surveys) * 100;

		$question_3_opt2 = $this->survey->get_survey_options_stats($survey_id, 38, 139); //fairly
		$question_3_opt2_per = ($question_3_opt2/$total_submitted_surveys) * 100;
		
		$question_satisfied['38'] = ($question_3_opt1_per + $question_3_opt2_per);

		//Question 4a Dissatisfied
		$question_4a_opt2 = $this->survey->get_survey_options_stats($survey_id, 52, 143); //Very Poor
		$question_4a_opt2_per = ($question_4a_opt2/$total_submitted_surveys) * 100;
	
		$question_4a_opt3 = $this->survey->get_survey_options_stats($survey_id, 52, 144); //Fairly Poor
		$question_4a_opt3_per = ($question_4a_opt3/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['52'] = ($question_4a_opt2_per + $question_4a_opt3_per);
		
		//Question 4a Satisfied
		$question_4a_opt4 = $this->survey->get_survey_options_stats($survey_id, 52, 145); //Fairly Good 
		$question_4a_opt4_per = ($question_4a_opt4/$total_submitted_surveys) * 100;

		$question_4a_opt5 = $this->survey->get_survey_options_stats($survey_id, 52, 146); //Very Good
		$question_4a_opt5_per = ($question_4a_opt5/$total_submitted_surveys) * 100;
		
		$question_satisfied['52'] = ($question_4a_opt4_per + $question_4a_opt5_per);

		//Question 4b Dissatisfied
		$question_4b_opt2 = $this->survey->get_survey_options_stats($survey_id, 53, 148); //Very Poor
		$question_4b_opt2_per = ($question_4b_opt2/$total_submitted_surveys) * 100;
	
		$question_4b_opt3 = $this->survey->get_survey_options_stats($survey_id, 53, 149); //Fairly Poor
		$question_4b_opt3_per = ($question_4b_opt3/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['53'] = ($question_4b_opt2_per + $question_4b_opt3_per);
		
		//Question 4b Satisfied
		$question_4b_opt4 = $this->survey->get_survey_options_stats($survey_id, 53, 150); //Fairly Good 
		$question_4b_opt4_per = ($question_4b_opt4/$total_submitted_surveys) * 100;

		$question_4b_opt5 = $this->survey->get_survey_options_stats($survey_id, 53, 151); //Very Good
		$question_4b_opt5_per = ($question_4b_opt5/$total_submitted_surveys) * 100;
		
		$question_satisfied['53'] = ($question_4b_opt4_per + $question_4b_opt5_per);

		//Question 4c Dissatisfied
		$question_4c_opt2 = $this->survey->get_survey_options_stats($survey_id, 54, 153); //Very Poor
		$question_4c_opt2_per = ($question_4c_opt2/$total_submitted_surveys) * 100;
	
		$question_4c_opt3 = $this->survey->get_survey_options_stats($survey_id, 54, 154); //Fairly Poor
		$question_4c_opt3_per = ($question_4c_opt3/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['54'] = ($question_4c_opt2_per + $question_4c_opt3_per);
		
		//Question 4c Satisfied
		$question_4c_opt4 = $this->survey->get_survey_options_stats($survey_id, 54, 155); //Fairly Good 
		$question_4c_opt4_per = ($question_4c_opt4/$total_submitted_surveys) * 100;

		$question_4c_opt5 = $this->survey->get_survey_options_stats($survey_id, 54, 156); //Very Good
		$question_4c_opt5_per = ($question_4c_opt5/$total_submitted_surveys) * 100;
		
		$question_satisfied['54'] = ($question_4c_opt4_per + $question_4c_opt5_per);

		//Question 4d Dissatisfied
		$question_4d_opt2 = $this->survey->get_survey_options_stats($survey_id, 55, 158); //Very Poor
		$question_4d_opt2_per = ($question_4d_opt2/$total_submitted_surveys) * 100;
	
		$question_4d_opt3 = $this->survey->get_survey_options_stats($survey_id, 55, 159); //Fairly Poor
		$question_4d_opt3_per = ($question_4d_opt3/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['55'] = ($question_4d_opt2_per + $question_4d_opt3_per);

		
		//Question 4d Satisfied
		$question_4d_opt4 = $this->survey->get_survey_options_stats($survey_id, 55, 160); //Fairly Good 
		$question_4d_opt4_per = ($question_4d_opt4/$total_submitted_surveys) * 100;

		$question_4d_opt5 = $this->survey->get_survey_options_stats($survey_id, 55, 161); //Very Good
		$question_4d_opt5_per = ($question_4d_opt5/$total_submitted_surveys) * 100;
		
		$question_satisfied['55'] = ($question_4d_opt4_per + $question_4d_opt5_per);

		//Question 4e Dissatisfied
		$question_4e_opt2 = $this->survey->get_survey_options_stats($survey_id, 56, 163); //Very Poor
		$question_4e_opt2_per = ($question_4e_opt2/$total_submitted_surveys) * 100;
	
		$question_4e_opt3 = $this->survey->get_survey_options_stats($survey_id, 56, 164); //Fairly Poor
		$question_4e_opt3_per = ($question_4e_opt3/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['56'] = ($question_4e_opt2_per + $question_4e_opt3_per);
		
		//Question 4e Satisfied
		$question_4e_opt4 = $this->survey->get_survey_options_stats($survey_id, 56, 165); //Fairly Good 
		$question_4e_opt4_per = ($question_4e_opt4/$total_submitted_surveys) * 100;

		$question_4e_opt5 = $this->survey->get_survey_options_stats($survey_id, 56, 166); //Very Good
		$question_4e_opt5_per = ($question_4e_opt5/$total_submitted_surveys) * 100;
		
		$question_satisfied['56'] = ($question_4e_opt4_per + $question_4e_opt5_per);

		//Question 5 Dissatisfied
		$question_5_opt2 = $this->survey->get_survey_options_stats($survey_id, 40, 201); //Very Poor
		$question_5_opt2_per = ($question_5_opt2/$total_submitted_surveys) * 100;
	
		$question_5_opt3 = $this->survey->get_survey_options_stats($survey_id, 40, 202); //Fairly Poor
		$question_5_opt3_per = ($question_5_opt3/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['40'] = ($question_5_opt2_per + $question_5_opt3_per);
		
		//Question 5 Satisfied
		$question_5_opt4 = $this->survey->get_survey_options_stats($survey_id, 40, 203); //Fairly Good 
		$question_5_opt4_per = ($question_5_opt4/$total_submitted_surveys) * 100;

		$question_5_opt5 = $this->survey->get_survey_options_stats($survey_id, 40, 204); //Very Good
		$question_5_opt5_per = ($question_5_opt5/$total_submitted_surveys) * 100;
		
		$question_satisfied['40'] = ($question_5_opt4_per + $question_5_opt5_per);

		//Question 6a Dissatisfied
		$question_6a_opt2 = $this->survey->get_survey_options_stats($survey_id, 57, 206); //Very Poor
		$question_6a_opt2_per = ($question_6a_opt2/$total_submitted_surveys) * 100;
	
		$question_6a_opt3 = $this->survey->get_survey_options_stats($survey_id, 57, 207); //Fairly Poor
		$question_6a_opt3_per = ($question_6a_opt3/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['57'] = ($question_6a_opt2_per + $question_6a_opt3_per);
		
		//Question 6a Satisfied
		$question_6a_opt4 = $this->survey->get_survey_options_stats($survey_id, 57, 208); //Fairly Good 
		$question_6a_opt4_per = ($question_6a_opt4/$total_submitted_surveys) * 100;

		$question_6a_opt5 = $this->survey->get_survey_options_stats($survey_id, 57, 209); //Very Good
		$question_6a_opt5_per = ($question_6a_opt5/$total_submitted_surveys) * 100;
		
		$question_satisfied['57'] = ($question_6a_opt4_per + $question_6a_opt5_per);

		//Question 6b Dissatisfied
		$question_6b_opt2 = $this->survey->get_survey_options_stats($survey_id, 58, 211); //Very Poor
		$question_6b_opt2_per = ($question_6b_opt2/$total_submitted_surveys) * 100;
	
		$question_6b_opt3 = $this->survey->get_survey_options_stats($survey_id, 58, 212); //Fairly Poor
		$question_6b_opt3_per = ($question_6b_opt3/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['58'] = ($question_6b_opt2_per + $question_6b_opt3_per);
		
		//Question 6b Satisfied
		$question_6b_opt4 = $this->survey->get_survey_options_stats($survey_id, 58, 213); //Fairly Good 
		$question_6b_opt4_per = ($question_6b_opt4/$total_submitted_surveys) * 100;

		$question_6b_opt5 = $this->survey->get_survey_options_stats($survey_id, 58, 214); //Very Good
		$question_6b_opt5_per = ($question_6b_opt5/$total_submitted_surveys) * 100;
		
		$question_satisfied['58'] = ($question_6b_opt4_per + $question_6b_opt5_per);

		//Question 6c Dissatisfied
		$question_6c_opt2 = $this->survey->get_survey_options_stats($survey_id, 59, 216); //Very Poor
		$question_6c_opt2_per = ($question_6c_opt2/$total_submitted_surveys) * 100;
	
		$question_6c_opt3 = $this->survey->get_survey_options_stats($survey_id, 59, 217); //Fairly Poor
		$question_6c_opt3_per = ($question_6c_opt3/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['59'] = ($question_6c_opt2_per + $question_6c_opt3_per);
		
		//Question 6c Satisfied
		$question_6c_opt4 = $this->survey->get_survey_options_stats($survey_id, 59, 218); //Fairly Good 
		$question_6c_opt4_per = ($question_6c_opt4/$total_submitted_surveys) * 100;

		$question_6c_opt5 = $this->survey->get_survey_options_stats($survey_id, 59, 219); //Very Good
		$question_6c_opt5_per = ($question_6c_opt5/$total_submitted_surveys) * 100;
		
		$question_satisfied['59'] = ($question_6c_opt4_per + $question_6c_opt5_per);

		//Question 6d Dissatisfied
		$question_6d_opt2 = $this->survey->get_survey_options_stats($survey_id, 60, 221); //Very Poor
		$question_6d_opt2_per = ($question_6d_opt2/$total_submitted_surveys) * 100;
	
		$question_6d_opt3 = $this->survey->get_survey_options_stats($survey_id, 60, 222); //Fairly Poor
		$question_6d_opt3_per = ($question_6d_opt3/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['60'] = ($question_6d_opt2_per + $question_6d_opt3_per);
		
		//Question 6d Satisfied
		$question_6d_opt4 = $this->survey->get_survey_options_stats($survey_id, 60, 223); //Fairly Good 
		$question_6d_opt4_per = ($question_6d_opt4/$total_submitted_surveys) * 100;

		$question_6d_opt5 = $this->survey->get_survey_options_stats($survey_id, 60, 224); //Very Good
		$question_6d_opt5_per = ($question_6d_opt5/$total_submitted_surveys) * 100;
		
		$question_satisfied['60'] = ($question_6d_opt4_per + $question_6d_opt5_per);

		// ---------------------------------------
		//Question 7a Dissatified
		$question_7a_opt1 = $this->survey->get_survey_options_stats($survey_id, 61, 168); //No
		$question_7a_opt1_per = ($question_7a_opt1/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['61'] = $question_7a_opt1_per;
		
		//Question 7a Satisfied
		$question_7a_opt2 = $this->survey->get_survey_options_stats($survey_id, 61, 167); //Yes
		$question_7a_opt2_per = ($question_7a_opt2/$total_submitted_surveys) * 100;
		
		$question_satisfied['61'] = $question_7a_opt2_per;

		//Question 7b Dissatified
		$question_7b_opt1 = $this->survey->get_survey_options_stats($survey_id, 62, 170); //No
		$question_7b_opt1_per = ($question_7b_opt1/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['62'] = $question_7b_opt1_per;
		
		//Question 7b Satisfied
		$question_7b_opt2 = $this->survey->get_survey_options_stats($survey_id, 62, 169); //Yes
		$question_7b_opt2_per = ($question_7b_opt2/$total_submitted_surveys) * 100;
		
		$question_satisfied['62'] = $question_7b_opt2_per;
		
		//Question 7c Dissatified
		$question_7c_opt1 = $this->survey->get_survey_options_stats($survey_id, 63, 172); //No
		$question_7c_opt1_per = ($question_7c_opt1/$total_submitted_surveys) * 171;
		
		$question_dissatisfied['63'] = $question_7b_opt1_per;
		
		//Question 7c Satisfied
		$question_7c_opt2 = $this->survey->get_survey_options_stats($survey_id, 63, 103); //Yes
		$question_7c_opt2_per = ($question_7c_opt2/$total_submitted_surveys) * 100;
		
		$question_satisfied['63'] = $question_7c_opt2_per;

		//Question 9 Dissatisfied
		$question_9_opt4 = $this->survey->get_survey_options_stats($survey_id, 44, 179); //Fair
		$question_9_opt4_per = ($question_9_opt4/$total_submitted_surveys) * 100;
	
		$question_9_opt5 = $this->survey->get_survey_options_stats($survey_id, 44, 180); //Poor
		$question_9_opt5_per = ($question_9_opt5/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['44'] = ($question_9_opt4_per + $question_9_opt5_per);
		
		//Question 9 Satisfied
		$question_9_opt1 = $this->survey->get_survey_options_stats($survey_id, 44, 176); //Excellent
		$question_9_opt1_per = ($question_9_opt1/$total_submitted_surveys) * 100;

		$question_9_opt2 = $this->survey->get_survey_options_stats($survey_id, 44, 177); //Very Good
		$question_9_opt2_per = ($question_9_opt2/$total_submitted_surveys) * 100;

		$question_9_opt3 = $this->survey->get_survey_options_stats($survey_id, 44, 178); //Very Good
		$question_9_opt3_per = ($question_9_opt3/$total_submitted_surveys) * 100;

		$question_satisfied['44'] = ($question_9_opt1_per + $question_9_opt2_per + $question_9_opt3_per);
		
		// ---------------------------------------
		//Question 11a Dissatisfied

		$question_11a_opt1 = $this->survey->get_survey_options_stats($survey_id, 46, 181); // YES (in this case YES is trreated as Disatisufied
		$question_11a_opt1_per = ($question_11a_opt1/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['46'] = $question_11a_opt1_per;
		
		//Question 11a Satisfied
		$question_11a_opt2 = $this->survey->get_survey_options_stats($survey_id, 46, 182); // No (In this cae NO is treated a satified answer)
		$question_11a_opt2_per = ($question_11a_opt2/$total_submitted_surveys) * 100;
		
		$question_satisfied['46'] = $question_11a_opt2_per;

		//Question 11c Dissatisfied

		$question_11c_opt1 = $this->survey->get_survey_options_stats($survey_id, 48, 184); // No
		$question_11c_opt1_per = ($question_11c_opt1/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['48'] = $question_11c_opt1_per;
		
		//Question 11c Satisfied
		$question_11c_opt2 = $this->survey->get_survey_options_stats($survey_id, 48, 185); // Yes
		$question_11c_opt2_per = ($question_11c_opt2/$total_submitted_surveys) * 100;
		
		$question_satisfied['48'] = $question_11c_opt2_per;

	}//end if($this->session->pharmacy_type == 'OF')

	arsort($question_dissatisfied);
	arsort($question_satisfied);


?>
<style>
	/* Page 1 Styles */
	
	.page_1_left {
		float:left; 
		width:24%; 
		border: solid 0px #000;
		padding-left:20px;
	}
	
	.page_1_left_img {
		width:25%; 
		float:left; 
		padding:5px 0px;
	}
	
	.page_1_left_heading{
		width:70%; 
		float:left; 
		padding:10px 0px;
		color:#006EBF;
		font-size:16px;
	}
	
	.page_1_left_text{
		float:left;
		width:100%;
		text-align:justify;	
		padding:20px 0px 10px 0px; 
		font-size:14px;
	}
	
	.page_1_right {
		float:left; width:70%; 
		border: solid 0px #000;
	}
	
	.page_1_right_heading {
		float:left; width:100%; 
		font-size:24px; 
		color:#006EBF;
		padding:30px 10px 10px 10px;
		text-align:center; 
	}
	
	.page_1_right_heading .heading_1 {
		color:#1B5DAE; 
		font-weight:bold;
		font-size: 32px;
	}
	
	.page_1_right_chart{
		float:left;	
	}
	
	/* Page 2 Styles */

	.page_2_left {
		float:left; 
		width:48%; 
		border: solid 0px #000;
		padding: 20px 5px;
	}

	.page_2_left_heading{
		float:left; 
		font-size:16px; 
		width:100%; 
		padding:5px 10px; 
		background-color:#006EBF; 
		color:#FFF	
	}

	.page_2_left_table_container{
		float:left; 
		width:100%; 
	}
	
	.page_2_right {
		float:left; width:50%; 
		border: solid 0px #000;
		padding: 20px 0px 20px 5px;
	}

	.page_2_right_chart{
		float:left;	
	}
	
	.demographic_data{
		width:100%; 
		float:left; 
		margin:10px 0px 5px 0px;	
		font-size:18px;	
	}
	
	.demographic_data p {
		margin: 10px 0px;	
	}

	.demographic_data_container{
		float: left; 
		width:100%; 
	}
	
	/*Common  Style */
	table{
		font-family:Arial, Helvetica, sans-serif;
		font-size:12px;
	}
	
	div {
		font-family:Arial, Helvetica, sans-serif;
		font-size:12px;
	}

	.poster_footer{
		width:100%;
		background-color:#007BC3; 
		color:#fff; 
		font-size:20px; 
		padding:40px 20px;
		height:60px; 
		font-weight:normal; 
		font-family:Arial, Helvetica, sans-serif;	
	}
	
	.footer_container {
		padding-top:30px; 
		width:100%; 
		float:left; 
		text-align:center 	
	}
	
	.footer_container .footer_note {
		text-align:center; 
		font-size:17px; 
		margin-top:0; 
		color:#CCC;
	}
	
	#age_stats {
		font-size: 24px;	
	}
	#age_stats td{
		padding:12px;	
	}
	
	
</style>

	<div class="page_1_left">
    	<div class="page_1_left_img">
        	<img src="<?php echo IMAGES?>nhs.png" width="50px" />
        </div>
        <div class="page_1_left_heading">
        	Providing an NHS service
        </div>
        
        <div class="page_1_left_text">
        
        	<?php 
				$PATIENT_SURVEY_RESULT_NHS_TEXT = 'PATIENT_SURVEY_RESULT_NHS_TEXT';
				$patient_survey_nhs_text = get_global_settings($PATIENT_SURVEY_RESULT_NHS_TEXT); //Set from the Global Settings
				$patient_survey_nhs_text = filter_string($patient_survey_nhs_text['setting_value']);
				
				echo $patient_survey_nhs_text = str_replace('[PHARMACY_NAME]',$this->session->pharmacy_name,$patient_survey_nhs_text);
			?>
        </div>
        
    </div>
    
    <div class="page_1_right">
    	<div class="page_1_right_heading">
        	<span class="heading_1">NHS Pharmacy Patient Survey Results</span> <br />
            <span> <span style="font-size:36px"><?php echo number_format($avg_exc_good,0)?>%</span> of our customers rate our service as <?php echo $avg_exc_good_txt?></span>
            </span> 
        </div>
        <div class="page_1_right_chart" style="margin-top:20px">

			<?php 
			
                $svg_image =  filter_string($survey_question_stats['closing_comments_arr']['svg_poster_code']);
                $svg_image = str_replace('<rect x="0" y="0" width="1000" height="800" stroke="none" stroke-width="0" fill="#ffffff"></rect>','',$svg_image);
                $svg_image = str_replace('<rect x="0" y="0" width="1138" height="900" stroke="none" stroke-width="0" fill="#ffffff"></rect>','',$svg_image);
                $svg_image = str_replace('<rect x="678" y="20" width="243" height="16" stroke="none" stroke-width="0" fill-opacity="0" fill="#ffffff"></rect>','',$svg_image);
				$svg_image = str_replace('<defs id="defs"></defs><g>','<defs id="defs"></defs><g transform="translate(20,172.5)">',$svg_image);
				
                echo $svg_image = str_replace('10 0 750 500','100 80 750 380',$svg_image);
				
            ?>
        </div>
    </div>
    
<pagebreak [ P ] />

	<div class="page_2_left">
    	<div class="page_2_left_heading">
        	Top areas of performance
        </div>
        <div class="page_2_left_table_container" style="margin-top:10px;">
        
        	<table cellpadding="5" cellspacing="0" width="99%">
            	<thead>
                    <tr>
                    	<td bgcolor="#CCCCCC" width="70%"><strong>Quesions</strong></td>
                        <td bgcolor="#CCCCCC" width="30%"><strong>Satisfied customers</strong></td>
                    </tr>
                </thead>
                <tbody>
                	<?php 
						for($i=0;$i<count($well_performed_arr);$i++){

							$survey_question_stats = get_survey_question_stats($this->session->p_id, $survey_id, $pharmacy_survey_details['survey_ref_no'], $well_performed_arr[$i]['question_id'],$this->session->pharmacy_type);
					
							$question_txt = ($survey_question_stats['parent_question']) ? filter_string($survey_question_stats['parent_question']).' <br><b>('.filter_string($survey_question_stats['question']) .') </b>' : filter_string($survey_question_stats['question']);
							
					?>
                            <tr>
                                <td>
                                    <?php echo $question_txt?>
                                </td>
                                <td>
                                	<?php 
										$satisfied_percentage = $question_satisfied[$well_performed_arr[$i]['question_id']];
										echo number_format($satisfied_percentage).'%';
									?>
                                </td>
                            </tr>
                    
                    <?php		
						}//end for($i=0;$i<count($well_performed_arr);$i++)
					?>
                </tbody>
            </table>
        </div>

    	<div class="page_2_left_heading">
        	Areas in greatest need of improvement
        </div>
        <div class="page_2_left_table_container" style="margin-top:10px;">
        	<table cellpadding="5" cellspacing="0" width="99%">
            	<thead>
                    <tr>
                    	<td bgcolor="#CCCCCC" style="width:70%"><strong>Quesions</strong></td>
                        <td bgcolor="#CCCCCC" style="width:30%"><strong>Dissatified customers</strong></td>
                    </tr>
                </thead>
                <tbody>
                	<?php 
						for($i=0;$i<count($bad_performed_arr);$i++){

							$survey_question_stats = get_survey_question_stats($this->session->p_id, $survey_id, $pharmacy_survey_details['survey_ref_no'], $bad_performed_arr[$i]['question_id'], $this->session->pharmacy_type);
							
							//print_this($survey_question_stats);
							
							$question_txt = ($survey_question_stats['parent_question']) ? filter_string($survey_question_stats['parent_question']).' <br><b>('.filter_string($survey_question_stats['question']) .') </b>' : filter_string($survey_question_stats['question']);
							
					?>
                        <tr>
                            <td>
                                <?php echo $question_txt?>
                            </td>
                            <td>
								<?php 
                                    $dissatisfied_percentage = $question_dissatisfied[$bad_performed_arr[$i]['question_id']];
                                    echo number_format($dissatisfied_percentage).'%';
                                ?>
                            </td>
                        </tr>                    	
                        <tr>
                            <td colspan="2" bgcolor="#F0F0F0">
                                <strong>Action Plan: </strong><?php echo filter_string($bad_performed_arr[$i]['bad_performed_comments'])?>
                            </td>
                        </tr>
					<?php		
							
						}//end for($i=0;$i<count($bad_performed_arr);$i++)
					?>
                </tbody>
            </table>
        </div>
        
    </div>
    
    <div class="page_2_right">
        
    	<div class="page_2_left_heading">
        	Demographic Data
        </div>
        <div class="page_2_right_chart">
        	<h3 style="padding-left:10px">Age range of customers</h3>
			<?php 
               $svg_age_pie_chart =  filter_string($svg_age_pie_chart);
               echo $svg_age_pie_chart = str_replace('10 0 750 500','100 0 750 500',$svg_age_pie_chart);
                
            ?>
        </div>

    	<!--<div class="page_2_left_heading">
        	Gender of customers
        </div>
        <div class="page_2_right_chart" id="svg_age_pie_chart">
			<?php 
               $svg_gender_pie_chart =  filter_string($svg_gender_pie_chart);
               echo $svg_gender_pie_chart = str_replace('10 0 750 500','100 0 900 500',$svg_gender_pie_chart);
                
            ?>
        </div>
        
        -->
        
        <div class="demographic_data">
        	
			<?php 
				$total_survey_attempt = ($survey_gender_question_stats['total_survey_attempt'] == 0) ? 1 : $survey_gender_question_stats['total_survey_attempt'];
				
				$female_option_attempted = $survey_gender_question_stats['options_attempts']['Female'];
				$female_eval_percentage = number_format( ($female_option_attempted / $total_survey_attempt) * 100, 2);

				$male_option_attempted = $survey_gender_question_stats['options_attempts']['Male'];
				$male_eval_percentage = number_format( ($male_option_attempted / $total_survey_attempt) * 100, 2);
				
			?>
            <p>Male respondents (<?php echo $male_eval_percentage?>%), Female (<?php echo $female_eval_percentage?>%)</p>
            
            <?php 
				$total_survey_attempt = ($accessing_pharmacy_stats['total_survey_attempt'] == 0) ? 1 : $accessing_pharmacy_stats['total_survey_attempt'];
				
				if($this->session->pharmacy_type == 'OF')
					$for_yourself_option_attempted = $accessing_pharmacy_stats['options_attempts']['Yourself'];
				else
					$for_yourself_option_attempted = $accessing_pharmacy_stats['options_attempts']['For yourself'];
					
				$for_yourself_eval_percentage = number_format(($for_yourself_option_attempted / $total_survey_attempt) * 100, 2);
			?>
            
            <p><?php echo $for_yourself_eval_percentage?>% of patients accessing the pharmacy for themselves</p>
            
        </div>
        
        <div class="page_2_left_table_container">
        	<div class="demographic_data_container">
            	<table cellpadding="5" cellspacing="0" width="100%" border="1" bordercolor="#CCCCCC" style="font-size:14px;" >
                    <tr>
                        <td bgcolor="#233D88" style="color:#FFF">The dates between which the survey was undertaken</td>
                        <td bgcolor="#233D88" style="color:#FFF">Number of survey responses received</td>
                     </tr>
                     <tr>
                     	<td>
							<?php echo filter_uk_date($pharmacy_survey_details['created_date'])?> - 
                        
                            <?php 
                                if(strtotime($survey_question_stats['closing_comments_arr']['created_date']) > strtotime($pharmacy_survey_details['survey_finish_date'])){
                                    echo filter_uk_date($pharmacy_survey_details['survey_finish_date']);
                                }else{
                                    echo filter_uk_date($survey_question_stats['closing_comments_arr']['created_date']);
                                }
                            ?>
                        </td>
                        <td><?php echo $total_submitted_surveys?></td>
                     </tr>
               	</table>
                <div class="footer_container">
		                <img src="<?php echo IMG?>logo.png" width="234px" />
                        <p class="footer_note">Powered by surveyfocus.co.uk</p>
                </div>
                
            </div>
        </div>
    </div>
