<?php

	$question_dissatisfied = array();
	$question_satisfied = array();

	if($pharmacy_details['pharmacy_type'] == 'OF'){

		//Question 3a Dissatisfied
		$question_3_opt1 = $this->survey->get_survey_options_stats($survey_id, 3, 9); //Not at all satisfied
		$question_3_opt1_per = ($question_3_opt1/$total_submitted_surveys) * 100;
	
		$question_3_opt2 = $this->survey->get_survey_options_stats($survey_id, 3, 10); //Not very satisfied
		$question_3_opt2_per = ($question_3_opt2/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['3'] = ($question_3_opt1_per + $question_3_opt2_per);
		
		//Question 3a Satisfied
		$question_3_opt4 = $this->survey->get_survey_options_stats($survey_id, 3, 12); //Very Satisfied
		$question_3_opt4_per = ($question_3_opt4/$total_submitted_surveys) * 100;
		
		$question_satisfied['3'] = $question_3_opt4_per;
	
		// ---------------------------------------
		//Question 3b Dissatisfied
		$question_3b_opt2 = $this->survey->get_survey_options_stats($survey_id, 33, 126); //YES
		$question_3b_opt2_per = ($question_3b_opt2/$total_submitted_surveys) * 100;
	
		$question_dissatisfied['33'] = $question_3b_opt2_per;
		
		//Question 3b Satisfied
		$question_3b_opt1 = $this->survey->get_survey_options_stats($survey_id, 33, 127); //NO
		$question_3b_opt1_per = ($question_3b_opt1/$total_submitted_surveys) * 100;
		
		$question_satisfied['33'] = $question_3b_opt1_per;
	
		// ---------------------------------------
		//Question 3c Dissatisfied
		$question_3c_opt1 = $this->survey->get_survey_options_stats($survey_id, 34, 129); //NO
		$question_3c_opt1_per = ($question_3c_opt1/$total_submitted_surveys) * 100;
	
		$question_dissatisfied['34'] = $question_3c_opt1_per;
		
		//Question 3c Satisfied
		$question_3c_opt2 = $this->survey->get_survey_options_stats($survey_id, 34, 128); //YES
		$question_3c_opt2_per = ($question_3c_opt2/$total_submitted_surveys) * 100;
		
		$question_satisfied['34'] = $question_3c_opt2_per;
	
		// ---------------------------------------
		//Question 3d Dissatisfied
		$question_3d_opt1 = $this->survey->get_survey_options_stats($survey_id, 35, 131); //NO
		$question_3d_opt1_per = ($question_3d_opt1/$total_submitted_surveys) * 100;
	
		$question_dissatisfied['35'] = $question_3d_opt1_per;
		
		//Question 3d Satisfied
		$question_3d_opt2 = $this->survey->get_survey_options_stats($survey_id, 35, 130); //YES
		$question_3d_opt2_per = ($question_3d_opt2/$total_submitted_surveys) * 100;
		
		$question_satisfied['35'] = $question_3d_opt2_per;
	
		// ---------------------------------------
		//Question 4a Dissatisfied
		$question_4_opt1 = $this->survey->get_survey_options_stats($survey_id, 14, 13); //Very Poor
		$question_4_opt1_per = ($question_4_opt1/$total_submitted_surveys) * 100;
	
		$question_4_opt2 = $this->survey->get_survey_options_stats($survey_id, 14, 14); //Fairly Poor
		$question_4_opt2_per = ($question_4_opt2/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['14'] = ($question_4_opt1_per + $question_4_opt2_per);
		
		//Question 4a Satisfied
		$question_4_opt3 = $this->survey->get_survey_options_stats($survey_id, 14, 15); //Fairly Good
		$question_4_opt3_per = ($question_4_opt3/$total_submitted_surveys) * 100;
	
		$question_4_opt4 = $this->survey->get_survey_options_stats($survey_id, 14, 16); //Very Good
		$question_4_opt4_per = ($question_4_opt4/$total_submitted_surveys) * 100;
		
		$question_satisfied['14'] = ($question_4_opt3_per + $question_4_opt4_per); 
	
		// ---------------------------------------
		//Question 4b Dissatisfied
		$question_4b_opt1 = $this->survey->get_survey_options_stats($survey_id, 15, 19); //Very Poor
		$question_4b_opt1_per = ($question_4b_opt1/$total_submitted_surveys) * 100;
	
		$question_4b_opt2 = $this->survey->get_survey_options_stats($survey_id, 15, 20); //Fairly Poor
		$question_4b_opt2_per = ($question_4b_opt2/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['15'] = ($question_4b_opt1_per + $question_4b_opt2_per);
		
		//Question 4b Satisfied
		$question_4b_opt3 = $this->survey->get_survey_options_stats($survey_id, 15, 21); //Fairly Good
		$question_4b_opt3_per = ($question_4b_opt3/$total_submitted_surveys) * 100;
	
		$question_4b_opt4 = $this->survey->get_survey_options_stats($survey_id, 15, 22); //Very Good
		$question_4b_opt4_per = ($question_4b_opt4/$total_submitted_surveys) * 100;
		
		$question_satisfied['15'] = ($question_4b_opt3_per + $question_4b_opt4_per); 
	
		// ---------------------------------------
		//Question 4c Dissatisfied
		$question_4c_opt1 = $this->survey->get_survey_options_stats($survey_id, 16, 26); //Very Poor
		$question_4c_opt1_per = ($question_4c_opt1/$total_submitted_surveys) * 100;
	
		$question_4c_opt2 = $this->survey->get_survey_options_stats($survey_id, 16, 27); //Fairly Poor
		$question_4c_opt2_per = ($question_4c_opt2/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['16'] = ($question_4c_opt1_per + $question_4c_opt2_per);
		
		//Question 4c Satisfied
		$question_4c_opt3 = $this->survey->get_survey_options_stats($survey_id, 16, 29); //Fairly Good
		$question_4c_opt3_per = ($question_4c_opt3/$total_submitted_surveys) * 100;
	
		$question_4c_opt4 = $this->survey->get_survey_options_stats($survey_id, 16, 31); //Very Good
		$question_4c_opt4_per = ($question_4c_opt4/$total_submitted_surveys) * 100;
		
		$question_satisfied['16'] = ($question_4c_opt3_per + $question_4c_opt4_per); 
	
		// ---------------------------------------
		//Question 4d Dissatisfied
		$question_4d_opt1 = $this->survey->get_survey_options_stats($survey_id, 17, 34); //Very Poor
		$question_4d_opt1_per = ($question_4d_opt1/$total_submitted_surveys) * 100;
	
		$question_4d_opt2 = $this->survey->get_survey_options_stats($survey_id, 17, 35); //Fairly Poor
		$question_4d_opt2_per = ($question_4d_opt2/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['17'] = ($question_4d_opt1_per + $question_4d_opt2_per);
		
		//Question 4d Satisfied
		$question_4d_opt3 = $this->survey->get_survey_options_stats($survey_id, 17, 36); //Fairly Good
		$question_4d_opt3_per = ($question_4d_opt3/$total_submitted_surveys) * 100;
	
		$question_4d_opt4 = $this->survey->get_survey_options_stats($survey_id, 17, 37); //Very Good
		$question_4d_opt4_per = ($question_4d_opt4/$total_submitted_surveys) * 100;
		
		$question_satisfied['17'] = ($question_4d_opt3_per + $question_4d_opt4_per); 
	
		// ---------------------------------------
		//Question 4e Dissatisfied
		$question_4e_opt1 = $this->survey->get_survey_options_stats($survey_id, 18, 39); //Very Poor
		$question_4e_opt1_per = ($question_4e_opt1/$total_submitted_surveys) * 100;
	
		$question_4e_opt2 = $this->survey->get_survey_options_stats($survey_id, 18, 40); //Fairly Poor
		$question_4e_opt2_per = ($question_4e_opt2/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['18'] = ($question_4e_opt1_per + $question_4e_opt2_per);
		
		//Question 4e Satisfied
		$question_4e_opt3 = $this->survey->get_survey_options_stats($survey_id, 18, 41); //Fairly Good
		$question_4e_opt3_per = ($question_4e_opt3/$total_submitted_surveys) * 100;
	
		$question_4e_opt4 = $this->survey->get_survey_options_stats($survey_id, 18, 42); //Very Good
		$question_4e_opt4_per = ($question_4e_opt4/$total_submitted_surveys) * 100;
		
		$question_satisfied['18'] = ($question_4e_opt3_per + $question_4e_opt4_per); 
		
		// ---------------------------------------
		//Question 4f Dissatisfied
		$question_4f_opt1 = $this->survey->get_survey_options_stats($survey_id, 19, 44); //Very Poor
		$question_4f_opt1_per = ($question_4f_opt1/$total_submitted_surveys) * 100;
	
		$question_4f_opt2 = $this->survey->get_survey_options_stats($survey_id, 19, 45); //Fairly Poor
		$question_4f_opt2_per = ($question_4f_opt2/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['19'] = ($question_4f_opt1_per + $question_4f_opt2_per);
		
		//Question 4f Satisfied
		$question_4f_opt3 = $this->survey->get_survey_options_stats($survey_id, 19, 46); //Fairly Good
		$question_4f_opt3_per = ($question_4f_opt3/$total_submitted_surveys) * 100;
	
		$question_4f_opt4 = $this->survey->get_survey_options_stats($survey_id, 19, 47); //Very Good
		$question_4f_opt4_per = ($question_4f_opt4/$total_submitted_surveys) * 100;
		
		$question_satisfied['19'] = ($question_4f_opt3_per + $question_4f_opt4_per); 
		
		// ---------------------------------------
		//Question 5a Dissatisfied
		$question_5a_opt1 = $this->survey->get_survey_options_stats($survey_id, 20, 49); //Very Poor
		$question_5a_opt1_per = ($question_5a_opt1/$total_submitted_surveys) * 100;
	
		$question_5a_opt2 = $this->survey->get_survey_options_stats($survey_id, 20, 50); //Fairly Poor
		$question_5a_opt2_per = ($question_5a_opt2/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['20'] = ($question_5a_opt1_per + $question_5a_opt2_per);
		
		//Question 5a Satisfied
		$question_5a_opt3 = $this->survey->get_survey_options_stats($survey_id, 20, 51); //Fairly Good
		$question_5a_opt3_per = ($question_5a_opt3/$total_submitted_surveys) * 100;
	
		$question_5a_opt4 = $this->survey->get_survey_options_stats($survey_id, 20, 52); //Very Good
		$question_5a_opt4_per = ($question_5a_opt4/$total_submitted_surveys) * 100;
		
		$question_satisfied['20'] = ($question_5a_opt3_per + $question_5a_opt4_per); 
	
		// ---------------------------------------
		//Question 5b Dissatisfied
		$question_5b_opt1 = $this->survey->get_survey_options_stats($survey_id, 21, 54); //Very Poor
		$question_5b_opt1_per = ($question_5b_opt1/$total_submitted_surveys) * 100;
	
		$question_5b_opt2 = $this->survey->get_survey_options_stats($survey_id, 21, 55); //Fairly Poor
		$question_5b_opt2_per = ($question_5b_opt2/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['21'] = ($question_5b_opt1_per + $question_5b_opt2_per);
		
		//Question 5b Satisfied
		$question_5b_opt3 = $this->survey->get_survey_options_stats($survey_id, 21, 56); //Fairly Good
		$question_5b_opt3_per = ($question_5b_opt3/$total_submitted_surveys) * 100;
	
		$question_5b_opt4 = $this->survey->get_survey_options_stats($survey_id, 21, 57); //Very Good
		$question_5b_opt4_per = ($question_5b_opt4/$total_submitted_surveys) * 100;
		
		$question_satisfied['21'] = ($question_5b_opt3_per + $question_5b_opt4_per); 
	
		// ---------------------------------------
		//Question 5c Dissatisfied
		$question_5c_opt1 = $this->survey->get_survey_options_stats($survey_id, 22, 59); //Very Poor
		$question_5c_opt1_per = ($question_5c_opt1/$total_submitted_surveys) * 100;
	
		$question_5c_opt2 = $this->survey->get_survey_options_stats($survey_id, 22, 60); //Fairly Poor
		$question_5c_opt2_per = ($question_5c_opt2/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['22'] = ($question_5c_opt1_per + $question_5c_opt2_per);
		
		//Question 5c Satisfied
		$question_5c_opt3 = $this->survey->get_survey_options_stats($survey_id, 22, 61); //Fairly Good
		$question_5c_opt3_per = ($question_5c_opt3/$total_submitted_surveys) * 100;
	
		$question_5c_opt4 = $this->survey->get_survey_options_stats($survey_id, 22, 62); //Very Good
		$question_5c_opt4_per = ($question_5c_opt4/$total_submitted_surveys) * 100;
		
		$question_satisfied['22'] = ($question_5c_opt3_per + $question_5c_opt4_per); 
	
		// ---------------------------------------
		//Question 5d Dissatisfied
		$question_5d_opt1 = $this->survey->get_survey_options_stats($survey_id, 23, 64); //Very Poor
		$question_5d_opt1_per = ($question_5d_opt1/$total_submitted_surveys) * 100;
	
		$question_5d_opt2 = $this->survey->get_survey_options_stats($survey_id, 23, 65); //Fairly Poor
		$question_5d_opt2_per = ($question_5d_opt2/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['23'] = ($question_5d_opt1_per + $question_5d_opt2_per);
		
		//Question 5d Satisfied
		$question_5d_opt3 = $this->survey->get_survey_options_stats($survey_id, 23, 66); //Fairly Good
		$question_5d_opt3_per = ($question_5d_opt3/$total_submitted_surveys) * 100;
	
		$question_5d_opt4 = $this->survey->get_survey_options_stats($survey_id, 23, 67); //Very Good
		$question_5d_opt4_per = ($question_5d_opt4/$total_submitted_surveys) * 100;
		
		$question_satisfied['23'] = ($question_5d_opt3_per + $question_5d_opt4_per); 
	
		// ---------------------------------------
		//Question 5e Dissatisfied
		$question_5e_opt1 = $this->survey->get_survey_options_stats($survey_id, 24, 69); //Very Poor
		$question_5e_opt1_per = ($question_5e_opt1/$total_submitted_surveys) * 100;
	
		$question_5e_opt2 = $this->survey->get_survey_options_stats($survey_id, 24, 70); //Fairly Poor
		$question_5e_opt2_per = ($question_5e_opt2/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['24'] = ($question_5e_opt1_per + $question_5e_opt2_per);
		
		//Question 5e Satisfied
		$question_5e_opt3 = $this->survey->get_survey_options_stats($survey_id, 24, 71); //Fairly Good
		$question_5e_opt3_per = ($question_5e_opt3/$total_submitted_surveys) * 100;
	
		$question_5e_opt4 = $this->survey->get_survey_options_stats($survey_id, 24, 72); //Very Good
		$question_5e_opt4_per = ($question_5e_opt4/$total_submitted_surveys) * 100;
		
		$question_satisfied['24'] = ($question_5e_opt3_per + $question_5e_opt4_per); 
	
		// ---------------------------------------
		//Question 5f Dissatisfied
		$question_5f_opt1 = $this->survey->get_survey_options_stats($survey_id, 25, 74); //Very Poor
		$question_5f_opt1_per = ($question_5f_opt1/$total_submitted_surveys) * 100;
	
		$question_5f_opt2 = $this->survey->get_survey_options_stats($survey_id, 25, 75); //Fairly Poor
		$question_5f_opt2_per = ($question_5f_opt2/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['25'] = ($question_5f_opt1_per + $question_5f_opt2_per);
		
		//Question 5f Satisfied
		$question_5f_opt3 = $this->survey->get_survey_options_stats($survey_id, 25, 76); //Fairly Good
		$question_5f_opt3_per = ($question_5f_opt3/$total_submitted_surveys) * 100;
	
		$question_5f_opt4 = $this->survey->get_survey_options_stats($survey_id, 25, 77); //Very Good
		$question_5f_opt4_per = ($question_5f_opt4/$total_submitted_surveys) * 100;
		
		$question_satisfied['25'] = ($question_5f_opt3_per + $question_5f_opt4_per); 
	
		// ---------------------------------------
		//Question 6a Dissatisfied
		$question_6a_opt1 = $this->survey->get_survey_options_stats($survey_id, 26, 79); //Not At All Well
		$question_6a_opt1_per = ($question_6a_opt1/$total_submitted_surveys) * 100;
	
		$question_6a_opt2 = $this->survey->get_survey_options_stats($survey_id, 26, 80); //Fairly Well
		$question_6a_opt2_per = ($question_6a_opt2/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['26'] = ($question_6a_opt1_per + $question_6a_opt2_per);
		
		//Question 6a Satisfied
		$question_6a_opt3 = $this->survey->get_survey_options_stats($survey_id, 26, 81); //Fairly Well
		$question_6a_opt3_per = ($question_6a_opt3/$total_submitted_surveys) * 100;
	
		$question_6a_opt4 = $this->survey->get_survey_options_stats($survey_id, 26, 82); //Very Well
		$question_6a_opt4_per = ($question_6a_opt4/$total_submitted_surveys) * 100;
		
		$question_satisfied['26'] = ($question_6a_opt3_per + $question_6a_opt4_per); 
	
	
		// ---------------------------------------
		//Question 6b Dissatisfied
		$question_6b_opt1 = $this->survey->get_survey_options_stats($survey_id, 27, 84); //Not At All Well
		$question_6b_opt1_per = ($question_6b_opt1/$total_submitted_surveys) * 100;
	
		$question_6b_opt2 = $this->survey->get_survey_options_stats($survey_id, 27, 85); //Fairly Well
		$question_6b_opt2_per = ($question_6b_opt2/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['27'] = ($question_6b_opt1_per + $question_6b_opt2_per) ;
		
		//Question 6b Satisfied
		$question_6b_opt3 = $this->survey->get_survey_options_stats($survey_id, 27, 86); //Fairly Well
		$question_6b_opt3_per = ($question_6b_opt3/$total_submitted_surveys) * 100;
	
		$question_6b_opt4 = $this->survey->get_survey_options_stats($survey_id, 27, 87); //Very Well
		$question_6b_opt4_per = ($question_6b_opt4/$total_submitted_surveys) * 100;
		
		$question_satisfied['27'] = ($question_6b_opt3_per + $question_6b_opt4_per) ; 
	
		// ---------------------------------------
		//Question 6c Dissatisfied
		$question_6c_opt1 = $this->survey->get_survey_options_stats($survey_id, 28, 89); //Not At All Well
		$question_6c_opt1_per = ($question_6c_opt1/$total_submitted_surveys) * 100;
	
		$question_6c_opt2 = $this->survey->get_survey_options_stats($survey_id, 28, 90); //Fairly Well
		$question_6c_opt2_per = ($question_6c_opt2/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['28'] = ($question_6c_opt1_per + $question_6c_opt2_per);
		
		//Question 6c Satisfied
		$question_6c_opt3 = $this->survey->get_survey_options_stats($survey_id, 28, 91); //Fairly Well
		$question_6c_opt3_per = ($question_6c_opt3/$total_submitted_surveys) * 100;
	
		$question_6c_opt4 = $this->survey->get_survey_options_stats($survey_id, 28, 92); //Very Well
		$question_6c_opt4_per = ($question_6c_opt4/$total_submitted_surveys) * 100;
		
		$question_satisfied['28'] = ($question_6c_opt3_per + $question_6c_opt4_per); 
		
		// ---------------------------------------
		//Question 6d Dissatisfied
		$question_6d_opt1 = $this->survey->get_survey_options_stats($survey_id, 29, 94); //Not At All Well
		$question_6d_opt1_per = ($question_6d_opt1/$total_submitted_surveys) * 100;
	
		$question_6d_opt2 = $this->survey->get_survey_options_stats($survey_id, 29, 95); //Fairly Well
		$question_6d_opt2_per = ($question_6d_opt2/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['29'] = ($question_6d_opt1_per + $question_6d_opt2_per);
		
		//Question 6d Satisfied
		$question_6d_opt3 = $this->survey->get_survey_options_stats($survey_id, 29, 96); //Fairly Well
		$question_6d_opt3_per = ($question_6d_opt3/$total_submitted_surveys) * 100;
	
		$question_6d_opt4 = $this->survey->get_survey_options_stats($survey_id, 29, 97); //Very Well
		$question_6d_opt4_per = ($question_6d_opt4/$total_submitted_surveys) * 100;
		
		$question_satisfied['29'] = ($question_6d_opt3_per + $question_6d_opt4_per); 
	
		// ---------------------------------------
		//Question 9 Dissatisfied
		$question_9_opt1 = $this->survey->get_survey_options_stats($survey_id, 9, 108); //Poor
		$question_9_opt1_per = ($question_9_opt1/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['9'] = $question_9_opt1_per;
	
		//Question 9 Satisfied
		$question_9_opt3 = $this->survey->get_survey_options_stats($survey_id, 9, 110); //Good
		$question_9_opt3_per = ($question_9_opt3/$total_submitted_surveys) * 100;
	
		$question_9_opt4 = $this->survey->get_survey_options_stats($survey_id, 9, 111); //Very Good
		$question_9_opt4_per = ($question_9_opt4/$total_submitted_surveys) * 100;
	
		$question_9_opt5 = $this->survey->get_survey_options_stats($survey_id, 9, 112); //Excellent
		$question_9_opt5_per = ($question_9_opt5/$total_submitted_surveys) * 100;
		
		$question_satisfied['9'] = ($question_9_opt3_per + $question_9_opt4_per + $question_9_opt5_per); 
		
	}else{

		//Question 2 Dissatisfied
		$question_2_opt3 = $this->survey->get_survey_options_stats($survey_id, 37, 136); //Not Very
		$question_2_opt3_per = ($question_3_opt1/$total_submitted_surveys) * 100;
	
		$question_2_opt4 = $this->survey->get_survey_options_stats($survey_id, 37, 137); //Not at all
		$question_2_opt4_per = ($question_2_opt4/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['37'] = ($question_2_opt3_per + $question_2_opt4_per);
		
		//Question 2 Satisfied
		$question_2_opt1 = $this->survey->get_survey_options_stats($survey_id, 37, 134); //Very 
		$question_2_opt1_per = ($question_2_opt1/$total_submitted_surveys) * 100;

		$question_2_opt2 = $this->survey->get_survey_options_stats($survey_id, 37, 135); //fairly
		$question_2_opt2_per = ($question_2_opt2/$total_submitted_surveys) * 100;
		
		$question_satisfied['37'] = ($question_2_opt1_per + $question_2_opt2_per);

		//Question 3 Dissatisfied
		$question_3_opt3 = $this->survey->get_survey_options_stats($survey_id, 38, 140); //Not Very
		$question_3_opt3_per = ($question_3_opt3/$total_submitted_surveys) * 100;
	
		$question_3_opt4 = $this->survey->get_survey_options_stats($survey_id, 38, 141); //Not at all
		$question_3_opt4_per = ($question_3_opt4/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['38'] = ($question_3_opt3_per + $question_3_opt4_per);
		
		//Question 3 Satisfied
		$question_3_opt1 = $this->survey->get_survey_options_stats($survey_id, 38, 138); //Very 
		$question_3_opt1_per = ($question_3_opt1/$total_submitted_surveys) * 100;

		$question_3_opt2 = $this->survey->get_survey_options_stats($survey_id, 38, 139); //fairly
		$question_3_opt2_per = ($question_3_opt2/$total_submitted_surveys) * 100;
		
		$question_satisfied['38'] = ($question_3_opt1_per + $question_3_opt2_per);

		//Question 4a Dissatisfied
		$question_4a_opt2 = $this->survey->get_survey_options_stats($survey_id, 52, 143); //Very Poor
		$question_4a_opt2_per = ($question_4a_opt2/$total_submitted_surveys) * 100;
	
		$question_4a_opt3 = $this->survey->get_survey_options_stats($survey_id, 52, 144); //Fairly Poor
		$question_4a_opt3_per = ($question_4a_opt3/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['52'] = ($question_4a_opt2_per + $question_4a_opt3_per);
		
		//Question 4a Satisfied
		$question_4a_opt4 = $this->survey->get_survey_options_stats($survey_id, 52, 145); //Fairly Good 
		$question_4a_opt4_per = ($question_4a_opt4/$total_submitted_surveys) * 100;

		$question_4a_opt5 = $this->survey->get_survey_options_stats($survey_id, 52, 146); //Very Good
		$question_4a_opt5_per = ($question_4a_opt5/$total_submitted_surveys) * 100;
		
		$question_satisfied['52'] = ($question_4a_opt4_per + $question_4a_opt5_per);

		//Question 4b Dissatisfied
		$question_4b_opt2 = $this->survey->get_survey_options_stats($survey_id, 53, 148); //Very Poor
		$question_4b_opt2_per = ($question_4b_opt2/$total_submitted_surveys) * 100;
	
		$question_4b_opt3 = $this->survey->get_survey_options_stats($survey_id, 53, 149); //Fairly Poor
		$question_4b_opt3_per = ($question_4b_opt3/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['53'] = ($question_4b_opt2_per + $question_4b_opt3_per);
		
		//Question 4b Satisfied
		$question_4b_opt4 = $this->survey->get_survey_options_stats($survey_id, 53, 150); //Fairly Good 
		$question_4b_opt4_per = ($question_4b_opt4/$total_submitted_surveys) * 100;

		$question_4b_opt5 = $this->survey->get_survey_options_stats($survey_id, 53, 151); //Very Good
		$question_4b_opt5_per = ($question_4b_opt5/$total_submitted_surveys) * 100;
		
		$question_satisfied['53'] = ($question_4b_opt4_per + $question_4b_opt5_per);

		//Question 4c Dissatisfied
		$question_4c_opt2 = $this->survey->get_survey_options_stats($survey_id, 54, 153); //Very Poor
		$question_4c_opt2_per = ($question_4c_opt2/$total_submitted_surveys) * 100;
	
		$question_4c_opt3 = $this->survey->get_survey_options_stats($survey_id, 54, 154); //Fairly Poor
		$question_4c_opt3_per = ($question_4c_opt3/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['54'] = ($question_4c_opt2_per + $question_4c_opt3_per);
		
		//Question 4c Satisfied
		$question_4c_opt4 = $this->survey->get_survey_options_stats($survey_id, 54, 155); //Fairly Good 
		$question_4c_opt4_per = ($question_4c_opt4/$total_submitted_surveys) * 100;

		$question_4c_opt5 = $this->survey->get_survey_options_stats($survey_id, 54, 156); //Very Good
		$question_4c_opt5_per = ($question_4c_opt5/$total_submitted_surveys) * 100;
		
		$question_satisfied['54'] = ($question_4c_opt4_per + $question_4c_opt5_per);

		//Question 4d Dissatisfied
		$question_4d_opt2 = $this->survey->get_survey_options_stats($survey_id, 55, 158); //Very Poor
		$question_4d_opt2_per = ($question_4d_opt2/$total_submitted_surveys) * 100;
	
		$question_4d_opt3 = $this->survey->get_survey_options_stats($survey_id, 55, 159); //Fairly Poor
		$question_4d_opt3_per = ($question_4d_opt3/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['55'] = ($question_4d_opt2_per + $question_4d_opt3_per);
		
		//Question 4d Satisfied
		$question_4d_opt4 = $this->survey->get_survey_options_stats($survey_id, 55, 160); //Fairly Good 
		$question_4d_opt4_per = ($question_4d_opt4/$total_submitted_surveys) * 100;

		$question_4d_opt5 = $this->survey->get_survey_options_stats($survey_id, 55, 161); //Very Good
		$question_4d_opt5_per = ($question_4d_opt5/$total_submitted_surveys) * 100;
		
		$question_satisfied['55'] = ($question_4d_opt4_per + $question_4d_opt5_per);

		//Question 4e Dissatisfied
		$question_4e_opt2 = $this->survey->get_survey_options_stats($survey_id, 56, 163); //Very Poor
		$question_4e_opt2_per = ($question_4e_opt2/$total_submitted_surveys) * 100;
	
		$question_4e_opt3 = $this->survey->get_survey_options_stats($survey_id, 56, 164); //Fairly Poor
		$question_4e_opt3_per = ($question_4e_opt3/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['56'] = ($question_4e_opt2_per + $question_4e_opt3_per);
		
		//Question 4e Satisfied
		$question_4e_opt4 = $this->survey->get_survey_options_stats($survey_id, 56, 165); //Fairly Good 
		$question_4e_opt4_per = ($question_4e_opt4/$total_submitted_surveys) * 100;

		$question_4e_opt5 = $this->survey->get_survey_options_stats($survey_id, 56, 166); //Very Good
		$question_4e_opt5_per = ($question_4e_opt5/$total_submitted_surveys) * 100;
		
		$question_satisfied['56'] = ($question_4e_opt4_per + $question_4e_opt5_per);

		//Question 5 Dissatisfied
		$question_5_opt2 = $this->survey->get_survey_options_stats($survey_id, 40, 201); //Very Poor
		$question_5_opt2_per = ($question_5_opt2/$total_submitted_surveys) * 100;
	
		$question_5_opt3 = $this->survey->get_survey_options_stats($survey_id, 40, 202); //Fairly Poor
		$question_5_opt3_per = ($question_5_opt3/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['40'] = ($question_5_opt2_per + $question_5_opt3_per);
		
		//Question 5 Satisfied
		$question_5_opt4 = $this->survey->get_survey_options_stats($survey_id, 40, 203); //Fairly Good 
		$question_5_opt4_per = ($question_5_opt4/$total_submitted_surveys) * 100;

		$question_5_opt5 = $this->survey->get_survey_options_stats($survey_id, 40, 204); //Very Good
		$question_5_opt5_per = ($question_5_opt5/$total_submitted_surveys) * 100;
		
		$question_satisfied['40'] = ($question_5_opt4_per + $question_5_opt5_per);

		//Question 6a Dissatisfied
		$question_6a_opt2 = $this->survey->get_survey_options_stats($survey_id, 57, 206); //Very Poor
		$question_6a_opt2_per = ($question_6a_opt2/$total_submitted_surveys) * 100;
	
		$question_6a_opt3 = $this->survey->get_survey_options_stats($survey_id, 57, 207); //Fairly Poor
		$question_6a_opt3_per = ($question_6a_opt3/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['57'] = ($question_6a_opt2_per + $question_6a_opt3_per);
		
		//Question 6a Satisfied
		$question_6a_opt4 = $this->survey->get_survey_options_stats($survey_id, 57, 208); //Fairly Good 
		$question_6a_opt4_per = ($question_6a_opt4/$total_submitted_surveys) * 100;

		$question_6a_opt5 = $this->survey->get_survey_options_stats($survey_id, 57, 209); //Very Good
		$question_6a_opt5_per = ($question_6a_opt5/$total_submitted_surveys) * 100;
		
		$question_satisfied['57'] = ($question_6a_opt4_per + $question_6a_opt5_per);

		//Question 6b Dissatisfied
		$question_6b_opt2 = $this->survey->get_survey_options_stats($survey_id, 58, 211); //Very Poor
		$question_6b_opt2_per = ($question_6b_opt2/$total_submitted_surveys) * 100;
	
		$question_6b_opt3 = $this->survey->get_survey_options_stats($survey_id, 58, 212); //Fairly Poor
		$question_6b_opt3_per = ($question_6b_opt3/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['58'] = ($question_6b_opt2_per + $question_6b_opt3_per);
		
		//Question 6b Satisfied
		$question_6b_opt4 = $this->survey->get_survey_options_stats($survey_id, 58, 213); //Fairly Good 
		$question_6b_opt4_per = ($question_6b_opt4/$total_submitted_surveys) * 100;

		$question_6b_opt5 = $this->survey->get_survey_options_stats($survey_id, 58, 214); //Very Good
		$question_6b_opt5_per = ($question_6b_opt5/$total_submitted_surveys) * 100;
		
		$question_satisfied['58'] = ($question_6b_opt4_per + $question_6b_opt5_per);

		//Question 6c Dissatisfied
		$question_6c_opt2 = $this->survey->get_survey_options_stats($survey_id, 59, 216); //Very Poor
		$question_6c_opt2_per = ($question_6c_opt2/$total_submitted_surveys) * 100;
	
		$question_6c_opt3 = $this->survey->get_survey_options_stats($survey_id, 59, 217); //Fairly Poor
		$question_6c_opt3_per = ($question_6c_opt3/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['59'] = ($question_6c_opt2_per + $question_6c_opt3_per);
		
		//Question 6c Satisfied
		$question_6c_opt4 = $this->survey->get_survey_options_stats($survey_id, 59, 218); //Fairly Good 
		$question_6c_opt4_per = ($question_6c_opt4/$total_submitted_surveys) * 100;

		$question_6c_opt5 = $this->survey->get_survey_options_stats($survey_id, 59, 219); //Very Good
		$question_6c_opt5_per = ($question_6c_opt5/$total_submitted_surveys) * 100;
		
		$question_satisfied['59'] = ($question_6c_opt4_per + $question_6c_opt5_per);

		//Question 6d Dissatisfied
		$question_6d_opt2 = $this->survey->get_survey_options_stats($survey_id, 60, 221); //Very Poor
		$question_6d_opt2_per = ($question_6d_opt2/$total_submitted_surveys) * 100;
	
		$question_6d_opt3 = $this->survey->get_survey_options_stats($survey_id, 60, 222); //Fairly Poor
		$question_6d_opt3_per = ($question_6d_opt3/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['60'] = ($question_6d_opt2_per + $question_6d_opt3_per);
		
		//Question 6d Satisfied
		$question_6d_opt4 = $this->survey->get_survey_options_stats($survey_id, 60, 223); //Fairly Good 
		$question_6d_opt4_per = ($question_6d_opt4/$total_submitted_surveys) * 100;

		$question_6d_opt5 = $this->survey->get_survey_options_stats($survey_id, 60, 224); //Very Good
		$question_6d_opt5_per = ($question_6d_opt5/$total_submitted_surveys) * 100;
		
		$question_satisfied['60'] = ($question_6d_opt4_per + $question_6d_opt5_per);

		//Question 9 Dissatisfied
		$question_9_opt4 = $this->survey->get_survey_options_stats($survey_id, 44, 179); //Fair
		$question_9_opt4_per = ($question_9_opt4/$total_submitted_surveys) * 100;
	
		$question_9_opt5 = $this->survey->get_survey_options_stats($survey_id, 44, 180); //Poor
		$question_9_opt5_per = ($question_9_opt5/$total_submitted_surveys) * 100;
		
		$question_dissatisfied['44'] = ($question_9_opt4_per + $question_9_opt5_per);
		
		//Question 9 Satisfied
		$question_9_opt1 = $this->survey->get_survey_options_stats($survey_id, 44, 176); //Excellent
		$question_9_opt1_per = ($question_9_opt1/$total_submitted_surveys) * 100;

		$question_9_opt2 = $this->survey->get_survey_options_stats($survey_id, 44, 177); //Very Good
		$question_9_opt2_per = ($question_9_opt2/$total_submitted_surveys) * 100;

		$question_9_opt3 = $this->survey->get_survey_options_stats($survey_id, 44, 178); //Very Good
		$question_9_opt3_per = ($question_9_opt3/$total_submitted_surveys) * 100;

		$question_satisfied['44'] = ($question_9_opt1_per + $question_9_opt2_per + $question_9_opt3_per);

	}//end if($pharmacy_details['pharmacy_type'] == 'OF')

	arsort($question_dissatisfied);
	arsort($question_satisfied);
	
?>

<style>
	.main_container{
		width:100%; 
		font-family:Arial, Helvetica, sans-serif; 
		font-size:12px;
	}

	.question_list_container {
		width:100%; 
		float:left;
	}
	
	.question_cell{
		width:50%; 
		float:left;
		padding: 5px 5px;
	}
	
	.option_cell{
		width:12%; 
		float:left;
		padding: 5px 0;
		text-align:center;
	}

	.question_list {
		width:100%; 
		float:left;
	}

	.stats_table{
		font-family:Arial, Helvetica, sans-serif; 
		font-size:12px;
	}

	.stats_table_comments{
		
		font-size:14px;
	}

	.stats_table_notes{
		
		margin: 10px 0;
	}
	
	.stats_cell{
		padding:5px 0;
	}

	.stats_comments_cell{
		padding:5px 4px;
	}
	
	.svg_container{
		width:60%	
	}

</style>

<div class="main_container">
	<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
    <div style="text-align:center">
        <h1 style="color:#000; font-size:35px">
            Community Pharmacy Patient<br /> 
            Questionnaire (CPPQ) <br />Report <?php echo ltrim(filter_string($pharmacy_survey_details['survey_title']),'Survey ')?>
        </h1>
        <br />
        <h1>For</h1>
    </div>
 <br />
    <div style="text-align:center">
        <h1><?php echo $pharmacy_details['pharmacy_name']  ?></h1>
        <h2>
            <?php echo filter_string($pharmacy_details['address']).', ';?> 
            <?php echo (filter_string($pharmacy_details['address_2'])) ? filter_string($pharmacy_details['address_2']).', ' : '';?> 
            <?php echo filter_string($pharmacy_details['postcode']);?><br /> 
            <?php echo filter_string($pharmacy_details['county']);?>
        </h2>
    </div>
    
    <div style="margin-top:20px">
    	<table cellpadding="0" cellspacing="0" width="100%">
        	<tr>
            	<td align="center">
                	<a href="http://digitalhealthgroup.co.uk/"><img src="<?php echo IMAGES?>dhg.png" width="100px" style="margin-right:10px;" border="0" /></a>
                	<a href="http://www.nhs.uk/pages/home.aspx"><img src="<?php echo IMAGES?>nhs.png" width="100px" style="margin-right:10px;" border="0" /></a><br />
                    <a href="http://surveyfocus.co.uk"><img src="http://surveyfocus.co.uk/assets/img/logo.png" width="230px" /></a>
                </td>
                
            </tr>
        </table>
    </div>    



<pagebreak [ P ] />

	<h2 align="center">Summary of the information recorded in the report.</h2>
    <p>Summary of the information recorded below. The CCG or successor organisation may want to see this on monitoring visits or at other times (it can be paper based or electronic) (the ranking is the order from 1 to 21, of the percentage responses, as either satisfied or dissatisfied with 1 being the most satisfied or the most dissatisfied as appropriate)</p>
    
    <div class="question_list_container">
    	<div class="question_cell"><strong>Question</strong></div> 
        <div class="option_cell"><strong>Dissatisfied</strong></div> 
        <div class="option_cell"><strong>Ranking</strong></div> 
        <div class="option_cell"><strong>Satisfied</strong></div> 
        <div class="option_cell"><strong>Ranking</strong></div> 
		<?php
			if($pharmacy_details['pharmacy_type'] == 'OF'){

				$ranking_question_exclude = array(1,2,7,8,10,11,12,13);
				
				foreach($question_satisfied as $index => $question_id){
	
					$question_arr = $survey_question_stats[$index];
					
					if($question_arr['parent_id'] != 0){
						
						$parent_id = $question_arr['parent_id'];
						$sub_question_arr = $survey_question_stats[$parent_id];	
						
						$question_str = 'Question '.$sub_question_arr['children'][$index]['question_code'].': '.$sub_question_arr['children'][$index]['parent_question'].' <strong><i>(Option: '.$sub_question_arr['children'][$index]['question'].')</i></strong>';
	
						$dissatisfied_percentage = $question_dissatisfied[$index];
						$dissastisfied_rank = (array_search($index, array_keys($question_dissatisfied)))+1;
	
						$satisfied_percentage = $question_satisfied[$index];
						$satisfied_rank = (array_search($index, array_keys($question_satisfied)))+1;
						
					}else{
						$question_str = 'Question '.$question_arr['question_code'].': '.$question_arr['question'].' <i>'.filter_string($question_arr['sub_notes']).'</i>';
	
						$dissatisfied_percentage = $question_dissatisfied[$index];
						$dissastisfied_rank = (array_search($index, array_keys($question_dissatisfied)))+1;
	
						$satisfied_percentage = $question_satisfied[$index];
						$satisfied_rank = (array_search($index, array_keys($question_satisfied)))+1;
						
					}//end if($question_arr['parent_id'] != 0)
	
				?>
						<div class="question_cell"><?php echo filter_string($question_str)?></div> 
						<div class="option_cell"><?php echo number_format($dissatisfied_percentage)?></div> 
						<div class="option_cell"><?php echo $dissastisfied_rank?></div> 
						<div class="option_cell"><?php echo number_format($satisfied_percentage)?></div> 
						<div class="option_cell"><?php echo $satisfied_rank?></div> 
				<?php
					
				}//end for($i=0;$i<count($question_dissatisfied);$i++)
				
			}else{

				$ranking_question_exclude = array(36,42,43,45,46,47,48,49,50,51);
				
				//print_this($question_satisfied); exit;
				
				foreach($question_satisfied as $index => $question_id){
					
					$question_arr = $survey_question_stats[$index];
					
					if($question_arr['parent_id'] != 0){
						
						$parent_id = $question_arr['parent_id'];
						$sub_question_arr = $survey_question_stats[$question_arr['parent_id']];	
						
						$question_str = 'Question '.$sub_question_arr['children'][$index]['question_code'].': '.$sub_question_arr['children'][$index]['parent_question'].' <strong><i>(Option: '.$sub_question_arr['children'][$index]['question'].')</i></strong>';
	
						$dissatisfied_percentage = $question_dissatisfied[$index];
						$dissastisfied_rank = (array_search($index, array_keys($question_dissatisfied)))+1;
	
						$satisfied_percentage = $question_satisfied[$index];
						$satisfied_rank = (array_search($index, array_keys($question_satisfied)))+1;
						
					}else{
						
						$question_str = 'Question '.$question_arr['question_code'].': '.$question_arr['question'].' <i>'.filter_string($question_arr['sub_notes']).'</i>';
	
						$dissatisfied_percentage = $question_dissatisfied[$index];
						$dissastisfied_rank = (array_search($index, array_keys($question_dissatisfied)))+1;
	
						$satisfied_percentage = $question_satisfied[$index];
						$satisfied_rank = (array_search($index, array_keys($question_satisfied)))+1;
						
					}//end if($question_arr['parent_id'] != 0)
	
				?>
						<div class="question_cell"><?php echo filter_string($question_str)?></div> 
						<div class="option_cell"><?php echo number_format($dissatisfied_percentage)?></div> 
						<div class="option_cell"><?php echo $dissastisfied_rank?></div> 
						<div class="option_cell"><?php echo number_format($satisfied_percentage)?></div> 
						<div class="option_cell"><?php echo $satisfied_rank?></div> 
				<?php
					
				}//end for($i=0;$i<count($question_dissatisfied);$i++)
				
			}//end if($pharmacy_details['pharmacy_type'] == 'OF')

        ?>

    </div>
</div>

<pagebreak [ P ]/>
<div class="main_container">
	
    <?php
		$c = 0;
		
		foreach($survey_question_stats as $p_question_id => $question_arr){
			
			//$question_closing_comments = get_survey_question_closing_comments($this->session->p_id, $survey_id, $p_question_id);
			
			if($question_arr['parent_id'] == 0 && !$question_arr['children']){
				//QUESTIONS WITH NO PARENTS AND HAVE NO CHILDRRENS
	?>
                <h2 align="center"> Community Pharmacy Patient Questionnaire Survey <br /> Results for <?php echo $this->session->pharmacy_name ?></h2>
                
                <table cellpadding="0" cellspacing="0" width="100%" border="0" class="stats_table stats_table_comments"  align="center">
                    <tr>
                        <td align="left"><strong> Question <?php echo filter_string($question_arr['question_code'])?>: <?php echo filter_string($question_arr['question'])?></strong> <i><?php echo filter_string($question_arr['sub_notes']);?></i></td>
                    </tr>
                </table>
                
               <br /><br />

                <table cellpadding="0" cellspacing="0" width="60%" border="0" class="stats_table"  align="center" style="border:solid 1px #CCC">
                    <tr><td><?php echo filter_string($question_arr['closing_question_comments']['svg_code'])?></td></tr>
                </table>
                
                <br /><br />
                
                <div align="center">    
                    <table cellpadding="0" cellspacing="0" width="70%" class="stats_table"  align="center" style="border: solid 1px #CCC">
                        <tr>
                        	<?php 
								if($question_arr['question_id'] != 10 && $question_arr['question_id'] != 45){
									
									foreach($question_arr['options_attempts'] as $option_title => $option_value){
										
										$eval_percentage = number_format( ($option_value / $question_arr['total_survey_attempt']) * 100, 2);
							?>
                                        <td class="stats_cell" style="border-right: solid 1px #ccc" align="center">
                                            <?php echo wordwrap(filter_string($option_title), 30, "<br />\n"); ?>  
                                            <br /> <strong style="font-size:18px"><?php echo filter_percentage($eval_percentage)?>%</strong>
                                        </td>
                            <?php			
										
									}//end foreach($question_arr['options_attempts'] as $option_title => $option_value)
									
								}else{
									
									$eval_commented_percentage = number_format( ($question_arr['total_commented'] / $question_arr['total_survey_attempt']) * 100, 2);
									$eval_non_commented_percentage = number_format( ($question_arr['total_non_commented'] / $question_arr['total_survey_attempt']) * 100, 2);									
							?>
                                    <td class="stats_cell" align="center" style="border-right: solid 1px #ccc;">
										Total Commented 
                                        <br /> <strong style="font-size:18px; border-right: solid 1px #ccc;"><?php echo filter_percentage($eval_commented_percentage)?>%</strong>
                                    </td>                            
                                    <td class="stats_cell" align="center" style="border-right: solid 1px #ccc;">
										Total Non Commented 
                                        <br /> <strong style="font-size:18px;"><?php echo filter_percentage($eval_non_commented_percentage)?>%</strong>
                                    </td>                            

                            <?php		
								}//end if($question_arr['question_id'] != 10)
							?>
                    </table>  
                    <br /><br />
                    <?php 
						if($p_question_id == 1){
					?>
                        <table cellpadding="0" cellspacing="0" width="100%" border="0" class="stats_table stats_table_comments"  align="center" style="border:solid 1px #ccc">

                            <tr>
                                <td class="stats_cell" style="border-right: solid 1px #ccc; border-bottom: solid 1px #ccc;" align="center"><strong>No</strong></td>
                                <td class="padding:5px 0" style="border-right: solid 1px #ccc; border-bottom: solid 1px #ccc;" align="center"><strong>Other Reasons</strong></td>
                            </tr>
                        
                        	<?php 
								for($i=0;$i<count($question_arr['other_reasons_txt_arr']);$i++){
							?>
                                    <tr>
                                        <td class="stats_comments_cell" style="border-right: solid 1px #ccc; border-bottom: solid 1px #ccc;" align="center"><?php echo ($i+1);?></td>
                                        <td class="stats_comments_cell" style="border-right: solid 1px #ccc; border-bottom: solid 1px #ccc;"><?php echo filter_string($question_arr['other_reasons_txt_arr'][$i]['option_txt'])?></td>
                                    </tr>
                            <?php		
								}//end for($i=0;$i<count($question_arr['other_reasons_txt_arr']);$i++)
							?>
                        </table>
                    <?php		
						}elseif($p_question_id == 10 || $p_question_id == 45){
					?>
                            <table cellpadding="0" cellspacing="0" width="100%" border="0" class="stats_table stats_table_comments" align="center" style="border:solid 1px #ccc">
        
                                <tr>
                                    <td class="stats_cell" align="center" style="border-bottom: solid 1px #ccc; border-right: solid 1px #ccc;"><strong>No</strong></td>
                                    <td class="padding:5px 0" align="center" style="border-bottom: solid 1px #ccc; border-right: solid 1px #ccc;"><strong>Comments</strong></td>
                                </tr>
                            
                                <?php 
                                    for($i=0;$i<count($question_arr['comments']);$i++){
                                ?>
                                        <tr>
                                            <td class="stats_comments_cell" style="border-bottom: solid 1px #ccc; border-right: solid 1px #ccc;" align="center"><?php echo ($i+1);?></td>
                                            <td class="stats_comments_cell" style="border-bottom: solid 1px #ccc; border-right: solid 1px #ccc;"><?php echo filter_string($question_arr['comments'][$i]['option_txt'])?></td>
                                        </tr>
                                <?php		
                                    }//end for($i=0;$i<count($question_arr['comments']);$i++)
                                ?>
                            </table>
                    <?php		
						}//end if($p_question_id == 1)
					?>
                    
                    <br />
                    <table cellpadding="0" cellspacing="0" width="100%" border="0" class="stats_table stats_table_comments stats_table_notes"  align="center">
                    	<tr>
                        	<td>
                                <strong>Comments:</strong> <br />
                                <?php echo filter_string($question_arr['closing_question_comments']['comments'])?>
                            </td>
                        </tr>
                    </table>
                </div>
    <?php			
				echo '<pagebreak [ P ]/>' ;
			
			}elseif($question_arr['parent_id'] == 0 && $question_arr['children']){

				//QUESTIONS WITH CHILDRENS/ SUB QUESTION
				foreach($question_arr['children'] as $sub_q_id => $sub_q_arr){
	?>
                    <h2 align="center"> Community Pharmacy Patient Questionnaire Survey <br /> Results for <?php echo $this->session->pharmacy_name ?></h2>
                    <table cellpadding="0" cellspacing="0" width="100%" border="0" class="stats_table stats_table_comments"  align="center">
                        <tr>
                            <td align="left">
                                <strong>Question <?php echo filter_string($sub_q_arr['question_code'])?>:</strong>  <?php echo filter_string($question_arr['question'])?>
                                <br />
                                <strong><i>(Option: <?php echo filter_string($sub_q_arr['question'])?>)</i></strong>                    
                            </td>
                        </tr>
                    </table>
                   <br /><br />
    
                    <table cellpadding="0" cellspacing="0" width="60%" border="0" class="stats_table"  align="center" style="border:solid 1px #CCC">
                        <tr><td><?php echo filter_string($sub_q_arr['closing_question_comments']['svg_code']);?></td></tr>
                    </table>
                    <br /><br />
                    	
					<div align="center">   
						<table cellpadding="0" cellspacing="0" width="70%" border="0" class="stats_table"  align="center" style="border:solid 1px #ccc">
                        	<tr>
								<?php
                                    foreach($sub_q_arr['options_attempts'] as $sub_option_title => $sub_option_value){
                                         $eval_percentage = number_format( ($sub_option_value / $sub_q_arr['total_options_attempted']) * 100, 2);
                                ?>
                                        <td class="stats_cell" style="border-right: solid 1px #ccc" align="center">
                                            <?php echo filter_string($sub_option_title)?>  <br /> 
                                            <strong style="font-size:18px"><?php echo filter_percentage($eval_percentage)?>%</strong>                                
                                        </td>
                                <?php								 
                                    }//end foreach($sub_q_arr['options_attempts'] as $sub_option_title => $sub_option_value)
                                ?>
	                        </tr>
                        </table>

                        <br />
                        <table cellpadding="0" cellspacing="0" width="100%" border="0" class="stats_table stats_table_comments stats_table_notes"  align="center">
                            <tr>
                                <td>
                                    <strong>Comments:</strong> <br />
                                    <?php echo filter_string($sub_q_arr['closing_question_comments']['comments'])?>
                                </td>
                            </tr>
                        </table>
                 </div>
    <?php					
					echo '<pagebreak [ P ]/>' ;
				}//end foreach($question_arr['children'] as $sub_q_id => $sub_q_arr)
				
			}//end if($question_arr['parent_id'] == 0 && !$question_arr['children'])
			
			//echo '<br><br><br>';
			
			$c++;
		}//end foreach($survey_question_stats as $p_question_id => $question_arr)

		$sequence_arr = array('First area', 'Second area', 'Third area');
		
		$well_performed_str = '<table width="100%">';
		//print_this($well_performed_arr); exit;
		for($i=0;$i<count($well_performed_arr);$i++){
			
			$survey_question_stats = get_survey_question_stats($this->session->p_id, $survey_id, $pharmacy_survey_details['survey_ref_no'], $well_performed_arr[$i]['question_id'],$pharmacy_details['pharmacy_type']);
	
			$question_txt = ($survey_question_stats['parent_question']) ? filter_string($survey_question_stats['parent_question']).' <br><b>('.filter_string($survey_question_stats['question']) .') </b>' : filter_string($survey_question_stats['question']);
			
			$well_performed_str .= "<tr><td><h4>".$sequence_arr[$i]." in which the pharmacy performed well:</h4></td></tr>";
			
			if($well_performed_arr[$i]['question_id'] != 10 && $well_performed_arr[$i]['question_id'] != 45){
				
				$well_performed_str .= '<tr><td><p>'.$question_txt.'</p><br><p><table cellpadding="0" cellspacing="0" width="100%" class="stats_table"  align="center" style="border: solid 1px #CCC"><tr>';
					
				foreach($survey_question_stats['options_attempts'] as $options => $stats){
					
					$eval_percentage = number_format( ($stats / $survey_question_stats['total_survey_attempt']) * 100, 2);
					
					$well_performed_str .= '<td class="stats_cell" align="center" style="border-right: solid 1px #ccc;">
											'.$options.' <br> <strong style="font-size:14px;">'.filter_percentage($eval_percentage).'%</strong>
											</td>';
				}//end foreach($survey_question_stats['options_attempts'] as $options => $stats)
				
				$well_performed_str .= '</tr></table></p></td></tr>';
				$well_performed_str .= '<tr><td><p><strong>Comments:</strong> '.$well_performed_arr[$i]['well_performed_comments'].'</p></td></tr>';
				
			}else{
				
				$well_performed_str .= '<tr><td><p>'.filter_string($survey_question_stats['question']).'</p><br><p><table cellpadding="0" cellspacing="0" width="100%" class="stats_table"  align="center" style="border: solid 1px #CCC"><tr>';
	
	
				$eval_commented_percentage = number_format( ($survey_question_stats['total_commented'] / $survey_question_stats['total_survey_attempt']) * 100, 2);
				$eval_non_commented_percentage = number_format( ($survey_question_stats['total_non_commented'] / $survey_question_stats['total_survey_attempt']) * 100, 2);									
						
				
				$well_performed_str .= '<td class="stats_cell" align="center" style="border-right: solid 1px #ccc;">
											Total Commented  <br> <strong style="font-size:14px;">'.$eval_commented_percentage.'%</strong>
											</td><td class="stats_cell" align="center" style="border-right: solid 1px #ccc;">
											Total Non Commented <br> <strong style="font-size:14px;">'.filter_percentage($eval_non_commented_percentage).'%</strong>
											</td>';
											
				$well_performed_str .= '</tr></table></p></td></tr>';
				$well_performed_str .= '<tr><td><p><strong>Comments:</strong> '.$well_performed_arr[$i]['well_performed_comments'].'</p></td></tr>';
				
			}//end if($well_performed_arr[$i]['question_id'] != 10)
			
		}//end for($i=0;$i<count($well_performed_arr);$i++)
		$well_performed_str .= '</table>';
		
		$bad_performed_str = '<table width="100%">';
		for($i=0;$i<count($bad_performed_arr);$i++){
			
			$survey_question_stats = get_survey_question_stats($this->session->p_id, $survey_id, $pharmacy_survey_details['survey_ref_no'], $bad_performed_arr[$i]['question_id'], $pharmacy_details['pharmacy_type']);
			
			//print_this($survey_question_stats);
			
			$question_txt = ($survey_question_stats['parent_question']) ? filter_string($survey_question_stats['parent_question']).' <br><b>('.filter_string($survey_question_stats['question']) .') </b>' : filter_string($survey_question_stats['question']);
			
			$bad_performed_str .= "<tr><td><strong>".$sequence_arr[$i]." in which the pharmacy needs improvement:</strong></td></tr>";
			
			if($bad_performed_arr[$i]['question_id'] != 10 && $bad_performed_arr[$i]['question_id'] != 45){
				
				$bad_performed_str .= '<tr><td><p>'.$question_txt.'</p><br><p><table cellpadding="0" cellspacing="0" width="100%" class="stats_table"  align="center" style="border: solid 1px #CCC"><tr>';
					
				foreach($survey_question_stats['options_attempts'] as $options => $stats){
					
					$eval_percentage = number_format( ($stats / $survey_question_stats['total_survey_attempt']) * 100, 2);
					
					$bad_performed_str .= '<td class="stats_cell" align="center" style="border-right: solid 1px #ccc;">
											'.$options.' <br> <strong style="font-size:14px;">'.filter_percentage($eval_percentage).'%</strong>
											</td>';
				}//end foreach($survey_question_stats['options_attempts'] as $options => $stats)
				
				$bad_performed_str .= '</tr></table></p></td></tr>';
				$bad_performed_str .= '<tr><td><p><strong>Comments:</strong> '.$bad_performed_arr[$i]['bad_performed_comments'].'</p></td></tr>';
				
			}else{
				
				$bad_performed_str = '<tr><td><p>'.filter_string($survey_question_stats['question']).'</p><br><p><table cellpadding="0" cellspacing="0" width="100%" class="stats_table"  align="center" style="border: solid 1px #CCC"><tr>';
	
				$eval_commented_percentage = number_format( ($survey_question_stats['total_commented'] / $survey_question_stats['total_survey_attempt']) * 100, 2);
				$eval_non_commented_percentage = number_format( ($survey_question_stats['total_non_commented'] / $survey_question_stats['total_survey_attempt']) * 100, 2);									
						
				
				$bad_performed_str .= '<td class="stats_cell" align="center" style="border-right: solid 1px #ccc;">
											Total Commented  <br> <strong style="font-size:14px;">'.$eval_commented_percentage.'%</strong>
											</td><td class="stats_cell" align="center" style="border-right: solid 1px #ccc;">
											Total Non Commented <br> <strong style="font-size:14px;">'.filter_percentage($eval_non_commented_percentage).'%</strong>
											</td>';
											
				$bad_performed_str .= '</tr></table></p></td></tr>';
				$bad_performed_str .= '<tr><td><p><strong>Comments:</strong> '.$bad_performed_arr[$i]['bad_performed_comments'].'</p></td></tr>';
				
			}//end if($bad_performed_str[$i]['question_id'] != 10)
			
		}//end for($i=0;$i<count($bad_performed_str);$i++)
		$bad_performed_str .= '</table>';
		
    ?>
    
    <table cellpadding="5" cellspacing="12" width="100%" style="font-family:Arial, Helvetica, sans-serif; font-size:12px">
    
        <tr><td style="border-bottom: solid 1px #999"><h3>Areas where the pharmacy is performing strongly</h3></td></tr>
        <tr><td><?php echo $well_performed_str; ?></td></tr>
    </table>
    <?php 		echo '<pagebreak [ P ]/>';?>    
    <table cellpadding="5" cellspacing="12" width="100%" style="font-family:Arial, Helvetica, sans-serif; font-size:12px">
        <tr><td style="border-bottom: solid 1px #999"> <h3>Area identified that needs improvement</h3></td></tr>
        <tr><td><?php echo $bad_performed_str?></td></tr>
        
    </table>    
</div>