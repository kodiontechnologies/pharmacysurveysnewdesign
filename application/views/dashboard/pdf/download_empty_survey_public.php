<style>
	.main_container{
		width:100%; 
		font-family:Arial, Helvetica, sans-serif; 
		font-size:12px;
	}
	.question_list {
		width:100%; 
		float:left;
	}
	
	.option_container{
		width:100%; float:left; 
		margin:8px 0;
	}
	.options{
		float:left; 
		width:18%;
	}
	
</style>

    <div class="main_container">
    	 <div class="question_list" align="center" style="padding-bottom:20px;"><h2>COMMUNITY PHARMACY PATIENT QUESTIONNAIRE</h2></div>
         <?php 
		 	if($pharmacy_type == 'OF'){
		?>
        		<div class="question_list" align="center"><h3>This section is about why you visited the pharmacy today</h3></div>
        <?php		
			}//end if($pharmacy_type == 'OF')
		 ?>
         
        <?php 
            foreach($questionnnair_arr as $question_id => $question_list){
				
				echo ($question_id == 7) ? '<pagebreak [ P ]/>' : '';
                
                $choosen_answers = get_survey_submitted_answers($submitted_surveys_list[$i]['id'],$question_id);
                
                if($question_id == 4){
        ?>
                 <div class="question_list" align="center"><h3>This section is about the pharmacy and the staff who work there more generally, not just for today's visit</h3></div>
        <?php		
                }elseif($question_id == 11){
        ?>		
                    <div class="question_list" align="center"><h3>These last few questions are just to help us categorise your answers</h3></div>
        <?php
                }//end if($question_id == 4)
        ?>
                    <div class="question_list"><strong>Q<?php echo filter_string($question_list['question_code'])?> - <?php echo filter_string($question_list['question']);?> <i><?php echo filter_string($question_list['sub_notes']);?></i></strong></div>
                
        <?php
                 if($question_id != 10 && $question_id != 45){
                     
                     if(count($question_list['sub_question']) == 0){
        ?>	
                    <div class="option_container">
        <?php				 
                         foreach($question_list['options'] as $option_id => $option_list){
							 
                             $width_style = 'width:18%;';
                             if($question_id == 11)
                                $width_style = 'width:10%';
                            elseif($question_id == 8 || $question_id == 13 || $question_id == 2 || $question_id == 36 || $question_id == 43 || $question_id == 51)	
                                $width_style = 'width:30%';
								
        ?>
                            <div class="options" style="<?php echo $width_style ?>" >
                                <?php 
                                        echo '<img src="'.IMAGES.'empty_checkbox.png" width="15px" /> '.filter_string($option_list);
                                ?>
                            </div>
        <?php					 
                         }//end foreach($question_list['options'] as $option_id => $option_list)
        ?>
                    </div><!--./option_container -->
        <?php				 
                         if($question_id == 1){
        ?>
                             <div class="question_list">
                                 <textarea style="width:100%; height:70px" >&nbsp;</textarea>
                             </div>
        <?php					 
                         }//end if($question_id == 1)
                         
                     }else{
                         
                          foreach($question_list['sub_question'] as $subquestion_id =>$subquestion_list){
        ?>
                                <div class="question_list" style="margin:10px 0">
                                 <strong><?php echo filter_string($subquestion_list['question_code'])?> - <?php echo filter_string($subquestion_list['question']);?></strong>
                             </div>
        <?php					  
                             echo '<div class="option_container">';
                             foreach($subquestion_list['options'] as $suboption_id => $suboption_list){

        ?>
                                <div class="options">
                                    <?php
                                          echo '<img src="'.IMAGES.'empty_checkbox.png" width="15px" /> '.filter_string($suboption_list);
                                    ?>      
                                </div>
        <?php						 
                             }//end foreach($subquestion_list['options'] as $suboption_id => $suboption_list)
                             
                             echo '</div> <!--./option_container -->';
                          }//end foreach($question_list['sub_question'] as $subquestion_id =>$subquestion_list)
                         
                     }//end if(count($question_list['sub_question']) == 0)
                     
                 }else{
        ?>
                        <div class="question_list">
                            <textarea style="width:100%; height:70px" >&nbsp;</textarea>
                        </div>
        <?php			 
                 }//end if($question_id !=10)
               
            }//end foreach($questionnnair_arr as $question_id => $question_list)
        ?>
    </div>
