<style>
	div{
		font-family:Arial, Helvetica, sans-serif;
		font-size:12px;	
		color: #233D88;
	}
	
	table{
		font-family:Arial, Helvetica, sans-serif;
		font-size:12px;	
	}
	
	#age_stats td{
		padding:8px;	
	}
	
	.container{
		float:left; 
		border: solid 2px #233D88; 
		width:100%;
	}
	
	.top_heading {
		background-color:#233D88; 
		width:100%; 
		float:left; 
		color:#FFF; 
		padding:0 10px;	
	}
	
	.top_heading h1{
		text-align:center; 
		font-size:36px; 
		font-weight:bold;	
	}
	
	.pdf_body {
		padding: 10px;
		float:left;
		width:100%;
		text-align:center;	
	}
	
	.chart_heading {
		float:left; 
		font-weight:bold; 
		font-size:36px; 
		text-align:center;
	}
	
	.chart_heading span {
		font-size:48px;
	}
	
	.chart {
		float:left;
		width:100%;	
	}

	.page_1_left {
		float:left; 
		width:48%; 
		border: solid 0px #000;
		text-align:left;
		padding:5px;
		font-size:18px;
	}
	
	.page_1_right {
		float:left; width:48%; 
		border: solid 0px #000;
		text-align:left;
		padding:5px;
		font-size:16px;
	}
	
	.page_1_left h1, .page_1_right h1 {
		font-size:18px;	
	}
	
	.note_heading{
		float:left; 
		width:100%; 
		font-size:18px;
		font-weight:bold;
	}
	.note_sub_heading{
		float:left; 
		width:100%; 
		color:#000;
		padding-bottom: 10px;
	}
	
</style>
<div class="container">
	
    <div class="top_heading">
    	<h1>
        	Patient Satisfaction Survey Results <br />
            Speeds Pharmacy
        </h1>
    </div>
    
    <div class="pdf_body">
    	<div class="chart_heading">
            <span>95%</span> of our <br />
            customers rate our services as <br />
            very good or excellent
         </div>

        <div class="chart">

			<?php 
			
                $svg_image =  filter_string($survey_question_stats['closing_comments_arr']['svg_poster_code']);
                $svg_image = str_replace('<rect x="0" y="0" width="1000" height="800" stroke="none" stroke-width="0" fill="#ffffff"></rect>','',$svg_image);
                $svg_image = str_replace('<rect x="0" y="0" width="1138" height="900" stroke="none" stroke-width="0" fill="#ffffff"></rect>','',$svg_image);
                $svg_image = str_replace('<rect x="678" y="20" width="243" height="16" stroke="none" stroke-width="0" fill-opacity="0" fill="#ffffff"></rect>','',$svg_image);
                
                echo $svg_image = str_replace('10 0 750 500','100 0 750 500',$svg_image);
				
            ?>
        </div>
        
        <div class="page_1_left">
        	<h1>Where did we do best?</h1>
            <p>Over 98% of customers rated our pharmacy very good in:</p>
            <ul style="padding:10px;">
            	<li>Answering your queries</li>
                <li>Service received from pharmacist and Pharmacy staff</li>
                <li>Providing an efficient service</li>
            </ul>
            
        </div>
        <div class="page_1_right">
        	<h1>Where can we improve?</h1>
            <p>Over 90% of our customers have never been given advice about stopping smoking, healthy eating or physical exercise</p>
            <p>Action: We aim to help improve your health by promoting more health and wellbeing services.  </p>
            
        </div>
        
        <div class="note_heading">We continue to strive to improve our service to you.</div>
        <div class="note_sub_heading">Patient Satisfaction Survey undertaken during January - March 2015 in a sample of 150 patients visiting Speeds Pharmacy.</div>
        
        <div class="" style="width:15%; float:left; padding:10px">
        	<img src="<?php echo IMAGES?>dhg.png" width="100px" />
        </div>
        <div class="" style="width:60%; float:left; margin:10px; 0px">
        	<table cellpadding="0" cellspacing="0" width="100%" border="1" bordercolor="#CCCCCC" id="age_stats">
            	<tr>
                	<td bgcolor="#233D88" style="color:#FFF">Age</td>
                    <td>16-19</td>
                    <td>20-24</td>
                    <td>25-34</td>
                    <td>35-44</td>
                    <td>45-54</td>
                    <td>55-64</td>
                    <td> >65</td>
                </tr>
            	<tr>
                	<td bgcolor="#233D88" style="color:#FFF">Percentage</td>
                    <td>1%</td>
                    <td>2%</td>
                    <td>25%</td>
                    <td>35%</td>
                    <td>45%</td>
                    <td>55%</td>
                    <td>65%</td>
                </tr>
            </table>
            <p>Male respondents (39%), Female (61%)</p>
            <p>63% of patients accessing the pharmacy for themselves</p>
        </div>
        <div class="" style="width:15%; float:left; padding:10px">
        	<img src="<?php echo IMAGES?>dhg.png" width="100px" />
        </div>
        
        
    </div>
</div>