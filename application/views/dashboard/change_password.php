<main>
  <section class="well5 well6__ins1">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12 wow fadeInDown"> 
          
          <!-- CMS Page title -->
          <h1 class="wow"> Change Password</h1>
          <br />
          <hr />
          <?php 
                if($this->session->flashdata('err_message')){
            ?>
          <div class="alert alert-danger"><?php echo $this->session->flashdata('err_message'); ?></div>
          <?php
                }//end if($this->session->flashdata('err_message'))
                if($this->session->flashdata('ok_message')){
            ?>
          <div class="alert alert-success alert-dismissable"><?php echo $this->session->flashdata('ok_message'); ?></div>
          <?php 
                }//if($this->session->flashdata('ok_message'))
            ?>
          <!-- CMS Page Description -->
          <p><?php echo (filter_string($page_data['page_description'])) ? filter_string($page_data['page_description']) : '' ; ?><br />
          </p>
        </div>
        <div class="col-md-2 col-sm-3 col-xs-3 "> </div>
        <div class="col-md-8 col-sm-6 col-xs-6 wow fadeInUp">
        
        	<form enctype="application/x-www-form-urlencoded" action="<?php echo SURL;?>dashboard/change-password-process" class="form_validate" method="post" name="registration" id="registration">
            
            <div class="form-group has-feedback">
              <label for="old_password" class="form-label">Old Password<span class="required">*</span></label>
              <input type="password" class="form-control" name="old_password" id="old_password" required="required" placeholder="Enter your OLD password">
              <div class="help-block with-errors"></div>
            </div>
            
            <div class="form-group has-feedback">
              <label for="password" class="form-label">New Password<span class="required">*</span></label>
              <input type="password" class="form-control" name="password" id="password" required="required" placeholder="Enter your NEW password">
              <div class="help-block with-errors"></div>
            </div>
            
            <div class="form-group has-feedback">
              <label for="conf_password" class="form-label">Confirm Password<span class="required">*</span></label>
              <input type="password" class="form-control" name="conf_password" id="conf_password" required="required" placeholder="Re-enter your NEW password">
              <div class="help-block with-errors"></div>
            </div>
            
            <div class="text-center">
              <button type="submit"  id="chnage_pass_btn" name="chnage_pass_btn" class="btn btn-success pull-right">Update Password</button>
            </div>
          </form>
          
          
        </div>
        <div class="col-md-2 col-sm-3 col-xs-3 "> </div>
      </div>
    </div>
  </section>
</main>
