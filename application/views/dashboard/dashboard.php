<?php 
	//All Pharmacy Survey List of Pharamcy
	$pharmacy_survey_list = get_pharmacy_survey_list($this->session->p_id);
	$tab = $this->input->get('t');

?>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<div class="module">
  <div class="stripe-4">
    <div class="container">
      <div class="row">
        <h2>Dashboard</h2>
        <ul class="breadcrumbs">
            <li><a href="<?php echo SURL?>">Home</a></li>
            <li> > </li>
            <li class="active">Dashboard</li>
        </ul>
      </div>
     </div>
    </div>
</div>


<div class="dashboard_main">
  <div class="container">
  
    <div class="row for_row_escap">
		  <?php
            if($this->session->flashdata('err_message')){
        ?>
                <div class="alert alert-danger hide_alert"><?php echo $this->session->flashdata('err_message'); ?></div>
          <?php
            }//end if($this->session->flashdata('err_message'))
            
            if($this->session->flashdata('ok_message')){
        ?>
                <div class="alert alert-success alert-dismissable hide_alert"><?php echo $this->session->flashdata('ok_message'); ?></div>
          <?php 
            }//if($this->session->flashdata('ok_message'))
        ?>
        <div class="col-md-12"><h1><?php echo $this->session->pharmacy_name;?></h1></div>
       
      <ul class="nav nav-tabs">
      
        <li class="<?php echo ($tab == 1 || !$tab)? 'active' : ''?>"><a href="#current_survey_tab" data-toggle="tab">Current Survey</a></li>
        <li class="<?php echo ($tab == 2)? 'active' : ''?>"><a href="#survey_form_tab" data-toggle="tab">Survey Form</a></li>
        <li class="<?php echo ($tab == 3)? 'active' : ''?>"><a href="#results_tab" data-toggle="tab">Results</a></li>
        <li class="<?php echo ($tab == 4)? 'active' : ''?>"><a href="#archives_tab" data-toggle="tab">Archives</a></li>
        <li class="<?php echo ($tab == 5)? 'active' : ''?>"><a href="#settings_tab" data-toggle="tab">Edit Profile</a></li>
        <li class="<?php echo ($tab == 6)? 'active' : ''?>"><a href="#payment_tab" data-toggle="tab">Payment History</a></li>
        <li class="<?php echo ($tab == 7)? 'active' : ''?>"><a href="#incomplete_survey_tab" data-toggle="tab">Incomplete Surveys </a></li>
        <li class="<?php echo ($tab == 8)? 'active' : ''?>"><a href="#help_videos" data-toggle="tab">Help Videos</a></li>

      </ul>
      <div class="tab-content">
        <div id="current_survey_tab" class="tab-pane <?php echo ($tab == 1 || !$tab)? 'active' : ''?>"> 

			<?php 
                if(!$pharmacy_current_survey){
                    
                    //No Survey is currently started. Showing the statistics of the previous servey.
                    
                    //Now check if there is anyy most rcent survey, its tenure of survey period is over or not. If Not then show message
                    $get_most_recent_survey = get_most_recent_survey($this->session->p_id);
					
                    if(filter_string($get_most_recent_survey['survey_start_date']) == NULL ){
                        $show_new_survey = 1;

                    }else{
                        
                        $recent_survey_end_date = $get_most_recent_survey['survey_finish_date'];
                        
                        if($recent_survey_end_date >= date('Y-m-d')){
                            //Survey is complete but not yet expired
                            $show_new_survey = 0;
                        }else{
                            //No Survy available, No Expired, No Purchased. Go for buying new Servey
                            $show_new_survey = 2;
                        }
                    }//end if(filter_string($get_most_recent_survey['survey_start_date']) == NULL )

                    if($show_new_survey == 1){
						
                        //No Survey is available
            ?>
                        <p class="current_survey_notes"><i class="fa fa-info-circle"></i> Start your survey for the year <?php echo ltrim($most_recent_survey['survey_title'],'Survey ')?></p>
                          <p>All pharmacies are required to carry out an annual community pharmacy patient questionnaire (CCPQ). The minimum number of returned surveys required for analysis for the year is proportional to dispensing volume. Make a selection from the table below according to your dispensing volume. </p>
                          
                          <form action="<?php echo SURL?>dashboard/start-survey" method="post" name="start_survey_frm" id="start_survey_frm" enctype="multipart/form-data">
                              <table class="table table-condensed">
                                <thead>
                                  <tr>
                                    <th>Average monthly script volume (itmes)</th>
                                    <th>Minimum number of returned surveys</th>
                                    <th>Select</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  
                                  <tr>
                                    <td>0 - 2000</td>
                                    <td>50</td>
                                    <td>
                                        <div class="radio radio-success radio-circle no-margins">
                                            <input type="radio" value="0 - 2000|50" id="opt_1" name="radio_no_of_survey" class="no-margins">
                                            <label for="opt_1">&nbsp;</label>
                                        </div>                                        
                                     </td>
                                  </tr>
                                  <tr>
                                    <td>2001 - 4000</td>
                                    <td>75</td>
                                    <td>
                                        <div class="radio radio-success radio-circle no-margins">
                                            <input type="radio" value="2001 - 4000|75" id="opt_2" name="radio_no_of_survey" class="no-margins">
                                            <label for="opt_2">&nbsp;</label>
                                        </div>                                            
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>4001 - 6000</td>
                                    <td>100</td>
                                    <td>
                                        <div class="radio radio-success radio-circle no-margins">
                                            <input type="radio" value="4001 - 6000|100" id="opt_3" name="radio_no_of_survey" class="no-margins">
                                            <label for="opt_3">&nbsp;</label>
                                        </div>                                            
                                     </td>
                                  </tr>
                                  <tr>
                                    <td>6001 - 8000</td>
                                    <td>125</td>
                                    <td>
                                        <div class="radio radio-success radio-circle no-margins">
                                            <input type="radio" value="6001 - 8000|125" id="opt_4" name="radio_no_of_survey" class="no-margins">
                                            <label for="opt_4">&nbsp;</label>
                                        </div>                                            
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>8001 - upwards</td>
                                    <td>150</td>
                                    <td>
                                                                            
                                        <div class="radio radio-success radio-circle">
                                            <input type="radio" value="8001 - upwards|150" id="opt_5" name="radio_no_of_survey" class="no-margins">
                                            <label for="opt_5">&nbsp;</label>
                                        </div>                                            
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                              <div class="text-right">
                              	<?php
									$last_pharmacy_survey_arr = get_pharmacy_survey_list($this->session->p_id, '3', '', '');
									
									$last_pharmacy_survey = $last_pharmacy_survey_arr[0];
									if($last_pharmacy_survey['survey_finish_date'] < date('Y-m-d')){
										
										//Current date is less than the last survey survey finish date. Can start the survey.
											
								?>
                                
                                        <button class="btn btn-success no-margins" type="button" name="start_survey_btn" id="start_survey_btn" value="survey start"> Start <?php echo ltrim($pharmacy_current_survey['survey_title'],'Survey ')?> Survey Now</button>
                                        <input type="hidden" name="survey_id" value="<?php echo filter_string($get_most_recent_survey['id'])?>" readonly="readonly" />
                                        <p id="option_error" class="text-danger"></p>    
                                
                                <?php
										
									}else{
										
										//Previous survey was closed earlier so cannot start the survey before. The previous survey finish date has not reach. So sop
								?>
                                		<a href="#survey_start_error" class="btn btn-success no-margins fancy_dialogue" type="button"> Start <?php echo ltrim($pharmacy_current_survey['survey_title'],'Survey ')?> Survey Now</a>
                                        
                                        <div id="survey_start_error" style="display:none; min-width: 480px;">
                                          <div class="color-line"></div>
                                          <div class="modal-header text-left">
                                            <h4 class="modal-title">Start Survey</h4>
                                          </div>
                                          <div class="modal-body">
                                            <div class="row">
                                              <div class="col-md-12">
                                              	<p style="font-size:16px">
                                                	The survey for the year <strong><?php echo ltrim($most_recent_survey['survey_title'],'Survey ')?></strong> cannot be started before 31<sup>st</sup> March.
                                                </p>
                                              </div>
                                            </div>
                                          </div>
                                          
                                          <div class="modal-footer"> 
                                                <button type="button" class="btn btn-danger fancy_close no-margins" onClick="jQuery.fancybox.close();"> Close </button>
                                           </div>
                                        </div>
                                
                                <?php										
									}//end if($last_pharmacy_survey['survey_finish_date'] <= date('Y-m-d'))
                                ?>
                            </div>
                          </form>
            <?php		
                    }elseif($show_new_survey == 0){
                        
                        //Survey is there but not yet expired.
                        
                        $min_no_of_surveys = filter_string($get_most_recent_survey['min_no_of_surveys']);
                        $total_submitted_surveys = get_survey_submitted_count($this->session->p_id, $get_most_recent_survey['id']);
                        
                        $remaining_surveys = $min_no_of_surveys - $total_submitted_surveys;
                        $remaining_surveys = ($remaining_surveys < 0) ? 0 : $remaining_surveys;

                        $total_submitted_online_surveys = get_survey_submitted_count($this->session->p_id, $get_most_recent_survey['id'],'ONLINE');
                        $total_submitted_paper_surveys = get_survey_submitted_count($this->session->p_id, $get_most_recent_survey['id'],'PAPER');
                        
                        $survey_chart_data = array();
                        
                        $survey_chart_data[0][0] = 'Survey Stats';
                        $survey_chart_data[0][1] = 'Hours per Day';
                        
                        $survey_chart_data[1][0] = 'Remaining';
                        $survey_chart_data[1][1] =  (int) ($remaining_surveys);
                        
                        $survey_chart_data[2][0] = 'Paper Based';
                        $survey_chart_data[2][1] = (int) $total_submitted_paper_surveys;
                        
                        $survey_chart_data[3][0]= 'Online';
                        $survey_chart_data[3][1] = (int) $total_submitted_online_surveys;
                        
                        // print_this($survey_chart_data); exit;
                        
                        $survey_arr_json = json_encode($survey_chart_data);									

						//Survey is not yet started means we have to start the survey of the current one.
						$current_date_2 = date('m/d');	
						$survey_start_date_2 = date('Y-m-d');
						
						$SURVEY_END_MONTH = 'SURVEY_END_MONTH';
						$survey_end_global_value = get_global_settings($SURVEY_END_MONTH); //Set from the Global Settings
						$next_survey_end_date_2 = filter_string($survey_end_global_value['setting_value']);
						
						$next_survey_end_2 = strtotime("$next_survey_end_date +1 year");	
						$survey_session_2 = (date('Y', $next_survey_end_2)-1).'-'.date('y', $next_survey_end_2);
				?>
           
                    <div style="overflow:auto;">
                        <div class="col-md-12">
                            <p class="current_survey_notes">
                            <i class="fa fa-info-circle"></i> 
                            Purchase your new survey for the year <?php echo $survey_session_2?>
                            <a href="<?php echo SURL?>dashboard/buy-survey-p" class="btn btn-sm btn-warning fancy_ajax_close_btn fancybox.ajax" style="margin-top:0px;" >Purchase Now</a>
                            </p>
                        
                        </div>
                        <div class="col-md-6">
                          <div class="contact_content_border">
                            <div class="module2">
                              <div class="stripe-5">
                                <h4> <?php echo filter_string($get_most_recent_survey['survey_title'])?> Statistics </h4>
                              </div>
                            </div>
                            <div class="contact_content_border2">
                            
                            <div class="alert bg-success m-t-lg">
                                <button type="button" class="close"><?php echo filter_string($get_most_recent_survey['min_no_of_surveys'])?></button>
                                The minimum number of returned surveys required
                            </div>

                            <div class="alert bg-warning m-t-lg">
                                <button type="button" class="close"><?php echo $total_submitted_online_surveys?></button>
                                Total completed surveys online
                            </div>

                            <div class="alert bg-danger m-t-lg">
                                <button type="button" class="close"><?php echo $total_submitted_paper_surveys?></button>
                                Total completed paper based surveys 
                            </div>
                            <!--
                            <div class="alert bg-info m-t-lg">
                                <button type="button" class="close">
                                    <?php 
                                        $remaining_surveys = filter_string($get_most_recent_survey['min_no_of_surveys']) - $total_submitted_surveys;
                                        echo $remaining_surveys = ($remaining_surveys < 0) ? 0 : $remaining_surveys;
                                    ?>
                                </button>
                                Remaining surveys
                                
                            </div>
                            -->
                            <div class="alert m-t-lg">
                                <button type="button" class="close">
                                    <?php echo $total_completed_survey = $total_submitted_paper_surveys + $total_submitted_online_surveys;?>
                                </button>
                                Total surveys completed so far
                            </div>
                            
                              <div class="clearfix"></div>
                              <div style="margin-top:1px;"></div>
                              
                            </div>
                            <div class="clearfix"></div>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="contact_content_border">
                            <div class="module2">
                              <div class="stripe-5">
                                <h4> Graphical Representation </h4>
                              </div>
                            </div>
                            <div class="contact_content_border2">
                                <script type="text/javascript">
                                    jQuery(document).ready(function(){
                                          google.charts.load("current", {packages:["corechart"]});
                                          google.charts.setOnLoadCallback(current_survey_initialize);
                                    });
                                </script>
                                <div id="survey_piechart_3d" style="width: 100%; height: 350px;"></div>
                                <textarea id="survey_arr_json" name="survey_arr_json" class="hidden" readonly="readonly"><?php echo $survey_arr_json?></textarea>
                                <input type="hidden" readonly="readonly" name="current_survey_title" id="current_survey_title" value="Current <?php echo filter_string($pharmacy_current_survey['survey_title'])?> Statistics" />
                                <a href="#" id="current_survey_chart" class="hidden"></a>
                              
                            </div>
                            <div class="clearfix"></div>
                          </div>
                        </div>
                      
                    </div>

            <?php 
                    } elseif ($show_new_survey == 2) { 
            ?>
                         
		                <!-- Survey BuyNow -->
                        <form id="credit_card_frm" action="<?php echo base_url(); ?>dashboard/buy-survey" method="post">
                          <div class="row">
                          	<div class="contact_content_border2">
                            	<p class="current_survey_notes"><i class="fa fa-info-circle"></i> Your survey has been expired. Please buy your survey for year <?php echo ltrim($survey_session,'Survey ')?></p>
                                <h4>Select Payment Method</h4>
                                  <p>Please select any of the payment method to buy a new survey.</p>
                                  
                                  <div class="col-md-8">
                                    <div class="payments_tabs">
                                      <div class="row for_row_escap">
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a href="#card_tab" data-toggle="tab"><img src="<?php echo IMAGES?>visa_mastercard_logo.png" width="99px" /></a></li>
                                            <li><a href="#paypal_tab" data-toggle="tab"><img src="<?php echo IMAGES?>payal-logo.png" width="120px" /></a></li>
                                        </ul>
                                        <div class="tab-content">
                                          <div id="card_tab" class="tab-pane fade in active">
                                            <p>Please enter your credit card details below.</p>
                                            <div class="single-field half-field ful_field form-group">
                                                <label for="card number" style="float: left; width: 200px">Card Number</label>
                                                <input name="card" id="card" type="text" placeholder="Card Number" required>
            
                                            </div>
                                            <div class="row">
                                              <div class="col-md-4">
                                                <div class="single-field half-field ful_field form-group">
                                                  <label for="expMonth">Expiry Month</label>
                                                  <select class="form-control" name="expMonth" id="expMonth" required>
                                                        <option value="">Month</option>
                                                        <?php 
                                                            for($i=1;$i<=12;$i++){
                                                        ?>
                                                                <option value="<?php echo ($i<10) ? '0'.$i : $i?>"><?php echo ($i<10) ? '0'.$i : $i?></option>
                                                        <?php
                                                            }//end for($i=date('Y');$i<=date('Y')+10;$i++)
                                                        ?>
                                                      </select>
                                                </div>
                                              </div>
                                              <div class="col-md-4">
                                                <div class="single-field half-field ful_field form-group">
                                                  <label>&nbsp;</label>
                                                  <select class="form-control" name="expYear" id="expYear" required>
                                                    <option value="">Year</option>
                                                    <?php 
                                                        for($i=date('Y');$i<=date('Y')+10;$i++){
                                                    ?>
                                                            <option value="<?php echo $i?>"><?php echo $i?></option>
                                                    <?php
                                                        }//end for($i=date('Y');$i<=date('Y')+10;$i++)
                                                    ?>
                                                  </select>
                                                </div>
                                              </div>
                                              <div class="col-md-4">
                                                <div class="single-field half-field ful_field form-group">
                                                  <label>CVV</label>
                                                  <input class="" name="cvc" id="cvc" type="text" placeholder="CVV" required>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                          <div id="paypal_tab" class="tab-pane fade">
                                            <p>You can make payment using your PayPal account. </p>
                                            <div class="row">
                                              <div class="col-md-5">
                                                <h5 class="paypal_accepts">Paypal accepts: </h5>
                                              </div>
                                              <div class="col-md-5"> <img src="<?php echo IMAGES?>paypal_cards.jpg" alt=""> </div>
                                            </div>
                                            <div class="clearfix"></div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <div class="text-right"> 
            
                                        <button name="add_pharamcy_btn" onClick="validate_credit_card_form();" type="button" id="save_btn" value="Make Payment" class="btn btn-success no-margins">Make Payment</button>
            
                                        <a href="#credit_card_confirm_popup" id="confirm_popup_trigger" style="text-decoration:none" class="fancy_dialogue_payment hidden">Make Payment</a>
                                        
                                        <div id="credit_card_confirm_popup" style="display:none; min-width: 480px;">
                                            <div class="color-line"></div>
                                            <div class="modal-body">
                                              <div class="modal-header text-left">
                                                  <h4 class="modal-title" id="payment_popup_message">Please wait while processing your payment.</h4>
                                             </div>
                                            </div>
                                            
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-md-4 padd_right_0">
                                    <div class="contact_content_border order_suumry">
                                      <div class="module2">
                                        <div class="stripe-5">
                                          <h4> Order Summary</h4>
                                        </div>
                                      </div>
                                      <div class="contact_content_border2">
                                        <div class="box1">
                                          <div style="height: 5px"></div>
                                          <div class="a">
                                            <div class="col-md-8"><strong>Survey <?php echo ($survey_session) ? $survey_session : '' ; ?></strong></div>
                                            <div class="col-md-4"></div>
                                          </div>
                                          <div class="clearfix"></div>
                                          <hr style="margin:10px">
                                          <div class="a">
                                            <div class="col-md-8">Sub Total</div>
                                            <div class="col-md-4">&pound;<?php echo ($sub_total) ? filter_price($sub_total) : '' ; ?></div>
                                          </div>
                                          <div class="clearfix"></div>
                                          <hr style="margin:10px">
                                          <div class="a">
                                            <div class="col-md-8">VAT (<?php echo ($vat) ? $vat : '' ; ?>%)</div>
                                            <div class="col-md-4">&pound;<?php echo ($vat_amount) ? filter_price($vat_amount) : '' ; ?></div>
                                          </div>
                                          <div class="clearfix"></div>
                                          <hr style="margin:10px">
                                          <div class="a">
                                            <div class="col-md-8"><strong>GRAND TOTAL</strong></div>
                                            <div class="col-md-4"><strong>&pound;<?php echo ($grand_total) ? filter_price($grand_total) : '' ; ?></strong></div>
                                          </div>
                                          <div class="clearfix"></div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  
                            </div>
                          </div>
                          <input type="hidden" name="payment_method_radio" id="payment_method_radio" value="credit_card" />
                        </form>                
            <?php
                        //No Survey available ask for repayment
                        
                    }//end if($show_new_survey == 1)
                }else{
                    
                    //Survey already in process

                    $min_no_of_surveys = filter_string($pharmacy_current_survey['min_no_of_surveys']);
                    $remaining_surveys = $min_no_of_surveys - $total_submitted_surveys;
                    $remaining_surveys = ($remaining_surveys < 0) ? 0 : $remaining_surveys;

                    $survey_chart_data = array();
                
                    $survey_chart_data[0][0] = 'Survey Stats';
                    $survey_chart_data[0][1] = 'Hours per Day';
                    
                    $survey_chart_data[1][0] = 'Remaining';
                    $survey_chart_data[1][1] =  (int) ($remaining_surveys);
                    
                    $survey_chart_data[2][0] = 'Paper Based';
                    $survey_chart_data[2][1] = (int) $total_submitted_paper_surveys;
                
                    $survey_chart_data[3][0]= 'Online';
                    $survey_chart_data[3][1] = (int) $total_submitted_online_surveys;
                    
                    // print_this($survey_chart_data); exit;
                    
                    $survey_arr_json = json_encode($survey_chart_data);
                    
            ?>
                    
                    <div class="row">
                      <div class="contact_content_border2">
                        <div class="current_survey_top">
                          <div class="row">
                            <div class="col-md-12">

                                <?php 
									//Remaing/ required  Survey has been completed. Its time to close the survey if not closed.
                                    if($remaining_surveys > 0){
                                ?>
                                        <p class="current_survey_notes"><i class="fa fa-info-circle"></i> Your survey for the year <?php echo ltrim($pharmacy_current_survey['survey_title'],'Survey ')?> has been started</p>
                                <?php 
                                    }else{
										
                                        if($pharmacy_current_survey['survey_status'] == 0){
											//Survey is not close,d time to close.
                                ?>
                                        <p class="current_survey_notes"><i class="fa fa-info-circle"></i> The minimum number of returned questionnaires required for <?php echo ltrim($pharmacy_current_survey['survey_title'],'Survey ')?> have been successfully completed, now you may <a href="<?php echo SURL?>dashboard/?t=3"><strong style="color:#17D0EF">close the survey</strong></a></p>
                                        
                                        <div id="close_survey_pop" style="display:none; min-width: 480px;">
                                          <div class="color-line"></div>
                                          <div class="modal-header text-left">
                                            <h4 class="modal-title">Close Survey Now</h4>
                                          </div>
                                          <div class="modal-body">
                                            <div class="row">
                                              <div class="col-md-12">
                                              	<p style="font-size:16px">The minimum number of returned questionnaires required for Survey <strong><?php echo ltrim($pharmacy_current_survey['survey_title'],'Survey ')?></strong> have been successfully completed.</p>
                                              </div>
                                            </div>
                                          </div>
                                          
                                          <div class="modal-footer"> 
                                                <a href="<?php echo SURL?>dashboard/?t=3" class="btn btn-success" style="margin:0" data-survey="<?php echo $completed_survey_list[$i]['id']?>"> Yes, Close Survey </a>
                                               </form>
                                           </div>
                                        </div>
                                        
                                        <a href="#close_survey_pop" name="close_fancy_link" id="close_fancy_link" class="fancy_dialogue hidden" >Close Survey</a>
                                        <?php 
											if((!$tab || $tab == '1') && !$this->session->show_close_pop){
										?>
												<script>
                                                    jQuery(window).load(function(){
                                                        $('#close_fancy_link').click();
                                                    })
                                                </script>
                                        <?php 
												$this->session->show_close_pop = 1;
											}//end if(!$tab || $tab == '1')
										?>
                                        
                                <?php
                                        }elseif($pharmacy_current_survey['survey_status'] == 1){
											
											//Survey is closed but not finalized. Time to finalized
                                ?>
                                        <p class="current_survey_notes"><i class="fa fa-info-circle"></i> The survey for the year <?php echo ltrim($pharmacy_current_survey['survey_title'],'Survey ')?> has been closed. In order to finalise the survey you need to <a href="<?php echo SURL?>dashboard/?t=3"><strong style="color:#17D0EF">review the results</strong></a></p>
                                        
                                        <div id="finalize_survey_pop" style="display:none; min-width: 480px;">
                                          <div class="color-line"></div>
                                          <div class="modal-header text-left">
                                            <h4 class="modal-title">Finalise Survey</h4>
                                          </div>
                                          <div class="modal-body">
                                            <div class="row">
                                              <div class="col-md-12">
                                              	<p style="font-size:16px">
                                                	The survey for the year <strong><?php echo ltrim($pharmacy_current_survey['survey_title'],'Survey ')?></strong> has been closed. Now you need to finalise the survey.
                                                </p>
                                              </div>
                                            </div>
                                          </div>
                                          
                                          <div class="modal-footer"> 
                                                <a href="<?php echo SURL?>dashboard/?t=3" class="btn btn-success" style="margin:0" data-survey="<?php echo $completed_survey_list[$i]['id']?>"> Yes, Finalise Now</a>
                                           </div>
                                        </div>
                                        
                                        <a href="#finalize_survey_pop" name="finalize_survey_link" id="finalize_survey_link" class="fancy_dialogue hidden" >Close Survey</a>
                                        <?php 
											if((!$tab || $tab == '1') && !$this->session->show_finalize_pop ){
										?>
												<script>
                                                    jQuery(window).load(function(){
                                                        $('#finalize_survey_link').click();
                                                    })
                                                </script>
                                        <?php 
												$this->session->show_finalize_pop = 1;
											}//end if(!$tab || $tab == '1')
										?>
                                        
                                
                                <?php			
                                        }
                                    }//end if($remaining_surveys > 0)
                                ?>

                            </div>
                            <div class="col-md-6">
                              <div class="contact_content_border">
                                <div class="module2">
                                  <div class="stripe-5">
                                    <h4> Current Survey Statistics </h4>
                                  </div>
                                </div>
                                <div class="contact_content_border2">
                                
                                <div class="alert bg-success m-t-lg">
                                    <button type="button" class="close"><?php echo filter_string($pharmacy_current_survey['min_no_of_surveys'])?></button>
                                    The minimum number of returned surveys required
                                </div>

                                <div class="alert bg-warning m-t-lg">
                                    <button type="button" class="close"><?php echo $total_submitted_online_surveys?></button>
                                    Total completed surveys online
                                </div>

                                <div class="alert bg-danger m-t-lg">
                                    <button type="button" class="close"><?php echo $total_submitted_paper_surveys?></button>
                                    Total completed paper based surveys 
                                </div>
                                
                                <!--
                                <div class="alert bg-info m-t-lg">
                                    <button type="button" class="close">
                                        <?php 
                                            $remaining_surveys = filter_string($pharmacy_current_survey['min_no_of_surveys']) - $total_submitted_surveys;
                                            echo $remaining_surveys = ($remaining_surveys < 0) ? 0 : $remaining_surveys;
                                        ?>
                                    </button>
                                    Remaining surveys
                                </div>
                                -->
                                
                                <div class="alert m-t-lg">
                                    <button type="button" class="close">
                                        <?php echo $total_completed_survey = $total_submitted_paper_surveys + $total_submitted_online_surveys;?>
                                    </button>
                                    Total surveys completed so far
                                </div>
                                
                                  <div class="clearfix"></div>
                                  <div style="margin-top:1px;"></div>
                                  
                                </div>
                                <div class="clearfix"></div>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="contact_content_border">
                                <div class="module2">
                                  <div class="stripe-5">
                                    <h4> Graphical Representation </h4>
                                  </div>
                                </div>
                                <div class="contact_content_border2">
                                    <script type="text/javascript">
                                        jQuery(document).ready(function(){
                                              google.charts.load("current", {packages:["corechart"]});
                                              google.charts.setOnLoadCallback(current_survey_initialize);
                                        });
                                    </script>
                                    <div id="survey_piechart_3d" style="width: 100%; height: 350px;"></div>
                                    <textarea id="survey_arr_json" name="survey_arr_json" class="hidden" readonly="readonly"><?php echo $survey_arr_json?></textarea>
                                    <input type="hidden" readonly="readonly" name="current_survey_title" id="current_survey_title" value="Current <?php echo filter_string($pharmacy_current_survey['survey_title'])?> Statistics" />
                                    <a href="#" id="current_survey_chart" class="hidden"></a>
                                  
                                </div>
                                <div class="clearfix"></div>
                              </div>
                            </div>
                          </div>
                        <?php 
                            if($pharmacy_current_survey['survey_status'] == '0'){
                        ?>
                                <div class="row">
                                    <div class="col-md-6">
                                      <div class="contact_content_border">
                                        <div class="module2">
                                          <div class="stripe-5">
                                            <h4> SMS Survey <span class="pull-right" style="margin-right:15px">Remaining SMS: <?php echo $pharmacy_profile['pharmacy_sms_quota'] - $pharmacy_profile['pharmacy_sms_quota_consumed']; ?></span></h4>
                                            
                                          </div>
                                        </div>
                                        <form id="sms_quota_form" name="sms_quota_form" action="<?php echo base_url(); ?>dashboard/send-sms" method="post">
                                            <div class="contact_content_border2">
                                            <div style="height:2px"></div>
                                              <div class="single-field full-field">
                                                  <!-- SMS Response: Success & Failure messages -->
                                                  <?php if($this->session->flashdata('sms_err_message')){ ?>  
                                                      <div class="alert alert-danger hide_alert"><?php echo $this->session->flashdata('sms_err_message'); ?></div>
                                                  <?php
                                                  }//end if($this->session->flashdata('err_message'))
    
                                                  if($this->session->flashdata('sms_ok_message')){ ?>
                                                      <div class="alert alert-success alert-dismissable hide_alert"><?php echo $this->session->flashdata('sms_ok_message'); ?></div>
                                                  <?php }//if($this->session->flashdata('ok_message')) ?>
                                                <label>Mobile Number (<i><small>No leading 0's or country code</small></i>)</label>
                                                <div class="row">
                                                  <div class="col-md-12">
                                                    
                                                    <div class="form-group">

                                                    <div class="input-group">
                                                     
                                                        <div class="input-group-btn">
                                                            <div class="single-field">
                                                              <select class="input-name" id="select_country_code" name="country_code">
                                                                <option value="+44">+44 (UK)</option>
                                                                <option value="+353">+353 (IRE)</option>          
                                                              </select>
                                                          </div>
                                                        </div><!-- /btn-group -->
                                                        
                                                        <div class="single-field full-field">  
                                                          
                                                          <input type="text" name="mobile_number" id="mobile_number" value="" class="m-t-xs" placeholder="e.g 7788824965" required="required" data-fv-regexp="true" data-fv-regexp-regexp="^(?=.*[0-9])[0-9]{8,}$" data-fv-regexp-message="Please use allowed characters (Numbers) and length should be minimum 8 characters"  maxlength="15" data-fv-message="Please enter the mobile number." >

                                                        </div>

                                                    </div>

                                                      </div>

                                                  </div>
                                                </div>
                                                <div class="single-field full-field form-group">
                                                  <label>Comments</label>
                                                  
                                                  <textarea name="sms_comment" id="sms_comment" maxlength="183" style="height:140px" onKeyup="sms_counter()"  required="required"><?php echo ($sms_body_global_settings) ? filter_string($sms_body_global_settings) : '' ; ?></textarea>
                                                  
                                                  <p><i id="sms_counter_txt">Max 160 characters allowed </i></p>

                                                </div>
                                              </div>
                                              <div class="clearfix"></div>
                                              
                                              <div class="text-right"> 
                                                <div id="comment-submit">
                                                    <button name="submit_sms" type="submit" id="submit_sms" value="Submit" class="btn btn-success" style="margin-top: 0px;"> Send</button>
                                                    <input type="hidden" name="survey_id" value="<?php echo filter_string($pharmacy_current_survey['id'])?>" readonly="readonly" />
                                                    <input type="hidden" name="survey_ref_code" value="<?php echo filter_string($pharmacy_current_survey['survey_ref_no'])?>" readonly="readonly" />
                                                </div>                                              
                                              </div>
                                            </div>
                                        </form>
                                        <div class="clearfix"></div>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="contact_content_border">
                                        <div class="module2">
                                          <div class="stripe-5">
                                            <h4> Email Survey </h4>
                                          </div>
                                        </div>
                                        <form id="send_survey_link_frm" name="send_survey_link_frm" method="post" action="<?php echo SURL ?>dashboard/send-survey-link-process">
                                            
                                            <div style="height:6px"></div>
                                            <div class="contact_content_border2">
                                              <div class="single-field full-field form-group">
                                                  <label>Email (<i><small>For multiple emails use (,) as a seperator</small></i>)</label>
                                                  <input type="text" name="email_addresses"  id="email_addresses" value="" required  class="input-name" autocomplete="off" placeholder="e.g email@yourdomain.com" data-fv-message="Please enter the recipient email address." />
                                              </div>
                                              <div class="single-field full-field form-group" >
                                                <div style="margin-bottom: -6px;">&nbsp;</div>
                                                <label>Message</label>
                                                <textarea name="friend_message" id="friend_message" style="height:140px" required="required" class="textarea-comment"><?php echo filter_string($email_body)?></textarea>
                                                <p><i>&nbsp;</i></p
                                              >
                                              </div>

                                              <div class="clearfix"></div>
                                              
                                              <div class="text-right"> 
                                                <input type="hidden" name="pharmacy_name" id="pharmacy_name"  value="<?php echo $this->session->pharmacy_name?>" readonly="readonly" />
                                                <input type="hidden" name="survey_id" value="<?php echo filter_string($pharmacy_current_survey['id'])?>" readonly="readonly" />
                                                <input type="hidden" name="survey_ref_code" value="<?php echo filter_string($pharmacy_current_survey['survey_ref_no'])?>" readonly="readonly" />
                                                <button class="btn btn-success" name="survey_email_btn" type="submit" id="survey_email_btn" value="Submit" style="margin-top: 0px;"> Send</button>
                                              
                                               </div>
                                            </div>
                                        </form>
                                        
                                        <div class="clearfix"></div>
                                      </div>
                                    </div>
                                  </div>
                                  
                                <div class="row">
                                    <div class="col-md-12">
                                    	<div class="contact_content_border">
                                            <div class="module2">
                                              <div class="stripe-5">
                                                <h4> Share Survey Link</h4>
                                              </div>
                                            </div>
                                            <div class="contact_content_border2">
                                                <div style="height:2px"></div>
                                                <div class="single-field full-field current_survey_notes" style="font-size:14px">Copy the survey link below and share it with your patients.</div>
                                                <div class="single-field full-field form-group" >
	                                                <div class="input-group input-group-lg">
                                                  		<span class="input-group-addon" id="sizing-addon1"><i class="glyphicon glyphicon-link"></i></span>
                                                  		<input type="text" style="color:#333" class="form-control" value="<?php echo $embed_link?>" aria-describedby="sizing-addon1"  onfocus="$(this).select();" onclick="$(this).select();">
                                                	</div>
                                                </div>                                                
                                                
                                                <div class="text-center pull-left btn-block" style="margin-bottom:10px">OR</div>
                                                
                                                <div class="single-field full-field form-group" >
	                                                <div class="input-group input-group-lg">
                                                  		<span class="input-group-addon" id="sizing-addon1"><i class="glyphicon glyphicon-link"></i></span>
                                                  		<input type="text" style="color:#333" class="form-control" value="<?php echo SURL.'online-survey/'.filter_string($this->session->p_id).'/'.filter_string($pharmacy_current_survey['survey_ref_no'])?>/embed" aria-describedby="sizing-addon1"  onfocus="$(this).select();" onclick="$(this).select();">
                                                	</div>
                                                </div>                                                
                                                
                                            </div>
                                        </div>
                                    </div>
                                 </div>
                                 
                                <div class="row">
                                    <div class="col-md-12">
                                    	<div class="contact_content_border">
                                            <div class="module2">
                                              <div class="stripe-5">
                                                <h4> Embed Survey</h4>
                                              </div>
                                            </div>
                                            <div class="contact_content_border2">
                                                <div style="height:2px"></div>
                                                <div class="single-field full-field current_survey_notes" style="font-size:14px">Copy the embed code from below and paste it into your website.</div>
                                                <div class="single-field full-field form-group" >
	                                                <div class="input-group input-group-lg">
                                                  		<span class="input-group-addon" id="sizing-addon1"><i class="glyphicon glyphicon-link"></i></span>
                                                  		<input type="text" style="color:#333" class="form-control" value="<?php echo htmlentities("<iframe src='".$embed_link."' width='100%' height='100%' style='min-height:1500px' frameborder='0'></iframe>")?>" aria-describedby="sizing-addon1"  onfocus="$(this).select();" onclick="$(this).select();">
                                                	</div>
                                                </div>                                                
                                            </div>
                                        </div>
                                    </div>
                                 </div>
                        <?php		
                            }//end if($pharmacy_current_survey['survey_status'] == '0')
                        ?>
                        </div>
                      </div>
                    </div>                        
                    <div class="clearfix"></div>
                    
            <?php		
                }//end if(!$pharmacy_current_survey)
            ?>

        </div>
        
        <div id="survey_form_tab" class="tab-pane <?php echo ($tab == 2)? 'active' : ''?>" style="overflow:auto">
          <div class="">
            <div class="c12 text-right">
                <a style="font-size:16" href="<?php echo SURL?>dashboard/download-survey"><i class="fa fa-print"></i> Print Form</a>
            </div>          
            <div class="module2">
              <div class="stripe-5">
                <h4>COMMUNITY PHARMACY PATIENT QUESTIONNAIRE</h4>
              </div>
            </div>
            <div class="col-md-12">
              <div class="company_detail">
                <p><strong><?php echo $this->session->pharmacy_name;?></strong><br>
					  <?php 
                        $address_2 = ($this->session->address_2) ? $this->session->address_2.', ': '';
                        $phramcy_address = $this->session->address.', '.$address_2.$this->session->town.', '.$this->session->county.', '.$this->session->postcode;
                        echo $phramcy_address;
                      ?>
                 </p>
              </div>
            </div>
            <form action="<?php echo ($pharmacy_current_survey['id'] && $pharmacy_current_survey['survey_status'] == '0' ) ? SURL.'dashboard/submit-paper-survey' : '' ?>"  id="manage_survey_frm" name="manage_survey_frm" method="POST" enctype="multipart/form-data"  class="prevent_enter_key">
            
                <div class="col-md-12 m-t">
                  	<?php 
						if($this->session->pharmacy_type == 'OF'){
					?>
                            <div class="module2">
                                <div class="stripe-5 m-b-sm">
	                                <h4 class="no-margins">This section is about why you visited the pharmacy today</h4>
                                </div>
                            </div>
                    <?php		
						}//end if($this->session->pharmacy_type == 'OF')
					?>
                                            
					<?php 
                        foreach($questionnnair_arr as $question_id => $question_list){
                            
                            if($question_id == 4){
					?>

                                <div class="module2">
                                  <div class="stripe-5 m-b-sm">
                                    <h4 class="no-margins">This section is about the pharmacy and the staff who work there more generally, not just for today's visit</h4>
                                  </div>
                                </div>                    
                    <?php			
							}elseif($question_id == 11){
					?>

                                <div class="module2">
                                  <div class="stripe-5 m-b-sm">
                                    <h4 class="no-margins">These last few questions are just to help us categorise your answers</h4>
                                  </div>
                                </div>   

                    <?php			
							}//end if($question_id == 4)
					?>
                            <div class="row">
                                <div class="col-lg-12">
                                  <div class="panel3 panel-primary">
                                    <div class="panel-heading"> <span class="badge"><strong><?php echo filter_string($question_list['question_code'])?></strong></span> <?php echo filter_string($question_list['question']);?> <i><?php echo filter_string($question_list['sub_notes']);?></i> </div>
                                    <div class="panel-body">
                                    <?php 
                                        if($question_id != 10 && $question_id != 45){
                                            
                                            if(count($question_list['sub_question']) == 0){
                                                
                                                $total_options = count($question_list['options']);
                                                $per_cell = floor(12/$total_options);
                            
                                                foreach($question_list['options'] as $option_id => $option_list){
                                                    
                                                    $per_cell = ($total_options == 2) ? '2' : $per_cell;
                                    ?>
                                                    <div class="col-lg-<?php echo $per_cell;?>" <?php echo ($question_id == 11 || $question_id == 49 ) ? 'style="width: 120px;"' : '' ?>>
                                                        <div class="radio radio-success radio-circle">
                                                            <input class="<?php echo ($question_id == 1 || $question_id == 36) ? 'show_hide_textbox' : ''?>" type="radio" required="required" value="<?php echo filter_string($option_id)?>" id="opt_<?php echo $question_id.'_'.$option_id?>" name="q[<?php echo $question_id?>]"  >
                                                            <label for="opt_<?php echo $question_id.'_'.$option_id?>"><?php echo filter_string($option_list);?></label>
                                                        </div>
                                                    </div>
                                    <?php				
                                                }//end foreach($question_list['options'] as $option_id => $option_list)
                                    ?>
                                                <div class="row hidden" id="err_q<?php echo filter_string($question_id)?>"><div class="col-md-12 alert alert-sm alert-danger">Please select one of the options</div></div>
                                    <?php			
                                                if($question_id == 1 || $question_id == 36){
                                    ?>
                                                <div class="col-lg-12 hidden no-paddings" id="question_10_txtbox">
                                                    <textarea rows="5" style="width:100%; background-color:#fff" id="other_reason_txt" name="other_reason_txt" placeholder="For some other reason (please write in the reason for your visit):" maxlength="250"></textarea>
                                                    <p><i>Max 250 characters allowed </i></p>
                                                </div>
                                    <?php				
                                                }
                                            }else{
                                                
                                                 foreach($question_list['sub_question'] as $subquestion_id =>$subquestion_list){
                                    ?>
                                                    <div class="col-lg-12"><p class="subtitles">
                                                    <strong><?php echo substr(filter_string($subquestion_list['question_code']),1,2);?>) <?php echo filter_string($subquestion_list['question']);?></strong></p></div>
                                    <?php				 
                                                    $total_options = count($subquestion_list['options']);
                                                    $per_cell = floor(12/$total_options);
                                            
                                                    foreach($subquestion_list['options'] as $suboption_id => $suboption_list){
                                                        
                                                        $per_cell = ($total_options == 2) ? '2' : $per_cell;
                                    ?>
                                                        <div class="col-lg-<?php echo $per_cell?>">
                                                            <div class="radio radio-success radio-circle">
                                                                <input required="required" type="radio" value="<?php echo filter_string($suboption_id)?>" id="<?php echo $subquestion_id.'_'.$suboption_id?>" name="q[<?php echo filter_string($subquestion_id)?>]">
                                                                <label for="<?php echo $subquestion_id.'_'.$suboption_id?>">
                                                                   <?php echo $suboption_list?>
                                                                </label>
                                                            </div>                                        
                                                        </div>
                                    <?php					
                                                    }//end foreach($subquestion_list['options'] as $suboption_id => $suboption_list)
                                    ?>
                                                    <div class="row hidden" id="err_q<?php echo filter_string($subquestion_id)?>"><div class="col-md-12 alert alert-sm alert-danger">Please select one of the options</div></div>
                                    <?php				
                                                 }//end foreach($question_list['sub_question'] as $subquestion_id =>$subquestion_list)
                                                
                                            }//end if(count($question_list['sub_question']) == 0)
                                            
                                        }else{
                                    ?>
                                            <div class="col-lg-12 no-paddings">
                                                <textarea rows="5" style="width:100%; background-color:#fff" id="comment" name="q[<?php echo $question_id?>]" placeholder="[Insert here, if required, additional questions relating to healthcare service provision]" maxlength="250" ></textarea>
                                                <p><i>Max 250 characters allowed </i></p>	                            
                                            </div>                    
                                    <?php		
                                        }//end if($question_id !=10)
                                    ?>
                                    </div>
                                  </div>
                                </div>
                              </div>
                    <?php		
						}//end foreach($questionnnair_arr as $question_id => $question_list)
                    ?>		
                    
                </div>
                
                <?php 

					if($pharmacy_current_survey['id'] && $pharmacy_current_survey['survey_status'] == '0'){
						//Show submit only if there is any active survey in progress
				?>
						<div class="row no-margins">
							<div class="col-md-10 text-center">
								<p class="hidden alert alert-danger" id="common_err"><strong>One of the option above is not yet selected. Please select the options from the highlighed sections above.</strong></p>
							</div>
							<div class="col-md-2 text-right no-margins">
                                <button type="button" name="submit_suv_btn" id="submit_suv_btn" value="submit_suv_btn" class="btn btn-success">Submit Survey</button>
                                <input type="hidden" name="pharmacy_id" value="<?php echo $this->session->p_id ?>" readonly="readonly" />
                                <input type="hidden" name="submit_type" value="PAPER" readonly="readonly" />
                                <input type="hidden" name="survey_id" id="survey_id" value="<?php echo filter_string($pharmacy_current_survey['id']) ?>" readonly="readonly" />	
							</div>
						</div>
						<div class="row no-margins">
							<div class="col-md-12">
                            	<div style="border: dashed 1px #ccc" class="m-t p-xs">
                                
                                    <small style="color:#999" class="text-justify">
                                    <p>
                                        <?php 
                                            $SURVEY_FOOTER_TEXT = 'SURVEY_FOOTER_TEXT';
                                            $survey_footer_text = get_global_settings($SURVEY_FOOTER_TEXT); //Set from the Global Settings
                                            $survey_footer_text = nl2br(filter_string($survey_footer_text['setting_value']));
                                            echo str_replace('[PHARMACY_NAME]',$this->session->pharmacy_name,$survey_footer_text);
                                        ?>
                                    </p>
                                    </small>
                                </div>
							</div>
						</div>

				<?php		
					}//end if(!$pharmacy_current_survey['id'])
				?>
            </form>
            <div class="clearfix"></div>
          </div>
        </div>
        <div id="results_tab" class="tab-pane <?php echo ($tab == 3)? 'active' : ''?>">

            <div class="row no-margins">
                <div class="col-md-12 hidden" id="survey_comment"></div>
            </div>
            
           <div class="row no-margins" id="survey_body">

			<?php 

				if(count($pharmacy_survey_list) > 0){
					//Means Pharamcy have one or more surveys in their list
			?>
                    <div class="col-md-4">
                      <div class="contact_content_border">
                        <div class="module2">
                          <div class="stripe-5">
                            <h4>Select Survey</h4>
                          </div>
                        </div>
                        <div class="contact_content_border2">
                          <div class="single-field half-field ful_field">
                                <select class="form-control" id="survey_list" name="survey_list">
                                    <option value="" selected="selected">Select a Survey</option>
                                    <?php 
                                        for($i=0;$i<count($pharmacy_survey_list);$i++){
                                            
                                            if($pharmacy_survey_list[$i]['survey_start_date'] != NULL){
                                    ?>
                                            <option <?php echo (filter_string($pharmacy_current_survey['id']) == filter_string($pharmacy_survey_list[$i]['id'])) ? 'selected="selected"' : '' ?>  value="<?php echo filter_string($pharmacy_survey_list[$i]['id']).'|'.$pharmacy_survey_list[$i]['survey_ref_no'];?>"><?php echo filter_string($pharmacy_survey_list[$i]['survey_title']);?></option>
                                            
                                    <?php
                                            }//end if($pharmacy_survey_list[$i]['start_date'] != NULL)
											
                                        }//end for($i=0;$i<count($pharmacy_survey_list);$i++)
                                    ?>
                                </select>                          
                          </div>
                          
                        <div id="survey_statistics_pane" class="hidden">
                            <div class="row">
                            	<div class="clearfix"></div>                                    
                                <div class="col-md-12 no-paddings">
                                    <div class="module2">
                                        <div class="stripe-5">
                                            <h4>Survey Statistics <!--<span id="s_survey_status_1">In progress</span>--></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>                        
                            <table class="table m-t">
                              <tr>
                                <td><strong>Minimum surveys required</strong></td>
                                <td id="s_min_no_of_surveys_1">0</td>
                              </tr>
                              <tr>
                                <td><strong>Total completed</strong></td>
                                <td id="s_total_survey_completed_1">0</td>
                              </tr>
                              <tr>
                                <td><strong>Completed online</strong></td>
                                <td id="s_total_submitted_online_surveys_1">0</td>
                              </tr>
                              <tr>
                                <td><strong>Completed Paper based</strong></td>
                                <td id="s_total_submitted_paper_surveys_1">0</td>
                              </tr>
                            </table>
                        </div>
                          
                          <div id="current_survey_actions" class="hidden">
								<?php
                                   // if($pharmacy_current_survey['survey_status'] == '0' && $remaining_surveys > 0){
                                ?>
                                    <!--
                                    <div class="row">
                                    	<div class="clearfix"></div>                                    
                                        <div class="col-md-12 no-paddings">
                                            <div class="module2">
                                                <div class="stripe-5">
                                                    <h4>Status: In progress</h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <table class="table m-t">
                                      <tr>
                                        <td><strong>Minimum surveys required</strong></td>
                                        <td id="s_min_no_of_surveys">0</td>
                                      </tr>
                                      <tr>
                                        <td><strong>Total completed</strong></td>
                                        <td id="s_total_survey_completed">0</td>
                                      </tr>
                                      <tr>
                                        <td><strong>Completed online</strong></td>
                                        <td id="s_total_submitted_online_surveys">0</td>
                                      </tr>
                                      <tr>
                                        <td><strong>Completed Paper based</strong></td>
                                        <td id="s_total_submitted_paper_surveys">0</td>
                                      </tr>
                                    </table>
                                    
                                    -->
									
                                    <div class="row hidden" id="closed_btn_container">
                                            <div class="col-md-12">
                                            	<p class="redtexts text-justify m-t">The minimum number of returned questionnaires required for <?php echo ltrim(filter_string($pharmacy_current_survey['survey_title'],'Survey '))?> have been successfully completed, now you may close the survey.</p>
                                                
                                                <p class="text-right">
	                                                <a href="#close_survey" style="text-decoration:none" class="btn btn-success fancy_dialogue no-margins">Close Survey</a>
                                                </p>
                                                <div id="close_survey" style="display:none; min-width: 480px;">
                                                  <div class="color-line"></div>
                                                  <div class="modal-header text-left">
                                                        <h4 class="modal-title">Confirmation</h4>
                                                   </div>
                                                  <div class="modal-body">
                                                    <div class="row">
                                                      <div class="col-md-12"> Are you sure you want to close the current survey? </div>
                                                    </div>
                                                  </div>
                                                  <form name="close_survey_frm" id="close_survey_frm" method="post" action="<?php echo SURL?>dashboard/close-survey">
                                                      <div class="modal-footer"> 
                                                      	<button type="submit" class="btn btn-success no-margins" > Yes</button>
                                                        <button type="button" class="btn btn-danger fancy_close no-margins" onClick="jQuery.fancybox.close();"> No </button>
                                                        <input type="hidden" name="close_survey_id" id="close_survey_id" value="" readonly="readonly" />	
                                                      </div>
                                                  </form>
                                                </div>
                                            </div>
                                            
                                         </div>
                                                                     
                                    <!-- START: FINALIZE REPORT -->
                                    <div class="hidden" id="finalize_report_container">
                                        <form name="survey_finish_frm" id="survey_finish_frm" method="post" action="<?php echo SURL?>dashboard/finish-survey">
                                            <div class="clearfix"></div>                                    
                                            <div class="row">
                                                <div class="clearfix"></div>                                    
                                                <div class="col-md-12 no-paddings">
                                                <div class="module2">
                                                    <div class="stripe-5">
                                                        <h4>Well Performed Areas</h4>
                                                    </div>
                                                </div>
                                            </div>
    
                                                <div class="col-md-12">
                                                    <p class="text-justify m-t">Select three areas where the pharmacy has well performed</p>
                                                    <div class="single-field half-field ful_field">
                                                        <?php $not_allowed_area_question = array(1,2,8, 10, 11, 12, 13, 36, 43, 45, 47, 49, 50, 51); ?>
                                                        <select class="well_perf_slt" data-type="good" data-survey="<?php echo filter_string($pharmacy_current_survey['id']) ?>" name="well_perf_q_1" id="well_perf_q_1">
                                                            <option value="">Select Question</option>
                                                                <?php
                                                                foreach($questionnnair_arr as $question_id => $question_list){
                                                                    
                                                                    if(!in_array($question_id,$not_allowed_area_question )){
                                                                        
                                                                        if(count($question_list['sub_question']) == 0){
                                                                ?>
                                                                            <option value="<?php echo $question_id?>">Question <?php echo $question_list['question_code']?></option>
                                                                 <?php 
                                                                        }else{
                                                                         foreach($question_list['sub_question'] as $subquestion_id =>$subquestion_list){
                                                                    ?>
                                                                             <option value="<?php echo $subquestion_id?>">Question  <?php echo $subquestion_list['question_code'];?></option>	
                                                                     <?php
                                                                     
                                                                          }//end foreach($question_list['sub_question'] as $subquestion_id =>$subquestion_list)
                            
                                                                        }//end if(count($question_list['sub_question']) == 0)
                                                                        
                                                                    }//end if(!in_array($question_id,$not_allowed_area_question ))
                                                                    
                                                                }//end foreach($questionnnair_arr as $question_id => $question_list)
                                                                
                                                                ?>
                                                        </select>
                                                    </div>
                                                    <div class="single-field half-field ful_field">
                                                      <textarea class="form-control input-sm m-b-sm hidden" style="height:80px" name="well_perf_q_1_comment" id="well_perf_q_1_comment" placeholder="enter comments on question"></textarea>
                                                    </div>
                                                    <div class="single-field half-field ful_field">
                                                        <select class="well_perf_slt" data-survey="<?php echo filter_string($pharmacy_current_survey['id']) ?>" data-type="good" name="well_perf_q_2" id="well_perf_q_2">
                                                            <option value="">Select Question</option>
                                                                <?php
                                                                foreach($questionnnair_arr as $question_id => $question_list){
                                                                    
                                                                     if(!in_array($question_id,$not_allowed_area_question )){
                                                                         
                                                                        if(count($question_list['sub_question']) == 0){
                                                                ?>
                                                                            <option value="<?php echo $question_id?>">Question <?php echo $question_list['question_code']?></option>
                                                                 <?php 
                                                                        }else{
                                                                            foreach($question_list['sub_question'] as $subquestion_id =>$subquestion_list){
                                                                    ?>
                                                                                <option value="<?php echo $subquestion_id?>">Question  <?php echo $subquestion_list['question_code'];?></option>
                                                                     <?php
                                                                     
                                                                            }//end foreach($question_list['sub_question'] as $subquestion_id =>$subquestion_list)
                                                        
                                                                        }//end if(count($question_list['sub_question']) == 0)
                                                                     
                                                                     }//end if(!in_array($question_id,$not_allowed_area_question ))
                                                                     
                                                                }//end foreach($questionnnair_arr as $question_id => $question_list)
                                                                
                                                                ?>
                                                        </select>
                                                    </div>
                                                    <div class="single-field half-field ful_field">
                                                      <textarea class="form-control input-sm m-b-sm hidden" style="height:80px" name="well_perf_q_2_comment" id="well_perf_q_2_comment" placeholder="enter comments on question"></textarea>
                                                    </div>
                                                    <div class="single-field half-field ful_field">
                                                        <select class="well_perf_slt" data-survey="<?php echo filter_string($pharmacy_current_survey['id']) ?>" data-type="good" name="well_perf_q_3" id="well_perf_q_3">
                                                            <option value="">Select Question</option>
                                                                <?php
                                                                foreach($questionnnair_arr as $question_id => $question_list){
                                                                    
                                                                    if(!in_array($question_id,$not_allowed_area_question )){
                                                                        if(count($question_list['sub_question']) == 0){
                                                                ?>
                                                                            <option value="<?php echo $question_id?>">Question <?php echo $question_list['question_code']?></option>
                                                                 <?php 
                                                                        }else{
                                                                            foreach($question_list['sub_question'] as $subquestion_id =>$subquestion_list){
                                                                    ?>
                                                                                <option value="<?php echo $subquestion_id?>">Question  <?php echo $subquestion_list['question_code'];?></option>
                                                                     <?php
                                                                            }//end foreach($question_list['sub_question'] as $subquestion_id =>$subquestion_list)
                                                        
                                                                        }//end if(count($question_list['sub_question']) == 0)
                                                                        
                                                                    }//end if(!in_array($question_id,$not_allowed_area_question ))
                                                                    
                                                                }//end foreach($questionnnair_arr as $question_id => $question_list)
                                                                
                                                                ?>
                                                        </select>
                                                    </div>
                                                    <div class="single-field half-field ful_field">
                                                      <textarea class="form-control input-sm m-b-sm hidden" style="height:80px"  name="well_perf_q_3_comment" id="well_perf_q_3_comment" placeholder="enter comments on question"></textarea>
                                                    </div>
                                                </div>
                                              </div>
                                            
                                            <div class="row">
                                                <div class="clearfix"></div>                                    
                                                <div class="col-md-12 no-paddings">
                                                    <div class="module2">
                                                        <div class="stripe-5">
                                                            <h4>Need Improvement Areas</h4>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>                                         
                                              <div class="row">
                                                <div class="col-md-12">
                                                <p class="text-justify m-t">Select three areas where the pharmacy needs improvement</p>
                                                <div class="single-field half-field ful_field">
                                                    <select class="well_perf_slt" data-survey="<?php echo filter_string($pharmacy_current_survey['id']) ?>" data-type="bad" name="well_bad_q_1" id="well_bad_q_1">
                                                        <option value="">Select Question</option>
                                                            <?php
                                                            foreach($questionnnair_arr as $question_id => $question_list){
                                                                
                                                               if(!in_array($question_id,$not_allowed_area_question )){
                                                                   
                                                                    if(count($question_list['sub_question']) == 0){
                                                            ?>
                                                                    <option value="<?php echo $question_id?>">Question <?php echo $question_list['question_code']?></option>
                                                             <?php 
                                                                }else{
                                                                        foreach($question_list['sub_question'] as $subquestion_id =>$subquestion_list){
                                                                ?>
                                                                            <option value="<?php echo $subquestion_id?>">Question  <?php echo $subquestion_list['question_code'];?></option>
                                                                 <?php
                                                                        }//end foreach($question_list['sub_question'] as $subquestion_id =>$subquestion_list)
                                                    
                                                                    }//end if(count($question_list['sub_question']) == 0)
                                                                    
                                                               }//end if(!in_array($question_id,$not_allowed_area_question ))
                                                               
                                                            }//end foreach($questionnnair_arr as $question_id => $question_list)
                                                            
                                                            ?>
                                                    </select>
                                                </div>
                                                <div class="single-field half-field ful_field">
                                                  <textarea class="form-control input-sm m-b-sm hidden" style="height:80px" name="well_bad_q_1_comment" id="well_bad_q_1_comment" placeholder="enter comments on question"></textarea>
                                                </div>
                                                <div class="single-field half-field ful_field">
                                                    <select class="well_perf_slt" data-survey="<?php echo filter_string($pharmacy_current_survey['id']) ?>" data-type="bad" name="well_bad_q_2" id="well_bad_q_2">
                                                        <option value="">Select Question</option>
                                                            <?php
                                                            foreach($questionnnair_arr as $question_id => $question_list){
                                                                
                                                                if(!in_array($question_id,$not_allowed_area_question )){
                                                                    if(count($question_list['sub_question']) == 0){
                                                            ?>
                                                                        <option value="<?php echo $question_id?>">Question <?php echo $question_list['question_code']?></option>
                                                             <?php 
                                                                    }else{
                                                                        foreach($question_list['sub_question'] as $subquestion_id =>$subquestion_list){
                                                                ?>
                                                                            <option value="<?php echo $subquestion_id?>">Question  <?php echo $subquestion_list['question_code'];?></option>
                                                                 <?php
                                                                 
                                                                        }//end foreach($question_list['sub_question'] as $subquestion_id =>$subquestion_list)
                                                    
                                                                    }//end if(count($question_list['sub_question']) == 0)
                                                                    
                                                                }//end if(!in_array($question_id,$not_allowed_area_question ))
                                                                
                                                            }//end foreach($questionnnair_arr as $question_id => $question_list)
                                                            
                                                            ?>
                                                    </select>
                                                </div>
                                                <div class="single-field half-field ful_field">
                                                  <textarea class="form-control input-sm m-b-sm hidden" style="height:80px" name="well_bad_q_2_comment" id="well_bad_q_2_comment" placeholder="enter comments on question"></textarea>
                                                </div>
                                                <div class="single-field half-field ful_field">
                                                    <select class="well_perf_slt" data-survey="<?php echo filter_string($pharmacy_current_survey['id']) ?>" data-type="bad" name="well_bad_q_3" id="well_bad_q_3">
                                                        <option value="">Select Question</option>
                                                            <?php
                                                            foreach($questionnnair_arr as $question_id => $question_list){
                                                                
                                                                if(!in_array($question_id,$not_allowed_area_question )){
                                                                    
                                                                    if(count($question_list['sub_question']) == 0){
                                                            ?>
                                                                        <option value="<?php echo $question_id?>">Question <?php echo $question_list['question_code']?></option>	
                                                             <?php 
                                                                    }else{
                                                                        foreach($question_list['sub_question'] as $subquestion_id =>$subquestion_list){
                                                                ?>
                                                                            <option value="<?php echo $subquestion_id?>">Question  <?php echo $subquestion_list['question_code'];?></option>
                                                                 <?php
                                                                     
                                                                        }//end foreach($question_list['sub_question'] as $subquestion_id =>$subquestion_list)
                                                    
                                                                    }//end if(count($question_list['sub_question']) == 0)
                                                                    
                                                                }//end if(!in_array($question_id,$not_allowed_area_question ))
                                                                
                                                            }//end foreach($questionnnair_arr as $question_id => $question_list)
                                                            
                                                            ?>
                                                    </select>
                                                </div>
                                                <div class="single-field half-field ful_field">
                                                  <textarea class="form-control input-sm m-b-sm hidden" style="height:80px" name="well_bad_q_3_comment" id="well_bad_q_3_comment" placeholder="enter comments on question"></textarea>
                                                </div>
                                                </div>
                                              </div>
                                              
                                              <div class="row">
                                                <div class="col-md-12">
                                                    
                                                    <p class="text-right">
                                                        <a id="verify_finish_report" class="btn btn-success verify_finish_report"> Finish Report </a>
                                                        <a href="#finish_survey_err_fancy" id="finish_survey_err_btn" class="fancy_dialogue hidden "> Finish Report Error</a>
                                                        
                                                        <div id="finish_survey_err_fancy" style="display:none; min-width: 480px;">
                                                          <div class="color-line"></div>
                                                          <div class="modal-header text-left">
                                                            <h4 class="modal-title">Warning</h4>
                                                          </div>
                                                          <div class="modal-body">
                                                            <div class="row">
                                                              <div class="col-md-12" id="survey_close_err_msg"> You have to select all six questions to finish the survey.</div                                                            ></div>
                                                          </div>
                                                        </div>
                                                        
                                                        <a href="#finish_survey" id="finish_survey_btn" class="fancy_dialogue hidden"> Finish Report Dialogue </a>
                                                        
                                                        <div id="finish_survey" style="display:none; min-width: 480px;">
                                                          <div class="color-line"></div>
                                                          <div class="modal-header text-left">
                                                            <h4 class="modal-title">Confirmation</h4>
                                                       </div>
                                                          <div class="modal-body">
                                                            <div class="row">
                                                              <div class="col-md-12"> Are you sure you want to Finish the current survey? </div>
                                                            </div>
                                                          </div>
                                                      
                                                          <div class="modal-footer"> 
                                                            <button type="button" class="btn btn-success" id="finish_survey_sbt" style="margin:0"> Yes</button>
                                                            <button type="button" class="btn btn-danger fancy_close" style="margin:0" onClick="jQuery.fancybox.close();"> No</button>
                                                            
                                                          </div>
                                                        </div>
                                                        
                                                        <a href="#already_taken_question" id="already_taken_btn" class="hidden fancy_dialogue"> Already Taken</a>
                                                        <div id="already_taken_question" style="display:none; min-width: 480px;">
                                                          <div class="color-line"></div>
                                                          <div class="modal-header text-left">
                                                            <h4 class="modal-title">Warning</h4>
                                                          </div>
                                                          <div class="modal-body">
                                                            <div class="row">
                                                              <div class="col-md-12"> You have already selected this question. Please select another question from the drop-down menu.</div>
                                                            </div>
                                                          </div>
                                                        </div>
                                                        <input type="hidden" name="finish_survey_id" id="finish_survey_id" value="" readonly="readonly" />	
    
                                                    </p>
                                                    
                                                </div>
                                                
                                             </div>
                                            <?php 
                                                foreach($questionnnair_arr as $question_id => $question_list){
                                                                
                                                        if(count($question_list['sub_question']) == 0){
                                                    ?>
                                                            <textarea class="hidden" name="svg[<?php echo filter_string($question_id)?>]" id="svg_<?php echo filter_string($question_id)?>" readonly="readonly"></textarea>                   
                                                     <?php 
                                                            if($question_id == 9 || $question_id == 44){
                                                    ?>
                                                              <textarea class="hidden" name="svg_poster_9" id="svg_poster_9" readonly="readonly"></textarea>                   
                                                              
                                                    <?php
                                                            }//end if($question_id == 9 || $question_id == 44)
                                                            
                                                        }else{
                                                            foreach($question_list['sub_question'] as $subquestion_id =>$subquestion_list){
                                                        ?>
                                                                <textarea class="hidden" name="svg[<?php echo filter_string($subquestion_id)?>]" id="svg_<?php echo filter_string($subquestion_list['question_code'])?>" readonly="readonly"></textarea>                   
                                                         <?php
                                                                
                                                         
                                                            }//end foreach($question_list['sub_question'] as $subquestion_id =>$subquestion_list)
                
                                                        }//end if(count($question_list['sub_question']) == 0)
                                                        
                                                    }//end foreach($questionnnair_arr as $question_id => $question_list)
                                            ?>                                        
                                 
                                        </form>
                                    </div>
                          </div>
                        </div>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                    <div class="col-md-8">
                      <div class="contact_content_border">
                      	<div class="clearfix"></div>                                    
                        <div class="module2">
                          <div class="stripe-5">
                            <h4>Survey Questions </h4>
                          </div>
                        </div>
                        <div class="contact_content_border2"> 

                            <div id="survey_chart_container">
	                            <div class="alert alert-default">Select the Survey from the drop down list to view survey statistics.</div>
                            </div>                          
                        </div>
                      </div>
                    </div>
            <?php		
				}else{
			?>
            		<div class="col-md-12">
                    	<div class="alert alert-danger">No survey(s) currently available for results.</div>
                     </div>
            <?php	
				}//end if(count($pharmacy_survey_list) > 0)
            ?>
           
          </div>
          <div class="clearfix"></div>
        </div>
        <div id="archives_tab" class="tab-pane <?php echo ($tab == 4)? 'active' : ''?>">
          <div class="">
            <div class="module2">
              <div class="stripe-5">
                <h4 class="no-margins">Archives</h4>
              </div>
            </div>
            <div class="" style="min-height:500px">
              <div class="table-responsive">
                <table class="table table-condensed table-striped table-bordered table-hover no-margin">
                  <thead>
                    <tr>
                      <th>Survey Year</th>
                      <th>Survey Forms</th>
                      <th>Final Report</th>
                      <th>DoH Feedback Letter</th>
                      <th>Poster</th>
                      <th>Results Leaflet</th>

                    </tr>
                  </thead>
                  <tbody>
					<?php 
                        $completed_survey_list = get_pharmacy_survey_list($this->session->p_id,'3');
                        
                        if(count($completed_survey_list) > 0){
                            
                            for($i=0;$i<count($completed_survey_list);$i++){
                    ?>
                                <tr align="left">
                                    <td><?php echo filter_string($completed_survey_list[$i]['survey_title'])?></td>
                                    <td>
                                        <a href="#download_survey_forms_<?php echo filter_string($completed_survey_list[$i]['id'])?>" onclick="jQuery('.download_forms_btn').attr('disabled',false)" class="fancy_dialogue"><i class="fa fa-file-pdf-o fa-2x text-danger"></i></a>
                                    
                                        <div id="download_survey_forms_<?php echo filter_string($completed_survey_list[$i]['id'])?>" style="display:none; min-width: 480px;">
                                          <div class="color-line"></div>
                                          <div class="modal-header text-left">
                                            <h4 class="modal-title">Download Survey Forms</h4>
                                          </div>
                                          <div class="modal-body">
                                            <div class="row">
                                              <div class="col-md-12">
                                              <?php 
												$DOWNLOAD_SURVEY_FORM_ALERT_TXT = 'DOWNLOAD_SURVEY_FORM_ALERT_TXT';
												$survey_form_download_alert = get_global_settings($DOWNLOAD_SURVEY_FORM_ALERT_TXT); //Set from the Global Settings
												echo $survey_form_download_alert = filter_string($survey_form_download_alert['setting_value']);
											  ?>
                                              
                                              </div>
                                            </div>
                                          </div>
                                          
                                          <div class="modal-footer"> 
                                            <form name="survey_form_frm" id="survey_form_frm_<?php echo $completed_survey_list[$i]['id']?>" action="<?php echo SURL?>dashboard/download-survey-form/<?php echo $completed_survey_list[$i]['id']?>">                                                  
                                                <button type="button" class="btn btn-success download_forms_btn" style="margin:0" data-survey="<?php echo $completed_survey_list[$i]['id']?>"> Download </button>
                                                <button type="button" class="btn btn-danger fancy_close" style="margin:0" onClick="jQuery.fancybox.close();"> Close</button>
                                               </form>
                                           </div>
                                        </div>
                                    </td>
                                    <td>
                                        <a href="#download_final_report_<?php echo filter_string($completed_survey_list[$i]['id'])?>" onclick="jQuery('.download_report_btn').attr('disabled',false)" class="fancy_dialogue"><i class="fa fa-file-pdf-o fa-2x text-danger"></i></a>
                                    
                                        <div id="download_final_report_<?php echo filter_string($completed_survey_list[$i]['id'])?>" style="display:none; min-width: 480px;">
                                          <div class="color-line"></div>
                                          <div class="modal-header text-left">
                                            <h4 class="modal-title">Download Final Report</h4>
                                          </div>
                                          <div class="modal-body">
                                            <div class="row">
                                              <div class="col-md-12">
                                              
                                              <?php 
												$DOWNLOAD_SURVEY_FINAL_REPORT_ALERT_TXT = 'DOWNLOAD_SURVEY_FINAL_REPORT_ALERT_TXT';
												$survey_final_report_alert = get_global_settings($DOWNLOAD_SURVEY_FINAL_REPORT_ALERT_TXT); //Set from the Global Settings
												echo $survey_final_report_alert = filter_string($survey_final_report_alert['setting_value']);
											  ?>
                                              

                                              </div>
                                            </div>
                                          </div>
                                          
                                          <div class="modal-footer"> 
                                            <form name="final_report_frm" id="final_report_frm_<?php echo $completed_survey_list[$i]['id']?>" action="<?php echo SURL?>dashboard/download-final-report/<?php echo $completed_survey_list[$i]['id']?>">                                                  
                                                <button type="button"  class="btn btn-success download_report_btn" style="margin:0" data-survey="<?php echo $completed_survey_list[$i]['id']?>"> Download </button>
                                                <button type="button" class="btn btn-danger fancy_close" style="margin:0" onClick="jQuery.fancybox.close();"> Close</button>
                                               </form>
                                           </div>
                                        </div>
                                    
                                    </td>
                                    <td>
                                        <a href="#download_doh_letter_<?php echo filter_string($completed_survey_list[$i]['id'])?>" onclick="jQuery('.download_doh_btn').attr('disabled',false)" class="fancy_dialogue"><i class="fa fa-file-pdf-o fa-2x text-danger"></i></a>
                                        <div id="download_doh_letter_<?php echo filter_string($completed_survey_list[$i]['id'])?>" style="display:none; min-width: 480px;">
                                          <div class="color-line"></div>
                                          <div class="modal-header text-left">
                                            <h4 class="modal-title">Download DoH Letter</h4>
                                          </div>
                                          <div class="modal-body">
                                            <div class="row">
                                              <div class="col-md-12">
                                             
                                              <?php 
												$DOWNLOAD_SURVEY_DOH_ALERT_TXT = 'DOWNLOAD_SURVEY_DOH_ALERT_TXT';
												$survey_doh_report_alert = get_global_settings($DOWNLOAD_SURVEY_DOH_ALERT_TXT); //Set from the Global Settings
												echo $survey_doh_report_alert = filter_string($survey_doh_report_alert['setting_value']);
											  ?>
                                              
                                              </div>
                                            </div>
                                          </div>
                                          
                                          <div class="modal-footer"> 
                                            <form name="doh_frm" id="doh_frm_<?php echo $completed_survey_list[$i]['id']?>" action="<?php echo SURL?>dashboard/download-survey-doh-letter/<?php echo $completed_survey_list[$i]['id']?>">                                                  
                                            <button onclick="jQuery(this).attr('disabled',true)" style="margin:0" class="btn btn-success download_doh_btn" data-survey="<?php echo $completed_survey_list[$i]['id']?>"> Download </button>
                                            <button type="button" class="btn btn-danger fancy_close" style="margin:0" onClick="jQuery.fancybox.close();"> Close</button>
                                        </form>
                                           </div>
                                        </div>
                                    
                                    </td>
                                    <td>

                                        <a href="#download_survey_poster_<?php echo filter_string($completed_survey_list[$i]['id'])?>" onclick="jQuery('.download_poster_btn').attr('disabled',false)" class="fancy_dialogue"><i class="fa fa-file-pdf-o fa-2x text-danger"></i></a>
                                        <div id="download_survey_poster_<?php echo filter_string($completed_survey_list[$i]['id'])?>" style="display:none; min-width: 480px;">
                                          <div class="color-line"></div>
                                          <div class="modal-header text-left">
                                            <h4 class="modal-title">Download Survey Poster</h4>
                                          </div>
                                          <div class="modal-body">
                                            <div class="row">
                                              <div class="col-md-12"> 
                                             
												  <?php 
                                                    $DOWNLOAD_SURVEY_POSTER_ALERT_TXT = 'DOWNLOAD_SURVEY_POSTER_ALERT_TXT';
                                                    $survey_poster_alert = get_global_settings($DOWNLOAD_SURVEY_POSTER_ALERT_TXT); //Set from the Global Settings
                                                    echo $survey_poster_alert = filter_string($survey_poster_alert['setting_value']);
                                                  ?>
                                              
                                              
                                              </div>
                                            </div>
                                          </div>
                                          
                                          <div class="modal-footer"> 
                                            <form name="poster_frm" id="poster_frm_<?php echo $completed_survey_list[$i]['id']?>" action="<?php echo SURL?>dashboard/download-survey-poster/<?php echo $completed_survey_list[$i]['id']?>">                                                  
                                            <button type="button" class="btn btn-success download_poster_btn" style="margin:0" data-survey="<?php echo $completed_survey_list[$i]['id']?>" > Download </button>
                                            <button type="button" class="btn btn-danger fancy_close" style="margin:0" onClick="jQuery.fancybox.close();"> Close</button>
                                        </form>
                                           </div>
                                        </div>
										
                                        <!-- Patient Satisfaction Survey Poster Report -->

                                        <!-- <a href="#download_patient_satisfaction_survey_poster_<?php echo filter_string($completed_survey_list[$i]['id'])?>" class="fancy_dialogue"><i class="fa fa-file-pdf-o fa-2x text-danger"></i> Patient Satisfaction Survey</a> -->
                                        
                                        <div id="download_patient_satisfaction_survey_poster_<?php echo filter_string($completed_survey_list[$i]['id'])?>" style="display:none; min-width: 480px;">
                                          <div class="color-line"></div>
                                          <div class="modal-header text-left">
                                            <h4 class="modal-title">Download Patient Satisfaction Survey Results</h4>
                                          </div>
                                          <div class="modal-body">
                                            <div class="row">
                                              <div class="col-md-12"> 
												  <?php 
                                                    $DOWNLOAD_SURVEY_POSTER_ALERT_TXT = 'DOWNLOAD_SURVEY_POSTER_ALERT_TXT';
                                                    $survey_poster_alert = get_global_settings($DOWNLOAD_SURVEY_POSTER_ALERT_TXT); //Set from the Global Settings
                                                    echo $survey_poster_alert = filter_string($survey_poster_alert['setting_value']);
                                                  ?>
                                              </div>
                                            </div>
                                          </div>
                                          
                                          <div class="modal-footer"> 
                                            <form name="poster_frm" id="patient_satis_survey_poster_frm_<?php echo $completed_survey_list[$i]['id']?>" action="<?php echo SURL?>dashboard/download-patient-satisfaction-survey-poster/<?php echo $completed_survey_list[$i]['id']?>" method="post">                                                  
                                            <button type="button" class="btn btn-success download_patient_satisfaction_survey_poster_btn" style="margin:0" data-survey="<?php echo $completed_survey_list[$i]['id']?>" > Download </button>
                                            <button type="button" class="btn btn-danger fancy_close" style="margin:0" onClick="jQuery.fancybox.close();"> Close</button>
                                            
                                        </form>
                                           </div>
                                        </div>

                                    </td>
                                    <td>
                                        <!-- Patient Survey POster Report -->

                                        <a href="#download_patient_survey_poster_<?php echo filter_string($completed_survey_list[$i]['id'])?>" class="fancy_dialogue"><i class="fa fa-file-pdf-o fa-2x text-danger"></i></a>
                                        
                                        <div id="download_patient_survey_poster_<?php echo filter_string($completed_survey_list[$i]['id'])?>" style="display:none; min-width: 480px;">
                                          <div class="color-line"></div>
                                          <div class="modal-header text-left">
                                            <h4 class="modal-title">Download Patient Survey Results</h4>
                                          </div>
                                          <div class="modal-body">
                                            <div class="row">
                                              <div class="col-md-12"> 
												  <?php 
                                                    $DOWNLOAD_PATIENT_SURVEY_POSTER_ALERT_TXT = 'DOWNLOAD_PATIENT_SURVEY_POSTER_ALERT_TXT';
                                                    $patient_survey_poster_alert = get_global_settings($DOWNLOAD_PATIENT_SURVEY_POSTER_ALERT_TXT); //Set from the Global Settings
                                                    echo $survey_poster_alert = filter_string($patient_survey_poster_alert['setting_value']);
                                                  ?>
                                              </div>
                                            </div>
                                          </div>
                                          
                                          <div class="modal-footer"> 
                                            <form name="poster_frm" id="patient_survey_poster_frm_<?php echo $completed_survey_list[$i]['id']?>" action="<?php echo SURL?>dashboard/download-patient-survey-poster/<?php echo $completed_survey_list[$i]['id']?>" method="post">                                                  
                                            <button type="button" class="btn btn-success download_patient_survey_poster_btn" style="margin:0" data-survey="<?php echo $completed_survey_list[$i]['id']?>" > Download </button>
                                            <button type="button" class="btn btn-danger fancy_close" style="margin:0" onClick="jQuery.fancybox.close();"> Close</button>

                                            <textarea id="svg_gender_pie_chart" name="svg_gender_pie_chart" class="svg_gender_pie_chart hidden" readonly="readonly"></textarea> 
                                            <textarea id="svg_age_pie_chart" name="svg_age_pie_chart" class="svg_age_pie_chart hidden" readonly="readonly"></textarea> 
                                            
                                        </form>
                                           </div>
                                        </div>
                                    
                                    </td>
                                </tr>
                    <?php			
                            }//end for($i=0;$i<count($completed_survey_list);$i++)
                            
                        }else{
                    ?>
                        <tr align="center"><td colspan="5" class="text-danger">No survey(s) in completed list yet.</td></tr>	
                    <?php		
                        }//end if(count($completed_survey_list) > 0)
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <div id="settings_tab" class="tab-pane <?php echo ($tab == 5)? 'active' : ''?>">
          <div class="row">
            <div class="col-md-6">
              <div class="contact_content_border">
                <div class="module2">
                  <div class="stripe-5">
                    <h4> Update Settings</h4>
                  </div>
                </div>
                <form id="form-sVNhxxRtKO" name="form-sVNhxxRtKO" action="<?php echo base_url(); ?>dashboard/update-pharmacy-profile" method="post" class="wowcontactform">
                    <div class="contact_content_border2">
                      <div class="single-field half-field ful_field form-group">
                        <label>Business name </label>
						<input type="text" name="pharmacy_name" id="pharmacy_name" placeholder="Business name"  value="<?php echo ($pharmacy_profile['pharmacy_name']) ? filter_string($pharmacy_profile['pharmacy_name']) : '' ; ?>" required="required" class="input-name" data-fv-regexp="true" data-fv-regexp-regexp="^[a-zA-z0-9\s\-,\]+[a-zA-Z0-9\s\-_,.'/]{1,100}$" data-fv-regexp-message="Please use allowed characters (Alphabet, Numbers, Hyphens, Underscore, Comma, Apostrophe, Dot, Forward Slash, Back Slash, Space)" maxlength="100">
                      </div>
                      <div class="single-field half-field ful_field form-group">
                        <label> Contact name </label>
                        <input type="text" name="owner_name" id="owner_name" placeholder="Contact name"  value="<?php echo ($pharmacy_profile['owner_name']) ? filter_string($pharmacy_profile['owner_name']) : '' ; ?>" required="required" class="input-name">
                      </div>
                      <div class="single-field half-field ful_field form-group">
                        <label> Contact number </label>
                        <input type="text" name="contact_no" id="contact_no" placeholder="Contact number"  value="<?php echo ($pharmacy_profile['contact_no']) ? filter_string($pharmacy_profile['contact_no']) : '' ; ?>" required="required" class="input-name" data-fv-regexp="true"  data-fv-regexp-regexp="^(?=.*[0-9])[0-9]{11,}$" data-fv-regexp-message="Please use allowed characters (Numbers) and length should be minimum 11 characters"  maxlength="15">
                      </div>
                      <div class="single-field half-field ful_field form-group">
                        <label> Email address </label>
                        <input type="email" name="email_address" id="email_address" placeholder="Email address"  value="<?php echo ($pharmacy_profile['email_address']) ? filter_string($pharmacy_profile['email_address']) : '' ; ?>" required="required" class="input-name" data-fv-regexp="true" data-fv-regexp-regexp="^[a-zA-z0-9\s\-]+[a-zA-Z0-9\s\-_.@]+$" data-fv-regexp-message="Please use allowed characters (Alphabet, Numbers, Hyphens, Underscore, Dot and @)" autocomplete="off" maxlength="50">
                        <div id="error_msg" class="error help-block" style="font-size: 85%;"></div>
                      </div>
                      <div class="single-field half-field ful_field form-group">
                        <label>Find your Postcode</label>
                        <div id="postcode_lookup" class="">
                      </div>
                      <div class="single-field half-field ful_field form-group">
                        <label>Address </label>
                        <input type="text" name="address" id="address" placeholder="Address 1"  value="<?php echo ($pharmacy_profile['address']) ? filter_string($pharmacy_profile['address']) : '' ; ?>" class="input-name" required="required" data-fv-regexp="true" data-fv-regexp-regexp="^[a-zA-z0-9\s\-,\]+[a-zA-Z0-9\s\-_,.'/]{1,50}$"  data-fv-regexp-message="Please use allowed characters (Alphabet, Numbers, Hyphens, Underscore, Comma, Apostrophe, Dot, Forward Slash, Back Slash, Space)" maxlength="50">
                      </div>
                      <div class="single-field half-field ful_field form-group">
                        <label> Address 2 (optional)</label>
                        <input type="text" name="address_2" id="address_2" placeholder="Address 2"  value="<?php echo ($pharmacy_profile['address_2']) ? filter_string($pharmacy_profile['address_2']) : '' ; ?>" aria-required="true" class="input-name" data-fv-regexp="true" data-fv-regexp-regexp="^[a-zA-z0-9\s\-,\]+[a-zA-Z0-9\s\-_,.'/]{1,50}$"  data-fv-regexp-message="Please use allowed characters (Alphabet, Numbers, Hyphens, Underscore, Comma, Apostrophe, Dot, Forward Slash, Back Slash, Space)" maxlength="50">
                      </div>
                      <div class="single-field half-field ful_field form-group">
                        <label>Town </label>
                        <input type="text" name="town" id="town" placeholder="Town"  value="<?php echo ($pharmacy_profile['town']) ? filter_string($pharmacy_profile['town']) : '' ; ?>" aria-required="true" class="input-name" required="required" data-fv-regexp="true" data-fv-regexp-regexp="^[a-zA-z0-9\s\-,\]+[a-zA-Z0-9\s\-_,.'/]{1,50}$" data-fv-regexp-message="Please use allowed characters (Alphabet, Numbers, Hyphens, Underscore, Comma, Apostrophe, Dot, Forward Slash, Back Slash, Space)" maxlength="50">
                      </div>
                      <div class="single-field half-field ful_field form-group">
                        <label>County (optional)</label>
                        <input type="text" name="county" id="county" placeholder="County" value="<?php echo ($pharmacy_profile['county']) ? filter_string($pharmacy_profile['county']) : '' ; ?>" aria-required="true" data-fv-regexp="true" data-fv-regexp-regexp="^[a-zA-z0-9\s\-,\]+[a-zA-Z0-9\s\-_,.'/]{1,50}$" data-fv-regexp-message="Please use allowed characters (Alphabet, Numbers, Hyphens, Underscore, Comma, Apostrophe, Dot, Forward Slash, Back Slash, Space)">
                      </div>
                      <div class="single-field half-field ful_field form-group">
                        <label>Postcode</label>
                        <input type="text" name="postcode" id="postcode" placeholder="Postcode" value="<?php echo ($pharmacy_profile['postcode']) ? filter_string($pharmacy_profile['postcode']) : '' ; ?>" aria-required="true" class="input-name" required="required" data-fv-regexp="true"  data-fv-regexp-regexp="^[a-z|A-Z|]+[a-z|A-Z|0-9|\s]{0,10}$" data-fv-regexp-message="Please use allowed characters (Alphabets, Numbers and spaces) and first character must be alphabet." maxlength="10">
                      </div>
                      <div class="text-right finish_report"> 
                        <button type="submit" class="btn btn-success" value="Update Settings">Update Settings</button>
                        <input type="hidden" id="pharmacy_id_2" value="<?php echo $this->session->p_id;?>" readonly="readonly" />
                      </div>
                    </div>
                    </div>
                </form>
                <div class="clearfix"></div>
              </div>
            </div>
            
            <div class="col-md-6">
              <div class="contact_content_border">
                <div class="module2">
                  <div class="stripe-5">
                    <h4> Change Password </h4>
                  </div>
                </div>
                <form id="form-sVNhxxRtKO11" name="form-sVNhxxRtKO" action="<?php echo base_url(); ?>dashboard/create-new-password" method="post" >
                    <div class="contact_content_border2">
                      <!--
                      	<div class="single-field half-field ful_field form-group">
                        <label> Current password </label>
                        <input type="password" name="password" id="password" placeholder="Current Password"  value="" required="required" class="input-name" data-fv-regexp="true" data-fv-regexp-regexp="^^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[A-Za-z0-9]{8,20}$" data-fv-regexp-message="Password must be between 8 to 20 characters with atleast one uppercase, one lowercase and one digit. Allowed characters (Alphabet, Numbers)" maxlength="20">
                      </div>
                     	-->
                      <div class="single-field half-field ful_field">
                        <label> New password </label>
                        <input type="password" name="new_password" id="password" placeholder="New Password"  value="" required="required" class="input-name" data-fv-regexp="true" data-fv-regexp-regexp="^^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[A-Za-z0-9]{8,20}$" data-fv-regexp-message="Password must be between 8 to 20 characters with atleast one uppercase, one lowercase and one digit. Allowed characters (Alphabet, Numbers)" maxlength="20">
                      </div>
                      <div class="single-field half-field ful_field form-group">
                        <label> Retype password </label>
                        <input type="password" name="re_new_password" id="re_password" placeholder="Retype password"  value="" required="required" class="input-name" data-fv-identical="true" data-fv-identical-field="new_password" data-fv-identical-message="The password and its confirm password does not match" maxlength="20" >
                      </div>
                      <div class="text-right"> 
                        <button type="submit" id="submit_21" value="Change Password" class="btn btn-success">Change Password</button>
                      </div>
                    </div>
                </form>
                <div class="clearfix"></div>
              </div>
            </div>
          </div>
        </div>
        <div id="payment_tab" class="tab-pane <?php echo ($tab == 6)? 'active' : ''?>">
          <div class="" style="min-height:500px">
            <div class="module2">
              <div class="stripe-5">
                <h4 class="no-margins">Payment History</h4>
              </div>
            </div>
            <div class="">
            
              <div class="table-responsive">
                <table class="table table-condensed table-striped table-bordered table-hover no-margin">
                  <thead>
                    <tr>
                      <th>Date</th>
                      <th>Survey Year</th>
                      <th>Payment Method</th>
                      <th>Amount(£)</th>
                      <th width="10%">Action</th>
                    </tr>
                  </thead>
                  <tbody>
					<?php 
                        if(count($survey_payment_invoices) > 0){
                            for($i=0;$i<count($survey_payment_invoices);$i++){
                    ?>
                                <tr>
                                  <td><?php echo filter_uk_date($survey_payment_invoices[$i]['created_date'])?></td>
                                  <td><?php echo filter_string($survey_payment_invoices[$i]['survey_session'])?></td>
                                  <td><?php echo filter_string($survey_payment_invoices[$i]['payment_method'])?></td>
                                  <td><?php echo filter_string($survey_payment_invoices[$i]['grand_total'])?></td>
                                  <td><!--<a href="<?php echo SURL?>dashboard/view-invoice/<?php echo filter_string($survey_payment_invoices[$i]['id'])?>" class="fancy_ajax_close_btn fancybox.ajax btn btn-sm btn-info view_table">View</a>-->
                                  		<a href="<?php echo SURL?>dashboard/download-invoice/<?php echo filter_string($survey_payment_invoices[$i]['id']);?>" class="btn btn-sm btn-info view_table" target="_blank">View</a>
                                  </td>
                                </tr>                                
                    <?php				
                            }//end for($i=0;$i<count($survey_payment_invoices);$i++)
                        }else{
                    ?>
                            <tr>
                                <td class="text-danger" colspan="6">No Record Found</td>
                            </tr>
                    <?php		
                        }//end if(count($survey_payment_invoices) > 0)
                    ?>
                    
                    
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        
        <div id="incomplete_survey_tab" class="tab-pane <?php echo ($tab == 7)? 'active' : ''?>" style="overflow:auto">
        	<p class="current_survey_notes hidden" id="in_survey_txt"><i class="fa fa-info-circle"></i> Minimum number of required surveys is complete. You can now close the survey by clicking on RESULTS tab or <a href="<?php echo SURL?>dashboard/?t=3" class="btn btn-warning no-margins">CLICK HERE</a></p>
			<?php 
                $survey_incomplete_list = get_pharmacy_incomplete_survey_list($this->session->p_id);

                if(count($survey_incomplete_list) > 0 ){
            ?>
                <div class="" style="min-height:500px">
                    <div class="row">
                        <div class="col-md-3 text-right form-group">
                            <select class="form-control" name="incomplete_survey_slt" id="incomplete_survey_slt">
                                
                                <?php 
									for($i=0;$i<count($survey_incomplete_list);$i++){
										
										if($survey_incomplete_list[$i]['survey_start_date'] != NULL && $survey_incomplete_list[$i]['survey_status'] == '0'){
								?>
											<option value="<?php echo filter_string($survey_incomplete_list[$i]['id'])?>"><?php echo filter_string($survey_incomplete_list[$i]['survey_title'])?></option>                                
                                <?php		
										}//end if($survey_incomplete_list['survey_start_date'] != NULL && $survey_incomplete_list['survey_status'] == '0')
										
									}//end for($i=0;$i<count($survey_incomplete_list);$i++)
								?>
                            </select>
                        </div>        
                        <div class="col-md-3 text-right">
                            <select class="form-control" name="min_survey_required" id="min_survey_required">
                                <option value="">Minimum Survey Required</option>
                                <option value="50">50 (0 - 2000)</option>
                                <option value="75">75 (2001 - 4000)</option>
                                <option value="100">100 (4001 - 6000)</option>
                                <option value="125">125 (6001 - 8000)</option>
                                <option value="150">150 (8001 - onwards)</option>
                            </select>
                        </div>          
                        <div class="col-md-6 text-right">
                            <button class="btn btn-lg btn-info no-margins hidden" id="btn_survey_stats">COMPLETED  <span id="spn_survey_completed"></span>/ <span id="spn_min_survey_required"></span></button>
                        </div>          
                    </div>
                    <hr />
                    <div class="module2">
                      <div class="stripe-5">
                        <h4>COMMUNITY PHARMACY PATIENT QUESTIONNAIRE</h4>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="company_detail">
                        <p><strong><?php echo $this->session->pharmacy_name;?></strong><br>
                              <?php 
                                $address_2 = ($this->session->address_2) ? $this->session->address_2.', ': '';
                                $phramcy_address = $this->session->address.', '.$address_2.$this->session->town.', '.$this->session->county.', '.$this->session->postcode;
                                echo $phramcy_address;
                              ?>
                         </p>
                      </div>
                    </div>
                    <form action="<?php echo SURL ?>dashboard/submit-paper-incomplete-survey"  id="in_manage_survey_frm" name="manage_survey_frm" method="POST" enctype="multipart/form-data"  class="prevent_enter_key">
                    
                        <div class="col-md-12 m-t">
                          
                                <div class="module2">
                                  <div class="stripe-5 m-b-sm">
                                    <h4 class="no-margins">This section is about why you visited the pharmacy today</h4>
                                  </div>
                                </div>                    
                            <?php 
                                foreach($questionnnair_arr as $question_id => $question_list){
                                    
                                    if($question_id == 4){
                            ?>
        
                                <div class="module2">
                                  <div class="stripe-5 m-b-sm">
                                    <h4 class="no-margins">This section is about the pharmacy and the staff who work there more generally, not just for today's visit</h4>
                                  </div>
                                </div>                    
        
        
                            <?php			
                                    }elseif($question_id == 11){
                            ?>
        
                                        <div class="module2">
                                          <div class="stripe-5 m-b-sm">
                                            <h4 class="no-margins">These last few questions are just to help us categorise your answers</h4>
                                          </div>
                                        </div>   
        
                            <?php			
                                    }//end if($question_id == 4)
                            ?>
                                    <div class="row">
                                        <div class="col-lg-12">
                                          <div class="panel3 panel-primary">
                                            <div class="panel-heading"> <span class="badge"><strong><?php echo filter_string($question_list['question_code'])?></strong></span> <?php echo filter_string($question_list['question']);?> <i><?php echo filter_string($question_list['sub_notes']);?></i> </div>
                                            <div class="panel-body">
                                            <?php 
                                                if($question_id !=10 && $question_id != 45){
                                                    
                                                    if(count($question_list['sub_question']) == 0){
                                                        
                                                        $total_options = count($question_list['options']);
                                                        $per_cell = floor(12/$total_options);
                                    
                                                        foreach($question_list['options'] as $option_id => $option_list){
                                                            
                                                            $per_cell = ($total_options == 2) ? '1' : $per_cell;
                                            ?>
                                                            <div class="col-lg-<?php echo $per_cell;?>" <?php echo ($question_id == 11 || $question_id == 49) ? 'style="width: 120px;"' : '' ?>>
                                                                <div class="radio radio-success radio-circle">
                                                                    <input class="<?php echo ($question_id == 1 || $question_id == 36) ? 'show_hide_textbox_in' : ''?>" type="radio" required="required" value="<?php echo filter_string($option_id)?>" id="in_opt_<?php echo $question_id.'_'.$option_id?>" name="q[<?php echo $question_id?>]"  >
                                                                    <label for="in_opt_<?php echo $question_id.'_'.$option_id?>"><?php echo filter_string($option_list);?></label>
                                                                </div>
                                                            </div>
                                            <?php				
                                                        }//end foreach($question_list['options'] as $option_id => $option_list)
                                            ?>
                                                        <div class="row hidden" id="in_err_q<?php echo filter_string($question_id)?>"><div class="col-md-12 alert alert-sm alert-danger">Please select one of the options</div></div>
                                            <?php			
                                                        if($question_id == 1 || $question_id == 36){
                                            ?>
                                                        <div class="col-lg-12 hidden" id="question_10_txtbox_in">
                                                            <textarea rows="5" style="width:100%; background-color:#fff" id="other_reason_txt_in" name="other_reason_txt" placeholder="For some other reason (please write in the reason for your visit):" maxlength="250"></textarea>
                                                            <p><i>Max 250 characters allowed </i></p>
                                                        </div>
                                            <?php				
                                                        }
                                                    }else{
                                                        
                                                         foreach($question_list['sub_question'] as $subquestion_id =>$subquestion_list){
                                            ?>
                                                            <div class="col-lg-12"><p class="subtitles">
                                                            <strong><?php echo substr(filter_string($subquestion_list['question_code']),1,2);?>) <?php echo filter_string($subquestion_list['question']);?></strong></p></div>
                                            <?php				 
                                                            $total_options = count($subquestion_list['options']);
                                                            $per_cell = floor(12/$total_options);
                                                    
                                                            foreach($subquestion_list['options'] as $suboption_id => $suboption_list){
                                                                
                                                                $per_cell = ($total_options == 2) ? '1' : $per_cell;
                                            ?>
                                                                <div class="col-lg-<?php echo $per_cell?>">
                                                                    <div class="radio radio-success radio-circle">
                                                                        <input required="required" type="radio" value="<?php echo filter_string($suboption_id)?>" id="in_<?php echo $subquestion_id.'_'.$suboption_id?>" name="q[<?php echo filter_string($subquestion_id)?>]">
                                                                        <label for="in_<?php echo $subquestion_id.'_'.$suboption_id?>">
                                                                           <?php echo $suboption_list?>
                                                                        </label>
                                                                    </div>                                        
                                                                </div>
                                            <?php					
                                                            }//end foreach($subquestion_list['options'] as $suboption_id => $suboption_list)
                                            ?>
                                                            <div class="row hidden" id="in_err_q<?php echo filter_string($subquestion_id)?>"><div class="col-md-12 alert alert-sm alert-danger">Please select one of the options</div></div>
                                            <?php				
                                                         }//end foreach($question_list['sub_question'] as $subquestion_id =>$subquestion_list)
                                                        
                                                    }//end if(count($question_list['sub_question']) == 0)
                                                    
                                                }else{
                                            ?>
                                                    <div class="col-lg-12">
                                                        <textarea rows="5" style="width:100%; background-color:#fff" id="comment" name="q[<?php echo $question_id?>]" placeholder="[Insert here, if required, additional questions relating to healthcare service provision]" maxlength="250" ></textarea>
                                                        <p><i>Max 250 characters allowed </i></p>	                            
                                                    </div>                    
                                            <?php		
                                                }//end if($question_id !=10)
                                            ?>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                            <?php		
                                }//end foreach($questionnnair_arr as $question_id => $question_list)
                            ?>		
                            
                        </div>
                        
                        <div class="row no-margins">
                            <div class="col-md-10 text-center">
                                <p class="hidden alert alert-danger" id="in_common_err"><strong>One of the option above is not yet selected. Please select the options from the highlighed sections above.</strong></p>
                            </div>
                            <div class="col-md-2 text-right no-margins">
                                <button type="button" name="submit_suv_btn" id="submit_incomplete_suv_btn" value="submit_suv_btn" class="btn btn-success">Submit Survey</button>
                                <input type="hidden" name="pharmacy_id" value="<?php echo $this->session->p_id ?>" readonly="readonly" />
                                <input type="hidden" name="submit_type" value="PAPER" readonly="readonly" />
                                <input type="hidden" name="survey_id" id="incomplete_survey_id" value="" readonly="readonly" />	
                                
                            </div>
                        </div>
                        <div class="row no-margins">
                            <div class="col-md-12">
                                <div style="border: dashed 1px #ccc" class="m-t p-xs">
                                
                                    <small style="color:#999" class="text-justify">
                                    <p>
                                        <?php 
                                            $SURVEY_FOOTER_TEXT = 'SURVEY_FOOTER_TEXT';
                                            $survey_footer_text = get_global_settings($SURVEY_FOOTER_TEXT); //Set from the Global Settings
                                            $survey_footer_text = nl2br(filter_string($survey_footer_text['setting_value']));
                                            echo str_replace('[PHARMACY_NAME]',$this->session->pharmacy_name,$survey_footer_text);
                                        ?>
                                    </p>
                                    </small>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="clearfix"></div>
                  </div>
            <?php		
                }else{
            ?>
                <div class="alert text-danger" style="min-height:500px" ><h4>No incomplete survey available.</h4></div>
                    
            <?php		
                }//end if(count($survey_incomplete_list) > 0)
            ?>
        </div>
        
        <div id="help_videos" class="tab-pane <?php echo ($tab == 8)? 'active' : ''?>">
          <div class="">
            <div class="module2">
              <div class="stripe-5">
                <h4 class="no-margins">Help Videos</h4>
              </div>
            </div>
            <div class="" style="min-height:500px">
              <div class="row">
                <?php 
					if(!empty($video_list)) {
						
  					  	foreach($video_list as $each){
				?>
                            <div class="col-md-6" style="margin-top:20px;">
                                <h4><?php echo filter_string($each['video_title'])?></h4>
                                <?php echo filter_string($each['embed_code'])?>
                                <p><?php echo filter_string($each['video_description'])?></p>
                            </div>
                <?php			
						}//end foreach($video_list as $each)
					}else{
			?>
            			<div class="col-md-12" style="margin-top:10px;">
	                        <div class="alert alert-danger">No Record Found.</div>
                        </div>
            <?php			
					}
  				  ?>
              
              </div>
            </div>
          </div>
        </div>
        
      </div>
    </div>
  </div>
</div>
<!-- This chart is used  to save the SVG for question 9 for survey poster-->
<div id="autoload_charts" class="hidden"></div> 
<div id="age_pie_chart" class="hidden" style="width: 1000px; height:75%;"></div> 
<div id="gender_pie_chart" class="hidden" style="width: 1000px; height:75%;"></div> 

<div id="piechart_9" class="hidden" style="width: 1000px; height:75%;"></div> 
<script src="https://getaddress.io/js/jquery.getAddress-2.0.5.min.js"></script> 

<!-- Add after your form --> 

<?php  
	   // Get POSTCODE API KEY
	   $POSTCODE_KEY = 'POSTCODE_KEY'; 
	   $key = get_global_settings($POSTCODE_KEY);
	   $api_postcode_key = $key['setting_value']; 
?>

<script>
(function($){
$('#postcode_lookup').getAddress({
   api_key: '<?php echo $api_postcode_key;?>',
    <!--  Or use your own endpoint - api_endpoint:https://your-web-site.com/getAddress, -->
    output_fields:{
        line_1: '#address',
        line_2: '#address_2',
        line_3: '#line3',
        post_town: '#town',
        county: '#county',
        postcode: '#postcode'
    },
<!--  Optionally register callbacks at specific stages -->                                                                                                               
    onLookupSuccess: function(data){/* Your custom code */},
    onLookupError: function(){/* Your custom code */},
    onAddressSelected: function(elem,index){/* Your custom code */}
});
})(jQuery);

jQuery(document).ready(function(){

	jQuery('.show_hide_textbox').click(function(){
		
		checked_val = jQuery(this).attr('value');

		if(checked_val == 4 || checked_val == 226){
			jQuery('#question_10_txtbox').removeClass('hidden');
		}else{
			jQuery('#question_10_txtbox').addClass('hidden');
			jQuery('#other_reason_txt').val('');
			
		}
	})
	
	jQuery('.show_hide_textbox_in').click(function(){
		
		checked_val = jQuery(this).attr('value');

		if(checked_val == 4 || checked_val == 226){
			jQuery('#question_10_txtbox_in').removeClass('hidden');
		}else{
			jQuery('#question_10_txtbox_in').addClass('hidden');
			jQuery('#other_reason_txt_in').val('');
			
		}
	})
	
});
</script> 