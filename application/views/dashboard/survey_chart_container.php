<?php 
$bar_color_arr = array('#5CB85C','#5BC0DE','#D9534F','#F0AD4E','#00BBA7','#FFD700','#E5E4E2');
$btn_color_arr = array('btn-success','btn-info','btn-danger','btn-warning','btn-seagreen','btn-yellow','btn-grey');

?>
<!-- Overlay Section -->
<div class="overlay hidden" id="overlay_survey">
  <div align="center" style="margin-top:150px;"><i class="fa fa-refresh fa-spin" style="font-size:40px"></i></div>                
</div>

<div class="row">
    <div id="survey_carousel" class="carousel slide" data-ride="carousel" data-interval="false">

      <!-- Wrapper for slides -->
      <div class="carousel-inner" role="listbox">
        <?php 
            $k=0;
            foreach($survey_question_stats as $p_question_id => $question_arr){
                $k++;

                if($question_arr['parent_id'] == 0 && !$question_arr['children']){
                    //QUESTIONS WITH NO PARENTS AND HAVE NO CHILDRRENS
        ?>
                <div class="item <?php echo ($k==1) ? 'active': ''?>">
                    <div class="row">
                        <div class="col-md-1"></div> 
                        <div class="col-md-11"><p class="text-justify no-margins p-l-sm" style="padding-right:125px;"><strong>Question <?php echo filter_string($question_arr['question_code'])?>:</strong>  <?php echo filter_string($question_arr['question'])?> <i><?php echo filter_string($question_arr['sub_notes']);?></i></p></div>
                        
                    </div>
                    
                        <div id="column_3d_<?php echo filter_string($question_arr['question_id'])?>" style="width: 100%;"></div> 

                        <script type="application/javascript">
                            jQuery(document).ready(function(){
                                
                                  google.charts.load("current", {packages:["corechart"]});
                                  google.charts.setOnLoadCallback(drawChart);
                                  
                                  function drawChart() {
                                      
                                    var data = google.visualization.arrayToDataTable([
                                      ['Option', 'Percentage', { role: 'style' }],
                                      <?php 
                                      
                                        if($question_arr['question_id'] != 10 && $question_arr['question_id'] != 45){
                                            
                                            $j=0;
                                            foreach($question_arr['options_attempts'] as $option_title => $option_value){
                                                
                                                $total_survey_attempt = ($question_arr['total_survey_attempt'] == 0) ? 1 : $question_arr['total_survey_attempt'];
                                                $eval_percentage = number_format( ($option_value / $total_survey_attempt) * 100, 2);
                                                
                                        ?>
                                                ["<?php echo substr(filter_string($option_title),0,25).'..'; ?>", <?php echo ($eval_percentage)?>, '<?php echo $bar_color_arr[$j] ?>'],
                                        <?php	
                                                $j++;	
                                            }//end foreach($question_arr['options_attempts'] as $option_title => $option_value)

                                        }else{
                                            
                                            $total_survey_attempt = ($question_arr['total_survey_attempt'] == 0) ? 1 : $question_arr['total_survey_attempt'];
                                            $eval_commented_percentage = number_format( ($question_arr['total_commented'] / $total_survey_attempt) * 100, 2);
                                            
                                            $eval_non_commented_percentage = number_format( ($question_arr['total_non_commented'] /$total_survey_attempt) * 100, 2);
                                    ?>
                                            ['Total Commented', <?php echo filter_percentage($eval_commented_percentage)?>, '5CB85C'],
                                            ['Total Non Commented', <?php echo filter_percentage($eval_non_commented_percentage)?>, '#5BC0DE']
                                    
                                    <?php		
                                        }//end if($question_arr['question_id'] != 10)
                                      ?>
                                    ]);
                            
                                    var options = {
                                      title: '<?php echo filter_string($pharmacy_survey_details['survey_title'])?>',
                                      legend: 'none',
                                      width:'700',
                                      height: '300',
									  vAxis: {minValue: 0}
                                    };
                            
                                    var chart = new google.visualization.ColumnChart(document.getElementById('column_3d_<?php echo filter_string($question_arr['question_id'])?>'));
                                    chart.draw(data, options);
                                    //jQuery('#svg_'+<?php echo filter_string($question_arr['question_id'])?>).val(jQuery('#column_3d_'+<?php echo filter_string($question_arr['question_id'])?>).find('svg').html());
                                    chart_svg = jQuery('#column_3d_'+<?php echo filter_string($question_arr['question_id'])?>).find('svg').html();
                                    chart_svg = '<svg width="700" height="300" style="overflow: hidden;" aria-label="A chart.">'+chart_svg+'</svg>';
                                    jQuery('#svg_'+<?php echo filter_string($question_arr['question_id'])?>).val(chart_svg);
                                    //jQuery('#svg_777').val(chart_svg);
                                  }
                                  
                                  <?php 
                                    if($question_arr['question_id'] == 9 || $question_arr['question_id'] == 44){

                                ?>
                                        google.charts.load("current", {packages:["corechart"]});
                                        google.charts.setOnLoadCallback(drawChart_pie9);
                                  
                                          function drawChart_pie9(){
                                              
                                            var data = google.visualization.arrayToDataTable([
                                              ['Option', 'Percentage', { role: 'style' }],
                                              <?php 
                                                $j=0;
                                                foreach($question_arr['options_attempts'] as $option_title => $option_value){
                                                    
                                                    $total_survey_attempt = ($question_arr['total_survey_attempt'] == 0) ? 1 : $question_arr['total_survey_attempt'];
                                                    $eval_percentage = number_format( ($option_value / $total_survey_attempt) * 100, 2);
                                            ?>
                                                    ["<?php echo substr(filter_string($option_title),0,25).' ('.number_format($eval_percentage,1).'%)'; ?>", <?php echo filter_percentage($eval_percentage)?>, '<?php echo $bar_color_arr[$j] ?>'],
                                            <?php	
                                                    $j++;	
                                                }//end foreach($question_arr['options_attempts'] as $option_title => $option_value)
                                              ?>
                                            ]);
                                    
                                            var options = {
                                              width:'1000',
                                              height: '800',
                                              is3D: true,
                                              pieSliceText: 'none',
                                              chartArea:{top:20},
											  vAxis: {minValue: 0}
                                             
                                            };
                                    
                                            var chart = new google.visualization.PieChart(document.getElementById('piechart_9'));
                                            chart.draw(data, options);
                                            
                                            chart_svg = jQuery('#piechart_9').find('svg').html();
                                            chart_svg = '<svg viewBox="10 0 750 500" width="100%" height="75%" style="overflow: hidden;" aria-label="A chart.">'+chart_svg+'</svg>';
                                            jQuery('#svg_poster_9').val(chart_svg);
											
											//jQuery('#pie_9').val(jQuery('#svg_poster_9').val());

                                          }//end drawChart_pie9

                                <?php		
                                    }//end if($question_arr['question_id'] == 9)
                                  ?>
                            });
                        </script>
                        <div class="row">
                            
                            <div class="col-md-12">
                                <?php if(filter_string($question_arr['question_id']) == 10 || filter_string($question_arr['question_id']) == 1 || filter_string($question_arr['question_id']) == 36 || filter_string($question_arr['question_id']) == 45){?>
                                
                                    <p class="text-center no-margins"><strong><i class="fa fa-comment-o"></i> <a class="fancy_ajax_close_btn fancybox.ajax" href="<?php echo SURL?>dashboard/view-question-all-comments/<?php echo filter_string($pharmacy_survey_details['id']) ?>/<?php echo filter_string($question_arr['question_id']) ?>">View Comments</a></strong> </p>
                                    
                                <?php }//end if(filter_string($question_arr['question_id']) == 10 || filter_string($question_arr['question_id']) == 1)?>
                            
                            </div>
                            <div class="col-md-12 text-center p-b-20">
                                
                                <?php 
                                    if($question_arr['question_id'] != 10 && $question_arr['question_id'] != 45){
                                        
                                        $j=0;
                                        foreach($question_arr['options_attempts'] as $option_title => $option_value){
                                            
                                             $total_survey_attempt = ($question_arr['total_survey_attempt'] == 0) ? 1 : $question_arr['total_survey_attempt'];
                                             $eval_percentage = number_format( ($option_value / $total_survey_attempt) * 100, 2);
                                    ?>
                                            <button <?php echo ($question_arr['question_id'] == 8 || $question_arr['question_id'] == 13) ? 'style="width:200px; height:93px"' : ''?>  class="btn btn-sm <?php echo $btn_color_arr[$j]?>" type="button"> <?php echo wordwrap(filter_string($option_title), 30, "<br />\n"); ?>  
                                            <br /> <strong style="font-size:18px"><?php echo filter_percentage($eval_percentage)?>%</strong></button>
                                    <?php		
                                            $j++;
                                        }//end foreach($question_arr['options_attempts'] as $option_title => $option_value)

                                    }else{
                                        
                                        $total_survey_attempt = ($question_arr['total_survey_attempt'] == 0) ? 1 : $question_arr['total_survey_attempt'];
                                        $eval_commented_percentage = number_format( ($question_arr['total_commented'] / $total_survey_attempt) * 100, 2);
                                        $eval_non_commented_percentage = number_format( ($question_arr['total_non_commented'] / $total_survey_attempt) * 100, 2);
                                ?>
                                        <button class="btn btn-sm <?php echo $btn_color_arr[0]?>" type="button"> Total Commented  <br /> <strong style="font-size:18px"><?php echo filter_percentage($eval_commented_percentage)?>%</strong></button>
                                        
                                        <button class="btn btn-sm <?php echo $btn_color_arr[1]?>" type="button"> Total Non Commented  <br /> <strong style="font-size:18px"><?php echo filter_percentage($eval_non_commented_percentage) ?>%</strong></button>
                                <?php		
                                    }//end if($question_arr['question_id'] != 10)
                                ?>
                            </div>
                            
                            <?php 
                                if($pharmacy_survey_details['survey_status'] == '1'){
                                    //Survey is set to be closed time to Finalize the report by adding the comments against each survey
                            ?>
                                    <div class="col-md-1"></div>
                                    <div class="col-md-10 text-center p-b-20" align="center">
                                        
                                        <div class="input-group" >

                                        <textarea style="width:97%; margin-left:19px;; height:80px"  placeholder="enter comments" name="comment_<?php echo filter_string($question_arr['question_id']) ?>" id="comment_<?php echo filter_string($question_arr['question_id']) ?>" class="form-control input-sm"><?php echo filter_string($question_arr['closing_question_comments']['comments'])?></textarea>
                                            
                                            <span class="input-group-addon">
                                                <a href="javascript:;" class="survey_chart_comments" data-survey="<?php echo filter_string($pharmacy_survey_details['id']) ?>" data-question="<?php echo filter_string($question_arr['question_id']) ?>"><span class="fa fa-save"></span></a>
                                            </span>
                                        </div>
                                        <p class="text-success hidden" id="success_msg_<?php echo filter_string($question_arr['question_id']) ?>"></p>
                                    </div>
                                    <div class="col-md-1"></div>
                            <?php		
                                }elseif($pharmacy_survey_details['survey_status'] > 1){
                            ?>
                                    <div class="col-md-1"></div>
                                    <div class="col-md-10 text-center p-b-20" align="center">
                                        <textarea style="width:97%; margin-left:19px" readonly="readonly" class="form-control input-sm"><?php echo filter_string($question_arr['closing_question_comments']['comments'])?></textarea>
                                        
                                    </div>
                                    <div class="col-md-1"></div>
                                    
                            <?php			
                                }//end if($pharmacy_current_survey['survey_status'] == '1')
                            ?>
                        </div>
                </div>
        <?php
                }elseif($question_arr['parent_id'] == 0 && $question_arr['children']){
                    
                    //QUESTIONS WITH CHILDRENS/ SUB QUESTION
                    
                    foreach($question_arr['children'] as $sub_q_id => $sub_q_arr){
            ?>
                        <div class="item">
                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-11"><p class="text-justify no-margins p-l-sm" style="padding-right:125px;">
                                    <strong>Question <?php echo filter_string($sub_q_arr['question_code'])?>:</strong>  <?php echo filter_string($question_arr['question'])?>
                                    <br />
                                    <strong><i>(Option: <?php echo filter_string($sub_q_arr['question'])?>)</i></strong>                                        
                                </p></div>
                            </div>
                        
                                <div id="column_3d_<?php echo filter_string($sub_q_arr['question_code'])?>" style="width: 100%;"></div> 
                                <script type="application/javascript">
                                    jQuery(document).ready(function(){
                                        
                                          google.charts.load("current", {packages:["corechart"]});
                                          google.charts.setOnLoadCallback(drawChart);
                                          
                                          function drawChart(){
                                              
                                            var data = google.visualization.arrayToDataTable([
                                              ['Option', 'Percentage', { role: 'style' }],
                                              <?php 
                                                    
                                                    $j=0;
                                                    foreach($sub_q_arr['options_attempts'] as $sub_option_title => $sub_option_value){
                                                        
                                                        $total_survey_attempt = ($sub_q_arr['total_options_attempted'] == 0) ? 1 : $sub_q_arr['total_options_attempted'];
                                                        $eval_percentage = number_format( ($sub_option_value / $total_survey_attempt) * 100, 2);
                                                ?>
                                                        ["<?php echo filter_string($sub_option_title)?>", <?php echo ($eval_percentage)?>, '<?php echo $bar_color_arr[$j] ?>'],
                                                <?php	
                                                        $j++;	
                                                    }//end foreach($sub_q_arr['options_attempts'] as $sub_option_title => $sub_option_value)
                                              ?>
                                            ]);
                                    
                                            var options = {
                                              title: '<?php echo filter_string($pharmacy_survey_details['survey_title'])?>',
                                              legend: 'none',
                                              width:'700',
                                              height: '300',
											  vAxis: {minValue: 0}
                                            };
                                    
                                            var chart = new google.visualization.ColumnChart(document.getElementById('column_3d_<?php echo filter_string($sub_q_arr['question_code'])?>'));
                                            chart.draw(data, options);
                                            
                                            chart_svg = jQuery('#column_3d_<?php echo filter_string($sub_q_arr['question_code'])?>').find('svg').html();
                                            chart_svg = '<svg width="700" height="300" style="overflow: hidden;" aria-label="A chart.">'+chart_svg+'</svg>';
                                            
                                            jQuery('#svg_<?php echo filter_string($sub_q_arr['question_code'])?>').val(chart_svg);
                                            
                                          }//end drawChart()											
                                    });
                                </script>

                                <div class="row">
                                    <div class="col-md-12 text-center p-b-20">
                                    <?php 
                                        $j=0;
                                         foreach($sub_q_arr['options_attempts'] as $sub_option_title => $sub_option_value){
                                            
                                             $total_survey_attempt = ($sub_q_arr['total_options_attempted'] == 0) ? 1 : $sub_q_arr['total_options_attempted'];
                                             $eval_percentage = number_format( ($sub_option_value / $total_survey_attempt) * 100, 2);
                                    ?>
                                            <button class="btn btn-sm <?php echo $btn_color_arr[$j]?>" type="button"> <?php echo filter_string($sub_option_title)?>  <br /> <strong style="font-size:18px"><?php echo filter_percentage($eval_percentage)?>%</strong></button>
                                    <?php		
                                            $j++;
                                        }//end  foreach($sub_q_arr['options_attempts'] as $sub_option_title => $sub_option_value){
                                    
                                    ?>
                                    </div>
                                </div>
                            <?php 
                                if($pharmacy_survey_details['survey_status'] == '1'){
                                    //Survey is set to be closed time to Finalize the report by adding the comments against each survey
                            ?>
                                    <div class="col-md-1"></div>
                                    <div class="col-md-10 text-center p-b-20" align="center">
                                        <div class="input-group" >
                                            <textarea style="width:97%; margin-left:19px" placeholder="enter comments" name="comment_<?php echo filter_string($question_arr['question_id']) ?>" id="comment_<?php echo filter_string($sub_q_id) ?>" class="form-control input-sm"><?php echo filter_string($sub_q_arr['closing_question_comments']['comments'])?></textarea>
                                            <span class="input-group-addon">
                                                 <a href="javascript:;" class="survey_chart_comments" data-survey="<?php echo filter_string($pharmacy_survey_details['id']) ?>" data-question="<?php echo filter_string($sub_q_id) ?>"><span class="fa fa-save"></span></a>
                                            </span>
                                        </div>
                                        <p class="text-success hidden" id="success_msg_<?php echo filter_string($sub_q_id) ?>"></p>
                                    </div>
                                    <div class="col-md-1"></div>
                            <?php		
                                }elseif($pharmacy_survey_details['survey_status'] > 1){
                            ?>
                                    <div class="col-md-1"></div>
                                    <div class="col-md-10 text-center p-b-20" align="center">
                                        <textarea style="width:93%; margin-left:19px" readonly="readonly" class="form-control input-sm"><?php echo filter_string($sub_q_arr['closing_question_comments']['comments'])?></textarea>
                                    </div>
                                    <div class="col-md-1"></div>
                                    
                            <?php			
                                }//end if($pharmacy_current_survey['survey_status'] == '1')
                            ?>
                        </div>
            <?php			
                    }//end foreach($question_arr['children'] as $sub_q_id => $sub_q_arr)
                        
                }//end if($question_arr['parent_id'] == 0 && !$question_arr['children'])
                    
            }//end foreach($survey_question_stats as $p_question_id => $question_arr)
        ?>
        
      </div>
    
          <!-- Left and right controls -->
          <a class="left carousel-control survey_slider_prev" href="#survey_carousel" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="right carousel-control survey_slider_next" href="#survey_carousel" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
      
        <div class="row" id="slider_pagination">
            <!-- Indicators -->

            <div class="col-md-12">
                <ol class="carousel-indicators carousel-indicators-numbers">
                
                    <?php
                    $k = 0;
					
                    foreach($questionnnair_arr as $question_id => $question_list){
                        
                        if(count($question_list['sub_question']) == 0){
                    ?>
                            <li data-target="#survey_carousel" data-survey="<?php echo filter_string($pharmacy_survey_details['id'])?>" data-slide-to="<?php echo $k?>" id="page_q_<?php echo $question_id?>" class="<?php echo ($k == 0) ? 'active': ''?> survey_slider"><?php echo $question_list['question_code']?></li>
                     <?php 
                            $k++;
                        }else{
                            foreach($question_list['sub_question'] as $subquestion_id => $subquestion_list){
                        ?>
                                <li data-target="#survey_carousel" data-slide-to="<?php echo $k?>" data-survey="<?php echo filter_string($pharmacy_survey_details['id'])?>" id="page_q_<?php echo $subquestion_id?>" class="survey_slider"><?php echo $subquestion_list['question_code'];?></li>
                         <?php
                                $k++;
                            }//end foreach($question_list['sub_question'] as $subquestion_id =>$subquestion_list)

                            
                        }//end if(count($question_list['sub_question']) == 0)
                        
                        
                    }//end foreach($questionnnair_arr as $question_id => $question_list)
                    
                    ?>
                </ol>
            </div>
       </div>
    </div>
</div>

<?php 
	if($pharmacy_survey_details['survey_status'] == '1'){
?>
		<script>

            jQuery('#survey_carousel').bind('slide.bs.carousel', function (e) {
				
				//Saving comments on page change event
				current_page_index = jQuery('#slider_pagination').find('.active');
				current_page_id = jQuery(current_page_index).attr('id');
				current_question_id =  current_page_id.replace('page_q_','');
				question_comment = jQuery('#comment_'+current_question_id).val();
				question_comment = question_comment.trim();
				
				if(question_comment != ''){
					
					jQuery.ajax({
	
						type: "POST",
						url: SURL + "dashboard/save-survey-questions-comments",
						data: {'survey_id' : '<?php echo filter_string($pharmacy_survey_details['id'])?>', 'question_id' : current_question_id, 'comments' : question_comment },		
						beforeSend : function(result){
							
							jQuery("#success_msg_"+current_question_id).addClass("hidden");
							jQuery("#success_msg_"+current_question_id).removeClass("text-danger");
							jQuery("#success_msg_"+current_question_id).removeClass("text-success");
						},
						success: function(result){
							
							json_result = JSON.parse(result);
							
							jQuery("#success_msg_"+current_question_id).removeClass("hidden");
							
							if(json_result.success == 1){
								
								jQuery("#success_msg_"+current_question_id).addClass("text-success");
								jQuery("#success_msg_"+current_question_id).html('Comments saved successfully.');
								
							}else{
								
								jQuery("#success_msg_"+current_question_id).addClass("text-danger");
								jQuery("#success_msg_"+current_question_id).html('Comments cannot save comments. Please try later or contact site admin');	
							}
						}
			
					});
			
				}//end if(question_comment != '')
				
                //Marking the questions as reviewd
                setTimeout(function(){
                    nexe_page_index = jQuery('#slider_pagination').find('.active');
        
                    id = jQuery(nexe_page_index).attr('id');
                    q_id = id.replace('page_q_','');
                    survey_id = jQuery(nexe_page_index).attr('data-survey');
                    
                    if(survey_id != ''){
            			
                        jQuery.ajax({
                    
                            type: "POST",
                            url: SURL + "dashboard/mark-question-reviewed",
                            data: {'survey_id' : survey_id, 'question_id' : q_id},		
                            beforeSend : function(result){},
                            success: function(result){}
                        }); 
						
                    }//end if(survey_id != '')
                    
                 }, 500);
            });
			
			jQuery('#survey_carousel').bind('slide.bs.carousel', function (e) {
                
            });
           
        </script>
<?php		
	}//end if($pharmacy_survey_details['survey_status'] == '1')
?>

