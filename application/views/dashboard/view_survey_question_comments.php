<div class="color-line"></div>
<div class="modal-header text-left">
  <h4 class="modal-title">View Comments</h4>
</div>
<div class="modal-body">
  <div class="row">
    <div class="col-md-12">
    	<?php 
			if($question_id == 10 || $question_id == 45){
		?>
            <table class="table table-hover table-condensed" width="100%">
                <thead>
                    <tr>
                        <th width="10%">Sr. No</th>
                        <th width="90%">Comments</th>
                    </tr>
                </thead>
                <tbody>
                	<?php 
						for($i=0;$i<count($survey_question_stats['comments']);$i++){
					?>
                        <tr>
                            <td><?php echo $i+1?></td>
                            <td><?php echo filter_string($survey_question_stats['comments'][$i]['option_txt']);?></td>
                        </tr>
                    <?php		
						}//end for($i=0;$i<count($survey_question_stats['comments']);$i++)
					?>
                    
                </tbody>
            </table>
        <?php 
			}elseif($question_id == 1 || $question_id == 36){
		?>
                <table class="table table-hover table-condensed" width="100%">
                    <thead>
                        <tr>
                            <th width="10%">Sr. No</th>
                            <th width="90%">Other Reasons</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            for($i=0;$i<count($survey_question_stats['other_reasons_txt_arr']);$i++){
                        ?>
                            <tr>
                                <td><?php echo $i+1?></td>
                                <td><?php echo filter_string($survey_question_stats['other_reasons_txt_arr'][$i]['option_txt']);?></td>
                            </tr>
                        <?php		
                            }//end for($i=0;$i<count($survey_question_stats['comments']);$i++)
                        ?>
                        
                    </tbody>
                </table>
        <?php		
			}//end if($question_id == 10)
		?>
    </div>
  </div>
</div>
