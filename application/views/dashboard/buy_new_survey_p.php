<div style="">
  <div class="color-line"></div>
  <div class="modal-header text-left">
    <h4 class="modal-title">Buy New Survey for  the year <?php echo $survey_session_2?></h4>
  </div>
  <div class="modal-body">
    <div class="row">
      <div class="col-md-12">
      <form class="validate_frm" id="credit_card_frm" action="<?php echo base_url(); ?>dashboard/buy-survey" method="post">
          <div class="row">
            <div class="contact_content_border2">
                <p class="current_survey_notes"><i class="fa fa-info-circle"></i> You can buy new survey for the year <?php echo $survey_session_2?> now. You will be able to start the new survey after 31<sup>st</sup> of March.</p>
                <h4>Select Payment Method</h4>
                  <p>Please select any of the payment method to buy a new survey.</p>
                  
                  <div class="col-md-8">
                    <div class="payments_tabs">
                      <div class="row for_row_escap">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#card_tab" data-toggle="tab"><img src="<?php echo IMAGES?>visa_mastercard_logo.png" width="99px" /></a></li>
                            <li><a href="#paypal_tab" data-toggle="tab"><img src="<?php echo IMAGES?>payal-logo.png" width="120px" /></a></li>
                        </ul>
                        <div class="tab-content">
                          <div id="card_tab" class="tab-pane fade in active">
                            <p>Please enter your credit card details below.</p>
                            <div class="single-field half-field ful_field form-group">
                                <label for="card number" style="float: left; width: 200px">Card Number</label>
                                <input name="card" id="card" type="text" placeholder="Card Number" required>
    
                            </div>
                            <div class="row">
                              <div class="col-md-4">
                                <div class="single-field half-field ful_field form-group">
                                  <label for="expMonth">Expiry Month</label>
                                  <select class="form-control" name="expMonth" id="expMonth" required>
                                        <option value="">Month</option>
                                        <?php 
                                            for($i=1;$i<=12;$i++){
                                        ?>
                                                <option value="<?php echo ($i<10) ? '0'.$i : $i?>"><?php echo ($i<10) ? '0'.$i : $i?></option>
                                        <?php
                                            }//end for($i=date('Y');$i<=date('Y')+10;$i++)
                                        ?>
                                      </select>
                                </div>
                              </div>
                              <div class="col-md-4">
                                <div class="single-field half-field ful_field form-group">
                                  <label>&nbsp;</label>
                                  <select class="form-control" name="expYear" id="expYear" required>
                                    <option value="">Year</option>
                                    <?php 
                                        for($i=date('Y');$i<=date('Y')+10;$i++){
                                    ?>
                                            <option value="<?php echo $i?>"><?php echo $i?></option>
                                    <?php
                                        }//end for($i=date('Y');$i<=date('Y')+10;$i++)
                                    ?>
                                  </select>
                                </div>
                              </div>
                              <div class="col-md-4">
                                <div class="single-field half-field ful_field form-group">
                                  <label>CVV</label>
                                  <input class="" name="cvc" id="cvc" type="text" placeholder="CVV" required>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div id="paypal_tab" class="tab-pane fade">
                            <p>You can make payment using your PayPal account. </p>
                            <div class="row">
                              <div class="col-md-5">
                                <h5 class="paypal_accepts">Paypal accepts: </h5>
                              </div>
                              <div class="col-md-5"> <img src="<?php echo IMAGES?>paypal_cards.jpg" alt=""> </div>
                            </div>
                            <div class="clearfix"></div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="text-right"> 
                        <button name="add_pharamcy_btn" type="submit" id="save_btn" value="Make Payment" class="btn btn-success no-margins">Make Payment</button>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4 padd_right_0">
                    <div class="contact_content_border order_suumry">
                      <div class="module2">
                        <div class="stripe-5">
                          <h4> Order Summary</h4>
                        </div>
                      </div>
                      <div class="contact_content_border2">
                        <div class="box1">
                          <div style="height: 5px"></div>
                          <div class="a">
                            <div class="col-md-8"><strong>Survey <?php echo ($survey_session) ? $survey_session : '' ; ?></strong></div>
                            <div class="col-md-4"></div>
                          </div>
                          <div class="clearfix"></div>
                          <hr style="margin:10px">
                          <div class="a">
                            <div class="col-md-8">Sub Total</div>
                            <div class="col-md-4">&pound;<?php echo ($sub_total) ? filter_price($sub_total) : '' ; ?></div>
                          </div>
                          <div class="clearfix"></div>
                          <hr style="margin:10px">
                          <div class="a">
                            <div class="col-md-8">VAT (<?php echo ($vat) ? $vat : '' ; ?>%)</div>
                            <div class="col-md-4">&pound;<?php echo ($vat_amount) ? filter_price($vat_amount) : '' ; ?></div>
                          </div>
                          <div class="clearfix"></div>
                          <hr style="margin:10px">
                          <div class="a">
                            <div class="col-md-8"><strong>GRAND TOTAL</strong></div>
                            <div class="col-md-4"><strong>&pound;<?php echo ($grand_total) ? filter_price($grand_total) : '' ; ?></strong></div>
                          </div>
                          <div class="clearfix"></div>
                        </div>
                      </div>
                    </div>
                  </div>
            </div>
          </div>
              <input type="hidden" name="payment_method_radio" id="payment_method_radio" value="credit_card" />
              <input type="hidden"  value="1" name="next_year" id="next_year" readonly="readonly" />
            </form>
      </div>
    </div>
  </div>
</div>
<script>
	jQuery('.validate_frm').formValidation(); // end #login_frm
</script>
<script src="<?php echo JS?>kod_scripts/dashboard.js"></script>