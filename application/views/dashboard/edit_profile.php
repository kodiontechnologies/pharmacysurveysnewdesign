<main>
  <section class="well5 well6__ins1">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12 wow fadeInDown"> 
          
          <!-- CMS Page title -->
          <h1 class="wow"> Edit Profile </h1>
          <br />
          <hr />
          <?php 
                if($this->session->flashdata('err_message')){
            ?>
          <div class="alert alert-danger"><?php echo $this->session->flashdata('err_message'); ?></div>
          <?php
                }//end if($this->session->flashdata('err_message'))
                if($this->session->flashdata('ok_message')){
            ?>
          <div class="alert alert-success alert-dismissable"><?php echo $this->session->flashdata('ok_message'); ?></div>
          <?php 
                }//if($this->session->flashdata('ok_message'))
            ?>
          
          <!-- CMS Page Description -->
          <p><?php echo (filter_string($page_data['page_description'])) ? filter_string($page_data['page_description']) : '' ; ?><br />
          </p>
        </div>
        <div class="col-md-2 col-sm-3 col-xs-3 "> </div>
        <div class="col-md-8 col-sm-6 col-xs-6 wow fadeInUp">
          <form  data-toggle="validator" role="form" enctype="application/x-www-form-urlencoded" action="<?php echo SURL;?>dashboard/edit-profile-process" class="text-left" method="post" name="registration" id="registration">
            <div class="form-group has-feedback">
              <label for="first_name" class="form-label form-label-outside">First Name<span class="required">*</span></label>
              <input name="first_name" id="first_name" type="text" required="required" class="form-control" data-error="Please use allowed characters (Alphabets, Hyphens, Space or Dot)"  pattern="[a-zA-z]+([ '-.][a-zA-Z]+)*"  maxlength="30"  value="<?php echo filter_string($user_profile_details['first_name']);?>" >
              <div class="help-block with-errors"></div>
            </div>
            <div class="form-group has-feedback">
              <label for="last_name" class="form-label form-label-outside">Last Name<span class="required">*</span></label>
              <input name="last_name" id="last_name" type="text" required="required" class="form-control" data-error="Please use allowed characters (Alphabets, Numbers, Hyphens, Space or Dot)"  pattern="[a-zA-z0-9 -.]+([ '-.][a-zA-Z0-9]+)*"  maxlength="30" value="<?php echo filter_string($user_profile_details['last_name']);?>">
              <div class="help-block with-errors"></div>
            </div>
            <div class="form-group has-feedback">
              <label for="email_address" class="form-label form-label-outside">Email<span class="required">*</span></label>
              <input name="email_address" id="email_address" type="email" required="required" class="form-control" disabled="disabled" value="<?php echo filter_string($user_profile_details['email_address']);?>" >
              <div class="help-block with-errors"></div>
            </div>
            <div class="error help-block" id="error_msg"  style="color:#F00;"></div>
            
            <div class="form-group has-feedback">
              <label for="address" class="form-label form-label-outside">Address <span class="required">*</span></label>
              <input name="address" id="address" type="text" required="required" class="form-control" value="<?php echo filter_string($user_profile_details['address']);?>" >
              <div class="help-block with-errors"></div>
            </div>
            
            <div class="form-group has-feedback">
              <label for="postcode" class="form-label form-label-outside">Post Code<span class="required">*</span></label>
              <input name="postcode" id="postcode" type="text" required="required" class="form-control"  pattern="^[a-z|A-Z|]+[a-z|A-Z|0-9|\s]*" data-error="Please use allowed characters (Alphabets, Numbers and spaces) and first character must be alphabet." maxlength="8" value="<?php echo filter_string($user_profile_details['postcode']);?>">
              <div class="help-block with-errors"></div>
            </div>
            
            <div class="form-group has-feedback">
                <div class="form-group has-feedback">
                  <label for="birth_date_label" class=""> Country<span class="required">*</span></label>
                  <select name="country" id="country" class="form-control" required="required">
                    <option value="">Select Country</option>
                    <?php 
						$country_list_arr = get_active_countries();
						
						for($i=0;$i<count($country_list_arr);$i++){
					?>
		                    <option <?php echo ($country_list_arr[$i]['id'] == $user_profile_details['country']) ? 'selected="selected"' : '' ?> value="<?php echo filter_string($country_list_arr[$i]['id']);?>"> <?php echo filter_string($country_list_arr[$i]['country_name']);?> </option>
                    <?php 
						}//end for($i=0;$i<=count($country_list_arr);$i++)
					?>
                  </select>
                  <div class="help-block with-errors"></div>
                </div>
            </div>
            <!-- /.row -->
            <div class="text-center">
              <button type="submit" id="profile_btn" name="profile_btn" class="btn btn-success pull-right ">Update Profile</button>
            </div>
          </form>
        </div>
        <div class="col-md-2 col-sm-3 col-xs-3 "> </div>
      </div>
    </div>
  </section>
</main>
