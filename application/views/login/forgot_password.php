<div class="module">
  <div class="stripe-4">
    <div class="container">
      <div class="row">
        <h2>Forgot Password</h2>
        <ul class="breadcrumbs">
          <li><a href="<?php echo SURL?>">Home</a></li>
          <li> > </li>
          <li class="active">Forgot Password</li>
        </ul>
      </div>
    </div>
  </div>
</div>
<div class="site-content pt-80"> 
  <!-- Start Fullwide Map -->
  <div class="fullwide-map"> 
    
    <!-- End Fullwide Map -->
    <div class="container">
      <div class="row">

        <div class="col-md-12">
          <div class="content-area pt-100">
            <?php
                if($this->session->flashdata('err_message')){
            ?>
            <div class="alert alert-danger hide_alert"><?php echo $this->session->flashdata('err_message'); ?></div>
            <?php
               }//end if($this->session->flashdata('err_message'))
                
                if($this->session->flashdata('ok_message')){
            ?>
            <div class="alert alert-success alert-dismissable hide_alert"><?php echo $this->session->flashdata('ok_message'); ?></div>
            <?php 
                }//if($this->session->flashdata('ok_message'))
            ?>
          
            <div class="row">
              <div class="col-md-3"></div>
              <div class="col-xs-12 col-sm-8 col-md-6 pb-100">
                <div class="contact_content_border">
                  <div class="module2">
                    <div class="stripe-5">
                      <h4>Forgot Password</h4>
                    </div>
                  </div>
                  <div class="contact_content_border2">
	                  <p><?php echo ($cms_page['page_description']) ? filter_string($cms_page['page_description']) : '' ; ?></p>
                    <form id="forgot_pass_frm" name="forgot_pass_frm" method="post"  enctype="multipart/form-data" action="<?php echo SURL;?>login/forgot-password-process">
                      <!-- End Single Field -->
                      <div class="single-field full-field form-group">
                        <label for="email_address">Email address</label>
                              <input type="email" class="form-control" id="username" name="username"  placeholder="Email address" required="required" data-fv-regexp="true" data-fv-regexp-regexp="^[a-zA-z0-9\s\-]+[a-zA-Z0-9\s\-_.@]+$"
                          data-fv-regexp-message="Please use allowed characters (Alphabet, Numbers, Hyphens, Underscore, Dot and @)" autocomplete="off">
                      </div>
                      <!-- End Single Field -->
                      
                      <div class="col-md-12 no-paddings">
                      <div id="wrap" align="left">
                        <div class="col-sm-12" style="padding-left:7px;"> <img align="left" src="<?php echo SURL;?>login/get-captcha" alt="" id="captcha" /> </div>
                        <div class="col-sm-12" style="">Please type the numbers above in the box below.</div>
                        <div class="col-sm-12 form-group">
                          <div class="col-sm-5" style="padding-left:4px;">
                            <input name="g-recaptcha-response" type="text" required>
                          </div>
                          <div class="col-sm-5" style="padding:10px 9px 20px 0;"> Try different one? <img src="<?php echo ASSETS;?>captcha/refresh.jpg" alt="" id="refresh" width="25" align="right"> </div>
                        </div>
                      </div>
                    </div>
                      
                      <!-- End Single Field -->
                      <div class="text-right"> 
                        <button name="fgetpass_btn" id="fgetpass_btn" type="submit" value="Submit" class="btn btn-success">Submit</button>
                      </div>
                      <div class="clearfix"></div>
                    </form>
                    
                  </div>
                </div>
                <div class="not_a_member"> <a class="text-info" href="<?php echo SURL?>register">Not a member yet? Register Now</a>&nbsp; | 
                  &nbsp;<a class="text-info" href="<?php echo SURL?>login">Already a Member? Login Here</a> </div>
              </div>
              <div class="col-md-3"></div>
            </div>
                        
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
jQuery(document).ready(function() { 
 // refresh captcha
   jQuery('#refresh').click(function() {  
    document.getElementById('captcha').src="<?php echo SURL;?>login/get-captcha?rnd=" + Math.random();
   });
});
</script> 
