<!DOCTYPE html>
<html lang="en">
    <head>

        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta charset="utf-8" />
        <title>Login Page - CD Register</title>

        <meta name="description" content="User login page" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

        <?php echo $login_header; ?>

    </head>

    <body class="login-layout">

        <div class="main-container">
            <div class="main-content">

                <?php echo $content; ?>

            </div><!-- /.main-content -->
        </div><!-- /.main-container -->

        <?php echo $login_footer; ?>
        
    </body>
    
</html>
