<!DOCTYPE html>

<html lang="en-US">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo $title; ?></title>
<!-- Start - Kod-Header Scripts -->
<?php
        echo $meta;        
        echo $header_scripts;
        echo $css;
    ?>
<!-- End - Header Scripts -->

</head>

<body>

<!-- Kod / header --> 
<?php echo $header_contents; ?>
<div id="site-container"> 

<?php echo $content; ?> </div>

<!-- Footer --> 
<!-- Start - Kod-Footer --> 
<?php echo $footer_contents; ?> 
<!-- End - Kod-Footer --> 

<?php echo $footer_scripts; ?>
</body>
</html>
