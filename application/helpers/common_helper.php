<?php

//Function print_this(&$array): Used for development purpose. Pritning Array or any Object with <pre>.
function print_this(&$arr){
	
	echo '<pre>';
	print_r($arr);
	
}//end print_this(&$array);

//Function filter_string():  will filter the string from unwanted characters while displaying it on teh screen.
function filter_string(&$string){
	
	$ci =& get_instance();
	
	$filter_txt = stripcslashes(trim($string));
	
	return $filter_txt;
	
}//end filter_string()

//WIll retuirn date format in MM/dd/YY, if time is true it will return the time as well
function filter_uk_date($date, $time = false){
	
	if($time)
		return date('d/m/Y G:i:s',strtotime($date));
	else
		return date('d/m/Y',strtotime($date));
		
}//end filter_uk_date($date, $time = false)


//Function filter_price():  Wil change the price formats as per the need.
function filter_price(&$price){
	
	$ci =& get_instance();
	
	$filter_price = number_format($price,2);
	
	return $filter_price;
	
}//end filter_price()

//Function filter_price():  Wil change the price formats as per the need.
function filter_quantity(&$quantity){
	
	$ci =& get_instance();
	
	if(trim($quantity) != ''){
		
		if(fmod(filter_string($quantity),1) == 0)
			$filter_qty = number_format(filter_string($quantity), 0,'.','');
		else
			$filter_qty =  filter_string($quantity); 

	}else{

		$filter_qty =  0;
		
	}//end if(trim($quantity) != '')

	return $filter_qty;
	
}//end filter_price()

//Function filter_percentage():  Wil change the Percentage formats as per the need.
function filter_percentage(&$percentage){
	
	$filter_per = number_format($percentage,0);
	return $filter_per;
	
}//end filter_percentage()

//Function filter_price():  Wil change the price formats as per the need.
function filter_name($name){
	
	$filter_txt = ucfirst(stripcslashes(trim($name)));
	return $filter_txt;
	
}//end filter_price()

//Function random_number_generator($digit): random number generator function
function random_number_generator($digit){
	$randnumber = '';
	$totalChar = $digit;  //length of random number
	$salt = "0123456789abcdefjhijklmnopqrstuvwxyz";  // salt to select chars
	srand((double)microtime()*1000000); // start the random generator
	$password=""; // set the inital variable
	
	for ($i=0;$i<$totalChar;$i++)  // loop and create number
	$randnumber = $randnumber. substr ($salt, rand() % strlen($salt), 1);
	return $randnumber;
	
}// end random_password_generator()


//Function filter_price():  Wil change the price formats as per the need.
function get_global_settings(&$setting_name){
	
	$ci =& get_instance();
	
	$ci->db->dbprefix('global_settings');
	$ci->db->where('setting_name',$setting_name);
	$get_result = $ci->db->get('global_settings');
	
	return $get_result->row_array();
	
}//end get_global_settings(&$setting_name)

require(APPPATH.'libraries/AWSSDKforPHP/SESUtils.php');

function send_email($to_email_address, $from_email, $email_from_txt, $email_subject, $email_body, $attachment_arr = array()){
	
	$SES_KEY = 'SES_KEY';
	$ses_key = get_global_settings($SES_KEY);
	$ses_key = trim($ses_key['setting_value']);

	$SES_SECRET = 'SES_SECRET';
	$ses_secret = get_global_settings($SES_SECRET);
	$ses_secret = trim($ses_secret['setting_value']);

	$SES_REGION = 'SES_REGION';
	$ses_region = get_global_settings($SES_REGION);
	$ses_region = trim($ses_region['setting_value']);

	//////////////////////////////////////////////////////////////

	/*
	$attachment_arr[] = 
					array('name' => 'kodion-fbr.pdf',
							'mime' => 'application/pdf',
							'filepath' => 'kodion-fbr.pdf'
				);
	*/
	  //Now that you have the client ready, you can build the message
	  $msg = array();
	  $msg['from'] = "$email_from_txt<$from_email>";
	  //ToAddresses must be an array
	  $msg['to'] = $to_email_address;
	  $msg['subject'] = $email_subject;
	  $msg['message'] = $email_body;
	  $msg['files'] = $attachment_arr;
	  $msg['ses_key'] = $ses_key;
	  $msg['ses_secret'] = $ses_secret;
	  $msg['ses_region'] = $ses_region;
	
	//send the email

	$result = SESUtils::sendMail($msg);
	
	//now handle the result if you wish
	//print_r($result);
	
	return true;

}//end send_email($to, $from_email, $from_text, $subject, $email_body)


// Start - function kod_date_format($date='', $time=''): Function to return the formated date (first parameter date and second is boolean true or false to get time)
if (!function_exists('kod_date_format')) {
	function kod_date_format($date = '', $time = FALSE){
		
		if(substr($date,0,4) != '0000'){
			$date1 = $date;
			if($date != ''){
				
				if($time == true)
					return date_format(date_create($date),"j M, Y g:i a");
				elseif($time == false)
					return date_format(date_create($date),"j M, Y");
					
			} else
				return $date;
				
		}else{
			return $date;
			
		}//end if(substr($date,0,4) != '0000')
		
	}//end kod_date_format($date='', $time='')

} // End - function kod_date_format($date='', $time='')

function get_pharmacy_survey_list($pharmacy_id, $survey_status = '', $survey_id = '', $is_started = ''){
	
	$ci =& get_instance();
	return $ci->survey->get_pharmacy_survey_list($pharmacy_id, $survey_status, $survey_id, $is_started);
	
}//end get_pharmacy_survey_listget_pharmacy_survey_list($pharmacy_id, $survey_status = '')

function get_pharmacy_incomplete_survey_list($pharmacy_id){
	
	$ci =& get_instance();
	return $ci->survey->get_pharmacy_incomplete_survey_list($pharmacy_id);
	
}//end get_pharmacy_survey_listget_pharmacy_survey_list($pharmacy_id)


function get_most_recent_survey($pharmacy_id){
	
	$ci =& get_instance();
	return $ci->survey->get_most_recent_survey($pharmacy_id);
	
}//end get_pharmacy_survey_listget_pharmacy_survey_list($pharmacy_id, $survey_status = '')

function get_survey_submitted_answers($survey_submitted_id, $question_id = ''){

	$ci =& get_instance();
	return $ci->survey->get_survey_submitted_answers($survey_submitted_id, $question_id);
	
}//end get_survey_submitted_answers($survey_submitted_id, $question_id = '')

function get_survey_question_closing_comments($pharmacy_id, $survey_id, $question_id){

	$ci =& get_instance();
	return $ci->survey->get_survey_question_closing_comments($pharmacy_id, $survey_id, $question_id);
	
}//end get_survey_submitted_answers($survey_submitted_id, $question_id = '')

//Function get_survey_submitted_count(): Returns the number of Survey submitted so far
function get_survey_submitted_count($pharmacy_id, $survey_id, $submit_type = ''){

	$ci =& get_instance();
	return $ci->survey->get_survey_submitted_count($pharmacy_id, $survey_id, $submit_type);

	
}//end get_survey_submitted_count($this->session->p_id, $pharmacy_current_survey['id'])

//Function get_survey_questionnaire_details(): Get the Record from the Questinnair Table to get teh question record
function get_survey_questionnaire_details($question_id){

	$ci =& get_instance();
	return $ci->survey->get_survey_questionnaire_details($question_id);
	
}//end get_survey_questionnaire_details($question_id)


function get_survey_question_stats($pharmacy_id, $survey_id, $survey_ref_no, $question_id = '', $pharmacy_type){

	$ci =& get_instance();
	return $ci->survey->get_survey_question_stats($pharmacy_id, $survey_id, $survey_ref_no, $question_id, $pharmacy_type);
	
}//end get_survey_submitted_answers($survey_submitted_id, $question_id = '')

// Start => function get_cms_page($url_slug)
function get_cms_page($url_slug){

	$ci =& get_instance();
	return $ci->cms->get_cms_page($url_slug);

} // End => function get_cms_page($url_slug)

?>